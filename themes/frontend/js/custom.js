jQuery(function() {
	$(window).load(function() {

		$('#menu-toggle').click(function(e) {
			$('#mobile-menu-collapse').slideToggle("slow");
			e.preventDefault();
		});


		$(".search-toggle-btn").click(function() {
			$(".search-toggle").toggleClass("is-triggered");
			$(".search-toggle-btn").toggleClass("is-triggered");
		});

		$("input.product-quantity").TouchSpin({
			min: 1,
			max: 1000000,
			verticalbuttons: true,
			verticalupclass: 'glyphicon glyphicon-plus',
			verticaldownclass: 'glyphicon glyphicon-minus'
		});

		$('#create').click(function(){
			$('.form-create').css('display', 'block');
		});

		$('#guest').click(function() {
			$('.form-create').css('display', 'none');
		});

		
		$(".slider-primary .slides").slick({
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			autoplay: false,
			arrows : false,
		});
		$(".slider-title .slides").slick({
			dots: false,
			infinite: true,
			speed: 500,
			slidesToShow: 1,
			autoplay: false,
			arrows : false,
		});
		$('.slider-single .slides').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-single-nav .slides'
		});
		$('.slider-single-nav .slides').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.slider-single .slides',
			dots: false,
			centerMode: false,
			focusOnSelect: true,
			responsive:[
			{
				breakpoint: 580,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 1000,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
				}
			},
			],
		});
	});

	// BOL - Info tabs : table - convert it to table format
	if($(".info_table").length && $(".info_table_preview").length)
	{
		var parse_info_table = function(){

			var value 	= $(".info_table").val();

			$.ajax({
				type: 'POST',
				url: baseUrl + 'product/infotab/parse_info_table',
				data: {
					table: value
				},
				success: function(data) {

					if(data != '')
						$('.info_table_preview').html(data);
					else
						$('.info_table_preview').html($(".info_table_empty").html());
				}
			});
		};

		var initiate_parsing = function(){
			
			$('.info_table_preview').html($(".info_table_loading").html());
			
			var timer 	= $('.info_table').data('timeout');
			var n 		= 2;// the number of seconds to wait

			if(timer) {
				clearTimeout(timer);
				$('.info_table').removeData('timeout');
			}

			$('.info_table').data('timeout', setTimeout(parse_info_table, 600 * n));
		};
		initiate_parsing();
	}
	// END - Info tabs : table - convert it to table format

	// BOL - Info tabs : hotspot - hotspot image
	if($(".hotspot-show-data").length)
	{
		$('.hotspot-show-data').click(function(e){
			e.preventDefault();
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: baseUrl + 'product/infotab/get_hotspot/'+$(this).attr('rel'),
				success: function(data) {
					var html = '<h3>'+data.title+'</h3>';
					html += data.description;
					if(data.image)
					{
						html += '<a class="btn btn-green btn-block" target="_blank" href="'+data.image.url+'" >View Image</a>';
					}
					if(data.video)
					{
						html += '<a class="btn btn-green btn-block" target="_blank" href="'+data.video.url+'" >View Video</a>';
					}
					$('.hotspot-view-data').html(html);
				}
			});
		});
	}
	// END - Info tabs : hotspot - hotspot image

});
