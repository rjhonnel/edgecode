function setAlert($html)
{
    $('.info-wrap').hide(function(){
        $('.info-box').html($html);
        $('.info-wrap').slideDown('500');
    });
}

function showLoader($selector)
{
     var $loading_img = baseUrl + 'themes/frontend/images/select2-spinner.gif';
     $loading_img = '<div align="center"><img src="' + $loading_img + '"/></div>';
     $selector.html('').append($loading_img).children().fadeIn().parent().delay(300);
}

function showError($selector)
{
     var $error_img = baseUrl + 'themes/frontend/images/error.gif';
     $error_img = '<div align="center"><img src="' + $error_img + '"/></div>';
     $selector.html('').append($error_img).children().fadeIn().parent().delay(300);
}

function showMsg($selector, $data)
{
    $selector.append($data).show('fast');
}

function cursorLoader()
{
     document.body.style.cursor = "wait";
}

function cursorDefault()
{
    document.body.style.cursor = "default";
}

function inColorbox(element)
{
    if(element.parents('#colorbox').first().length > 0) return true;
    return false;
}

function cartInfo(){
    $.ajax({
        dataType: 'json',
        type: 'get',
        url: baseUrl + 'order/cart/cart_info',
        success: function(data){
            $('.total_cart_items').text(data.items);
        }
    }); 
}

 // Open cart tab
function openCart(){
    $.ajax({
        dataType: 'html',
        type: 'post',
        url: baseUrl + 'order/cart/open_cart',
        success: function(data){
            $('.cart_wrap').html(data);
        }
    });
    $('.cart_wrap').slideDown(400);
}

function productCarousel(){
    $('#carousel').carouFredSel({
        responsive: true,
        circular: false,
        auto: false,
        items: {
            visible: 1,
            width: 460,
            height: '120%'
        },
        scroll: {
            fx: 'directscroll'
        }
    });

    $('#thumbs').carouFredSel({
        responsive: false,
        circular: false,
        infinite: false,
        auto: false,
        prev: '#prev',
        next: '#next',
        items: {
            visible: 6,
            width: 80,
            height: 80
        }
    });
 
    $('#thumbs a').click(function() {
        $('#carousel').trigger('slideTo', '#' + this.href.split('#').pop() );
        $('#thumbs a').removeClass('selected');
        $(this).addClass('selected');
        return false;
    });
    
}

$(document).load(function() {
   var prod_img = $('.prod-single .prod-image');
    var prod_dsc = $('.prod-single .prod-desc');

    if(prod_img.outerHeight()>prod_dsc.outerHeight()){
        prod_dsc.height(prod_img.outerHeight());
    }else{
        prod_img.height(prod_dsc.outerHeight());
    }
});
$(document).ready(function() {
     
      // Update cart
    $('body').on('keyup change', 'form.edit_cart_form .product-quantity',function(e) {
        e.preventDefault();
        var _this = $(this);
        $.ajax({
            dataType: 'json',
            type: 'post',
            data: _this.closest('form').serializeArray(),
            url: baseUrl + 'order/cart/update_cart',
            beforeSend: function(){
                _this.closest('td').find('.hold-loader').html('<img src="'+baseUrl+'themes/frontend/assets/images/ajax-loader.gif" class="loader">');
            },
            success: function(data){
                if(data.error){
                    _this.closest('td').find('.hold-loader').html('');
                    _this.closest('td').find('.msg').html('Minimum order of this product is '+ data.minimum_order);
                   
                }
                else{
                    _this.closest('td').find('.msg').html('');
                    _this.closest('tr').find('td.price').text('P'+data.product_subtotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    if(!data.reach_stock_limit)
                    {
                        _this.closest('td').find('.hold-message').html('');
                    }
                    else
                    {
                        if(data.product_stock)
                        {
                            _this.val(data.product_stock);
                            _this.closest('td').find('.hold-message').html('<span class="t-xs text-danger">You can only have '+data.product_stock+' of this item in your cart. </span>');
                        }
                    }

                    if(data.grand_total)
                        $('#cart_grand_total').text('P'+data.grand_total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    _this.closest('td').find('.hold-loader').html('');
                }
            }
        });
    });

    // Messages 
    if($(".message_container").length > 0)
    {
        /**
         * Display system messages throughout website
         */
        $.colorbox({
            html: $('.message_container').html()
        });
    }
    $(".add_to_cart").on('click', function(e){
        e.preventDefault();
        var p_title = $(this).closest('.prod-item').find('.prod-image .prod-title').text();
        $('#addedtocart').find('.modal-subtitle span').text(p_title);

    });
    // TinyNav.js 1
    // $('#sidemenu').tinyNav({
    //   active: 'selected'
    // });
    
    // Front Fixes
    $(".images .images_title:odd").css("background", "#343434");
//    $(".sub_category_images .images .images_title:odd").css("background", "url('images/sub_bgr.png') left top no-repeat");
    
    
    // $(".sidebar .side_box").last().css("border-bottom", "none");
    $('.breadcrumbs li a').last().css({"color" : "#323536", "background" : "none"});
    
    $('body').on('click', '#discuss_button, #discuss_button_inner',function(e) {
        discussHtml();
        $('.contact_form').slideToggle(400);
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });
    
    // Discuss html
    function discussHtml(){
        $.ajax({
            dataType: 'html',
            type: 'get',
            url: baseUrl + 'page/discuss_brief',
            success: function(data){
                $('.contact_form').html(data);
                $('#discuss_form').parsley();
                $('.contact_form .select_init').select2();
            },
        });
    }
    
    // Vertical Accordion
    // $('#sidemenu').dcAccordion();
    
    // Slider
    if($('#news_slider').length > 0){
        $('#news_slider').carouFredSel({
            auto: false,
            responsive: true,
            width: '100%',
            scroll: 1,
            pagination  : "#news_pag",
            items: {
                width: 400,
                //height: '90%',    //  optionally resize item-height
                visible: {
                    min: 1,
                    max: 1
                }
            }
        });
    }
    
    if($('#product_slider').length > 0){
        $('#product_slider').carouFredSel({
            responsive: true,
            auto: false,
            width: '100%',
            scroll: 1,
            pagination  : "#product_pag",
            items: {
                width: 400,
                //height: '90%',    //  optionally resize item-height
                visible: {
                    min: 1,
                    max: 1
                }
            }
        });
    }
    
    if($('.product_slider').length > 0){
        $('.product_slider').each(function(){
            $(this).carouFredSel({
                responsive: true,
                auto: false,
                width: '100%',
                scroll: 1,
                pagination: $(this).parents('.relative_app').first().find('.pagination').first(),
                items: {
                    width: 400,
                    visible: {
                        min: 1, max: 1
                    }
                }
            });
        });
    }
    
    if($('#related_products_slider').length > 0){
        $('#related_products_slider').carouFredSel({
            auto: false,
            width: '100%',
            scroll: 1,
            prev    : { 
                 button : "#foo2_prev",
                 key        : "left"
             },
             next   : { 
                 button : "#foo2_next",
                 key        : "right"
             }
         });
    }
    
    
    if($('#carousel').length > 0){
        //productCarousel();
    }
    
    // Select
    // $(".select_init").select2();
    
    /*
    $(".select_init.discuss_select").select2({
        showSearchBox: false
    });
    */
    
    // Tabs
    $('#desc_tab_info a, #related_products a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })
    
    
    $('.change_submit').on('change', function(){
        $(this).closest('form').submit();
    })
    
    $('.click_submit').on('click', function(e){
        e.preventDefault();
        var $form = $(this).attr('data-form');
        if($form) $($form).submit();
        else $(this).closest('form').submit();
    })
    
    /****** CART ******/
    // Open /close cart tab
    $('.cart_button').on('click', function(e) {
        e.preventDefault();
        openCart();        
    });
    
    $('body').on('click', '.close_cart_link',function(e) {
        e.preventDefault();
        if(inColorbox($(this))){
            $.colorbox.close(); 
        } else {
            $('.cart_wrap').slideUp(400);
        }
    });
    
    // Remove item from cart
    $('body').on('click', '.remove_cart_item', function(e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            dataType: 'html',
            type: 'get',
            url: baseUrl + 'order/cart/remove_cart_item/' + $this.attr('data-uid'),
            success: function(data){
                cartInfo();
                if(inColorbox($this)) openEditCart();
                // else openCart();
                location.reload();
            },
        });
    });
    
    // Edit cart
    function openEditCart(msg)
    { 
        $.colorbox({
            href: baseUrl + 'order/cart/edit_cart',
            //data: $form,
            width: '60%', 
            height: '90%',
            trapFocus: false,
            onComplete: function(){
               $(".select_init", '#colorbox').select2();
               if(msg) showMsg($('.cart_message_box', '#colorbox'), msg);
            }
        });
    }
    
    // Open cart
    $('body').on('click', '.edit_cart',function(e) {
        e.preventDefault();
        $('.cart_wrap').slideUp(400);
        openEditCart();
    });
    
    // Clear cart
    $('body').on('click', '.clear_cart',function(e) {
        e.preventDefault();
        $.ajax({
            dataType: 'html',
            type: 'get',
            url: baseUrl + 'order/cart/clear_cart',
            success: function(data){
                cartInfo();
                openEditCart(data);
            },
        });
    });
    
    // Add artwork type
    $('body').on('change', '.ec_artwork_type', function(){
        var $this = $(this);
        var table = $this.parents('td').first().find('.ec_artwork_table');
        var totalItems = $this.val();
        var items = table.find('tr').length - 1;
        if(items > totalItems) {
           table.find('tr').slice(parseFloat(totalItems) + 1).detach();
        } else {
            $.ajax({
                dataType: 'html',
                type: 'post',
                data: {
                    uid: $this.next('.ec_uid').val(),
                    artwork_type: totalItems,
                    items: items,
                },
                url: baseUrl + 'order/cart/add_artwork_type',
                success: function(data){
                    $this.parents('td').first().find('.ec_artwork_table > tbody:last').append(data);
                    $(".select_init", '#colorbox').select2("destroy")
                    $(".select_init", '#colorbox').select2();
                },
            });
        }
    });
    
    // Update cart
    $('body').on('click', '#update_cart',function(e) {
        e.preventDefault();
        $.ajax({
            dataType: 'html',
            type: 'post',
            data: $('.edit_cart_form').serializeArray(),
            url: baseUrl + 'order/cart/update_cart',
            success: function(data){
                cartInfo();
                // openEditCart(data);
                location.reload();
            },
        });
    });
    
    // Delete artwork
    $('body').on('click', '#colorbox .remove_item',function(e) {
        var $this = $(this);
        e.preventDefault();
        $.ajax({
            dataType: 'html',
            type: 'get',
            url: $this.attr('href'),
            success: function(data){
                openEditCart(data);
            },
        });
    });
    
    $('body').on('click', '.ec_submit',function(e) {
        var $this = $(this);
        var $parent = $this.parents('tr').first();
        var $form = $this.closest('form');
        var uploadForm = $parent.find('form.edit_artwork_file_upload');
        e.preventDefault();
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: baseUrl + 'order/cart/init_upload',
            data: {
                token: $form.find("input[name='token']").val(),
            },
            success: function(data){
                if(data.errormessage){
                    alert(data.errormessage);
                } else {
                    $form.find("input[name='itemid']").val(data.itemid);
                    $form.find("input[name='URLs']").val(data.URLs);
                    uploadForm.attr('action', data.URLs);
                    uploadForm.find("input[name='bid']").val(data.itemid);
                    uploadArtworkFile($this);
                }
            },
        });
    });
    
    function uploadArtworkFile($this){
        var $parent = $this.parents('tr').first();
        var uploadForm = $parent.find('form.edit_artwork_file_upload');
        var options = { 
            type: 'post',
            dataType: 'xml',
            success: function(data){
                commitUpload($this);
            },
            error: function(){
                alert('Internal server error, please try again.');
            }
        }; 
        uploadForm.ajaxSubmit(options);
    }
    
    function commitUpload($this){
        var $parent = $this.parents('tr').first();
        var $form = $this.closest('form');
        var options = { 
            url: baseUrl + 'order/cart/commit_upload',
            type: 'post',
            dataType: 'json',
            data: {
                artwork_quantity_new: $parent.find("select[name='artwork_quantity_new']").val(),
            },
            //data: $this.parent('td').find(':input').fieldSerialize(),
            success: function(data){
                if(data.errormessage){
                    alert(data.errormessage);
                } else {
                    artworkUploaded($this, data.data.file_id, data.data.cart_uid);
                }
                
            },
        }; 
        $form.ajaxSubmit(options);
    }
    
    // Artwork upload
    function artworkUploaded($this, file_id, cart_uid){
        var $parent = $this.parents('tr').first();
        $.ajax({
            dataType: 'html',
            type: 'post',
            url: baseUrl + 'order/cart/artwork_uploaded',
            data: {
                file_id: file_id,
                cart_uid: cart_uid,
            },
            success: function(data){
                $parent.replaceWith(data);
                $(".select_init", '#colorbox').select2("destroy")
                $(".select_init", '#colorbox').select2();
            },
        });
    }
    
    // Checkout 
    $('.edit_address').on('click', function(e){
        e.preventDefault();
        editAddress($(this).attr('data-type'));
    });
    
    // Edit address colorbox
    function editAddress($type, msg)
    { 
        $.colorbox({
            href: baseUrl + 'order/checkout/edit_address/' + $type,
            //data: $form,
            width: '560px', 
//            height: '90%',
            trapFocus: false,
            onComplete: function(){
               $(".select_init", '#colorbox').select2();
               $('#edit_address_form').parsley();
               if(msg) showMsg($('.cart_message_box', '#colorbox'), msg);
            }
        });
    }
    
    // Update address
    $('body').on('click', '#edit_address_submit',function(e) {
        e.preventDefault();
        var $form = $(this).closest('form').serializeArray();
        var $type = $(this).attr('data-type');

        if($('#edit_address_form').parsley('validate')){
            $.colorbox({
                href: baseUrl + 'order/checkout/edit_address/' + $type,
                data: $form,
                width: '560px', 
//                height: '90%',
                trapFocus: false,
                onComplete: function(){
                   $(".select_init", '#colorbox').select2();

                   $.ajax({
                        type: 'POST',
                        url: '/order/checkout/refresh_address',
                        success: function(data) {
                            data = jQuery.parseJSON(data);

                            if(data.user.shipping_first_name && data.user.shipping_last_name){
                                $('#shipping_name').html(data.user.shipping_first_name +' '+ data.user.shipping_last_name);
                            } else {
                                $('#shipping_name').html(data.user.first_name +' '+ data.user.last_name);
                            }

                            if(data.user.shipping_address){
                                $('#shipping_address').html(data.user.shipping_address);
                            } else {
                                $('#shipping_address').html(data.user.address);
                            }

                            if(data.user.shipping_suburb){
                                $('#shipping_suburb').html(data.user.shipping_suburb);
                            } else {
                                $('#shipping_suburb').html(data.user.suburb);
                            }

                            if(data.user.shipping_state){
                                $('#shipping_state').html(data.user.shipping_state);
                            } else {
                                $('#shipping_state').html(data.user.state);
                            }

                            if(data.user.shipping_postcode){
                                $('#shipping_postcode').html(data.user.shipping_postcode);
                            } else {
                                $('#shipping_postcode').html(data.user.postcode);
                            }

                            $('#billing_name').html(data.user.first_name +' '+ data.user.last_name);
                            $('#billing_address').html(data.user.address);
                            $('#billing_suburb').html(data.user.suburb);
                            $('#billing_state').html(data.user.state);
                            $('#billing_postcode').html(data.user.postcode);
                        }
                    });
                }
            });
        }
    });
    
    $(document).ajaxStart(function(){
        cursorLoader();
        $("#cboxLoadingGraphic").show();
    });
    $(document).ajaxStop(function(){
        cursorDefault();
        $("#cboxLoadingGraphic").hide();
    });
    
    
});

function number_format (number, decimals, dec_point, thousands_sep) {
  // http://kevin.vanzonneveld.net
  // +   original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +     bugfix by: Michael White (http://getsprink.com)
  // +     bugfix by: Benjamin Lupton
  // +     bugfix by: Allan Jensen (http://www.winternet.no)
  // +    revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
  // +     bugfix by: Howard Yeend
  // +    revised by: Luke Smith (http://lucassmith.name)
  // +     bugfix by: Diogo Resende
  // +     bugfix by: Rival
  // +      input by: Kheang Hok Chin (http://www.distantia.ca/)
  // +   improved by: davook
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Jay Klehr
  // +   improved by: Brett Zamir (http://brett-zamir.me)
  // +      input by: Amir Habibi (http://www.residence-mixte.com/)
  // +     bugfix by: Brett Zamir (http://brett-zamir.me)
  // +   improved by: Theriault
  // +      input by: Amirouche
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: number_format(1234.56);
  // *     returns 1: '1,235'
  // *     example 2: number_format(1234.56, 2, ',', ' ');
  // *     returns 2: '1 234,56'
  // *     example 3: number_format(1234.5678, 2, '.', '');
  // *     returns 3: '1234.57'
  // *     example 4: number_format(67, 2, ',', '.');
  // *     returns 4: '67,00'
  // *     example 5: number_format(1000);
  // *     returns 5: '1,000'
  // *     example 6: number_format(67.311, 2);
  // *     returns 6: '67.31'
  // *     example 7: number_format(1000.55, 1);
  // *     returns 7: '1,000.6'
  // *     example 8: number_format(67000, 5, ',', '.');
  // *     returns 8: '67.000,00000'
  // *     example 9: number_format(0.9, 0);
  // *     returns 9: '1'
  // *    example 10: number_format('1.20', 2);
  // *    returns 10: '1.20'
  // *    example 11: number_format('1.20', 4);
  // *    returns 11: '1.2000'
  // *    example 12: number_format('1.2000', 3);
  // *    returns 12: '1.200'
  // *    example 13: number_format('1 000,50', 2, '.', ' ');
  // *    returns 13: '100 050.00'
  // Strip all characters but numerical ones.
  number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function (n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

function nf($num){
    return number_format ($num, 2, '.', ',');
}
