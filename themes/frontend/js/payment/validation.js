$(document).ready(function() {
	
	toggle_cc_details($('.payment_type:checked').val());
	
	$('.payment_type').change(function(){
		toggle_cc_details($(this).val());
	});
	
	function toggle_cc_details(type)
	{
		if (type == 'creditCard') {
			$('.div-credit-card').show();	
		} 
		else
		{
			$('.div-credit-card').hide();
		}
	}
});