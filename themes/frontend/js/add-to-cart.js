
$('.carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});

$(document).ready(function() {

    $('body').on('click', '.button1', function(){
         uploadForm('1', $(this));
    })

    $('body').on('change', 'select[name="artwork_types_required"]', function(){ 
        var totalItems = $(this).val();
        var items = $('.artwork_items').length + 1;
        if(items > totalItems) {
            $('.artwork_items').slice(totalItems).detach();
            maxQty();
        } else {
            addArtworkType(items, totalItems, $('.artworks_upload'));
        }
    })

    $('body').on('change', 'select.artwork_quantity', function(){
        calculateQty($(this));
    })

    $('body').on('click', 'input[name="upload_later"]', function(){
        if($(this).prop('checked')){
            $('.artworks_upload, .select_artwork_qty').hide(); 
        } else {
            $('.artworks_upload, .select_artwork_qty').show();
        }
    })

    $('body').on('click', '.delete_artwork', function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        deleteArtwork(href, $(this));
    })
   
    $('.add_to_cart').on('click', function(e){
        e.preventDefault(); 
        if(createOrder()){
            if($artwork_required){
                if(itemInCart($(this).closest('form').serialize())){
                    addToCart($(this).closest('form').serialize());
                } else {
                    uploadArtwork($(this).closest('form').serializeArray());
                }
            } else {
                addToCart($(this).closest('form').serialize());
            } 
        }
    })

    $this = $('.attribute_select');
    if($this.length > 0){
        $('h5.product-code').text('');
        $('.attributeid').val($this.attr('data-attributeid'));
        $('.price_wrapper .price_box').queue(function() {
            getProductData($this.closest('form').serialize());
            $(this).dequeue();
        });
    }
 
    $('.attribute_select').on('change', function(){
        $this = $(this);
        $('.attributeid').val($this.attr('data-attributeid'));
        $('.price_wrapper .price_box').queue(function() {
            getProductData($this.closest('form').serialize());
            $(this).dequeue();
        });
    });
    
    function calculateQty($this){
        var total = 0;    
        var count = $('select.artwork_quantity').length;
        var qty = $('.artwork_product_qty').text();     
        $('select.artwork_quantity').each(function(index){
            total += parseFloat($(this).val());
        })
        if(total > qty){
            alert('Total qty of artworks can not be greater than number of items ordered!');
            $('option:first', $this).attr('selected','selected');
            return false;
        }
        return true;
    }
    
    function maxQty(){
        var count = parseFloat($('select.artwork_quantity').length);   
        var qty = parseFloat($('.artwork_product_qty').text());     
        var options;
        var last;
        var maxValue = qty - count;
        $('select.artwork_quantity').each(function(index){
            last = parseFloat($('option:last-child', this).val()) + 1;
            for (var i=last; i<=qty; i++){
                $(this).append($('<option>', { 
                    value: i,
                    text : i 
                }));
            }
        })
        $('select.artwork_quantity').each(function(index){
            $('option:gt(' + maxValue + ')', this).remove();
        })
        
    }
    
    function addArtworkType($from, $to, $append){
        $.ajax({
            dataType: 'html',
            type: 'post',
            url: $artworkUrl,
            data: {
                from: $from,
                to: $to,
                quantity: $('.artwork_product_qty').text(),
                cart_uid: $('input[name="cuid"]').val(),
            },
            success: function(data){
                $append.append(data);
                maxQty();
                $(".select_init", '#colorbox').select2("destroy")
                $(".select_init", '#colorbox').select2();
            }
        })
    }
    
    function createOrder(){
    
        var $return = false;
        
        $.ajax({
            dataType: 'html',
            type: 'post',
            url: $createOrderUrl,
            success: function(data){
                $return = true;
            },
            async: false,
        })

        return $return;
    }

    function addUploadItem($from, $to, $append){
        $.ajax({
            dataType: 'html',
            type: 'post',
            cache: false,
            url: $uploadItemUrl,
            data: {
                from: 1,
                to: 1,
                cart_uid: $('input[name="cuid"]').val(),
            },
            success: function(data){
                $append.append(data);
            }
        })
    }
    
    function addToCart($form)
    {
         $.ajax({
            dataType: 'html',
            type: 'post',
            url: $addToCartUrl,
            data: $form,
            dataType: 'json',
            success: function(data){
                cartInfo();
                // openCart();
                if(data.status == 'error')
                {
                    $.colorbox({
                        html: '<div class="popup"><div class="legend"><h2>Oops!</h2></div><div class="container_12"><div class="grid_12">'+data.message+'</div></div><div class="clear"></div></div>'
                    });
                }
                else
                    window.location.replace(baseUrl+"order/cart/edit_cart");
            }
        })
    }
    
    function itemInCart($form)
    {
         var $return = true;
         $.ajax({
            dataType: 'html',
            type: 'post',
            url: $itemInCartUrl,
            data: $form,
            success: function(data){
                if(data == ''){
                    $return = false;
                }
            },
            async: false,
        });
        return $return;
    }
    
    function uploadArtwork($form)
    {
        $.colorbox({
            href: $uploadArtworkUrl,
            data: $form,
            width: '60%', 
            height: '80%',
            onComplete: function(){
                $(".select_init", '#colorbox').select2("destroy")
                $(".select_init", '#colorbox').select2();
                cartInfo();
            },
            onClosed: function (){
                cartInfo();
            }
        });
        
    }
    
    function getProductData($form){
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: $getProductDataUrl,
            data: $form,
            beforeSend: function(){
                showLoader($('.price_wrapper .price_box'));
            },
            error: function(){
                showError($('.price_wrapper .price_box'));
                showError($('.qty_cart_but'));
            },
            success: function(data){
                $('.price_wrapper .price_box, h5.product-code').fadeOut(function(){
                    $('.price_wrapper .price_box, h5.product-code').html(''); 
                    // Populate select lists
                    if(!$.isEmptyObject(data.sx) && !$.isEmptyObject(data.select)){ 
                        $.each(data.sx, function(k,v){ 
                            var $list = $('select[name="select['+ v +']"]');
                            // $list.select2('destroy').html('').select2();
                            $list.html('');
                            $.each(data.select[v], function(kk, vv) { 
                                var $selected = ''; 
                                if(data.current_attributes[k].option.id == kk)
                                    $list.append($('<option selected="selected"></option>').attr('value', kk).text(vv));
                                else
                                    $list.append($('<option></option>').attr('value', kk).text(vv));
                            }); 
                            $list.select2();
                        });
                    }
                    
                    // Set product attribute id
                    $('.product_attribute_id').val(data.current_attributes[0].product_attribute.id);
                    
                    $('.price_wrapper').html('');           
                    $('#template .price_box').clone().appendTo('.price_wrapper'); 
                    if(data.code != '') $('h5.product-code').text(data.code); 
                    else $('h5.product-code').text(''); 

                    if(data.price > 0){ 
                        $('.qty_cart_but').show('fast');                   
                        $('.price_wrapper .product_price').text('$' + (parseFloat(data.price).toFixed(2).replace(/\B(?=(\d{3})+\b)/g, ","))); // adds comma to number replace(/\B(?=(\d{3})+\b)/g, ",")
                        if(data.price_type == 'sale_price' && data.retail_price > 0){
                            $('.price_wrapper .old_price').text('$' + parseFloat(data.retail_price).toFixed(2).replace(/\B(?=(\d{3})+\b)/g, ",")); 
                        }
                    } else {
                        $('.price_wrapper .product_price').text('Please Call');
                        $('.qty_cart_but').hide('fast');
                    }
                });
                
                $('.price_wrapper, h5.product-code').fadeIn();
            
                // Fade out image wrap and add image to it
                $('#carousel, #thumbs').fadeOut(function(){
                   
                    // Set images
                    if(data.images != null){
                        var $thumbs = '';
                        var $images = '';
                        $.each(data.images, function(k,v){
                            $thumbs += '<a href="#img' + k +  '">';                      
                            $thumbs += '<img src="' + $images_base_url + 'thumbs/' + v.image + '" width="86" height="86">';
                            $thumbs += '</a>';       
                            
                            $images += '<span id="img' + k + '">'; 
                            // $images += '<img src="' + $images_base_url + v.image +'" width="460" height="552">';
                            $images += '<img src="' + $images_base_url + 'large/' + v.image +'">';
                            $images += '</span>';
                        });
                        
                        $('#carousel').trigger("destroy").html($images);
                        $("#thumbs").trigger("destroy").html($thumbs);
                        
                        productCarousel();
                    }
                });
                $('#carousel, #thumbs').fadeIn();
            }
        }); 
    }
});

function uploadForm(fileCount, $this) {
    var element = $this.closest('.addfile');
    $this.hide('fast');

    var options = {
        dataType: 'json',
        beforeSend: function(){
            element.find(".spinner").html($loading_img);
        },
        success: function(data) {
            if(data.errormessage){
                alert(data.errormessage);
            } else {
                sendData(data.itemid, data.URLs[0], element);
            }
        },
        error: function(){
            alert('Internal server error, please try again.');
        }
    }

    element.find('.uploadf').ajaxSubmit(options);
}

function sendData(itemid, urls, element){

        element.find('.form1').first().attr('action', urls);
        element.find('.bid1').first().val(itemid);

        var options = {
                //forceSync: true,
                dataType: 'xml',
                beforeSend: function(){
                    element.find('.prog1').html('In progress...');
                },
                error: function(){
                    alert('Internal server error, please try again.');
                },
                success: function(){ 
                    element.find('.prog1').html('Finishing...');
                    sendRequest('commitupload',
                        $sendDataUrl, 
                        element, 
                        element.find('.token').val(), 
                        element.find('.parent').val(), 
                        element.find('.bid1').val(), 
                        element.find('.product_id').val(), 
                        element.find('.type').val(), 
                        element.find('.cart_uid').val(), 
                        element.closest('.artwork_items').find('select.artwork_quantity').first().val()
                    )
                }
        }           
        element.find('.form1').ajaxSubmit(options);
}

function sendRequest() {
    method = arguments[0];
    file = arguments[1];
    element = arguments[2];
    params = new Array();
    for( var i = 3; i < arguments.length; i++ ) {
        params.push(arguments[i]);
    }

    var toSend = "method=" + method + "&var0=";
    for ( var i = 0; i < params.length; i++) {
        if (i == (params.length - 1)) {
            toSend = toSend + params[i];
        } else {
            var k = i + 1;
            toSend = toSend + params[i] + "&var" + k + "=";
        }
    }

    var parameterString = encodeURI(toSend);

    $.ajax({
        dataType: 'json',
        type: 'post',
        cache: false,
        url: file,
        data: parameterString,
        success: function(data){
            uploadDone(data, element);
        },
        error: function(){
            var obj = {};
            obj.errorcode = '';
            obj.errormessage =  'Internal server error, please try again1.';
            uploadDone(obj, element);
        },
    })
}

function uploadDone(data, element){
    element.find(".spinner").html('');
    if(data.errormessage){
        element.find('.prog1').html('Internal server error, please try again2.');
        element.find('.button1').show('fast');
    } else {
        element.find('.prog1').html('Upload done.');
        element.find('.form1').hide('fast');
        artworkUploaded(data, element);
    }
}

function artworkUploaded(data, element){
    $.ajax({
        dataType: 'html',
        type: 'post',
        cache: false,
        url: $artworkUploaded,
        data: data,
        success: function(response){
             element.find('.prog1').append(response);
        }
    })
}

function resetFormElement(e){
    e.wrap('<form>').closest('form').get(0).reset();
    e.unwrap();
}

function deleteArtwork(href, $this){
    $.ajax({
        dataType: 'json',
        type: 'post',
        cache: false,
        url: href,
        beforeSend: function(){
                $this.closest('.prog1').hide('fast');
                $this.closest('.addfile').find('.spinner').first().html($loading_img);
            },
        success: function(data){
            var addFile = $this.closest('.addfile');
            addFile.find('.spinner').first().html('');

            if(data.errormessage){
                alert('Internal server error, please try again.');
            } else {
                alert('Artwork deleted.');
                addFile.find('.prog1').show('fast').html('');
                addFile.find('.form1').show('fast');
                addFile.find('.form1').attr('action', '');
                addFile.find('.button1').show('fast');
                addFile.find('.form1').find('.bid1').val('');
                resetFormElement(addFile.find('.form1').find('.bid1'));
            }
        },
        error: function(){
            $this.closest('.prog1').show('fast');
            $this.closest('.addfile').find('.spinner').first().html('');
            alert('Internal server error, please try again.');
        },
    })
}