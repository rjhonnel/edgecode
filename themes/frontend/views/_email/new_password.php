<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=320, target-densitydpi=device-dpi">
        <style type="text/css">
            body { width: 100% !important; font-size: 12px; font-weight: normal;}
            body { background-color: #fff; margin: 0; padding: 0; }
            img { outline: none; text-decoration: none; display: block;}
            body, td { font-family: Arial, Helvetica, sans-serif; color: #000; text-decoration: none; font-size: 12px; font-weight: normal;}
            a{
                color: #000; font-weight:bold; text-decoration:none;
            }
        </style>
    </head>
    <body>
        <?php $settings = \Config::load('autoresponder.db'); ?>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td align="center" bgcolor="#fff">
                        <table style="margin:0 10px;" width="640" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr><td  width="640" height="20"></td></tr>
                                <?php $hold_logo_url = (isset($settings['logo_url']) ? $settings['logo_url'] : false); ?>
                                <?php if($hold_logo_url): ?>
                                    <tr>
                                        <td width="640" align="center" bgcolor="#fff">
                                            <table width="640" cellpadding="0" cellspacing="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td width="320">
                                                            <a href="<?php echo \Uri::create('/') ?>">
                                                                <img src="<?php echo \Uri::create('media/images/' . $hold_logo_url); ?>">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr><td width="320" height="15"></td><td width="320"></td></tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                <?php endif; ?>

                                <tr><td width="640" height="30" bgcolor="#ffffff"></td></tr>
                                <tr>
                                    <td width="640" bgcolor="#ffffff">
                                        <table width="640" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td width="30"></td>
                                                    <td width="580">
                                                        <table width="580" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="580">
                                                                        <h1 style="font-weight: normal; margin: 0; padding: 0 0 10px 0; border-bottom: 1px solid #dddddd;">
                                                                            New Password
                                                                        </h1>
                                                                        <p align="left">
                                                                            
                                                                           <table cellpadding="10" cellspacing="0" border="0" width="100%" align="left">

                                                                                <tr>
                                                                                    <td style="padding: 0 0 20px 15px;">
                                                                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">Hello <?php echo $content['content']['customer_identity']; ?>,</p>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td style="padding: 0 0 20px 15px;">
                                                                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
                                                                                            You have received this email because we changed your password. Thanks! 
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td style="padding: 0 0 20px 15px;">
                                                                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
                                                                                            Your new password: <br><strong><?php echo $content['content']['new_password']; ?></strong>
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td style="padding: 0 0 20px 15px;">
                                                                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
                                                                                            Thanks,
                                                                                        </p>
                                                                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
                                                                                            <?php echo $settings['company_name']; ?> Team
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td style="padding: 0 0 20px 15px;">
                                                                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; font-style: italic; color: #d3d3d3;">
                                                                                            Please do not reply to this email as it won't reach us. You have received this email as part of your <?php echo $settings['company_name']; ?> account. Please forward any query you have to <a href="<?php echo $settings['contact_us_email_address'] ?>"><?php echo $settings['contact_us_email_address'] ?></a>
                                                                                        </p>
                                                                                        <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868; font-weight: bold;">
                                                                                            
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                                
                                                                            </table>
                                                                            
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr><td width="580" height="10"></td></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="30"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td width="640" height="15" style="border-bottom: 1px solid #dddddd;"></td></tr>
                                <tr><td  width="640" height="15" bgcolor="#ffffff"></td></tr>
                                <tr><td  width="640" height="15" bgcolor="#fff"></td></tr>

                                <tr>
                                    <td width="640">
                                        <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#fff">
                                            <tbody>
                                                <tr>
                                                    <td width="320">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <?php if($hold_logo_url): ?>
                                                                        <td width="70">
                                                                            <a href="<?php echo \Uri::create('/') ?>">
                                                                                <img src="<?php echo \Uri::create('media/images/' . $hold_logo_url); ?>" >
                                                                            </a>
                                                                        </td>
                                                                    <?php endif; ?>
                                                                    <td>
                                                                        <?php echo $settings['address']; ?><br>
                                                                        <a href="<?php echo \Uri::create('/') ?>"><?php echo $settings['website']; ?></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
