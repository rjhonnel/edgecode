<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=320, target-densitydpi=device-dpi">
        <style type="text/css">
            body { width: 100% !important; font-size: 12px; font-weight: normal;}
            body { background-color: #333436; margin: 0; padding: 0; }
            img { outline: none; text-decoration: none; display: block;}
            body, td { font-family: Arial, Helvetica, sans-serif; color: #585858; text-decoration: none; font-size: 12px; font-weight: normal;}
            a{
                color: #585858; font-weight:bold; text-decoration:none;
            }
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td align="center" bgcolor="#333436">
                        <table style="margin:0 10px;" width="640" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr><td  width="640" height="20"></td></tr>
                                <tr>
                                    <td width="640" align="center" bgcolor="#333436">
                                        <table width="640" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td width="320">
                                                        <?php echo $theme->asset->img('logo.png'); ?>
                                                    </td>
                                                    <td width="320">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style="color: #fff300; font-weight: bold; font-size: 22px;">1800 990 989</td>
                                                                                    <td width="45">
                                                                                        <a href="https://www.facebook.com/EvanEvansAu"><?php echo $theme->asset->img('facebook.jpg', array('width' => 36, 'height' => 36)); ?></a>
                                                                                    </td>
                                                                                    <td width="45">
                                                                                        <a href="http://instagram.com/evanevans_au"><?php echo $theme->asset->img('instagram.jpg', array('width' => 36, 'height' => 36)); ?></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="40"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" valign="bottom">
                                                                       
                                                                        <table width="100%" cellpadding="10" cellspacing="0" border="0">
                                                                            <tr>
                                                                                <td>
                                                                                    <a href="<?php echo \Uri::create('user/account/dashboard') ?>" style="color: #fff; text-transform: uppercase; font-size: 14px;">My Account</a>
                                                                                </td>
                                                                                <td>
                                                                                    <a href="<?php echo \Uri::create('/') ?>" style="color: #fff; text-transform: uppercase; font-size: 14px;">Website</a>
                                                                                </td>
                                                                                <td>
                                                                                    <a href="<?php echo \Uri::create('contact-us') ?>" style="color: #fff; text-transform: uppercase; font-size: 14px;">Contact Us</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr><td width="320" height="15"></td><td width="320"></td></tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                                <tr><td width="640" height="30" bgcolor="#ffffff"></td></tr>
                                <tr>
                                    <td width="640" bgcolor="#ffffff">
                                        <table width="640" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td width="30"></td>
                                                    <td width="580">
                                                        <table width="580" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="580">
                                                                        <h1 style="font-weight: normal; margin: 0; padding: 0 0 10px 0; border-bottom: 1px solid #dddddd;">
                                                                            Discuss Brief
                                                                        </h1>
                                                                        <p align="left">
                                                                            
                                                                            <?php if(isset($content) && !empty($content)): ?>
                                                                            
                                                                                <?php 
                                                                                if(isset($content['content']) && !empty($content['content'])):
                                                                                $data = $content['content'];   
                                                                                ?>

                                                                                    Hi <?php echo $data['name']; ?>,<br><br>

                                                                                    Thanks for contacting us! A member of our team will get back to you shortly. <br /><br />
                                                                                    If your enquiry is urgent, please call us on 1800 990 989. <br /><br />
                                                                                    The Evan Evans team <br /><br />

                                                                                    <table cellpadding="10" cellspacing="0" border="0" width="100%" align="left">
                                                                                        <tr>
                                                                                            <td><b>Date:</b></td>
                                                                                            <td><?php echo date('d-m-Y H:i', time()); ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Name:</b></td>
                                                                                            <td><?php echo $data['name']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Email:</b></td>
                                                                                            <td><?php echo $data['email']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>User IP address:</b></td>
                                                                                            <td><?php echo \Input::ip(); ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Product:</b></td>
                                                                                            <td><?php if(isset($data['product']['title'])) echo $data['product']['title']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Company:</b></td>
                                                                                            <td><?php echo $data['company']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Position:</b></td>
                                                                                            <td><?php echo $data['position']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Phone:</b></td>
                                                                                            <td><?php echo $data['phone']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>How did you hear about us:</b></td>
                                                                                            <td><?php echo $data['hear_about_us']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Preferred method of contact:</b></td>
                                                                                            <td><?php echo $data['method_of_contact']; ?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td><b>Comments:</b></td>
                                                                                            <td><?php echo $data['comments']; ?></td>
                                                                                        </tr>

                                                                                    </table>
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                            
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                                <tr><td width="580" height="10"></td></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="30"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td  width="640" height="15" bgcolor="#ffffff"></td></tr>
                                <tr><td  width="640" height="15" bgcolor="#333436"></td></tr>

                                <tr>
                                    <td width="640">
                                        <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#333436">
                                            <tbody>
                                                <tr>
                                                    <td width="320">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <td width="70">
                                                                        <?php echo $theme->asset->img('logo.png', array('width' => 200)); ?>
                                                                    </td>
                                                                    <td style="color: #c2c3c5;">
                                                                        673 Spencer Street <br>
                                                                        West Melbourne, VIC 3003<br>
                                                                        <a href="<?php echo \Uri::create('/') ?>" style="color: #c2c3c5;">www.evanevans.com.au</a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="320">
                                                        <table align="right" width="90" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="80" align="right" colspan="2" style="color: #fff; text-transform: uppercase; padding-bottom: 5px;">
                                                                        Get Social:
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" width="40">
                                                                        <a href="https://www.facebook.com/EvanEvansAu"><?php echo $theme->asset->img('facebook.jpg', array('width' => 36, 'height' => 36)); ?></a>
                                                                    </td>
                                                                    <td align="right" width="40">
                                                                        <a href="http://instagram.com/evanevans_au"><?php echo $theme->asset->img('instagram.jpg', array('width' => 36, 'height' => 36)); ?></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td width="640" height="15" style="border-bottom: 1px solid #666769;"></td></tr>
                                <tr><td width="640" height="15"></td></tr>
                                <tr>
                                    <td width="640" style="color: #c2c3c5; font-size: 13px;" align="center">
                                        Please do not reply to this email as it won't reach us. You have received this
                                        email as part of your<br> Evan Evans account.
                                    </td>
                                </tr>
                                <tr><td width="640" height="15"></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
