<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User Contact</title>

</head>
<body style="margin:0 auto; width: 620px;">
	<table style="margin-top: 20px;" width="620" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td style="text-align: center; padding:0 0 20px 0;">
				<a href="<?php echo \Uri::front_create(); ?>">
					<?php echo \Theme::instance()->asset->img('logo.png');?>
				</a>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 0 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">Hello <?php echo $email_data['name']; ?>, you have just sent contact request. Request details are displayed bellow. </p>
			</td>
		</tr>
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;"> We will try to answer you as soon as possible. Thanks. </p>
			</td>
		</tr>
		
		<!-- Order details: -->
		<tr>
			<td width="100%" style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					
                    <table width="100%">
                        <tr>
                            <td width="100%" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td colspan="2">
                                            <span style="color: #474747; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-transform: uppercase;">Contact Details:</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Name:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['name']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Email:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['email']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Phone:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['phone']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;" valign="top">Message:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['message']; ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
				</p>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					Kind Regards,
				</p>
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					<?php echo $email_data['site_title']; ?> Team
				</p>
			</td>
		</tr>
        
        <tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; font-style: italic; color: #d3d3d3;">
                    Please do not reply to this email as it won't reach us. You have received this email as part of your <?php echo $email_data['site_title']; ?> account. Please forward any query you have to <a href="mailto:support@myshortlist.net">support@myshortlist.net</a>
				</p>
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868; font-weight: bold;">
					
				</p>
			</td>
		</tr>
        <tr>
			<td style="text-align: left; padding:0 0 20px 15px;">
				<a href="http://www.thekidscancerproject.org.au/">
					<?php echo \Theme::instance()->asset->img('the-kids-cancer-project-logo-lg.jpg'); ?>
				</a>
			</td>
		</tr>
	</table>
</body>
</html>