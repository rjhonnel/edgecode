<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Job Contact</title>

</head>
<body style="margin:0 auto; width: 620px;">
	<table style="margin-top: 20px;" width="620" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td style="text-align: center; padding:0 0 20px 0;">
				<a href="<?php echo \Uri::front_create(); ?>">
					<?php echo \Theme::instance()->asset->img('logo.png');?>
				</a>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">Hello,</p>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					You have just received a contact request about job position. Message details are bellow:
				</p>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
                    <strong>Message:</strong>
				</p>
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
                    <?php echo \Theme::instance()->asset->img('quote.png', array('width' => 15, 'height' => 15)) ?>
					<i><?php echo nl2br($email_data['message']); ?></i>
                    <?php echo \Theme::instance()->asset->img('quote.png', array('width' => 15, 'height' => 15)) ?>
				</p>
			</td>
		</tr>
        
        <tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
                    <strong>Job Details:</strong>
				</p>
                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
                    <table style="text-align: center; border: 1px solid #d9d9d9; border-collapse: collapse; margin-bottom: 20px;" width="100%" cellpadding="0" cellspacing="0" border="0">
                        <tr style="background-color: #ebebeb; height: 32px;">
                            <td style="border: 1px solid #d9d9d9; color: #474747; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-transform: uppercase;">Reference number</td>
                            <td style="border: 1px solid #d9d9d9; color: #474747; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-transform: uppercase;">Job title</td>
                            <td style="border: 1px solid #d9d9d9; color: #474747; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-transform: uppercase;">Activation Date</td>
                        </tr>
                        <tr style="border: 1px solid #d9d9d9;">
                            <td style="border: 1px solid #d9d9d9; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;">
                                <b><?php echo $email_data['job']->reference; ?></b>
                            </td>
                            <td style="border: 1px solid #d9d9d9; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; ">
                                <?php echo $email_data['job']->title; ?>
                            </td>
                            <td style="border: 1px solid #d9d9d9; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;">
                                <?php echo date('d M Y', $email_data['job']->active_from ?: $email_data['job']->created_at); ?>
                            </td>
                        </tr>
                        <tr style="border: 1px solid #d9d9d9;">
                            <td style="border: 1px solid #d9d9d9; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;">
                                <b>Job Board Link:</b>
                            </td>
                            <td colspan="2" style="border: 1px solid #d9d9d9; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; ">
                                <?php echo \Html::anchor(\Uri::front_create('job/' . $email_data['job']->seo->slug), null, array('style' => 'color: #229ECA;')); ?>
                            </td>
                        </tr>
                        <tr style="border: 1px solid #d9d9d9;">
                            <td style="border: 1px solid #d9d9d9; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;">
                                <b>Admin Link:</b>
                            </td>
                            <td colspan="2" style="border: 1px solid #d9d9d9; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; ">
                                <?php echo \Html::anchor(\Uri::create('admin/job/update/' . $email_data['job']->id), null, array('style' => 'color: #229ECA;')); ?>
                            </td>
                        </tr>
                    </table>
                </p>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					Kind Regards,
				</p>
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					<?php echo $email_data['site_title']; ?> Team
				</p>
			</td>
		</tr>
        
        <tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; font-style: italic; color: #d3d3d3;">
                    Please do not reply to this email as it won't reach us. You have received this email as part of your <?php echo $email_data['site_title']; ?> account. Please forward any query you have to <a href="mailto:support@myshortlist.net">support@myshortlist.net</a>
				</p>
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868; font-weight: bold;">
					
				</p>
			</td>
		</tr>
        <tr>
			<td style="text-align: left; padding:0 0 20px 15px;">
				<a href="http://www.thekidscancerproject.org.au/">
					<?php echo \Theme::instance()->asset->img('the-kids-cancer-project-logo-lg.jpg'); ?>
				</a>
			</td>
		</tr>
	</table>
</body>
</html>