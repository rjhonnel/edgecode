<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Order Complete</title>

</head>
<body style="margin:0 auto; width: 620px;">
	<table style="margin-top: 20px;" width="620" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td style="text-align: center; padding:0 0 20px 0;">
				<a href="<?php echo \Uri::front_create(); ?>">
					<?php echo \Theme::instance()->asset->img('logo.png');?>
				</a>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">Hello,</p>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					User <?php echo ucwords($email_data['order']['first_name'] . ' ' . $email_data['order']['last_name']); ?> just posted new job with credits.
				</p>
			</td>
		</tr>
		
		<!-- Order details: -->
		<tr>
			<td width="100%" style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					
                    <table width="100%">
                        <tr>
                            <td width="50%" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td colspan="2">
                                            <span style="color: #474747; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-transform: uppercase;">Order details:</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Job Title:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['job']['title'];?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Job link:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><a href="<?php echo \Uri::front_create('job/' . $email_data['job']->seo->slug);?>"><?php echo \Uri::front_create('job/' . $email_data['job']->seo->slug);?></a></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Order Number:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['order']['id'];?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Order Date:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo date('d M Y',$email_data['order']['created_at']);?></td>
                                    </tr>
                                </table>
                            </td>
                            <td width="50%" valign="top">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td colspan="2">
                                            <span style="color: #474747; font-family: Arial, Helvetica, sans-serif; font-size: 12px; text-transform: uppercase;">Sold to:</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Name:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo ucwords($email_data['order']['first_name'] . ' ' . $email_data['order']['last_name']); ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Company:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['order']['company']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Address:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['order']['address']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">State:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['order']['postcode'] . ' ' .$email_data['order']['suburb']; ?></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px; text-align: left;">Country:</td>
                                        <td style="color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; padding: 8px;"><?php echo $email_data['order']['country']; ?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
				</p>
			</td>
		</tr>
		
		<tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					Kind Regards,
				</p>
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868;">
					<?php echo $email_data['site_title']; ?> Team
				</p>
			</td>
		</tr>
        
        <tr>
			<td style="padding: 0 0 20px 15px;">
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; font-style: italic; color: #d3d3d3;">
                    Please do not reply to this email as it won't reach us. You have received this email as part of your <?php echo $email_data['site_title']; ?> account. Please forward any query you have to <a href="mailto:support@myshortlist.net">support@myshortlist.net</a>
				</p>
				<p style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #686868; font-weight: bold;">
					
				</p>
			</td>
		</tr>
        <tr>
			<td style="text-align: left; padding:0 0 20px 15px;">
				<a href="http://www.thekidscancerproject.org.au/">
					<?php echo \Theme::instance()->asset->img('the-kids-cancer-project-logo-lg.jpg'); ?>
				</a>
			</td>
		</tr>
	</table>
</body>
</html>