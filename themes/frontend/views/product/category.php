<?php
$group_id = \Sentry::user()->group_id();
if($items)
  {
    for($x=0;$x<sizeof($items);$x++){
        if($items[$x]->user_group){
            
           if(!in_array($group_id,(array)json_decode($items[$x]->user_group))){
               unset($items[$x]);
           }
         
        } 
    }
  }  
?>
<div class="section-grey">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo \Uri::front_create('/'); ?>">Home</a></li>
      <li>></li>
      <li><a href="<?php echo \Uri::front_create('/products'); ?>">Products</a></li>
    </ul>
  </div>
</div>

<?php if(!empty($category->images)):?>
  <div class="slider-title">
    <div class="slides" role="listbox">
      <?php foreach($category->images as $image){ ?>
      <div class="item" style="background-image: url('<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $image->image); ?>')">
      <div class="slide-content">
          <h1 class="slide-title"><?php echo $category->title; ?></h1>
          <div class="slide-copy">
            <p><?php echo $category->images[0]->alt_text; ?></p>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
<?php endif; ?>

<div class="app-content">
  <div class="container">
    <h2 class="copy-title"><?php echo !empty($category->images) ? $category->description_intro : $category->title; ?></h2>
    <div class="category-products">
      <div class="row">
        <?php if($items): ?>
          <?php foreach($items as $item ): ?>
            <div class="col-md-4 col-sm-6">
              <div class="product-item">
                <a href="<?php echo \Uri::create($item->seo->slug) ?>">
                  <div class="product-img">
                    <?php
                      if($cover_image = \Product\Model_Category::get_cover_image($item)):
                        echo \Html::img(\Helper::amazonFileURL( \Config::get('media_base_url') . $cover_image->image, array('alt' => $cover_image->alt_text))); 
                      else:
                    ?>
                      <div class="no-image">
                        <i class="icon icon-image2"></i>
                        <h4>No image</h4>
                      </div>
                    <?php endif; ?>
                  </div>
                  <span class="product-display">
                    <span class="product-content">
                      <h3 class="product-title" title="<?php echo $item->title; ?>"><?php echo $item->title; ?></h3>
                    </span>
                  </span>
                </a>
              </div>
            </div>
          <?php endforeach; ?>
            <div class="pagination_holder bottom_pagination">
              <?php echo $theme->view('views/_partials/pagination', array('pagination' => $pagination), false); ?>
            </div>
        <?php else: ?>
          <div class="col-md-4 col-sm-6">
            No Available Products.
          </div>
        <?php endif; ?>
      </div>
    </div>
    <div class="copy">

        <?php if($category->infotabs): ?>
          <div>
            <ul class="nav nav-tabs" role="tablist">
              <?php $counter = 0; foreach ($category->infotabs as $key => $infotab): ?>
                <li class="<?php echo $counter==0 ? 'active': ''; ?>" role="presentation">
                  <a href="#infotab<?php echo $infotab->id ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $infotab->title ?></a>
                </li>
              <?php $counter++; endforeach; ?>
            </ul>

            <div class="tab-content">
              <?php $counter = 0; foreach ($category->infotabs as $key => $infotab): ?>
                <div role="tabpanel" class="tab-pane fade in <?php echo $counter==0 ? 'active': ''; ?>" id="infotab<?php echo $infotab->id ?>">
                  <?php
                    if($infotab->type == 'table')
                    {
                      echo '<textarea class="info_table hide">'.$infotab->description.'</textarea><div class="info_table_preview"></div>';

                      if($infotab->images)
                      {
                        foreach ($infotab->images as $key => $image)
                        {
                          echo '<a class="btn btn-green btn-block" href="'.\Helper::amazonFileURL('media/images/' . $image->image).'" title="'.($image->title ?: $image->image).'" target="_blank">'.($image->title ?: $image->image).'</a>'; 
                        }
                      }
                    }
                    else if($infotab->type == 'hotspot')
                    {
                      if(isset($infotab->image)):
                        ?>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="hotspot_cover_holder" style="position: relative; display: inline-block;" >
                            <?php echo \Html::img(\Helper::amazonFileURL('media/images/' . $infotab->image), array('class' => 'hotspot_cover', 'style' => 'border: solid 1px #D0D0D0;')); ?>

                            <?php if(!empty($infotab->images)): ?>
                              <?php foreach($infotab->images as $image): ?>

                                <?php if(!is_null($image->position_x) && !is_null($image->position_y)): ?>

                                  <?php 
                                  $image_name = isset($hotspot) && $image->id == $hotspot->id ? 'hotspot_red.png' : 'hotspot_blue.png';
                                  ?>

                                  <a class="hotspot-show-data" rel="<?php echo $image->id; ?>" style="position: absolute; top: <?php echo $image->position_y; ?>px; left: <?php echo $image->position_x; ?>px;" href="#">
                                    <?php echo \Theme::instance()->asset->img('hotspot/'.$image_name); ?>
                                  </a>
                                <?php endif; ?>

                              <?php endforeach; ?>
                            <?php endif; ?>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="hotspot-view-data"></div>
                        </div>
                      </div>
                      <?php
                      endif;
                    }
                    else
                    {
                      echo $infotab->description;
                      if($infotab->files)
                      {
                        foreach ($infotab->files as $key => $file)
                        {
                          echo '<a class="btn btn-green btn-block" href="'.\Helper::amazonFileURL('media/files/' . $file->file).'" title="'.($file->title ?: $file->file).'" download>'.($file->title ?: $file->file).'</a>'; 
                        }
                      }
                    }
                    ?>
                </div>
              <?php $counter++; endforeach; ?>
            </div>

          </div>
        <?php endif; ?>
    </div>
  </div>
</div>