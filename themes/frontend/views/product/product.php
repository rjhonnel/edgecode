<?php
  $product_data = $product->data;
  $stock_quantity = isset($product_data['current_attributes'][0]->product_attribute->stock_quantity)?$product_data['current_attributes'][0]->product_attribute->stock_quantity:(isset($product->attributes[0]->stock_quantity)?$product->attributes[0]->stock_quantity:0);
 
?>
<div class="section-grey">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo \Uri::front_create('/'); ?>">Home</a></li>
      <li>></li>
      <li><a href="<?php echo \Uri::front_create('/products'); ?>">Products</a></li>
      <li>></li>
      <?php if($category_parents): ?>
        <?php foreach($category_parents as $parent): ?>
          <li><a href="<?php echo \Uri::create($parent->seo->slug); ?>"><?php echo $parent->title; ?></a></li>
          <li>></li>
        <?php endforeach; ?>
      <?php endif; ?>
      <li class="active"><?php echo $product->title; ?></li>
    </ul>
  </div>
</div>
<div class="app-content">
 <div class="container">
    <div class="row">
      <div class="col-md-5">
        <div id="slider" class="slider-single">
          <div class="slides">
            <?php if(empty($product->images)): ?>
              <div class="<?php echo $stock_quantity <= 0 ? 'no-stock': ''; ?> ">
                <div class="product-img">
                  <div class="no-image">
                    <i class="icon icon-image2"></i>
                    <h4>No image</h4>
                  </div>
                </div>
                <?php if($stock_quantity <= 0): ?>
                  <div class="ribbon">
                    <h2 class="ribbon-title">Out of Stock</h2>
                  </div>
                <?php endif; ?>
              </div>
            <?php endif; ?>
            <?php foreach($product->images as $image): ?>
              <div class="item <?php echo $stock_quantity <= 0 ? 'no-stock': ''; ?>"><img src="<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $image->image); ?>" alt="<?php echo $image->alt_text; ?>">
              <?php if($stock_quantity <= 0): ?>
                <div class="ribbon">
                  <h2 class="ribbon-title">Out of Stock</h2>
                </div>
              <?php endif; ?>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="slider-single-nav">
          <div class="slides">
            <?php foreach($product->images as $image): ?>
              <div class="item"><img src="<?php echo \Helper::amazonFileURL( \Config::get('media_base_url'). 'medium/' . $image->image); ?>" alt="<?php echo $image->alt_text; ?>"></div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
      
      <div class="col-md-7">
        <div class="page-single">
          <div class="product-heading">
          <h2 class="product-title text-left"><?php echo $product->title; ?></h2>
          <div class="price_wrapper">
            <div class="price_box">
              <span class="product-price text-right product_price">$<?php echo $product_data['price']?number_format($product_data['price'], 2):number_format($product_data['retail_price'], 2); ?></span>
            </div>
          </div>
          <h5 class="product-code"><?php echo $product->code; ?></h5>
        </div>
        <div class="copy">
          <?php echo $product->description_full; ?>
        </div>

          <?php echo \Form::open(array('action' => \Uri::current(), 'method' => 'post', 'id' => 'cart_form')); ?>
            <?php if($product->product_type == 'pack'): ?>
              <?php if($packs = $pack_list): ?>
                <?php foreach ($packs as $key => $pack): ?>
                  <div class="selection" style="margin-bottom: 20px;border: 1px solid #e8e8e9;padding: 5px;">
                    <div class="row" style="margin-bottom: 10px;">
                      <?php if(isset($pack['image'])): ?>
                        <?php if($pack['image']): ?>
                          <div class="col-md-2">
                            <a href="<?php echo \Helper::amazonFileURL('media/images/'. $pack['image']); ?>" target="_blank">
                              <img src="<?php echo \Helper::amazonFileURL('media/images/medium/'. $pack['image']); ?>" height="100"/>
                            </a>
                          </div>
                        <?php endif; ?>
                      <?php endif; ?>
                      <div class="col-md-10">
                        <h1 class="label-title"><?php echo $pack['name']; ?></h1>
                        <?php if($pack['description']): ?><?php echo $pack['description']; ?><?php endif; ?>
                      </div>
                    </div>
                    <?php if(isset($pack['products'])): ?>
                      <?php echo $pack['selection_limit']?'<label>Select '.$pack['selection_limit'].' item(s):</label>':'';?>
                      <div class="row">
                        <?php foreach ($pack['products'] as $key => $pack_product): ?>
                          <?php if($get_item = \Product\Model_Product::find_one_by_id($pack_product)): ?>
                            <div class="col-md-6">
                              <label><input type="checkbox" name="packs[<?php echo $pack['id']; ?>][<?php echo $key; ?>]" value="<?php echo $pack_product; ?>"> <?php echo $get_item->title; ?></label>
                              <div class="row">
                                <?php if($cover_image = \Product\Model_Product::get_cover_image($get_item)): ?>
                                  <div class="col-md-5">
                                    <img src="<?php echo \Helper::amazonFileURL( \Config::get('media_base_url' ) .$cover_image->image); ?>" height="100">
                                  </div>
                                <?php endif; ?>
                                <div class="col-md-7">
                                  <div title="<?php echo strip_tags($get_item->description_full); ?>"><?php echo substr(strip_tags($get_item->description_full), 0, 50).'...'; ?></div>
                                  <?php if($pack['type'] == 'extras'): ?>
                                    <input class="product-quantity" type="text" value="" name="quantity_extra[<?php echo $pack['id']; ?>][<?php echo $key; ?>]">
                                  <?php endif; ?>
                                </div>
                              </div>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </div>
                    <?php endif; ?>
                  </div>
                <?php endforeach; ?>
              <?php endif; ?>
            <?php endif; ?>

            <div class="product-quantity">
              <?php if($stock_quantity > 0 || $do_not_allow_buy_out_of_stock == 0): ?>
                <?php if(isset($product->data['sx'])): ?>
                  <?php foreach($product->data['sx'] as $num => $sx): ?>
                    <div class="form-group">
                        <?php if(isset($sx) && isset($product->data['select'][$sx])): ?>
                             <h4 class="mb-0"> <label><?php echo $product->data['select_name'][$sx] ?></label></h4>
                              <?php if(!empty($product->data['select'][$sx])): ?>
                                  <?php echo \Form::select("select[{$sx}]", $product->data['current_attributes'][$num]->option->id, $product->data['select'][$sx], array('class' => 'form-control select_init attribute_select',  'data-attributeid' => $sx, 'style' => 'width:100%')); ?>
                              <?php endif; ?>
                          <?php endif; ?>
                    </div>
                  <?php endforeach; ?>
                <?php endif; ?>
                  Quantity
                  <input class="product-quantity" type="text" value="<?php echo $product->minimum_order?:1; ?>" name="quantity">
                  <?php echo \Form::hidden('product_id', $product->id); ?>
                  <?php echo \Form::hidden('attributeid', '', array('class' => 'attributeid')); ?>
                  <?php if(isset($attr_obj)) echo \Form::hidden('product_attribute_id', $attr_obj->id, array('class' => 'product_attribute_id')); ?>
                  <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>
                  <input type="hidden" id="minimum_order" name= "minimum_order" value="<?php echo $product->minimum_order;?>">
                  <button class="btn btn-green add_to_cart" value="Add to Cart">Add to Cart</button>
              <?php endif; ?>
            </div>
          <?php echo \Form::close(); ?>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="copy">

          <?php if($product->infotabs): ?>
            <div>
              <ul class="nav nav-tabs" role="tablist">
                <?php $counter = 0; foreach ($product->infotabs as $key => $infotab): ?>
                  <li class="<?php echo $counter==0 ? 'active': ''; ?>" role="presentation">
                    <a href="#infotab<?php echo $infotab->id ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $infotab->title ?></a>
                  </li>
                <?php $counter++; endforeach; ?>
              </ul>

              <div class="tab-content">
                <?php $counter = 0; foreach ($product->infotabs as $key => $infotab): ?>
                  <div role="tabpanel" class="tab-pane fade in <?php echo $counter==0 ? 'active': ''; ?>" id="infotab<?php echo $infotab->id ?>">
                    <?php
                      if($infotab->type == 'table')
                      {
                        echo '<textarea class="info_table hide">'.$infotab->description.'</textarea><div class="info_table_preview"></div>';

                        if($infotab->images)
                        {
                          foreach ($infotab->images as $key => $image)
                          {
                            echo '<a class="btn btn-green btn-block" href="'.\Helper::amazonFileURL('media/images/' . $image->image).'" title="'.($image->title ?: $image->image).'" target="_blank">'.($image->title ?: $image->image).'</a>'; 
                          }
                        }
                      }
                      else if($infotab->type == 'hotspot')
                      {
                        if(isset($infotab->image)):
                          ?>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="hotspot_cover_holder" style="position: relative; display: inline-block;" >
                              <?php echo \Html::img(\Helper::amazonFileURL('media/images/' . $infotab->image), array('class' => 'hotspot_cover', 'style' => 'border: solid 1px #D0D0D0;')); ?>

                              <?php if(!empty($infotab->images)): ?>
                                <?php foreach($infotab->images as $image): ?>

                                  <?php if(!is_null($image->position_x) && !is_null($image->position_y)): ?>

                                    <?php 
                                    $image_name = isset($hotspot) && $image->id == $hotspot->id ? 'hotspot_red.png' : 'hotspot_blue.png';
                                    ?>

                                    <a class="hotspot-show-data" rel="<?php echo $image->id; ?>" style="position: absolute; top: <?php echo $image->position_y; ?>px; left: <?php echo $image->position_x; ?>px;" href="#">
                                      <?php echo \Theme::instance()->asset->img('hotspot/'.$image_name); ?>
                                    </a>
                                  <?php endif; ?>

                                <?php endforeach; ?>
                              <?php endif; ?>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="hotspot-view-data"></div>
                          </div>
                        </div>
                        <?php
                        endif;
                      }
                      else
                      {
                        echo $infotab->description;
                        if($infotab->files)
                        {
                          foreach ($infotab->files as $key => $file)
                          {
                            echo '<a class="btn btn-green btn-block" href="'.\Helper::amazonFileURL('media/files/' . $file->file).'" title="'.($file->title ?: $file->file).'" download>'.($file->title ?: $file->file).'</a>'; 
                          }
                        }
                      }
                      ?>
                  </div>
                <?php $counter++; endforeach; ?>
              </div>

            </div>
          <?php endif; ?>
      </div>


      <?php if($product->related_products): ?>
        <div class="section-bottom">
          <div class="label-heading">
            <h5 class="label-title">related products</h5>
          </div>
          <ul class="list-inline">
            <?php 
              foreach($product->related_products as $related_product):
                $related_product_data = $related_product->data; 
            ?>
              <li>
                <a href="<?php echo \Uri::create($related_product->seo->slug); ?>">
                  <div class="product-img">
                    <?php 
                        if($cover_image = \Product\Model_Product::get_cover_image($related_product))
                            echo \Html::img( \Helper::amazonFileURL( \Config::get('media_base_url' ) . 'thumbs/' .$cover_image->image ) , array( 'alt' => $related_product->images[0]->alt_text ) ); 
                        else
                            echo $theme->asset->img( 'placeholders/thumb-product-01.jpg', array( 'alt' => 'product' ) );
                    ?>
                  </div>
                  <div class="product-content">
                    <h3 class="product-title"><?php echo $related_product->title; ?></h3>
                  <span class="product-price"><?php echo number_format($related_product_data['price'], 2);  ?></span>
                  </div>
                </a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>

<div id="template" style="display: none">    
  <div class="price_box">
    <span class="product-price text-right product_price"></span>
  </div>
</div>
  
<script type="text/javascript">
    // set variable needed for add to cart product in add-to-cart.js
    var $loading_img = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>select2-spinner.gif';
    var $images_base_url = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>';
    var $artwork_required = 0;
    var $createOrderUrl = '<?php echo \Uri::create('product/create_order'); ?>';
    var $addToCartUrl = '<?php echo \Uri::create('product/add_to_cart'); ?>';
    var $sendDataUrl = '<?php echo \Uri::create('product/commit_upload') ?>';
    var $artworkUploaded = '<?php echo \Uri::create('product/artwork_uploaded'); ?>';

    var $artworkUrl = '<?php echo \Uri::create('product/add_artwork_type/'); ?>';
    var $uploadItemUrl = '<?php echo \Uri::create('product/add_upload_item/'); ?>';
    var $itemInCartUrl = '<?php echo \Uri::create('product/add_to_cart/'); ?>';
    var $uploadArtworkUrl = '<?php echo \Uri::create('product/upload_artwork/'); ?>';
    var $getProductDataUrl = '<?php echo \Uri::create('product/product_data/' . $product->id); ?>';

    // set variable needed for add to cart product when .add_to_cart button is click
    $('.add_to_cart').on('click', function(e){
      $artworkUrl = $artworkUrl + $(this).attr('data-productid');
      $uploadItemUrl = $uploadItemUrl + $(this).attr('data-productid');
      $itemInCartUrl = $itemInCartUrl + $(this).attr('data-productid') + '/true';
      $uploadArtworkUrl = $uploadArtworkUrl + $(this).attr('data-productid');
      $getProductDataUrl = $getProductDataUrl + $(this).attr('data-productid');
    });
</script>
<!-- Add add-to-cart.js to make add to cart functionality working -->
<script type="text/javascript" src="/themes/frontend/js/add-to-cart.js"></script>