
<?php if(!empty($category->images)):?>
  <div class="slider-title">
    <div class="slides" role="listbox">
      <?php foreach($category->images as $image){ ?>
      <div class="item" style="background-image: url('<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $image->image); ?>')">
      <div class="slide-content">
          <h1 class="slide-title"><?php echo $category->title; ?></h1>
          <div class="slide-copy">
            <p><?php echo $category->images[0]->alt_text; ?></p>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
<?php endif; ?>

<div class="section-grey section-breadcrumb">
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="<?php echo \Uri::front_create('/'); ?>">Home</a></li>
            <li>></li>
            <li><a href="<?php echo \Uri::front_create('/menu'); ?>">Menu</a></li>
            <?php if($category_parents): $count = 1; ?>
                <li>></li>
                <?php foreach($category_parents as $parent): ?>
                    <li><a href="<?php echo \Uri::create($parent->seo->slug); ?>"><?php echo $parent->title; ?></a></li>
                    <?php if(count($category_parents) != $count ): ?><li>></li><?php endif; ?>
                    <?php $count++; endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>

<div class="section">
  <div class="container">
    <div class="category-products">

        <?php if($items): ?>
        <div class="row">
          <?php foreach ($items as $product): ?>
              <?php
                $product_data = $product->data;
                $stock_quantity = isset($product_data['current_attributes'][0]->product_attribute->stock_quantity)?$product_data['current_attributes'][0]->product_attribute->stock_quantity:(isset($product->attributes[0]->stock_quantity)?$product->attributes[0]->stock_quantity:0);
                ?>
              <div class="col-md-4 col-sm-6">
                <div class="product-item <?php echo ($stock_quantity <= 0) ? 'no-stock' : '' ?>">
                  <a href="<?php echo \Uri::create($product->seo->slug); ?>">
                    <div class="product-img">
                      <?php 
                          if($cover_image = \Product\Model_Product::get_cover_image($product))
                              echo \Html::img( \Helper::amazonFileURL( \Config::get('media_base_url' ) .$cover_image->image ) , array( 'width' => '356', 'height' => '321', 'class' => 'product-img', 'alt' => $cover_image->alt_text ) ); 
                      ?>
                      <?php if(empty($product->images)): ?>
                        <div class="no-image">
                          <i class="icon icon-image2"></i>
                          <h4>No image</h4>
                        </div>
                      <?php endif; ?>
                    </div>
                    <?php if($stock_quantity <= 0): ?>
                      <div class="ribbon">
                        <h2 class="ribbon-title">Out of Stock</h2>
                      </div>
                    <?php endif; ?>


                    <?php $available = ($stock_quantity > 0 || $do_not_allow_buy_out_of_stock == 0)  && $product->product_type == 'simple' ? TRUE:FALSE; ?>
                    <span class="product-display">
                      <span class="product-content">
                        <h3 class="product-title" title="<?php echo $product->title; ?>"><?php echo $product->title; ?></h3>
                        <span class="product-price">$<?php echo number_format($product_data['price'], 2); ?> <small>per person</small></span>
                        <p>
                        <?php if($product->minimum_order):?>
                        <small>minimum <?php echo $product->minimum_order;?> people</small>
                        <?php else: ?>
                          &nbsp;
                        <?php endif;?> 
                        </p>

                        <?php if($available): ?>
                          <?php echo \Form::open(array('action' => \Uri::current(), 'method' => 'post', 'id' => 'cart_form')); ?>
                            <?php echo \Form::hidden('product_id', $product->id); ?>
                            <?php echo \Form::hidden('attributeid', '', array('class' => 'attributeid')); ?>
                            <?php if(isset($attr_obj)) echo \Form::hidden('product_attribute_id', $attr_obj->id, array('class' => 'product_attribute_id')); ?>
                            <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>
                            <span>
                              Qty: <input type="number" min="1" name="quantity" value="<?php echo $product->minimum_order?:1; ?>" onclick="return false;" class="form-control" style="width:100px;display:inline;">
                            </span>
                            <input type="hidden" id="minimum_order" name= "minimum_order" value="<?php echo $product->minimum_order;?>">
                            <span class="product-action add_to_cart" style="display:none">
                                <i class="icon icon-plus"></i>
                            </span>
                          <?php echo \Form::close(); ?>
                        <?php endif; ?>
                      </span>

                      <?php if ($available): ?>
                        <span class="product-action add_to_cart_dummy">
                            <i class="icon icon-plus"></i>
                        </span>
                      <?php else: ?>
                        <span class="product-action">
                          <i class="icon icon-search"></i>
                        </span>
                      <?php endif; ?>
                    </span>
                  </a>
                </div>
              </div>
          <?php endforeach; ?>
        </div>
            <div class="pagination_holder bottom_pagination pagination_holder_theme">
              <?php echo $theme->view('views/_partials/pagination', array('pagination' => $pagination), false); ?>
            </div>
        <?php else: ?>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    No Available Products.
                </div>
            </div>

        <?php endif; ?>

    </div>
    <div class="copy">

        <?php if($category->infotabs): ?>
          <div>
            <ul class="nav nav-tabs" role="tablist">
              <?php $counter = 0; foreach ($category->infotabs as $key => $infotab): ?>
                <li class="<?php echo $counter==0 ? 'active': ''; ?>" role="presentation">
                  <a href="#infotab<?php echo $infotab->id ?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $infotab->title ?></a>
                </li>
              <?php $counter++; endforeach; ?>
            </ul>

            <div class="tab-content">
              <?php $counter = 0; foreach ($category->infotabs as $key => $infotab): ?>
                <div role="tabpanel" class="tab-pane fade in <?php echo $counter==0 ? 'active': ''; ?>" id="infotab<?php echo $infotab->id ?>">
                  <?php
                    if($infotab->type == 'table')
                    {
                      echo '<textarea class="info_table hide">'.$infotab->description.'</textarea><div class="info_table_preview"></div>';

                      if($infotab->images)
                      {
                        foreach ($infotab->images as $key => $image)
                        {
                          echo '<a class="btn btn-green btn-block" href="'.\Helper::amazonFileURL('media/images/' . $image->image).'" title="'.($image->title ?: $image->image).'" target="_blank">'.($image->title ?: $image->image).'</a>'; 
                        }
                      }
                    }
                    else if($infotab->type == 'hotspot')
                    {
                      if(isset($infotab->image)):
                        ?>
                      <div class="row">
                        <div class="col-md-6">
                          <div class="hotspot_cover_holder" style="position: relative; display: inline-block;" >
                            <?php echo \Html::img(\Helper::amazonFileURL('media/images/' . $infotab->image), array('class' => 'hotspot_cover', 'style' => 'border: solid 1px #D0D0D0;')); ?>

                            <?php if(!empty($infotab->images)): ?>
                              <?php foreach($infotab->images as $image): ?>

                                <?php if(!is_null($image->position_x) && !is_null($image->position_y)): ?>

                                  <?php 
                                  $image_name = isset($hotspot) && $image->id == $hotspot->id ? 'hotspot_red.png' : 'hotspot_blue.png';
                                  ?>

                                  <a class="hotspot-show-data" rel="<?php echo $image->id; ?>" style="position: absolute; top: <?php echo $image->position_y; ?>px; left: <?php echo $image->position_x; ?>px;" href="#">
                                    <?php echo \Theme::instance()->asset->img('hotspot/'.$image_name); ?>
                                  </a>
                                <?php endif; ?>

                              <?php endforeach; ?>
                            <?php endif; ?>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="hotspot-view-data"></div>
                        </div>
                      </div>
                      <?php
                      endif;
                    }
                    else
                    {
                      echo $infotab->description;
                      if($infotab->files)
                      {
                        foreach ($infotab->files as $key => $file)
                        {
                          echo '<a class="btn btn-green btn-block" href="'.\Helper::amazonFileURL('media/files/' . $file->file).'" title="'.($file->title ?: $file->file).'" download>'.($file->title ?: $file->file).'</a>'; 
                        }
                      }
                    }
                    ?>
                </div>
              <?php $counter++; endforeach; ?>
            </div>

          </div>
        <?php endif; ?>
    </div>
    
    <script type="text/javascript">
        var $loading_img = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>select2-spinner.gif';
        var $images_base_url = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>';
        var $artwork_required = 0;
        var $createOrderUrl = '<?php echo \Uri::create('product/create_order'); ?>';
        var $addToCartUrl = '<?php echo \Uri::create('product/add_to_cart'); ?>';
        var $sendDataUrl = '<?php echo \Uri::create('product/commit_upload') ?>';
        var $artworkUploaded = '<?php echo \Uri::create('product/artwork_uploaded'); ?>';

        var $artworkUrl = '<?php echo \Uri::create('product/add_artwork_type/'); ?>';
        var $uploadItemUrl = '<?php echo \Uri::create('product/add_upload_item/'); ?>';
        var $itemInCartUrl = '<?php echo \Uri::create('product/add_to_cart/'); ?>';
        var $uploadArtworkUrl = '<?php echo \Uri::create('product/upload_artwork/'); ?>';
        var $getProductDataUrl = '<?php echo \Uri::create('product/product_data/'); ?>';

        $('.add_to_cart').on('click', function(e){
          $artworkUrl = $artworkUrl + $(this).attr('data-productid');
          $uploadItemUrl = $uploadItemUrl + $(this).attr('data-productid');
          $itemInCartUrl = $itemInCartUrl + $(this).attr('data-productid') + '/true';
          $uploadArtworkUrl = $uploadArtworkUrl + $(this).attr('data-productid');
          $getProductDataUrl = $getProductDataUrl + $(this).attr('data-productid');
        });

        $('.add_to_cart_dummy').click(function(e){
          $(this).parent('.product-display').find('.add_to_cart').click();
        });
    </script>
    <script type="text/javascript" src="/themes/frontend/js/add-to-cart.js"></script>
  </div>
</div>