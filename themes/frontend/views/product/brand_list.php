<div class="section-grey">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo \Uri::front_create('/'); ?>">Home</a></li>
      <li>></li>
      <li><a href="<?php echo \Uri::front_create('/products'); ?>">Products</a></li>
      <li><?php echo $brand->title; ?></li>
    </ul>
  </div>
</div>
<?php if(!empty($brand->images)):?>
  <div class="slider-title">
    <div class="slides" role="listbox">
      <?php foreach($brand->images as $image){ ?>
      <div class="item" style="background-image: url('<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $image->image); ?>')">
      <div class="slide-content">
          <h1 class="slide-title"><?php echo $brand->title; ?></h1>
          <div class="slide-copy">
            <p><?php echo $brand->images[0]->alt_text; ?></p>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
<?php endif; ?>

<div class="app-content">
  <div class="container">
    <h2 class="copy-title"><?php echo !empty($brand->images) ? $brand->description_intro : $brand->title; ?></h2>
    <div class="category-products">
      <div class="row">
        <?php if($items): ?>
          <?php foreach ($items as $product): ?>
              <?php
                $product_data = $product->data;
                $stock_quantity = isset($product_data['current_attributes'][0]->product_attribute->stock_quantity)?$product_data['current_attributes'][0]->product_attribute->stock_quantity:(isset($product->attributes[0]->stock_quantity)?$product->attributes[0]->stock_quantity:0);
              ?>
              <div class="col-md-4 col-sm-6">
                <div class="product-item <?php echo ($stock_quantity <= 0) ? 'no-stock' : '' ?>">
                  <a href="<?php echo \Uri::create($product->seo->slug); ?>">
                    <div class="product-img">
                      <?php 
                          if($cover_image = \Product\Model_Product::get_cover_image($product))
                              echo \Html::img( \Helper::amazonFileURL( \Config::get('media_base_url' ) .$cover_image->image ) , array( 'width' => '356', 'height' => '321', 'class' => 'product-img', 'alt' => $cover_image->alt_text ) ); 
                      ?>
                      <?php if(empty($product->images)): ?>
                        <div class="no-image">
                          <i class="icon icon-image2"></i>
                          <h4>No image</h4>
                        </div>
                      <?php endif; ?>
                    </div>
                    <?php if($stock_quantity <= 0): ?>
                      <div class="ribbon">
                        <h2 class="ribbon-title">Out of Stock</h2>
                      </div>
                    <?php endif; ?>


                    <span class="product-display">
                      <span class="product-content">
                        <h3 class="product-title" title="<?php echo $product->title; ?>"><?php echo $product->title; ?></h3>
                        <span class="product-price">$<?php echo number_format($product_data['price'], 2); ?> <small>per person</small></span>
                        <?php if($product->minimum_order):?>
                        <p><small>minimum <?php echo $product->minimum_order;?> people</small></p>
                        <?php endif;?> 
                      </span>
                      <?php if(($stock_quantity > 0 || $do_not_allow_buy_out_of_stock == 0) && $product->product_type == 'simple'): ?>

                        <?php echo \Form::open(array('action' => \Uri::current(), 'method' => 'post', 'id' => 'cart_form')); ?>
                          <?php echo \Form::hidden('product_id', $product->id); ?>
                          <?php echo \Form::hidden('attributeid', '', array('class' => 'attributeid')); ?>
                          <?php if(isset($attr_obj)) echo \Form::hidden('product_attribute_id', $attr_obj->id, array('class' => 'product_attribute_id')); ?>
                          <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>
                          <?php echo \Form::hidden('quantity', $product->minimum_order?:1); ?>
                          <input type="hidden" id="minimum_order" name= "minimum_order" value="<?php echo $product->minimum_order;?>">
                          <span class="product-action add_to_cart">
                              <i class="icon icon-plus"></i>
                          </span>
                        <?php echo \Form::close(); ?>
                      <?php else: ?>
                        <span class="product-action">
                          <i class="icon icon-search"></i>
                        </span>
                      <?php endif; ?>
                    </span>
                  </a>
                </div>
              </div>
          <?php endforeach; ?>
            <div class="pagination_holder bottom_pagination">
              <?php echo $theme->view('views/_partials/pagination', array('pagination' => $pagination), false); ?>
            </div>
        <?php else: ?>
          <div class="col-md-4 col-sm-6">
            No Available Products.
          </div>
        <?php endif; ?>
      </div>
    </div>
    
    <script type="text/javascript">
        var $loading_img = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>select2-spinner.gif';
        var $images_base_url = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>';
        var $artwork_required = 0;
        var $createOrderUrl = '<?php echo \Uri::create('product/create_order'); ?>';
        var $addToCartUrl = '<?php echo \Uri::create('product/add_to_cart'); ?>';
        var $sendDataUrl = '<?php echo \Uri::create('product/commit_upload') ?>';
        var $artworkUploaded = '<?php echo \Uri::create('product/artwork_uploaded'); ?>';

        var $artworkUrl = '<?php echo \Uri::create('product/add_artwork_type/'); ?>';
        var $uploadItemUrl = '<?php echo \Uri::create('product/add_upload_item/'); ?>';
        var $itemInCartUrl = '<?php echo \Uri::create('product/add_to_cart/'); ?>';
        var $uploadArtworkUrl = '<?php echo \Uri::create('product/upload_artwork/'); ?>';
        var $getProductDataUrl = '<?php echo \Uri::create('product/product_data/'); ?>';

        $('.add_to_cart').on('click', function(e){
          $artworkUrl = $artworkUrl + $(this).attr('data-productid');
          $uploadItemUrl = $uploadItemUrl + $(this).attr('data-productid');
          $itemInCartUrl = $itemInCartUrl + $(this).attr('data-productid') + '/true';
          $uploadArtworkUrl = $uploadArtworkUrl + $(this).attr('data-productid');
          $getProductDataUrl = $getProductDataUrl + $(this).attr('data-productid');
        });
    </script>
    <script type="text/javascript" src="/themes/frontend/js/add-to-cart.js"></script>
  </div>
</div>