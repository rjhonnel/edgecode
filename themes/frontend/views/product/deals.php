<?php
  $images_base_url = 'media/images/';
?>
<div class="app-content">
    <div class="container">

      	<div class="label-heading">
     		<h1 class="label-title">Deals</h1>
      	</div>

     	<div class="app-content">
     		<?php
          if($deals)
          {
            foreach ($deals as $deal)
            {
              $item = $deal->product;
              ?>
                <div class="images">
                  <div class="col-sm-12">
                      <div class="product-item">
                          <a href="<?php echo \Uri::create('deals/'.$item->seo->slug.'/'.$deal->id); ?>">
                              <div class="product-img">
                                  <?php 
                                      // Get product data
                                      $product_data = \Product\Model_Product::product_data($item);
                                  ?>
                                  <?php if($cover_image = \Product\Model_Product::get_cover_image($item)): ?>
                                      <?php echo \Html::img(\Helper::amazonFileURL($images_base_url . 'medium/' . $cover_image->image), array('width' => 'auto', 'height' => 321, 'alt' =>  $product_data['images'][0]->alt_text)); ?>
                                  <?php else: ?>
                                    <div class="no-image">
                                      <i class="icon icon-image2"></i>
                                      <h4>No image</h4>
                                    </div>
                                  <?php endif; ?>
                              </div>
                              <span class="product-display">
                                  <div class="product-content">
                                      <h3 class="product-title" style="width:auto"><?php echo $item->title; ?>: <?php echo $deal->title; ?></h3>
                                      <div class="product-price">
                                          <?php echo '$' . number_format($product_data['price'], 2);  ?>
                                      </div>
                                  </div>
                                  <span class="product-action">
                                      <i class="icon icon-search"></i>
                                  </span>
                              </span>
                          </a>
                      </div>
                  </div>
                </div>
              <?php
            }
          }
          else
            echo 'No product deals.';
     		?>
      </div>

    </div>
</div>