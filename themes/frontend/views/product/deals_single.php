<?php
  	$product_data = $product->data;
  	$stock_quantity = isset($product_data['current_attributes'][0]->product_attribute->stock_quantity)?$product_data['current_attributes'][0]->product_attribute->stock_quantity:(isset($product->attributes[0]->stock_quantity)?$product->attributes[0]->stock_quantity:0);
?>
<div class="app-content">
    <div class="container">

      	<div class="label-heading">
     		<h1 class="label-title"><?php echo $title; ?></h1>
      	</div>

     	<div class="app-content">
		    <div class="row">
		    	<?php if($deal->short_description): ?>
			    	<div class="col-md-12">
				        <div class="copy">
				          	<p><?php echo $deal->short_description; ?></p>
				        </div>
			    	</div>
			    <?php endif; ?>
		      	<div class="col-md-12">
			        <div id="slider" class="slider-single">
			          <div class="slides">
			            <?php if(empty($product->images)): ?>
			              <div class="<?php echo $stock_quantity <= 0 ? 'no-stock': ''; ?> ">
			                <div class="product-img">
			                  <div class="no-image">
			                    <i class="icon icon-image2"></i>
			                    <h4>No image</h4>
			                  </div>
			                </div>
			                <?php if($stock_quantity <= 0): ?>
			                  <div class="ribbon">
			                    <h2 class="ribbon-title">Out of Stock</h2>
			                  </div>
			                <?php endif; ?>
			              </div>
			            <?php endif; ?>
			            <?php foreach($product->images as $image): ?>
			              <div class="item <?php echo $stock_quantity <= 0 ? 'no-stock': ''; ?>"><img src="<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $image->image); ?>" alt="<?php echo $image->alt_text; ?>">
			              <?php if($stock_quantity <= 0): ?>
			                <div class="ribbon">
			                  <h2 class="ribbon-title">Out of Stock</h2>
			                </div>
			              <?php endif; ?>
			              </div>
			            <?php endforeach; ?>
			          </div>
			        </div>
			        <div class="slider-single-nav">
			          <div class="slides">
			            <?php foreach($product->images as $image): ?>
			              <div class="item"><img src="<?php echo \Helper::amazonFileURL( \Config::get('media_base_url'). 'thumbs/' . $image->image); ?>" alt="<?php echo $image->alt_text; ?>"></div>
			            <?php endforeach; ?>
			          </div>
			        </div>
		      	</div>
			  	<div class="col-md-12">
			        <div class="page-single">
				       	<div class="product-heading">
				          <h2 class="product-title text-left">Product Code:</h2>
				          <div class="price_wrapper">
				            <div class="price_box">
				              <span class="product-price text-right product_price">$<?php echo $product_data['price']?number_format($product_data['price'], 2):number_format($product_data['retail_price'], 2); ?></span>
				            </div>
				          </div>
				          <h5 class="product-code"><?php echo $product->code; ?></h5>
				        </div>

				          <div class="product-quantity">
				            <?php if($stock_quantity > 0 || $do_not_allow_buy_out_of_stock == 0): ?>
				              <?php echo \Form::open(array('action' => \Uri::current(), 'method' => 'post', 'id' => 'cart_form')); ?>
				              <?php if(isset($product->data['sx'])): ?>
				                <?php foreach($product->data['sx'] as $num => $sx): ?>
				                  <div class="form-group">
				                      <?php if(isset($sx) && isset($product->data['select'][$sx])): ?>
				                           <h4 class="mb-0"> <label><?php echo $product->data['select_name'][$sx] ?></label></h4>
				                            <?php if(!empty($product->data['select'][$sx])): ?>
				                                <?php echo \Form::select("select[{$sx}]", $product->data['current_attributes'][$num]->option->id, $product->data['select'][$sx], array('class' => 'form-control select_init attribute_select',  'data-attributeid' => $sx, 'style' => 'width:100%')); ?>
				                            <?php endif; ?>
				                        <?php endif; ?>
				                  </div>
				                <?php endforeach; ?>
				              <?php endif; ?>
				                Quantity
				                <input class="product-quantity" type="text" value="1" name="quantity">
				                <?php echo \Form::hidden('product_id', $product->id); ?>
				                <?php echo \Form::hidden('attributeid', '', array('class' => 'attributeid')); ?>
				                <?php if(isset($attr_obj)) echo \Form::hidden('product_attribute_id', $attr_obj->id, array('class' => 'product_attribute_id')); ?>
				                <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>
				                <button class="btn btn-green add_to_cart" value="Add to Cart">Add to Cart</button>
				              <?php echo \Form::close(); ?>
				            <?php endif; ?>
				          </div>
			        </div>
			  	</div>
		    	<?php if($deal->full_description): ?>
			    	<div class="col-md-12">
				        <div class="copy">
				            <div style="margin-top: 50px;">
				              <ul class="nav nav-tabs" role="tablist">
				                  <li class="active" role="presentation">
				                    <a href="#description-tab" aria-controls="home" role="tab" data-toggle="tab">Description</a>
				                  </li>
				              </ul>

				              <div class="tab-content">
				                  <div role="tabpanel" class="tab-pane fade in active" id="description-tab">
				                  	<?php echo $deal->full_description; ?>
				                  </div>
				              </div>

				            </div>
				        </div>
			    	</div>
			    <?php endif; ?>
		  	</div>
      	</div>

    </div>
</div>

<div id="template" style="display: none">    
  <div class="price_box">
    <span class="product-price text-right product_price"></span>
  </div>
</div>
  
<script type="text/javascript">
    // set variable needed for add to cart product in add-to-cart.js
    var $loading_img = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>select2-spinner.gif';
    var $images_base_url = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>';
    var $artwork_required = 0;
    var $createOrderUrl = '<?php echo \Uri::create('product/create_order'); ?>';
    var $addToCartUrl = '<?php echo \Uri::create('product/add_to_cart'); ?>';
    var $sendDataUrl = '<?php echo \Uri::create('product/commit_upload') ?>';
    var $artworkUploaded = '<?php echo \Uri::create('product/artwork_uploaded'); ?>';

    var $artworkUrl = '<?php echo \Uri::create('product/add_artwork_type/'); ?>';
    var $uploadItemUrl = '<?php echo \Uri::create('product/add_upload_item/'); ?>';
    var $itemInCartUrl = '<?php echo \Uri::create('product/add_to_cart/'); ?>';
    var $uploadArtworkUrl = '<?php echo \Uri::create('product/upload_artwork/'); ?>';
    var $getProductDataUrl = '<?php echo \Uri::create('product/product_data/' . $product->id); ?>';

    // set variable needed for add to cart product when .add_to_cart button is click
    $('.add_to_cart').on('click', function(e){
      $artworkUrl = $artworkUrl + $(this).attr('data-productid');
      $uploadItemUrl = $uploadItemUrl + $(this).attr('data-productid');
      $itemInCartUrl = $itemInCartUrl + $(this).attr('data-productid') + '/true';
      $uploadArtworkUrl = $uploadArtworkUrl + $(this).attr('data-productid');
      $getProductDataUrl = $getProductDataUrl + $(this).attr('data-productid');
    });
</script>
<!-- Add add-to-cart.js to make add to cart functionality working -->
<script type="text/javascript" src="/themes/frontend/js/add-to-cart.js"></script>