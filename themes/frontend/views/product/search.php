<?php 
$images_base_path = \Config::get('details.image.location.root');
$images_base_url = 'media/images/';

$per_page = array(
    3 => 3,
    15 => 15,
    30 => 30,
    60 => 60,
    99999 => 'All'
);

$sort = array(
    'title_asc' => 'Title Ascending',
    'title_desc' => 'Title Descending',
    //'price_asc' => 'Price Ascending',
    //'price_desc' => 'Price Descending',
);

?>

<div class="app-content">
    <div class="container">

      <div class="label-heading">
          <h2 class="label-title">Search Results</h2>
      </div>

         <div class="app-content">

         <div class="pagination_holder">
                <?php echo $theme->view('views/_partials/pagination', array('pagination' => $pagination), false); ?>
            </div>
         </div>

             <?php if(!empty($items)): ?>
                    <?php foreach ($items as $key => $item): ?>
                        <?php if($key > 0 && $key % 5 == 0) echo '<div class="clear"></div>'; ?>
                        <div class="images">
                        <div class="col-sm-4">
                            <div class="product-item">
                                <a href="<?php echo \Uri::create($item->seo->slug); ?>">
                                    <div class="product-img">
                                        <?php 
                                            // Get product data
                                            $product_data = \Product\Model_Product::product_data($item);
                                        ?>
                                        <?php if($cover_image = \Product\Model_Product::get_cover_image($item)): ?>
                                            <?php echo \Html::img(\Helper::amazonFileURL($images_base_url . 'medium/' . $cover_image->image), array('width' => 'auto', 'height' => 321, 'alt' =>  $product_data['images'][0]->alt_text)); ?>
                                        <?php else: ?>
                                          <div class="no-image">
                                            <i class="icon icon-image2"></i>
                                            <h4>No image</h4>
                                          </div>
                                        <?php endif; ?>
                                    </div>
                                    <span class="product-display">
                                        <div class="product-content">
                                            <h3 class="product-title"><?php echo $item->title; ?></h3>
                                            <div class="product-price">
                                                <?php echo '$' . number_format($product_data['price'], 2);  ?>
                                            </div>
                                        </div>
                                        <span class="product-action">
                                            <i class="icon icon-search"></i>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

         <div class="pagination_holder bottom_pagination">
                <?php echo $theme->view('views/_partials/pagination', array('pagination' => $pagination), false); ?>
            </div>
         </div> 


    </div>
</div>