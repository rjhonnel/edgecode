
<!-- Desktop Header -->
<header class="app-header">
  <div class="app-masthead">
    <div class="container">      
      <a href="<?php echo \Uri::create('/'); ?>">
        <div class="app-brand"></div>
      </a>      
      <div class="app-login-btn dropdown">
        <?php if($logged_type == 'user'): ?>
          <a href="<?php echo \Uri::front_create('user/account/dashboard'); ?>" class="btn btn-stroked dropdown">
            My Account
          <i class="icon icon-caret-down"></i>
          </a>
          <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
            <li><a href="<?php echo \Uri::front_create('user/account/orders'); ?>">Orders</a></li>
            <?php if(strtolower(\Sentry::user()->groups()[0]['name']) == 'club members'): ?>
              <li><a href="<?php echo \Uri::front_create('user/account/referrals'); ?>">Referrals</a></li>
            <?php endif; ?>
            <li><a href="<?php echo \Uri::front_create('user/logout'); ?>">Log Out</a></li>
          </ul>
        <?php else: ?>
          <a href="<?php echo \Uri::front_create('user/login'); ?>" class="btn btn-stroked pr-22px">
            Log in
        <?php endif; ?>
        </a>
      </div>
    </div>
  </div>
  <div class="app-drawer">
    <div class="container">
      <nav class="nav-menu">
        <ul>
          <?php
            // Make sure we have our config loaded
            \Config::load('page::page', 'page', true);
            
            // Get root pages
            $pages = \Page\Model_Page::find(function($query){
                $query->where('parent_id', 0);
                $query->order_by('sort', 'asc');
            });

            // Get root product categories
            $categories = \Product\Model_Category::find(function($query){
                $query->where('parent_id', 0);
                $query->order_by('sort', 'asc');
            });
            $group_id = \Sentry::user()->group_id();

            if($categories){
              for($x=0;$x<sizeof($categories);$x++){
                     
                  if(!empty((array)json_decode($categories[$x]->user_group))){
                      
                     if(!in_array($group_id,(array)json_decode($categories[$x]->user_group))){
                         unset($categories[$x]);
                     }
                   
                  } 
              }
            }
            $limit = 4;
          ?>

          <?php 
            foreach($pages as $page): 
              $limit--;

              // We want to show only $limit number of pages in this menu
              if($limit < 0) break;
              
              // Generate page uri
              $uri = $page->seo->slug;

              if($page->id == \Config::get('page.locked_items.home_page', 0))
              {
                  $uri = '/';
              }

              // Locked itmes specific links
              $locked_items_url = \Config::get('page.locked_items_url', array());
              if(isset($locked_items_url[$page->id]))  $uri = $locked_items_url[$page->id];
          ?>
            <li class="<?php echo ($page->id != \Config::get('page.locked_items.gallery', 0) && count($page->children) > 0) || $page->id == 118 ? 'has-dropdown' : ''; ?>">
              <a href="<?php echo \Uri::create($uri); ?>"><?php echo $page->title; ?></a>

              <?php if($page->id != \Config::get('page.locked_items.gallery', 0) && $page->children): ?>
                  <ul class="dropdown-menu">
                      <?php foreach($page->children as $child_page): ?>
                          <li><a href="<?php echo \Uri::create($child_page->seo->slug); ?>"><?php echo $child_page->title; ?></a></li>
                      <?php endforeach; ?>
                  </ul>
              <?php elseif($page->id == 118 && $categories): ?>
                <ul class="dropdown-menu">
                  <?php foreach($categories as $category): ?>
                    <li><a href="<?php echo \Uri::create($category->seo->slug) ?>"><?php echo $category->title; ?></a></li>
                  <?php endforeach; ?>
                </ul>
              <?php endif; ?>
            </li>
          <?php endforeach; ?>
          <li><a href="<?php echo \Uri::create('deals'); ?>">Deals</a></li>

          <?php
            $brands = \Product\Model_Brand::find(function($query){
              $query->where('parent_id', 0);
              $query->order_by('sort', 'asc');
            });
          ?>
          <?php if($brands): ?>
            <li class="has-dropdown">
                <a href="javascript:void(0);">Brands</a>
                <ul class="dropdown-menu">
                  <?php foreach($brands as $brand ): ?>
                    <li><a href="<?php echo \Uri::create($brand->seo->slug) ?>"><?php echo $brand->title;?></a></li>
                  <?php endforeach; ?>
                </ul>
            </li>
          <?php endif; ?>
        </ul>
      </nav>
      <nav class="nav-menu nav-stroked">
        <ul>
          <li>
            <a href="<?php echo \Uri::create('order/cart/edit_cart'); ?>"><i class="icon icon-cart"></i>
              <span class="js-counter-view total_cart_items"><?php echo count(\Cart::items()); ?></span></a>
          </li>
          <li>
              <div class="search-toggle-btn">
                <i class="icon icon-search"></i>
              </div>
          </li>
        </ul>
        <div class="search-toggle">
          <div class="search-wrapper">
            <div class="search-wrapper-inner">
              <?php echo \Form::open(array('action' => \Uri::create('search'), 'class' => 'form-search', 'id' => 'form-search',  'method' => 'get')) ?>
                <div class="input-group">
                  <input type="text" class="form-control" name="keyword" value="<?php echo \Input::get('keyword'); ?>" placeholder="Search...">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default" type="button">search</button>
                  </span>
                </div>
              <?php echo \Form::close(); ?>
            </div>
          </div>
        </div>
      </nav>
    </div>
  </div>
</header>
<!-- EOF Desktop Header -->

<!-- Mobile Header -->
<header class="app-header-mobile">
  <div class="app-masthead">
    <div class="container">
     <a class="menu-toggle" id="menu-toggle">
        Menu
      </a>      
      <a href="<?php echo \Uri::create('/'); ?>">
        <div class="app-brand-logo">
          <img src="<?php echo \Uri::create('/') ?>/themes/frontend/images/brand.jpg">
        </div>
      </a>

      <ul class="mobile-menu" id="mobile-menu-collapse">
          <?php
            // Make sure we have our config loaded
            \Config::load('page::page', 'page', true);
            
            // Get root pages
            $pages = \Page\Model_Page::find(function($query){
                $query->where('parent_id', 0);
                $query->order_by('sort', 'asc');
            });
            
            $limit = 5;
          ?>

          <?php 
            foreach($pages as $page): 
              $limit--;

              // We want to show only $limit number of pages in this menu
              if($limit < 0) break;
              
              // Generate page uri
              $uri = $page->seo->slug;

              if($page->id == \Config::get('page.locked_items.home_page', 0))
              {
                  $uri = '/';
              }

              // Locked itmes specific links
              $locked_items_url = \Config::get('page.locked_items_url', array());
              if(isset($locked_items_url[$page->id]))  $uri = $locked_items_url[$page->id];
          ?>
            <li class="<?php echo $page->id != \Config::get('page.locked_items.gallery', 0) && count($page->children) > 0 ? 'has-dropdown' : ''; ?>">
              <a href="<?php echo \Uri::create($uri); ?>"><?php echo $page->title; ?></a>

              <?php if($page->id != \Config::get('page.locked_items.gallery', 0) && $page->children): ?>
                  <ul class="dropdown-menu">
                      <?php foreach($page->children as $child_page): ?>
                          <li><a href="<?php echo \Uri::create($child_page->seo->slug); ?>"><?php echo $child_page->title; ?></a></li>
                      <?php endforeach; ?>
                  </ul>
              <?php endif; ?>
            </li>
          <?php endforeach; ?>
          <li><a href="<?php echo \Uri::create('deals'); ?>">Deals</a></li>
          <li class="dropdown">
            <?php if($logged_type == 'user'): ?>
          <a href="<?php echo \Uri::front_create('user/account/dashboard'); ?>" data-target="#" data-toggle="dropdown disabled" aria-haspopup="true" aria-expanded="false">
            My Account
          <i class="icon icon-caret-down"></i>
          </a>
          <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a href="<?php echo \Uri::front_create('user/account/orders'); ?>">Orders</a></li>
            <?php if(strtolower(\Sentry::user()->groups()[0]['name']) == 'club members'): ?>
              <li><a href="<?php echo \Uri::front_create('user/account/referrals'); ?>">Referrals</a></li>
            <?php endif; ?>
            <li><a href="<?php echo \Uri::front_create('user/logout'); ?>">Log Out</a></li>
          </ul>
        <?php else: ?>
          <a href="<?php echo \Uri::front_create('user/login'); ?>" class="btn btn-stroked pr-22px">
            Log in
        <?php endif; ?>
        </a>
          </li>
        </ul>     

    </div>
    <div class="app-drawer">
      <nav class="nav-menu nav-stroked">
        <ul>
          <li>
            <a href="<?php echo \Uri::create('order/cart/edit_cart'); ?>"><i class="icon icon-cart"></i>
              <span class="js-counter-view total_cart_items"><?php echo count(\Cart::items()); ?></span></a>
          </li>
          <li>
              <div class="search-toggle-btn">
                <i class="icon icon-search"></i>
              </div>
          </li>
        </ul>
        <div class="search-toggle">
          <div class="search-wrapper">
            <div class="search-wrapper-inner">
              <?php echo \Form::open(array('action' => \Uri::create('search'), 'class' => 'form-search', 'id' => 'form-search',  'method' => 'get')) ?>
                <div class="input-group">
                  <input type="text" class="form-control" name="keyword" value="<?php echo \Input::get('keyword'); ?>" placeholder="Search...">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-default" type="button">search</button>
                  </span>
                </div>
              <?php echo \Form::close(); ?>
            </div>
          </div>
        </div>
      </nav>
    </div>
  </div>
</header>
<!-- EOF Mobile Header -->