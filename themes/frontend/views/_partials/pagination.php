
<!-- Pagination -->
<?php 
    $per_page = array(
        
        15 => 15,
        30 => 30,
        60 => 60,
        99999 => 'All'
    );

    $sort = array(
        'title_asc' => 'Title Ascending',
        'title_desc' => 'Title Descending',
    );
?>
<div class="content_holder">
    <div class="pagination" style="display:block">
    <?php echo $pagination; ?>

    <!-- <ul class="pagination">
        <li>items <?php echo $pagination->total_items; ?></li>
        <li class="pagination_list_separator">|</li>
        <li>show:</li>

        <?php foreach ($per_page as $k => $v): ?>
            <li>
                <?php if(\Input::get('per_page', $pagination->per_page) == $k):?>
                    <span><?php echo $v; ?></span>
                <?php else: ?>
                    <a href="<?php echo \Uri::create(\Uri::front(), array(), array('per_page' => $k) + \Input::get()); ?>"><?php echo $v; ?></a>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>

        <li class="pagination_list_separator">|</li>
    </ul> -->

        <div class="right">
            <?php echo \Form::open(array('action' => \Uri::front(), 'method' => 'get'), \Input::get()); ?>
                
                <ul class="pagination">
                    <li>sort by:</li>
                    <li>
                        <?php echo \Form::select('sort', \Input::get('sort'), $sort, array('class' => 'select_init right change_submit')) ?>
                    </li>
                </ul>
            <?php echo \Form::close(); ?>
        </div>

    </div>
</div>
<!-- EOF Pagination -->