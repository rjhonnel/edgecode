<footer class="app-footer">
  <div class="newsletter-subscribe">
    <div class="container">
      <!-- Subscribe Form -->
      <?php echo \Form::open(array('action' => \Uri::create('newsletter/subscribe'), 'data-validate' => 'parsley', 'id' => 'subscribe_form', 'class' => 'form-inline form-subscribe')); ?>
        <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>
        <div class="row">
          <div class="col-sm-4">
            <label>Sign up to our newsletter:</label>
          </div>
          <div class="col-sm-8">
            <div class="input-group">
              <?php echo \Form::input('email', \Input::post('email'), array("class" => "required form-control", 'data-type' => 'email', 'placeholder' => 'Email')); ?>
              <button type="submit" class="btn btn-green">Sign Up</button>
            </div>
          </div>
        </div>
      <?php echo \Form::close(); ?>
      <!-- EOF Subscribe Form -->
    </div>
  </div>
  <div class="container">
    <div class="pull-left">
      <nav class="menu-secondary">
        <ul>
          <li><a href="<?php echo \Uri::create('sitemap')?>">Sitemap</a></li>
          <li><a href="<?php echo \Uri::create('terms')?>">Terms of Use</a></li>
          <li><a href="<?php echo \Uri::create('privacy-policy')?>">Privacy Policy</a></li>
          <li><a href="<?php echo \Uri::create('warranty')?>">Warranty</a></li>
          <li><a <?php if($logged_type == 'user'): ?>href="<?php echo \Uri::create('referrals')?>"<?php else: ?>href="#" data-toggle="modal" data-target="#loginModal"<?php endif; ?> >Referrals</a></li>
        </ul>
      </nav>
      <div class="copyright clearfix">&copy; 2015  Edge Commerce All Rights Reserved.</div>
    </div>
    <div class="pull-right">
      <ul class="social clearfix">
        <li class="facebook">
          <a href="https://facebook.com" target="_blank"><i class="icon icon-facebook"></i></a>
        </li>
        <li class="twitter">
          <a href="https://twitter.com" target="_blank"><i class="icon icon-twitter"></i></a>
        </li>
        <li class="google">
          <a href="https://google.com" target="_blank"><i class="icon icon-google"></i></a>
        </li>
      </ul>
    </div>
  </div>
</footer>