
<div class="app-content">
    <div class="container">
        <div class="label-heading">
        <h3 class="label-title">Cart</h3>
        <p>These are the items you are purchasing on your checkout</p>
        </div>

        <div class="app-content pt-20">
        <div class="table-responsive">
            <table class="table table-bordered-l1" >
            <thead>
              <tr>
                <td>Product name</td>
                <td>Product code</td>
                <td width="15%">Unit price</td>
                <td width="15%">quantity</td>
                <td width="15%">total Price</td>
                <td></td>
              </tr>
            </thead>
            <!-- Display Items in the Cart -->
                <?php if(!empty($items)): ?>
                    <tbody>
                        <?php 
                            $cart_total = 0;
                            foreach ($items as $item): 

                            $price = $item->singlePrice(true);

                            // Calculate total price in cart
                            $total_price = $item->totalDiscountedPrice(true);
                            if(isset($product_data["price"]) && $product_data["price"] != 0){
                                $price = $product_data["price"];
                                $total_price = number_format($product_data["price"], 2)*$item->get('quantity');
                            }
                            $cart_total += $total_price;
                            // EOF Calculate total price in cart
                        ?>
                          <tr>
                            <td><?php echo $item->get('title'); ?></td>
                            <td><?php echo $item->get('product_code'); ?></td>
                            <td>$<?php echo number_format($price, 2); ?></td>
                            <td>
                                <?php echo \Form::open(array('action' => \Uri::create('order/cart/cart_update'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'edit_cart_form')); ?>
                                    <input type="hidden" value="<?php echo $item->get('minimum_order'); ?>" name="minimum_order[<?php echo $item->get('minimum_order'); ?>]">
                                    <input class="product-quantity" type="text" value="<?php echo $item->get('quantity'); ?>" name="quantity[<?php echo $item->uid(); ?>]">
                                    <div class="msg"></div>
                                    <div class="hold-loader"></div>
                                <?php echo \Form::close(); ?>
                                <div class="hold-message"></div>
                            </td>
                            <td class="price">$<?php echo number_format($total_price, 2); ?></td>
                            <td><a href="#" class="remove_cart_item" data-uid="<?php echo $item->uid();?>" ><i class="icon icon-close"></i></a></td>
                          </tr>
                          <?php if($item->get('packs')): ?>
                            <?php foreach($item->get('packs') as $pack): ?>
                                <tr>
                                    <td colspan="7" style="padding-left: 30px;">
                                        <?php 
                                            echo $pack['name'];
                                            if($pack['products'])
                                            {
                                                echo ' ( ';
                                                foreach ($pack['products'] as $key => $product)
                                                {
                                                    echo $product['name'];
                                                    if($key != (count($pack['products']) - 1))
                                                        echo ', ';
                                                }
                                                echo ' ) ';
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <td colspan="4">Total</td>
                        <td id="cart_grand_total">$<?php echo number_format($cart_total, 2); ?></td>
                        <td></td>
                      </tr>
                    </tfoot>
                <?php else: ?>
                  <tr>
                    <td colspan="4">Cart Empty</td>
                    <td></td>
                    <td></td>
                  </tr>
                <?php endif; ?>
            <!-- EOF Display Items in the Cart -->
            </table>
        </div>

        <!-- Action Buttons -->
            <?php if(!empty($items)): ?>
                <div class="btn-actions">
                    <a href="#" id="update_cart" class="btn btn-green btn-sm">Update Cart</a>
                    <a href="<?php echo \Uri::front_create('order/cart/clear_cart'); ?>" class="btn btn-green btn-sm">Clear Cart</a>
                    <div class="pull-right">
                        <a href="<?php echo \Uri::front_create('products'); ?>" class="btn btn-green btn-sm">Continue Shopping</a>
                        <a href="<?php echo \Uri::create('order/checkout'); ?>" class="btn btn-grey btn-sm">Place order now</a>
                        
                    </div>
                </div>
                <div class="clearfix"></div>
            <?php else: ?>
                <a href="<?php echo \Uri::create('products'); ?>" class="btn btn-green btn-sm">Shop Now</a>
            <?php endif; ?>        
        <!-- EOF Action Buttons -->
        </div>
    </div>
</div>