<?php \Config::load('order::order', 'order', true); ?>
<?php echo \Theme::instance()->asset->css('plugins/jquery-ui-1.12.1.css'); ?>
<div class="app-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="label-heading">
                    <h2 class="label-title">
                        Checkout
                    </h2>
                    <p>Please fill in the fields below and click Place order to complete your purchase! </p>
                </div>
            </div>
        </div>

        <div class="app-content">
         <?php echo \Form::open(array('action' => \Uri::create('order/checkout/cost'), 'data-validate' => 'parsley', 'method' => 'post')); ?>
         <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>
            <div class="note">
                <p class="note-title">Delivery Date and Time</p>
                <div class="row">
                    <div class="col-md-3">
                        <div style="position:relative">
                            <input type='text' class="form-control" placeholder="Date"  name="delivery_datetime" id="delivery-date" required/>
                        </div>
                        <input type="hidden" name="delivery_datetime_list">
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-12">
                                <select data-order="1" class="form-control additional-time-select" name="delivery_time_hourminute" style="width:110px;display:inline;margin-bottom:5px;">
                                    <?php
                                        foreach (\Config::get('order.time.hour_list') as $hour_key => $hour)
                                        {
                                            echo '<option value="'.$hour_key.'">'.$hour.'</option>';
                                        }
                                    ?>
                                </select>
                                <?php /*
                                    <select data-order="1" data-type="hour" class="form-control additional-time-select" name="delivery_timehour" style="width:90px;display:inline;margin-bottom:5px;">
                                        <?php
                                            foreach (\Config::get('order.time.hours') as $hour_key => $hour)
                                            {
                                                echo '<option value="'.$hour_key.'">'.$hour.'</option>';
                                            }
                                        ?>
                                    </select>
                                    <select data-order="1" data-type="minute" class="form-control additional-time-select" name="delivery_timeminute" style="width:90px;display:inline;margin-bottom:5px;">
                                        <?php
                                            foreach (\Config::get('order.time.minutes') as $minute_key => $minute)
                                            {
                                                echo '<option value="'.$minute_key.'">'.$minute.'</option>';
                                            }
                                        ?>
                                    </select>
                                */ ?>
                                <div id="hold-additional-time" style="display:inline;">
                                </div>
                                <a id="add-time-button" href="#" class="btn btn-green">Add Time</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-l3" id="order-table">
                <thead>
                    <tr>
                        <td data-orig-colspan="4" colspan="5">Order Review
                            <p>
                                These are the items you are purchasing on your checkout
                            </p>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="50%" class="product-name-header">Delivery Time</td>
                        <td width="50%">Product Name</td>
                        <td width="15%">Unit Price</td>
                        <td width="15%">Quantity</td>
                        <td width="15%">Total Price</td>
                    </tr>

                    <!-- Display Items in Cart -->
                    <?php if(!empty($items)): ?>
                        <?php 
                            $cart_total = 0;
                            $total_delivery_charge = 0;
                            $total_category_discount = 0;
                            foreach ($items as $item):
                                $product_data = null;
                                if($product = \Product\Model_Product::find_one_by_id($item->get('id')))
                                {
                                    $product_data = \Product\Model_Product::product_data($product, $item->get('attributes'));
                                    extract($product_data);
                                };

                                $price = $item->singlePrice(true);
                                $total_price = $item->totalDiscountedPrice(true);
                                if(isset($product_data["price"]) && $product_data["price"] != 0){
                                    $price = $product_data["price"];
                                    $total_price = $product_data["price"]*$item->get('quantity');
                                }
                                $cart_total += $total_price;
                                
                                if(isset($product_data["delivery_charge"]))
                                    $total_delivery_charge += floatval($product_data["delivery_charge"]);

                                $total_category_discount += \Discountcode\Model_Discountcode::getCategoryDiscount($item->get('id'), $total_price);
                        ?>
                            <tr>
                                <td width="50%" class="product-name-td" data-productid="<?php echo $item->get('id'); ?>"></td>
                                <td data-productid="<?php echo $item->get('id'); ?>"><?php echo $item->get('title'); ?></td>
                                <td>$<?php echo number_format($price, 2); ?></td>
                                <td><?php echo $item->get('quantity'); ?></td>
                                <td>$<?php echo number_format($total_price, 2); ?></td>
                            </tr>
                              <?php if($item->get('packs')): ?>
                                <?php foreach($item->get('packs') as $pack): ?>
                                    <tr>
                                        <td width="50%" class="product-name-td" data-productid="<?php echo $item->get('id'); ?>-<?php echo $pack['id']; ?>"></td>
                                        <td data-orig-colspan="4" colspan="5" style="padding-left: 30px;">
                                            <?php 
                                                echo $pack['name'];
                                                if($pack['products'])
                                                {
                                                    echo ' ( ';
                                                    foreach ($pack['products'] as $key => $product)
                                                    {
                                                        echo $product['name'];
                                                        if($key != (count($pack['products']) - 1))
                                                            echo ', ';
                                                    }
                                                    echo ' ) ';
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                              <?php endif; ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td valign="middle" data-orig-colspan="4" colspan="5">Your cart is empty</td>
                        </tr>
                    <?php endif; ?>
                    <!-- EOF Display Items in Cart -->

                    <?php $total = \Helper::total_price(\Cart::getDiscountedTotal('price'), $shipping_price); ?>
                    <?php
                        $final_gst = 0;
                        $sub_total = $cart_total + $shipping_price + $total_delivery_charge - $total_category_discount;

                        $hold_gst = (isset($settings['gst']) ? 1 : 0);
                        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
                    ?>
                   
                    <tr>
                        <td></td>
                        <td align="right" data-orig-colspan="2" colspan="3">Products Total</td>
                        <td><span >AU</span> $<?php echo number_format($cart_total, 2); ?></td>
                    </tr>
                    <?php if(!empty($items)): ?>
                        <tr>
                            <td></td>
                            <td align="right" data-orig-colspan="2" colspan="3">Shipping</td>
                            <td><span class="t-md">AU </span>$<span class="shipping_price"><?php echo number_format($shipping_price, 2); ?></span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="right" data-orig-colspan="2" colspan="3">Extra Delivery Charge</td>
                            <td><span class="t-md">AU </span>$<span class="delivery_charge"><?php echo number_format($total_delivery_charge, 2)?></span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="right" data-orig-colspan="2" colspan="3">Discount</td>
                            <td><span class="t-md">-AU </span>$<span class="txt-discount" data-categoryDiscount="<?php echo $total_category_discount; ?>"><?php echo number_format($total_category_discount, 2); ?></span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td align="right" data-orig-colspan="2" colspan="3"><strong>Grand Total</strong></td>
                            <td><span class="t-md">AU </span>$<span class="txt-total"><?php echo number_format($sub_total, 2); ?></span></td>
                        </tr>
                        <?php if($hold_gst && $hold_gst_percentage > 0): ?>
                            <?php $final_gst = \Helper::calculateGST($sub_total, $hold_gst_percentage); ?>
                            <tr>
                                <td></td>
                                <td align="right" data-orig-colspan="2" colspan="3">GST Tax <?php echo $hold_gst_percentage; ?>% (Included)</td>
                                <td><span class="t-md">AU </span>$<span class="gst_amount"><?php echo number_format($final_gst, 2); ?></span></td>
                            </tr>
                        <?php endif;?>
                    <?php endif; ?>
                </tbody>
            </table>
            </div>

            <div class="note">
                <p class="note-title">Shipping Method</p>
                <div class="form-group">
                    <div class="radio-inline">  
                      <div class="radio radio-box radio-icon">
                        <label>
                          <input type="radio" name="delivery" value="delivery" checked="checked"/>
                          <span></span>
                          <span class="icon icon-truck"></span>
                          <span class="text">Delivery</span>
                        </label>
                      </div> <!--  .radio -->
                    </div> <!--  .radio-group -->
                    <div class="radio-inline">  
                      <div class="radio radio-box radio-icon">
                        <label>
                          <input type="radio" name="delivery" value="pickup" />
                          <span></span>
                          <span class="icon icon-shop"></span>
                          <span class="text">Store Pickup</span>
                        </label>
                      </div> <!--  .radio -->
                    </div> <!--  .radio-group -->
                </div>
            </div>

            <!-- Coupon Code -->
            <div class="note">
                <p class="note-title">If you have a coupon code, you may apply it here</p>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <?php echo \Form::input('code', '', array('class' => 'form-control')); ?>
                            <?php echo \Form::hidden('discount_amount', $total_category_discount); ?>
                            <?php echo \Form::hidden('gst_amount', $final_gst); ?>
                            <?php echo \Form::hidden('total_price', isset($cart_total) ? $cart_total: 0); ?>
                            <?php echo \Form::hidden('total_delivery_charge', isset($total_delivery_charge) ? $total_delivery_charge: 0); ?>
                            <?php echo \Form::hidden('grand_total', $cart_total + $shipping_price + $total_delivery_charge) ?>
                            <?php echo \Form::hidden('action', 'apply'); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <a class="toggle-discount btn btn-stroked btn-block"><span class="lbl-toggle-discount">APPLY</span></a>
                        </div>
                    </div>
                    <div class='div-discount-error' style="display:none">
                        Please enter a valid discount code
                    </div>
                </div>
            </div>
            <!-- EOF Coupon Code -->

            <?php
                \Config::load('order::order', 'order', true);
                if($payment_list = \Config::get('order.dynamic_payment_method')):
            ?>
                <!-- Choose payment option -->
                <div class="note">
                    <p class="note-title">Choose payment option</p>
                    <div class="form-group">
                        <?php
                            $counter = 0;
                            foreach($payment_list as $key => $payment):
                        ?>
                            <div class="radio-inline">  
                                <div class="radio radio-box radio-icon">
                                    <label>
                                        <input type="radio" name="payment_type" value="<?php echo $key; ?>" <?php echo $counter ==0?'checked="checked"':''; ?> >
                                        <span></span>
                                        <span class="fa fa-<?php echo $key=='paypal'?'paypal':'credit-card'; ?>"></span>
                                        <span class="text"><?php echo $payment; ?></span>
                                    </label>
                                </div> <!--  .radio -->
                            </div>
                        <?php
                            $counter++;
                            endforeach;
                        ?>
                    </div>
                    <div id="credit-card-section">
                    </div>
                </div>
                <!-- EOF Choose payment option -->
            <?php endif; ?>

            <!-- Action Buttons -->
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="checkbox">
                        <label><input type="checkbox" checked="checked" required name='agree' value='1'> <span></span>I read and agree to <a href="/terms" target="_blank">Terms and Conditions</a></label>
                    </div>
                    </div>
                    <div class="col-sm-6">
                        <p class="mb-0 text-right">Forgot an item? <a href="/order/cart/edit_cart">Edit your Cart</a></p>
                    </div>
                </div>                          
            </div>
            <button id="place-order-button" type="submit" class="btn btn-green">Place order</button>
            <!-- EOF Action Buttons -->
             <?php echo \Form::close(); ?>
        </div>
    </div>
</div>
<?php
    echo \Theme::instance()->asset->js('payment/validation.js');
    echo \Theme::instance()->asset->js('plugins/moment.min-2.18.0.js');
    echo \Theme::instance()->asset->js('plugins/jquery-ui-1.12.1.js');
?>
<script type="text/javascript">
    $(document).ready(function(){

        // Clear discount when page reload
        $.ajax({
            type: 'POST',
            url: '/order/checkout/clear_discount',
            success: function(data) {
                console.log('discount clear');
            }
        });

        // Discount
        $('.toggle-discount').click(function(){
            $('.div-discount-error').hide();
            $.ajax({
                type: 'POST',
                url: '/order/checkout/get_discount',
                data: {
                    'action':           $('#form_action').val(),
                    'discount_code':    $('#form_code').val(), 
                    'total_price':      $('#form_total_price').val(),  
                    'total_delivery_charge':      $('#form_total_delivery_charge').val(),  
                    'method':           $('input[name="delivery"]:checked').val(),
                    'gst_price':        $('.gst_amount').text(),
                },
                success: function(data) {
                    $('.row-discount').hide();
                    data = jQuery.parseJSON(data);
                    if(data.grand_total)
                        $('#form_grand_total').val(data.grand_total);
                    var discount = 0;
                    var grand_total = parseFloat($('#form_grand_total').val());
                    if (data.success)
                    {
                        // discount code was removed
                        if (data.btn_label == 'apply')
                        {
                            $('#form_code').val('');
                        }
                        // discount code was applied
                        else
                        {
                            discount = parseFloat(data.discount_amount);
                            $('.row-discount').show();
                        }
                        $('#form_action').val(data.btn_label);
                        $('.lbl-toggle-discount').html(data.btn_label);
                    }
                    else
                    {
                        // error in retrieving discount code
                        $('#form_code').focus();
                        $('.div-discount-error').show();
                    }
                    discount += parseFloat($('.txt-discount').attr('data-categorydiscount'));

                    if (discount)
                    {
                        grand_total -= discount.toFixed(2);
                    }

                    $('#form_discount_amount').val(discount);
                    $('.txt-discount').html(discount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    $('.txt-total').html(grand_total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
                    $('.gst_amount').html(parseFloat(data.gst_price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

                    $('#form_grand_total').val(grand_total);
                    $('#form_gst_amount').val(data.gst_price);
                }
            });
        });
        
        //Delivery
        $('input[name="delivery"]').click(function(){
            $.ajax({
                type: 'POST',
                url: '/order/checkout/shipment_price',
                data: {
                    'method':               $(this).val(),
                    'discount':             $('.txt-discount').text(), 
                    'discount_code':        $('#form_code').val(), 
                    'action':               $('#form_action').val(),
                    'total_price':          $('#form_total_price').val(),  
                    'total_delivery_charge':    $('#form_total_delivery_charge').val(),  
                },
                success: function(data) {
                    data = jQuery.parseJSON(data);
                    $('.shipping_price').html(data.shipping_price.toFixed(2));
                    $('.txt-total').html(data.grand_total);
                    $('#form_grand_total').val(data.grand_total);
                    $('.gst_amount').html(data.gst_price);
                    $('#form_total_delivery_charge').val(data.delivery_charge);
                    $('#form_gst_amount').val(data.gst_price);
                    
                    if(data.check_discount_shipping)
                        $('.txt-discount').html(data.discount.toFixed(2));
                }
            });
        });
        
        $("#delivery-date").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: new Date(),
            showOn: "button",
            buttonImage: baseUrl + "themes/frontend/images/icon-calendar.png",
            buttonImageOnly: true,
        });

        if($('input[name="payment_type"]').length)
        {
            payment_form();
            $('input[name="payment_type"]').change(function(){
                payment_form();
            });
        }

        function payment_form()
        {
            var html = '';
            var monthNames = ["January", "February", "March", "April", "May", "June",
              "July", "August", "September", "October", "November", "December"
            ];
            var option;
            var option2;

            $.each(monthNames,function(i,obj){
                option += '<option value="'+(i+1)+'">'+obj+'</option>';
            });

            var currentYear = new Date().getFullYear();
            for(var x = currentYear; x < (currentYear + 30); x++){
                option2 += '<option value="'+x+'">'+x+'</option>';
            }

            if($('input[name="payment_type"]:checked').val() != 'paypal')
            {
                html += '<div class="row">'+
                        '<div class="col-sm-12">'+
                            '<div class="form-group">'+
                                '<div class="row">'+
                                    '<label class="col-sm-4 control-label">Card Number <span class="text-danger">*</span></label>'+
                                    '<div class="col-sm-8">'+
                                        '<input class="form-control" name="ccnumber" required autocomplete="off">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<div class="row">'+
                                    '<label class="col-sm-4 control-label">Expiration Date (MO / YEAR) <span class="text-danger">*</span></label>'+
                                    '<div class="col-sm-8" style="padding:0;">'+
                                        '<div class="col-sm-6">'+
                                            '<select name="exmonth" class="form-control" autocomplete="off">' + option + '</select>'+
                                        '</div>'+
                                        '<div class="col-sm-6">'+
                                            '<select name="exyear" class="form-control" autocomplete="off">' + option2 + '</select>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            '<div class="form-group">'+
                                '<div class="row">'+
                                    '<label class="col-sm-4 control-label">CVV <span class="text-danger">*</span></label>'+
                                    '<div class="col-sm-8">'+
                                        '<input class="form-control" name="cvv" required maxlength="4" autocomplete="off">'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>';
            }
            $('#credit-card-section').html(html);
        }

        var time_list = ["Time 1"];
        var get_hourtime_options = '';
        $('select[name="delivery_time_hourminute"]').find('option').each(function(index, elem){
            get_hourtime_options += $(elem).get(0).outerHTML;
        });
        // var get_hour_options = '';
        // $('select[name="delivery_timehour"]').find('option').each(function(index, elem){
        //     get_hour_options += $(elem).get(0).outerHTML;
        // });
        // var get_minute_options = '';
        // $('select[name="delivery_timeminute"]').find('option').each(function(index, elem){
        //     get_minute_options += $(elem).get(0).outerHTML;
        // });
        var countTime = 1;
        generate_time_in_product_name();
        $("#add-time-button").click(function(e){
            e.preventDefault();

            if(countTime == 1)
            {
                if($('select[name="delivery_time_hourminute"]').val() != '-')
                    time_list.push($('select[name="delivery_time_hourminute"]').val());
                else
                    time_list.push('Time '+countTime);
            }

            countTime += 1;

            var select_time_hourminute = '<select data-order="'+countTime+'" class="form-control additional-time-select" style="width:110px;display:inline;margin-left:5px;margin-bottom:5px;">';
            select_time_hourminute += get_hourtime_options;
            select_time_hourminute += '</select>';

            // var select_hour = '<select data-order="'+countTime+'" data-type="hour" class="form-control additional-time-select" style="width:90px;display:inline;margin-left:5px;margin-bottom:5px;">';
            // select_hour += get_hour_options;
            // select_hour += '</select>';

            // var select_minute = '<select data-order="'+countTime+'" data-type="minute" class="form-control additional-time-select" style="width:90px;display:inline;margin-left:5px;margin-right:5px;margin-bottom:5px;">';
            // select_minute += get_minute_options;
            // select_minute += '</select>';

            var time = 'Time '+countTime;
            $('#hold-additional-time').append('<div data-order="'+countTime+'" class="additional-time-group" style="display:inline-block;"><label style="margin-left: 10px;">and</label>'+select_time_hourminute+' <a class="remove-time" href="#"><i class="fa fa-close"></i></a></div>');
            time_list.push(time);
            generate_time_in_product_name();
            generate_delivery_datetime_list();
        });
        $( "div" ).on( "click", "a.remove-time", function(e) {
            e.preventDefault();
            var getOrder = $(this).closest('div.additional-time-group').data('order');
            time_list.splice((getOrder-1), 1);
            $(this).closest('div.additional-time-group').remove();
            $('select[name*="product_time"]').each(function(index, elem){
                $(elem).find('option').eq(getOrder-1).remove();
            });
            countTime -= 1;
            var count = 2;
            $('#hold-additional-time').find('div.additional-time-group').each(function(index, elem){
                $(elem).attr('data-order', count);
                $(elem).find('select.additional-time-select').each(function(ind, ele){
                    $(ele).attr('data-order', count);
                });
                count += 1;
            });
            generate_delivery_datetime_list();

            $('.product-name-td select').each(function(index, elem) {
                $(elem).find('option').each(function(ind, ele){
                    
                    if($(ele).attr('value').indexOf(':') < 0)
                    {
                        var getOrderOpt = ind+1;
                        $(ele).attr('value', 'Time '+getOrderOpt);
                        $(ele).html('Time '+getOrderOpt);
                    }
                });
            });
        });

        $( "div" ).on( "change", "select.additional-time-select", function(e) {
            var getOrder = $(this).data('order');
            if(time_list.length)
            {
                if($('select[data-order="'+getOrder+'"].additional-time-select').val() != '-')
                {
                    var time = $('select[data-order="'+getOrder+'"].additional-time-select').val();
                    time_list[getOrder-1] = time;
                    $(this).closest('div').find('a').attr('data-time', time);

                    var time_format = $('select[data-order="'+getOrder+'"].additional-time-select option:selected').text();
                    
                    $('.product-name-td').each(function(index, elem) {
                        $(elem).find('select option').eq(getOrder-1).attr('value', time);
                        $(elem).find('select option').eq(getOrder-1).html(time_format);
                    });
                }
                else
                {
                    time_list[getOrder-1] = 'Time '+getOrder;
                    $('.product-name-td').each(function(index, elem) {
                        $(elem).find('select option').eq(getOrder-1).attr('value', 'Time '+getOrder);
                        $(elem).find('select option').eq(getOrder-1).html('Time '+getOrder);
                    });
                }
            }
            generate_delivery_datetime_list();
        });
        $( "td" ).on( "change", 'select[name*="product_time"]', function(e) {
            generate_delivery_datetime_list();
        });

        function generate_time_in_product_name()
        {
            if($('.product-name-td').find('select').length == 0)
            {
                var options = '';
                for(var i = 0; i < time_list.length; i++)
                {
                    options += '<option value="'+time_list[i]+'">'+time_list[i]+'</option>';
                }
                if(options != '')
                {
                    $('.product-name-td').each(function(index, elem) {
                        var select = '<select class="form-control" style="width:110px;" name="product_time['+$(elem).data('productid')+']">';
                        select += options;
                        select += '</select>';
                        $(elem).find('select').remove();
                        $(elem).prepend(select);
                    });
                }
            }
            else
            {
                var getLast = time_list[time_list.length-1];
                var options = '<option value="'+getLast+'">'+getLast+'</option>';
                $('.product-name-td').find('select').append(options);
            }
            $('.product-name-header').show();
            $('.product-name-td').show();
            $('#order-table td[colspan]').each(function(index, elem){
                var colspan = parseInt($(elem).attr('data-orig-colspan')) + 1;
                $(elem).attr('colspan', colspan);
            });
        }

        function generate_delivery_datetime_list()
        {
            if($('select[name*="product_time"]').length)
            {
                var datetime_list = '';
                $('select[name*="product_time"]').each(function(index, elem){
                    
                    if($(elem).val().indexOf(':') > -1)
                    {
                        if(datetime_list.indexOf($(elem).val()) < 0)
                            datetime_list += $(elem).val()+';';
                    }
                });
                datetime_list = datetime_list.substring(0, datetime_list.length - 1);
                $('input[name="delivery_datetime_list"]').val(datetime_list);
            }
        }

        // validate form
        $('#place-order-button').click(function(e){
            e.preventDefault();

            var check_product_time = true;

            $('select[name*="product_time"]').each(function(index, elem){
                if($(elem).val().indexOf(':') < 0)
                {
                    check_product_time = false;
                    return false;
                }
            });

            if($('input[name="ccnumber"]').length && $('select[name="exmonth"]').length && $('select[name="exyear"]').length && $('input[name="cvv"]').length)
            {
                if(!$('input[name="ccnumber"]').val() || !$('select[name="exmonth"]').val() || !$('select[name="exyear"]').val() || !$('input[name="cvv"]').val())
                    alert("Please complete payment details.");
                else
                {
                    if(!$('input[name="agree"]:checked').val())
                    {
                        alert("Please accept terms and condition.");
                    }
                    else
                        $(this).closest('form').submit();
                }
            }
            else
            {
                if(!$('input[name="agree"]:checked').val())
                {
                    alert("Please accept terms and condition.");
                }
                else
                    $(this).closest('form').submit();
            }
        });
    });
</script>
<!-- EOF Checkout Page