<!-- Order Complete -->
<div class="app-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="label-heading">
                    <h2 class="label-title">Order Complete</h2>
                </div>
            </div>
        </div>

        <div class="app-content">
            <div class="row">
            <div class="col-sm-4">
                <div class="l-aside">
                     <?php if(isset($order[0])): ?>
                         <p>
                            <b>Order Info</b><br>
                            Order Number: <?php echo $order[0]->id; ?><br>
                            Order Date: <?php echo date('d/m/Y h:i:s', $order[0]->created_at); ?><br>
                            Delivery: <?php echo $order[0]->delivery_datetime?(date('d/m/Y', strtotime($order[0]->delivery_datetime)).' '.date('h:i a', strtotime($order[0]->delivery_datetime))):''; ?> <?php echo $order[0]->delivery_datetime_list?(count(explode(';', $order[0]->delivery_datetime_list))>1?'+':''):''; ?><br>
                        </p>
                    <?php endif; ?>
                    <ul class="list-details">
                        <li>
                             <?php if($logged_type == 'user'): ?>
                                <a href="<?php echo \Uri::front_create('user/account/orders'); ?>" >
                                    <span>My Orders</span>
                                </a>
                            <?php endif; ?>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-8">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title">Thanks for your order!</h3>
                  </div>
                  <div class="panel-body">
                    <h3 class="panel-body-title">Hi <span><?php echo $user->get('metadata.first_name') . ' ' . $user->get('metadata.last_name') ?></span></h3>
                    <div class="copy">
                        <p>
                        Please note your Order Confirmation Number (below).
                        </p>
                        <p>
                            You will receive an email shortly confirming you order details.
                            If you have created an account with <?php $settings = \Config::load('autoresponder.db'); echo $settings['website']; ?> you can track your order at any time by logging in and cliking My Orders.
                            If you have any questions about your order please  <a href="<?php echo \Uri::front_create('contact-us'); ?>">contact us</a>.
                        </p>
                        <p>Thanks for shopping with us, we look forward to seeing you online again soon!</p>
                    </div>
                  </div>
                </div>
                <?php if(isset($order[0])): ?>
                    <table class="table table-l3">
                        <thead>
                            <tr>
                                <td colspan="7">Order</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Product Code</td>
                                <td>Item</td>
                                <td>Description</td>
                                <td>Quantity</td>
                                <td>Delivery Time</td>
                                <td>Unit Price</td>
                                <td>Price</td>
                            </tr>
                            <?php
                                $items = \Order\Model_Products::find(array('where' => array('order_id' => $order[0]->id)));
                                if($items):
                            ?>
                                    <?php foreach($items as $item): ?>
                                    <tr>
                                        <td><?php echo $item->code; ?></td>
                                        <td><?php echo $item->title; ?></td>
                                        <td><?php echo $item->description; ?></td>
                                        <td><?php echo $item->quantity; ?></td>
                                        <td><?php echo $order[0]->delivery_datetime_list?(date('h:i a', strtotime($item->delivery_time))):($order[0]->delivery_datetime?(date('h:i a', strtotime($order[0]->delivery_datetime))):''); ?></td>
                                        <td>$<?php echo number_format($item->price, 2); ?></td>
                                        <td>$<?php echo number_format($item->subtotal, 2); ?></td>
                                    </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php
                                $settings = \Config::load('autoresponder.db');
                                $hold_gst = (isset($settings['gst']) ? 1 : 0);
                                $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
                            ?>

                            <tr>
                                <td colspan="5"></td>
                                <td>Products Total</td>
                                <td>$<?php echo number_format($order[0]->total_price, 2); ?></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td>Shipping</td>
                                <td>$<?php echo number_format($order[0]->shipping_price, 2); ?></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td>Extra Delivery Charge</td>
                                <td>$<?php echo number_format($order[0]->total_delivery_charge, 2); ?></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td>Discount</td>
                                <td>-$<?php echo number_format($order[0]->discount_amount, 2); ?></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td><strong>Order Total</strong></td>
                                <td>$<?php echo number_format($order[0]->total_price(), 2); ?></td>
                            </tr>
                            <?php if($hold_gst && $hold_gst_percentage > 0): ?>
                                <tr>
                                    <td colspan="5"></td>
                                    <td>GST Tax <?php echo $hold_gst_percentage; ?>% (Included)</td>
                                    <td>$<?php echo number_format(\Helper::calculateGST($order[0]->total_price(), $hold_gst_percentage), 2); ?></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
        </div>
        </div>
    </div>
</div>
<!-- EOF Order Complete -->