<?php 
if(!isset($edit)):
    $edit = $user = $metadata = null;
else:
    if(\Input::post()) echo \Messages::display();
endif;
?>

<!-- Display Billing Details - Partial view -->
<div class="row password">
  <div class="col-sm-6">
    <div class="form-group">
      <label class="form-label">First Name *</label>
      <?php echo \Form::input('first_name', \Input::post('first_name', !$edit ? '': $metadata['first_name']), array("class" => "form-control required", 'data-trigger' => 'change', 'tabindex' => '3')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Last Name *</label>
      <?php echo \Form::input('last_name', \Input::post('last_name', !$edit ? '': $metadata['last_name']), array("class" => "form-control required", 'data-trigger' => 'change', 'tabindex' => '4')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Company Name</label>
      <?php echo \Form::input('business_name', \Input::post('business_name', !$edit ? '': $metadata['business_name']), array("class" => "form-control", 'data-trigger' => 'change', 'tabindex' => '1')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Email Address *</label>
      <?php echo \Form::input('email', \Input::post('email', !$edit ? '': $user['email']), array("class" => "form-control required", 'data-type' => 'email', 'data-trigger' => 'change', 'tabindex' => '8')); ?>
    </div>
    <div class="holder">
      <div class="form-group">
        <label class="form-label">Password *</label>
        <?php
            $validation = array();

            $validation['class'] = 'required form-control'; 
            $validation['data-minlength'] = '6'; 
            $validation['data-trigger'] = 'change'; 
        ?>
        <?php echo \Form::password('password', \Input::post('password'), $validation); ?>
      </div>
      <div class="form-group">
        <label class="form-label">Retype Password *</label>

        <?php
            $validation = array();

            $validation['class'] = 'required form-control'; 
            $validation['data-equalto'] = '#form_password'; 
            $validation['data-trigger'] = 'change'; 
        ?>
        <?php echo \Form::password('confirm_password', \Input::post('password_confirm'), $validation); ?>
      </div>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label class="form-label">Address *</label>
      <?php echo \Form::input('address', \Input::post('address', !$edit ? '': $metadata['address']), array( 'class' => 'form-control required', 'tabindex' => '10')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Address 2</label>
      <?php echo \Form::input('address2', \Input::post('address2', !$edit ? '': $metadata['address2']), array( 'class' => 'form-control', 'tabindex' => '10')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Suburb *</label>
      <?php echo \Form::input('suburb', \Input::post('suburb', !$edit ? '': $metadata['suburb']), array( 'class' => 'form-control required', 'tabindex' => '11')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">State *</label>
      <?php echo \Form::select('state', \Input::post('state', !$edit ? '': $metadata['state']), \Config::get('user.states', array()), array('class' => 'form-control required select_init w_state_popup', 'tabindex' => '12')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">PostCode *</label>
      <?php echo \Form::input('postcode', \Input::post('postcode', !$edit ? '': $metadata['postcode']), array( 'class' => 'form-control required', 'tabindex' => '13')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Phone *</label>
      <?php echo \Form::input('phone', \Input::post('phone', !$edit ? '': $metadata['phone']), array("class" => "form-control required", 'data-trigger' => 'change', 'tabindex' => '14')); ?>
    </div>
  </div>
</div>
<!-- EOF Display Billing Details - Partial view -->