<!-- Edit Details Layout -->
<div class="cart_heading text-center">
    <h2 class="modal-title">
        <span class="edit_cart left" style="margin-top: -2px; height: 15px;"></span><?php echo $subtitle; ?>
        <a href="#" class="close_cart_link close_rounded right"></a>
    </h2>
</div>
<div class="content_wrapper gradient forms_gradient">
    <div class="cart_content">
        
        <div class="cart_message_box" style="display: none;"></div>
         
        <?php echo \Form::open(array('action' => \Uri::front('current'), 'data-validate' => 'parsley', 'id' => 'edit_address_form')); ?>
        
            <?php 
                echo $content;
            ?>

            <div class="row">
                <div class="col-sm-12">
                    <a href="#" class="btn btn-bordered btn-block m-30" id="edit_address_submit" data-type="<?php echo $type; ?>"><span>Update</span></a>
                </div>
            </div>
        
        <?php echo \Form::close(); ?>
    </div>
</div>
<!-- EOF Edit Details Layout -->