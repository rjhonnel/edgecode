<?php 
if(!isset($edit)):
    $edit = $user = $metadata = null;
else:
    if(\Input::post()) echo \Messages::display();
endif;
?>

<!-- Display Shipping Details - Partial view -->
<div class="row">
  <div class="col-sm-6">
    <div class="form-group">
      <label class="form-label">Salutation</label>
      <?php echo \Form::input('shipping_title', \Input::post('shipping_title', !$edit ? '': $metadata['shipping_title']), array("class" => "form-control", 'tabindex' => '21')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">First Name *</label>
      <?php echo \Form::input('shipping_first_name', \Input::post('shipping_first_name', !$edit ? '': $metadata['shipping_first_name']), array("class" => "form-control required", 'data-trigger' => 'change', 'tabindex' => '22')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Last Name *</label>
      <?php echo \Form::input('shipping_last_name', \Input::post('shipping_last_name', !$edit ? '': $metadata['shipping_last_name']), array("class" => "form-control required", 'data-trigger' => 'change', 'tabindex' => '23')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Job Title</label>
      <?php echo \Form::input('shipping_job_title', \Input::post('shipping_job_title', !$edit ? '': $metadata['shipping_job_title']), array("class" => "form-control", 'data-trigger' => 'change', 'tabindex' => '24')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Department</label>
      <?php echo \Form::input('shipping_department', \Input::post('shipping_department', !$edit ? '': $metadata['shipping_department']), array("class" => "form-control", "placeholder" => "", 'tabindex' => '25')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Department Code</label>
      <?php echo \Form::input('shipping_department_code', \Input::post('shipping_department_code', !$edit ? '': $metadata['shipping_department_code']), array("class" => "form-control", "placeholder" => "", 'tabindex' => '26')); ?>	
    </div>
    <div class="form-group">
      <label class="form-label">Address *</label>
      <?php echo \Form::input('shipping_address', \Input::post('shipping_address', !$edit ? '': $metadata['shipping_address']), array( 'class' => 'form-control required', 'tabindex' => '10')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Address 2</label>
      <?php echo \Form::input('shipping_address2', \Input::post('shipping_address2', !$edit ? '': $metadata['shipping_address2']), array( 'class' => 'form-control', 'tabindex' => '10')); ?>
    </div>
  </div>
  <div class="col-sm-6">
    <div class="form-group">
      <label class="form-label">Suburb *</label>
      <?php echo \Form::input('shipping_suburb', \Input::post('shipping_suburb', !$edit ? '': $metadata['shipping_suburb']), array('class' => 'form-control required', 'tabindex' => '28')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">State *</label>
      <?php echo \Form::select('shipping_state', \Input::post('shipping_state', !$edit ? '': $metadata['shipping_state']), \Config::get('user.states', array()), array('class' => 'form-control required select_init w_state_popup', 'tabindex' => '29')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">PostCode *</label>
      <?php echo \Form::input('shipping_postcode', \Input::post('shipping_postcode', !$edit ? '': $metadata['shipping_postcode']), array('class' => 'form-control required', 'tabindex' => '30')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Phone *</label>
      <?php echo \Form::input('shipping_phone', \Input::post('shipping_phone', !$edit ? '': $metadata['shipping_phone']), array("class" => "form-control required", 'data-trigger' => 'change', 'tabindex' => '31')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Direct Phone</label>
      <?php echo \Form::input('shipping_direct_phone', \Input::post('shipping_direct_phone', !$edit ? '': $metadata['shipping_direct_phone']), array("class" => "form-control", 'tabindex' => '32')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Mobile</label>
      <?php echo \Form::input('shipping_mobile', \Input::post('shipping_mobile', !$edit ? '': $metadata['shipping_mobile']), array("class" => "form-control", 'tabindex' => '33')); ?>
    </div>
    <div class="form-group">
      <label class="form-label">Fax</label>
      <?php echo \Form::input('shipping_fax', \Input::post('shipping_fax', !$edit ? '': $metadata['shipping_fax']), array("class" => "form-control", 'tabindex' => '34')); ?>
    </div>
  </div>
</div>
<!-- EOF Display Shipping Details - Partial view -->