  <!-- Step before checkout when user is a visitor -->
  <script>
      
      // Show / Hide password fields
      function password_holder()
      {
          var element = $('input[name=create_account]:checked');
          var fields = element.parents('.form-group').first().next('.password').find('.holder');
          var value = element.val();
          
          if(value == 1) fields.show();
          else fields.hide();
      }
      
      // Show / Hide shipping address fields
      function address_holder()
      {
          var element = $('input[name=same_address]:checked');
          var fields = element.parents('.form-group').first().next('.holder');
          var value = element.val();
          
          if(value == 1) fields.hide();
          else fields.show();
      }
      
      $(document).ready(function(){
          
          $('input[name=create_account]').click(function(){
              password_holder();
          });
          $('input[name=same_address]').click(function(){
              address_holder();
          });
          
          password_holder();
          address_holder();
          
          // Override default Parsley behaviour
          // In order not to validate hidden fields
          /*$('#address_form').parsley('addListener', {
              onFieldValidate: function(elem) {

                  // if field is not visible, do not apply Parsley validation!
                  if (!$(elem).is(':visible')) {
                      return true;
                  }

                  return false;
              }
          });*/
      });
  </script>
  <div class="app-content">
    <div class="container">
      <div class="checkout-fieldset">
        <div class="row">
          <div class="col-md-6">
            <h2 class="checkout-title">One Step Checkout</h2>
            <p>Please fill in the fields below and click proceed to complete your purchase</p>
          </div>
          <div class="col-md-6">
            <div class="checkout-login-section">
              Already have an account?

              <a href="<?php echo \Uri::create('user/login'); ?>" class="btn btn-green">Click here to login</a>
            </div>
          </div>
        </div>
        <div class="checkout-information">
          <h3>Your Details</h3>
          <p>Please provide your billing information.</p>
          <?php echo \Form::open(array('action' => \Uri::front('current'), 'data-validate' => 'parsley', 'id' => 'address_form'), array('signup' => 1, 'user_group' => 4)); ?>
            <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>

           <div class="form-group">
              <label for="" class="mr-15px"><?php echo \Form::radio('create_account', 1,\Input::post('create_account', 1) == 1 ? true : false); ?> Create Account</label>
              <label for=""><?php echo \Form::radio('create_account', 0, \Input::post('create_account') == 0 ? true : false); ?> Checkout as Guest</label>
           </div>

            <?php echo $theme->view('views/order/checkout/_billing_address'); ?>

           <button type="submit" name="details" value="1" class="btn btn-green">Proceed</button>
          <?php echo \Form::close(); ?>
        </div>
      </div>
    </div>
  </div>
  <!-- EOF Step before checkout when user is a visitor -->
