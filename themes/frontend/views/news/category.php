<div class="section-grey">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo \Uri::front_create('/'); ?>">Home</a></li>
      <li>></li>
      <li><a href="<?php echo \Uri::front_create('/news'); ?>">News</a></li>
    </ul>
  </div>
</div>

<div class="app-content">
  <div class="container">
  <!-- check if it has child -->
  <?php if($children): ?>
    <div class="category-products">
      <div class="row">
        <?php foreach($children as $child ): ?>
          <div class="col-md-4 col-sm-6">
            <div class="product-item">
              <a href="<?php echo \Uri::create('news/'.$child->seo->slug) ?>">
                <div class="product-img">
                  <?php
                    if(isset($child->images[0]->image)):
                      echo \Html::img(\Helper::amazonFileURL( \Config::get('media_base_url') . $child->images[0]->image, array('alt' => $child->images[0]->alt_text))); 
                    else:
                  ?>
                    <div class="no-image">
                      <i class="icon icon-image2"></i>
                      <h4>No image</h4>
                    </div>
                  <?php endif; ?>
                </div>
                <span class="product-display">
                  <span class="product-content">
                    <h3 class="product-title" title="<?php echo $child->title; ?>"><?php echo $child->title; ?></h3>
                  </span>
                </span>
              </a>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  <?php endif;?>  
 </div>
</div>  