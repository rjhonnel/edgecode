<div class="section-grey">
  <div class="container">
    <ul class="breadcrumb">
      <li><a href="<?php echo \Uri::front_create('/'); ?>">Home</a></li>
      <li>></li>
      <li><a href="<?php echo \Uri::front_create('/products'); ?>">News</a></li>
      <?php if($category_parents): $count = 1; ?>
        <li>></li>
        <?php foreach($category_parents as $parent): ?>
          <li><a href="<?php echo \Uri::create('news/'.$parent->seo->slug); ?>"><?php echo $parent->title; ?></a></li>
          <?php if(count($category_parents) != $count ): ?><li>></li><?php endif; ?>
        <?php $count++; endforeach; ?>
      <?php endif; ?>
    </ul>
  </div>
</div>
<div class="banner-slim banner-about">
  <div class="container">
    <div class="banner-heading">
      <h2 class="banner-title"><?php echo $item->title; ?></h2>
    </div>
  </div>
</div>

<div class="section skin-white-lighter">
  <div class="container">
    <div class="section-heading">
      <a href="<?php echo \Uri::create( 'news'); ?>" class="btn btn-default">Back to News</a>
    </div>

  	<div class="row copy-blog">
      <div class="col-md-4">
        <div class="border-box">
          <?php
            if($cover_image = \News\Model_News::get_cover_image($item))
              echo \Html::img( \Helper::amazonFileURL( \Config::get('media_base_url' ) .$cover_image->image ) , array( 'width' => '356', 'height' => '321', 'class' => 'product-img', 'alt' => $cover_image->alt_text ) ); 
          ?>
        </div>

      </div>
       <div class="col-md-8">
         <h3 class="copy-title"><?php echo $item->title; ?></h3>
         <p class="meta">Date Posted : <span><?php echo date("d-m-Y", $item->created_at); ?></span></p>
         <?php echo $item->description_full; ?>
       </div>
    </div>
  </div>
</div>