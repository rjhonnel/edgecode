<div class="app-content">
  <div class="container">
    <h2 class="copy-title">Categories</h2>

  <!-- check if it has category -->
  <?php if($categories): ?>
    <div class="category-products">
      <div class="row">
        <?php foreach($categories as $category ): ?>
          <div class="col-md-4 col-sm-6">
            <div class="product-item">
              <a href="<?php echo \Uri::create('news/'.$category->seo->slug) ?>">
                <div class="product-img">
                  <?php
                    if(isset($category->images[0]->image)):
                      echo \Html::img(\Helper::amazonFileURL( \Config::get('media_base_url') . $category->images[0]->image, array('alt' => $category->images[0]->alt_text))); 
                    else:
                  ?>
                    <div class="no-image">
                      <i class="icon icon-image2"></i>
                      <h4>No image</h4>
                    </div>
                  <?php endif; ?>
                </div>
                <span class="product-display">
                  <span class="product-content">
                    <h3 class="product-title" title="<?php echo $category->title; ?>"><?php echo $category->title; ?></h3>
                  </span>
                </span>
              </a>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  <?php endif;?>  
 </div>
</div>  