<div class="banner-slim" <?php (!empty($item->images))?'style="background-image: url(' . \Helper::amazonFileURL( \Config::get('media_base_url') . $item->images[0]->image) .')"':''; ?> >
  <div class="container">
    <div class="banner-heading">
      <h2 class="banner-title"><?php echo $item->title; ?></h2>
    </div>
  </div>
</div>

<?php
	$news = \Application\Model_Casestudy::find(function($query){

	    $query->where('parent_id', 143);
	    $query->order_by('created_at', 'desc');
	});
?>

<?php if($news): ?>
	<?php foreach($news as $post): ?>
		<div class="section skin-white-lighter">
			<div class="container">
				<div class="row copy-blog">
					<div class="col-sm-4">
						<div class="border-box">
							<img src="<?php echo $post->images ? \Helper::amazonFileURL( \Config::get('media_base_url' ) ) .$post->images[0]->image : \Uri::create('/').'/themes/frontend/images/no-image.png'; ?>" alt="<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $post->images[0]->alt_text); ?>">
						</div>
					</div>

					<div class="col-sm-8">
						<div class="copy">
							<h2 class="copy-title"><a href="/news/<?php echo $post->seo->slug; ?>"><?php echo $post->title; ?></a></h2>
							<p class="meta">Date Posted : <span><?php echo date("d-m-Y", $post->created_at); ?></span></p>
							<p><?php echo $post->description_intro; ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
<?php else: ?>
	<div class="section skin-white-lighter">
	  <div class="container">
	    <p>No post found.</p>
	  </div>
	</div>		
<?php endif; ?>