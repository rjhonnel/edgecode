  <!-- Image Banner with Description Intro and Alt text of image -->
  <?php if(!empty($item->images)): ?>
    <div class="slider-primary">
      <div class="slides" role="listbox">
        <?php foreach($item->images as $image){ ?>
        <div class="item" style="background-image: url('<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $image->image); ?>')">
        <div class="slide-content">
            <h1 class="slide-title"><?php echo $item->description_intro; ?></h1>
            <div class="slide-copy">
              <p><?php echo $image->alt_text; ?></p>
            </div>
            <a href="<?php echo \Uri::create('products'); ?>" class="btn btn-lg">Get Started</a>
          </div>
        </div>
        <?php } ?>
      </div>
    </div>
  <?php endif; ?>
  <!-- Image Banner with Description Intro and Alt text of image -->

  <div class="blurb">
    <div class="container">
      <!-- Description -->
      <h2 class="blurb-title"><?php echo $item->description_full; ?></h2>
      <!-- EOF Description -->
    </div>
  </div>

  <?php
    // get active products
  $products = \Product\Model_Product::find(function($query){
    $query->where('featured', 1);
    $query->order_by('created_at', 'desc');
  });

  $group_id = \Sentry::user()->group_id();
  if($products)
  {
    foreach ($products as $key => $value) {
     
      $category = \Product\Model_Category::find_by('id',$value->data['category_id']);
      $c_category = \Product\Model_Category::find_by('id',$value->data['parent_id']);

      if(!empty((array)json_decode($category[0]->user_group))){

          if(!in_array($group_id,(array)json_decode($category[0]->user_group))){

            unset($products[$key]);
            if($value->data['children']){
       
            foreach ($value->data['children'] as $k => $v) {

                   if($v->parent_id == $value->data['category_id']){

                      foreach ($products as $k2 => $v2) {
                        if($v2->data['category_id'] == $v->id){
                          unset($products[$k2]);     
                        }
                      }
                   }
              }
            }
          }


      }

      if($c_category){
        if(!empty((array)json_decode($c_category[0]->user_group))){

            if(!in_array($group_id,(array)json_decode($c_category[0]->user_group))){
              unset($products[$key]);
            }

        }
      }
      
    }
  }

?>
  <div class="featured-products">
    <div class="container">
      <h2 class="featured-title">Featured Products</h2>
      <?php if($products): ?>
        <div class="row">
          <?php foreach ($products as $product): 
           
            $category = \Product\Model_Category::find_by('id',$product->data['category_id']);
            
            $product_data = $product->data;
            $stock_quantity = isset($product_data['current_attributes'][0]->product_attribute->stock_quantity)?$product_data['current_attributes'][0]->product_attribute->stock_quantity:(isset($product->attributes[0]->stock_quantity)?$product->attributes[0]->stock_quantity:0);
            $get_stock_quantity = $stock_quantity?$stock_quantity :0; ?>
          <div class="col-md-4 col-sm-6">
            <div class="product-item <?php echo ($get_stock_quantity <= 0) ? 'no-stock' : '' ?>">
              <a href="<?php echo \Uri::create($product->seo->slug); ?>">
                <div class="product-img">
                  <?php 
                  if($cover_image = \Product\Model_Product::get_cover_image($product))
                    echo \Html::img( \Helper::amazonFileURL( \Config::get('media_base_url' ) .$cover_image->image ) , array( 'width' => '356', 'height' => '321', 'class' => 'product-img', 'alt' => $cover_image->alt_text ) ); 
                  ?>
                  <?php if(empty($product->images)): ?>
                    <div class="no-image">
                      <i class="icon icon-image2"></i>
                      <h4>No image</h4>
                    </div>
                  <?php endif; ?>
                </div>
                <?php if($get_stock_quantity <= 0): ?>
                  <div class="ribbon">
                    <h2 class="ribbon-title">Out of Stock</h2>
                  </div>
                <?php endif; ?>
                  <?php $available = ($get_stock_quantity > 0 || $do_not_allow_buy_out_of_stock == 0) && $product->product_type == 'simple'?true:false; ?>
                    <span class="product-display">
                      <span class="product-content">
                        <h3 class="product-title" title="<?php echo $product->title; ?>"><?php echo $product->title; ?></h3>
                        <span class="product-price">$<?php echo number_format($product_data['price'], 2); ?> <small>per person</small></span>
                        <p>
                        <?php if($product->minimum_order):?>
                        <small>minimum <?php echo $product->minimum_order;?> people</small>
                        <?php else: ?>
                          &nbsp;
                        <?php endif;?> 
                        </p>

                        <?php if($available): ?>
                          <?php echo \Form::open(array('action' => \Uri::current(), 'method' => 'post', 'id' => 'cart_form')); ?>
                            <?php echo \Form::hidden('product_id', $product->id); ?>
                            <?php echo \Form::hidden('attributeid', '', array('class' => 'attributeid')); ?>
                            <?php if(isset($attr_obj)) echo \Form::hidden('product_attribute_id', $attr_obj->id, array('class' => 'product_attribute_id')); ?>
                            <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>
                            <span>
                              Qty: <input type="number" min="1" name="quantity" value="<?php echo $product->minimum_order?:1; ?>" onclick="return false;" class="form-control" style="width:100px;display:inline;">
                            </span>
                            <input type="hidden" id="minimum_order" name= "minimum_order" value="<?php echo $product->minimum_order;?>">
                            <span class="product-action add_to_cart" style="display:none">
                                <i class="icon icon-plus"></i>
                            </span>
                          <?php echo \Form::close(); ?>
                        <?php endif; ?>
                      </span>

                      <?php if ($available): ?>
                        <span class="product-action add_to_cart_dummy">
                            <i class="icon icon-plus"></i>
                        </span>
                      <?php else: ?>
                        <span class="product-action">
                          <i class="icon icon-search"></i>
                        </span>
                      <?php endif; ?>
                    </span>
              </a>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>

<script type="text/javascript">
  var $loading_img = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>select2-spinner.gif';
  var $images_base_url = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>';
  var $artwork_required = 0;
  var $createOrderUrl = '<?php echo \Uri::create('product/create_order'); ?>';
  var $addToCartUrl = '<?php echo \Uri::create('product/add_to_cart'); ?>';
  var $sendDataUrl = '<?php echo \Uri::create('product/commit_upload') ?>';
  var $artworkUploaded = '<?php echo \Uri::create('product/artwork_uploaded'); ?>';

  var $artworkUrl = '<?php echo \Uri::create('product/add_artwork_type/'); ?>';
  var $uploadItemUrl = '<?php echo \Uri::create('product/add_upload_item/'); ?>';
  var $itemInCartUrl = '<?php echo \Uri::create('product/add_to_cart/'); ?>';
  var $uploadArtworkUrl = '<?php echo \Uri::create('product/upload_artwork/'); ?>';
  var $getProductDataUrl = '<?php echo \Uri::create('product/product_data/'); ?>';

  $('.add_to_cart').on('click', function(e){
    $artworkUrl = $artworkUrl + $(this).attr('data-productid');
    $uploadItemUrl = $uploadItemUrl + $(this).attr('data-productid');
    $itemInCartUrl = $itemInCartUrl + $(this).attr('data-productid') + '/true';
    $uploadArtworkUrl = $uploadArtworkUrl + $(this).attr('data-productid');
    $getProductDataUrl = $getProductDataUrl + $(this).attr('data-productid');
  });

  $('.add_to_cart_dummy').click(function(e){
    $(this).parent('.product-display').find('.add_to_cart').click();
  });
</script>
<script type="text/javascript" src="/themes/frontend/js/add-to-cart.js"></script>