<!-- Image Banner with Title -->
<?php if(!empty($item->images)): ?>
  <div id="main-carousel" class="carousel slide slide-secondary">
    <div class="carousel-inner" role="listbox">
      <div class="item active" style="background-image: url('<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $item->images[0]->image); ?>')">
        <div class="carousel-content">
          <h1 class="carousel-title"><?php echo $item->title; ?></h1>
          <div class="carousel-copy">
            <p><?php echo $item->images[0]->alt_text; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- EOF Image Banner with Title -->

<div class="app-content">
  <div class="container">
    <!-- Show Title if there is image, if none show intro description -->
    <h2 class="copy-title"><?php echo !empty($item->images) ? $item->description_intro : $item->title; ?></h2>
    <!-- EOF Show Title if there is image, if none show intro description -->
    <div class="row">
      <div class="copy">
        <!-- Description -->
        <?php echo $item->description_full; ?>
        <!-- EOF Description -->
      </div>
    </div>
  </div>
</div>