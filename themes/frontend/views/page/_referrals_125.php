<!-- Image Banner with Title -->
<?php if(!empty($item->images)): ?>
  <div id="main-carousel" class="carousel slide slide-secondary">
    <div class="carousel-inner" role="listbox">
      <div class="item active" style="background-image: url('<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $item->images[0]->image); ?>')">
        <div class="carousel-content">
          <h1 class="carousel-title"><?php echo $item->title; ?></h1>
          <div class="carousel-copy">
            <p><?php echo $item->images[0]->alt_text; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- EOF Image Banner with Title -->

<div class="app-content">
  <div class="container">
    <!-- Show Title if there is image, if none show intro description -->
    <h2 class="copy-title"><?php echo !empty($item->images) ? $item->description_intro : $item->title; ?></h2>
    <!-- EOF Show Title if there is image, if none show intro description -->
    <div class="row">
      <!-- Description -->
      <?php echo $item->description_full; ?>
      <!-- EOF Description -->
    </div>
    <div class="row">
      <div class="col-md-12">
        <!-- Referral Form -->
        <form action="page/add_referal" method="post" data-validate='parsley'>
          
          <input type="hidden" name="user_added" value="<?php echo \Sentry::user()->id; ?>" >

          <div class="form-group">
            <h1>Fill Your Details</h1>
          </div>
          <div class="form-group">
            <label>Name *</label>
            <input type="text" class="form-control form-control-lg required" placeholder="Name" name="name" data-trigger ='change' >
          </div>
          <div class="form-group">
            <label>Phone *</label>
            <input type="text" class="form-control form-control-lg required" placeholder="Phone" name="phone" data-trigger ='change' >
          </div>
          <div class="form-group">
            <label>Email *</label>
            <input type="email" class="form-control form-control-lg required" placeholder="Email" name="email" data-type='email' data-trigger ='change' >
          </div>
          <div class="form-group">
            <label>Suburb *</label>
            <input type="text" class="form-control form-control-lg required" placeholder="Suburb" name="suburb" data-trigger ='change' >
          </div>
          <div class="form-group form-action pull-right">
            <button name="submit" class="btn btn-grey btn-xlg">Submit</button>
          </div>
        </form>
        <!-- EOF Referral Form -->
      </div>
    </div>
  </div>
</div>