
<!-- Image Banner with Title -->
<?php if(!empty($item->images)): ?>
  <div id="main-carousel" class="carousel slide slide-secondary">
    <div class="carousel-inner" role="listbox">
      <div class="item active" style="background-image: url('<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $item->images[0]->image); ?>')">
        <div class="carousel-content">
          <h1 class="carousel-title"><?php echo $item->title; ?></h1>
          <div class="carousel-copy">
            <p><?php echo $item->images[0]->alt_text; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- EOF Image Banner with Title -->

<div class="app-content">
  <div class="container">
    <!-- Show Title if there is image, if none show intro description -->
    <h2 class="copy-title"><?php echo !empty($item->images) ? $item->description_intro : $item->title; ?></h2>
    <!-- EOF Show Title if there is image, if none show intro description -->
    <div class="row">
      <div class="col-md-6">
        <div class="map">
          <!-- Map Frame -->
          <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6591.545134854114!2d146.079961796912!3d-34.30533946416309!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b1faef32a4ea135%3A0x8781c81768a21a8b!2s78+Oakes+Rd%2C+Yoogali+NSW+2680%2C+Australia!5e0!3m2!1sen!2sph!4v1427784681822" width="100%" height="450" frameborder="0" style="border:0"></iframe>
          <!-- EOF Map Frame -->
          <ul>
            <li>78 Oakes Road | PO Box 2525 | Griffith NSW 2680</li>
            <li>T: 02 6964 0000 | F: 02 6964 0200</li>
            <li><a href="mailto:sales@enviroform.com.au">sales@enviroform.com.au</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-6">
        <!-- Contact Form -->
        <form action="<?php echo \Uri::create('page/send_contact_form'); ?>" method="post">
          <div class="form-group">
            <input type="text" class="form-control form-control-lg" placeholder="Name" name="name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control form-control-lg" placeholder="Email" name="email">
          </div>
          <div class="form-group">
            <input type="text" class="form-control form-control-lg" placeholder="Subject" name="subject">
          </div>
          <div class="form-group">
            <textarea name="message" id="" cols="30" rows="10" class="form-control form-control-lg" placeholder="Message"></textarea>
          </div>
          <div class="form-group">
            <!-- Captcha -->
            <?php \Config::load('page::page', 'page', true); ?>
            <div class="g-recaptcha" data-sitekey="<?php echo \Config::get('page.recaptcha.site_key'); ?>"></div>
            <!-- EOF Captcha -->
          </div>
          <div class="form-group form-action pull-right">
            <button name="submit" class="btn btn-grey btn-xlg submit-contact-form">Send</button>
          </div>
        </form>
        <!-- EOF Contact Form -->
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div style="display: none;" id="contact_us_modal">
  <div class="popup">
    <div class="legend">
      <h2 id="contact_us_modal_header"></h2>
    </div>
    <div class="container_12">
      <div class="grid_12" id="contact_us_modal_message">
      </div>
    </div>
    <div class="clear"></div>
  </div>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>