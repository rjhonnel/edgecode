<!-- Image Banner with Title -->
<?php if(!empty($item->images)): ?>
  <div id="main-carousel" class="carousel slide slide-secondary">
    <div class="carousel-inner" role="listbox">
      <div class="item active" style="background-image: url('<?php echo \Helper::amazonFileURL( \Config::get('media_base_url') . $item->images[0]->image); ?>')">
        <div class="carousel-content">
          <h1 class="carousel-title"><?php echo $item->title; ?></h1>
          <div class="carousel-copy">
            <p><?php echo $item->images[0]->alt_text; ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>
<!-- EOF Image Banner with Title -->

<div class="app-content">
  <div class="container">
    <!-- Show Title if there is image, if none show intro description -->
    <h2 class="copy-title"><?php echo !empty($item->images) ? $item->description_intro : $item->title; ?></h2>
    <!-- EOF Show Title if there is image, if none show intro description -->
  <?php
      // get all active parent category
      $categories = \Product\Model_Category::find(function($query){
            $query->where('parent_id', 0);
            $query->order_by('sort', 'asc');
        });
      $group_id = \Sentry::user()->group_id();
  ?>
  <!-- check if it has category -->
  <?php if($categories): ?>
    <?php for($x=0;$x<sizeof($categories);$x++): ?>
    <?php
        if(!empty((array)json_decode($categories[$x]->user_group))){
            
            if(!in_array($group_id,(array)json_decode($categories[$x]->user_group))){
                unset($categories[$x]);
            }

        } 
    ?>
  <?php endfor;?>
    <div class="category-products">
      <div class="row">
        <?php foreach($categories as $category ): ?>
          <div class="col-md-4 col-sm-6">
            <div class="product-item">
              <a href="<?php echo \Uri::create($category->seo->slug) ?>">
                <div class="product-img">
                  <?php
                    if(isset($category->images[0]->image)):
                      echo \Html::img(\Helper::amazonFileURL( \Config::get('media_base_url') . $category->images[0]->image, array('alt' => $category->images[0]->alt_text))); 
                    else:
                  ?>
                    <div class="no-image">
                      <i class="icon icon-image2"></i>
                      <h4>No image</h4>
                    </div>
                  <?php endif; ?>
                </div>
                <span class="product-display">
                  <span class="product-content">
                    <h3 class="product-title" title="<?php echo $category->title; ?>"><?php echo $category->title; ?></h3>
                  </span>
                </span>
              </a>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  <?php else: ?>
        <?php
          // if no category display all active products
          $products = \Product\Model_Product::find(function($query){

              $query->order_by('created_at', 'desc');
          });
        ?>
        <div class="category-products">
          <div class="row">
            <?php foreach ($products as $product): ?>
                <?php
                  $product_data = $product->data;
                  $stock_quantity = isset($product_data['current_attributes'][0]->product_attribute->stock_quantity)?$product_data['current_attributes'][0]->product_attribute->stock_quantity:(isset($product->attributes[0]->stock_quantity)?$product->attributes[0]->stock_quantity:0);
                ?>
                <div class="col-md-4 col-sm-6">
                  <div class="product-item <?php echo ($stock_quantity <= 0) ? 'no-stock' : '' ?>">
                    <a href="<?php echo \Uri::create($product->seo->slug); ?>">
                      <div class="product-img">
                        <?php 
                            if($cover_image = \Product\Model_Product::get_cover_image($product))
                                echo \Html::img( \Helper::amazonFileURL( \Config::get('media_base_url' ) .$cover_image->image ) , array( 'width' => '356', 'height' => '321', 'class' => 'product-img', 'alt' => $cover_image->alt_text ) ); 
                        ?>
                        <?php if(empty($product->images)): ?>
                          <div class="no-image">
                            <i class="icon icon-image2"></i>
                            <h4>No image</h4>
                          </div>
                        <?php endif; ?>
                      </div>
                      <?php if($stock_quantity <= 0): ?>
                        <div class="ribbon">
                          <h2 class="ribbon-title">Out of Stock</h2>
                        </div>
                      <?php endif; ?>


                      <span class="product-display">
                        <span class="product-content">
                          <h3 class="product-title" title="<?php echo $product->title; ?>"><?php echo $product->title; ?></h3>
                          <span class="product-price">$<?php echo number_format($product_data['price'], 2); ?></span>
                          <?php if($product->minimum_order):?>
                            <p><small>minimum <?php echo $product->minimum_order;?> people</small></p>
                          <?php endif;?> 
                        </span>
                        <?php if(($stock_quantity > 0 || $do_not_allow_buy_out_of_stock == 0) && $product->product_type == 'simple'): ?>
                          <!-- Add to cart product -->
                          <?php echo \Form::open(array('action' => \Uri::current(), 'method' => 'post', 'id' => 'cart_form')); ?>
                            <?php echo \Form::hidden('product_id', $product->id); ?>
                            <?php echo \Form::hidden('attributeid', '', array('class' => 'attributeid')); ?>
                            <?php if(isset($attr_obj)) echo \Form::hidden('product_attribute_id', $attr_obj->id, array('class' => 'product_attribute_id')); ?>
                            <?php echo \Form::hidden(\Config::get('security.csrf_token_key'), \Security::fetch_token()); ?>
                            <?php echo \Form::hidden('quantity', $product->minimum_order?:1); ?>
                            <span class="product-action add_to_cart">
                                <i class="icon icon-plus"></i>
                            </span>
                          <?php echo \Form::close(); ?>
                          <!-- EOF Add to cart product -->
                        <?php else: ?>
                          <span class="product-action">
                            <i class="icon icon-search"></i>
                          </span>
                        <?php endif; ?>
                      </span>
                    </a>
                  </div>
                </div>
            <?php endforeach; ?>
          </div>
        </div>

    <script type="text/javascript">
        // set variable needed for add to cart product in add-to-cart.js
        var $loading_img = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>select2-spinner.gif';
        var $images_base_url = '<?php echo \Uri::create(\Config::get("media_base_url")); ?>';
        var $artwork_required = 0;
        var $createOrderUrl = '<?php echo \Uri::create('product/create_order'); ?>';
        var $addToCartUrl = '<?php echo \Uri::create('product/add_to_cart'); ?>';
        var $sendDataUrl = '<?php echo \Uri::create('product/commit_upload') ?>';
        var $artworkUploaded = '<?php echo \Uri::create('product/artwork_uploaded'); ?>';

        var $artworkUrl = '<?php echo \Uri::create('product/add_artwork_type/'); ?>';
        var $uploadItemUrl = '<?php echo \Uri::create('product/add_upload_item/'); ?>';
        var $itemInCartUrl = '<?php echo \Uri::create('product/add_to_cart/'); ?>';
        var $uploadArtworkUrl = '<?php echo \Uri::create('product/upload_artwork/'); ?>';
        var $getProductDataUrl = '<?php echo \Uri::create('product/product_data/'); ?>';

        // set variable needed for add to cart product when .add_to_cart button is click
        $('.add_to_cart').on('click', function(e){
          $artworkUrl = $artworkUrl + $(this).attr('data-productid');
          $uploadItemUrl = $uploadItemUrl + $(this).attr('data-productid');
          $itemInCartUrl = $itemInCartUrl + $(this).attr('data-productid') + '/true';
          $uploadArtworkUrl = $uploadArtworkUrl + $(this).attr('data-productid');
          $getProductDataUrl = $getProductDataUrl + $(this).attr('data-productid');
        });
    </script>
    <!-- Add add-to-cart.js to make add to cart functionality working -->
    <script type="text/javascript" src="/themes/frontend/js/add-to-cart.js"></script>
  <?php endif; ?>
  </div>
</div>