<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="UTF-8">
  <title>
    <?php echo isset($title) ? $title . ' | ' . \Config::get('site_title')  : \Config::get('site_title'); ?>
  </title>

  <!-- Meta Content -->
  <meta name="description" content="<?php echo isset($meta_description) ? $meta_description  : ''; ?>">
  <meta name="keywords" content="<?php echo isset($meta_keywords) ? $meta_keywords  : ''; ?>">
  <meta name="robots" content="<?php echo isset($robots) ? implode(',',$robots)  : ''; ?>">
  <link rel="canonical" href="<?php echo isset($canonical) ? $canonical  : ''; ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- EOF Meta Content -->

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="/favicon.ico/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="/favicon.ico/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="/favicon.ico/manifest.json">
  <link rel="mask-icon" href="/favicon.ico/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">

  <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,300,700' rel='stylesheet' type='text/css'>
  <?php
    // styles for html elements
    echo $theme->asset->css('../assets/css/styles.css');
    // EOF styles for html elements

    // required file to run jquery functions
    echo $theme->asset->js('plugins/jquery-2.2.4.min.js');
  ?>
  <script type="text/javascript">
      var baseUrl = '<?php echo \Uri::create('/'); ?>';
  </script>
  </head>
  <body>
    <!-- Display messages -->
    <?php echo \Messages::display_front(); ?>
    <!-- EOF Display messages -->

    <!--Footer-->
    <?php echo $theme->view('views/_partials/header'); ?>
    <!--EOF Footer-->

    <!--Main Content-->
    <div class="section" style="padding: 60px 0;">
		<div class="container">
			<div class="row">
				<div class="span16">
					<h1 class="text-center" style="font-size: 75px;line-height: 1.5;">404 Page</h1>
				</div>
			</div>
		</div>
	</div>
    <!--EOF Main Content-->

    <!--Footer-->
    <?php echo $theme->view('views/_partials/footer'); ?>
    <!--EOF Footer-->

    <?php if($logged_type != 'user'): ?>
      <!-- Login Modal if referal link is clicked and not login -->
      <div class="modal fade in" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <?php echo $theme->view('views/user/single_login'); ?>
            </div>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <!-- EOF Login Modal if referal link is clicked and not login -->

    <?php
      echo $theme->asset->js('../assets/js/all.js');
      echo $theme->asset->js('main.js');
      echo $theme->asset->js('custom.js');
    ?>
  </body>
</html>