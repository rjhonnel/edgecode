<div class="app-content">
  <div class="container">
   <div class="row">
   <div class="label-heading">
         <h3 class="label-title">Reset Password</h3>
       </div>
     <div class="col-sm-6">
       
       <div class="app-content">
        <!-- Reset Password Form -->
         <?php echo \Form::open(array('action' => \Uri::current(), 'data-validate' => 'parsley'), array('forgot' => 1)); ?>
           <div class="form-group">
           <label for="" class="form-label">New Password</label>
             <?php echo \Form::input('new_password', \Input::post('new_password'), array("class" => "required form-control", "type" => "password", "data-minlength" => "6", 'data-trigger' => 'change')); ?>
           </div>
           <div class="form-group">
             <label for="" class="form-label">Confirm New Password</label>
             <?php echo \Form::input('confirm_new_password', \Input::post('confirm_new_password'), array("class" => "required form-control", "type" => "password", "data-equalto" => "#form_new_password", 'data-trigger' => 'change')); ?>
           </div>

           <button type="submit" class="btn btn-green">Reset</button>
          <?php echo \Form::close(); ?>
          <!-- EOF Reset Password Form -->
       </div>
     </div>
   </div>
  </div>
</div>
