
<div class="app-content">
  <div class="container">
   <div class="row">
   <div class="label-heading">
         <h3 class="label-title">Login</h3>
       </div>
     <div class="col-sm-6">
       
       <div class="app-content">
        <!-- Login Form -->
         <?php echo \Form::open(array('action' => \Uri::create('user/login'), 'data-validate' => 'parsley'), array('login' => 1)); ?>
           <div class="form-group">
           <label for="" class="form-label">Email Address</label>
             <?php echo \Form::input('identity', \Input::post('identity'), array("class" => "required form-control", 'data-type' => 'email', 'data-trigger' => 'change')); ?>
           </div>
           <div class="form-group">
             <label for="" class="form-label">Password</label>
             <?php echo \Form::password('password', \Input::post('password'), array("class" => "required form-control", 'data-minlength' => '6', 'data-trigger' => 'change')); ?>
           </div>

           <button type="submit" class="btn btn-green">Login</button>
           <a href="<?php echo \Uri::front_create('user/password'); ?>" class="link-default pull-right">Forgot Password?</a>
          <?php echo \Form::close(); ?>
          <!-- EOF Login Form -->
       </div>
     </div>
   </div>
  </div>
</div>
