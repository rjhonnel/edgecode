
<div class="app-content">
  <div class="container">
   <div class="row">
   <div class="label-heading">
         <h3 class="label-title">Forgot Password</h3>
       </div>
     <div class="col-sm-6">
       
       <div class="app-content">
       <div class="form-group">
         <p>Please enter your registered email address. You will then receive an email to reset your password.</p>
       </div>
       <!-- Forgot Password Form -->
         <?php echo \Form::open(array('action' => \Uri::current(), 'data-validate' => 'parsley'), array('forgot' => 1)); ?>
           <div class="form-group">
           <label for="" class="form-label">Email Address</label>
             <?php echo \Form::input('identity', \Input::post('identity'), array("class" => "required form-control", 'data-type' => 'email', 'data-trigger' => 'change')); ?>
           </div>
           <button type="submit" class="btn btn-green">Submit</button>
            <a href="<?php echo \Uri::front_create('user/login'); ?>" class="link-default pull-right">Back to login?</a>
          <?php echo \Form::close(); ?>
        <!-- EOF Forgot Password Form -->
       </div>
     </div>
   </div>
  </div>
</div>
