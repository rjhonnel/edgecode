
<div class="app-content">
  <div class="container">
   <div class="row">
   <div class="label-heading">
         <h3 class="label-title">Signup</h3>
       </div>
     <div class="col-sm-6">
       
       <div class="app-content">
        <!-- Signup Form -->
         <?php echo \Form::open(array('action' => \Uri::front('current'), 'data-validate' => 'parsley'), array('signup' => 1, 'user_group' => 4)); ?>

            <?php echo $theme->view('views/user/_form'); ?>
            
        <?php echo \Form::close(); ?>
          <!-- EOF Signup Form -->
       </div>
     </div>
   </div>
  </div>
</div>
