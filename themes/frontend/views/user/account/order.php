<div class="app-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
              <div class="label-heading">
                  <h2 class="label-title">DASHBOARD</h2>
                  <p>Your order list.</p>
              </div>
            </div>
        </div>

        <div class="app-content">
            <div class="row">
                <div class="col-sm-4">
                    <aside class="l-sidebar form-l2">
                        <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => 'orders')); ?>
                    </aside>                    
                </div>

                <div class="col-sm-8">
                    <table class="table table-l3">
                        <thead>
                            <tr>
                                <td colspan="7">Order</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Product Code</td>
                                <td>Item</td>
                                <td>Description</td>
                                <td>Quantity</td>
                                <td>Delivery Time</td>
                                <td>Unit Price</td>
                                <td>Price</td>
                            </tr>
                            <?php if($items): ?>
                                <?php foreach($items as $item): ?>
                                <tr>
                                    <td><?php echo $item->code; ?></td>
                                    <td><?php echo $item->title; ?></td>
                                    <td><?php echo $item->description; ?></td>
                                    <td><?php echo $item->quantity; ?></td>
                                    <td><?php echo $order->delivery_datetime_list?(date('h:i a', strtotime($item->delivery_time))):($order->delivery_datetime?(date('h:i a', strtotime($order->delivery_datetime))):''); ?></td>
                                    <td>$<?php echo number_format($item->price, 2); ?></td>
                                    <td>$<?php echo number_format($item->subtotal, 2); ?></td>
                                </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            <tr>
                                <td colspan="7"></td>
                            </tr>
                            <?php
                                $settings = \Config::load('autoresponder.db');
                                $hold_gst = (isset($settings['gst']) ? 1 : 0);
                                $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
                            ?>

                            <tr>
                                <td colspan="5"></td>
                                <td>Products Total</td>
                                <td>$<?php echo number_format($order->total_price, 2); ?></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td>Shipping</td>
                                <td>$<?php echo number_format($order->shipping_price, 2); ?></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td>Extra Delivery Charge</td>
                                <td>$<?php echo number_format($order->total_delivery_charge, 2); ?></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td>Discount</td>
                                <td>-$<?php echo number_format($order->discount_amount, 2); ?></td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td><strong>Order Total</strong></td>
                                <td>$<?php echo number_format($order->total_price(), 2); ?></td>
                            </tr>
                            <?php if($hold_gst && $hold_gst_percentage > 0): ?>
                                <tr>
                                    <td colspan="5"></td>
                                    <td>GST Tax <?php echo $hold_gst_percentage; ?>% (Included)</td>
                                    <td>$<?php echo number_format(\Helper::calculateGST($order->total_price(), $hold_gst_percentage), 2); ?></td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>


