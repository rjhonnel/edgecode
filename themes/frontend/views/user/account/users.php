<div class="row-fluid">
                
    <?php echo $theme->view('views/_partials/sidebar'); ?>

    <div class="main_area">

        <div class="breadcrumbs_holder">
            <div class="content_holder">
                <?php 
                    \Breadcrumb::set('Home', '/');
                    \Breadcrumb::set('User', 'user/account');
                    \Breadcrumb::set('Users');
                    echo \Breadcrumb::create_links();
                ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="title_holder">
            <div class="content_holder">
                <h1>Users</h1>
            </div>
        </div>

        <div class="content_wrapper gradient forms_gradient">

            <div class="content_holder">
                
                <div class="row-fluid">
                    
                    <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => 'users')); ?>
                    
                    <div class="span9">
                        
                        <!-- Users  -->
                        
                        <h3 class="bold">SUB USERS</h3>
                
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu risus gravida, 
                        elementum odio et, sagittis felis. Etiam eu aliquet arcu. Morbi ultrices lectus 
                        neque, id interdum dolor ornare vel. Praesent id adipiscing libero. Maecenas 
                        fermentum dolor ut ultricies porttitor. Vestibulum venenatis massa quis augue <br><br><br>
                        
                        <div class="box">
                            
                            <table class="table table-striped">
                                <tr>
                                    <th>Entity</th>
                                    <th>Name</th>
                                    <th>Email Address</th>
                                    <th class="center">View</th>
                                </tr>
                                <?php if(!empty($users)): ?>
                                    <?php 
                                        foreach($users as $user_item): 
                                        $user_data = \Sentry::user((int)$user_item['id']);
                                    ?>
                                        <tr>
                                            <td><?php echo $user_data->get('metadata.business_name'); ?></td>
                                            <td><?php echo $user_data->get('metadata.first_name') . ' ' . $user_data->get('metadata.last_name'); ?></td>
                                            <td><a href="mailto:<?php echo $user_data->email; ?>"><?php echo $user_data->email; ?></a></td>
                                            <td class="center"><a href="<?php echo \Uri::front_create('user/account/profile/' . $user_data->id); ?>" class="fancybox"><i class="icon-search"></i></a></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <tr>
                                        <td class="center" colspan="4">There are no users yet.</td>
                                    </tr>
                                <?php endif; ?>
                            </table>
                            
                        </div>
                        
                        <div class="right"><?php echo $pagination; ?></div>
                        
                        <a href="<?php echo \Uri::front_create('user/account/create'); ?>" class="gray_btn left"><span>Add Sub User</span></a>
                        
                        <!-- EOF Users  -->
                        
                    </div>
                    
                </div>
                
            </div>

        </div>

    </div>

</div>