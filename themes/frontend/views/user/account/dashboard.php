<div class="app-content">
    <div class="container">
       <div class="row">
            <div class="col-sm-12">
              <div class="label-heading">
                  <h2 class="label-title">DASHBOARD</h2>
                  <p>Your account details.</p>
              </div>
            </div>
          </div>

          <div class="app-content">
              <div class="row">
                  <div class="col-sm-4">
                      <div class="l-sidebar">
                          <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => 'dashboard')); ?>
                      </div>
                  </div>

                  <div class="col-sm-8">
                      <div class="table-responsive">
                        <table class="table table-l2">
                          <thead>
                              <tr>
                                  <td colspan="2">Account</td>
                              </tr>
                          </thead>

                          <tbody>
                            <tr>
                                <td>First Name</td>
                                <td><?php echo $user->get('metadata.first_name'); ?></td>
                            </tr>
                            <tr>
                                <td>Last Name</td>
                                <td><?php echo $user->get('metadata.last_name'); ?></td>
                            </tr>
                            <?php if($user->get('metadata.purchase_limit_period') > 0): ?>
                                <tr>
                                    <td>Purchase Limit</td>
                                    <td>
                                        $ <?php echo $user->get('metadata.purchase_limit_value'); ?><br>
                                        Over period: <?php echo $user->get('metadata.purchase_limit_period'); ?> months
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <tr>
                                <td>Company Name</td>
                                <td><?php echo $user->get('metadata.business_name'); ?></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><?php echo $user->get('metadata.address'); ?></td>
                            </tr>
                            <tr>
                                <td>Address 2</td>
                                <td><?php echo $user->get('metadata.address2'); ?></td>
                            </tr>
                            <tr>
                                <td>Suburb</td>
                                <td><?php echo $user->get('metadata.suburb'); ?></td>
                            </tr>
                            <tr>
                                <td>State</td>
                                <td><?php echo $user->get('metadata.state'); ?></td>
                            </tr>
                            <tr>
                                <td>Postcode</td>
                                <td><?php echo $user->get('metadata.postcode'); ?></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td><?php echo $user->get('metadata.phone'); ?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?php echo $user->get('email'); ?></td>
                            </tr>
                          </tbody>
                          <tfoot>
                              <tr>
                                  <td colspan="2">
                                       <a href="<?php echo \Uri::front_create('user/account/details/' . $user->id); ?>" class="btn btn-green btn-block"><span>Edit details</span></a> 
                                  </td>
                              </tr>
                          </tfoot>
                      </table>
                      </div>
                  </div>
              </div>
          </div>


    </div>
</div>