<div class="app-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
              <div class="label-heading">
                  <h2 class="label-title">ORDERS</h2>
                  <p>Your order list.</p>
              </div>
            </div>
        </div>

        <div class="app-content">
            <div class="row">
                <div class="col-sm-4">
                    <div class="l-sidebar">
                          <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => 'dashboard')); ?>
                      </div>
                </div>

                <div class="col-sm-8">
                    <div class="table-responsive">
                        <table class="table table-l3">
                        <thead>
                            <tr>
                                <td colspan="8">Orders</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Order Id</td>
                                <td>Order Amount</td>
                                <td>Date</td>
                                <td style="width: 110px;">Delivery</td>
                                <td>Ordered By</td>
                                <td>Order Status</td>
                                <td>Accepted</td>
                                <td>View</td>
                            </tr>
                            <?php if(!empty($items)): ?>
                                <?php foreach($items as $item): ?>
                                    <tr>
                                        <td><?php echo $item->id; ?></td>
                                        <td>$<?php echo number_format($item->total_price(), 2); ?></td>
                                        <td><?php echo date('d/m/Y', $item->created_at); ?></td>
                                        <td><?php echo $item->delivery_datetime?(date('d/m/Y', strtotime($item->delivery_datetime)).'<br />'.date('h:i a', strtotime($item->delivery_datetime))):''; ?> <?php echo $item->delivery_datetime_list?(count(explode(';', $item->delivery_datetime_list))>1?'+':''):''; ?></td>
                                        <td><?php echo $item->user->get('metadata.first_name') . ' ' . $item->user->get('metadata.last_name'); ?></td>
                                        <td><?php echo isset($order_status[strtolower($item->status)]) ? $order_status[strtolower($item->status)] : ''; ?></td>
                                        <td><?php echo $item->accepted == 1 ? 'Yes' : 'No'; ?></td>
                                        <td class="center"><a href="<?php echo \Uri::front_create('user/account/order/' . $item->id); ?>">View</a></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td align="center" colspan="8">There are no orders yet.</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="8" align="left">
                                     <?php echo $pagination; ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- <div class="title-page title-l4">
  <div class="row">
    <div class="col-sm-12">
      <h1 class="t-huge">DASHBOARD</h1>
      <span class="intro">Your order list.</span>
    </div>
  </div>
</div>
<div class="section section-l1 section-details">
    <div class="well-bordered">
        <div class="row">
            <div class="col-sm-4">
                <aside class="l-sidebar form-l2">
                    <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => 'orders')); ?>
                </aside>
            </div>
            <div class="col-sm-8">
                <div class="well well-white">
                    <div class="form-group group-bordered">
                        <h1>ORDERS</h1>
                    </div>
                    <div class="section section-l2">
                        <table class="table table-l1">
                            <thead>
                                <tr>
                                    <th>Order id</th>
                                    <th>Order Amount</th>
                                    <th>Date</th>
                                    <th>Ordered By</th>
                                    <th>Order Status</th>
                                    <th>Accepted</th>
                                    <th class="center">View</th>
                                </tr>
                            </thead>
                            <?php if(!empty($items)): ?>
                                <?php foreach($items as $item): ?>
                                    <tr>
                                        <td><?php echo $item->id; ?></td>
                                        <td>$<?php echo number_format($item->total_price() + $item->gst_price, 2); ?></td>
                                        <td><?php echo date('d/m/Y', $item->created_at); ?></td>
                                        <td><?php echo $item->user->get('metadata.first_name') . ' ' . $item->user->get('metadata.last_name'); ?></td>
                                        <td><?php echo isset($order_status[strtolower($item->status)]) ? $order_status[strtolower($item->status)] : ''; ?></td>
                                        <td><?php echo $item->accepted == 1 ? 'Yes' : 'No'; ?></td>
                                        <td class="center"><a href="<?php echo \Uri::front_create('user/account/order/' . $item->id); ?>">View</a></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td class="center" colspan="7">There are no orders yet.</td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                    <div class="section section-l1 text-center">
                        <div class="pagination">
                            <?php echo $pagination; ?>
                        </div>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
</div> -->



