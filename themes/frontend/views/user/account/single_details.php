<div class="app-content">
    <div class="container">
          <div class="row">
    <div class="col-sm-12">
      <div class="label-heading">
          <h2 class="label-title">Dashboard</h2>
          <p>Update your account details.</p>
      </div>
    </div>
  </div>

      <div class="app-content">
          <div class="row">
              <div class="col-sm-4">
                  <div class="l-sidebar">
                      <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => ($user->id == \Sentry::user()->id ? 'dashboard' : 'users'))); ?>
                  </div>
              </div>

              <div class="col-sm-8">
                  <div class="table-responsive">
                    <table class="table table-l2">
                      <thead>
                          <tr>
                              <td colspan="2">Details</td>
                          </tr>
                      </thead>
                       <?php echo \Form::open(array('action' => \Uri::front('current'), 'data-validate' => 'parsley'), array('details' => 1)); ?>
                            <?php echo $theme->view('views/user/_form', array('user' => $user, 'edit' => true), false); ?>
                        <?php echo \Form::close(); ?>
                  </table>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>




