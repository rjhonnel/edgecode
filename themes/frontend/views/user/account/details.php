<div style="width: 695px; margin-bottom: 0;" class="account_form content">
    <?php echo \Form::open(\Uri::front('current'), array('details' => 1)); ?>
    <?php echo \Theme::instance()->view('views/user/account/_form', array('user' => $user, 'edit' => true, 'states' => $states), false); ?>
    <?php //echo \Form::button(array('type' => 'submit', 'value' => '<span>Save Details</span>', 'class' => 'button right arrow')); ?>
    <?php echo \Form::close(); ?>
</div>