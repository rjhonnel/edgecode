<div class="row-fluid">
                
    <?php echo $theme->view('views/_partials/sidebar'); ?>

    <div class="main_area">

        <div class="breadcrumbs_holder">
            <div class="content_holder">
                <?php 
                    \Breadcrumb::set('Home', '/');
                    \Breadcrumb::set('User', 'user/account');
                    \Breadcrumb::set('Users', 'user/account/users');
                    \Breadcrumb::set('Sub User', 'user/account/details/' . $user->id);
                    echo \Breadcrumb::create_links();
                ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="title_holder">
            <div class="content_holder">
                <h1>Sub User</h1>
            </div>
        </div>

        <div class="content_wrapper gradient forms_gradient">

            <div class="content_holder">
                
                <div class="row-fluid">
                    
                    <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => 'users')); ?>
                    
                    <div class="span9">
                        
                        <!-- Account  -->
                        
                        <h3 class="bold"><?php echo $user->get('metadata.first_name'); ?> <?php echo $user->get('metadata.last_name'); ?></h3>
                
                        <div class="row-fluid">
                            <div class="span6 box">
                                <table class="table first_bold table-striped">
                                    <tr>
                                        <td>Entity Name</td>
                                        <td><?php echo $user->get('metadata.business_name'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Salutation</td>
                                        <td><?php echo $user->get('metadata.title'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>First Name</td>
                                        <td><?php echo $user->get('metadata.first_name'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Last Name</td>
                                        <td><?php echo $user->get('metadata.last_name'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Job Title</td>
                                        <td><?php echo $user->get('metadata.job_title'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Department</td>
                                        <td><?php echo $user->get('metadata.department'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Department Code</td>
                                        <td><?php echo $user->get('metadata.department_code'); ?></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="span6 box">
                                <table class="table first_bold table-striped">
                                    <tr>
                                        <td>Street</td>
                                        <td><?php echo $user->get('metadata.address'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Suburb</td>
                                        <td><?php echo $user->get('metadata.suburb'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>State</td>
                                        <td><?php echo $user->get('metadata.state'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Postcode</td>
                                        <td><?php echo $user->get('metadata.postcode'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Phone</td>
                                        <td><?php echo $user->get('metadata.phone'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Direct Phone</td>
                                        <td><?php echo $user->get('metadata.direct_phone'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Mobile</td>
                                        <td><?php echo $user->get('metadata.mobile'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Fax</td>
                                        <td><?php echo $user->get('metadata.fax'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td><?php echo $user->get('email'); ?></td>
                                    </tr>
                                </table>
                            </div>
                            <a href="<?php echo \Uri::front_create('user/account/details/' . $user->id); ?>" class="gray_btn right"><span>Edit details</span></a>
                        </div>
                        
                        <!-- EOF Account  -->
                        
                    </div>
                    
                </div>
                
            </div>

        </div>

    </div>

</div>