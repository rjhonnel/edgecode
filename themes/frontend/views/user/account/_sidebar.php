<?php 
    // Get current user
    $user = \Sentry::user();
    $master_user = false;

    if(isset($user['metadata']['master']) && $user['metadata']['master']) 
        $master_user = true;

?>

<ul class="list-details">
    <li <?php echo $active == 'dashboard' ? 'class="active"' : ''; ?>><a href="<?php echo \Uri::front_create('user/account/dashboard'); ?>" class="">Account</a></li>
    <li <?php echo $active == 'orders' ? 'class="active"' : ''; ?>><a href="<?php echo \Uri::front_create('user/account/orders'); ?>" class="">Orders</a></li>
    <?php if(strtolower(\Sentry::user()->groups()[0]['name']) == 'club members'): ?>
        <li <?php echo $active == 'orders' ? 'class="active"' : ''; ?>><a href="<?php echo \Uri::front_create('user/account/referrals'); ?>" class="">Referrals</a></li>
        <li><a href="<?php echo \Uri::front_create('referrals'); ?>" class="">Submit A Referral</a></li>
    <?php endif; ?>
    <?php if($master_user): ?>
        <li <?php echo $active == 'users' ? 'class="active"' : ''; ?>><a href="<?php echo \Uri::front_create('user/account/users'); ?>" class="">Users</a></li>
    <?php endif; ?>
</ul>
