<div class="row-fluid">
                
    <?php echo $theme->view('views/_partials/sidebar'); ?>

    <div class="main_area">

        <div class="breadcrumbs_holder">
            <div class="content_holder">
                <?php 
                    \Breadcrumb::set('Home', '/');
                    \Breadcrumb::set('User', 'user/account');
                    \Breadcrumb::set('Users', 'user/account/users');
                    \Breadcrumb::set('Create', 'user/account/create');
                    echo \Breadcrumb::create_links();
                ?>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="title_holder">
            <div class="content_holder">
                <h1>Create User</h1>
            </div>
        </div>

        <div class="content_wrapper gradient forms_gradient">

            <div class="content_holder">
                
                <div class="row-fluid">
                    
                    <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => 'users')); ?>
                    
                    <div class="span9">

                        <?php echo \Form::open(array('action' => \Uri::front('current'), 'data-validate' => 'parsley'), array('details' => 1, 'user_group' => $user_group['id'])); ?>

                            <?php echo $theme->view('views/user/_form', array('create' => true), false); ?>

                        <?php echo \Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>