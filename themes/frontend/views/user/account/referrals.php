<div class="app-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
              <div class="label-heading">
                  <h2 class="label-title">REFERRALS</h2>
                  <p>Your referral list.</p>
              </div>
            </div>
        </div>

        <div class="app-content">
            <div class="row">
                <div class="col-sm-4">
                    <div class="l-sidebar">
                          <?php echo \Theme::instance()->view('views/user/account/_sidebar', array('active' => 'dashboard')); ?>
                      </div>
                </div>

                <div class="col-sm-8">
                    <table class="table table-l3">
                        <thead>
                            <tr>
                                <td colspan="7">Referrals</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Phone</td>
                                <td>Suburb</td>
                            </tr>
                            <?php if(!empty($items)): ?>
                                <?php foreach($items as $item): ?>
                                    <tr>
                                        <td><?php echo $item->name; ?></td>
                                        <td><?php echo $item->email; ?></td>
                                        <td><?php echo $item->phone; ?></td>
                                        <td><?php echo $item->suburb; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td align="center" colspan="7">There are no orders yet.</td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7" align="left">
                                     <?php echo $pagination; ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>