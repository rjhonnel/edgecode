<?php 
    // Make sure we have $jobs variable defined and that its array
    isset($jobs) && $jobs or $jobs = array();
?>

<div class="section job_ads_table">
    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="cartTable dashboard_table">
        <tbody>
            <tr>
                <th scope="col">REF</th>
                <th scope="col">DATE CREATED</th>
                <th scope="col">JOB TITLE</th>
                <th scope="col">EXPIRATION DATE</th>
                <th scope="col">STATUS</th>
                <th scope="col"><?php echo $theme->asset->img('eye.png', array('width' => 19, 'height' => 11, 'alt' => '')); ?></th>
            </tr>
            <?php if($jobs): ?>
                <?php foreach($jobs as $job): ?>
                    <tr>
                        <td><?php echo $job->reference; ?></td>
                        <td title="<?php echo date('jS \o\f F, Y', $job->active_from ?: $job->created_at); ?>"><?php echo date('d M Y', $job->active_from ?: $job->created_at); ?></td>
                        <td title="<?php echo $job->search_title; ?>"><?php echo $job->title; ?></td>
                        <td title="<?php echo $job->active_to ? date('jS \o\f F, Y', $job->active_to) : ''; ?>">
                            <?php echo $job->active_to ? date('d M Y', $job->active_to) : 'Never'; ?>

                            <?php if($job->active_to && $job->active_to > strtotime('now')): ?>
                                <br /><small>(<?php echo ceil(abs($job->active_to - strtotime('now')) / 86400); ?> Days remaining)</small>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php 
                                if($job->status == 1 || ($job->status == 2 && $job->active_to >= strtotime('now')))
                                    echo 'Active';
                                else
                                    echo 'Expired';
                            ?>
                        </td>
                        <td><a href="<?php echo \Uri::front_create('user/account/jobs/' . $job->seo->slug); ?>" class="arrowLink right">View</a></td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr>
                    <td colspan="6"><center>You have no job ads at the moment</center></td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>