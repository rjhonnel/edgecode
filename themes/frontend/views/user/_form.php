    <?php 
        \Config::load('user', 'user', false);
    ?>
    
    <?php isset($user) or $user = false; ?>
    <?php isset($edit) or $edit = false; ?>
    <?php isset($create) or $create = false; ?>

<!-- Fill Up Form for User Info - Partial view -->
<tbody>
    <tr>
        <td>
            <?php echo \Form::label('First Name' . ' <span class="req">*</span>'); ?>
        </td>
        <td>
            <?php echo \Form::input('first_name', \Input::post('first_name', $user ? $user->get('metadata.first_name') : ''), array("class" => "required form-control", 'data-trigger' => 'change', 'tabindex' => '3')); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo \Form::label('Last Name' . ' <span class="req">*</span>'); ?>
        </td>
        <td>
            <?php echo \Form::input('last_name', \Input::post('last_name', $user ? $user->get('metadata.last_name') : ''), array("class" => "required form-control", 'data-trigger' => 'change', 'tabindex' => '4')); ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo \Form::label('Company Name'); ?>
        </td>
        <td>
             <?php echo \Form::input('business_name', \Input::post('business_name', $user ? $user->get('metadata.business_name') : ''), array("class" => "required form-control", 'data-trigger' => 'change', 'tabindex' => '1')); ?>
        </td>
    </tr>
    <tr>
       <td> <?php echo \Form::label('Address'. ' <span class="req">*</span>'); ?></td>
        <td>
            <?php echo \Form::input('address', \Input::post('address', $user ? $user->get('metadata.address') : ''), array("class" => "required form-control", 'tabindex' => '11')); ?>
        </td>
    </tr>
    <tr>
       <td> <?php echo \Form::label('Address 2'); ?></td>
        <td>
            <?php echo \Form::input('address2', \Input::post('address2', $user ? $user->get('metadata.address2') : ''), array("class" => "form-control", 'tabindex' => '11')); ?>
        </td>
    </tr>
    <tr>
        <td><?php echo \Form::label('Suburb' . ' <span class="req">*</span>'); ?></td>
        <td>
            <?php echo \Form::input('suburb', \Input::post('suburb', $user ? $user->get('metadata.suburb') : ''), array("class" => "required form-control", 'data-trigger' => 'change', 'tabindex' => '12')); ?>
        </td>
    </tr>
    <tr>
        <td><?php echo \Form::label('State'. ' <span class="req">*</span>'); ?></td>
        <td>
            <?php echo \Form::select('state', \Input::post('state', $user ? $user->get('metadata.state') : ''), \Config::get('user.states', array()), array('class' => 'select_init w_state_popup form-control required', 'tabindex' => '13')); ?>
        </td>
    </tr>
    <tr>
       <td> <?php echo \Form::label('Postcode' . ' <span class="req">*</span>'); ?></td>
       <td>
           <?php echo \Form::input('postcode', \Input::post('postcode', $user ? $user->get('metadata.postcode') : ''), array("class" => "required form-control", 'data-trigger' => 'change', 'tabindex' => '14')); ?>
       </td>
    </tr>
    <tr>
        <td><?php echo \Form::label('Phone' . ' <span class="req">*</span>'); ?></td>
        <td>
            <?php echo \Form::input('phone', \Input::post('phone', $user ? $user->get('metadata.phone') : ''), array("class" => "required form-control", 'data-trigger' => 'change', 'tabindex' => '15')); ?>
        </td>
    </tr>
    <tr>
        <td><?php echo \Form::label('Email Address' . ' <span class="req">*</span>'); ?></td>
        <td>
             <?php echo \Form::input('email', \Input::post('email', $user ? $user->get('email') : ''), array("class" => "required form-control", 'data-type' => 'email', 'data-trigger' => 'change', 'tabindex' => '5')); ?>
        </td>
    </tr>
    <tr>
        <td><?php echo \Form::label('Password' . ' <span class="req">*</span>'); ?></td>
       <td>
            <?php
            $validation = array();

            if(!$edit) 
                $validation['class'] = 'required form-control'; 

            $validation['data-minlength'] = '6'; 
            $validation['data-trigger'] = 'change'; 
            $validation['tabindex'] = '6';
            $validation['class'] = 'form-control'; 
        ?>
        <?php echo \Form::password('password', \Input::post('password'), $validation); ?>
       </td>
    </tr>
    <tr>
       <td><?php echo \Form::label('Re-Type Password' . ' <span class="req">*</span>'); ?></td>
        <td>
             <?php
                $validation = array();

                if(!$edit) 
                    $validation['class'] = 'required form-control'; 

                $validation['data-equalto'] = '#form_password'; 
                $validation['data-trigger'] = 'change'; 
                $validation['tabindex'] = '7';
                $validation['class'] = 'form-control'; 
            ?>
            <?php echo \Form::password('confirm_password', \Input::post('password_confirm'), $validation); ?>
        </td>
    </tr>
</tbody>
 <tfoot>
    <tr>
        <td colspan="2">
            <?php echo \Form::button(array('type' => 'submit', 'value' => '<span>' . ($create ? 'Create' : ($edit ? 'Save Details' : 'Sign Up')) . '</span>', 'class' => 'btn btn-green btn-block')); ?>
        </td>
    </tr>
</tfoot>
<!-- EOF Fill Up Form for User Info - Partial view -->