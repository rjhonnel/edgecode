						<?php 
							
                            // Get all users and remove admin users from array
                            $users = \SentryAdmin::user()->all('frontend');
                        
							$user = isset($user) ? $user : false;
							$selected = isset($selected) ? $selected : false;
							
						?>
						
                        <div class="side_tree_holder">
                            <div class="tree_heading">
                                <h4>Users</h4>
                            </div>
                            <div class="tree_content">
                                <div id="sidetree">
                                
                                	<?php if(empty($users)): ?>
                                		<div class="wide"><span class="req">Note: </span> There are no users yet.</div>
                                	<?php else: ?>
                                	
	                                    <ul class="treeview" id="tree">
	                                    	
	                                    	<?php
	                                    		if(!empty($users))
	                                    		{
		                                    		foreach($users as $key => $user_item)
		                                    		{
                                                        $user_item = (Object)$user_item;
                                                        $user_item = \SentryAdmin::user((int)$user_item->id);
                                                        
                                                        ?>
                                                            <li>
                                                                <div class="radio_link_holder">
                                                                    <a href="<?php echo \Uri::create('admin/order/list/' . $user_item->id); ?>" <?php echo $selected == $user_item->id ? 'class="active"' : ''; ?>>
                                                                        <?php echo $user_item->get('metadata.first_name') . ' ' . $user_item->get('metadata.last_name'); ?>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        <?php
		                                    		}
	                                    		}
	                                    	?>
	                                        
	                                    </ul>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
