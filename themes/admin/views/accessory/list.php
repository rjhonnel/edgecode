<div class="main-content">

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Accessories');
        \Breadcrumb::set('Manage Accessories', 'admin/accessory/list');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner">

            <header class="main-content-heading">
                <h4 class="pull-left">Accessories</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/accessory/_action_links'); ?>
                </div>
            </header>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">List</h3>

                    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
                    <div class="form-inline pull-right">
                        <label>Show entries:</label>
                        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'select_init items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                    </div>
                    <?php echo \Form::close(); ?>



                </div>
                <div class="panel-body">
                    <?php
                    // Load page listing table
                    echo \Theme::instance()->view('views/accessory/_listing_table',
                        array(
                            'pagination' 	=> $pagination,
                            'items'			=> $items,
                        ),
                        false
                    );
                    ?>
                </div>
            </div>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
