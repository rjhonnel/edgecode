<?php 
// Configs
$invoice = array('' => '-') + \Config::get('details.invoice', array());
$status = array('' => '-') + \Config::get('details.status', array());
$delivery = array('' => '-') + \Config::get('details.delivery', array());
?>								
<div class="panelContent">

    <?php 
        isset($filters) or $filters = true;
        
        if($filters)
        {
            echo \Theme::instance()->view('views/_partials/search_filters', array(
                'pagination' => $pagination,
                'module' => 'company and full',
                'options' => array(
                    //'order_date', 
                    //'order_total', 
                    //'order_status', 
                    //'delivery_status', 
                    //'invoice_status', 
                    //'user_group', 
                    //'sch_delivery',
                    
                    //'business_name',
                    //'discount_coupon',
                    //'payment_type',
                    //'payment_status',
                    //'shipping_status',
                    //'country_select',
                    'product_category',
                    'product_group',
                    'product_status',
                    'accessories_price',
                    'accessories_date',
                    //'state_select',
                    ),
            ), false);
        }
    ?>
    
    <?php 
        echo \Theme::instance()->view('views/_partials/action_header'); 
    ?>

    <table class="grid greyTable2 separated" width="100%">
        <thead>
            <tr class="blueTableHead">
                <th scope="col" style="width: 40px;"></th>
                <th scope="col" class="center" style="width: 40px;">Code</th>
<!--                <th scope="col" class="center" style="width: 80px;">Purchase Order</th>-->
                <th scope="col">Product Name</th>
                <th scope="col" class="noresize">Status</th>
                <th scope="col" class="center" style="width: 40px;">View</th>
                <th scope="col" class="center" style="width: 40px;">Delete</th>
            </tr>
        </thead>
        <tbody>
            
            <?php foreach($items as $item): ?>

                <?php $item_info = \Order\Model_Order::order_info($item->id); ?>
                <tr>
                    <td class="noresize">
                        <?php 
                            echo \Form::hidden('action[' . $item->id . ']', 0);
                            echo \Form::checkbox('action[' . $item->id . ']', 1, null, array('class' => 'check_active', 'data-id' => $item->id));
                        ?>
                    </td>
<!--                    <td class="center"><?php// echo $item->number; ?></td> -->
                    <td class="center"><?php echo $item->purchase_number; ?></td>
                    <td>
                        <?php 
                            $names = array('Red Eleven Slim Fit Suit Jacket', 'Canvas Backpack With Contrast Straps', 'Bellfield Cjecked Shirt', 'Roche Run Flywire Trainers'); 
                            echo $names[(int)rand(0, 3)];
                        ?>
                    </td>
                    <td>
                        <?php 
                            $names = array('Active', 'Inactive'); 
                            echo $names[(int)rand(0, 1)];
                        ?>
                    </td>
                    <td class="icon center">
                        <a href="<?php echo \Uri::create('admin/accessory/update/' . $item->id); ?>">
                            Update
                        </a>
                    </td>
                    <td class="icon center">
                        <a href="#">
                            Delete
                        </a>
                    </td>
                </tr>

            <?php endforeach; ?>

            <?php if(empty($items)): ?>

                <tr class="nodrag nodrop">
                    <td colspan="10" class="center"><strong>There are no orders.</strong></td>
                </tr>

            <?php endif; ?>

        </tbody>
    </table>
    
    <div class="pagination_holder">
        <?php echo $pagination->render(); ?>
    </div>
    <div class="clear"></div>
</div>