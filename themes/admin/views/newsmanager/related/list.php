                                            <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Applications')); ?>
            
            <?php echo \Theme::instance()->view('views/newsmanager/_navbar_links', array('application' => $application)); ?>
            
            <!-- Content -->
		    <div class="content_wrapper">
		    	<div class="content_holder">
		    		
		    		<div class="elements_holder">
		    		
                        <div class="row-fluid breadcrumbs_holder">
                            <div class="breadcrumbs_position">
                                <?php 
                                    \Breadcrumb::set('Home', 'admin/dashboard');
                                        \Breadcrumb::set('Content Manager');
                                        \Breadcrumb::set('Manage Applications', 'admin/newscategory/list');
                                        \Breadcrumb::set('Edit Application');
                                        \Breadcrumb::set($application->title, 'admin/application/update/' . $application->id);
                                        \Breadcrumb::set('Related Products');

                                        echo \Breadcrumb::create_links();
                                ?>
                                
                                <?php echo \Theme::instance()->view('views/newsmanager/_action_links'); ?>
                            </div>
                        </div>
					    
					    <div class="row-fluid">
					    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                            
		                    <!-- Main Content Holder -->
		                    <div class="content">
								
		                        <div class="panel">
		                        	<div class="panelHeader">
		                                <h4>Edit Related Products</h4>
		                            </div>
		                            <div class="panelContent">
		                            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_b_10">
		                            		<tr>
		                            			<td class="span7 p_r_10">
		                            				 <span class="text-danger">*</span> You can select and drag items from left to right, or select and click on button above.<br /><br />
		                            			</td>
		                            			<td class="span5 sort_message_container"></td>
		                            		</tr>
		                            	</table>
		                            	
                                        <table width="100%">
                                            <tr>
                                                <td width="49.5%" valign="top" class="dual_filters">
                                                    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
                                                        <table class="controls">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <h4>Select Related Products</h4>

                                                                        <div class="items_per_page_holder">
                                                                            <label>Show entries:</label>
                                                                            <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'select_init items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <?php 
                                                            echo \Theme::instance()->view('views/_partials/search_filters', array(
                                                                'pagination' => $pagination,
                                                                'status' => $status,
                                                                'module' => 'product',
                                                                'options' => array('status', 'category_id'),
                                                            ), false);
                                                        ?>
                                                    <?php echo \Form::close(); ?>
                                                </td>

                                                <td width="1%" class="noresize"></td>

                                                <td width="49.5%" valign="top" class="wide_search_filters gray_gradient search_holder">
                                                    <table class="controls">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <h4>Related Products</h4>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <?php 
                                                        echo \Theme::instance()->view('views/_partials/search_filters', array(
                                                            'pagination' => $pagination,
                                                            'status' => $status,
                                                            'module' => 'product',
                                                            'options' => array(),
                                                            'show_reset' => false,
                                                        ), false);
                                                    ?>

                                                    <div class="formRow info_table_loading" style="display: none;">
                                                        <div class="search_loader">
                                                            <!-- This is loaded while user is typing -->
                                                            <div class="clear"></div>
                                                            <div class="m_t_15"></div>
                                                            <center style="margin: 0px 0 19px 0;">Searching for products...</center>
                                                            <center>
                                                                <i class="fa fa-spinner fa-span"></i>
                                                            </center>
                                                        </div>
                                                    </div>
                                                </td>	
                                            </tr>
                                            <tr>
                                                <?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>
                                                    <td width="49.5%" valign="top">

                                                        <div class="row-fluid wide_all">

                                                            <ul class="default_list">
                                                                <li><a href="" onclick="return false;" class="select_all">Select All</a></li>
                                                                <li class="separator">/</li>
                                                                <li><a href="" onclick="return false;" class="unselect_all">Unselect All</a></li>
                                                                <li class="separator">/</li>
                                                                <li><a href="" onclick="return false;" class="select_qty"><span>0</span> items selected</a></li>
                                                            </ul>

                                                            <button name="add" value="add" type="submit" class="btn btn-small btn-primary pull-right">
                                                                <i class="icon-plus icon-white"></i>Add Selected
                                                            </button>
                                                        </div>

                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 separated scrollable_body">
                                                            <thead>
                                                                <tr class="blueTableHead">
                                                                    <th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'products[add]'); ?></th>
                                                                    <th scope="col" class="noresize">Code</th>
                                                                    <th scope="col">Title</th>
                                                                    <th scope="col" class="noresize">Price</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="drag_table remove">
                                                                <?php if(!empty($application->not_related_products)): ?>
                                                                    <?php foreach($application->not_related_products as $not_related): ?>
                                                                        <?php $product_data = $not_related->data; ?>

                                                                        <tr>
                                                                            <td class="noresize"><?php echo \Form::checkbox('products[add][]', $not_related->id); ?></td>
                                                                            <td class="noresize"><?php echo $product_data['code']; ?></td>
                                                                            <td><?php echo $not_related->title; ?></td>
                                                                            <td class="noresize"><?php echo $product_data['price'] ? nf($product_data['price']) : '-';  ?></td>
                                                                        </tr>

                                                                    <?php endforeach; ?>
                                                                    <?php $no_items = 'style="display: none;"'?>
                                                                <?php else: ?>
                                                                    <?php $no_items = ''?>
                                                                <?php endif; ?>

                                                                <tr class="no_items" <?php echo $no_items; ?>>
                                                                    <td class="noresize center" colspan="4">There are no more products to add</td>
                                                                </tr>

                                                            </tbody>                    
                                                        </table>
                                                        
                                                        <div class="pagination_holder">
                                                            <?php echo $pagination->render(); ?>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </td>

                                                    <td width="1%" class="noresize"></td>

                                                    <td width="49.5%" valign="top">

                                                        <div class="row-fluid wide_all">
                                                            <ul class="default_list">
                                                                <li><a href="" onclick="return false;" class="select_all">Select All</a></li>
                                                                <li class="separator">/</li>
                                                                <li><a href="" onclick="return false;" class="unselect_all">Unselect All</a></li>
                                                                <li class="separator">/</li>
                                                                <li><a href="" onclick="return false;" class="select_qty"><span>0</span> items selected</a></li>
                                                            </ul>

                                                            <button name="remove" value="remove" type="submit" class="btn btn-small btn-primary pull-right">
                                                                <i class="icon-minus icon-white"></i>Remove Selected
                                                            </button>
                                                        </div>

                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 separated scrollable_body search_table">
                                                            <thead>
                                                                <tr class="blueTableHead">
                                                                    <th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'products[remove]'); ?></th>
                                                                    <th scope="col" class="noresize">Code</th>
                                                                    <th scope="col">Title</th>
                                                                    <th scope="col" class="noresize">Price</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="drag_table add" style="min-height: <?php echo count($application->not_related_products)*56; ?>px">
                                                                <?php if(!empty($application->related_products)): ?>
                                                                    <?php foreach($application->related_products as $related): ?>
                                                                        <?php $product_data = $related->data; ?>
                                                                        <tr>
                                                                            <td class="noresize"><?php echo \Form::checkbox('products[remove][]', $related->id); ?></td>
                                                                            <td class="noresize"><?php echo $product_data['code']; ?></td>
                                                                            <td><?php echo $related->title; ?></td>
                                                                            <td class="noresize"><?php echo $product_data['price'] ? nf($product_data['price']) : '-';  ?></td>
                                                                        </tr>

                                                                    <?php endforeach; ?>
                                                                    <?php $no_items = 'style="display: none;"'; ?>
                                                                <?php else: ?>
                                                                    <?php $no_items = ''; ?>
                                                                <?php endif; ?>

                                                                <tr class="no_items" <?php echo $no_items; ?>>
                                                                    <td class="noresize center" colspan="4">There are no related products</td>
                                                                </tr>
                                                            </tbody>                    
                                                        </table>
                                                    </td>
                                                <?php echo \Form::close(); ?>
                                            </tr>
                                        </table>
		                            	
		                            </div>
		                        </div>
		                        
		                    </div><!-- EOF Main Content Holder -->
		                    
                            <!-- Sidebar Holder -->
<!--		                    <div class="sidebar">
		                        <?php echo \Theme::instance()->view('views/newsmanager/_tree_links'); ?>
		                    </div>-->
                            <!-- EOF Sidebar Holder -->
                            
		                    <div class="clear"></div>
					    	
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <!-- EOF Content -->
			    
		    <script>
				var product_id = <?php echo $application->id; ?>;
			</script>
			<?php echo \Theme::instance()->asset->js('product/related/drag_lists.js'); ?>
           	
                            