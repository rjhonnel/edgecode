<?php \Config::load('application::casestudy', 'casestudy', true); ?>
<?php \Config::load('application::accordion', 'accordion', true); ?>

<?php if(isset($casestudy)): // Used on "UPDATE" pages?>
    <script type="text/javascript">
        // Select current tab
        $(document).ready(function(){
            if($(".btn-group").length > 0)
            {
                $(".btn-group a.active").removeClass('active');
                $(".btn-group a").each(function(){
                    if(($(this).attr("href") == uri_current) ||
                        (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                    {
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>
    <div class="panel-nav-holder">
        <div class="btn-group">
            <a href="<?php echo \Uri::create('admin/application/casestudy/update/' . $casestudy->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-info-circle"></i> General Information</a>
            <?php if(\Config::get('casestudy.file.enabled', false) || \Config::get('casestudy.video.enabled', false)): ?>
                <?php
                $title = array();
                \Config::get('casestudy.file.enabled', false) and array_push($title, 'Documents');
                \Config::get('casestudy.video.enabled', false) and array_push($title, 'Videos');
                $title = implode(' & ', $title);
                ?>
                <a href="<?php echo \Uri::create('admin/application/casestudy/update_files/' . $casestudy->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-file"></i> <?php echo $title; ?></a>
            <?php endif; ?>
            <?php if(\Config::get('accordion.enabled', false)): ?>
                <a href="<?php echo \Uri::create('admin/application/casestudy/accordion/list/' . $casestudy->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-list"></i> Accordions <?php echo count($casestudy->accordions) > 0 ? '<label class="label label-primary">' . count($casestudy->accordions) . '</label>' : ''; ?></a>
            <?php endif; ?>
        </div>
    </div>
<?php else: // Used on "CREATE" casestudy ?>
    <?php if(FALSE): ?>
        <div class="panel-nav-holder">
            <div class="btn-group">
                <a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>