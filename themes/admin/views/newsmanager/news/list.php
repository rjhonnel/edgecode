<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('News Feed');
		\Breadcrumb::set('News Post', 'admin/newslist');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">News Post</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/newsmanager/news/_action_links', array('hide_show_all' => 1)); ?>
				</div>
			</header>

			<div class="row">
				<div class="col-md-9">
					<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
					<?php
					// Load case study listing table
					echo \Theme::instance()->view('views/newsmanager/news/_listing_table',
						array(
							'pagination' 	=> $pagination,
							'items'			=> $items,
							'status'		=> $status,
						),
						false
					);
					?>
					<?php echo \Form::close(); ?>
				</div>
				<div class="col-md-3" style="padding-left:0">
					<?php echo \Theme::instance()->view('views/newsmanager/news/_tree_links'); ?>
				</div>
			</div>



		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
                            