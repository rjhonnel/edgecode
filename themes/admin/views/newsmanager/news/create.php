<div class="main-content">

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('News Feed');
		\Breadcrumb::set('News Post', 'admin/newslist');
		\Breadcrumb::set('Add New');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner">

			<header class="main-content-heading">
				<h4 class="pull-left">Add News</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/newsmanager/news/_action_links', array('create_form' => 1, 'hide_add_new'=>1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/newsmanager/news/_navbar_links'); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="row">
				<div class="col-sm-9">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">General Information</h3>
						</div>
						<div class="panel-body">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">News Name  <span class="text-danger">*</span></label>
									<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title'), array('class'=>'form-control')); ?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Small Description:</label>
									<div class="col-sm-10"><?php echo \Form::textarea('description_intro', \Input::post('description_intro'), array('class'=>'form-control')); ?></div>
								</div>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Description' ); ?>
								<div class="clear"></div>
								<?php echo \Form::textarea('description_full', \Input::post('description_full'), array('class' => 'form-control ck_editor')); ?>
							</div>
						</div>
					</div>

				</div>
				<div class="col-sm-3" style="padding-left:0">

					<div class="form-group">
						<label class="">Status:</label>
						<?php echo \Form::select('status', \Input::post('status', '1'), array(
							'1' => 'Active',
							'0' => 'Inactive',
							'2' => 'Active in Period',
						), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
					</div>
					<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>
						<label">Dates Active:</label>
						<div class="datepicker-holder-control">
							<?php echo \Form::input('active_from', \Input::post('active_from'), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
						</div>
						<div class="datepicker-holder-control mt-5px">
							<?php echo \Form::input('active_to', \Input::post('active_to'), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
						</div>
					</div>

					<?php
					$casestudy = new stdClass();
					$casestudy->id 			= 0;
					$casestudy->parent_id 	= 0;
					?>
					<?php echo \Theme::instance()->view('views/newsmanager/news/_tree_links', array('application' => $casestudy)); ?>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>
				</div>
				<div class="panel-body">

					<table class="table table-striped table-bordered ">
						<tr class="nodrop nodrag blueTableHead">
							<th scope="col" class="noresize">Image</th>
							<th scope="col">Image Properties</th>
						</tr>
						<tr>
							<td class="td-thumb">
								<div class="fa fa-picture-o"></div>
							</td>
							<td class="upload">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="noresize">Alt Text</td>
										<td>
											<div class="input_holder">
												<?php echo \Form::input('alt_text', \Input::post('alt_text'), array('class' => 'form-control')); ?>
											</div>
										</td>
									</tr>
									<tr>
										<td class="noresize">Replace Image</td>
										<td>
											<?php echo \Form::file('image'); ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>

			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
				<?php echo \Form::button('update', '<i class="fa fa-check"></i> Save & Update', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<?php echo ckeditor_replace('ck_editor'); ?>



