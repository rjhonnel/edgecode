<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Application Manager');
        \Breadcrumb::set('Manage Case Studies', 'admin/application/casestudy/list');
        \Breadcrumb::set('Edit Case Study');
        \Breadcrumb::set($casestudy->title, 'admin/application/casestudy/update/' . $casestudy->id);
        \Breadcrumb::set('Sub Case Studies');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">title here</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/newsmanager/news/_action_links'); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/newsmanager/news/_navbar_links', array('application' => $casestudy)); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Edit News: Videos</h3>

                </div>
                <div class="panel-body">
                    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
                    <!-- Accordions Panel -->
                    <div class="panel">
                        <div class="panelHeader">
                            <h4>Application Manager</h4>

                            <div class="items_per_page_holder">
                                <label>Show entries:</label>
                                <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'select_init items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                            </div>
                        </div>

                        <?php
                        // Load application listing table
                        echo \Theme::instance()->view('views/newsmanager/news/_listing_table',
                            array(
                                'pagination' 	=> $pagination,
                                'items'			=> $items,
                                'status'		=> $status,
                            ),
                            false
                        );
                        ?>

                    </div><!-- EOF Accordions Panel -->
                    <?php echo \Form::close(); ?>
                </div>
            </div>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

