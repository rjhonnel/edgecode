	            <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Case Studies')); ?>

                <?php echo \Theme::instance()->view('views/newsmanager/news/_navbar_links', array('application' => $casestudy)); ?>

	            <!-- Content -->
			    <div class="content_wrapper">
			    	<div class="content_holder">

			    		<div class="elements_holder">

                            <div class="row-fluid breadcrumbs_holder">
			    				<div class="breadcrumbs_position">
                                    <?php
                                        \Breadcrumb::set('Home', 'admin/dashboard');
                                        \Breadcrumb::set('Application Manager');
                                        \Breadcrumb::set('Manage Case Studies', 'admin/application/casestudy/list');
                                        \Breadcrumb::set('Edit Case Study');
                                        \Breadcrumb::set($casestudy->title, 'admin/application/casestudy/update/' . $casestudy->id);
                                        \Breadcrumb::set('Meta Content');

                                        echo \Breadcrumb::create_links();
                                    ?>

                                    <?php echo \Theme::instance()->view('views/newsmanager/news/_action_links'); ?>
                                </div>
                            </div>

						    <div class="row-fluid">
                                <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>

						    	<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

				                    <!-- Main Content Holder -->
				                    <div class="content">

				                        <!-- Case Study Optimisation Panel -->
				                        <div class="panel">
				                            <div class="panelHeader">
				                            	<div class="togglePanel"><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-minus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-plus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a></div>
				                                <h4>Case Study Optimisation</h4>
				                            </div>
				                            <div class="panelContent">
				                                <div class="row-fluid">
				                                	<div class="formRow">
				                                    	<label>Meta Title  <span class="text-danger">*</span></label>
				                                        <div class="input_holder">
				                                        	<?php echo \Form::input('meta_title', \Input::post('meta_title', $casestudy->seo->meta_title ?: $casestudy->title)); ?>
				                                            <small class="form-instruction">Try to keep your case study under 70 characters.</small>
				                                        </div>
				                                    </div>
				                                    <div class="formRow">
				                                    	<label>Case Study URL</label>
				                                        <div class="input_holder">
				                                        	<?php echo \Form::input('slug', \Input::post('slug', $casestudy->seo->slug), array('class' => 'slug_source slug_target')); ?>
				                                        </div>
				                                    </div>
				                                    <div class="formRow">
				                                    	<label>Meta Keywords</label>
				                                        <div class="input_holder">
				                                        	<?php echo \Form::textarea('meta_keywords', \Input::post('meta_keywords', $casestudy->seo->meta_keywords), array('rows' => 2)); ?>
				                                            <small class="form-instruction">Input keywords, separated by comma (,). Ensure the keywords inputed are reflected in the case study content.</small>
				                                        </div>
				                                    </div>
				                                    <div class="formRow">
				                                    	<label>Meta Description</label>
				                                        <div class="input_holder">
				                                        	<?php echo \Form::textarea('meta_description', \Input::post('meta_description', $casestudy->seo->meta_description), array('rows' => 2)); ?>
				                                            <small class="form-instruction">Best practices:<br />&bull; Please provide a concise description of what the content of this case study is about, in 155 characters or less.<br />&bull; Include your main targeted keyword.<br />&bull; Ensure descriptions are unique.</small>
				                                        </div>
				                                    </div>
				                                    <div class="formRow">
				                                    	<label>H1 Tag</label>
				                                        <div class="input_holder">
				                                        	<?php echo \Form::input('h1_tag', \Input::post('h1_tag', $casestudy->seo->h1_tag ?: $casestudy->title)); ?>
				                                            <small class="form-instruction">Ensure Case Study Heading Tag is unique.</small>
				                                        </div>
				                                    </div>
				                                </div>
				                            </div>
				                        </div><!-- EOF Case Study Optimisation Panel -->

				                        <!-- Redirects Panel -->
				                        <div class="panel">
				                            <div class="panelHeader">
				                            	<div class="togglePanel closed"><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-minus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-plus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a></div>
				                                <h4>Redirects</h4>
				                            </div>
				                            <div class="panelContent">
				                               <div class="row-fluid">
				                                	<div class="formRow">
				                                    	<label>301 Redirect from</label>
				                                        <div class="input_holder">
				                                        	<?php echo \Form::input('redirect_301', \Input::post('redirect_301', $casestudy->seo->redirect_301)); ?>
				                                            <small class="form-instruction">Input the url for this case study previously available on your old website (if applicable); otherwise, leave blank.</small>
				                                        </div>
				                                    </div>
				                                    <div class="formRow">
				                                    	<label>Canonical Links</label>
				                                        <div class="input_holder">
				                                        	<?php echo \Form::input('canonical_links', \Input::post('canonical_links', $casestudy->seo->canonical_links)); ?>
				                                            <small class="form-instruction">Input the duplicate urls (links that displays the same case study) separated by comma (,).<br />Example: http://myurl.com.au/index, http://www.myurl.com.au/index.html/</small>
				                                        </div>
				                                    </div>
				                                </div>
				                            </div>
				                        </div><!-- EOF Redirects Panel -->

				                        <!-- Meta Robots Panel -->
				                        <div class="panel">
				                            <div class="panelHeader">
				                            	<div class="togglePanel closed"><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-minus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-plus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a></div>
				                                <h4>Meta Robots</h4>
				                            </div>
				                            <div class="panelContent">
				                                <div class="row-fluid">
				                                	<div class="formRow">
				                                    	<label>Meta Robots Tag</label>
				                                        <div class="input_holder">
				                                        	<label class="radio"><?php echo \Form::radio('meta_robots_index', 1, \Input::post('meta_robots_index', $casestudy->seo->meta_robots_index)); ?>Index (Default)</label>
		                                                	<label class="radio"><?php echo \Form::radio('meta_robots_index', 0, \Input::post('meta_robots_index', $casestudy->seo->meta_robots_index)); ?>No Index</label>
				                                        </div>
				                                    </div>
				                                    <div class="formRow">
				                                    	<label>Meta Robots Follow</label>
				                                        <div class="input_holder">
				                                        	<label class="radio"><?php echo \Form::radio('meta_robots_follow', 1, \Input::post('meta_robots_follow', $casestudy->seo->meta_robots_follow)); ?>Follow (Default)</label>
		                                                	<label class="radio"><?php echo \Form::radio('meta_robots_follow', 0, \Input::post('meta_robots_follow', $casestudy->seo->meta_robots_follow)); ?>No Follow</label>
				                                        </div>
				                                    </div>
				                                </div>
				                            </div>
				                        </div><!-- EOF Meta Robots Panel -->
				                        <div class="save_button_holder text-right">
				                        	<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
				                        </div>
				                    </div><!-- EOF Main Content Holder -->

                                    <!-- Sidebar Holder -->
<!--				                    <div class="sidebar">
				                        <?php echo \Theme::instance()->view('views/newsmanager/casestudy/_tree_links', array('link' => 'update_seo')); ?>
				                    </div>-->
                                    <!-- EOF Sidebar Holder -->

				                    <div class="clear"></div>
								<?php echo \Form::close(); ?>
						    </div>
			    		</div>
			    	</div>
			    </div>
			    <!-- EOF Content -->


