<?php \Config::load('application::application', 'application', true); ?>
<?php \Config::load('application::accordion', 'accordion', true); ?>

<?php if(isset($application)): // Used on "UPDATE" pages?>
    <div class="panel-nav-holder">
        <div class="btn-group">
            <a href="<?php echo \Uri::create('admin/application/update/' . $application->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-info-circle"></i> General Information</a>
            <?php if(\Config::get('application.file.enabled', false) || \Config::get('application.video.enabled', false)): ?>
                <?php
                $title = array();
                \Config::get('application.file.enabled', false) and array_push($title, 'Documents');
                \Config::get('application.video.enabled', false) and array_push($title, 'Videos');
                $title = implode(' & ', $title);
                ?>
                <a href="<?php echo \Uri::create('admin/application/update_files/' . $application->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-file"></i> <?php echo $title; ?></a>
            <?php endif; ?>

            <?php if(\Config::get('accordion.enabled', false)): ?>
                <a href="<?php echo \Uri::create('admin/application/accordion/list/' . $application->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-list"></i> <?php echo count($application->accordions) > 0 ? count($application->accordions) : '0'; ?> Accordions</a>
            <?php endif; ?>

            <a href="<?php echo \Uri::create('admin/application/hotspot/show/' . $application->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-bullseye"></i> Hotspots</a>
            <a href="<?php echo \Uri::create('admin/application/update_seo/' . $application->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-check-circle"></i> Meta Content</a>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            if($(".btn-group").length > 0)
            {
                $(".btn-group a.active").removeClass('active');
                $(".btn-group a").each(function(){
                    if(($(this).attr("href") == uri_current) ||
                        (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                    {
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>

<?php else: // Used on "CREATE" application ?>
    <?php if(FALSE): ?>
        <div class="second_menu">
            <ul>
                <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
            </ul>
        </div>
    <?php endif; ?>
<?php endif; ?>