<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('News Feed');
		\Breadcrumb::set('News Categories', 'admin/newscategory/list');
		\Breadcrumb::set($application->title, 'admin/newscategory/update/' . $application->id);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Category: General Information</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/newsmanager/_action_links', array('save_button'=>1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/newsmanager/_navbar_links', array('application' => $application)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>


				<div class="row">
					<div class="col-sm-9">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title pull-left">General Information</h3>

							</div>
							<div class="panel-body">
								<div class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label">News Category  <span class="text-danger">*</span></label>
										<?php echo \Form::label(''); ?>
										<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title', $application->title), array('class' => 'form-control')); ?></div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Small Description</label>
										<?php echo \Form::label(''); ?>
										<div class="col-sm-10"><?php echo \Form::textarea('description_intro', \Input::post('description_intro', $application->description_intro), array('class' => 'form-control')); ?></div>
									</div>
								</div>

								<div class="form-group">
									<?php echo \Form::label('Description' ); ?>
									<div class="clear"></div>
									<?php echo \Form::textarea('description_full', \Input::post('description_full', $application->description_full), array('class' => 'form-control ck_editor')); ?>
								</div>

							</div>
						</div>

					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<?php //echo \Form::label('Status', null, array('class' => 'text_right')); ?>
							<?php echo \Form::label('Status', null, array('class' => 'text_right m_r_15')); ?>
							<?php echo \Form::select('status', \Input::post('status', $application->status), array(
								'1' => 'Active',
								'0' => 'Inactive',
								'2' => 'Active in Period',
							), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
						</div>
						<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

							<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
							<div class="datepicker-holder-control">
								<?php echo \Form::input('active_from', \Input::post('active_from', !is_null($application->active_from) ? date('d/m/Y', $application->active_from) : ''), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
							</div>
							<div class="datepicker-holder-control mt-5px">
								<?php echo \Form::input('active_to', \Input::post('active_to', !is_null($application->active_to) ? date('d/m/Y', $application->active_to) : ''), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
							</div>
						</div>
					</div>
				</div>


				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title pull-left">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>

					</div>
					<div class="panel-body">
						<div class="span4 sort_message_container"></div>
						<div class="header_messages"></div>
						<table rel="<?php echo \Uri::create('admin/application/sort/image/' . $application->id); ?>" class="sortable table table-striped table-bordered">
							<tr class="nodrop nodrag blueTableHead">
								<th scope="col" class="noresize">Image</th>
								<th scope="col">Image Properties</th>
								<?php if(is_array($application->images)): ?>
									<?php if(count($application->images) > 1): ?>
										<th>Main Image</th>
										<th>Re-order</th>
									<?php endif; ?>
									<th>Delete</th>
								<?php endif; ?>
							</tr>

							<?php if(is_array($application->images)): ?>
								<?php foreach($application->images as $image): ?>

									<tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
										<td class="center noresize">
											<a href="<?php echo \Helper::amazonFileURL('media/images/' . $image->image); ?>" class="preview-image-popup">
												<img src="<?php echo \Helper::amazonFileURL('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $image->image); ?>" width="130" height="130" alt="<?php echo $image->alt_text; ?>"/>
											</a>
										</td>
										<td class="upload btn-file">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="110">Alt Text</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class' => 'form-control')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td width="110">Replace Image</td>
													<td>
														<?php echo \Form::file('image_'.$image->id); ?>
														<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if(count($application->images) > 1): ?>
											<td width="110" class="text-center">
												<input type="radio" name="cover_image" value="<?php echo $image->id; ?>" <?php echo $image->cover?'checked="checked"':''; ?>>
											</td>
											<td width="120" class="dragHandle">
												<ul class="table-action-inline">

													<li>
														<a href="" onclick="return false;">
															Re-order
														</a>
													</li>

												</ul>
											</td>
										<?php endif; ?>
										<td width="120">
											<ul class="table-action-inline">
												<?php if((!\Config::get('details.image.required', false) && !empty($application->images)) || count($application->images) > 1): ?>
													<li>
														<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/application/delete_image/' . $image->id . '/' . $application->id); ?>">
															Delete
														</a>
													</li>
												<?php endif; ?>

											</ul>
										</td>

									</tr>

								<?php endforeach; ?>
							<?php endif; ?>

							<?php if(\Config::get('details.image.multiple', false) || empty($application->images)): ?>
								<tr class="nodrop nodrag">
									<td class="td-thumb">
										<i class="fa fa-picture-o"></i>
									</td>
									<td class="upload">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="110">Alt Text</td>
												<td>
													<div class="input_holder">
														<?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1'), array('class' => 'form-control')); ?>
													</div>
												</td>
											</tr>
											<tr>
												<td width="110">Choose Image</td>
												<td>
													<?php echo \Form::file('image_new_1'); ?>
												</td>
											</tr>
										</table>
									</td>
									<?php if(count($application->images) > 1): ?>
										<td class="icon center"></td>
										<td class="icon center"></td>
									<?php endif; ?>
									<?php if((!\Config::get('details.image.required', false) && !empty($application->images)) || count($application->images) > 1): ?>
										<td class="icon center"></td>
									<?php endif; ?>
								</tr>
							<?php endif; ?>

						</table>
					</div>
				</div>




				<div class="save_button_holder text-right">
					<?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1', 'id'=>'save_button_down')); ?>
					<?php echo \Form::button('exit', 'Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
				</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>


<?php echo ckeditor_replace('ck_editor'); ?>




