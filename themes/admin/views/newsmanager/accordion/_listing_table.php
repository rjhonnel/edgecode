								
<div class="panelContent">

    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>

        <?php
            // Prep some values needed for grid
            $status = array(
                'false' => 'Select',
                '1' => 'Active',
                '0' => 'Inactive',
                '2' => 'Active in period',
            );
        ?>

        <table rel="<?php echo \Uri::create('admin/application/accordion/sort/accordion'); ?>" class="grid greyTable2 separated sortable_rows" width="100%">
            <thead>
                <tr class="blueTableHead">
                    <th scope="col">Accordion Name</th>
                    <th scope="col" class="center" style="width: 70px;">Status</th>
                    <th scope="col" class="center" style="width: 40px;">Edit</th>
                    <th scope="col" class="center" style="width: 40px;">Delete</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach ($items as $item): ?>
                    <?php $item = (Object) $item; ?>

                    <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
                        <td><?php echo $item->title; ?></td>
                        <td><?php echo $status[$item->status]; ?></td>
                        <td class="icon center">
                            <a href="<?php echo \Uri::create('admin/application/accordion/update/' . $item->id); ?>">
                                Update
                            </a>
                        </td>
                        <td class="icon center">
                            <a href="<?php echo \Uri::create('admin/application/accordion/delete/' . $item->id); ?>">
                                Delete
                            </a>
                        </td>
                    </tr>

                <?php endforeach; ?>

                <?php if (empty($items)): ?>

                    <tr class="nodrag nodrop">
                        <td colspan="4" class="center"><strong>There are no items.</strong></td>
                    </tr>

                <?php endif; ?>

            </tbody>
        </table>

        <div class="buttons">
            <a class="btn btn-primary right" href="<?php echo \Uri::create('admin/application/accordion/create/' . $application->id); ?>"><i class="icon-plus icon-white"></i>Add New Accordion</a>
        </div>

    <?php echo \Form::close(); ?>
</div>