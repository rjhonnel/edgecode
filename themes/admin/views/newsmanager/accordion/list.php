                                            <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Application Accordions')); ?>
            
            <!-- Second menu -->
		    <div class="second_menu_wrapper">
		    	<div class="second_menu_holder">
		    		<div class="elements_holder">
				        <!-- Navigation bar -->
	                    <?php echo \Theme::instance()->view('views/newsmanager/_navbar_links', array('application' => $application)); ?>
	                    <!-- EOF Navigation bar -->
                    </div>
		    	</div>
		    </div>
		    <!-- EOF Second menu -->
            
            <!-- Content -->
		    <div class="content_wrapper">
		    	<div class="content_holder">
		    		
		    		<div class="elements_holder">
		    		
                        <div class="row-fluid breadcrumbs_holder">
                            <div class="breadcrumbs_position">
                                <?php 
                                    \Breadcrumb::set('Home', 'admin/dashboard');
                                    \Breadcrumb::set('Content Manager');
                                    \Breadcrumb::set('Manage Applications', 'admin/newscategory/list');
                                    \Breadcrumb::set('Edit Application');
                                    \Breadcrumb::set($application->title, 'admin/application/update/' . $application->id);
                                    \Breadcrumb::set('Accordions', 'admin/application/accordion/list/' . $application->id);

                                    echo \Breadcrumb::create_links();
                                ?>
		                            
                                <?php echo \Theme::instance()->view('views/newsmanager/_action_links'); ?>
                            </div>
                        </div>
                        
					    <div class="row-fluid">
					    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                            
		                    <!-- Main Content Holder -->
		                    <div class="content">
								
		                        <!-- Accordions Panel -->
		                        <div class="panel">
		                        	<div class="panelHeader">
		                        		<h4>Accordion Manager</h4>
		                            </div>
		                            
	                            	<?php
	                                	// Load application listing table
	                                	echo \Theme::instance()->view('views/newsmanager/accordion/_listing_table', 
		                                	array(
		                                		'application'	=> $application,
		                                		'items'	=> $items,
		                                	), 
		                                	false
		                                ); 
	                                ?>
	                                
		                        </div><!-- EOF Accordions Panel -->
		                        
		                    </div><!-- EOF Main Content Holder -->
		                    
                            <!-- Sidebar Holder -->
<!--		                    <div class="sidebar">
		                        <?php echo \Theme::instance()->view('views/newsmanager/_tree_links', array('link' => 'accordion/list')); ?>
		                    </div>-->
                            <!-- EOF Sidebar Holder -->
                            
		                    <div class="clear"></div>
					    	
					    </div>
					    
		    		</div>
		    		
		    	</div>
		    </div>
		    <!-- EOF Content -->
           	
                            