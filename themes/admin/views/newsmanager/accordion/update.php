				<?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Application Accordions')); ?>
	            
                <?php echo \Theme::instance()->view('views/newsmanager/_navbar_links', array('application' => $application)); ?>
	            
	            <!-- Content -->
			    <div class="content_wrapper">
			    	<div class="content_holder">
			    		
			    		<div class="elements_holder">
			    		
                            <div class="row-fluid breadcrumbs_holder">
                                <div class="breadcrumbs_position">
                                    <?php 
                                        \Breadcrumb::set('Home', 'admin/dashboard');
                                        \Breadcrumb::set('Content Manager');
                                        \Breadcrumb::set('Manage Applications', 'admin/application/list');
                                        \Breadcrumb::set('Edit Application');
                                        \Breadcrumb::set($application->title, 'admin/application/update/' . $application->id);
                                        \Breadcrumb::set('Accordions', 'admin/application/accordion/list/' . $application->id);
                                        \Breadcrumb::set('Edit Accordion');
                                        \Breadcrumb::set($accordion->title);

                                        echo \Breadcrumb::create_links();
                                    ?>
		                            
                                    <?php echo \Theme::instance()->view('views/newsmanager/_action_links'); ?>
                                </div>
                            </div>
                            
						    <div class="row-fluid">
						    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                                
						    	<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>
						    	
				                    <!-- Main Content Holder -->
				                    <div class="content">
				                    	
				                        <!-- Accordions Panel -->
				                        <div class="panel">
				                        	<div class="panelHeader">
				                        		<div class="togglePanel"><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-minus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-plus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a></div>
				                                <h4><i class="panel_information"></i>General Information</h4>
				                            </div>
				                            <div class="panelContent">
			                                	<div class="formRow">
			                                		<div class="span7">
					                                	<div class="formRow">
					                                    	<?php echo \Form::label('Application Title' . '  <span class="text-danger">*</span>'); ?>
					                                        <div class="input_holder"><?php echo \Form::input('title', \Input::post('title', $accordion->title)); ?></div>
					                                    </div>
				                                    </div>
			                                    	<div class="span4 right">
					                                    <div class="formRow">
					                                        <?php echo \Form::label('Status', null, array('class' => 'text_right')); ?>
		                                                	<?php echo \Form::select('status', \Input::post('status', $accordion->status), array(
			                                                	'1' => 'Active',
			                                                	'0' => 'Inactive',
			                                                	'2' => 'Active in Period',
			                                                ), array('class' => 'toggle_dates select_init', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
			                                                
		                                                	<div class="toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>
		                                                	
			                                                	<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
			                                                	<?php echo \Form::input('active_from', \Input::post('active_from', !is_null($accordion->active_from) ? date('m/d/Y', $accordion->active_from) : ''), array('id' => 'from', 'class' => 'dateInput', 'placeholder' => 'From')); ?>
			                                                	<?php echo \Form::input('active_to', \Input::post('active_to', !is_null($accordion->active_to) ? date('m/d/Y', $accordion->active_to) : ''), array('id' => 'to', 'class' => 'dateInput', 'placeholder' => 'To')); ?>
			                                                	
			                                                </div>
					                                    </div>
		                                    		</div>
			                                    </div>
			                                    <div class="formRow">
			                                        <?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
			                                        <div class="clear"></div>
			                                        <?php echo \Form::textarea('description_full', \Input::post('description_full', $accordion->description_full), array('class' => 'form-control ck_editor')); ?>
			                                    </div>
					                            <div class="clear"></div>
				                            </div>
				                        </div><!-- EOF Accordions Panel -->
			                        
				                        <!-- Images Panel -->
				                        <div class="panel">
				                        	<div class="panelHeader">
				                            	<div class="togglePanel"><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-minus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a><a>
			                                		<?php echo \Theme::instance()->asset->img('panelToggle-plus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
			                                	</a></div>
				                                <h4><i class="panel_images"></i>Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h4>
				                            </div>
				                            <div class="panelContent">
				                            	
				                            	<div class="span4 sort_message_container"></div>
												<div class="clear"></div>
				                            	
				                            	<table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/application/accordion/sort/image/' . $accordion->id); ?>" class="greyTable2 sortable files_table separated ">
				                                    <tr class="nodrop nodrag blueTableHead">
				                                        <th scope="col" class="noresize">Image</th>
		                                            	<th scope="col">Image Properties</th>
		                                            	<?php if(count($accordion->images) > 1): ?>
			                                            	<th scope="col" class="center">Re-order</th>
			                                           	<?php endif; ?>
			                                           	<?php if((!\Config::get('details.image.required', false) && !empty($accordion->images)) || count($accordion->images) > 1): ?>
			                                            	<th scope="col" class="center">Delete</th>
			                                            <?php endif; ?>
				                                    </tr>
				                                    
				                                    <?php if(is_array($accordion->images)): ?>
				                                        <?php foreach($accordion->images as $image): ?>
				                                        	
					                                        <tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
					                                            <td class="center noresize">
						                                        	<img src="<?php echo \Uri::create('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $image->image); ?>" width="74" height="74" alt="<?php echo $image->alt_text; ?>"/>
						                                        </td>
					                                            <td class="upload">
					                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                                    <tr>
					                                                        <td class="noresize">Alt Text</td>
					                                                        <td>
					                                                        	<div class="input_holder">
					                                                        		<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text)); ?>
					                                                        	</div>
					                                                        </td>
					                                                    </tr>
					                                                    <tr>
					                                                        <td class="noresize">Replace Image</td>
					                                                        <td>
					                                                        	<?php echo \Form::file('image_'.$image->id); ?>
					                                                        	<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
					                                                        </td>
					                                                    </tr>
					                                                </table>
					                                            </td>
					                                            <?php if(count($accordion->images) > 1): ?>
						                                            <td class="icon center dragHandle">
						                                            	<a href="" onclick="return false;">
																			Order
																		</a>
						                                            </td>
						                                        <?php endif; ?>  
						                                        <?php if((!\Config::get('details.image.required', false) && !empty($accordion->images)) || count($accordion->images) > 1): ?> 
						                                            <td class="icon center">
						                                            	<a class="text-danger" href="<?php echo \Uri::create('admin/application/accordion/delete_image/' . $image->id . '/' . $accordion->id); ?>">
																			Delete
																		</a>
						                                            </td>
						                                        <?php endif; ?>
					                                        </tr>
					                                        
				                                        <?php endforeach; ?>
													<?php endif; ?>
			                                        
			                                        <?php if(\Config::get('details.image.multiple', false) || empty($accordion->images)): ?>
				                                        <tr class="nodrop nodrag">
															<td class="td-thumb">
																<i class="fa fa-picture-o"></i>
															</td>
				                                            <td class="upload">
				                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
				                                                    <tr>
				                                                        <td class="noresize">Alt Text</td>
				                                                        <td>
				                                                        	<div class="input_holder">
						                                                        <?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1')); ?>
				                                                        	</div>
				                                                        </td>
				                                                    </tr>
				                                                    <tr>
				                                                        <td class="noresize">Choose Image</td>
				                                                        <td>
				                                                        	<?php echo \Form::file('image_new_1'); ?>
				                                                        </td>
				                                                    </tr>
				                                                </table>
				                                            </td>
				                                            <?php if(count($accordion->images) > 1): ?>
					                                            <td class="icon center"></td>
					                                        <?php endif; ?>
					                                        <?php if((!\Config::get('details.image.required', false) && !empty($accordion->images)) || count($accordion->images) > 1): ?>
					                                            <td class="icon center"></td>
					                                        <?php endif; ?>
				                                        </tr>
			                                        <?php endif; ?>
				                                    
				                                </table>
				                            	
				                            </div>
				                        </div><!-- EOF Images Panel -->
				                        
	                        			<div class="save_button_holder text-right">
                                            <?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
		                                	<?php echo \Form::button('exit', 'Save & Exit', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
		                                </div>
					                        
				                    </div><!-- EOF Main Content Holder -->
				                    
							    	<!-- Sidebar Holder -->
<!--				                    <div class="sidebar">
				                        <?php echo \Theme::instance()->view('views/newsmanager/_tree_links', array('link' => 'accordion/list', 'selected' => $application->id)); ?>
				                    </div>-->
                                    <!-- EOF Sidebar Holder -->
				                    
				                    <div class="clear"></div>
			                    
						    	<?php echo \Form::close(); ?>
						    	
						    </div>
					    
			    		</div>
			    	</div>
			    </div>
			    <!-- EOF Content -->
	            
	            <?php echo ckeditor_replace('ck_editor'); ?>
	            
            	