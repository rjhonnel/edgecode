<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('News Feed');
		\Breadcrumb::set('News Categories', 'admin/application/list');
		\Breadcrumb::set($application->title, 'admin/application/update/' . $application->id);
		\Breadcrumb::set('Meta Content');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Category: Meta Content</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/newsmanager/_action_links', array('save_button'=>1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/newsmanager/_navbar_links', array('application' => $application)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">News Category Meta Content / SEO</h3>

				</div>
				<div class="panel-body form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">Title  <span class="text-danger">*</span></label>
						<div class="col-sm-10">
							<?php echo \Form::input('meta_title', \Input::post('meta_title', $application->seo->meta_title ?: $application->title), array('class' => 'form-control')); ?>
							<small class="form-instruction">Recommendation: under 70 characters.</small><br>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">URL</label>
						<div class="col-sm-10">
							<?php echo \Form::input('slug', \Input::post('slug', $application->seo->slug), array('class' => 'slug_source slug_target', 'class' => 'form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Keywords</label>
						<div class="col-sm-10">
							<?php echo \Form::textarea('meta_keywords', \Input::post('meta_keywords', $application->seo->meta_keywords), array('rows' => 2, 'class' => 'form-control')); ?>
							<small class="form-instruction">Recommenation: 10 keywords separated by comma.</small>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10">
							<?php echo \Form::textarea('meta_description', \Input::post('meta_description', $application->seo->meta_description), array('rows' => 2, 'class' => 'form-control')); ?>
							<small class="form-instruction">Description of what the content of this page is about. Best practices:<br />&bull; Recommendation: 155 characters or less.<br />&bull; Include your main targeted keyword.<br />&bull; Ensure description is unique.</small><br>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">H1 Tag</label>
						<div class="col-sm-10">
							<?php echo \Form::input('h1_tag', \Input::post('h1_tag', $application->seo->h1_tag ?: $application->title), array('class' => 'form-control')); ?>
							<small class="form-instruction">Product Name is H1 by default. This will replace the Product Name as H1.</small>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Redirects</h3>

				</div>
				<div class="panel-body form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">301 Redirect from</label>
						<div class="col-sm-10">
							<?php echo \Form::input('redirect_301', \Input::post('redirect_301', $application->seo->redirect_301), array('class' => 'form-control')); ?>
							<small class="form-instruction">Input the url for this application previously available on your old website (if applicable); otherwise, leave blank.</small>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Canonical Links</label>
						<div class="col-sm-10">
							<?php echo \Form::input('canonical_links', \Input::post('canonical_links', $application->seo->canonical_links), array('class' => 'form-control')); ?>
							<small class="form-instruction">Input the duplicate urls (links that displays the same application) separated by comma (,).<br />Example: http://myurl.com.au/index, http://www.myurl.com.au/index.html/</small>
						</div>
					</div>
				</div>
			</div><!-- EOF Redirects Panel -->

			<!-- Meta Robots Panel -->

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Robots</h3>

				</div>
				<div class="panel-body form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">Robots Tag</label>
						<div class="col-sm-10">
							<label class="radio"><?php echo \Form::radio('meta_robots_index', 1, \Input::post('meta_robots_index', $application->seo->meta_robots_index)); ?>Index (Default)</label>
							<label class="radio"><?php echo \Form::radio('meta_robots_index', 0, \Input::post('meta_robots_index', $application->seo->meta_robots_index)); ?>No Index</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Robots Follow</label>
						<div class="col-sm-10">
							<label class="radio"><?php echo \Form::radio('meta_robots_follow', 1, \Input::post('meta_robots_follow', $application->seo->meta_robots_follow)); ?>Follow (Default)</label>
							<label class="radio"><?php echo \Form::radio('meta_robots_follow', 0, \Input::post('meta_robots_follow', $application->seo->meta_robots_follow)); ?>No Follow</label>
						</div>
					</div>
				</div>
			</div><!-- EOF Meta Robots Panel -->
			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success', 'id'=>'save_button_down')); ?>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>





	            
	            