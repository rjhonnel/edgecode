<div class="action-list">
    <?php if(isset($save_button)): ?>
        <a id="save_button_up" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Save</a>
    <?php endif; ?>
	<?php if(!isset($hide_add_new)): ?>
    	<a href="<?php echo \Uri::create('admin/application/create'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New</a>
    <?php endif; ?> 
    <?php if(!isset($hide_show_all)): ?>
    	<a href="<?php echo \Uri::create('admin/newscategory/list'); ?>" class="btn btn-default btn-sm"><i class="fa fa-list"></i> Show All</a>
    <?php endif; ?> 
</div>
                            
                            