	            <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Applications')); ?>
	            
                <?php echo \Theme::instance()->view('views/newsmanager/_navbar_links', array('application' => $application)); ?>
            
            	<!-- Content -->
			    <div class="content_wrapper">
			    	<div class="content_holder">
			    		
			    		<div class="elements_holder">
			    		
                            <div class="row-fluid breadcrumbs_holder">
			    				<div class="breadcrumbs_position">
                                    <?php 
                                        \Breadcrumb::set('Home', 'admin/dashboard');
                                        \Breadcrumb::set('Content Manager');
                                        \Breadcrumb::set('Manage Applications', 'admin/application/list');
                                        \Breadcrumb::set('Edit Application');
                                        \Breadcrumb::set($application->title, 'admin/application/update/' . $application->id);
                                        \Breadcrumb::set('Documents & Files');

                                        echo \Breadcrumb::create_links();
                                    ?>
		                            
                                    <?php echo \Theme::instance()->view('views/newsmanager/_action_links'); ?>
                                </div>
                            </div>
                            
						    <div class="row-fluid">
						    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                                
			                    <!-- Main Content Holder -->
			                    <div class="content">
			                    	
			                    	<?php if(\Config::get('details.file.enabled', false)): ?>
				                    	<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>   
				                    		<?php echo \Form::hidden('file_upload', 1); ?>
					                        
					                        <!-- Document Files -->
					                        <div class="panel">
					                        	<div class="panelHeader">
					                            	<div class="togglePanel"><a>
				                                		<?php echo \Theme::instance()->asset->img('panelToggle-minus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
				                                	</a><a>
				                                		<?php echo \Theme::instance()->asset->img('panelToggle-plus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
				                                	</a></div>
					                                <h4><i class="panel_documents"></i>Document Files <?php echo \Config::get('details.file.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h4>
					                            </div>
					                            <div class="panelContent">
					                            	
					                            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_b_10">
					                            		<tr>
					                            			<td class="span7 p_r_10">
					                            				 <span class="text-danger">*</span> List of acceptable document instructions e.g: You may upload any of the following image type: MS Word, PDF, PowerPoint, MS Access.
					                            				You can rearrange the order of documents by performing a simple drag and drop function.
					                            			</td>
					                            			<td class="span5 sort_message_container"></td>
					                            		</tr>
					                            	</table>
					                            	
					                            	<table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/application/sort/file/' . $application->id); ?>" class="greyTable2 sortable files_table separated ">
					                                    <tr class="nodrop nodrag blueTableHead">
					                                        <th scope="col" class="noresize">File</th>
				                                            <th scope="col">File Properties</th>
				                                            <?php if(count($application->files) > 1): ?>
					                                            <th scope="col" class="center">Re-order</th>
			                                            	<?php endif; ?>
			                                            	<?php if((!\Config::get('details.file.required', false) && !empty($application->files)) || count($application->files) > 1): ?>
					                                            <th scope="col" class="center">Delete</th>
			                                            	<?php endif; ?>
					                                    </tr>
					                                    
					                                    <?php if(is_array($application->files)): ?>
					                                        <?php foreach($application->files as $file): ?>
					                                        	<?php 
					                                        		// Get file extension
					                                        		$extension = strtolower(pathinfo($file->file, PATHINFO_EXTENSION));
					                                        	?>
						                                        <tr id="sort_<?php echo $file->id . '_' . $file->sort; ?>">
						                                            <td class="td-thumb">
						                                            	<a target="_blank" href="<?php echo \Uri::create('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
																			<i class="fa fa-file-<?php echo $extension; ?>-o"></i>
						                                            	</a>
						                                            </td>
						                                            <td class="upload">
						                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
						                                                    <tr>
						                                                        <td class="noresize"><div class="input_holder">File Name</div></td>
						                                                        <td>
					                                                        		<div class="input_holder"><i>
					                                                        			<a target="_blank" href="<?php echo \Uri::create('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
					                                                        				<?php echo $file->file; ?>
					                                                        			</a>
					                                                        		</i></div>
						                                                        </td>
						                                                    </tr>
						                                                    <tr>
						                                                        <td class="noresize">Title</td>
						                                                        <td>
						                                                        	<div class="input_holder">
						                                                        		<?php echo \Form::input('title_'.$file->id, \Input::post('title_' . $file->id, $file->title), array('class' => 'form-control')); ?>
						                                                        	</div>
						                                                        </td>
						                                                    </tr>
						                                                    <tr>
						                                                        <td class="noresize">Replace File</td>
						                                                        <td>
						                                                        	<?php echo \Form::file('file_'.$file->id); ?>
						                                                        	<?php echo \Form::hidden('file_db_'.$file->id, $file->file); ?>
						                                                        </td>
						                                                    </tr>
						                                                </table>
						                                            </td>

																	<td width="110">
																		<ul class="table-action-inline">
																			<?php if(count($application->files) > 1): ?>
																				<li>
																					<a href="" onclick="return false;">Order</a>
																				</li>
																			<?php endif; ?>
																			<?php if((!\Config::get('details.file.required', false) && !empty($application->files)) || count($application->files) > 1): ?>
																				<li>
																					<a class="text-danger" href="<?php echo \Uri::create('admin/application/delete_file/' . $file->id . '/' . $application->id); ?>">
																						Delete
																					</a>
																				</li>
																			<?php endif; ?>
																		</ul>
																	</td>
						                                        </tr>
						                                        
					                                        <?php endforeach; ?>
														<?php endif; ?>
				                                        
				                                        <?php if(\Config::get('details.file.multiple', false) || empty($application->files)): ?>
					                                        <tr class="nodrop nodrag">
					                                            <td class="td-thumb">
					                                            	<div class="fa fa-file-text-o"></div>
					                                            </td>
					                                            <td class="upload">
					                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                                    <tr>
					                                                        <td class="noresize">Title</td>
					                                                        <td>
					                                                        	<div class="input_holder">
					                                                        		<?php echo \Form::input('title_new_1', \Input::post('title_new_1'), array('class' => 'form-control')); ?>
					                                                        	</div>
					                                                        </td>
					                                                    </tr>
					                                                    <tr>
					                                                        <td class="noresize">Choose File</td>
					                                                        <td>
					                                                        	<?php echo \Form::file('file_new_1'); ?>
					                                                        </td>
					                                                    </tr>
					                                                </table>
					                                            </td>
					                                            <?php if(count($application->files) > 1): ?>
						                                            <td class="icon center"></td>
						                                        <?php endif; ?>
						                                        <?php if((!\Config::get('details.file.required', false) && !empty($application->files)) || count($application->files) > 1): ?>
						                                            <td class="icon center"></td>
						                                        <?php endif; ?>
					                                        </tr>
				                                        <?php endif; ?>
					                                    
					                                </table>
					                            	
					                                
					                            </div>
					                        </div><!-- EOF Document Files -->
					                        
			                            	<div class="save_button_holder text-right">
			                                	<?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save documents', array('type' => 'submit', 'class' => 'btn btn-success')); ?>
			                                </div>
			                                
										<?php echo \Form::close(); ?>
			                        <?php endif; ?>
			                        
			                        <?php if(\Config::get('details.video.enabled', false)): ?>
				                        <?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
				                        	<?php echo \Form::hidden('video_upload', 1); ?>
				                        	
				                        	<!-- Video Panel -->
				                            <div class="panel">
				                            
				                            	<div class="panelHeader">
					                            	<div class="togglePanel"><a>
				                                		<?php echo \Theme::instance()->asset->img('panelToggle-minus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
				                                	</a><a>
				                                		<?php echo \Theme::instance()->asset->img('panelToggle-plus.gif', array('width' => 10, 'height' => 10, 'alt' => '')); ?>
				                                	</a></div>
					                            	<h4><i class="panel_videos"></i>Video Files</h4>
					                            </div>
					                            <div class="panelContent">
					                               
					                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_b_10">
					                            		<tr>
					                            			<td class="span7 p_r_10">
					                            				 <span class="text-danger">*</span> Video instructions always placed here; For example: insert a Youtube URL link
					                                    		Default document will be the top image. You can rearange the order of documents by performing a simple drag and drop function.
					                            			</td>
					                            			<td class="span5 sort_message_container"></td>
					                            		</tr>
					                            	</table>
					                                <div class="clear"></div>
					                                
					                                <table width="100%" border="0" cellspacing="0" cellpadding="0" video="<?php echo \Uri::create('admin/application/video/'); ?>" rel="<?php echo \Uri::create('admin/application/sort/video/' . $application->id); ?>" class="greyTable2 files_table sortable separated ">
					                                    <tr class="nodrop nodrag blueTableHead">
					                                        <th scope="col" class="noresize">Video Files</th>
				                                            <th scope="col">Video File Properties</th>
				                                            <?php if(count($application->videos) > 1): ?>
					                                            <th scope="col" class="center">Re-order</th>
					                                        <?php endif; ?>
					                                        <?php if((!\Config::get('details.video.required', false) && !empty($application->videos)) || count($application->videos) > 1): ?>
					                                            <th scope="col" class="center">Delete</th>
					                                        <?php endif; ?>
					                                    </tr>
					                                    
					                                    <?php if(is_array($application->videos)): ?>
					                                        <?php foreach($application->videos as $video): ?>
					                                        	<?php
					                                        		$youtube = \App\Youtube::forge(); 
					                                        		$video->details = $youtube->parse($video->url)->get(); 
					                                        	?>
						                                        <tr id="sort_<?php echo $video->id . '_' . $video->sort; ?>">
						                                            <td class="td-thumb">
						                                            	<?php if($video->thumbnail): ?>
						                                            		<img src="<?php echo \Uri::create('media/videos/' . $video->thumbnail); ?>" class="default" width="80"/>
						                                            		<div style="margin-top: 5px; position: relative;">
							                                            		<?php echo \Form::checkbox('video_delete_image_' . $video->id, 1, \Input::post('video_delete_image_' . $video->id), array('class' => 'video_delete_image', 'style' => 'margin: 0;')); ?>
							                                            		<span style="font-size: 9px;">Use YouTube?</span>
						                                            		</div>
						                                            	<?php else: ?>
						                                            		<img src="<?php echo $video->details['thumbnail']['small']; ?>" class="default" width="80"/>
						                                            	<?php endif; ?>
						                                            </td>
						                                            
						                                            <td class="upload">
						                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
						                                                    <tr>
						                                                        <td class="noresize">Video URL</td>
						                                                        <td>
						                                                        	<div class="input_holder">
						                                                        		<?php echo \Form::input('video_url_'.$video->id, \Input::post('video_url_' . $video->id, $video->url), array('class' => 'video_url')); ?>
						                                                        	</div>
						                                                        </td>
						                                                        <td class="noresize v_top">
						                                                        	<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
						                                                        </td>
						                                                    </tr>
						                                                    <tr>
						                                                        <td class="noresize">Video Title</td>
						                                                        <td colspan="2">
						                                                        	<div class="input_holder">
						                                                        		<?php echo \Form::input('video_title_'.$video->id, \Input::post('video_title_' . $video->id, $video->title), array('class' => 'video_title')); ?>
						                                                        	</div>
						                                                        </td>
						                                                    </tr>
						                                                    <tr>
						                                                        <td class="noresize">Thumbnail Image</td>
						                                                        <td>
						                                                        	<?php echo \Form::file('video_file_'.$video->id); ?>
						                                                        	<?php if($video->thumbnail) : ?>
						                                                        		<?php echo \Form::hidden('video_file_db_'.$video->id, $video->thumbnail); ?>
						                                                        	<?php endif; ?>
						                                                        </td>
						                                                    </tr>  
						                                                </table>
						                                            </td>
						                                            <?php if(count($application->videos) > 1): ?>
							                                            <td class="icon center dragHandle">
																			<a href="" onclick="return false;">
																				Order
																			</a>
							                                            </td>
							                                        <?php endif; ?>
							                                        <?php if((!\Config::get('details.video.required', false) && !empty($application->videos)) || count($application->videos) > 1): ?>
							                                            <td class="icon center">
							                                            	<a class="text-danger" href="<?php echo \Uri::create('admin/application/delete_video/' . $video->id . '/' . $application->id); ?>">
																				Delete
																			</a>
							                                            </td>
							                                        <?php endif; ?>
						                                        </tr>
						                                        
					                                        <?php endforeach; ?>
														<?php endif; ?>
				                                        
				                                        <?php if(\Config::get('details.video.multiple', false) || empty($application->videos)): ?>
					                                        <tr class="nodrop nodrag">
					                                            <td class="td-thumb">
																	<i class="fa fa-youtube"></i>
																</td>
					                                            <td class="upload">
					                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
					                                                	<tr>
					                                                        <td class="noresize">Video URL</td>
					                                                        <td>
					                                                        	<div class="input_holder">
					                                                        		<?php echo \Form::input('video_url_new_1', \Input::post('video_url_new_1'), array('class' => 'video_url')); ?>
					                                                        	</div>
					                                                        </td>
					                                                        <td class="noresize v_top_exception">
					                                                        	<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
					                                                        </td>
					                                                    </tr>
					                                                    <tr>
					                                                        <td class="noresize">Video Title</td>
					                                                        <td colspan="2">
					                                                        	<div class="input_holder">
					                                                        		<?php echo \Form::input('video_title_new_1', \Input::post('video_title_new_1'), array('class' => 'video_title')); ?>
					                                                        	</div>
					                                                        </td>
					                                                    </tr>
					                                                    <tr>
					                                                        <td class="noresize">Thumbnail Image</td>
					                                                        <td>
					                                                        	<?php echo \Form::file('video_file_new_1'); ?>
					                                                        </td>
					                                                    </tr>                                                    
					                                                </table>
					                                            </td>
					                                            <?php if(count($application->videos) > 1): ?>
						                                            <td class="icon center"></td>
						                                        <?php endif; ?>
						                                        <?php if((!\Config::get('details.video.required', false) && !empty($application->videos)) || count($application->videos) > 1): ?>
						                                            <td class="icon center"></td>
						                                        <?php endif; ?>
					                                        </tr>
				                                        <?php endif; ?>
					                                    
					                                </table>
				                                    
				                                </div>                                
				                            </div><!-- EOF Video Panel -->
				                            
		                                    <div class="save_button_holder text-right">
			                                	<?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save videos', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
			                                </div>                               
			                                
				                            <?php \Theme::instance()->asset->js('product/videos.js', array(), 'basic'); ?>
				                        	
										<?php echo \Form::close(); ?>
									<?php endif; ?>
									
			                    </div><!-- EOF Main Content Holder -->
			                    
						    	<!-- Sidebar Holder -->
<!--			                    <div class="sidebar">
			                        <?php //echo \Theme::instance()->view('views/application/_tree_links', array('link' => 'update_files')); ?>
			                    </div>-->
                                <!-- EOF Sidebar Holder -->
			                    
			                    <div class="clear"></div>
						    	
						    </div>
			    		</div>
			    	</div>
			    </div>
			    <!-- EOF Content -->
            
            
            
			