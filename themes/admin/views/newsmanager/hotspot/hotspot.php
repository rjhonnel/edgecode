<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('News Feed');
		\Breadcrumb::set('News Categories', 'admin/newscategory/list');
		\Breadcrumb::set($application->title, 'admin/application/update/' . $application->id);
		\Breadcrumb::set('Hotspots');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Category: Hotstpot</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/newsmanager/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/newsmanager/_navbar_links', array('application' => $application)); ?>

			<?php echo \Form::open(array('action' => \Uri::create('admin/application/hotspot/edit/' . $application->id), 'enctype' => 'multipart/form-data')); ?>

			<h4 class="title-legend">Hotspot Image</h4>

			<table class="table table-striped table-bordered">
				<tr class="nodrop nodrag blueTableHead">
					<th scope="col" class="noresize">Image</th>
					<th scope="col">Image Properties</th>
					<?php if(!empty($application->hotspot_image)): ?>
						<th scope="col" class="center">Delete</th>
					<?php endif; ?>
				</tr>

				<tr class="nodrop nodrag">
					<td class="td-thumb hotspot_image">
						<?php if(!empty($application->hotspot_image)): ?>
							<?php echo \Html::img(\Helper::amazonFileURL('media/images/thumbs/' . $application->hotspot_image), array('width' => '80', 'height' => '80', 'class' => 'default')); ?>
						<?php else: ?>

						<i class="fa fa-picture-o"></i>
						<?php endif; ?>

						<?php if(!empty($application->images)): ?>
							<?php $image_path = 'media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $application->images[0]->image; ?>

							<?php echo \Html::img(\Helper::amazonFileURL($image_path), array('width' => '80', 'height' => '80', 'class' => 'cover', 'style' => 'display: none;')); ?>
							<div style="margin-top: 5px; position: relative;">
								<?php echo \Form::checkbox('use_cover_image', 1, \Input::post('use_cover_image'), array('class' => 'use_cover_image', 'style' => 'margin: 0;')); ?>
								<?php echo \Form::hidden('cover_image', $application->images[0]->image); ?>
								<span style="font-size: 9px;">Use cover image?</span>
							</div>
						<?php endif; ?>
					</td>
					<td class="upload">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="110">Alt Text</td>
								<td>
									<div class="input_holder">
										<?php echo \Form::input('alt_text', \Input::post('alt_text', $application->hotspot_alt_text), array('class' => 'form-control')); ?>
									</div>
								</td>
							</tr>
							<tr>
								<td width="110">Choose Image</td>
								<td class="btn-file">
									<?php echo \Form::file('image'); ?>
									<?php if(!empty($application->hotspot_image)): ?>
										<?php echo \Form::hidden('image_db', $application->hotspot_image); ?>
									<?php endif; ?>
								</td>
							</tr>
						</table>
					</td>
					<?php if(!empty($application->hotspot_image)): ?>
						<td class="icon center">
							<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/application/hotspot/delete_image/' . $application->id); ?>">Delete</a>
						</td>
					<?php endif; ?>
				</tr>

			</table>

			<div class="save_button_holder text-right">
				<?php echo \Form::button('image', '<i class="fa fa-edit"></i> Save Image', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>


			<?php if(!empty($application->hotspot_image)): ?>


				<h4 class="title-legend">Hotspot Image</h4>

				<small class="alert alert-info"> <span class="text-danger">*</span> Click on image to create new hotspot. You may drag and reposition the hotspot as you see fit.
					Edit the content of hotspot via the fields bellow the image.
					You may insert an image in the hotspot or link the hotspot to a video.</small>

				<div class="hotspot_cover_holder" style="position: relative; display: inline-block;" >
					<?php echo \Html::img(\Helper::amazonFileURL('media/images/' . $application->hotspot_image), array('class' => 'hotspot_cover', 'style' => 'border: solid 1px #D0D0D0;')); ?>
					<?php echo \Theme::instance()->asset->img('hotspot/recycle_bin.png', array('class' => 'trash', 'style' => 'position: absolute; bottom: 0; right: 0; padding: 5px;', 'title' => 'Drag hotspots here to delete them.')); ?>

					<?php if(!empty($application->hotspot->images)): ?>
						<?php foreach($application->hotspot->images as $image): ?>

							<?php if(!is_null($image->position_x) && !is_null($image->position_y)): ?>

								<?php
								$image_name = isset($hotspot) && $image->id == $hotspot->id ? 'hotspot_red.png' : 'hotspot_blue.png';
								?>

								<a class="hotspot" rel="<?php echo $image->id; ?>" style="position: absolute; top: <?php echo $image->position_y; ?>px; left: <?php echo $image->position_x; ?>px;" href="<?php echo \Uri::create('admin/application/hotspot/show/' . $application->id . '/' . $image->id); ?>">
									<?php //echo \Theme::instance()->asset->img('hotspot/'.$image_name, array('class' => 'hotspot', 'style' => 'position: absolute; top: '.$image->position_y.'px; left: '.$image->position_x.'px;')); ?>
									<?php echo \Theme::instance()->asset->img('hotspot/'.$image_name); ?>
								</a>
							<?php endif; ?>

						<?php endforeach; ?>
					<?php endif; ?>

				</div>

				<!-- HOTSPOT FORM -->
				<?php if(isset($hotspot)): ?>

					<?php echo \Form::open(array('action' => \Uri::create('admin/application/hotspot/hotspot/' . $application->id . '/' . $hotspot->id), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

						<h4 class="title-legend">Hotspot Details</h4>

					<div class="form-group">
						<?php echo \Form::label('Hotspot Title' . '  <span class="text-danger">*</span>'); ?>
						<div class="input_holder"><?php echo \Form::input('title', \Input::post('title', $hotspot->title), array('class' => 'form-control')); ?></div>
					</div>

					<div class="form-group">
						<?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
						<?php echo \Form::textarea('description', \Input::post('description', $hotspot->description), array('class' => 'form-control ck_editor')); ?>
					</div>

					<table class="table table-striped table-bordered">
						<tr class="nodrop nodrag blueTableHead">
							<th scope="col" class="noresize">Image</th>
							<th scope="col">Image Properties</th>
							<?php if(!empty($hotspot->image)): ?>
								<th scope="col" class="center">Delete</th>
							<?php endif; ?>
						</tr>

						<tr class="nodrop nodrag">
							<td class="td-thumb hotspot_image">
								<?php if(!empty($hotspot->image)): ?>
									<?php echo \Html::img(\Helper::amazonFileURL('media/images/' . key(\Config::get('infotab.image.resize', array('' => ''))) . $hotspot->image), array('width' => '80', 'height' => '80', 'class' => 'default')); ?>
								<?php else: ?>
									<i class="fa fa-picture-o"></i>
								<?php endif; ?>
							</td>
							<td class="upload">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="110">Alt Text</td>
										<td>
											<div class="input_holder">
												<?php echo \Form::input('alt_text', \Input::post('alt_text', $hotspot->alt_text), array('class' => 'form-control')); ?>
											</div>
										</td>
									</tr>
									<tr>
										<td width="110">Choose Image</td>
										<td>
											<?php echo \Form::file('image'); ?>
											<?php if(!empty($hotspot->image)): ?>
												<?php echo \Form::hidden('image_db', $hotspot->image); ?>
											<?php endif; ?>
										</td>
									</tr>
								</table>
							</td>
							<?php if(!empty($hotspot->image)): ?>
								<td class="icon center">
									<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/application/hotspot/delete_infotab_image/' . $hotspot->id . '/' . $application->id . '/hotspot/image'); ?>">
										Delete
									</a>
								</td>
							<?php endif; ?>
						</tr>

					</table>

					<small class="alert alert-info"> <span class="text-danger">*</span> Video instructions always placed here; For example: insert a Youtube URL link
						Default document will be the top image. You can rearange the order of documents by performing a simple drag and drop function.</small>

					<table  video="<?php echo \Uri::create('admin/application/video/'); ?>" class="table table-striped table-bordered">
						<tr class="nodrop nodrag blueTableHead">
							<th scope="col" width="110">Video File</th>
							<th scope="col">Video File Properties</th>
							<?php if(!empty($hotspot->video)): ?>
								<th scope="col" class="center">Delete</th>
							<?php endif; ?>
						</tr>

						<?php if($hotspot->video): ?>
							<?php
							$youtube = \App\Youtube::forge();
							$hotspot->video_details = $youtube->parse($hotspot->video)->get();
							?>
						<?php endif; ?>

						<tr class="nodrop nodrag">
							<td class="td-thumb">

								<?php if($hotspot->video): ?>
									<img src="<?php echo $hotspot->video_details['thumbnail']['small']; ?>" class="default" width="80"/>
								<?php else: ?>
									<i class="fa fa-youtube"></i>
								<?php endif; ?>
							</td>
							<td class="upload">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="110">Video URL</td>
										<td>
											<div class="input_holder">
												<?php echo \Form::input('video_url', \Input::post('video_url', $hotspot->video), array('class' => 'form-control video_url')); ?>
											</div>
										</td>
										<td>&nbsp;</td>
										<td class="noresize v_top_exception">
											<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
										</td>
									</tr>
									<tr>
										<td width="110">Video Title</td>
										<td colspan="3">
											<div class="input_holder">
												<?php echo \Form::input('video_title', \Input::post('video_title', $hotspot->video_title), array('class' => 'form-control video_title')); ?>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<?php if(!empty($hotspot->video)): ?>
								<td class="icon center">
									<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete video?" href="<?php echo \Uri::create('admin/application/hotspot/delete_infotab_image/' . $hotspot->id . '/' . $application->id . '/hotspot/video'); ?>">
										Delete
									</a>
								</td>
							<?php endif; ?>
						</tr>

					</table>

					<?php echo \Theme::instance()->asset->js('product/videos.js'); ?>

					<div class="save_button_holder text-right">
						<?php echo \Form::button('update', '<i class="fa fa-edit"></i>Save Hotspot', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
					</div>

					<?php echo \Form::close(); ?>
					<!-- END OF: HOTSPOT FORM -->
				<?php endif; ?>

			<?php endif; ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>


            





			    
<script>
	var application_id = <?php echo $application->id; ?>;
	var hotspot_id = <?php echo isset($hotspot) ? $hotspot->id : 'false'; ?>;
</script>
<?php echo \Theme::instance()->asset->js('application/hotspots.js'); ?>

<?php echo ckeditor_replace('ck_editor'); ?>

                            