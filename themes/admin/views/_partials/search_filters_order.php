<?php
   $status = array(
        'false' => 'Select',
        '1' => 'Active',
        '0' => 'Inactive',
    );
   ?>

<?php
    // check first if $layout is set
    if(!isset($layout))
        $layout = '';

    switch ((string)$layout) {
        case '3': // if 3 columns
            $columnClass = 'col-sm-4'; break;
        case '2': // if 2 columns
            $columnClass = 'col-sm-6'; break;
        case '1': // if 1 column
            $columnClass = 'col-sm-12'; break;
        default: // fallback to default: if $layout is null OR value is not 3,2 or 1 to avoid errors
            $columnClass = 'col-sm-3'; break;
    }
?>

<div class="filter-holder">
    <div class="panel-utility">
        <?php if($options || (!isset($show_reset) || $show_reset === true)): ?>
            <ul class="list-inline">
                <?php if($options): ?>
                    <li><a class="btn btn-sm btn-primary toggle-filters" href="">Filters</a></li>
                <?php endif; ?>
                <?php if(!isset($show_reset) || $show_reset === true): ?>
                    <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
                <?php endif; ?>
            </ul>
        <?php endif; ?>

        <div class="form-inline search-inline">
            <?php echo \Form::input('title', \Input::get('title'), array('class' => 'form-control', 'placeholder' => isset($search_text)? $search_text : ucfirst($module).' name keywords search' )); ?>
            <?php echo \Form::submit('search', 'Search', array('class' => 'btn btn-sm btn-default')); ?>
        </div>
    </div>
</div>

<?php if($options): ?>
    <?php
    // Put margin to all search filters except last one (4th in row)
    $margin = \Str::alternator('m_r_15', 'm_r_15', 'm_r_15', '');
    ?>
    <div class="form-filter form-filter-2 blue_filters">
        <div class="row">
            <div class="col-md-6">

                <h4 class="form-filter-label">DELIVERIES</h4>
                <div class="calendar-lg" id="canlendarDisplay"></div>
                <input type="hidden" name="delivery_datetime" data-delivery="<?php echo \Input::get('delivery_datetime')?\Input::get('delivery_datetime'):''; ?>">

                <div class="date-filter-btn">
                    <a class="btn btn-sm btn-default searchDateToday" data-href="<?php echo \Uri::create('/admin/order/list'); ?>" data-name="delivery_datetime" href="/admin/order/list">Today</a>
                    <a class="btn btn-sm btn-default searchDateTomorrow" data-href="<?php echo \Uri::create('/admin/order/list'); ?>" data-name="delivery_datetime" href="/admin/order/list">Tomorrow</a>
                    <?php if(\Input::get('d_order-sort') == 'desc'): ?>
                        <a class="btn btn-sm btn-default"href="<?php echo \Uri::create('/admin/order/list'); ?>?d_order-sort=asc">Soonest Deliveries</a>
                    <?php else: ?>
                        <a class="btn btn-sm btn-default" href="<?php echo \Uri::create('/admin/order/list'); ?>?d_order-sort=desc">Latest Deliveries</a>
                    <?php endif; ?>
                </div>

            </div>
            <div class="col-md-6">
                <h4 class="form-filter-label">FILTER</h4>
                <div class="row">
                    <?php if(in_array('featured', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Featured'); ?>
                                <?php echo \Form::select('featured', \Input::get('featured'), array('false' => 'Select', 1 => 'Yes', 0 => 'No'), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php // USER SPECIFIC SEARCH FILTERS ?>
                    <?php if(in_array('email', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Email'); ?>
                                <?php echo \Form::input('email', \Input::get('email'), array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('user_group', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('User Group', null, array('class' => '')); ?>
                                <?php echo \Form::select('user_group', \Input::get('user_group', isset($values['user_group']) ? $values['user_group'] : false ), array('false' => 'Select') + \User\Model_Group::fetch_pair('id', 'name', array(), false, \SentryAdmin::group()->all('front')), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('user_status', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('User Status', null, array('class' => '')); ?>
                                <?php echo \Form::select('activated', \Input::get('activated', isset($values['activated']) ? $values['activated'] : false ), array('false' => 'Select') + array(0 => 'Inactive', 1 => 'Active'), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if(in_array('user_group_admin', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('User Group', null, array('class' => '')); ?>
                                <?php echo \Form::select('user_group', \Input::get('user_group', isset($values['user_group']) ? $values['user_group'] : false ), array('false' => 'Select') + \User\Model_Group::fetch_pair('id', 'name', array(), false, \SentryAdmin::group()->all('admin')), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if(in_array('country', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Country'); ?>
                                <?php echo \Form::select('country', \Input::get('country'), array('false' => "Select") + \App\Countries::forge()->getCountries(), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>



                    <?php if(in_array('postcode', $options)): ?>
                        <div class="col-sm-6">
                            <div class="form-group toggable_dates date_content">
                                <?php echo \Form::label('Postcode'); ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php echo \Form::input('postcode_from', \Input::get('postcode_from'), array('class' => 'dateInput form-control', 'placeholder' => 'From')); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php echo \Form::input('postcode_to', \Input::get('postcode_to'), array('class' => 'dateInput form-control', 'placeholder' => 'To')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('group_id', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Parent Group'); ?>
                                <?php echo \Form::select('parent_id', \Input::get('parent_id'), array('false' => 'Select') + \Product\Model_Group::fetch_pair('id', 'title', array(), false, \Product\Model_Group::find_by_type('property')), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('category_id', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Product Categories'); ?>

                                <?php
                                $category_product = DB::query("SELECT id, case title when parent_id!=0 then title else title end as title FROM `product_categories` WHERE 1")->execute();
                                $arrayName = array();
                                if (!empty($category_product))
                                {
                                    foreach ($category_product as $row)
                                    {
                                        if(is_array($row))
                                        {
                                            $arrayName[$row['id']] = $row['title'];
                                        }
                                        else
                                        {
                                            $arrayName[$row->{'id'}] = $row->{'title'};
                                        }
                                    }
                                }
                                ?>
                                <?php echo \Form::select('category_id', \Input::get('category_id', isset($category) ? $category->id : ''), array('false' => 'Select') + $arrayName, array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if(in_array('job_category_id', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Parent Category'); ?>
                                <?php echo \Form::select('category_id', \Input::get('category_id', isset($category) ? $category->id : ''), array('false' => 'Select') + \Job\Model_Category::fetch_pair('id', 'title', array(), false, \Job\Model_Category::find_all()), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(in_array('is_posted', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Job Posted'); ?>
                                <?php echo \Form::select('is_posted', \Input::get('is_posted'), array('false' => 'Select', '1' => 'Yes', '0' => 'No'), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if(in_array('date_submited', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group date_content">
                                <?php echo \Form::label('Date Submited', null, array('class' => 'dateLabel')); ?>
                                <?php echo \Form::input('created_from', \Input::get('created_from'), array('class' => 'from_white dateInput form-control form-control', 'placeholder' => 'From')); ?>
                                <div class="left spacing_double">
                                    <?php echo \Form::input('created_to', \Input::get('created_to'), array('class' => 'to_white dateInput form-control form-control', 'placeholder' => 'To')); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('order_total', $options)): ?>
                        <div class="col-sm-12">
                            <div class="form-group toggable_dates date_content">
                                <?php echo \Form::label('Order Total'); ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php echo \Form::input('order_total_from', \Input::get('order_total_from'), array('class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?php echo \Form::input('order_total_to', \Input::get('order_total_to'), array('class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('order_date', $options)): ?>
                        <div class="col-sm-12">
                            <div class="form-group date_content">
                                <label for="" style="display: block;">Order Date</label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="datepicker-holder-control"><?php echo \Form::input('date_from', \Input::get('date_from'), array('class' => 'form-control from_white dateInput', 'placeholder' => 'From')); ?></div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="datepicker-holder-control"><?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => ' form-control to_white dateInput', 'placeholder' => 'To')); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('order_status', $options)): ?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo \Form::label('Order Status'); ?>
                                <?php echo \Form::select('status', \Input::get('status'), array('false' => 'Select') + \Config::get('details.status', array()), array('class' => 'form-control')); ?>
                            </div>

                        </div>
                    <?php endif; ?>

                    <!-- added -->

                    <?php if(in_array('tracking_no', $options)): ?>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <?php echo \Form::label('Tracking Number'); ?>
                                <?php echo \Form::input('tracking_no', \Input::get('tracking_no'), array('class' => 'form-control', 'placeholder' => 'Tracking No.')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    /************ Start generating query for payment method ***********/
                    $items = \Payment\Model_Payment::find(function($query){
                        $query->select('method');
                        $query->order_by('method', 'asc');
                        $query->distinct();
                    });

                    $payment_method = array();
                    if($items)
                    {
                        foreach ($items as $item)
                        {
                            if(!$item->method) continue;
                            $payment_method[$item->method] = $item->method;
                        }
                    }
                    ?>
                    <?php if(in_array('payment_method', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Payment Method'); ?>
                                <?php echo \Form::select('payment_method', \Input::get('payment_method'), array('false' => 'Select') + \Config::get('details.payment_method'), array('class' => 'form-control')); ?>
                            </div>

                        </div>
                    <?php endif; ?>


                    <?php
                    /************ Start generating query for payment status ***********/
                    $items = \Payment\Model_Payment::find(function($query){
                        $query->select('status');
                        $query->order_by('status', 'asc');
                        $query->distinct();
                    });

                    $payment_status = array();
                    if($items)
                    {
                        foreach ($items as $item)
                        {
                            if(!$item->status) continue;
                            $payment_status[$item->status] = $item->status;
                        }
                    }
                    ?>
                    <?php if(in_array('payment_status', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Payment Status'); ?>
                                <?php echo \Form::select('payment_status', \Input::get('payment_status'), array('false' => 'Select') + \Config::get('details.payment_status'), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    $categories = \Product\Model_Category::fetch_pair('id', 'title', array('order_by' => array('title' => 'asc')));
                    if(!is_array($categories)) $categories = array();
                    ?>
                    <?php if(in_array('product_category', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Product Category'); ?>
                                <?php echo \Form::select('product_category', \Input::get('product_category'), array('false' => 'Select') + $categories, array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('shipping_status', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Shipping Status'); ?>
                                <?php echo \Form::select('shipping_status', \Input::get('shipping_status'), array('false' => 'Select') + \Config::get('details.shipping_status', array()), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php
                    $states = \App\States::forge()->getStateProvinceArray(\Input::get('country')!='false'?\Input::get('country'):'AU');
                    if(!is_array($states)) $states = array();
                    ?>
                    <?php if(in_array('state', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('State'); ?>
                                <?php echo \Form::select('state', \Input::get('state'), array('false' => "Select") + $states, array('class' => 'form-control')); ?>
                            </div>

                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_coupon', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Discount Coupon Used'); ?>
                                <?php echo \Form::input('discount_coupon', \Input::get('discount_coupon'), array('class' => 'form-control', 'placeholder' => '')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_status', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Status'); ?>
                                <?php echo \Form::select('discount_status', \Input::get('discount_status'), array('' => 'Select') + \Config::get('details.status', array()), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_type', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Discount Type'); ?>
                                <?php echo \Form::select('discount_type', \Input::get('discount_type'), array('' => 'Select') + \Config::get('details.type', array()), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('type', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Type'); ?>
                                <?php echo \Form::select('type', \Input::get('type'), array('' => 'Select') +  \Config::get('details.use_type', array()), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_customer_group', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Customer Group'); ?>
                                <?php echo \Form::select('discount_customer_group', \Input::get('discount_customer_group'), array('false' => 'Select') + array(
                                        '1' => 'Subscribed',
                                        '2' => 'Unsubscribed',
                                    ), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_product_group', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Product Group'); ?>
                                <?php echo \Form::select('discount_product_group', \Input::get('discount_product_group'), array('false' => 'Select') + array(
                                        '1' => 'Subscribed',
                                        '2' => 'Unsubscribed',
                                    ), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_expiry_date', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <div class="date_content">
                                    <?php echo \Form::label('Expiry Date', null, array('class' => 'dateLabel')); ?>
                                    <?php echo \Form::input('discount_expiry_date', \Input::get('discount_expiry_date'), array('class' => 'from_white dateInput form-control', 'placeholder' => 'From')); ?>
                                    <div class="left spacing_double">
                                        <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'to_white dateInput form-control', 'placeholder' => 'To')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_amount', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <div class="toggable_dates date_content">
                                    <?php echo \Form::label('Discount Amount'); ?>
                                    <?php echo \Form::input('order_total_from', \Input::get('order_total_from'), array('class' => 'dateInput m_r_15', 'placeholder' => 'From', 'style' => 'width: 40% !important;')); ?>
                                    <?php echo \Form::input('order_total_to', \Input::get('order_total_to'), array('class' => 'dateInput', 'placeholder' => 'To', 'style' => 'width: 40% !important;')); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_minimum_order_qty', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Minimum Order Qty'); ?>
                                <?php echo \Form::input('discount_minimum_order_qty', \Input::get('discount_minimum_order_qty'), array('class' => 'form-control', 'placeholder' => '')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('discount_minimum_order_value', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Minimum Order Value'); ?>
                                <?php echo \Form::input('discount_minimum_order_value', \Input::get('discount_minimum_order_value'), array('class' => 'form-control', 'placeholder' => '')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('product_group', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Product Group'); ?>
                                <?php echo \Form::select('product_group', \Input::get('product_group'), array('false' => 'Select') + array(
                                        '1' => 'Group 1',
                                        '2' => 'Group 2',
                                    ), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('product_status', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Status'); ?>
                                <?php echo \Form::select('product_status', \Input::get('product_status'), array('false' => 'Select') + array(
                                        '1' => 'Active',
                                        '2' => 'Inactive',
                                    ), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('accessories_date', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <div class="date_content">
                                    <?php echo \Form::label('Date Added', null, array('class' => 'dateLabel')); ?>
                                    <?php echo \Form::input('accessories_date', \Input::get('accessories_date'), array('class' => 'from_white dateInput form-control', 'placeholder' => 'From')); ?>
                                    <div class="left spacing_double">
                                        <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'to_white dateInput form-control', 'placeholder' => 'To')); ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <?php endif; ?>

                    <?php if(in_array('accessories_price', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <div class="toggable_dates date_content">
                                    <?php echo \Form::label('Price'); ?>
                                    <?php echo \Form::input('order_total_from', \Input::get('order_total_from'), array('class' => 'dateInput m_r_15', 'placeholder' => 'From', 'style' => 'width: 40% !important;')); ?>
                                    <?php echo \Form::input('order_total_to', \Input::get('order_total_to'), array('class' => 'dateInput', 'placeholder' => 'To', 'style' => 'width: 40% !important;')); ?>
                                </div>
                            </div>

                        </div>
                    <?php endif; ?>

                    <?php if(in_array('newsletter_status', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Newsletter Status'); ?>
                                <?php echo \Form::select('newsletter_status', \Input::get('newsletter_status'), array('false' => 'Select') + array(
                                        '1' => 'Active',
                                        '2' => 'Inactive',
                                    ), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('customer_group', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Customer Group'); ?>
                                <?php echo \Form::select('customer_group', \Input::get('customer_group'), array('false' => 'Select') + array(
                                        '1' => 'Retailer Type A',
                                        '2' => 'Retailer Type B',
                                    ), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if(in_array('credit_account_status', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <?php echo \Form::label('Credit Account Status'); ?>
                                <?php echo \Form::select('credit_account_status', \Input::get('credit_account_status'), array('false' => 'Select') + array(
                                        '1' => 'Authorised',
                                        '2' => 'Unauthorised',
                                    ), array('class' => 'form-control')); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <!-- added -->

                    <?php // END OF: ORDER RELATED SEARCH ?>

                    <?php if(in_array('status', $options)): ?>
                        <div class="<?php echo $columnClass; ?>">
                            <div class="form-group">
                                <div class="double_select">
                                    <?php echo \Form::label('Status'); ?>
                                    <?php echo \Form::select('status', \Input::get('status'), $status, array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
                                </div>

                                <div class="toggable_dates date_content" <?php echo \Input::get('status') != '2' ? 'style="display: none;"' : ''; ?>>

                                    <?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
                                    <?php echo \Form::input('active_from', \Input::get('active_from'), array('class' => 'from_white dateInput form-control', 'placeholder' => 'From')); ?>
                                    <div class="left spacing_double">
                                        <?php echo \Form::input('active_to', \Input::get('active_to'), array('class' => 'to_white dateInput form-control', 'placeholder' => 'To')); ?>
                                    </div>

                                </div>
                            </div>

                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="form-filter-actions">
            <?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary')); ?>
        </div>
    </div>


<?php endif; ?>