<?php if($current_user): ?>
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	    <i class="fa fa-user-circle"></i>
	    Welcome, <strong><?php echo $current_user->get('metadata.first_name') . ' ' . $current_user->get('metadata.last_name'); ?></strong>
	    <span class="caret"></span>
	</a>
	<ul class="dropdown-menu">
	    <li><a href="<?php echo \Uri::create('admin/settings/user/update/'. $current_user->get('id'));?>"><i class="icon-cog"></i> Edit Profile</a></li>
	    <li><a href="<?php echo \Uri::create('admin/logout'); ?>"><i class="icon-off"></i> Log Out</a></li>
	</ul>
<?php endif; ?>