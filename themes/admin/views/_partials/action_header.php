                                <?php 
    if(!isset($bulk_actions)) $bulk_actions = array(); 
    if(!isset($action_config)) $action_config = array(); 
    $config_name = isset($config_name) ? $config_name : 'details' ;
    $action_name = isset($action_name) ? $action_name : 'details' ;
?>
<div class="actionbox">
    <?php if(!in_array('no_links', $action_config)): ?>
        <div class="actionlinks">
            <a href="#" class="action_all" data-value="<?php echo $action_name; ?>" data-action="select_all">Select All</a> / 
            <a href="#" class="action_all" data-value="<?php echo $action_name; ?>" data-action="unselect_all">Unselect All</a> / 
            <a href="#" class="action_all" data-value="<?php echo $action_name; ?>" data-action="select_inverse">Select Inverse</a> / 
            <span class="items_selected">0</span> items selected
        </div>
    <?php endif; ?>
    <?php if(!in_array('no_bulk_actions', $action_config)): ?>
        <div class="form-inline">
            <?php echo \Form::select('bulk_actions', null, \Config::get($config_name . '.bulk_actions', array()) + $bulk_actions, array('class' => 'form-control bulk_actions')); ?>
            <span class="bulk_input_box" style="display: none;">
                &nbsp;<?php echo \Form::input('bulk_input', '', array('class' => 'small_txt_input bulk_input')); ?>
            </span>
            <button id="bulk_apply" class="btn btn-small btn-primary apply_bulk_actions">Apply</button>
        </div>
    <?php endif; ?>
</div>
                            