<div class="quickAdd">
	<div id="quickPanel">
		<div>
			<a class="add" href="#"></a>
			<select name="quickAdd" class="select_init right">
				<option data-url="<?php echo \Uri::create('admin/product/create'); ?>">New Product</option>
				<option data-url="<?php echo \Uri::create('admin/user/create'); ?>">New Customer</option>
				<option data-url="<?php echo \Uri::create('admin/page/create'); ?>">New Page</option>
			</select>
		</div>
	</div>

	<a href="#" class="btn btn-primary btn-sm quickTab"><i class="fa fa-plus-circle"></i> Quick Add</a>
</div>