<?php
/**
 * @var $menu string
 */
$menu = isset($menu) ? $menu : '';

?><div class="main-content-sidebar sidebar"
	 id="my-sidebar"
	 data-scrollable
	 data-position="left">

	<ul id="left_menu" class="sidebar-menu navbar navbar-sidebar">
		<?php if (!\SentryAdmin::user()->is_production() && !\Sentry::user()->in_group('Admin')) :?>
		<li class="dropdown <?php echo Helper::menuIsActive(['admin/dashboard','admin/settings/user/list','admin/settings','admin/payment/paypal','admin/backup'], $menu, 'open') ?>">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-dashboard"></i> Dashboard</a>
			<ul class="sidebar-submenu">
				<li class="<?php echo Helper::menuIsActive('admin/dashboard', $menu) ?>"><a href="<?php echo \Uri::create('admin/dashboard'); ?>">Home</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/settings/user/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/settings/user/list'); ?>">Admin Users</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/settings', $menu) ?>"><a href="<?php echo \Uri::create('admin/settings'); ?>">Settings</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/payment/paypal', $menu) ?>"><a href="<?php echo \Uri::create('admin/payment/paypal'); ?>">Payment</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/backup', $menu) ?>"><a href="<?php echo \Uri::create('admin/backup'); ?>">Backup</a></li>
			</ul>
		</li>
		<?php endif;?>
		<li class="dropdown <?php echo Helper::menuIsActive(['admin/product/list','admin/product/category/list','admin/product/brand/list','admin/attribute/list','admin/attribute/group/list','admin/product/infotab/list'], $menu, 'open') ?>">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-book"></i> Catalogue</a>
			<ul class="sidebar-submenu">
				<li class="<?php echo Helper::menuIsActive('admin/product/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/product/list'); ?>">Products</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/product/category/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/product/category/list'); ?>">Categories</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/product/brand/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/product/brand/list'); ?>">Brands</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/attribute/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/attribute/list'); ?>">Attributes</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/attribute/group/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/attribute/group/list'); ?>">Attribute Groups</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/product/infotab/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/product/infotab/list'); ?>">Info Tabs</a></li>
			</ul>
		</li>
		<li class="dropdown <?php echo Helper::menuIsActive(['admin/product/stock/list','admin/product/stock/option',], $menu, 'open') ?>">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-stack-overflow"></i> Stock</a>
			<ul class="sidebar-submenu">
				<li class="<?php echo Helper::menuIsActive('admin/product/stock/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/product/stock/list'); ?>">Stock Control</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/product/stock/option', $menu) ?>"><a href="<?php echo \Uri::create('admin/product/stock/option'); ?>">Stock Options</a></li>
			</ul>
		</li>
		<li class="dropdown <?php echo Helper::menuIsActive(['admin/user/list','admin/user/group/list','admin/product/group/list/pricing','admin/company/list'], $menu, 'open') ?>">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-users"></i> Customers</a>
			<ul class="sidebar-submenu">
				<li class="<?php echo Helper::menuIsActive('admin/user/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/user/list'); ?>">Customers</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/user/group/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/user/group/list'); ?>">Customer Groups</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/product/group/list/pricing', $menu) ?>"><a href="<?php echo \Uri::create('admin/product/group/list/pricing'); ?>">Pricing Groups</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/company/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/company/list'); ?>">Companies</a></li>
			</ul>
		</li>
		<li class="<?php echo Helper::menuIsActive('admin/supplier/list', $menu) ?>">
			<a href="<?php echo \Uri::create('admin/supplier/list'); ?>"><i class="fa fa-tasks"></i> Suppliers</a>
		</li>
		<li class="dropdown <?php echo Helper::menuIsActive(['admin/order/list','admin/order/abandon_cart',], $menu, 'open') ?>">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-external-link"></i> Orders</a>
			<ul class="sidebar-submenu">
				<li class="<?php echo Helper::menuIsActive('admin/order/list', $menu) ?>">
					<a href="<?php echo \Uri::create('admin/order'); ?>" title="All Orders">Orders</a>
					<ul class="sidebar-submenu" style="margin-bottom: 0;">
						<li><a href="<?php echo \Uri::create('/admin/order/list'); ?>?order-sort=desc" title="Sort by Recent Orders">Recent Orders</a></li>
						<?php
							$count_new_orders = 0;
							$today = date('Y-m-d');
							$result = \DB::query("SELECT count(id) as total_new_orders FROM orders WHERE finished = 1 AND status = 'new'")->execute();
							$count_new_orders = $result[0]['total_new_orders'];
						?>
						<li><a href="<?php echo \Uri::create('/admin/order/list'); ?>?status=new" title="Orders Today">New Orders (<?php echo $count_new_orders; ?>)</a></li>
						<?php
							$count_deliveries_today = 0;
							$today = date('Y-m-d');
							$result = \DB::query("SELECT count(id) as total_deliveries_today FROM orders WHERE finished = 1 AND DATE_FORMAT(delivery_datetime, '%Y-%m-%d') = '".$today."'")->execute();
							$count_deliveries_today = $result[0]['total_deliveries_today'];
						?>
						<li><a data-href="<?php echo \Uri::create('/admin/order/list'); ?>" data-name="delivery_datetime" href="/admin/order/list" class="searchDateToday" title="Deliveries Today">Today (<?php echo $count_deliveries_today; ?>)</a></li>
						<?php
							$count_deliveries_tomorrow = 0;
							$tomorrow = date('Y-m-d', strtotime("+1 day", strtotime(date('Y-m-d'))));
							$result = \DB::query("SELECT count(id) as total_deliveries_tomorrow FROM orders WHERE finished = 1 AND DATE_FORMAT(delivery_datetime, '%Y-%m-%d') = '".$tomorrow."'")->execute();
							$count_deliveries_tomorrow = $result[0]['total_deliveries_tomorrow'];
						?>
						<li><a data-href="<?php echo \Uri::create('/admin/order/list'); ?>" data-name="delivery_datetime" href="/admin/order/list" class="searchDateTomorrow" title="Deliveries Tomorrow">Tomorrow (<?php echo $count_deliveries_tomorrow; ?>)</a></li>
						<?php
							$count_active_quotes = 0;
							$result = \DB::query("SELECT count(id) as total_active_quotes FROM orders WHERE finished = 1 AND status = 'pending'")->execute();
							$count_active_quotes = $result[0]['total_active_quotes'];
						?>
						<li><a href="<?php echo \Uri::create('/admin/order/list'); ?>?status=pending" title="Pending order status">Active Quotes (<?php echo $count_active_quotes; ?>)</a></li>
					</ul>
					<div class="calendar-sm calendar-light" id="canlendarDisplay"></div>
					<input type="hidden" id="canlendarDisplay_delivery_datetime" value="<?php echo \Input::get('delivery_datetime'); ?>">
				</li>
				<li class="<?php echo Helper::menuIsActive('admin/order/abandon_cart', $menu) ?>"><a href="<?php echo \Uri::create('admin/order/abandon_cart'); ?>">Abandoned Carts</a></li>
			</ul>
		</li>

			<li class="dropdown <?php echo Helper::menuIsActive(['admin/discountcode'], $menu, 'open') ?>">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-tags"></i> Promotionals</a>
				<ul class="sidebar-submenu">
					<li class="<?php echo Helper::menuIsActive('admin/discountcode', $menu) ?>"><a href="<?php echo \Uri::create('admin/discountcode/list'); ?>">Discount Codes</a></li>
				</ul>
			</li>
		<?php if(!\SentryAdmin::user()->is_production()):?>
				<li class="dropdown <?php echo Helper::menuIsActive(['admin/page/list'], $menu, 'open') ?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-edit"></i> Content Manager</a>
					<ul class="sidebar-submenu">
						
						<li class="<?php echo Helper::menuIsActive('admin/page/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/page/list'); ?>">Pages</a>
						<li>
						
					</ul>
				</li>
			<li class="dropdown <?php echo Helper::menuIsActive(['admin/newscategory/list','admin/newslist'], $menu, 'open') ?>">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-newspaper-o"></i> News Feed</a>
				<ul class="sidebar-submenu">
					<li class="<?php echo Helper::menuIsActive('admin/newscategory/list', $menu) ?>"><a href="<?php echo \Uri::create('admin/newscategory/list'); ?>">News Categories</a></li>
					<li class="<?php echo Helper::menuIsActive('admin/newslist', $menu) ?>"><a href="<?php echo \Uri::create('admin/newslist'); ?>">News Post</a></li>
				</ul>
			</li>
		<?php endif;?>


		<li class="dropdown <?php echo Helper::menuIsActive(['admin/reports/orders','admin/reports/customer_orders','admin/reports/products','admin/reports/customer_products','admin/reports/customers','admin/reports/sales_by_day','admin/reports/sales_by_item','admin/reports/sales_by_customer','admin/reports/sales_by_month','admin/reports/sales_by_month_report','admin/reports/orders_products','admin/reports/order_notes','admin/reports/print'], $menu, 'open') ?>">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><i class="fa fa-bar-chart"></i>Reports</a>
			<ul class="sidebar-submenu">
				<?php if(\SentryAdmin::user()->in_group('Super Admin')):?>
				<li class="<?php echo Helper::menuIsActive('admin/reports/orders', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/orders'); ?>">Orders</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/reports/products', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/products'); ?>">Products</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/reports/customers', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/customers'); ?>">Customers</a></li>
				<?php endif;?>
				<li class="<?php echo Helper::menuIsActive('admin/reports/orders_products', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/orders_products'); ?>">Orders x Products</a></li>
				<?php if(\SentryAdmin::user()->in_group('Super Admin')):?>
				<li class="<?php echo Helper::menuIsActive('admin/reports/customer_products', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/customer_products'); ?>">Customer x Products</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/reports/customer_orders', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/customer_orders'); ?>">Customer x Orders</a></li>
				<?php endif;?>
				<li class="<?php echo Helper::menuIsActive('admin/reports/order_notes', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/order_notes'); ?>">Order Notes</a></li>
				<?php if( \Sentry::user()->in_group('Super Admin') ):?>
				<li class="<?php echo Helper::menuIsActive('admin/reports/print', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/print'); ?>">Print</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/reports/sales_by_item', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/sales_by_item'); ?>">Sales x Item</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/reports/sales_by_customer', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/sales_by_customer'); ?>">Sales x Customer</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/reports/sales_by_day', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/sales_by_day'); ?>">Sales x Day</a></li>
                <li class="<?php echo Helper::menuIsActive('admin/reports/sales_by_month_report', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/sales_by_month_report'); ?>">Sales x Month</a></li>
				<li class="<?php echo Helper::menuIsActive('admin/reports/sales_by_month', $menu) ?>"><a href="<?php echo \Uri::create('admin/reports/sales_by_month'); ?>">Sales x Year</a></li>
				<?php endif;?>
			</ul>
		</li>
	</ul>
</div>