<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>

			<a href="<?php echo \Uri::create('admin'); ?>" class="navbar-brand">
				<?php echo \Theme::instance()->asset->img('../assets/images/logo.svg', array('alt' => 'EdgeCommerce')); ?>
			</a>

		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">

				<?php
					// Only show this part of header whan user is logged in
					if(\SentryAdmin::check() && \SentryAdmin::user()->is_admin()) : ?>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-plus"></i> Quick Add
						</a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo \Uri::create('admin/order/create'); ?>"><i class="icon-cog"></i> New Order</a></li>
							<li><a href="<?php echo \Uri::create('admin/product/create'); ?>"><i class="icon-cog"></i> New Product</a></li>
							<li><a href="<?php echo \Uri::create('admin/user/create'); ?>"><i class="icon-cog"></i> New Customer</a></li>
						</ul>
					</li>
					<li class="dropdown">
						<?php echo \Theme::instance()->view('views/_partials/account'); ?>
					</li>
				<?php endif; ?>
				<li class="navbar-tr">
					<a href="<?php echo \Uri::create('/'); ?>" target="_blank"><i class="fa fa-globe"></i></a>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<?php echo \Messages::display(); ?>