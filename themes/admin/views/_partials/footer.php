<div class="main-footer">
	<div class="navbar nabvar-default">
		<div class="navbar-inner">
			<p class="copyright pull-left">Copyright &copy; <?php echo date('Y'); ?> All Rights Reserved. <a href="http://www.edgecommerce.com.au" target='_blank'>Edge Commerce V<?php echo EDGECMS_VERSION ?></a></p>
		</div>
	</div>
</div>