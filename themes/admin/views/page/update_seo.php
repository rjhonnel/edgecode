<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Content Manager');
		\Breadcrumb::set('Page Manager', 'admin/page/list');
		\Breadcrumb::set($page->title, 'admin/page/update/' . $page->id);
		\Breadcrumb::set('Meta Content');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Page: Meta Content</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/page/_action_links', array('create_form'=>1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/page/_navbar_links', array('page' => $page)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Page Optimisation / SEO</h3>

				</div>
				<div class="panel-body form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">Title  <span class="text-danger">*</span></label>
						<div class="col-sm-10">
							<?php echo \Form::input('meta_title', \Input::post('meta_title', $page->seo->meta_title ?: $page->title), array('class'=>'form-control')); ?>
							<small class="form-instruction">Recommendation: under 70 characters.</small><br>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">URL</label>
						<div class="col-sm-10">
							<?php echo \Form::input('slug', \Input::post('slug', $page->seo->slug), array('class' => 'form-control slug_source slug_target')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Keywords</label>
						<div class="col-sm-10">
							<?php echo \Form::textarea('meta_keywords', \Input::post('meta_keywords', $page->seo->meta_keywords), array('rows' => 2,'class'=>'form-control')); ?>
							<small class="form-instruction">Input keywords, separated by comma (,). Ensure the keywords inputed are reflected in the page content.</small><br>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Meta Description</label>
						<div class="col-sm-10">
							<?php echo \Form::textarea('meta_description', \Input::post('meta_description', $page->seo->meta_description), array('rows' => 2,'class'=>'form-control')); ?>
							<small class="form-instruction">Description of what the content of this page is about. Best practices:<br />&bull; Recommendation: 155 characters or less.<br />&bull; Include your main targeted keyword.<br />&bull; Ensure description is unique.</small><br>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">H1 Tag</label>
						<div class="col-sm-10">
							<?php echo \Form::input('h1_tag', \Input::post('h1_tag', $page->seo->h1_tag ?: $page->title), array('class'=>'form-control')); ?>
							<small class="form-instruction">Product Name is H1 by default. This will replace the Product Name as H1.</small>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Redirects</h3>

				</div>
				<div class="panel-body form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">301 Redirect from</label>
						<div class="col-sm-10">
							<?php echo \Form::input('redirect_301', \Input::post('redirect_301', $page->seo->redirect_301), array('class'=>'form-control')); ?>
							<small class="form-instruction">Input the url for this page previously available on your old website (if applicable); otherwise, leave blank.</small>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">Canonical Links</label>
						<div class="col-sm-10">
							<?php echo \Form::input('canonical_links', \Input::post('canonical_links', $page->seo->canonical_links), array('class'=>'form-control')); ?>
							<small class="form-instruction">Input the duplicate urls (links that displays the same page) separated by comma (,).<br />Example: http://myurl.com.au/index, http://www.myurl.com.au/index.html/</small>
						</div>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Robots</h3>

				</div>
				<div class="panel-body form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">Robots Tag</label>
						<div class="col-sm-10">
							<label class="radio"><?php echo \Form::radio('meta_robots_index', 1, \Input::post('meta_robots_index', $page->seo->meta_robots_index)); ?>Index (Default)</label>
							<label class="radio"><?php echo \Form::radio('meta_robots_index', 0, \Input::post('meta_robots_index', $page->seo->meta_robots_index)); ?>No Index</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Robots Follow</label>
						<div class="col-sm-10">
							<label class="radio"><?php echo \Form::radio('meta_robots_follow', 1, \Input::post('meta_robots_follow', $page->seo->meta_robots_follow)); ?>Follow (Default)</label>
							<label class="radio"><?php echo \Form::radio('meta_robots_follow', 0, \Input::post('meta_robots_follow', $page->seo->meta_robots_follow)); ?>No Follow</label>
						</div>
					</div>
				</div>
			</div>

			<div class="text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id' => 'save_button_down', 'class' => 'btn btn-success')); ?>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

	            