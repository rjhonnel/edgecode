<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Content Manager');
		\Breadcrumb::set('Page Manager', 'admin/page/list');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Page Manager</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/page/_action_links', array('hide_show_all' => 1)); ?>
				</div>
			</header>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
			<?php
			// Load page listing table
			echo \Theme::instance()->view('views/page/_listing_table',
				array(
					'pagination' 	=> $pagination,
					'items'			=> $items,
					'status'		=> $status,
				),
				false
			);
			?>
			<?php echo \Form::close(); ?>

			<?php echo \Theme::instance()->view('views/page/_tree_links'); ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>