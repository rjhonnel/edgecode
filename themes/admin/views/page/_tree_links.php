<?php
	// Get pages
	$pages = \Page\Model_Page::find(function($query){
		$query->order_by('sort', 'asc');
		$query->order_by('id', 'asc');
	});
	$page = isset($page) ? $page : false;
	$link = isset($link) ? $link : 'update';
	$selected = isset($selected) ? $selected : false;
?>
<?php if($page): ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h4 class="panel-title pull-left"><?php echo $page ? 'Select Page' : 'Content Managed Pages'; ?></h4>
		<div id="sidetreecontrol" class="sidetreecontrol pull-right"><a href="#">Collapse All</a><a href="#">Expand All</a></div>
	</div>
	<div class="panel-body">
		<div id="sidetree">

			<?php if(!$page && empty($pages)): ?>
				<div class="wide"><span class="req">Note: </span> There are no pages yet.</div>
			<?php else: ?>

				<ul class="treeview" id="tree">

					<?php if($page): ?>
						<li>
							<div class="radio_link_holder">
								<?php echo \Form::radio('parent_id', 0, \Input::post('parent_id', $page->parent_id)); ?>
								<a href="#" onclick="return false;">ROOT</a>
							</div>
						</li>
					<?php endif; ?>

					<?php
						if(!empty($pages))
						{
							// If page parent_id or id is in this array than dont show radio input
							$hide_radio = $page ? array($page->id) : array();

							$list_subpages = function($page_item) use (&$hide_radio, $link, &$list_subpages, $page, $selected)
							{
								?><ul><?php
								foreach($page_item->children as $child)
								{
									if($page && (in_array($child->id, $hide_radio) || in_array($child->parent_id, $hide_radio)))
										array_push($hide_radio, $child->id);

									?>
										<li>
											<?php echo !empty($child->children) ? '<div class="hitarea"></div>' : ''; ?>
											<div class="radio_link_holder">
												<?php if($page): ?>
													<?php echo \Form::radio('parent_id', $child->id, \Input::post('parent_id', $page->parent_id), (in_array($child->id, $hide_radio) ? array('style' => 'display: none;', 'disabled' => 'diabled') : array())); ?>
												<?php endif; ?>
												<a href="<?php echo \Uri::create('admin/page/' . $link . '/' . $child->id); ?>" <?php echo $selected == $child->id ? 'class="active"' : ''; ?>>
													<?php echo $child->title; ?><?php echo !empty($child->children) ? ' <span class="tree_count">('.count($child->children).')</span>' : ''; ?>
												</a>
											</div><?php
									if(!empty($child->children))
										$list_subpages($child);
									else
										?></li><?php

								}
								?></ul><?php
							};

							foreach($pages as $key => $page_item)
							{
								// Only root pages in first pass
								if($page_item->parent_id == 0)
								{
									if($page && in_array($page_item->id, $hide_radio))
										array_push($hide_radio, $page_item->id);

									?>
										<li>
											<?php echo !empty($page_item->children) ? '<div class="hitarea"></div>' : ''; ?>
											<div class="radio_link_holder">
												<?php if($page): ?>
													<?php echo \Form::radio('parent_id', $page_item->id, \Input::post('parent_id', $page->parent_id), (in_array($page_item->id, $hide_radio) ? array('style' => 'display: none;', 'disabled' => 'diabled') : array())); ?>
												<?php endif; ?>
												<a href="<?php echo \Uri::create('admin/page/' . $link . '/' . $page_item->id); ?>" <?php echo $selected == $page_item->id ? 'class="active"' : ''; ?>>
													<?php echo $page_item->title; ?><?php echo !empty($page_item->children) ? ' <span class="tree_count">('.count($page_item->children).')</span>' : ''; ?>
												</a>
											</div><?php
									if(!empty($page_item->children))
										$list_subpages($page_item);
									else
										?></li><?php
								}
							}
						}
					?>

				</ul>

			<?php endif; ?>
		</div>

	</div>
	<div class="panel-footer">
		<?php if($page): ?>
			<small><span class="req">Note: </span> Please select parent page above.</small>
		<?php endif; ?>
	</div>
</div>
<?php endif; ?>
