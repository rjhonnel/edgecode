<div class="layout-content main-content" data-scrollable>
	<div class="main-content-body">
        <?php 
            \Breadcrumb::set('Home', 'admin/dashboard');
            \Breadcrumb::set('Content Manager');
            \Breadcrumb::set('Page Manager', 'admin/page/list');
            \Breadcrumb::set($page->title, 'admin/page/update/' . $page->id);
            \Breadcrumb::set('Accordions', 'admin/page/accordion/list/' . $page->id);
            \Breadcrumb::set('Edit Accordion');
            \Breadcrumb::set($accordion->title);

            echo \Breadcrumb::create_links();
        ?>
        <div class="main-content-body-inner layout-content" data-scrollable>

				<header class="main-content-heading">
					<h4 class="pull-left">Page: Edit Accordions</h4>

					<div class="pull-right">
						<?php echo \Theme::instance()->view('views/page/_action_links'); ?>
					</div>
				</header>
	            
                <?php echo \Theme::instance()->view('views/page/_navbar_links', array('page' => $page)); ?>
	            
                                
				<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">General Information</h3>

						</div>
						<div class="panel-body">
                        	<div class="formRow">
                        		<div class="span7">
                                	<div class="formRow">
                                    	<?php echo \Form::label('Page Title' . '  <span class="text-danger">*</span>'); ?>
                                        <div class="input_holder"><?php echo \Form::input('title', \Input::post('title', $accordion->title), array('class' => 'form-control')); ?></div>
                                    </div>
                                </div>
                            	<div class="span4 right">
                                    <div class="formRow">
                                        <?php echo \Form::label('Status', null, array('class' => 'text_right')); ?>
                                    	<?php echo \Form::select('status', \Input::post('status', $accordion->status), array(
                                        	'1' => 'Active',
                                        	'0' => 'Inactive',
                                        	'2' => 'Active in Period',
                                        ), array('class' => 'toggle_dates select_init form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
                                        
                                    	<div class="toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>
                                    	
                                        	<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
                                        	<div class="row">
                                                <div class="col-md-6">
                                                    <div class="datepicker-holder-control">
                                                      <?php echo \Form::input('active_from', \Input::post('active_from', !is_null($accordion->active_from) ? date('d/m/Y', $accordion->active_from) : ''), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="datepicker-holder-control">
                                                      <?php echo \Form::input('active_to', \Input::post('active_to', !is_null($accordion->active_to) ? date('d/m/Y', $accordion->active_to) : ''), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        	
                                        </div>
                                    </div>
                        		</div>
                            </div>
                            <div class="formRow">
                                <div class="span7">
                                    <div class="formRow">
                                        <?php echo \Form::label('Link'); ?>
                                        <div class="input_holder"><?php echo \Form::input('url', \Input::post('url', $accordion->url), array('class' => 'form-control')); ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="formRow">
                                <?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
                                <div class="clear"></div>
                                <?php echo \Form::textarea('description_full', \Input::post('description_full', $accordion->description_full), array('class' => 'form-control ck_editor')); ?>
                            </div>
                            <div class="clear"></div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h4>
						</div>
						<div class="panel-body">

                        	<table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/page/accordion/sort/image/' . $accordion->id); ?>" class="table table-striped table-bordered ">
                                <tr class="nodrop nodrag blueTableHead">
                                    <th scope="col" class="noresize">Image</th>
                                	<th scope="col">Image Properties</th>
                                   	<?php if(count($accordion->images)): ?>
                                    	<th scope="col" class="center">Delete</th>
                                    <?php endif; ?>
                                </tr>
                                
                                <?php if(is_array($accordion->images)): ?>
                                    <?php foreach($accordion->images as $image): ?>
                                    	
                                        <tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
                                            <td class="center noresize">
	                                        	<img src="<?php echo \Helper::amazonFileURL('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $image->image); ?>" width="74" height="74" alt="<?php echo $image->alt_text; ?>"/>
	                                        </td>
                                            <td class="upload">
                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="noresize">Alt Text</td>
                                                        <td>
                                                        	<div class="input_holder">
                                                        		<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class'=>'form-control')); ?>
                                                        	</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="noresize">Replace Image</td>
                                                        <td>
                                                        	<?php echo \Form::file('image_'.$image->id); ?>
                                                        	<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
											<td width="110">
												<ul class="table-action-inline">
													<?php if(count($accordion->images)): ?>
														<li>
															<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/page/accordion/delete_image/' . $image->id . '/' . $accordion->id); ?>">
																Delete
															</a>
														</li>
													<?php endif; ?>
												</ul>
											</td>


                                        </tr>
                                        
                                    <?php endforeach; ?>
								<?php endif; ?>
                                
                                <?php if(\Config::get('details.image.multiple', false) || empty($accordion->images)): ?>
                                    <tr class="nodrop nodrag">
										<td class="td-thumb">
											<i class="fa fa-picture-o"></i>
										</td>
                                        <td class="upload">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="noresize">Alt Text</td>
                                                    <td>
                                                    	<div class="input_holder">
	                                                        <?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1'), array('class'=>'form-control')); ?>
                                                    	</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="noresize">Choose Image</td>
                                                    <td>
                                                    	<?php echo \Form::file('image_new_1'); ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <?php if(count($accordion->images)): ?>
                                            <td class="icon center"></td>
                                        <?php endif; ?>
                                    </tr>
                                <?php endif; ?>
                                
                            </table>
						</div>
					</div>

					<div class="save_button_holder text-right">
                        <?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
                    	<?php echo \Form::button('exit', 'Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
					</div>
				<?php echo \Form::close(); ?>

		</div>
	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
	            <?php echo ckeditor_replace('ck_editor'); ?>
	            
            	