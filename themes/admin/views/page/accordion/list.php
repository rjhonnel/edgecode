<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Content Manager');
		\Breadcrumb::set('Page Manager', 'admin/page/list');
		\Breadcrumb::set($page->title, 'admin/page/update/' . $page->id);
		\Breadcrumb::set('Accordions', 'admin/page/accordion/list/' . $page->id);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Page: Accordions</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/page/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/page/_navbar_links', array('page' => $page)); ?>

			<?php
			// Load page listing table
			echo \Theme::instance()->view('views/page/accordion/_listing_table',
				array(
					'page'	=> $page,
					'items'	=> $items,
					'pagination'	=> $pagination,
				),
				false
			);
			?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>