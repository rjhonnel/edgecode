<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Content Manager');
		\Breadcrumb::set('Page Manager', 'admin/page/list');
		\Breadcrumb::set($page->title, 'admin/page/update/' . $page->id);
		\Breadcrumb::set('Accordions', 'admin/page/accordion/list/' . $page->id);
		\Breadcrumb::set('Add New');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Page: Add Accordions</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/page/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/page/_navbar_links', array('page' => $page)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title pull-left">General Information</h3>

					</div>
					<div class="panel-body">
						<div class="formRow">
							<div class="span7">
								<div class="formRow">
									<?php echo \Form::label('Accordion Title' . '  <span class="text-danger">*</span>'); ?>
									<div class="input_holder"><?php echo \Form::input('title', \Input::post('title'), array('class' => 'form-control')); ?></div>
								</div>
							</div>
							<div class="span4 right">
								<div class="formRow">
									<?php echo \Form::label('Status', null, array('class' => 'text_right')); ?>
									<?php echo \Form::select('status', \Input::post('status', '1'), array(
										'1' => 'Active',
										'0' => 'Inactive',
										'2' => 'Active in Period',
									), array('class' => 'toggle_dates select_init form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>

									<div class="toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

										<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
										<div class="row">
											<div class="col-md-6">
												<div class="datepicker-holder-control">
													<?php echo \Form::input('active_from', \Input::post('active_from'), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="datepicker-holder-control">
													<?php echo \Form::input('active_to', \Input::post('active_to'), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
												</div>
											</div>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div class="formRow">
							<div class="span7">
								<div class="formRow">
									<?php echo \Form::label('Link'); ?>
									<div class="input_holder"><?php echo \Form::input('url', \Input::post('url'), array('class' => 'form-control')); ?></div>
								</div>
							</div>
						</div>
						<div class="formRow">
							<?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
							<div class="clear"></div>
							<?php echo \Form::textarea('description_full', \Input::post('description_full'), array('class' => 'form-control ck_editor')); ?>
						</div>

					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h4>
					</div>
					<div class="panel-body">


						<table class="table table-striped table-bordered sortable">
							<tr class="nodrop nodrag blueTableHead">
								<th scope="col" class="noresize">Image</th>
								<th scope="col">Image Properties</th>
							</tr>
							<tr>
								<td class="td-thumb">
									<i class="fa fa-picture-o"></i>
								</td>
								<td class="upload">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="110">Alt Text</td>
											<td>
												<div class="input_holder">
													<?php echo \Form::input('alt_text', \Input::post('alt_text'), array('class'=>'form-control')); ?>
												</div>
											</td>
										</tr>
										<tr>
											<td width="110">Replace Image</td>
											<td>
												<?php echo \Form::file('image'); ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>

					</div>
				</div><!-- EOF Images Panel -->

				<div class="save_button_holder text-right">
					<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
					<?php echo \Form::button('update', '<i class="fa fa-check"></i> Save & Update', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
				</div>


			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<?php echo ckeditor_replace('ck_editor'); ?>
      	
