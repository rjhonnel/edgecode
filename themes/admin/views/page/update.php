<div class="main-content">

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Content Manager');
		\Breadcrumb::set('Page Manager', 'admin/page/list');
		\Breadcrumb::set($page->title, 'admin/page/update/' . $page->id);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner">

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Page: General Information</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/page/_action_links', array('create_form' => 1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/page/_navbar_links', array('page' => $page)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="row">
				<div class="col-sm-9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">General Information</h3>

						</div>
						<div class="panel-body">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">Page Name  <span class="text-danger">*</span></label>
									<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title', $page->title), array('class' => 'form-control')); ?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Small Description</label>
									<div class="col-sm-10"><?php echo \Form::textarea('description_intro', \Input::post('description_intro', $page->description_intro), array('class' => 'form-control')); ?></div>
								</div>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Description'); ?>
								<div class="clear"></div>
								<?php echo \Form::textarea('description_full', \Input::post('description_full', $page->description_full), array('class' => 'ck_editor')); ?>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>

						</div>
						<div class="panel-body">
							<div class="header_messages"></div>
							<table rel="<?php echo \Uri::create('admin/page/sort/image/' . $page->id); ?>" class="table table-striped table-bordered sortable">
								<tr class="nodrop nodrag blueTableHead">
									<th scope="col" class="noresize">Image</th>
									<th scope="col">Image Properties</th>
									<?php if(count($page->images) > 1): ?>
										<th>Main Image</th>
										<th scope="col" class="center">Re-order</th>
									<?php endif; ?>
									<?php if((!\Config::get('details.image.required', false) && !empty($page->images)) || count($page->images) > 1): ?>
										<th scope="col" class="center">Delete</th>
									<?php endif; ?>
								</tr>

								<?php if(is_array($page->images)): ?>
									<?php foreach($page->images as $image): ?>

										<tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
											<td class="td-thumb">
												<a href="<?php echo \Helper::amazonFileURL('media/images/' . $image->image); ?>" class="preview-image-popup">
													<img src="<?php echo \Helper::amazonFileURL('media/images/medium/' . $image->image); ?>" width="130" height="130" alt="<?php echo $image->alt_text; ?>"/>
												</a>
											</td>
											<td class="upload btn-file">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td class="noresize">Alt Text</td>
														<td>
															<div class="input_holder">
																<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class'=>'form-control')); ?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="noresize">Replace Image</td>
														<td class="btn-file">
															<?php echo \Form::file('image_'.$image->id); ?>
															<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
														</td>
													</tr>
												</table>
											</td>
											<?php if(count($page->images) > 1): ?>
												<td class="icon center text-center">
													<input type="radio" name="cover_image" value="<?php echo $image->id; ?>" <?php echo $image->cover?'checked="checked"':''; ?>>
												</td>
												<td class="icon center dragHandle">
													<a href="" onclick="return false;">
														Re-order
													</a>
												</td>
											<?php endif; ?>
											<?php if((!\Config::get('details.image.required', false) && !empty($page->images)) || count($page->images) > 1): ?>
												<td class="icon center">
													<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/page/delete_image/' . $image->id . '/' . $page->id); ?>">
														Delete
													</a>
												</td>
											<?php endif; ?>
										</tr>

									<?php endforeach; ?>
								<?php endif; ?>

								<?php if(\Config::get('details.image.multiple', false) || empty($page->images)): ?>
									<tr class="nodrop nodrag">
										<td class="td-thumb">
											<i class="fa fa-picture-o"></i>
										</td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="noresize">Alt Text</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1'), array('class'=>'form-control')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td class="noresize">Choose Image</td>
													<td class="btn-file">
														<?php echo \Form::file('image_new_1'); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if(count($page->images) > 1): ?>
											<td class="icon center"></td>
											<td class="icon center"></td>
										<?php endif; ?>
										<?php if((!\Config::get('details.image.required', false) && !empty($page->images)) || count($page->images) > 1): ?>
											<td class="icon center"></td>
										<?php endif; ?>
									</tr>
								<?php endif; ?>

							</table>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<?php //echo \Form::label('Status', null, array('class' => 'text_right')); ?>
						<?php echo \Form::label('Status', null, array('class' => 'text_right m_r_15')); ?>
						<?php echo \Form::select('status', \Input::post('status', $page->status), array(
							'1' => 'Active',
							'0' => 'Inactive',
							'2' => 'Active in Period',
						), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>


					</div>
					<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

						<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
						<div class="datepicker-holder-control">
							<?php echo \Form::input('active_from', \Input::post('active_from', !is_null($page->active_from) ? date('d/m/Y', $page->active_from) : ''), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
						</div>
						<div class="datepicker-holder-control mt-5px">
							<?php echo \Form::input('active_to', \Input::post('active_to', !is_null($page->active_to) ? date('d/m/Y', $page->active_to) : ''), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
						</div>

					</div>

					<?php echo \Theme::instance()->view('views/page/_tree_links', array('page' => $page)); ?>
				</div>
			</div>

			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
				<?php echo \Form::button('exit', '<i class="fa fa-check"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>



<?php echo ckeditor_replace('ck_editor'); ?>
	            
            	
                            
                            