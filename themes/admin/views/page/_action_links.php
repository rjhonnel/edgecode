<div class="action-list">
<?php if(isset($create_form)): ?>
<a id="save_button_up" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Save</a>
<?php endif; ?>     
<?php if(!isset($hide_add_new)): ?>
	<a href="<?php echo \Uri::create('admin/page/create'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New</a>
<?php endif; ?>
<?php if(!isset($hide_show_all)): ?>
	<a href="<?php echo \Uri::create('admin/page/list'); ?>" class="btn btn-default btn-sm"><i class="fa fa-list"></i> Show All</a>
<?php endif; ?>  
</div>
                            