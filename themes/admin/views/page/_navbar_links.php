<?php \Config::load('page::page', 'page', true); ?>
<?php \Config::load('page::accordion', 'accordion', true); ?>

<?php if(isset($page)): // Used on "UPDATE" pages?>
    <div class="panel-nav-holder">
        <div class="btn-group">
            <a href="<?php echo \Uri::create('admin/page/update/' . $page->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-info-circle"></i> General Information</a>
            <?php if(\Config::get('page.file.enabled', false) || \Config::get('page.video.enabled', false)): ?>
                <?php
                $title = array();
                \Config::get('page.file.enabled', false) and array_push($title, 'Documents');
                \Config::get('page.video.enabled', false) and array_push($title, 'Videos');
                $title = implode(' & ', $title);
                ?>
                <a href="<?php echo \Uri::create('admin/page/update_files/' . $page->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-file"></i> <?php echo $title; ?></a>
            <?php endif; ?>
            <?php if(\Config::get('accordion.enabled', false)): ?>
                <a href="<?php echo \Uri::create('admin/page/accordion/list/' . $page->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-list"></i> Accordions
                    <label class="label label-primary"><?php echo count($page->accordions) > 0 ? count($page->accordions) : ''; ?></label></a>
            <?php endif; ?>
            <a href="<?php echo \Uri::create('admin/page/update_seo/' . $page->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-check-circle"></i> Meta Content</a>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function(){
            if($(".btn-group").length > 0)
            {
                $(".btn-group a.active").removeClass('active');
                $(".btn-group a").each(function(){
                    if(($(this).attr("href") == uri_current) ||
                        (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                    {
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>
<?php else: // Used on "CREATE" page ?>
    <?php if(FALSE): ?>
        <div class="second_menu">
            <ul>
                <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
            </ul>
        </div>
    <?php endif; ?>
<?php endif; ?>