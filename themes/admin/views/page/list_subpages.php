            <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Pages - Sub Pages')); ?>
            
            <?php echo \Theme::instance()->view('views/page/_navbar_links', array('page' => $page)); ?>
            
            <!-- Content -->
		    <div class="content_wrapper">
		    	<div class="content_holder">
		    		
		    		<div class="elements_holder">
		    		
                        <div class="row-fluid breadcrumbs_holder">
                            <div class="breadcrumbs_position">
                                <?php 
                                    \Breadcrumb::set('Home', 'admin/dashboard');
                                    \Breadcrumb::set('Content Manager');
                                    \Breadcrumb::set('Page Manager', 'admin/page/list');
                                    \Breadcrumb::set($page->title, 'admin/page/update/' . $page->id);
                                    \Breadcrumb::set('Sub Pages');

                                    echo \Breadcrumb::create_links();
                                ?>
		                            
                                <?php echo \Theme::instance()->view('views/page/_action_links'); ?>
                            </div>
                        </div>
					    
					    <div class="row-fluid">
					    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                            
		                    <!-- Main Content Holder -->
		                    <div class="content">
		                    	
                                <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
                                    <!-- Accordions Panel -->
                                    <div class="panel">
                                        <div class="panelHeader">
                                            <h4>Sub Page Manager</h4>
                                            
                                            <div class="items_per_page_holder">
                                                <label>Show entries:</label>
                                                <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'select_init items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                                            </div>
                                        </div>

                                        <?php
                                            // Load page listing table
                                            echo \Theme::instance()->view('views/page/_listing_table', 
                                                array(
                                                    'pagination' 	=> $pagination,
                                                    'items'			=> $items,
                                                    'status'		=> $status,
                                                ), 
                                                false
                                            ); 
                                        ?>

                                    </div><!-- EOF Accordions Panel -->
                                <?php echo \Form::close(); ?>
		                        
		                    </div><!-- EOF Main Content Holder -->
		                    
					    	<!-- Sidebar Holder -->
<!--		                    <div class="sidebar">
		                    	<?php echo \Theme::instance()->view('views/page/_tree_links', array('link' => 'subpages')); ?>
		                    </div>-->
                            <!-- EOF Sidebar Holder -->
		                    
		                    <div class="clear"></div>
					    	
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <!-- EOF Content -->
            
            