<div class="top-filter-holder">
    <?php
    echo \Theme::instance()->view('views/_partials/search_filters', array(
        'pagination' => $pagination,
        'status' => $status,
        'module' => 'page',
        'options' => array('status'),
    ), false);
    ?>

    <div class="form-inline pull-right">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>

<div class="header_messages"></div>
<table rel="<?php echo \Uri::create('admin/page/sort/page'); ?>" class="table table-striped table-bordered sortable_rows">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Page Name</th>
        <th scope="col" class="center" style="width: 100px;">Status</th>
        <td scope="col" class="center" width="150">Actions</td>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($items as $item): ?>
        <?php
        $item = (Object) $item;

        // Start - Create dash to indicate a sub page
        $dash = '';
        $count = 0;

        $page = \Page\Model_Page::find_one_by_id($item->id);
        while($page->parent_id != 0)
        {
            $page = \Page\Model_Page::find_one_by_id($page->parent_id);
            $count++;
        }

        while($count != 0)
        {
            $dash .= '- ';
            $count--;
        }
        // End - Create dash to indicate a sub page
        ?>

        <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
            <td>
                <a href="<?php echo \Uri::create('admin/page/update/' . $item->id); ?>">
                    <strong><?php echo $dash; ?> <?php echo $item->title; ?></strong>
                </a>
            </td>
            <td>
                <?php
                // If page is active from certain date
                if($item->status == 2)
                {
                    $dates = array();
                    !is_null($item->active_from) and array_push($dates, date('d/m/Y', $item->active_from));
                    !is_null($item->active_to) and array_push($dates, date('d/m/Y', $item->active_to));

                    if(true)
                    {
                        ?>
                        Active
                        <a class="activeDate" rel="tooltip" title="<?php echo implode(' - ', $dates); ?>">
                            <?php echo \Theme::instance()->asset->img('icon-calendar.png', array('width' => 16, 'height' => 16)); ?>
                        </a>
                        <?php
                    }
                }
                else
                {
                    echo $status[$item->status];
                }
                ?>
            </td>
            <td width="110">
                <ul class="table-action-inline">
                    <li>
                        <a href="<?php echo \Uri::create('admin/page/update/' . $item->id); ?>">Edit</a>
                    </li>
                    <li>
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete page?" href="<?php echo \Uri::create('admin/page/delete/' . $item->id); ?>">Delete</a>
                    </li>
                </ul>
            </td>
        </tr>

    <?php endforeach; ?>

    <?php if (empty($items)): ?>

        <tr class="nodrag nodrop">
            <td colspan="4" class="center"><strong>There are no items.</strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>


