<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Content Manager');
		\Breadcrumb::set('Page Manager', 'admin/page/list');
		\Breadcrumb::set($page->title, 'admin/page/update/' . $page->id);
		\Breadcrumb::set('Files & Links');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Page: Documents & Videos</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/page/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/page/_navbar_links', array('page' => $page)); ?>

			<?php if(\Config::get('details.file.enabled', false)): ?>
				<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
				<?php echo \Form::hidden('file_upload', 1); ?>


				<h4 class="title-legend">
					File Manager <?php echo \Config::get('details.file.required', false) ? '<span class="star-required">*</span> ' : ''; ?>
				</h4>
				<span class="alert alert-info"><small><span class="text-danger">*</span> List of acceptable files types: MS Word, PDF, PowerPoint, MS Access.<br>You can rearrange the order of the files by performing drag and drop.</small></span>
				<span class="span5 sort_message_container"></span>
				<div class="header_messages"></div>
				<table rel="<?php echo \Uri::create('admin/page/sort/file/' . $page->id); ?>" class="sortable table table-striped table-bordered">
					<tr class="nodrop nodrag blueTableHead">
						<th scope="col" class="noresize">File</th>
						<th scope="col">File Properties</th>
						<?php if(count($page->files) > 1): ?>
							<th scope="col" class="center">Re-order</th>
						<?php endif; ?>
						<?php if((!\Config::get('details.file.required', false) && !empty($page->files)) || count($page->files) > 1): ?>
							<th scope="col" class="center">Delete</th>
						<?php endif; ?>
					</tr>

					<?php if(is_array($page->files)): ?>
						<?php foreach($page->files as $file): ?>
							<?php
							// Get file extension
							$extension = strtolower(pathinfo($file->file, PATHINFO_EXTENSION));
							?>
							<tr id="sort_<?php echo $file->id . '_' . $file->sort; ?>">
								<td class="td-thumb">
									<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
										<i class="fa fa-file-<?php echo $extension; ?>-o"></i>
									</a>
								</td>
								<td class="upload">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="noresize"><div class="input_holder">File Name</div></td>
											<td>
												<div class="input_holder"><i>
														<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
															<?php echo $file->file; ?>
														</a>
													</i></div>
											</td>
										</tr>
										<tr>
											<td class="noresize">Title</td>
											<td>
												<div class="input_holder">
													<?php echo \Form::input('title_'.$file->id, \Input::post('title_' . $file->id, $file->title), array('class' => 'form-control')); ?>
												</div>
											</td>
										</tr>
										<tr>
											<td class="noresize">Replace File</td>
											<td>
												<?php echo \Form::file('file_'.$file->id); ?>
												<?php echo \Form::hidden('file_db_'.$file->id, $file->file); ?>
											</td>
										</tr>
									</table>
								</td>
								<?php if(count($page->files) > 1): ?>
									<td width="110" class="dragHandle">
										<ul class="table-action-inline">
											<li>
												<a href="" onclick="return false;">Re-order</a>
											</li>
										</ul>
									</td>
								<?php endif; ?>
								<td width="110">
									<ul class="table-action-inline">
										<?php if((!\Config::get('details.image.required', false) && !empty($page->files)) || count($page->files) > 1): ?>
											<li>
												<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete file?" href="<?php echo \Uri::create('admin/page/delete_file/' . $file->id . '/' . $page->id); ?>">Delete</a>
											</li>
										<?php endif; ?>
									</ul>
								</td>
							</tr>

						<?php endforeach; ?>
					<?php endif; ?>

					<?php if(\Config::get('details.file.multiple', false) || empty($page->files)): ?>
						<tr class="nodrop nodrag">
							<td class="td-thumb">
								<div class="fa fa-file-text-o"></div>
							</td>
							<td class="upload">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="noresize">Title</td>
										<td>
											<div class="input_holder">
												<?php echo \Form::input('title_new_1', \Input::post('title_new_1'), array('class' => 'form-control')); ?>
											</div>
										</td>
									</tr>
									<tr>
										<td class="noresize">Choose File</td>
										<td>
											<?php echo \Form::file('file_new_1'); ?>
										</td>
									</tr>
								</table>
							</td>
							<?php if(count($page->files) > 1): ?>
								<td class="icon center"></td>
							<?php endif; ?>
							<?php if((!\Config::get('details.file.required', false) && !empty($page->files)) || count($page->files) > 1): ?>
								<td class="icon center"></td>
							<?php endif; ?>
						</tr>
					<?php endif; ?>

				</table>

				<div class="save_button_holder text-right mt-5px">
					<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save documents', array('type' => 'submit', 'class' => 'btn btn-success right')); ?>
				</div>

				<?php echo \Form::close(); ?>
			<?php endif; ?>

			<?php if(\Config::get('details.video.enabled', false)): ?>
				<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
				<?php echo \Form::hidden('video_upload', 1); ?>

				<br><br>

				<h4 class="title-legend">Video Links</h4>
				<span class="alert alert-info"><small><span class="text-danger">*</span> Insert a Youtube or Vimeo URL link</small></span>
				<span class="sort_message_container"></span>
				<div class="header_messages"></div>
				<table video="<?php echo \Uri::create('admin/page/video/'); ?>" rel="<?php echo \Uri::create('admin/page/sort/video/' . $page->id); ?>" class="table table-striped table-bordered sortable ">
					<tr class="nodrop nodrag blueTableHead">
						<th scope="col" class="noresize">Video Files</th>
						<th scope="col">Video File Properties</th>
						<?php if(count($page->videos) > 1): ?>
							<th scope="col" class="center">Re-order</th>
						<?php endif; ?>
						<?php if((!\Config::get('details.video.required', false) && !empty($page->videos)) || count($page->videos) > 1): ?>
							<th scope="col" class="center">Delete</th>
						<?php endif; ?>
					</tr>

					<?php if(is_array($page->videos)): ?>
						<?php foreach($page->videos as $video): ?>
							<?php
							$youtube = \App\Youtube::forge();
							$video->details = $youtube->parse($video->url)->get();
							?>
							<tr id="sort_<?php echo $video->id . '_' . $video->sort; ?>">
								<td class="td-thumb">
									<?php if($video->thumbnail): ?>
										<img src="<?php echo \Helper::amazonFileURL('media/videos/' . $video->thumbnail); ?>" class="default" width="80"/>
										<div style="margin-top: 5px; position: relative;">
											<?php echo \Form::checkbox('video_delete_image_' . $video->id, 1, \Input::post('video_delete_image_' . $video->id), array('class' => 'video_delete_image', 'style' => 'margin: 0;')); ?>
											<span style="font-size: 9px;">Use YouTube?</span>
										</div>
									<?php else: ?>
										<img src="<?php echo $video->details['thumbnail']['small']; ?>" class="default" width="80"/>
									<?php endif; ?>
								</td>

								<td class="upload">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="noresize">Video URL</td>
											<td>
												<div class="input_holder">
													<?php echo \Form::input('video_url_'.$video->id, \Input::post('video_url_' . $video->id, $video->url), array('class' => 'form-control video_url')); ?>
												</div>
											</td>
											<td class="noresize v_top_exception">
												<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary')); ?>
											</td>
										</tr>
										<tr>
											<td class="noresize">Video Title</td>
											<td colspan="2">
												<div class="input_holder">
													<?php echo \Form::input('video_title_'.$video->id, \Input::post('video_title_' . $video->id, $video->title), array('class' => 'form-control video_title')); ?>
												</div>
											</td>
										</tr>
										<tr>
											<td class="noresize">Thumbnail Image</td>
											<td>
												<?php echo \Form::file('video_file_'.$video->id); ?>
												<?php if($video->thumbnail) : ?>
													<?php echo \Form::hidden('video_file_db_'.$video->id, $video->thumbnail); ?>
												<?php endif; ?>
											</td>
										</tr>
									</table>
								</td>
								<?php if(count($page->videos) > 1): ?>
									<td width="110" class="dragHandle">
										<ul class="table-action-inline">
											<li>
												<a href="" onclick="return false;">Re-order</a>
											</li>
										</ul>
									</td>
								<?php endif; ?>
								<td width="110">
									<ul class="table-action-inline">
										<?php if((!\Config::get('details.video.required', false) && !empty($page->videos)) || count($page->videos) > 1): ?>
											<li>
												<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete video?" href="<?php echo \Uri::create('admin/page/delete_video/' . $video->id . '/' . $page->id); ?>">Delete</a>
											</li>
										<?php endif; ?>
									</ul>
								</td>
							</tr>

						<?php endforeach; ?>
					<?php endif; ?>

					<?php if(\Config::get('details.video.multiple', false) || empty($page->videos)): ?>
						<tr class="nodrop nodrag">
							<td class="td-thumb">
								<i class="fa fa-youtube"></i>
							</td>
							<td class="upload">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="noresize">Video URL</td>
										<td>
											<div class="input_holder">
												<?php echo \Form::input('video_url_new_1', \Input::post('video_url_new_1'), array('class' => 'video_url form-control')); ?>
											</div>
										</td>
										<td width="5"></td>
										<td class="noresize v_top_exception">
											<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary')); ?>
										</td>
									</tr>
									<tr>
										<td class="noresize">Video Title</td>
										<td colspan="2">
											<div class="input_holder">
												<?php echo \Form::input('video_title_new_1', \Input::post('video_title_new_1'), array('class' => 'video_title form-control')); ?>
											</div>
										</td>
									</tr>
									<tr>
										<td class="noresize">Thumbnail Image</td>
										<td>
											<?php echo \Form::file('video_file_new_1'); ?>
										</td>
									</tr>
								</table>
							</td>
							<?php if(count($page->videos) > 1): ?>
								<td class="icon center"></td>
							<?php endif; ?>
							<?php if((!\Config::get('details.video.required', false) && !empty($page->videos)) || count($page->videos) > 1): ?>
								<td class="icon center"></td>
							<?php endif; ?>
						</tr>
					<?php endif; ?>

				</table>

				<div class="save_button_holder text-right">
					<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save videos', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
				</div>

				<?php echo \Form::close(); ?>
			<?php endif; ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<?php echo \Theme::instance()->asset->js('product/videos.js'); ?>