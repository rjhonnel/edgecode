<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Supplier Manager', 'admin/supplier/list');
        \Breadcrumb::set('Add New Supplier');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Supplier: Add New</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/supplier/_action_links', array('create_form' => 1, 'hide_add_new' => 1)); ?>
                </div>
            </header>


            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
            <?php echo \Form::hidden('details', 1); ?>

            <?php echo \Theme::instance()->view('views/supplier/_form', array('groups' => $groups)); ?>

            <div class="row">
                <div class="col-sm-6">
                    <div class="save_button_holder text-right">
                        <?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
                        <?php echo \Form::button('update', '<i class="fa fa-check"></i> Save & Update', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
                    </div>
                </div>
            </div>







            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>


<!-- This two arrays are requred for script below to work -->
<script type="text/javascript">
    var countries = <?php echo json_encode(\App\Countries::forge()->getCountries()); ?>;
    var states = <?php echo json_encode(\App\States::forge()->getAll()); ?>;
</script>
<?php \Theme::instance()->asset->js('country_state_functions.js', array(), 'basic'); ?>
