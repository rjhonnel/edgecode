<div class="top-filter-holder">
    <?php
    echo \Theme::instance()->view('views/_partials/search_filters', array(
        'pagination' => $pagination,
        'module' => 'supplier ID or full',
        'options' => array(),
    ), false);
    ?>

    <div class="form-inline pull-right">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>

<table class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Supplier ID</th>
        <th scope="col">Name</th>
        <th scope="col">Company Name</th>
        <th scope="col">Email</th>
        <th scope="col">Activated</th>
        <th scope="col" class="center" width="150">Actions</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>
        <?php
        $user_groups = $item->groups();
        $user_group = current($user_groups);
        ?>
        <tr>
            <td>
                <a href="<?php echo \Uri::create('admin/supplier/update/' . $item->id); ?>">
                    <strong><?php if($item->metadata) echo $item->get('metadata.user_id'); ?></strong>
                </a>
            </td>
            <td><?php if($item->metadata) echo $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name'); ?></td>
            <td><?php if($item->metadata) echo $item->get('metadata.business_name'); ?></td>
            <td>
                <?php echo $item->get('email'); ?>
            </td>
            <td class="noresize">
                <?php echo $item->get('activated') == 1 ? 'Yes' : 'No'; ?>
            </td>
            <td width="110">
                <ul class="table-action-inline">
                    <li>
                        <a href="<?php echo \Uri::create('admin/supplier/update/' . $item->id); ?>">Edit</a>
                    </li>
                    <li>
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete supplier?" href="<?php echo \Uri::create('admin/supplier/delete/' . $item->id); ?>">Delete</a>
                    </li>
                </ul>
            </td>
        </tr>

    <?php endforeach; ?>

    <?php if(empty($items)): ?>

        <tr class="nodrag nodrop">
            <td colspan="7" class="center"><strong>There are no items.</strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>