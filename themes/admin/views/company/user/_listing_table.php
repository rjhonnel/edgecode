<div class="top-filter-holder">
    <?php
    echo \Theme::instance()->view('views/_partials/search_filters', array(
        'pagination' => $pagination,
        'module' => 'customer ID or full',
        'options' => array(),
    ), false);
    ?>

    <div class="form-inline show-table-filter">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>

<table class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Customer ID</th>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Activated</th>
        <th scope="col" class="center" width="150">Actions</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>
     
        <tr>
            <td>
                <a href="<?php echo \Uri::create('admin/user/update/' . $item->metadata['user_id']); ?>">
                    <strong><?php if($item->metadata) echo $item->metadata['user_id'];?></strong>
                </a>

            </td>
            <td><a href="<?php echo \Uri::create('admin/user/update/' . $item->metadata['user_id']); ?>">
                    <strong><?php if($item->metadata) echo $item->metadata['first_name'] . ' ' . $item->metadata['last_name']; ?></strong>
                </a>
            </td>
            <td>
                <?php echo $item->email; ?>
            </td>
            <td class="noresize">
                <?php echo $item->get('activated') == 1 ? 'Yes' : 'No'; ?>
            </td>
            <td width="110">
                <ul class="table-action-inline">
                    <li>
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete customer?" href="<?php echo \Uri::create('admin/user/delete/' . $item->metadata['user_id']); ?>">Delete</a>
                    </li>
                </ul>
            </td>
        </tr>
       
    <?php endforeach; ?>

    <?php if(empty($items)): ?>

        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>There are no items.</strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>