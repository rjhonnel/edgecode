<div class="panel-nav-holder">
    <div class="btn-group">
        <a href="<?php echo \Uri::create('admin/company/update/'.$company_id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-info-circle"></i> General Information</a>
        <a href="<?php echo \Uri::create('admin/company/user/list/'.$company_id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-users"></i> Customers</a>
        <a href="<?php echo \Uri::create('admin/company/order/list/'.$company_id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-external-link"></i> Orders</a>
        <a href="<?php echo \Uri::create('admin/company/notes/list/'.$company_id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-sticky-note"></i> All Notes</a>
    </div>
</div>

<style>
    .panel-nav-holder .btn {
        min-width: 160px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
        if($(".btn-group").length > 0)
        {
            $(".btn-group a.active").removeClass('active');
            $(".btn-group a").each(function(){
                if(($(this).attr("href") == uri_current) ||
                    (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                {
                    $(this).addClass('active');
                }
            });
        }
    });
</script>