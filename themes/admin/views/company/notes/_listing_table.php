<div class="top-filter-holder">
    <div class="form-inline show-table-filter">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>

<div class="panel panel-default">
               
<div class="panel-body">
    <div class="notes">
        <?php if($items): ?>
            <?php foreach($items as $item): ?>
                <div class="note">
                    <div class="note-indicator"></div>
                    <div class="note-item-content">
                        <div class="note-content" style="white-space: pre-wrap;"><?php echo $item->note; ?></div>
                        <div class="note-footer">
                            <span class="note-updated"> <a href="<?php echo \Uri::create('admin/user/update/' . $item->user_id); ?>"><?php echo $item->name; ?></a><?php echo ' - '.date('d/m/Y H:i:s', $item->created_at); ?> </span>
                            <span class="note-options"> <a href="javascript:void()" class="edit-note"> Edit </a> / <a class="text-danger confirmation-pop-up" href="<?php echo \Uri::create('admin/user/delete_notes/'.$item->user_id.'/'.$item->id); ?>" data-message="Are you sure you want to delete note?"> Delete </a> </span>
                        </div>
                    </div>
                    <div class="note-state-edit">
                        <div class="note-content">
                            <textarea class="form-control"><?php echo $item->note; ?></textarea>
                        </div>
                        <div class="note-footer">
                            <span class="note-updated"> <a href="<?php echo \Uri::create('admin/user/update/' . $item->user_id); ?>"><?php echo $item->name; ?></a><?php echo ' - '.date('d/m/Y H:i:s', $item->created_at); ?> </span>
                            <span class="note-options"> <a href="javascript:void()" class="cancel-edit-note"> Cancel </a> / <a href="javascript:void()" class="update-edit-note" data-url="<?php echo \Uri::create('admin/user/update_notes/'.$item->user_id.'/'.$item->id); ?>"> Update </a> </span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <small class="alert alert-info">There are no items.</small>
        <?php endif; ?>
    </div>
    <div class="pagination-holder">
        <?php echo $pagination->render(); ?>
    </div>
</div>
</div>