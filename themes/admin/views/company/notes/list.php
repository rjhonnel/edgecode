<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
            \Breadcrumb::set('Home', 'admin/dashboard');
            \Breadcrumb::set('Companies');
            \Breadcrumb::set($company->company_name,'admin/company/update/'.$company->id);
            \Breadcrumb::set('Notes');

            // If viewing category products show category title

            echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Notes</h4>
            </header>

            <?php echo \Theme::instance()->view('views/company/_navbar_links',array('company_id' => $company->id)); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>

            <?php
            // Load page listing table
            echo \Theme::instance()->view('views/company/notes/_listing_table',
                array(
                    'pagination' 	=> $pagination,
                    'items'			=> $items,
                ),
                false
            );
            ?>
            <?php echo \Form::close(); ?>
        </div>
    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>