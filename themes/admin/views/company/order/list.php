<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
            \Breadcrumb::set('Home', 'admin/dashboard');
            \Breadcrumb::set('Companies');
            \Breadcrumb::set($company->company_name,'admin/company/update/'.$company->id);
            \Breadcrumb::set('Orders', 'admin/order/list');

            // If viewing category products show category title

            echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Orders</h4>

             <!--    <div class="pull-right">
                    <?php
                        if(isset($group))
                            echo \Theme::instance()->view('views/user/_action_links');
                        else
                            echo \Theme::instance()->view('views/user/_action_links', array('hide_show_all' => 1, 'show_bulk_add' => 1));
                    ?>
                </div> -->
            </header>

            <?php echo \Theme::instance()->view('views/company/_navbar_links',array('company_id' => $company->id)); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>

            <?php
            // Load page listing table
            echo \Theme::instance()->view('views/company/order/_listing_table',
                array(
                    'pagination'    => $pagination,
                    'items'         => $items,
                    'get_attributes' => $get_attributes,
                ),
                false
            );
            ?>
            <?php echo \Form::close(); ?>
        </div>
    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>