<div class="top-filter-holder">
    <?php
    echo \Theme::instance()->view('views/_partials/search_filters', array(
        'pagination' => $pagination,
        'module' => 'company',
        'options' => array(),
    ), false);
    ?>

    <div class="form-inline pull-right">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>

<table class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Name</th>
        <th scope="col">Number</th>
        <th scope="col">Phone</th>
        <th scope="col">Email</th>
        <th scope="col">Suburb</th>
        <th scope="col">State</th>
        <th scope="col">Country</th>
        <th scope="col" class="center" width="150">Actions</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <tr>
            <td>
                <a href="<?php echo \Uri::create('admin/company/update/' . $item->id); ?>">
                    <strong><?php echo $item->company_name; ?></strong>
                </a>
            </td>
            <td><?php echo $item->company_mobile; ?></td>
            <td><?php echo $item->company_phone; ?></td>
            <td><?php echo $item->company_email; ?></td>
            <td><?php echo $item->company_suburb; ?></td>
            <td><?php echo $item->company_state; ?></td>
            <td><?php echo $item->company_country; ?></td>
            <td width="110">
                <ul class="table-action-inline">
                    <li>
                        <a href="<?php echo \Uri::create('admin/company/update/' . $item->id); ?>">Edit</a>
                    </li>
                    <li>
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete company?" href="<?php echo \Uri::create('admin/company/delete/' . $item->id); ?>">Delete</a>
                    </li>
                </ul>
            </td>
        </tr>

    <?php endforeach; ?>

    <?php if(empty($items)): ?>

        <tr class="nodrag nodrop">
            <td colspan="8" class="center"><strong>There are no items.</strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>