 <div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-12"><h3 class="panel-title pull-left">Company Information</h3></div>
        </div>
    </div>
    <div class="panel-body form-horizontal">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Name  <span class="text-danger">*</span></label>
                    <div class="col-sm-6"><?php echo \Form::input('company_name', \Input::post('company_name',isset($company) ? $company->company_name : ''),array('class' => 'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Company Number</label>
                    <div class="col-sm-6"><?php echo \Form::input('company_mobile', \Input::post('company_mobile',isset($company) ? $company->company_mobile : ''),array('class' => 'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-6"><?php echo \Form::input('company_phone', \Input::post('company_phone',isset($company) ? $company->company_phone : ''),array('class' => 'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-6"><?php echo \Form::input('company_email', \Input::post('company_email',isset($company) ? $company->company_email : ''),array('class' => 'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-6"><?php echo \Form::input('company_address', \Input::post('company_address',isset($company) ? $company->company_address : ''),array('class' => 'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Suburb/City</label>
                    <div class="col-sm-6"><?php echo \Form::input('company_suburb', \Input::post('company_suburb',isset($company) ? $company->company_suburb : ''),array('class' => 'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">State</label>
                    <div class="col-sm-6">
                        <?php if(!\App\States::forge()->getStateProvinceArray(\Input::post('country', 'AU'))): ?>
                            <?php echo \Form::input('company_state', \Input::post('company_state',isset($company) ? $company->company_state : ''), array('class' => 'form-control')); ?>
                        <?php else: ?>
                            <?php echo \Form::select('company_state', \Input::post('company_state',isset($company) ? $company->company_state : ''), \App\States::forge()->getStateProvinceArray(\Input::post('company_country', 'AU')), array('class' => 'form-control')); ?>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Postcode</label>
                    <div class="col-sm-6"><?php echo \Form::input('company_postcode', \Input::post('company_postcode',isset($company) ? $company->company_postcode : ''),array('class' => 'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Country</label>
                    <div class="col-sm-6">
                        <?php echo \Form::select('company_country', \Input::post('company_country',isset($company) ? $company->company_country : ''), \App\Countries::forge()->getCountries(), array('class' => 'form-control')); ?>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</div>