<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Companies');
        \Breadcrumb::set('Company Manager', 'admin/Company/list');
        \Breadcrumb::set($company->company_name);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Edit Company</h4>
                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/company/_action_links', array('create_form' => 1)); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/company/_navbar_links',array('company_id' => $company->id)); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
      
           <?php echo \Theme::instance()->view('views/company/_form', array('company' => $company)); ?>

            <div class="row">
                <div class="col-sm-12">
                    <div class="save_button_holder text-right">
                        <?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
                        <?php echo \Form::button('exit', '<i class="fa fa-check"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
                    </div>
                </div>
            </div>

            <?php echo \Form::close(); ?>

        </div>
    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

