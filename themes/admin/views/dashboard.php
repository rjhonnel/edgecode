<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Dashboard');

        echo \Breadcrumb::create_links();
        ?>


        <div class="main-content-body-inner layout-content" data-scrollable>


            <header class="main-content-heading">
                <h4>Dashboard</h4>
            </header>



            <h3 class="title-legend mt-5px"><?php echo $items['type']; ?></h3>
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?php echo $items['orders']; ?></h3>

                            <p>New Orders</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="<?php echo \Uri::create('admin/order/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3>$<?php echo nf($items['sales']); ?></h3>

                            <p>Sales</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="<?php echo \Uri::create('admin/reports/sales_by_date'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3><?php echo $items['users']; ?></h3>

                            <p>User Registrations</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="<?php echo \Uri::create('admin/user/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-red">
                        <div class="inner">
                            <h3><?php echo $items['visits']; ?></h3>

                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="<?php echo \Uri::create('admin/user/list'); ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>


            <div class="panel panel-default" id="pad-wrapper">

                <!-- statistics chart built with jQuery Flot -->
                <div class="chart">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title pull-left mt-5px">Statistics</h4>
                        <div class="btn-group pull-right" role="group" aria-label="Statistics Filter">
                            <a href="/admin/dashboard/d" class="glow btn btn-sm btn-default <?php echo (Uri::segment(3) == 'd') ? 'active' : '' ?>" >DAY</a>
                            <a href="/admin/dashboard/w" class="glow btn btn-sm btn-default <?php echo (Uri::segment(3) == 'w') ? 'active' : '' ?>">WEEK</a>
                            <a href="/admin/dashboard/m" class="glow btn btn-sm btn-default <?php echo (Uri::segment(3) == 'm') ? 'active' : '' ?>">MONTH</a>
                            <a href="/admin/dashboard/y" class="glow btn btn-sm btn-default <?php echo (Uri::segment(3) == 'y') ? 'active' : '' ?>">YEAR</a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <canvas id="dashboardChart" height="100"></canvas>
                    </div>
                </div>
                <!-- end statistics chart -->

            </div>
        </div>
    </div>

    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>

</div>

<script type="text/javascript">
    var sales_chart = [<?php echo $items['sales_chart']; ?>];
    var visits_chart = [<?php echo $items['visits_chart']; ?>];
</script>

<?php
    // dashboard js
    echo \Theme::instance()->asset->js('plugins/Chart.min.js');
    echo \Theme::instance()->asset->js('plugins/moment.min.js');
    echo \Theme::instance()->asset->js('dashboard/dashboard.js');
?>

