<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Content Manager');
		\Breadcrumb::set('Our Team Manager', 'admin/team/list');
		\Breadcrumb::set('Add New');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Add Team</h4>

				<div class="pull-right">

					<?php echo \Theme::instance()->view('views/team/_action_links', array('create_form' => 1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/team/_navbar_links'); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => '')); ?>

			<div class="row">
				<div class="col-sm-9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">General Information</h3>
						</div>
						<div class="panel-body">

							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">Member Name  <span class="text-danger">*</span></label>
									<div class="col-sm-10">
										<?php echo \Form::input('name', \Input::post('name'), array('class'=>'form-control')); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Position</label>
									<div class="col-sm-10"><?php echo \Form::input('position', \Input::post('position'), array('class'=>'form-control')); ?></div>
								</div>
							</div>

							<div class="form-group mb-0">
								<?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
								<?php echo \Form::textarea('description_full', \Input::post('description_full'), array('class' => 'form-control')); ?>
							</div>

						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<?php echo \Form::label('Status', null, array('class' => 'text_right m_r_15')); ?>
						<?php echo \Form::select('status', \Input::post('status', '1'), array(
							'1' => 'Active',
							'0' => 'Inactive',
							'2' => 'Active in Period',
						), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>

					</div>
					<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

						<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
						<div class="datepicker-holder-control">
							<?php echo \Form::input('active_from', \Input::post('active_from'), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
						</div>
						<div class="datepicker-holder-control mt-5px">
							<?php echo \Form::input('active_to', \Input::post('active_to'), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Social Media URL<?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>

				</div>
				<div class="panel-body">
					<div class="formRow">
						<div class="span7">
							<div class="formRow">
								<?php echo \Form::label('Facebook'); ?>
								<div class="input_holder"><?php echo \Form::input('facebook', \Input::post('facebook')); ?></div>
							</div>
							<div class="formRow">
								<?php echo \Form::label('Twitter'	); ?>
								<div class="input_holder"><?php echo \Form::input('twitter', \Input::post('twitter')); ?></div>
							</div>
							<div class="formRow">
								<?php echo \Form::label('LinkedIn'	); ?>
								<div class="input_holder"><?php echo \Form::input('linkedin', \Input::post('linkedin')); ?></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>

				</div>
				<div class="panel-body">

					<table class="sortable table table-striped table-bordered">
						<tr class="nodrop nodrag blueTableHead">
							<th scope="col" class="noresize">Image</th>
							<th scope="col">Image Properties</th>
						</tr>
						<tr>
							<td class="td-thumb">
								<i class="fa fa-picture-o"></i>
							</td>
							<td class="upload">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="110">Alt Text</td>
										<td>
											<div class="input_holder">
												<?php echo \Form::input('alt_text', \Input::post('alt_text'), array('class' => 'form-control')); ?>
											</div>
										</td>
									</tr>
									<tr>
										<td width="110">Replace Image</td>
										<td>
											<?php echo \Form::file('image'); ?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>


			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-primary', 'value' => '1')); ?>
				<?php echo \Form::button('update', '<i class="fa fa-check"></i> Save & Update', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>



<?php echo ckeditor_replace('ck_editor'); ?>
      	

                            