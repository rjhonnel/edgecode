<?php \Config::load('team::team', 'team', true); ?>
<?php \Config::load('team::accordion', 'accordion', true); ?>

<?php if(isset($team)): // Used on "UPDATE" team?>
    <script type="text/javascript">
        $(document).ready(function(){
            if($(".second_menu").length > 0)
            {
                $(".second_menu ul li a.active").removeClass('active');
                $(".second_menu ul li a").each(function(){
                    if(($(this).attr("href") == uri_current) ||
                        (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                    {
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>

    <!--
            <div class="second_menu">
            <div class="second_menu_wrapper">
            <ul>
                <li><a href="<?php echo \Uri::create('admin/team/update/' . $team->id); ?>" class="circle_information active" rel="tooltip" title="General Information"></a></li>
            </ul>
            </div>
        </div>-->
<?php else: // Used on "CREATE" team?>
    <?php if(FALSE): ?>
        <div class="second_menu">
            <ul>
                <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
            </ul>
        </div>
    <?php endif; ?>
<?php endif; ?>