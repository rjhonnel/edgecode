<div class="action-list">
<?php if(isset($create_form)): ?>
<a id="save_button_up" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i>Save</a>
<?php endif; ?>     
<a href="<?php echo \Uri::create('admin/team/create'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Add New</a>
<a href="<?php echo \Uri::create('admin/team/list'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> Show All</a>
</div>
                            
                            