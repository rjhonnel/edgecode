<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Content Manager');
		\Breadcrumb::set('Our Team Manager', 'admin/team/list');
		\Breadcrumb::set('Edit Member');
		\Breadcrumb::set($team->title);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Team</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/team/_action_links', array('create_form' => 1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/team/_navbar_links', array('team' => $team)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">General Information</h3>

				</div>
				<div class="panel-body">

					<div class="row">
						<div class="col-sm-9">
							<div class="form-group">
								<?php echo \Form::label('Member Name' . '  <span class="text-danger">*</span>'); ?>
								<div class="input_holder"><?php echo \Form::input('name', \Input::post('name', $team->name),array('class'=>'form-control')); ?></div>
							</div>

							<div class="form-group">
								<?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
								<?php echo \Form::textarea('description_full', \Input::post('description_full', $team->description_full), array('class' => 'form-control')); ?>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="form-group">
								<?php echo \Form::label('Status', null, array('class' => '')); ?>
								<?php echo \Form::select('status', \Input::post('status', $team->status), array(
									'1' => 'Active',
									'0' => 'Inactive',
									'2' => 'Active in Period',
								), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
							</div>
							<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

								<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
								<div class="datepicker-holder-control">
									<?php echo \Form::input('active_from', \Input::post('active_from', !is_null($team->active_from) ? date('m/d/Y', $team->active_from) : ''), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
								</div>
								<div class="datepicker-holder-control mt-5px">
									<?php echo \Form::input('active_to', \Input::post('active_to', !is_null($team->active_to) ? date('m/d/Y', $team->active_to) : ''), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
								</div>


							</div>
						</div>
					</div>


				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Social Media URL<?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>

				</div>
				<div class="panel-body form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">Facebook</label>
						<div class="col-sm-10"><?php echo \Form::input('facebook', \Input::post('facebook', $team->facebook), array('class' => 'form-control')); ?></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Twitter</label>
						<div class="col-sm-10"><?php echo \Form::input('twitter', \Input::post('twitter', $team->twitter), array('class' => 'form-control')); ?></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">LinkedIn</label>
						<div class="col-sm-10"><?php echo \Form::input('linkedin', \Input::post('linkedin', $team->linkedin), array('class' => 'form-control')); ?></div>
					</div>
				</div>
				</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>

				</div>
				<div class="panel-body">

					<div class="sort_message_container"></div>

					<table rel="<?php echo \Uri::create('admin/team/sort/image/' . $team->id); ?>" class="sortable table table-striped table-bordered">
						<tr class="nodrop nodrag blueTableHead">
							<th scope="col" class="noresize">Image</th>
							<th scope="col">Image Properties</th>
							<?php if(count($team->images) > 1): ?>
								<th scope="col" class="center">Re-order</th>
							<?php endif; ?>
							<?php if((!\Config::get('details.image.required', false) && !empty($team->images)) || count($team->images) > 1): ?>
								<th scope="col" class="center">Delete</th>
							<?php endif; ?>
						</tr>

						<?php if(is_array($team->images)): ?>
							<?php foreach($team->images as $image): ?>

								<tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
									<td class="center noresize">
										<img src="<?php echo \Uri::create('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $image->image); ?>" width="74" height="74" alt="<?php echo $image->alt_text; ?>"/>
									</td>
									<td class="upload">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="110">Alt Text</td>
												<td>
													<div class="input_holder">
														<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class'=>'form-control')); ?>
													</div>
												</td>
											</tr>
											<tr>
												<td width="110">Replace Image</td>
												<td>
													<?php echo \Form::file('image_'.$image->id); ?>
													<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
												</td>
											</tr>
										</table>
									</td>

									<td width="110">
										<ul class="table-action-inline">
											<?php if(count($team->images) > 1): ?>
												<li>
													<a href="" onclick="return false;">Order</a>
												</li>
											<?php endif; ?>
											<?php if((!\Config::get('details.image.required', false) && !empty($item->images)) || count($item->images) > 1): ?>
												<li>
													<a href="<?php echo \Uri::create('admin/team/delete_image/' . $image->id . '/' . $team->id); ?>">
														Delete
													</a>
												</li>
											<?php endif; ?>
										</ul>
									</td>
								</tr>

							<?php endforeach; ?>
						<?php endif; ?>

						<?php if(\Config::get('details.image.multiple', false) || empty($team->images)): ?>
							<tr class="nodrop nodrag">
								<td class="td-thumb">
									<i class="fa fa-picture-o"></i>
								</td>
								<td class="upload">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="110">Alt Text</td>
											<td>
												<div class="input_holder">
													<?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1'), array('class'=>'form-control')); ?>
												</div>
											</td>
										</tr>
										<tr>
											<td width="110">Choose Image</td>
											<td>
												<?php echo \Form::file('image_new_1'); ?>
											</td>
										</tr>
									</table>
								</td>
								<?php if(count($team->images) > 1): ?>
									<td class="icon center"></td>
								<?php endif; ?>
								<?php if((!\Config::get('details.image.required', false) && !empty($team->images)) || count($team->images) > 1): ?>
									<td class="icon center"></td>
								<?php endif; ?>
							</tr>
						<?php endif; ?>

					</table>
				</div>
			</div>


			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-primary', 'value' => '1')); ?>
				<?php echo \Form::button('exit', '<i class="fa fa-check"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<?php echo ckeditor_replace('ck_editor'); ?>
	            
            	
                            
                            