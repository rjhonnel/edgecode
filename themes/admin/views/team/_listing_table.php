<?php
echo \Theme::instance()->view('views/_partials/search_filters', array(
    'pagination' => $pagination,
    'status' => $status,
    'module' => 'team',
    'options' => array('status'),
), false);
?>
<div class="panel-body">
    <table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered sortable_rows" width="100%">
        <thead>
            <tr class="blueTableHead">
                <th scope="col">Member Name</th>
                <th scope="col" class="center" style="width: 70px;">Status</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>

            <?php foreach($items as $item): ?>
                <?php $item = (Object)$item; ?>

                <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
                    <td>
                        <a href="<?php echo \Uri::create('admin/team/update/' . $item->id); ?>">
                            <strong><?php echo $item->name; ?></strong>
                        </a>
                    </td>
                    <td>
                        <?php
                            // If page is active from certain date
                            if($item->status == 2)
                            {
                                $dates = array();
                                !is_null($item->active_from) and array_push($dates, date('m/d/Y', $item->active_from));
                                !is_null($item->active_to) and array_push($dates, date('m/d/Y', $item->active_to));

                                if(true)
                                {
                                    ?>
                                        Active
                                        <a class="activeDate" rel="tooltip" title="<?php echo implode(' - ', $dates); ?>">
                                            <?php echo \Theme::instance()->asset->img('icon-calendar.png', array('width' => 16, 'height' => 16)); ?>
                                        </a>
                                    <?php
                                }
                            }
                            else
                            {
                                echo $status[$item->status];
                            }
                        ?>
                    </td>
                    <td width="110">
                        <ul class="table-action-inline">
                            <li>
                                <a href="<?php echo \Uri::create('admin/team/update/' . $item->id); ?>">Edit</a>
                            </li>
                            <li>
                                <a class="text-danger" href="<?php echo \Uri::create('admin/team/delete/' . $item->id); ?>">Delete</a>
                            </li>
                        </ul>
                    </td>
                </tr>

            <?php endforeach; ?>

            <?php if(empty($items)): ?>

                <tr class="nodrag nodrop">
                    <td colspan="4" class="center"><strong>There are no items.</strong></td>
                </tr>

            <?php endif; ?>

        </tbody>
    </table>

    <div class="pagination-holder">
        <?php echo $pagination->render(); ?>
    </div>

</div>