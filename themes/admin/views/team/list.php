<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Content Manager');
        \Breadcrumb::set('Our Team Manager', 'admin/team/list');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Our Team</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/team/_action_links'); ?>
                </div>
            </header>

            <div class="panel panel-default">
                <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left mt-5px">List</h3>

                        <div class="form-inline pull-right">
                            <label>Show entries:</label>
                            <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                        </div>
                    </div>
                    <?php
                    // Load team listing table
                    echo \Theme::instance()->view('views/team/_listing_table',
                        array(
                            'pagination' 	=> $pagination,
                            'items'			=> $items,
                            'status'		=> $status,
                        ),
                        false
                    );
                    ?>
                <?php echo \Form::close(); ?>
            </div>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>