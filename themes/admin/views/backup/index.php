<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Backup', 'admin/backup');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Backup</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/settings/option/_action_links'); ?>
                </div>
            </header>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'form-horizontal')); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Backup</h3>
                </div>
                <div class="panel-body">

                    <?php
                        $hold_enable = (isset($settings['enable']) ? $settings['enable'] : 0);
                        $hold_email = (isset($settings['email']) ? $settings['email'] : '');
                        $hold_period = (isset($settings['period']) ? $settings['period'] : '');
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Enable Database Backup: </label>
                        <div class="col-md-4">
                            <label><?php echo \Form::checkbox('enable', 1, \Input::post('enable', $hold_enable)); ?> Yes</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Email Address:  <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input class="form-control" name="email" value="<?php echo \Input::post('email', $hold_email) ?>" type="text" id="form_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Period:  <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <select name="period" class="form-control">
                                <option value="" >-</option>
                                <option value="daily" <?php echo $hold_period == 'daily' ? 'selected="selected"' : '' ?> >Daily</option>
                                <option value="weekly" <?php echo $hold_period == 'weekly' ? 'selected="selected"' : '' ?> >Weekly</option>
                            </select>
                        </div>
                    </div>
                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success right" type="submit"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
