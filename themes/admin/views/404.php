<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<title><?php echo !empty($title) ? $title . ' | ' : ''; ?>Edge Commerce</title>
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon.ico/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon.ico/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/favicon.ico/manifest.json">
	<link rel="mask-icon" href="/favicon.ico/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">

	<script type="text/javascript">
		var baseUrl = '<?php echo \Uri::create('/'); ?>';
		// Base admin URL
		var uri_base = '<?php echo \Uri::create('admin'); ?>';
		// Current admin URL w/o query string
		var uri_current = '<?php echo \Uri::admin('current')?>';
		// Curent theme asset path
		var theme_path = '<?php echo \Uri::create(\Theme::instance()->asset_path('')); ?>';
		// Current admin URL with query string
		var uri_current_with_query = '<?php echo \Uri::admin('current') . (!empty($_SERVER['QUERY_STRING']) ? '?' . $_SERVER['QUERY_STRING']  : '') ; ?>';

	</script>

	<?php
	echo \Theme::instance()->asset->js('plugins/jquery-2.2.4.min.js');

	echo \Theme::instance()->asset->css('../assets/css/styles.css');
	echo \Theme::instance()->asset->css('plugins/jquery-ui-1.12.1.css');
	?>

</head>

<body class="<?php echo !isset($full_page) ? 'with_side_menu' : ''; ?>">

	<div class="main_wrapper">

		<!-- Header -->
		<?php echo \Theme::instance()->view('views/_partials/header'); ?>
		<!-- EOF Header -->

		<!-- Content -->
		<div class="layout-content main-content" data-scrollable>
			<div class="main-content-body">
				<div class="section-404">
					<h1>404</h1>
					<h4>We can't find that!</h4>
				</div> <!--  .section-404 -->
			</div>
			<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
		</div>
		<!-- EOF Content -->

	</div>
	<style>
		.section-404{
			padding: 100px 0;
			text-align: center;	
		}
		.section-404 h1{
			font-size: 150px;
		}
		.section-404 h1, .section-404 h4{
			color: #cacaca;
			text-shadow: #fff 0px 1px 0px;
		}
		.section-404 h4{
			text-transform: uppercase;
		}
		body{
			background-color: #f4f4f4;
		}
	</style>
	<!-- Footer -->
	<?php echo \Theme::instance()->view('views/_partials/footer'); ?>

	<?php
	echo \Theme::instance()->asset->js('../assets/js/all.js');
	echo \Theme::instance()->asset->js('plugins/jquery-ui-1.12.1.js');
	echo \Theme::instance()->asset->js('main.js');
	?>
</body>

</html>
