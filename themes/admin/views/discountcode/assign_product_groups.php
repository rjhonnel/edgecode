 <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Discount Codes')); ?>          

 <?php echo \Theme::instance()->view('views/discountcode/_navbar_links'); ?>

            <!-- Content -->
		    <div class="content_wrapper">
		    	<div class="content_holder">
		    		
		    		<div class="elements_holder">
		    		
                        <div class="row-fluid breadcrumbs_holder">
                            <div class="breadcrumbs_position">
                                <?php 
                                    \Breadcrumb::set('Home', 'admin/dashboard');
                                    \Breadcrumb::set('Discount Codes');
                                    \Breadcrumb::set('Discount Codes List', 'admin/discountcodes/list');

                                    // If viewing category products show category title
                                    if(isset($category))
                                        \Breadcrumb::set($category->title);
                                    
                                    echo \Breadcrumb::create_links();
                                ?>
		                            
                                <?php echo \Theme::instance()->view('views/discountcode/_action_links'); ?>
                            </div>
                        </div>
                        
					    <div class="row-fluid">
					    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                            
		                    <!-- Main Content Holder -->
		                    <div class="content">
                                
                                
                                 <div class="panel">
                                    <div class="panelHeader">
                                        <h4>Assign</h4>
                                    </div>
                                     <div class="panelContent">
                                         <label class="radio radio_inline">
                                             <input type="radio" name="1">
                                             All Products
                                         </label>
                                         <label class="radio radio_inline">
                                             <input type="radio" name="1" checked="checked">
                                             Product Groups
                                         </label>
                                         <label class="radio radio_inline">
                                             <input type="radio" name="1">
                                             Product Categories
                                         </label>
                                         <label class="radio radio_inline">
                                             <input type="radio" name="1">
                                             Selected Products
                                         </label>
                                         <div class="clear"></div>
                                     </div>
                                </div>
                                
                                
                                <div class="panel">
                                    <div class="panelHeader">
                                        <h4>Add Product Groups</h4>
                                    </div>
                                    <div class="panelContent">

                                        <table width="100%">

                                            <tr>
                                                <td width="49.5%" valign="top">

                                                    <table class="controls">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <h4>Select Product Groups</h4>
                                                                </td>
                                                                <td>
                                                                    <div class="right">
                                                                        results per page
                                                                        <select class="select_init" name="" id="form_">
                                                                            <option value="10">10</option>
                                                                            <option value="20" selected="selected">20</option>
                                                                            <option value="30">30</option>
                                                                            <option value="50">50</option>
                                                                            <option value="100">100</option>
                                                                            <option value="200">200</option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>	

                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 separated">
                                                        <thead>
                                                            <tr class="blueTableHead">
                                                                <th scope="col"><input type="checkbox" /></th>
                                                                <th scope="col">Category Name</th>
                                                                <th scope="col" class="center" style="width: 40px;">View</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="noresize"><input type="checkbox" /></td>
                                                                <td>Special Products</td>
                                                                <td class="icon center">
                                                                    <a href="#">
                                                                        Update
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                           <tr>
                                                                <td class="noresize"><input type="checkbox" /></td>
                                                                <td>Special Products</td>
                                                                <td class="icon center">
                                                                    <a href="#">
                                                                        Update
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="noresize"><input type="checkbox" /></td>
                                                                <td>Special Products</td>
                                                                <td class="icon center">
                                                                    <a href="#">
                                                                        Update
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="noresize"><input type="checkbox" /></td>
                                                                <td>Special Products</td>
                                                                <td class="icon center">
                                                                    <a href="#">
                                                                        Update
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>                    
                                                    </table>
                                                    
                                                    <div class="pagination_holder">
                                                        <ul class="pagination">
                                                            <li><a href="">Previous</a></li>
                                                            <li class="active"><a href="#">1</a></li>
                                                            <li class=""><a href="#">2</a></li>
                                                            <li class=""><a href="#">3</a></li>
                                                            <li class=""><a href="#">4</a></li>
                                                            <li class=""><a href="#">5</a></li>
                                                            <li class=""><a href="#">6</a></li>
                                                            <li><a href="#">Next</a></li>
                                                        </ul>
                                                    </div>

                                                </td>

                                                <td width="1%"></td>

                                                <td width="49.5%" valign="top">

                                                    <table class="controls">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <h4>Assigned Product Groups</h4>
                                                                </td>
                                                                <td>
                                                                    <div class="right">
                                                                        results per page
                                                                        <select class="select_init" name="" id="form_">
                                                                            <option value="10">10</option>
                                                                            <option value="20" selected="selected">20</option>
                                                                            <option value="30">30</option>
                                                                            <option value="50">50</option>
                                                                            <option value="100">100</option>
                                                                            <option value="200">200</option>
                                                                        </select>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>	

                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 separated">
                                                        <thead>
                                                            <tr class="blueTableHead">
                                                                <th scope="col"><input type="checkbox" /></th>
                                                                <th scope="col">Category Name</th>
                                                                <th scope="col" class="center" style="width: 40px;">View</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td class="noresize"><input type="checkbox" /></td>
                                                                <td>Special Products</td>
                                                                <td class="icon center">
                                                                    <a href="#">
                                                                        Update
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                           <tr>
                                                                <td class="noresize"><input type="checkbox" /></td>
                                                                <td>Special Products</td>
                                                                <td class="icon center">
                                                                    <a href="#">
                                                                        Update
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="noresize"><input type="checkbox" /></td>
                                                                <td>Special Products</td>
                                                                <td class="icon center">
                                                                    <a href="#">
                                                                        Update
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="noresize"><input type="checkbox" /></td>
                                                                <td>Special Products</td>
                                                                <td class="icon center">
                                                                    <a href="#">
                                                                        Update
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>                    
                                                    </table>
                                                    
                                                    <div class="pagination_holder">
                                                        <ul class="pagination">
                                                            <li><a href="">Previous</a></li>
                                                            <li class="active"><a href="#">1</a></li>
                                                            <li class=""><a href="#">2</a></li>
                                                            <li class=""><a href="#">3</a></li>
                                                            <li class=""><a href="#">4</a></li>
                                                            <li class=""><a href="#">5</a></li>
                                                            <li class=""><a href="#">6</a></li>
                                                            <li><a href="#">Next</a></li>
                                                        </ul>
                                                    </div>

                                                </td>
                                            </tr>

                                        </table>



                                    </div>
                                </div>
								
                               
                                    
		                    </div><!-- EOF Main Content Holder -->
		                    
                            
		                    <div class="clear"></div>
					    	
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <!-- EOF Content -->
           	