<?php 
// Configs
$invoice = array('' => '-') + \Config::get('details.invoice', array());
$status = array('' => '-') + \Config::get('details.status', array());
$delivery = array('' => '-') + \Config::get('details.delivery', array());
?>								
<div class="panelContent">

    <?php 
        isset($filters) or $filters = true;
        
        if($filters)
        {
            echo \Theme::instance()->view('views/_partials/search_filters', array(
                'pagination' => $pagination,
                'module' => 'company and full',
                'options' => array(
                    'discount_product_group', 
                    'discount_status', 
                    ),
            ), false);
        }
    ?>
    
    <?php 
        echo \Theme::instance()->view('views/_partials/action_header', array('bulk_actions' => array(
            '0' => 'Select Action',
            '1' => 'Export Discount Codes',
            '2' => 'Active',
            '3' => 'Inactive',
            '4' => 'Delete',
            '5' => 'Set Expiry Date',
        ))); 
    ?>

    <table class="grid greyTable2 separated" width="100%">
        <thead>
            <tr class="blueTableHead">
                <th scope="col" style="width: 40px;"></th>
                <th scope="col">Group Name</th>
                <th scope="col" class="noresize">Users</th>
                <th scope="col" class="center" style="width: 40px;">View</th>
                <th scope="col" class="center noresize">Assign</th>
            </tr>
        </thead>
        <tbody>

            <?php // var_dump($items); ?>
            
            <?php foreach($items as $item): ?>

                <?php $item_info = \Order\Model_Order::order_info($item->id); ?>
                <tr>
                    <td class="noresize">
                        <?php 
                            echo \Form::hidden('action[' . $item->id . ']', 0);
                            echo \Form::checkbox('action[' . $item->id . ']', 1, null, array('class' => 'check_active', 'data-id' => $item->id));
                        ?>
                    </td>
                    <td>
                        <?php 
                            $names = array('Visitors', 'Site Members', 'VIP', 'Special Members'); 
                            echo $names[(int)rand(0, 3)];
                        ?>
                    </td>
                    <td>
                        <?php 
                            $names = array('18', '5', '3', '14'); 
                            echo $names[(int)rand(0, 3)];
                        ?>
                    </td> 
                    <td class="icon center">
                        <a href="<?php echo \Uri::create('admin/discountcode/update/' . $item->id); ?>">
                            Update
                        </a>
                    </td>
                    <td>
                        <div class="custom_checkbox">
                            <?php echo \Form::checkbox('able_to_view[]', 1); ?>
                        </div> 
                    </td>
                </tr>

            <?php endforeach; ?>

            <?php if(empty($items)): ?>

                <tr class="nodrag nodrop">
                    <td colspan="10" class="center"><strong>There are no orders.</strong></td>
                </tr>

            <?php endif; ?>

        </tbody>
    </table>
    
    <div class="pagination_holder">
        <?php echo $pagination->render(); ?>
    </div>
    <div class="clear"></div>
    
    
    <table class="grid greyTable2 separated" width="100%">
        <tr class="adding_tr nodrag nodrop">
            <td>
                <table width="100%" class="greyTable2">
                    <tbody class="ui-sortable">
                        <tr class="nodrag nodrop">
                            <td>
                                <div class="left m_t_10">&nbsp;</div>
                                <input type="text" id="form_title" value="" name="title" class="right m_r_5" placeholder="Enter Group Name">					                                        	
                            </td>
                            <td class="noresize"> 
                                <button class="btn btn-primary right" type="submit"><i class="icon-plus icon-white"></i> Add New</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </table>
    
    
</div>