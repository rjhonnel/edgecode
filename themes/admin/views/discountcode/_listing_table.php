<div class="top-filter-holder">
    <?php
    isset($filters) or $filters = true;

    if($filters)
    {
        echo \Theme::instance()->view('views/_partials/search_filters', array(
            'pagination' => $pagination,
            'module' => 'discount code',
            'options' => array(
                'discount_status',
                'discount_type',
                'type'
            ),
        ), false);
    }
    ?>

    <div class="form-inline pull-right">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>

<table class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">ID</th>
        <th scope="col">Discount Code</th>
        <th scope="col">Discount Type</th>
        <th scope="col">Use</th>
        <th scope="col">Active Date</th>
        <th scope="col">Status</th>
        <?php if(!\SentryAdmin::user()->is_production()):?>
        <th scope="col" class="center" width="150">Actions</th>
        <?php endif;?>
    </tr>
    </thead>
    <tbody>
    <?php foreach($items as $item): ?>
        <tr>
            <td>
                <?php if(!\SentryAdmin::user()->is_production()):?>
                    <a href="<?php echo \Uri::create('admin/discountcode/update/' . $item->id); ?>">
                <?php endif;?>
                    <strong><?php echo $item->id; ?></strong>
                <?php if(!\SentryAdmin::user()->is_production()):?>
                    </a>
                <?php endif;?>
            </td>
            <td>
                <?php if(!\SentryAdmin::user()->is_production()):?>
                    <a href="<?php echo \Uri::create('admin/discountcode/update/' . $item->id); ?>">
                <?php endif;?>
                    <strong><?php echo $item->code; ?></strong>
                <?php if(!\SentryAdmin::user()->is_production()):?>
                    </a>
                <?php endif;?>
            </td>
            <td><?php echo ucfirst($item->type); ?></td>
            <td><?php echo ucfirst($item->use_type); ?></td>
            <td>
                <?php echo $item->active_from != '0000-00-00' ? ( date('\F\r\o\m\: d/m/Y', ($item->active_from != '0000-00-00' ? strtotime($item->active_from) : 0)) ) : ''; ?>
                <?php echo $item->active_to != '0000-00-00' ? ( date('\T\o\: d/m/Y', ($item->active_to != '0000-00-00' ? strtotime($item->active_to) : 0)) ) : ''; ?>
            </td>
            <td><?php echo ucfirst($item->status); ?></td>
            <?php if(!\SentryAdmin::user()->is_production()):?>
            <td width="110">
                <ul class="table-action-inline">
                    <li>
                        <a href="<?php echo \Uri::create('admin/discountcode/update/' . $item->id); ?>">Edit</a>
                    </li>
                    <li>
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete discount code?" href="<?php echo \Uri::create('admin/discountcode/delete/' . $item->id); ?>">Delete</a>
                    </li>
                </ul>
            </td>
            <?php endif;?>
        </tr>
    <?php endforeach; ?>

    <?php if(empty($items)): ?>

        <tr class="nodrag nodrop">
            <td colspan="10" class="center"><strong>There are no orders.</strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>