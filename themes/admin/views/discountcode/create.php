<div class="main-content">

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Promotionals');
		\Breadcrumb::set('Discount Code Manager', 'admin/discountcodes/list');
		\Breadcrumb::set('Add Discount Code');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Add Discount Code</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/discountcode/_action_links', array('create_form' => 1, 'hide_add_new'=>1)); ?>
				</div>
			</header>

			<?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>

			<div class="panel panel-default panel-col2">
				<div class="panel-heading">
					<div class="row">
						<div class="col-sm-6">
							<h3 class="panel-title pull-left">General Information</h3>
						</div>
						<div class="col-sm-6">
							<h3 class="panel-title pull-left">Discount Properties
								<span class="txt-discount-type capitalize"></span>
							</h3>
						</div>
					</div>
				</div>
				<div class="panel-body form-horizontal">

					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">Discount Code  <span class="text-danger">*</span></label>
								<div class="col-sm-4" <?php echo \Input::post('code_not_required')? 'style="display:none"':''; ?>><?php echo \Form::input('code', \Input::post('code'), array('placeholder' => 'ABC123','class' => 'form-control', 'data-original' => \Input::post('code'))); ?></div>
								<div class="col-sm-4">
									<label><input type="checkbox" value="1" name="code_not_required" <?php echo \Input::post('code_not_required')? 'checked':''; ?> > Discount to All</label>
								</div>
							</div>
							<div id="show_category_list" <?php echo \Input::post('code_not_required')? 'style="display:none"':''; ?> >
								<div class="form-group">
									<label class="col-sm-4 control-label">Categories <span class="text-danger">*</span></label>
									<div class="col-sm-8">
										<?php
											$category_product = DB::query("SELECT id, case title when parent_id!=0 then title else title end as title FROM `product_categories` WHERE 1")->execute();
					                        $arrayName = array();
					                        if (!empty($category_product))
					                        {
					                            foreach ($category_product as $row)
					                            {
					                                if(is_array($row))
					                                {
					                                    $arrayName[$row['id']] = $row['title'];
					                                }
					                                else
					                                {
					                                    $arrayName[$row->{'id'}] = $row->{'title'};
					                                }
					                            }
					                        }

					                        echo \Form::select('category_list', \Input::post('category_list'), $arrayName, array('class' => 'form-control', 'multiple' => 'multiple'));
										?>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Discount Type  <span class="text-danger">*</span></label>
								<div class="col-sm-8">
									<?php echo \Form::select('type', \Input::post('type'), \Config::get('details.type', array()), array('class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Status</label>
								<div class="col-sm-8">
									<?php echo \Form::select('status', \Input::post('status'), \Config::get('details.status', array()), array('class' => 'form-control')); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">Active Date</label>
								<div class="col-sm-8">

									<div class="row">
										<div class="col-sm-6" >
											<div class="datepicker-holder-control">
												<?php echo \Form::input('active_from', \Input::post('active_from'), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
											</div>
										</div>
										<div class="col-sm-6" >
											<div class="datepicker-holder-control">
												<?php echo \Form::input('active_to', \Input::post('active_to'), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="div-type-value form-group">
								<label class="col-sm-3 control-label">Amount  <span class="text-danger">*</span></label>
								<div class="col-sm-9"><?php echo \Form::input('type_value', \Input::post('type_value'), array('class' => 'form-control')); ?></div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Order Value  </label>
								<div class="col-sm-9">

									<div class="row">
										<div class="col-sm-5" style="padding-right: 5px">
											<?php echo \Form::input('min_order_value', \Input::post('min_order_value'), array('class' => 'form-control', 'placeholder' => 'Min')); ?>
										</div>
										<div class="col-sm-2" style="padding:0;text-align:center"><strong>to</strong></div>
										<div class="col-sm-5" style="padding-left: 5px">
											<?php echo \Form::input('max_order_value', \Input::post('max_order_value'), array('class' => 'form-control', 'placeholder' => 'Max')); ?>
										</div>
									</div>

								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-3 control-label">Type  <span class="text-danger">*</span></label>
								<div class="col-sm-9">
									<?php echo \Form::select('use_type', \Input::post('use_type'), \Config::get('details.use_type', array()), array('class' => 'form-control')); ?>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>



			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
				<?php echo \Form::button('exit', '<i class="fa fa-edit"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>
			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
<?php
	echo \Theme::instance()->asset->css('plugins/select2-4.0.3.min.css');
	echo \Theme::instance()->asset->js('plugins/select2-4.0.3.min.js');
?>
<script type="text/javascript">
	function toggle_discount_type(discount_type)
	{
		if (discount_type == 'free shipping')
		{
			$('#form_type_value').val('');
			$('.div-type-value').hide();
			$('.txt-discount-type').html('');
		}
		else
		{
			$('.div-type-value').show();
			$('.txt-discount-type').html('('+discount_type+')');
		}
	}
	function toggle_code_not_required()
	{
		if($('input[name="code_not_required"]').is(':checked'))
		{
			$('#show_category_list').show();
			$('#form_code').parent('div').hide();
			$('#form_code').val('');
		}
		else
		{
			$('#show_category_list').hide();
			$('#form_code').parent('div').show();
			$('#form_code').val($('#form_code').data('original'));
		}
	}

	$(document).ready(function(){
		toggle_discount_type($('#form_type').val());
		$('#form_type').change(function(){
			toggle_discount_type($(this).val());
		});

		toggle_code_not_required();
		$('input[name="code_not_required"]').change(function(){
			toggle_code_not_required();
		});

		$('#form_category_list').select2();
	});
</script>
                            