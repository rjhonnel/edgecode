<script>
	$(document).ready(function(){
		$("input[name='username']").focus();
	});
</script>

<style>
	.main-footer,
	.navbar {
		display: none!important
	}

	.login-logo img {
		display: block;
		margin: 0 auto 8px;
	}

	.navbar-fixed-top + .alert {
		margin: auto;
		text-align: center;
	}

	.navbar-fixed-top + .alert  p {
		text-align: center;
	}

	.login-logo {
		text-align: center;
		margin: 48px 0 20px;
		color: #008bd6;
		font-weight: 600;
	}

	body {
		background: #f5f5f5;
	}
	.form-group {
		margin-bottom: 14px;
	}
	.form-control {
		height: 36px;
	}
	label {
		font-weight: 400;
	}
	.login-holder {
		width: 350px;
		margin: auto;
		padding: 30px 30px 52px;
		box-shadow: rgba(153, 153, 153, 0.3) 0 1px 1px;
		background: #fff;
		border-radius: 1px;
	}
</style>

<div class="login-logo">
	<?php echo \Theme::instance()->asset->img('logo.png', array('width' => 46, 'height' => 30, 'alt' => 'Leading Edge Creative')); ?> EDGE COMMERCE
</div>

<div class="login-holder">
	<?php echo \Form::open(array('action' => Uri::create('admin/login'), 'name' => 'login_form')); ?>
	<div class="form-group">
		<?php echo \Form::label('Username', 'username'); ?>
		<?php echo \Form::input('username', \Input::post('username'), array('placeholder' => 'Type username', 'class' => 'form-control')); ?>
	</div>


	<div class="form-group">
		<?php echo \Form::label('Password', 'password'); ?>
		<?php echo \Form::password('password', \Input::post('password'), array('placeholder' => 'Type password', 'class' => 'form-control')); ?>
	</div>

	<label>
		<?php echo \Form::checkbox('remember', 1, \Input::post('remember')); ?>
		Remember me.
	</label>

	<?php echo \Form::button('submit', 'Log In', array('class' => 'btn btn-primary pull-right', 'type' => 'submit')); ?>

	<?php echo \Form::close(); ?>
</div>