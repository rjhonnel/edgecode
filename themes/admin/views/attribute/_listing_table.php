

<?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>
    <?php echo \Form::button('remove', '<i class="icon-remove icon-white"></i> Remove Selected', array('type' => 'submit', 'class' => 'btn btn-danger left xx', 'value' => 'remove', 'style' => 'display:none', 'id'=>'bulk_delete')); ?>
    <div class="header_messages"></div>
    <table rel="<?php echo \Uri::create('admin/attribute/sort/attribute'); ?>" class="table table-striped table-bordered sortable_rows" width="100%">
        <thead>
            <tr class="blueTableHead">
                <th scope="col" style="width: 40px;"></th>
                <th scope="col">Attribute Name</th>
                <th scope="col">Display Name</th>
                <th scope="col" class="center" width="150">actions</th>
            </tr>
        </thead>

            <?php foreach($items as $item): ?>
                <?php $item = (Object)$item; ?>

                <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
                    <td class="noresize">
                        <?php
                        echo \Form::hidden('action[' . $item->id . ']', 0);
                        echo \Form::checkbox('attribute[remove][]', $item->id, null, array('class' => 'check_active', 'data-value' => 'attribute', 'data-id' => $item->id));
                        ?>
                    </td>
                    <td>
                        <a href="<?php echo \Uri::create('admin/attribute/update/' . $item->id); ?>">
                            <strong><?php echo $item->title; ?></strong>
                        </a>
                    </td>
                    <td><?php echo $item->name; ?></td>
                    <td width="110">
                        <ul class="table-action-inline">
                            <li>
                                <a href="<?php echo \Uri::create('admin/attribute/update/' . $item->id); ?>">Edit</a>
                            </li>
                            <li>
                                <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete attribute?" href="<?php echo \Uri::create('admin/attribute/delete/' . $item->id); ?>">Delete</a>
                            </li>
                        </ul>
                    </td>
                </tr>

            <?php endforeach; ?>

        <?php if(empty($items)): ?>

            <tr class="nodrag nodrop">
                <td colspan="5" class="center"><strong>There are no items.</strong></td>
            </tr>

        <?php endif; ?>
    </table>
<?php echo \Form::close(); ?>


<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>

<?php echo \Form::open(array('action' => \Uri::create('admin/attribute/create'))); ?>

    <div class="panel panel-default panel-body mt-20px">
        <div class="row">
            <div class="col-sm-5">
                <?php echo \Form::input('title', \Input::post('title'), array('placeholder' => 'Enter Attribute Title', 'class' => 'form-control')); ?>
            </div>
            <div class="col-sm-5">
                <?php echo \Form::input('name', \Input::post('name'), array('placeholder' => 'Enter Display Name', 'class' => 'form-control')); ?>
            </div>
            <div class="col-sm-2">
                <button type="submit" class="btn btn-primary btn-block">Add New</button>
            </div>
        </div>

    </div>
<?php echo \Form::close(); ?>