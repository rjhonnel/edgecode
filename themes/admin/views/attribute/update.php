<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Attributes Manager', 'admin/attribute/list');
		\Breadcrumb::set($attribute->title);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Product Attribute Edit</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/attribute/_action_links', array('show_save' => 1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/attribute/_navbar_links'); ?>


			<?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">General Information</h3>

				</div>
				<div class="panel-body form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">Attribute Name  <span class="text-danger">*</span></label>
						<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title', $attribute->title), array('class'=>'form-control')); ?></div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label">Display Name</label>
						<div class="col-sm-10"><?php echo \Form::input('name', \Input::post('name', $attribute->name), array('class'=>'form-control')); ?></div>
					</div>

					<div class="form-group">
						<label class="control-label" style="width: 100%;">
							<div class="col-sm-2">
								<?php echo \Form::hidden('enable_image', 0); ?>
								<?php echo \Form::checkbox('enable_image', 1, \Input::post('enable_image', $attribute->enable_image), array('style' => 'margin-top: 0;')); ?>
							</div>
							<div class="col-sm-10 text-left">
								Enable Image upload for attribute options
								<?php \Form::hidden('type', 'select'); ?>
							</div>
						</label>
					</div>

				</div>
			</div>


			<div class="save_button_holder text-right hide">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1', 'id'=> 'save_button_down')); ?>
				<?php echo \Form::button('exit', '<i class="fa fa-check"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>
			<?php echo \Form::close(); ?>

			<!-- Options Panel -->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Attribute Options</h3>
				</div>
				<?php
				// Load page listing table
				echo \Theme::instance()->view('views/attribute/option/_listing_table',
					array(
						'attribute'	=> $attribute,
						'attribute_options'	=> $attribute_options,
						'attribute_options_pagination'	=> $attribute_options_pagination,
					),
					false
				);
				?>
			</div><!-- EOF Accordions Panel -->


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<?php echo \Theme::instance()->asset->js('attribute/options.js'); ?>
			    

