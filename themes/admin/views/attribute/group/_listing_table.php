<div class="header_messages"></div>
<table rel="<?php echo \Uri::create('admin/attribute/group/sort/group'); ?>" class="table table-striped table-bordered sortable_rows">
	<thead>
	<tr class="blueTableHead">
		<th scope="col" class="noresize">Attribute Group Name</th>
		<th scope="col">Attribute Combinations</th>
		<td colspan="2">Actions</td>
	</tr>
	</thead>
	<tbody>

	<?php foreach($items as $item): ?>
		<?php $item = (Object)$item; ?>

		<tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
			<td>
				<a href="<?php echo \Uri::create('admin/attribute/group/update/' . $item->id); ?>">
					<strong><?php echo $item->title; ?></strong>
				</a>

			</td>
			<td><?php echo $item->attributes_string; ?></td>
			<td colspan="2" width="10">
				<ul class="table-action-inline">
					<li>
						<a href="<?php echo \Uri::create('admin/attribute/group/update/' . $item->id); ?>">Edit</a>
					</li>
					<li>
						<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete item?" href="<?php echo \Uri::create('admin/attribute/group/delete/' . $item->id); ?>">Delete</a>
					</li>
				</ul>
			</td>
		</tr>

	<?php endforeach; ?>

	<?php if(empty($items)): ?>

		<tr class="nodrag nodrop">
			<td colspan="4" class="center"><strong>There are no items.</strong></td>
		</tr>

	<?php endif; ?>
	</tbody>
</table>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>

<?php echo \Form::open(array('action' => \Uri::create('admin/attribute/group/create'))); ?>
<div class="panel panel-default panel-body mt-20px">
	<div class="row">
		<div class="col-sm-10">
			<?php echo \Form::input('title', \Input::post('title'), array('placeholder' => 'Enter Attribute Group Title', 'class' => 'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<button type="submit" class="btn btn-primary btn-block"><i class="icon-plus icon-white"></i> Add New</button>
		</div>
	</div>
</div>
<?php echo \Form::close(); ?>
