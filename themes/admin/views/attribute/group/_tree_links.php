						<?php 
							// Get categories
							$groups = \Attribute\Model_Attribute_Group::find(function($query){ 
								$query->order_by('sort', 'asc');
								$query->order_by('id', 'asc');
							});
						?>
						
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title pull-left">Attribute Groups</h4>
                                <div id="sidetreecontrol" class="pull-right sidetreecontrol"><a href="#"><small>Collapse All</small></a><a href="#">Expand All</a></div>
                            </div>
                            <div class="panel-body">
                                <div id="sidetree">
                                
                                	<?php if(empty($groups)): ?>
                                		<div class="wide"><span class="req">Note: </span> There are no attribute groups yet.</div>
                                	<?php else: ?>
                                	
	                                    <ul class="treeview" id="tree">
	                                    	
	                                    	<?php
	                                    	
	                                    		$list_attributes = function($group)
	                                    		{
	                                    			?><ul><?php
	                                    			foreach($group->attributes as $attribute)
	                                    			{
	                                    				?>
	                                    					<li>
	                                    						<div class="checkbox_link_holder">
		                                    						<a href="<?php echo \Uri::create('admin/attribute/update/' . $attribute->id); ?>">
		                                    							<?php echo $attribute->title; ?>
		                                    						</a>
		                                    					</div>
		                                    				</li><?php
	                                    					
	                                    			}
	                                    			?></ul><?php
	                                    		};
	                                    	
	                                    		foreach($groups as $key => $group)
	                                    		{
	                                    			?>
	                                    				<li>
	                                    					<?php echo !empty($group->attributes) ? '<div class="hitarea"></div>' : ''; ?>
	                                    					<div class="checkbox_link_holder">
	                                    						<a href="<?php echo \Uri::create('admin/attribute/group/update/' . $group->id); ?>">
	                                    							<?php echo $group->title; ?><?php echo !empty($group->attributes) ? ' <span class="tree_count">('.count($group->attributes).')</span>' : ''; ?>
	                                    						</a>
	                                    					</div><?php
                                                            
                                                    if(!empty($group->attributes))
                                                    {
                                                        $list_attributes($group);
                                                    }
                                                    else
                                                    {
                                                        ?></li><?php
                                                    }
	                                    		}
	                                    	?>
	                                        
	                                    </ul>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
