<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Attribute Group Manager', 'admin/attribute/list');
		\Breadcrumb::set($group->title);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Attribute Group</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/attribute/group/_action_links', array('show_save' => 1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/attribute/group/_navbar_links'); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">General Information</h3>
				</div>
				<div class="panel-body form-horizontal">
					<?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>
					<div class="form-group">
						<label class="col-sm-3 control-label">Attributes Group Name  <span class="text-danger">*</span></label>
						<div class="col-sm-9"><?php echo \Form::input('title', \Input::post('title', $group->title), array('class'=>'form-control')); ?></div>
					</div>
					<div class="save_button_holder text-right hide">
						<?php echo \Form::button('update', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1', 'id'=> 'save_button_down')); ?>
					</div>
					<?php echo \Form::close(); ?>
				</div>
			</div>

			<div class="span5 sort_message_container"></div>

			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default panel-col2">
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h4 class="panel-title pull-left mt-5px">All Attributes</h4>
									<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
										<div class="form-inline pull-right">
										    <div class="form-inline show-table-filter">
										        <label>Show entries:</label>
										        <?php echo \Form::select('per_page', \Input::get('per_page', \Input::get('per_page', 10)), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
										    </div>
										</div>
									<?php echo \Form::close(); ?>
								</div>
								<div class="col-sm-6">
									<h4 class="panel-title pull-left mt-5px">Attributes Assigned to Group</h4>
								</div>
							</div>
						</div>
					</div>
					<?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>
						<div class="row">
							<div class="col-sm-6">
								<div class="clearfix">
			                        <ul class="default_list">
			                            <li><a href="" onclick="return false;" class="select_all">Select All</a></li>
			                            <li class="separator">/</li>
			                            <li><a href="" onclick="return false;" class="unselect_all">Unselect All</a></li>
			                            <li class="separator">/</li>
			                            <li><a href="" onclick="return false;" class="select_qty"><span>0</span> items selected</a></li>
			                        </ul>
									<button name="add" value="add" type="submit" class="btn btn-sm btn-primary pull-right">
										<i class="fa fa-plus"></i> Add selected
									</button>
								</div>
								<table class="table table-striped table-bordered mt-5px">
									<thead>
									<tr class="blueTableHead">
										<th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'attributes[add][]'); ?></th>
										<th scope="col">Title</th>
										<th scope="col">Dispaly Name</th>
									</tr>
									</thead>
									<tbody class="drag_table remove">
									<?php if($not_related_attributes): ?>
										<?php foreach($not_related_attributes as $attribute): ?>

											<tr>
												<td class="noresize"><?php echo \Form::checkbox('attributes[add][]', $attribute->id); ?></td>
												<td>
													<a href="<?php echo \Uri::create('admin/attribute/update/' . $attribute->id); ?>">
														<strong><?php echo $attribute->title; ?></strong>
													</a>
												</td>
												<td><?php echo $attribute->name; ?></td>
											</tr>

										<?php endforeach; ?>
										<?php $no_items = 'style="display: none;"'; ?>
									<?php else: ?>
										<?php $no_items = ''; ?>
									<?php endif; ?>

									<tr class="no_items" <?php echo $no_items; ?>>
										<td class="noresize center" colspan="3">There are no more attributes to add</td>
									</tr>

									</tbody>
								</table>
                    			<div class="pagination-holder">
									<?php echo $pagination_not_related_attributes->render(); ?>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="clearfix">
			                        <ul class="default_list">
			                            <li><a href="" onclick="return false;" class="select_all">Select All</a></li>
			                            <li class="separator">/</li>
			                            <li><a href="" onclick="return false;" class="unselect_all">Unselect All</a></li>
			                            <li class="separator">/</li>
			                            <li><a href="" onclick="return false;" class="select_qty"><span>0</span> items selected</a></li>
			                        </ul>
									<button name="remove" value="remove" type="submit" class="btn btn-sm btn-primary pull-right">
										<i class="fa fa-minus"></i> Remove selected
									</button>
								</div>
								<table class="table table-striped table-bordered mt-5px">
									<thead>
									<tr class="blueTableHead">
										<th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'attributes[remove][]'); ?></th>
										<th scope="col">Title</th>
										<th scope="col">Dispaly Name</th>
									</tr>
									</thead>
									<tbody class="drag_table add">
									<?php if(!empty($group->attributes)): ?>
										<?php foreach($group->attributes as $attribute): ?>

											<tr>
												<td class="noresize"><?php echo \Form::checkbox('attributes[remove][]', $attribute->id); ?></td>
												<td>
													<a href="<?php echo \Uri::create('admin/attribute/update/' . $attribute->id); ?>">
														<strong><?php echo $attribute->title; ?></strong>
													</a>
												</td>
												<td><?php echo $attribute->name; ?></td>
											</tr>

										<?php endforeach; ?>
										<?php $no_items = 'style="display: none;"'; ?>
									<?php else: ?>
										<?php $no_items = ''; ?>
									<?php endif; ?>

									<tr class="no_items" <?php echo $no_items; ?>>
										<td class="noresize center" colspan="3">There are no added attributess</td>
									</tr>

									</tbody>
								</table>
							</div>
						</div>
					<?php echo \Form::close(); ?>
				</div>
			</div>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>