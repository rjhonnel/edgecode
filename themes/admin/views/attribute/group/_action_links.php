<?php if(isset($show_save)): ?>
	<a id="save_button_up" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Save</a>
<?php endif; ?>
<?php if(!isset($hide_show_all)): ?>
	<a href="<?php echo \Uri::create('admin/attribute/group/list'); ?>" class="btn btn-sm btn-default"><i class="fa fa-list"></i> Show All</a>
<?php endif; ?>