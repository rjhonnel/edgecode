<div class="panel-body">
	<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>

		<div class="top-filter-holder">

		    <div class="form-inline show-table-filter">
		        <label>Show entries:</label>
		        <?php echo \Form::select('per_page', \Input::get('per_page', \Input::get('per_page', 10)), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
		    </div>
		</div>
	<?php echo \Form::close(); ?>
	<div class="header_messages"></div>
	<table rel="<?php echo \Uri::create('admin/attribute/option/sort/option'); ?>" class="table table-striped table-bordered sortable_rows" >
		<thead>
		<tr class="blueTableHead">
			<th scope="col">Option Name</th>
		</tr>
		</thead>
		<tbody>

		<?php foreach($attribute_options as $item): ?>
			<?php $item = (Object)$item; ?>

			<tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
				<td class="<?php echo $item->id; ?>">
					<strong class="fs-16px"><?php echo $item->title; ?></strong>
					<ul class="table-action-inline">
						<li>
							<a href="" onclick="return false;" class="live_update">Edit</a>
							<a href="" onclick="return false;" class="live_save" style="display:none">Save</a>
						</li>
						<li>
							<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete item?" href="<?php echo \Uri::create('admin/attribute/option/delete/' . $item->id); ?>">Delete</a>
						</li>
					</ul>
				</td>
			</tr>

		<?php endforeach; ?>

		<?php if(empty($attribute_options)): ?>

			<tr class="nodrag nodrop">
				<td colspan="4" class="center"><strong>There are no items.</strong></td>
			</tr>

		<?php endif; ?>

		</tbody>
	</table>
	<div class="pagination-holder">
		<span class="alert alert-info pull-left"> <span class="text-danger">*</span> Order the options by using the drag and drop function.</span>
	    <?php echo $attribute_options_pagination->render(); ?>
	</div>
</div>

<?php echo \Form::open(array('action' => \Uri::create('admin/attribute/option/create'))); ?>
<div class="panel-footer">
	<?php
	if(isset($attribute))
	{
		echo \Form::hidden('attribute_id', $attribute->id);
	}
	?>

	<div class="row">
		<div class="col-sm-10" style="padding-right:0">
			<?php echo \Form::input('option_title', \Input::post('option_title'), array('placeholder' => 'Enter Option Title', 'class' => 'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<button type="submit" class="btn btn-primary btn-block btn-sm"><i class="fa fa-plus"></i> Add New</button>
		</div>
	</div>
</div>
<?php echo \Form::close(); ?>