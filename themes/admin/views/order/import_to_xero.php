<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Orders');
        \Breadcrumb::set('Manage Orders', 'admin/order/list');
        \Breadcrumb::set('Import to Xero');

        // If viewing category products show category title
        if(isset($user))
            \Breadcrumb::set($user->get('metadata.first_name') . ' ' . $user->get('metadata.last_name'));

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Import to Xero</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/order/_action_links', array()); ?>
                </div>
            </header>

            <?php
            if(isset($user))
                echo \Theme::instance()->view('views/user/_navbar_links', array('user' => $user));
            ?>

            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title pull-left">Xero API Settings</h3></div>
                <div class="panel-body form-horizontal">
                    <?php echo \Form::open(array('enctype' => 'multipart/form-data', 'action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'row-fluid', 'id' => 'SetForm')); ?>
                    <input type="hidden" name="name" value="Xero" />
                    <input type="hidden" name="code" value="xero" />
                    <?php
                        $auth_token = (isset($settings['auth_token']) ? $settings['auth_token'] : '');
                        $auth_secret = (isset($settings['auth_secret']) ? $settings['auth_secret'] : '');
                        $pem = (isset($settings['pem']) ? $settings['pem'] : '');
                        $cer = (isset($settings['cer']) ? $settings['cer'] : '');
                    ?>
                    <input type="hidden" name="pem_hidden" value="<?php echo $pem; ?>" />
                    <input type="hidden" name="cer_hidden" value="<?php echo $cer; ?>" />

                    <div class="form-group clearfix">
                        <label class="col-md-3 control-label">Authentication Token:  <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input class="form-control" name="auth_token" value="<?php echo \Input::post('auth_token', $auth_token) ?>" type="text" id="form_auth_token">
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <label class="col-md-3 control-label">Authentication Secret:  <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input class="form-control" name="auth_secret" value="<?php echo \Input::post('auth_secret', $auth_secret) ?>" type="text" id="form_auth_secret">
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-md-3 control-label">Upload .pem File:  <span class="text-danger"></span></label>
                        <div class="col-md-4">
                            <?php echo \Form::file('pem'); ?>
                            <small><b><?php echo (!$pem == '')?'Current file: '.$pem:''; ?></b></small>
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label class="col-md-3 control-label">Upload .cer File:  <span class="text-danger"></span></label>
                        <div class="col-md-4">
                            <?php echo \Form::file('cer'); ?>
                            <small><b><?php echo (!$cer == '')?'Current file: '.$cer:''; ?></b></small>
                        </div>
                    </div>

                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success" name="submit" type="submit" value="save"><i class="fa fa-edit"></i> Save</button>
                                <button class="btn btn-primary" name="submit" type="submit" value="auth"><i class="fa fa fa-cloud-upload"></i> Import Orders to Xero </button>
                            </div>
                        </div>
                    </div>

                    <?php echo \Form::close(); ?>
                </div>
            </div>
            <p style="font-size: 12px;">Note: This Xero connection is for Private Application. It will require you to create a public and private key pair, <a href="https://developer.xero.com/documentation/api-guides/create-publicprivate-key" target="_blank">click here</a> for instructions.</p>
        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>