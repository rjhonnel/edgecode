<style type="text/css">
	.content .panel{
		width: 99%;
	}
	input[type=email]{
		width: 96%;
	}

	#step_guide li.active a {
		color: #0088cc;
	}

	#loading_area_item img, #loading_area_discount img, #loading_area_customer img  {
		width: 20px;
	}

	#loading_area_discount {
		float: right;
		display: inline-block;
	}

	#loading_area_customer {
		display: inline-block;
		margin-left: 10px;
	}
</style>

<?php
	$app_countries = \App\Countries::forge();
	$countries = $app_countries->getCountries();
	$app_states = \App\States::forge();
	$states = $app_states->getStateProvinceArray('AU');
	$payment_settings = \Config::load('payment_settings.db');
	$hold_enable_partial_payment = (isset($payment_settings['enable_partial_payment']) ? $payment_settings['enable_partial_payment'] : 0);
	$total_category_discount = 0;
    $shipping_price = $order->shipping_price;
?>

<div class="main-content">

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Orders');
		\Breadcrumb::set('Manage Orders', 'admin/order/list');
        \Breadcrumb::set('ID: ' . $order->id);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner">
			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid', 'id' => 'add_order_form')); ?>
				<header class="main-content-heading">
					<h4 class="pull-left">Edit Order: <strong class="text-primary">#<?php echo $order->id; ?></strong></h4>

					<div class="pull-right">
						<ul class="list-inline">
							<li style="vertical-align: top;">
								<div class="row" style="width:300px">
									<label class="col-sm-4 control-label text-right" style="padding-right:0;">Status</label>
									<div class="col-sm-8">
										<?php echo \Form::select('status', $order->status, \Config::get('details.status', array()), array('class' => 'form-control')); ?>
									</div>
								</div>
							</li>

							<li style="vertical-align: top;">
								<?php echo \Theme::instance()->view('views/order/_action_links', array('hide_add_new' => 1,'view_order' => 1, 'hide_show_all'=> 0, 'order_id' => $order->id)); ?>
							</li>
						</ul>




					</div>
				</header>

				<?php echo \Theme::instance()->view('views/product/_navbar_links'); ?>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title pull-left">Details</h3>

					</div>
					<div class="panel-body">
						
						<input name="order_id" type="hidden" value="<?php echo $order->id; ?>">
						<input type="hidden" name="partial_payment" value="<?php echo $hold_enable_partial_payment; ?>">

						<!-- Main Content Holder -->
						<div class="content">

							<ul id="step_guide" class="nav nav-tabs" role="tablist" id="tabbed-step" >
								<li id="step_one_tab" class="<?php echo (\Input::get('step') == 3 || \Input::get('step') == 2) ? 'disabled is_disabled' : 'active is_disabled';?>" role="presentation">
									<a href="#step_one" role="tab" data-toggle="tab"><span>Step 1: Order Details</span></a>
								</li>
								<li id="step_two_tab" class="<?php echo (\Input::get('step') == 2) ? 'active is_disabled' : 'disabled is_disabled';?>"  role="presentation">
									<a href="#step_two" role="tab" data-toggle="tab"><span>Step 2: Products</span></a>
								</li>
								<li id="step_three_tab" class="<?php echo (\Input::get('step') == 3) ? 'active is_disabled' : 'disabled is_disabled';?>"  role="presentation" >
									<a href="#step_three" role="tab" data-toggle="tab"><span>Step 3: Payment</span></a>
								</li>
								<li class="pull-right">
									<button id="back_button_top" class="btn btn-default" value="1" >Back</button>
									<button id="continue_button_top" class="btn btn-primary" value="1" >Continue</button>
									<?php echo \Form::button('save_order_only_button', '<i class="fa fa-edit"></i> Submit Order', array('id'=> '','class' => 'save_order_only_button btn btn-success', 'type' => 'submit', 'value' => '1')); ?>
								</li>
							</ul>
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane <?php echo (\Input::get('step') == 3 || \Input::get('step') == 2) ? '' : 'active';?>" id="step_one">

									<br>
									<div class="row">
										<div class="form-horizontal">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-sm-3 control-label">&nbsp;</label>
													<div class="col-sm-9">
														<h3 class="title-legend">CUSTOMER DETAILS</h3>
													</div>
												</div>
											</div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">&nbsp;</label>
                                                    <div class="col-sm-9">
                                                        <h3 class="title-legend">Order Details</h3>
                                                    </div>
                                                </div>
                                            </div>
										</div>
									</div>


									<div class="row">
										<div class="form-horizontal">
											<div class="col-sm-6">
												<div class="form-group">
													<label class="col-sm-3 control-label">Customer Name: <span class="text-danger">*</span></label>
													<div class="col-sm-9">
														<div class="form-search">
															<input class="form-control " value="<?php echo $order->first_name.' '.$order->last_name; ?>" disabled>

															<input type="hidden" name="customer_id" value="<?php echo $user->id; ?>">
														</div>
													</div>
												</div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label">Email Address:</label>
                                                    <div class="col-sm-9">
                                                        <input class="form-control customer_details" id="customer_details_email" value="<?php echo $user->email; ?>" disabled>
                                                    </div>
                                                </div>
											</div>
											<div class="col-sm-6">
                                                <div class="form-group ">
                                                    <label class="col-sm-3 control-label">Cater For</label>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" name="cater_for" value="<?php echo $order->cater_for; ?>" type="text" id="form_cater_for">
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <label class="col-sm-3 control-label">Client PO</label>
                                                    <div class="col-sm-9">
                                                        <input class="form-control" name="client_po" value="<?php echo $order->client_po; ?>" type="text" id="form_client_po">
                                                    </div>
                                                </div>
											</div>
										</div>
									</div>

									<hr>

									<div class="row">
										<div class="col-sm-6">
											<div class="form-horizontal">

												<div class="form-group">
													<label class="col-sm-3 control-label">&nbsp;</label>
													<div class="col-sm-9">
														<h3 class="title-legend">Billing Details</h3>
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">Title:</label>
													<div class="col-sm-9">
														<div class="row">
															<div class="col-sm-6">
																<input class="form-control customer_details" id="customer_details_title" disabled value="<?php echo $order->title; ?>"  placeholder="Title">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Name:</label>
													<div class="col-sm-9">
														<div class="row">
															<div class="col-sm-6">
																<input class="form-control customer_details" id="customer_details_first_name" disabled value="<?php echo $order->first_name; ?>"  placeholder="First Name">
															</div>
															<div class="col-sm-6">
																<input class="form-control customer_details" id="customer_details_last_name" disabled value="<?php echo $order->last_name; ?>" placeholder="Last Name"  placeholder="Last Name">
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Company:</label>
													<div class="col-sm-9">
														<input class="form-control customer_details" id="customer_details_company" disabled value="<?php echo $order->company; ?>"  placeholder="Company">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Address:</label>
													<div class="col-sm-9">
														<input class="form-control customer_details" id="customer_details_address" disabled value="<?php echo $order->address; ?>"  placeholder="Address 1">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Address 2:</label>
													<div class="col-sm-9">
														<input class="form-control customer_details" id="customer_details_address2" disabled value="<?php echo $order->address2; ?>"  placeholder="Address 2">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">&nbsp;</label>
													<div class="col-sm-9">
														<input class="form-control customer_details" id="customer_details_suburb" disabled value="<?php echo $order->suburb; ?>"  placeholder="Suburb">
													</div>
												</div>

												<div class="form-group">
													<label class="col-sm-3 control-label">&nbsp;</label>
													<div class="col-sm-9">
														<div class="row">
															<div class="col-sm-6">
																<input class="form-control customer_details" id="customer_details_state" disabled value="<?php echo $order->state; ?>"  placeholder="Last Name">
															</div>
															<div class="col-sm-6">
																<input class="form-control customer_details" id="customer_details_postcode" disabled value="<?php echo $order->postcode; ?>"  placeholder="Last Name">
															</div>
														</div>

													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Country:</label>
													<div class="col-sm-9">
														<input class="form-control customer_details" id="customer_details_country" disabled value="<?php echo $order->country; ?>"  placeholder="Last Name">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Telephone:</label>
													<div class="col-sm-9">
														<input class="form-control customer_details" id="customer_details_phone" disabled value="<?php echo $order->phone; ?>"  placeholder="Last Name">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">Order Notes:</label>
													<div class="col-sm-9">
														<textarea class="form-control" id="customer_details_notes" name="notes"><?php echo $order->notes; ?></textarea>
													</div>
												</div>

											</div>
										</div>
										<div class="col-sm-6 form-horizontal">
											
											<div class="form-group">
												<label class="col-sm-3 control-label">&nbsp;</label>
												<div class="col-sm-9">
													<h3 class="title-legend pull-left">Shipping Details</h3>
													<label class="control-label pull-right" style="margin-top:5px;"><input type="checkbox" id="same_as_billing_edit" checked> Same as Billing</label>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Title:</label>
												<div class="col-sm-9">
	                                    			<?php echo \Form::select('shipping_title', \Input::post('shipping_title', $order->shipping_title), \Config::get('user.titles', array()), array('class' => 'form-control', 'id' => 'customer_details_shipping_title', 'data-original' => $order->shipping_title, 'data-customer' => $order->title )); ?>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Name:</label>
												<div class="col-sm-9">
													<div class="row">
														<div class="col-sm-6">
															<input class="form-control customer_details" id="customer_details_shipping_first_name" name="shipping_first_name" value="<?php echo $order->shipping_first_name; ?>" data-original="<?php echo $order->shipping_first_name; ?>" data-customer="<?php echo $order->first_name; ?>" placeholder="First name">
														</div>
														<div class="col-sm-6">
															<input class="form-control customer_details" id="customer_details_shipping_last_name" name="shipping_last_name" value="<?php echo $order->shipping_last_name; ?>" data-original="<?php echo $order->shipping_last_name; ?>" data-customer="<?php echo $order->last_name; ?>" placeholder="Last name">
														</div>
													</div>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Company:</label>
												<div class="col-sm-9">
													<input class="form-control customer_details" id="customer_details_shipping_company" name="shipping_company" value="<?php echo $order->shipping_company; ?>" data-original="<?php echo $order->shipping_company; ?>" data-customer="<?php echo $order->company; ?>"  placeholder="Company">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Address:</label>
												<div class="col-sm-9">
													<input class="form-control customer_details" id="customer_details_shipping_address" name="shipping_address" value="<?php echo $order->shipping_address; ?>" data-original="<?php echo $order->shipping_address; ?>" data-customer="<?php echo $order->address; ?>" placeholder="Address">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Address 2:</label>
												<div class="col-sm-9">
													<input class="form-control customer_details" id="customer_details_shipping_address2" name="shipping_address2" value="<?php echo $order->shipping_address2; ?>" data-original="<?php echo $order->shipping_address2; ?>" data-customer="<?php echo $order->shipping_address2; ?>" placeholder="Address 2">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Suburb:</label>
												<div class="col-sm-9">
													<input class="form-control customer_details" id="customer_details_shipping_suburb" name="shipping_suburb" value="<?php echo $order->shipping_suburb; ?>" data-original="<?php echo $order->shipping_suburb; ?>" data-customer="<?php echo $order->suburb; ?>" placeholder="Suburb">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">State:</label>
												<div class="col-sm-9">
													<div class="row">
														<div class="col-sm-6">
															<?php echo \Form::select('shipping_state', \Input::post('shipping_state', $order->shipping_state), is_array($states) ? array('' => '-') + $states : array('' => 'Select State'), array('class' => 'form-control customer_details', 'data-original' => $order->shipping_state, 'data-customer' => $order->state)); ?>
														</div>
														<div class="col-sm-6">
															<input class="form-control customer_details" id="customer_details_shipping_postcode" name="shipping_postcode" value="<?php echo $order->shipping_postcode; ?>" data-original="<?php echo $order->shipping_postcode; ?>" data-customer="<?php echo $order->postcode; ?>" placeholder="Postcode">
														</div>
													</div>
												</div>
											</div>

											<div class="form-group">
												<label class="col-sm-3 control-label">Country:</label>
												<div class="col-sm-9">
													<?php echo \Form::select('shipping_country', \Input::post('shipping_country', $order->shipping_country), array('' => '-') + $countries, array('class' => 'form-control customer_details', 'data-state' => 'shipping_state', 'data-original' => $order->shipping_country, 'data-customer' => $order->country)); ?>
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Telephone:</label>
												<div class="col-sm-9">
													<input class="form-control customer_details" id="customer_details_shipping_phone" name="shipping_phone" value="<?php echo $order->shipping_phone; ?>" data-original="<?php echo $order->shipping_phone; ?>" data-customer="<?php echo $order->phone; ?>">
												</div>
											</div>
											<div class="form-group">
												<label class="col-sm-3 control-label">Delivery Notes:</label>
												<div class="col-sm-9">
													<textarea class="form-control" id="customer_delivery_notes" name="delivery_notes"><?php echo $order->delivery_notes; ?></textarea>
												</div>
											</div>
										</div>
									</div>


									<div class="save_button_holder text-right">
										<?php echo \Form::button('continue', 'Continue', array('id'=> 'continue_button_down', 'class' => 'btn btn-primary', 'value' => '1')); ?>
									</div>

								</div>
								<div role="tabpanel" class="tab-pane <?php echo (\Input::get('step') == 2) ? 'active' : '';?>" id="step_two">
									<div class="row">
										<div class="col-sm-9">
											<h3 class="title-legend">Products</h3>
	                                        <div class="row">
	                                            <?php
	                                            $delivery_date = '';
	                                            $delivery_timehour = '';
	                                            $delivery_timeminute = '';
	                                            $delivery_time_hourminute = '';
	                                            ?>
	                                            <div class="col-sm-12">
	                                                <label>Delivery Date and Time:  <span class="text-danger"></span></label>
	                                            </div>
	                                            <div class="col-sm-3">
	                                                <div class="form-group">
	                                                    <?php $del_date = ($order->delivery_datetime)?date('d/m/Y', strtotime($order->delivery_datetime)):''; ?>
	                                                    <label class="datepicker-holder-control" style="width: 100%;"><?php echo \Form::input('delivery_date', $del_date, array('class' => 'form-control deliveryDate')); ?></label>
	                                                    <input type="hidden" name="delivery_datetime_list" value="<?php echo $order->delivery_datetime_list; ?>">
	                                                </div>
	                                            </div>
	                                            <div class="col-sm-9">
	                                                <div class="form-group">
	                                                    <?php
	                                                        $hold_delivery_datetime_list = explode(';', $order->delivery_datetime_list);
	                                                        if(count($hold_delivery_datetime_list) > 0) {
	                                                            $cntr = 0;
	                                                            foreach ($hold_delivery_datetime_list as $time){
	                                                                $cntr++;
	                                                                if($cntr > 1)echo "<div class='additional-time-group' style='display:inline-block;'><label style='margin-left: 10px;'>and &nbsp;&nbsp;</label>";
	                                                                echo \Form::input('delivery_time_hourminute', \Input::post('delivery_time_hourminute', $time), array('class' => 'form-control time_field', 'style' => "padding: 5px;margin-bottom: 5px;display: inline;width: 110px;"));
	                                                                if($cntr > 1)echo "<a class='remove-time' href='#' style='margin-left:5px'><i class='fa fa-close'></i></a></div>";
	                                                            }
	                                                        }
	                                                        else{
	                                                            echo \Form::input('delivery_time_hourminute', \Input::post('delivery_time_hourminute', '12:00 PM'), array('class' => 'form-control time_field', 'style' => "padding: 5px;margin-bottom: 5px;display: inline;width: 110px;"));
	                                                        }
	                                                    ?>

	                                                    <div id="hold-additional-time" style="display:inline;">
	                                                    </div>
	                                                    <a id="add-time-button" href="#" class="btn btn-primary">Add Time</a>
	                                                </div>
	                                            </div>
	                                        </div>
											<div id="add_item_form">
												<div class="row">
													<div class="col-sm-5">
														<div class="form-group">
														
															<?php echo \Form::hidden('product_id'); ?>
	                										<?php echo \Form::hidden('attributeid'); ?>
	                										<?php echo \Form::hidden('uri',\Uri::create('product/product_data/')); ?>
	                										<input type="hidden" id="minimum_order">
															<label>Products:  <span class="text-danger">*</span></label>
												
															<div class="input_holder">
																<?php
																	$products = \Product\Model_Product::find(function($query){
																		$table_name = 'product';
																		$query->where(function($query) use ($table_name){
														                    $query->where($table_name . '.status', 1);
														                    $query->or_where(function($query) use ($table_name){
														                        $query->where($table_name . '.status', 2);
														                        $query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
														                        $query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
														                    });
														                });
																		$query->order_by('id', 'asc');
																	});

																	$product_list = array('' => 'Please select product');
																	foreach ($products as $product)
																	{
																		$product_list += array($product->id => $product->title);
																	}
																	echo \Form::select('product', \Input::post('product'),  $product_list, array('class' => 'form-control state require'));
																?>
																<div id="product-attributes-edit-holder"></div>
																<div id="product-pack-options-edit-holder"></div>
															</div>
														</div>
													</div>
													<div class="col-sm-2">
														<div class="form-group">
															<label>Price:  <span class="text-danger">*</span></label>
															<div class="input_holder">
																<?php
																	echo \Form::input('price', \Input::post('price', '' ), array('class' => 'form-control require'));
																?>
															</div>
														</div>
													</div>
													<div class="col-sm-2">
														<div class="form-group">
															<label>Quantity:  <span class="text-danger">*</span></label>
															<div class="input_holder">
																<?php echo \Form::input('quantity', \Input::post('quantity', 1), array('class' => 'form-control require','type' => 'number')); ?>
															</div>
														</div>
													</div>
													<div class="col-sm-3">
														<div class="form-group">
															<label>&nbsp;</label>
															<div class="form-search" id="add-product-group">
																<?php echo \Form::button('add', 'Add', array('id' => 'add_item_button', 'class' => 'btn btn-primary btn-block', 'value' => '1')); ?>
																<span class="is-loading text-white" id="loading_area_item"></span>
															</div>
															<div class="form-search hide" id="edit-product-group">
																<?php echo \Form::button('save', 'Save', array('id' => 'save_item_button', 'class' => 'btn btn-success ', 'value' => '1')); ?>
																<?php echo \Form::button('cancel', 'Cancel', array('id' => 'cancel_item_button', 'class' => 'btn btn-primary ', 'value' => '1')); ?>
															</div>

														</div>
													</div>
												</div>
												<div class="row">
													<div id="prod-attribute-list" class="col-sm-5">
													</div>
													<div id="pack-options" class="col-sm-7">
													</div>
												</div>
											</div>
											<table id="item_list_table" class="table table-striped table-bordered">
												<thead>
												<tr class="blueTableHead">
													<th scope="col" style="width: 135px;" class="center" scope="col">Delivery Time</th>
													<th scope="col">Product Name</th>
													<th scope="col">Product Code</th>
													<th style="width: 100px;" class="center" scope="col">Unit Price</th>
													<th style="width: 100px;" class="center" scope="col">Qty</th>
													<th style="width: 100px;" class="center" scope="col">Total Price</th>
													<th style="width: 50px;" class="center" scope="col">Edit</th>
													<th style="width: 50px;" class="center" scope="col">Delete</th>
												</tr>
												</thead>
												<tbody>

									                <?php $total = 0; if(!empty($order->products)): ?>
									                    <?php foreach($order->products as $key => $product): $total += ($product->price * $product->quantity); ?>
									                        <tr data-index="<?php echo $key; ?>">
									                        	<td>
									                        		<?php
									                        			if($order->delivery_datetime_list)
								                        				{
								                        					$hold_delivery_datetime_list = explode(';', $order->delivery_datetime_list);
								                        					$hold_options = '';
								                        					foreach ($hold_delivery_datetime_list as $time)
								                        					{
								                        						$hold_options .= '<option '.($product->delivery_time == $time?'selected':'').' value="'.$time.'">'.$time.'</option>';
								                        					}

								                        					if($hold_options)
								                        					{
								                        						$hold_select = '<select name="delivery_time[]" class="form-control" style="width:110px;display:inline;">';
								                        						$hold_select .= $hold_options;
								                        						$hold_select .= '</select>';
								                        						echo $hold_select;
								                        					}
								                        				}else{
                                                                            $hold_select = '<select name="delivery_time[]" class="form-control" style="width:110px;display:inline;">';
                                                                            $hold_select .= '<option></option></select>';
                                                                            echo $hold_select;
                                                                        }
								                        			?>
									                        	</td>
									                        	<td><input type="hidden" name="product_id[]" value="<?php echo $product->product_id; ?>">
									                        		<?php echo $product->title; ?>
									                        	</td>
									                        	<td>
									                        		<div class="product-attributes-holder">
									                        			<?php
									                        				$attribute_list = json_decode($product->attributes_id, true);
									                        				if($attribute_list)
									                        				{
										                        				foreach ($attribute_list as $attrKey => $attr)
										                        				{
										                        					echo '<input type="hidden" name="select_attributes['.$key.']['.$attrKey.']" value="'.$attr.'">';
										                        				}
									                        				}
									                        			?>
									                        		</div>
									                        		<input type="hidden" name="product_code[]" value="<?php echo $product->code; ?>">
									                        		<?php echo $product->code; ?>
									                        	</td>
									                        	<td><input type="hidden" name="product_price[]" value="<?php echo $product->price; ?>">$<?php echo nf($product->price); ?></td>
									                        	<td><input type="hidden" name="product_quantity[]" value="<?php echo $product->quantity; ?>"><?php echo $product->quantity; ?></td>
									                        	<td>$<?php echo nf($product->price * $product->quantity); ?><input type="hidden" name="product_delivery_charge[]" value="<?php echo $product->delivery_charge; ?>"></td>
									                        	<td class="icon center"><a class="text-success" href="#edit_product">Edit</a></td>
									                        	<td class="icon center"><a class="text-danger" href="#remove_product">Delete</a></td>
									                        </tr>
									                        <?php if($product->packs): $packs = json_decode($product->packs, true); ?>
									                        	<?php foreach($packs as $packs_key => $pack): ?>
											                        <tr data-index="<?php echo $key; ?>" data-pack_options="true" data-pack_id="<?php echo $pack['id']; ?>">
											                        	<td>
											                        		<?php
											                        			if($order->delivery_datetime_list)
										                        				{
										                        					$hold_delivery_datetime_list = explode(';', $order->delivery_datetime_list);
										                        					$hold_options = '';
										                        					foreach ($hold_delivery_datetime_list as $time)
										                        					{
										                        						$hold_options .= '<option '.($pack['time'] == $time?'selected':'').' value="'.$time.'">'.$time.'</option>';
										                        					}

										                        					if($hold_options)
										                        					{
										                        						$hold_select = '<select name="pack_delivery_time['.$product->product_id.']['.$pack['id'].']" class="form-control" style="width:110px;display:inline;">';
										                        						$hold_select .= $hold_options;
										                        						$hold_select .= '</select>';
										                        						echo $hold_select;
										                        					}
										                        				}
										                        			?>
											                        	</td>
											                        	<td colspan="7">
																			<input type="hidden" name="packs[<?php echo $product->product_id; ?>][<?php echo $pack['id']; ?>][name]" value="<?php echo $pack['name']; ?>">
																			<input type="hidden" name="packs[<?php echo $product->product_id; ?>][<?php echo $pack['id']; ?>][types]" value="<?php echo $pack['types']; ?>">
																			<input type="hidden" name="packs[<?php echo $product->product_id; ?>][<?php echo $pack['id']; ?>][selection_limit]" value="<?php echo $pack['selection_limit']; ?>">
																			<?php echo $pack['name']; ?>
																			<?php if(isset($pack['products'])): ?>
																				<?php $hold_prdct_names = array(); foreach ($pack['products'] as $prdct_key => $prdct): ?>
																					<input data-pack_option_product="<?php echo $prdct['id']; ?>" type="hidden" name="packs[<?php echo $product->product_id; ?>][<?php echo $pack['id']; ?>][products][<?php echo $prdct['id']; ?>]" value="<?php echo $prdct['name']; ?>">
																				<?php
																						array_push($hold_prdct_names, $prdct['name']);
																					endforeach;
																					if($hold_prdct_names)
																						echo '( '.implode(',', $hold_prdct_names).' )';
																				?>
																			<?php endif; ?>
																		</td>
											                        </tr>
										                        <?php endforeach; ?>
									                    	<?php endif; ?>
									                        <?php $total_category_discount += \Discountcode\Model_Discountcode::getCategoryDiscount($product->product_id, ($product->price * $product->quantity)); ?>
									                    <?php endforeach; ?>
									                <?php else: ?>
									                    <tr id="no_items"><td colspan="8">There are no items.</td></tr>
									                <?php endif; ?>
												</tbody>
											</table>
										</div>
										<div class="col-sm-3">
	                        				<?php //$gst =  ( $total + $order->shipping_price ) * 0.1;?>
											<h3 class="title-legend">Order Summary</h3>
											<div class="form-group">
												<label>
													Discount Code <div id="loading_area_discount"></div>
												</label>
												<?php
													$hold_discount = false;
													if($order->id_discount)
														$hold_discount = \Discountcode\Model_Discountcode::find_one_by_id($order->id_discount);
												?>
												<input id="discount_action" type="hidden" value="<?php echo ($hold_discount?'Remove':'Apply'); ?>" name="discount_action" data-code="<?php echo ($hold_discount?$hold_discount->code:''); ?>" data-value="<?php echo ($hold_discount?$order->discount_amount:''); ?>">
												<?php echo \Form::input('discount_code', $hold_discount?$hold_discount->code:'', array('class' => 'form-control')); ?>
												<?php echo \Form::button('discount_button', ($hold_discount?'Remove':'Apply'), array('id'=> '', 'class' => 'btn btn-primary mt-5px', 'value' => '1')); ?>
											</div>
											<div class="form-group">
												<?php echo \Form::label('Products Total'); ?>
												<?php echo \Form::input('products_total', '$'.nf($total), array('class' => 'form-control', 'disabled')); ?>
												<input type="hidden" name="products_total_hidden" value="<?php echo $total; ?>">
											</div>
											<div class="form-group">
												<?php echo \Form::label('Shipping'); ?>
												<?php echo \Form::input('shipping', '$'.nf($order->shipping_price), array('class' => 'form-control')); ?>
												<input type="hidden" name="shipping_hidden" value="<?php echo $order->shipping_price; ?>">
											</div>
											<div class="form-group">
												<?php echo \Form::label('Extra Delivery Charge'); ?>
												<div class="input_holder">
													<?php echo \Form::input('delivery_charge', '$'.nf($order->total_delivery_charge), array('class' => 'form-control', 'disabled')); ?>
													<input type="hidden" name="delivery_charge_hidden" value="<?php echo $order->total_delivery_charge; ?>">
												</div>
											</div>
											<div class="form-group">
												<?php echo \Form::label('Discount'); ?>
												<?php echo \Form::input('discount', '$'.nf($order->discount_amount), array('class' => 'form-control', 'disabled')); ?>
												<input type="hidden" name="discount_hidden" value="<?php echo $order->discount_amount; ?>" data-categoryDiscount="<?php echo $total_category_discount; ?>">
											</div>
											<div class="form-group">
												<?php echo \Form::label('Grand Total'); ?>
												<?php echo \Form::input('grand_total', '$'.nf($order->total_price()), array('class' => 'form-control', 'disabled')); ?>
												<input type="hidden" name="grand_total_hidden" value="<?php echo ($order->total_price()); ?>">
											</div>
											<?php 
					                            $hold_gst = (isset($settings['gst']) ? 1 : 0);
					                            $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
					                        ?>
											<?php if($hold_gst && $hold_gst_percentage > 0): ?>
												<div class="form-group">
													<?php echo \Form::label('GST Tax '.$hold_gst_percentage.'% (Included)'); ?>
													<div class="input_holder">
														<?php echo \Form::input('gst_amount', '$'.nf(\Helper::calculateGST($order->total_price(), $hold_gst_percentage)), array('class' => 'form-control', 'disabled')); ?>
														<input type="hidden" name="gst_amount_hidden" value="<?php echo \Helper::calculateGST($order->total_price(), $hold_gst_percentage); ?>">
													</div>
												</div>
											<?php endif;?>
										</div>
									</div>

									<div class="save_button_holder text-right">
										<input type="hidden" name="edit_form" value="1">
										<?php echo \Form::button('back', 'Back', array('id'=> 'back_button_down', 'class' => 'btn btn-default', 'value' => '1')); ?>
										<?php echo \Form::button('continue', 'Continue', array('id'=> 'continue_button_down2', 'class' => 'btn btn-primary', 'value' => '1')); ?>
										<?php echo \Form::button('save_order_only_button', '<i class="fa fa-edit"></i> Submit Order', array('id'=> '','class' => 'save_order_only_button btn btn-success', 'type' => 'submit', 'value' => '1')); ?>
									</div>
								</div>

								<div role="tabpanel" class="tab-pane <?php echo (\Input::get('step') == 3) ? 'active' : '';?>" id="step_three">

									<div class="row">
										<div class="col-sm-5">
											<div class="form-horizontal">
												<div class="form-group">
													<label class="col-sm-3 control-label">&nbsp;</label>
													<div class="col-sm-9">
														<h3 class="title-legend">Payment</h3>
													</div>
												</div>

												<div class="form-group ">
													<label class="col-sm-3 control-label">Payment Method</label>
													<div class="col-sm-9">
														<input type="hidden" name="payment_type" id="payment_type">
														<?php echo \Form::select('payment_method', \Input::post('payment_method', (!empty($order->last_payment->method)?$order->last_payment->method:'')), \Config::get('details.payment_method', array()), array('class' => 'form-control')); ?>
													</div>
												</div>
												<input type="hidden" name="payment_status" value="completed">
												<?php if($hold_enable_partial_payment): ?>
													<div id="partial-payment-section">
														<div class="form-group ">
															<label class="col-sm-3 control-label">Amount</label>
															<div class="col-sm-9">
																<input class="form-control" name="ppamount" required="" type="number" min="1" max="<?php echo $order->total_price(); ?>" step="0.01">
															</div>
														</div>
													</div>
												<?php endif; ?>
												<div id="total-payment-section">
													
												</div>
												<div class="text-right">
													<?php echo \Form::button('create', 'Add Payment', array('id'=> 'save_button_down', 'id'=> 'save_button_down', 'type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-primary', 'value' => '1')); ?>
												</div>
											</div>
										</div>
										<?php
											$partial_payment_list = \Payment\Model_Payment::find(function($query) use($order){
											    $query->where('order_id', $order->id);
											    $query->where('partial_payment', 1);
											    $query->order_by('updated_at', 'desc');
											});
											$partial_payment_completed_sum = \DB::select(\DB::expr('SUM(total_price) as payment_sum'))
																				->from('payments')
																				->where('order_id', $order->id)
																				->where('status', 'completed')
																				->where('partial_payment', 1)
																				->execute();
										?>
										<?php if($partial_payment_list): ?>
											<div class="col-sm-7">
												<div class="form-horizontal">
													<h3 class="title-legend">Partial Payment Transactions</h3>
													<table class="table table-striped table-bordered">
														<thead>
														<tr class="blueTableHead">
															<th scope="col">ID</th>
															<th scope="col">Gateway</th>
															<th scope="col">Amount</th>
															<th scope="col">Status</th>
															<th scope="col">Date</th>
															<th scope="col">Actions</th>
														</tr>
														</thead>
														<tbody>
															<?php foreach($partial_payment_list as $payment): ?>
																<tr>
																	<td><?php echo $payment->id; ?></td>
																	<td><?php echo Inflector::humanize($payment->method); ?></td>
																	<td>$<?php echo number_format(round($payment->total_price, 2), 2); ?></td>
																	<td><?php echo Inflector::humanize($payment->status); ?></td>
																	<td><?php echo date('d/m/Y', $payment->updated_at).'<br />'.date('H:i:s', $payment->updated_at); ?></td>
																	<td>
																		<?php if(in_array($payment->method, \Config::get('details.allow_delete_gateway_partial_payment_transaction', array()))): ?>
																			<ul class="table-action-inline">
															                    <li>
															                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete partial payment transaction?" href="<?php echo \Uri::create('admin/order/delete_ppt/'.$payment->id); ?>">Delete</a>
															                    </li>
															                </ul>
															            <?php endif; ?>
																	</td>
																</tr>
															<?php endforeach; ?>
														</tbody>
													</table>
													<div class="text-right">
														<div class="row">
															<div class="col-sm-12">
																<label>Grant Total :</label> $<span id="show_grand_total">0.00</span>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-12">
																<?php
																	$payment_sum = '0.00';
																	if($payment_sum = $partial_payment_completed_sum[0]['payment_sum'])
																		echo '<input type="hidden" name="pp_total_paid_amount" value="'.$payment_sum.'">';
																?>
																<label>Amount Paid :</label> $<span id="show_amount_paid"><?php echo number_format(round($payment_sum, 2), 2); ?></span>
															</div>
														</div>
														<div class="row">
															<div class="col-sm-12">
																<label>Balance Due :</label> $<span id="show_balance_due">0.00</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										<?php endif; ?>
									</div>
									<div class="save_button_holder text-right">
										<input type="hidden" name="payment_form" value="1" disabled>
										<?php echo \Form::button('back', 'Back', array('id'=> 'back_button_down2', 'class' => 'btn btn-default', 'value' => '1')); ?>
										<?php echo \Form::button('save_order_only_button', '<i class="fa fa-edit"></i> Submit Order', array('id'=> '','class' => 'save_order_only_button btn btn-success', 'type' => 'submit', 'value' => '1')); ?>
									</div>
								</div>			
							</div>
						</div><!-- EOF Main Content Holder -->

						<div class="clear"></div>
						
					</div>
				</div>

			<?php echo \Form::close(); ?>
		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
<?php
	echo \Theme::instance()->asset->css('plugins/select2-4.0.3.min.css');
	echo \Theme::instance()->asset->js('plugins/select2-4.0.3.min.js');
?>

<div id="product_time_template" style="display:none;">
<?php
	if($order->delivery_datetime_list)
	{
		$hold_delivery_datetime_list = explode(';', $order->delivery_datetime_list);
		$hold_options = '';
		if($hold_delivery_datetime_list)
		{
			foreach ($hold_delivery_datetime_list as $time)
			{
				$hold_options .= '<option value="'.$time.'">'.(isset(\Config::get('details.time.hour_list')[$time])?\Config::get('details.time.hour_list')[$time]:'').'</option>';
			}
		}

		if($hold_options)
		{
			$hold_select = '<select name="delivery_time[]" class="form-control" style="width:110px;display:inline;">';
			$hold_select .= $hold_options;
			$hold_select .= '</select>';
			echo $hold_select;
		}
	}else{
        $hold_select = '<select name="delivery_time[]" class="form-control" style="width:110px;display:inline;">';
        $hold_select .= '</select>';
        echo $hold_select;
    }
?>
</div>

<script type="text/javascript">
	var customer_list = [];
	var filtered_items = [], items = [], shipping_value = 0;
	var securePayCode = '', eWayCode = '';

	shipping_value = "<?php echo $shipping_price; ?>";

	<?php
		// Get all users and remove admin users from array
		$users = \SentryAdmin::user()->all('front');

		// get object
		foreach($users as $key=> $user)
		{
			$users[$key] = \SentryAdmin::user((int)$user['id']);
		}

		// filter only activated accounts
		foreach($users as $number => $user)
		{
			if($user->activated != '1') unset($users[$number]);
		}

		foreach ($users as $user)
		{
			?>
				customer_list.push({id: <?php echo $user->id; ?>, text: "<?php echo $user->get('metadata.first_name').' '.$user->get('metadata.last_name'); ?>" });
			<?php
		}

        if(isset(\Config::load('securePay.db')['code']))
        {
			?>
				securePayCode = "<?php echo \Config::load('securePay.db')['code']; ?>";
			<?php
        }
        if(isset(\Config::load('eWay.db')['code']))
        {
			?>
				eWayCode = "<?php echo \Config::load('eWay.db')['code']; ?>";
			<?php
        }
	?>
</script>
<?php echo \Theme::instance()->asset->js('order/create.js'); ?>
<script type="text/javascript">
    <?php if(!empty($order->products)): ?>
        <?php foreach($order->products as $product): ?>
        	var item = { product_id : "<?php echo $product->product_id; ?>", product_quantity : "<?php echo $product->quantity; ?>", product_price : <?php echo $product->price; ?>, product_code: "<?php echo $product->code; ?>", select: <?php echo $product->attributes_id?:'{}'; ?>,delivery_time : "<?php echo $product->delivery_time; ?>" };
        	items.push(item);
        <?php endforeach; ?>
    <?php endif; ?>

	var hold_items = items;
	filtered_items = [];
	for (var i = 0; i < items.length; i++) {
		var hold_id = items[i].product_id;
		var hold_quantity = 0;
		var hold_price = 0;
		var hold_attributes = hold_items[i].select;
		for (var j = 0; j < hold_items.length; j++) {
			var hold_id_sub = hold_items[j].product_id;
			var hold_attributes_sub = hold_items[j].select;
			if(hold_id == hold_id_sub && JSON.stringify(hold_attributes) === JSON.stringify(hold_attributes_sub)) // check if product id is existing and check attribute is already existing
			{
				hold_quantity += parseInt(hold_items[j].product_quantity);
				hold_price = parseFloat(hold_items[j].product_price);
			}
		}

		var filtered_item = { product_id : hold_id, product_quantity : hold_quantity, product_price : hold_price, select: hold_attributes  };
		// check if product id is existing and if not the item will be added
		if(returnIndexContainsObject(filtered_item, filtered_items) == -1)
			filtered_items.push(filtered_item);
	}
</script>

<?php
    echo \Theme::instance()->asset->js('plugins/jquery.plugin.js');
    echo \Theme::instance()->asset->js('plugins/jquery.timeentry.js');
?>

<script language="javascript">
    $(document).ready(function(){
        var time_list = [];
        var get_hour_options = '';
        var get_hourtime_options = '';
        $('select[name="delivery_time_hourminute"]').find('option').each(function(index, elem){
            get_hourtime_options += $(elem).get(0).outerHTML.replace('selected="selected"', '');
        });

        update_time_list();
        generate_delivery_datetime_list();

        $('.time_field').timeEntry({spinnerImage: '', ampmPrefix: ' '}).change(function(){
            update_time_list();
            generate_delivery_datetime_list();
        });

        var countTime = 1;
        $("#add-time-button").click(function(e){
            e.preventDefault();

            if(countTime == 1)
            {
                if($('input[name="delivery_time_hourminute"]').val() != '-')
                    time_list.push($('input[name="delivery_time_hourminute"]').val());
                else
                    time_list.push('Time '+countTime);
            }

            countTime += 1;

            var select_time_hourminute = "<input name='delivery_time_hourminute' value='12:00 PM' class='form-control time_field' style='padding: 5px;margin-bottom: 5px;display: inline;width: 110px;'/>";

            var time = 'Time '+countTime;
            $('#hold-additional-time').append('<div data-order="'+countTime+'" class="additional-time-group" style="display:inline-block;"><label style="margin-left: 10px;">and &nbsp;&nbsp;</label>'+select_time_hourminute+' <a class="remove-time" href="#"><i class="fa fa-close"></i></a></div>');
            $('.time_field').timeEntry({spinnerImage: '', ampmPrefix: ' '}).change(function() {
                update_time_list();
                generate_delivery_datetime_list();
            });
            update_time_list();
            generate_delivery_datetime_list();
            time_list.push(time);

        });


        $( "div" ).on( "click", "a.remove-time", function(e) {
            e.preventDefault();
            var getOrder = $(this).closest('div.additional-time-group').data('order');
            time_list.splice((getOrder-1), 1);
            $(this).closest('div.additional-time-group').remove();
            $('#product_time_template').find('select[name="delivery_time[]"]').find('option').eq(getOrder-1).remove();
            countTime -= 1;
            var count = 2;
            $('#hold-additional-time').find('div.additional-time-group').each(function(index, elem){
                $(elem).attr('data-order', count);
                $(elem).find('select.additional-time-select').each(function(ind, ele){
                    $(ele).attr('data-order', count);
                });
                count += 1;
            });
            update_time_list();
            generate_delivery_datetime_list();

        });
        $( "div" ).on( "change", "select.additional-time-select", function(e) {
            var getOrder = $(this).data('order');
            if(time_list.length)
            {
                if($('select[data-order="'+getOrder+'"].additional-time-select').val() != '-')
                {
                    var time = $('select[data-order="'+getOrder+'"].additional-time-select').val();
                    time_list[getOrder-1] = time;
                }
                else
                {
                    time_list[getOrder-1] = 'Time '+getOrder;
                }
            }
            else if(countTime == 1)
            {
                if($('select[name="delivery_timehour"]').val() != '-' && $('select[name="delivery_timeminute"]').val() != '-')
                    time_list.push($('select[name="delivery_timehour"]').val()+':'+$('select[name="delivery_timeminute"]').val());
                else
                    time_list.push('Time '+countTime);
            }
            generate_delivery_datetime_list();
        });

        function update_time_list(){
            time_list = [];
            $('input[name="delivery_time_hourminute"]').each(function(){
                if($(this).val() != ''){
                    time_list.push($(this).val());
                }
            });
        }

        function generate_delivery_datetime_list()
        {
            if(time_list.length)
            {
                var datetime_list = '';
                var delivery_time_options = '';
                for(var i=0; i < time_list.length; i++)
                {
                    if(time_list[i].indexOf(':') > -1)
                    {
                        if(datetime_list.indexOf(time_list[i]) < 0)
                        {
                            datetime_list += time_list[i]+';';
                            delivery_time_options += '<option value="'+time_list[i]+'">'+time_list[i]+'</option>';
                        }
                    }
                }
                datetime_list = datetime_list.substring(0, datetime_list.length - 1);
                $('input[name="delivery_datetime_list"]').val(datetime_list);
                $('#product_time_template').find('select[name="delivery_time[]"]').html(delivery_time_options);

                if($('#item_list_table').find('select[name="delivery_time[]"]').length)
                {
                    $('#item_list_table').find('select[name="delivery_time[]"]').each(function(index, elem){
                        var _time = $('#product_time_template select').clone();
                        var get_index_selected = $(this).find("option:selected").index();
                        _time.find('option').eq(get_index_selected).attr('selected', 'selected');
                        $(this).html(_time.html());
                    });

                    $('#item_list_table').find('select[name*="pack_delivery_time"]').each(function(index, elem){
                        var _time = $('#product_time_template select').clone();
                        var get_index_selected = $(this).find("option:selected").index();
                        _time.find('option').eq(get_index_selected).attr('selected', 'selected');
                        _time.attr('name', $(this).attr('name'));
                        $(this).html(_time.html());
                    });
                }
            }
        }
    });
</script>