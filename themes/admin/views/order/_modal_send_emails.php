<?php $settings = \Config::load('autoresponder.db'); ?>

<ul class="list-group hover">
  <li class="list-group-item">Order <span class="small pull-right"><a href="<?php echo \Uri::create('admin/order/pdf/' . $order->id); ?>" target="_blank"> Dowload pdf file</a> | <a href="#order-email" data-toggle="modal"> Send email</a></span></li>
  <li class="list-group-item">Invoice <span class="small pull-right"><a href="<?php echo \Uri::create('admin/order/pdf/' . $order->id.'/invoice'); ?>" target="_blank"> Dowload pdf file</a> | <a href="#invoice-email" data-toggle="modal"> Send email</a></span></li>
  <li class="list-group-item">Packing Slip <span class="small pull-right"><a href="<?php echo \Uri::create('admin/order/pdf/' . $order->id.'/packing-slip'); ?>" target="_blank"> Dowload pdf file</a> | <a href="#packing-slip-email" data-toggle="modal"> Send email</a></span></li>
  <li class="list-group-item">Delivery Note <span class="small pull-right"><a href="<?php echo \Uri::create('admin/order/pdf/' . $order->id.'/delivery-note'); ?>" target="_blank"> Dowload pdf file</a> | <a href="#delivery-note-email" data-toggle="modal"> Send email</a></span></li>
</ul>

<div class="modal fade" id="order-email" tabindex="-1" role="dialog" aria-labelledby="order-emailLabel">
  <div class="modal-dialog modal-lg" role="document">
    <form action="<?php echo \Uri::create('admin/order/pdf_send_email/' . $order->id); ?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="order-emailLabel">Order <?php echo $order->id; ?></h4>
        </div>
        <div class="modal-body form-horizontal">

          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>To* </i></label>
            <div class="col-sm-11"><input type="email" required name="email_to" class="form-control" value="<?php echo $order->email; ?>" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>CC</i></label>
            <div class="col-sm-11"><input type="email" required name="email_cc" class="form-control" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>From* </i></label>
            <div class="col-sm-11"><input required type="email" required name="email_from" class="form-control" value="<?php echo $settings['company_name'];?> <<?php echo $settings['sender_email_address']; ?>>" placeholder="name <example@mail.com> OR example@mail.com"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>Subject*</i></label>
            <div class="col-sm-11"><input type="text" required name="email_subject" class="form-control" placeholder="Enter subject here" value="<?php echo $settings['company_name']; ?> Order"></div>
          </div>
          <hr>
          <label class="control-label"><i>Message* </i></label>
          <textarea name="message" required class="form-control" rows="10">Dear <?php echo $order->first_name.' '.$order->last_name; ?>,
          
See attached in PDF your (order). 
          
Should you have any questions, please do not hesitate to contact us.
          
Kind regards
          
<?php echo $settings['company_name']; ?>

<?php echo $settings['phone']; ?>
          </textarea>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary pdf-send-email-submit">Send email</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="invoice-email" tabindex="-1" role="dialog" aria-labelledby="invoice-emailLabel">
  <div class="modal-dialog modal-lg" role="document">
    <form action="<?php echo \Uri::create('admin/order/pdf_send_email/' . $order->id . '/invoice'); ?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="invoice-emailLabel">Tax Invoice <?php echo $order->id; ?></h4>
        </div>
        <div class="modal-body form-horizontal">

          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>To* </i></label>
            <div class="col-sm-11"><input type="email" required name="email_to" class="form-control" value="<?php echo $order->email; ?>" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>CC</i></label>
            <div class="col-sm-11"><input type="email" required name="email_cc" class="form-control" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>From* </i></label>
            <div class="col-sm-11"><input type="email" required name="email_from" class="form-control" value="<?php echo $settings['company_name'];?> <<?php echo $settings['sender_email_address']; ?>>" placeholder="name <example@mail.com> OR example@mail.com"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>Subject*</i></label>
            <div class="col-sm-11"><input type="text" required name="email_subject" class="form-control" placeholder="Enter subject here" value="<?php echo $settings['company_name']; ?> Order"></div>
          </div>
          <hr>
          <label class="control-label"><i>Message* </i></label>
          <textarea name="message" required class="form-control" rows="10">Dear <?php echo $order->first_name.' '.$order->last_name; ?>,
          
See attached in PDF your (invoice). 
          
Should you have any questions, please do not hesitate to contact us.
          
Kind regards
          
<?php echo $settings['company_name']; ?>

<?php echo $settings['phone']; ?>
          </textarea>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary pdf-send-email-submit">Send email</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="packing-slip-email" tabindex="-1" role="dialog" aria-labelledby="packing-slip-emailLabel">
  <div class="modal-dialog modal-lg" role="document">
    <form action="<?php echo \Uri::create('admin/order/pdf_send_email/' . $order->id .'/packing-slip'); ?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="packing-slip-emailLabel">Packing slip <?php echo $order->id; ?></h4>
        </div>
        <div class="modal-body form-horizontal">

          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>To* </i></label>
            <div class="col-sm-11"><input type="email" required name="email_to" class="form-control" value="<?php echo $order->email; ?>" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>CC</i></label>
            <div class="col-sm-11"><input type="email" required name="email_cc" class="form-control" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>From* </i></label>
            <div class="col-sm-11"><input type="email" required name="email_from" class="form-control" value="<?php echo $settings['company_name'];?> <<?php echo $settings['sender_email_address']; ?>>" placeholder="name <example@mail.com> OR example@mail.com"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>Subject*</i></label>
            <div class="col-sm-11"><input type="text" required name="email_subject" class="form-control" placeholder="Enter subject here" value="<?php echo $settings['company_name']; ?> Order"></div>
          </div>
          <hr>
          <label class="control-label"><i>Message* </i></label>
          <textarea name="message" required class="form-control" rows="10">Dear <?php echo $order->first_name.' '.$order->last_name; ?>,
          
See attached in PDF your (packing slip). 
          
Should you have any questions, please do not hesitate to contact us.
          
Kind regards
          
<?php echo $settings['company_name']; ?>

<?php echo $settings['phone']; ?>
          </textarea>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary pdf-send-email-submit" >Send email</button>
        </div>
      </div>
    </form>
  </div>
</div>

<div class="modal fade" id="delivery-note-email" tabindex="-1" role="dialog" aria-labelledby="delivery-note-emailLabel">
  <div class="modal-dialog modal-lg" role="document">
    <form action="<?php echo \Uri::create('admin/order/pdf_send_email/' . $order->id .'/delivery-note'); ?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="delivery-note-emailLabel">Delivery note <?php echo $order->id; ?></h4>
        </div>
        <div class="modal-body form-horizontal">

          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>To* </i></label>
            <div class="col-sm-11"><input type="email" required name="email_to" class="form-control" value="<?php echo $order->email; ?>" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>CC</i></label>
            <div class="col-sm-11"><input type="email" required name="email_cc" class="form-control" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>From* </i></label>
            <div class="col-sm-11"><input type="email" required name="email_from" class="form-control" value="<?php echo $settings['company_name'];?> <<?php echo $settings['sender_email_address']; ?>>" placeholder="name <example@mail.com> OR example@mail.com"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>Subject*</i></label>
            <div class="col-sm-11"><input type="text" required name="email_subject" class="form-control" placeholder="Enter subject here" value="<?php echo $settings['company_name']; ?> Order"></div>
          </div>
          <hr>
          <label class="control-label"><i>Message* </i></label>
          <textarea name="message" required class="form-control" rows="10">Dear <?php echo $order->first_name.' '.$order->last_name; ?>,
          
See attached in PDF your (delivery note). 
          
Should you have any questions, please do not hesitate to contact us.
          
Kind regards
          
<?php echo $settings['company_name']; ?>

<?php echo $settings['phone']; ?>
          </textarea>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary pdf-send-email-submit" >Send email</button>
        </div>
      </div>
    </form>
  </div>
</div>