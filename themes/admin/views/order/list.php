<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Orders');
        \Breadcrumb::set('Manage Orders', 'admin/order/list');

        // If viewing category products show category title
        if(isset($user))
            \Breadcrumb::set($user->get('metadata.first_name') . ' ' . $user->get('metadata.last_name'));

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Orders</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/order/_action_links', array('hide_show_all' => 1, 'show_import_to_myob' => 1, 'show_import_xero' => 1)); ?>
                </div>
            </header>

            <?php
            if(isset($user))
                echo \Theme::instance()->view('views/user/_navbar_links', array('user' => $user));
            ?>
            <?php
            // Load page listing table
            echo \Theme::instance()->view('views/order/_listing_table',
                array(
                    'pagination' 	=> $pagination,
                    'items'			=> $items,
                    'get_attributes' => $get_attributes,
                ),
                false
            );
            ?>



        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>