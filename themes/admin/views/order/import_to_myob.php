<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Orders');
        \Breadcrumb::set('Manage Orders', 'admin/order/list');
        \Breadcrumb::set('Import to MYOB', 'admin/order/import_to_myob');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Import to MYOB</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/order/_action_links', array()); ?>
                </div>
            </header>

            <div class="panel panel-default">
                <div class="panel-heading"><h3 class="panel-title pull-left">MYOB API Settings</h3></div>
                <div class="panel-body form-horizontal">
                    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'row-fluid', 'id' => 'SetForm')); ?>
                    <input type="hidden" name="name" value="MYOB" />
                    <input type="hidden" name="code" value="myob" />
                    <?php
                        $api_key = (isset($settings['api_key']) ? $settings['api_key'] : '');
                        $api_secret = (isset($settings['api_secret']) ? $settings['api_secret'] : '');
                    ?>

                    <div class="form-group clearfix">
                        <label class="col-md-3 control-label">API Key:  <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input class="form-control" name="api_key" value="<?php echo \Input::post('api_key', $api_key) ?>" type="text" id="form_api_key">
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <label class="col-md-3 control-label">API Secret Key:  <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input class="form-control" name="api_secret" value="<?php echo \Input::post('api_secret', $api_secret) ?>" type="text" id="form_api_secret">
                        </div>
                    </div>

                    <div class="form-group clearfix">
                        <label class="col-md-3 control-label">Redirect Uri: </label>
                        <div class="col-md-4">
                            <input class="form-control" value="<?php echo \Uri::create('admin/order/process_import_to_myob'); ?>" readonly="">
                        </div>
                    </div>

                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success" name="submit" type="submit" value="save"><i class="fa fa-edit"></i> Save</button>
                                <button class="btn btn-primary" name="submit" type="submit" value="auth"><i class="fa fa fa-cloud-upload"></i> Import Orders to MYOB AccountRight</button>
                            </div>
                        </div>
                    </div>

                    <?php echo \Form::close(); ?>
                </div>
            </div>
        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>