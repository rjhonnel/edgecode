
<?php
// Configs
$invoice = array('' => '-') + \Config::get('details.invoice', array());
$status = array('' => '-') + \Config::get('details.status', array());
$delivery = array('' => '-') + \Config::get('details.delivery', array());

$app_countries = \App\Countries::forge();
$countries = $app_countries->getCountries();
$app_states = \App\States::forge();

$counter_time = 1;
?>
<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Orders');
        \Breadcrumb::set('Manage Orders', 'admin/order/list');
        \Breadcrumb::set('ID: ' . $order->id);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <div class="clearfix">
                    <div class="pull-left">
                        <h4 class="">View Order: <strong class="text-primary">#<?php echo $order->id; ?></strong></h4>
                    </div>
                    <div class="pull-right">
                        <?php //echo \Theme::instance()->view('views/order/_action_links', array('save' => 1, 'hide_add_new' => 1,'approved_order' => \Uri::create('admin/order/approved/'.$order->id), 'show_edit_order' => \Uri::create('admin/order/edit/'.$order->id), 'show_edit_payment' => \Uri::create('admin/order/edit/'.$order->id.'?step=3') )); ?>
                        <?php echo \Theme::instance()->view('views/order/_action_links', array('save' => 1, 'hide_add_new' => 1, 'order_id' => $order->id, 'show_edit_order' => \Uri::create('admin/order/edit/'.$order->id), 'show_edit_payment' => \Uri::create('admin/order/edit/'.$order->id.'?step=3') )); ?>
                    </div>
                </div>

                <?php
                $delivery_date = '';
                $delivery_time_hourminute = '';
                $delivery_timehour = '';
                $delivery_timeminute = '';
                if($order->delivery_datetime)
                {
                    $delivery_date = date('d/m/Y', strtotime($order->delivery_datetime));
                    $delivery_time_hourminute = date('H:ia', strtotime($order->delivery_datetime));
                    $delivery_timehour = date('H', strtotime($order->delivery_datetime));
                    $delivery_timeminute = date('i', strtotime($order->delivery_datetime));
                }
                ?>

                <ul class="list-inline-label">
                    <li>Date Submited: <strong><?php echo date('d/m/Y H:i:s', $order->created_at); ?></strong></li>
                    <li>Status: <?php echo \Form::select('status', \Input::post('status', $order->status), \Config::get('details.status', array()), array('class' => '')); ?></li>
                    <li>Delivery Date: <strong><?php echo $delivery_date; ?></strong></li>
                    <li>Delivery Time: <strong><?php echo $delivery_time_hourminute; ?> <?php echo $order->delivery_datetime_list?(count(explode(';', $order->delivery_datetime_list))>1?($order->delivery_datetime?'+':explode(';', $order->delivery_datetime_list)[0].' +'):''):''; ?></strong>
                    </li>
                </ul>
            </header>


            <?php echo \Form::open(array('action' => \Uri::admin(), 'method' => 'post')); ?>
            <?php echo \Form::hidden('order_details', 1); ?>

            <?php echo \Form::hidden('customer_details', 1); ?>

            <h4 class="title-legend">Customer Details</h4>

            <div class="panel panel-default panel-col2">

                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="panel-title pull-left mt-5px">Billing Information</h3>
                            <a class="btn btn-mini btn-primary pull-right" href="<?php echo ($user)?\Uri::create('admin/user/update/' . $user->get('id')):''; ?>"><i class="fa fa-user"></i> Profile</a>
                        </div>
                        <div class="col-sm-6">
                            <h3 class="panel-title pull-left mt-5px">Shipping Information</h3>
                            <label class="control-label pull-right" style="margin-top:3px;"><input type="checkbox" id="same_as_billing" checked> Same as Billing</label>
                        </div>
                    </div>
                </div>
                <div class="panel-body form-horizontal">
                    <input type="hidden" id="form_update_address" value="1" name="update_address">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title:</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php echo \Form::select('title', \Input::post('title', $order->title), \Config::get('user.titles', array()), array('class' => 'form-control', 'readonly')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Full Name:  <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php echo \Form::input('first_name', \Input::post('first_name', $order->first_name), array('class' => 'form-control', 'readonly')); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php echo \Form::input('last_name', \Input::post('last_name', $order->last_name), array('class' => 'form-control', 'readonly')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Company:</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('company', \Input::post('company', $order->company), array('class' => 'form-control', 'readonly')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Address:  <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('address', \Input::post('address', $order->address), array('class' => 'form-control', 'readonly')); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Address 2:</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('address2', \Input::post('address2', $order->address2), array('class' => 'form-control', 'readonly')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">&nbsp;</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('suburb', \Input::post('suburb', $order->suburb), array('class' => 'form-control', 'readonly')); ?>
                                </div>
                            </div>

                            <?php $states = $app_states->getStateProvinceArray($order->country?:'AU'); ?>
                            <div class="form-group">
                                <label class="col-sm-3 control-label ">&nbsp;</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php echo \Form::select('state', \Input::post('state', $order->state), is_array($states) ? array('' => '-') + $states : array('' => '-'), array('class' => 'form-control state', 'readonly')); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php echo \Form::input('postcode', \Input::post('postcode', $order->postcode), array('class' => 'form-control', 'readonly')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">&nbsp;</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::select('country', \Input::post('country', $order->country?:'AU'), array('' => '-') + $countries, array('class' => 'form-control country_select', 'data-state' => 'state', 'readonly')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Telephone:  <span class="text-danger">*</span></label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('phone', \Input::post('phone', $order->phone), array('class' => 'form-control', 'readonly')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title:</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php echo \Form::select('shipping_title', \Input::post('shipping_title', $order->shipping_title), \Config::get('user.titles', array()), array('class' => 'form-control', 'data-original' => $order->shipping_title)); ?>
                                        </div>
                                        <div class="col-sm-6"></div>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Full Name:</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php echo \Form::input('shipping_first_name', \Input::post('shipping_first_name', $order->shipping_first_name), array('class' => 'form-control', 'data-original' => $order->shipping_first_name, 'placeholder'=>'First Name')); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php echo \Form::input('shipping_last_name', \Input::post('shipping_last_name', $order->shipping_last_name), array('class' => 'form-control', 'data-original' => $order->shipping_last_name, 'placeholder'=>'Last Name')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Company:</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('shipping_company', \Input::post('shipping_company', $order->shipping_company), array('class' => 'form-control', 'data-original' => $order->shipping_company)); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Address:</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('shipping_address', \Input::post('shipping_address', $order->shipping_address), array('class' => 'form-control', 'data-original' => $order->shipping_address, 'placeholder'=>'Address')); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Address 2:</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('shipping_address2', \Input::post('shipping_address2', $order->shipping_address2), array('class' => 'form-control', 'data-original' => $order->shipping_address2, 'placeholder'=>'Address 2')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">&nbsp;</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('shipping_suburb', \Input::post('shipping_suburb', $order->shipping_suburb), array('class' => 'form-control', 'data-original' => $order->shipping_suburb, 'placeholder'=>'Suburb')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">&nbsp;</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php $states = $app_states->getStateProvinceArray($order->shipping_country?:'AU'); ?>
                                            <?php echo \Form::select('shipping_state', \Input::post('shipping_state', $order->shipping_state), is_array($states) ? array('' => '-') + $states : array('' => 'Select State'), array('class' => 'form-control shipping_state', 'data-original' => $order->shipping_state, 'placeholder'=>'State')); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php echo \Form::input('shipping_postcode', \Input::post('shipping_postcode', $order->shipping_postcode), array('class' => 'form-control', 'data-original' => $order->shipping_postcode, 'placeholder'=>'Postcode')); ?>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">&nbsp;</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::select('shipping_country', \Input::post('shipping_country', $order->shipping_country?:'AU'), array('' => 'Select Country') + $countries, array('class' => 'form-control country_select', 'data-state' => 'shipping_state', 'data-original' => $order->shipping_country?:'AU')); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Telephone:</label>
                                <div class="col-sm-9">
                                    <?php echo \Form::input('shipping_phone', \Input::post('shipping_phone', $order->shipping_phone), array('class' => 'form-control', 'data-original' => $order->shipping_phone)); ?>
                                </div>
                            </div>

                            <hr>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Shipping Method:</label>
                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?php echo \Form::select('shipping_method', \Input::post('shipping_method', $order->shipping_method), array('' => 'Select Shipping Method') + \Config::get('details.shipping_method', array()), array('class' => 'form-control')); ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?php echo \Form::input('tracking_no', \Input::post('tracking_no', $order->tracking_no), array('class' => 'form-control', 'placeholder'=>'Tracking Number')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="hide">
                                <?php
                                $delivery_date = '';
                                $delivery_time_hourminute = '';
                                $delivery_timehour = '';
                                $delivery_timeminute = '';
                                if($order->delivery_datetime)
                                {
                                    $delivery_date = date('d/m/Y', strtotime($order->delivery_datetime));
                                    $delivery_time_hourminute = date('H:i', strtotime($order->delivery_datetime));
                                    $delivery_timehour = date('H', strtotime($order->delivery_datetime));
                                    $delivery_timeminute = date('i', strtotime($order->delivery_datetime));
                                }
                                ?>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Delivery Date: <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <label class="datepicker-holder-control"><?php echo \Form::input('delivery_date', $delivery_date, array('class' => 'form-control deliveryDate')); ?></label>
                                    </div>
                                    <input type="hidden" name="delivery_datetime_list" value="<?php echo $order->delivery_datetime_list?>">
                                </div>

                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-sm-3 control-label">Delivery Time: <span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="row" >
                                                <?php if($order->delivery_datetime_list): ?>
                                                    <?php echo \Form::select('delivery_time_hourminute', $delivery_time_hourminute, \Config::get('details.time.hour_list'), array('data-order'=>$counter_time, 'class' => 'form-control additional-time-select', 'style' => 'margin-bottom:5px;display: inline;width:150px;padding: 5px;')); ?>
                                                    <?php /*echo \Form::select('delivery_timehour', $delivery_timehour, \Config::get('details.time.hours'), array('data-order'=>$counter_time, 'data-type'=>"hour",'class' => 'form-control additional-time-select', 'style' => 'margin-bottom:5px;display: inline;width:90px;padding: 5px;')); ?>
                                            <?php echo \Form::select('delivery_timeminute', $delivery_timeminute, \Config::get('details.time.minutes'), array('data-order'=>$counter_time, 'data-type'=>"minute",'class' => 'form-control additional-time-select', 'style' => 'margin-bottom:5px;display: inline;width:90px;margin-left: 5px;padding: 5px;'));*/ ?>
                                                    <div id="hold-additional-time" style="display:inline;">
                                                        <?php
                                                        $hold_delivery_datetime_list = str_replace($delivery_timehour.':'.$delivery_timeminute.';', '', $order->delivery_datetime_list);
                                                        $hold_delivery_datetime_list = str_replace(';'.$delivery_timehour.':'.$delivery_timeminute, '', $hold_delivery_datetime_list);
                                                        $hold_delivery_datetime_list = str_replace($delivery_timehour.':'.$delivery_timeminute, '', $hold_delivery_datetime_list);

                                                        if($hold_delivery_datetime_list):
                                                            $hold_delivery_datetime_list = explode(';', $hold_delivery_datetime_list);
                                                            foreach($hold_delivery_datetime_list as $key => $time):
                                                                // $hold_delivery_timehour = date('H', strtotime($time));
                                                                // $hold_delivery_timeminute = date('i', strtotime($time));
                                                                $hold_delivery_time_hourminute = date('H:i', strtotime($time));
                                                                $counter_time +=1;
                                                                ?>
                                                                <div data-order="<?php echo $counter_time; ?>" class="additional-time-group" style="display:inline-block;">
                                                                    <?php echo \Form::select('additional_delivery_time_hourminute_'.$key, $hold_delivery_time_hourminute, \Config::get('details.time.hour_list'), array('data-order'=>$counter_time, 'data-type'=>"hour",'class' => 'form-control additional-time-select', 'style' => 'margin-right: 5px;margin-bottom:5px;display: inline;width:150px;padding: 5px;')); ?>
                                                                    <?php /*echo \Form::select('additional_delivery_timehour_'.$key, $hold_delivery_timehour, \Config::get('details.time.hours'), array('data-order'=>$counter_time, 'data-type'=>"hour",'class' => 'form-control additional-time-select', 'style' => 'margin-left: 5px;margin-bottom:5px;display: inline;width:90px;padding: 5px;')); ?>
                                                            <?php echo \Form::select('additional_delivery_timeminute_'.$key, $hold_delivery_timeminute, \Config::get('details.time.minutes'), array('data-order'=>$counter_time, 'data-type'=>"minute", 'class' => 'form-control additional-time-select', 'style' => 'margin-bottom:5px;display: inline;width:90px;margin-left: 5px;padding: 5px;'));*/ ?>
                                                                    <a class="remove-time" href="#"><i class="fa fa-close"></i></a>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php else: ?>
                                                    <?php echo \Form::select('delivery_time_hourminute', $delivery_time_hourminute, \Config::get('details.time.hour_list'), array('data-order'=>$counter_time, 'class' => 'form-control additional-time-select', 'style' => 'margin-bottom:5px;display: inline;width:150px;padding: 5px;')); ?>
                                                    <?php /*echo \Form::select('delivery_timehour', $delivery_timehour, \Config::get('details.time.hours'), array('data-order'=>$counter_time, 'data-type'=>"hour",'class' => 'form-control', 'style' => 'margin-bottom:5px;display: inline;width:90px;padding: 5px;')); ?>
                                            <?php echo \Form::select('delivery_timeminute', $delivery_timeminute, \Config::get('details.time.minutes'), array('data-order'=>$counter_time, 'data-type'=>"minute",'class' => 'form-control', 'style' => 'margin-bottom:5px;display: inline;width:90px;margin-left: 5px;padding: 5px;'));*/ ?>
                                                    <div id="hold-additional-time" style="display:inline;">
                                                    </div>
                                                <?php endif; ?>
                                                <a id="add-time-button" href="#" class="btn btn-primary">Add Time</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



            <div >
                <h4 class="title-legend pull-left">Products Ordered</h4>
                <a href="<?php echo \Uri::create('admin/order/edit/'.$order->id.'?step=2'); ?>" class="pull-right"><h4 class="title-legend">Edit Products</h4></a>
            </div>

            <table class="table table-striped table-bordered">
                <thead>
                <tr class="blueTableHead">
                    <th style="width: 100px;" class="center" scope="col">Code</th>
                    <th scope="col" style="width: 250px;">Product Name</th>
                    <th scope="col" style="width: 250px;">Attributes</th>
                    <th style="width: 80px;" class="center" scope="col">Price Type</th>
                    <th style="width: 100px;" class="center" scope="col">Delivery Time</th>
                    <th style="width: 100px;" class="center" scope="col">Unit Price</th>
                    <th style="width: 50px;" class="center" scope="col">Qty</th>
                    <th style="width: 100px;" class="center" scope="col">Total Price</th>
                </tr>
                </thead>
                <tbody>
                <?php $total = 0;?>
                <?php if(!empty($order->products)): $total = 0; ?>
                    <?php foreach($order->products as $product): ?>
                        <tr>
                            <?php
                            $total += ($product->price * $product->quantity);
                            ?>
                            <td class="center"><a href="<?php echo \Uri::create('admin/product/update/' . $product->product_id); ?>"><?php echo $product->code; ?></a></td>
                            <td><a href="<?php echo \Uri::create('admin/product/update/' . $product->product_id); ?>"><?php echo $product->title; ?></a></td>
                            <td><?php echo $get_attributes($product->attributes)? $get_attributes($product->attributes): 'N/A'; ?></td>



                            <?php
                            $artwork_table = '';
                            // Find artworks
                            if($artworks = $product->artwork)
                            {
                                $artwork_table .= '<table class="inner_table">';
                                $artwork_table .= '<tr><td>#</td><td>Name / Download</td><td>Qty</td><td>Url</td><td>Remove</td></tr>';

                                foreach ($artworks as $artwork)
                                {
                                    $artwork_table .= '<tr>';
                                    $artwork_table .= '<td>';
                                    $artwork_table .= $artwork->type;
                                    $artwork_table .= '</td>';
                                    $artwork_table .= '<td>';
                                    $artwork_table .= '<a href="' . \Uri::create('admin/order/download_artwork/' . $artwork->file_id) . '" target="_blank">';
                                    $artwork_table .= $artwork->name;
                                    $artwork_table .= '</a>';
                                    $artwork_table .= '</td>';
                                    $artwork_table .= '<td class="center">';
                                    $artwork_table .= $artwork->quantity;
                                    $artwork_table .= '</td>';
                                    $artwork_table .= '<td class="center">';
                                    $artwork_table .= '<a href="' . \Uri::create('admin/order/download_artwork_url/' . $artwork->file_id) . '" target="_blank">';
                                    $artwork_table .= '<i class="icon-download"></i>';
                                    $artwork_table .= '</a>';
                                    $artwork_table .= '</td>';
                                    $artwork_table .= '<td class="center">';
                                    $artwork_table .= '<a href="' . \Uri::create('admin/order/delete_artwork/' . $artwork->file_id) . '">';
                                    $artwork_table .= '<i class="icon-remove"></i>';
                                    $artwork_table .= '</a>';
                                    $artwork_table .= '</td>';
                                    $artwork_table .= '</tr>';
                                }
                                $artwork_table .= '</table>';
                            }

                            echo $artwork_table;

                            ?>

                            <td class="center"><?php echo $product->price_type=='sale_price'?\Inflector::humanize($product->price_type):''; ?></td>
                            <td class="center"><?php echo $order->delivery_datetime_list?(date('h:i a', strtotime($product->delivery_time))):($order->delivery_datetime?(date('h:i a', strtotime($order->delivery_datetime))):''); ?></td>
                            <td class="center">$<?php echo nf($product->price); ?></td>
                            <td class="center"><?php echo $product->quantity; ?></td>
                            <td class="center" style="text-align:right">$<?php echo nf($product->price * $product->quantity); ?></td>
                        </tr>
                        <?php if($product->packs): $packs = json_decode($product->packs, true); ?>
                            <?php foreach($packs as $pack): ?>
                                <tr>
                                    <td></td>
                                    <td colspan="3" style="padding-left: 30px;">
                                        <?php
                                        echo $pack['name'];
                                        if($pack['products'])
                                        {
                                            echo ' ( ';
                                            foreach ($pack['products'] as $key => $prdct)
                                            {
                                                echo $prdct['name'];
                                                if($key != (count($pack['products']) - 1))
                                                    echo ', ';
                                            }
                                            echo ' ) ';
                                        }
                                        ?>
                                    </td>
                                    <td colspan="4">
                                        <?php
                                        echo date('h:i a', strtotime($pack['time']));
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>


                <?php else: ?>
                    <tr><td colspan="9"><small class="alert alert-info">There are no items.</small></td></tr>
                <?php endif; ?>
                </tbody>
            </table>

            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Order Notes <span class="label-note">(Visible to the client)</span></h4>
                                </div>
                                <div class="panel-body">
                                    <?php echo \Form::textarea('notes', \Input::post('notes', $order->notes), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Delivery Notes <span class="label-note">(Visible to the client)</span></h4>
                                </div>
                                <div class="panel-body">
                                    <?php echo \Form::textarea('delivery_notes', \Input::post('delivery_notes', $order->delivery_notes), array('class' => 'form-control')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6" >
                    <table class="table table-striped table-bordered">
                        <tr class="order_total">
                            <td class="order_text">Products Total</td>
                            <td class="center order_price" style="text-align:right">$<span class="sub_total"><?php echo nf($total); ?></span></td>
                            <!--<td class="order_price"></td>-->
                        </tr>
                        <tr class="order_total">
                            <td class="order_text">Shipping</td>
                            <td class="center order_price" style="text-align:right">$<span class="gst"><?php echo nf($order->shipping_price); ?></span></td>
                        </tr>
                        <tr class="order_total">
                            <td class="order_text">Extra Delivery Charge</td>
                            <td class="center order_price" style="text-align:right">$<span class="gst"><?php echo nf($order->total_delivery_charge); ?></span></td>
                        </tr>
                        <tr class="order_total">
                            <td class="order_text">Discount</td>
                            <td class="center order_price" style="text-align:right">-$<span class="sub_total"><?php echo nf($order->discount_amount); ?></span></td>
                        </tr>
                        <?php
                        $hold_gst = (isset($settings['gst']) ? 1 : 0);
                        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
                        ?>
                        <tr class="order_total">
                            <td class="order_dark_text"><strong>Grand Total</strong></td>
                            <td class="center order_dark_price" style="text-align:right"><strong class="grand_total_box">$<span class="grand_total"><?php echo nf($order->total_price()); ?></span></strong></td>
                        </tr>
                        <?php if($hold_gst && $hold_gst_percentage > 0): ?>
                            <tr class="order_total">
                                <?php $final_gst = \Helper::calculateGST($order->total_price(), $hold_gst_percentage); ?>
                                <td class="order_text">GST Tax <?php echo $hold_gst_percentage; ?>% (Included)</td>
                                <td class="center order_price" style="text-align:right">$<span class="gst"><?php echo nf($final_gst); ?></span></td>
                            </tr>
                        <?php endif;?>
                    </table>
                </div>
            </div>
            <h4 class="title-legend">Client Notes</h4>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Notes <?php echo '- ' . $order->first_name. ' ' . $order->last_name; ?>  <span class="label-note">(Not visible to the client)</span></h3>
                </div>
                <div class="panel-body">
                    <div class="notes">
                        <?php if($notes): ?>
                            <?php foreach($notes as $note): ?>
                                <div class="note">
                                    <div class="note-indicator"></div>
                                    <div class="note-item-content">
                                        <div class="note-content" style="white-space: pre-wrap;"><?php echo $note->note; ?></div>
                                        <div class="note-footer">
                                            <span class="note-updated"> <?php echo date('d/m/Y h:i:s a', $note->created_at); ?> </span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <small class="alert alert-info">There are no items.</small>
                        <?php endif; ?>
                    </div>
                    <div class="pagination-holder">
                        <?php echo $pagination->render(); ?>
                    </div>
                </div>


            </div>

            <div class="save_button_holder text-right">
                <button id="save_order_notes" name="save" value="1" class="btn btn-success" type="submit" style="display:none"><i class="icon-ok icon-white"></i> Save</button>
            </div>

            <?php echo \Form::close(); ?>



            <div class="tab-l2">
                <?php if($order->status == 'pending' || $order->status == 'new'): ?>
                    <div class="pull-right">
                        <?php echo \Theme::instance()->view('views/order/_action_links', array('save' => 0, 'hide_show_all' => 0, 'hide_add_new' => 0, 'approved_order' => \Uri::create('admin/order/approved/'.$order->id) )); ?>
                    </div>
                <?php endif; ?>


                <?php
                $partial_payment_list = \Payment\Model_Payment::find(function($query) use($order){
                    $query->where('order_id', $order->id);
                    $query->where('partial_payment', 1);
                    $query->order_by('updated_at', 'desc');
                });
                $partial_payment_completed_sum = \DB::select(\DB::expr('SUM(total_price) as payment_sum'))
                    ->from('payments')
                    ->where('order_id', $order->id)
                    ->where('status', 'completed')
                    ->where('partial_payment', 1)
                    ->execute();
                ?>

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#downloademails" aria-controls="home" role="tab" data-toggle="tab">Downloadable and emails</a>
                    </li>
                    <?php if($partial_payment_list): ?>
                        <li role="presentation">
                            <a href="#payments" aria-controls="payments" role="tab" data-toggle="tab">Payments <span class="label-note">(Amount Paid : <?php if($payment_sum = $partial_payment_completed_sum[0]['payment_sum']) echo '$'.number_format(round($payment_sum, 2), 2).'<input type="hidden" name="pp_total_paid_amount" value="'.$payment_sum.'">'; else echo '$0.00'; ?>)</span></a>
                        </li>
                    <?php endif; ?>
                    <li role="presentation">
                        <a href="#historylog" aria-controls="historylog" role="tab" data-toggle="tab">Log <span class="label-note">(Number of events: <?php echo count($order->history); ?>)</span></a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="downloademails">

                        <h4 class="title-legend">Downloadable and emails</h4>

                        <div class="panel panel-default">
                            <?php
                            echo \Theme::instance()->view('views/order/_modal_send_emails', array(
                                'order' => $order
                            ));
                            ?>


                            <!--h4 class="title-legend">Delivery Docket</h4>
                            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data'), array('upload_type' => 'delivery_docket')); ?>
                                <table class="sortable table table-striped table-bordered">
                                    <tr class="nodrop nodrag blueTableHead">
                                        <th scope="col" class="noresize">File</th>
                                        <th scope="col">File Properties</th>
                                    </tr>

                                    <tr class="nodrop nodrag">
                                        <td class="td-thumb">
                                            <div class="fa fa-file-text-o"></div>
                                        </td>
                                        <td class="upload">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <?php if($order->delivery_docket_file && is_file(\Config::get('details.file.location.root') . $order->delivery_docket_file)): ?>
                                                    <tr>
                                                        <td class="noresize">Current File</td>
                                                        <td>
                                                            <div class="input_holder">
                                                                <a href="<?php echo \Uri::create('media/documents/' . $order->delivery_docket_file); ?>" class="btn btn-primary" target="_blank">View File <?php echo $order->delivery_docket_title ? $order->delivery_docket_title : '' ; ?></a>
                                                                <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete file?" href="<?php echo \Uri::create('admin/order/delete_file/delivery_docket/' . $order->id) ?>" class="btn btn-danger">Delete</a><br>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php endif; ?>
                                                <tr>

                                                    <td valign="top">Title</td>
                                                    <td>
                                                        <div class="input_holder">
                                                            <?php echo \Form::input('delivery_docket_title', \Input::post('delivery_docket_title'), array('class'=>'form-control')); ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">Choose File</td>
                                                    <td>
                                                        <?php echo \Form::file('file'); ?>
                                                        <?php if($order->delivery_docket_file && is_file(\Config::get('details.file.location.root') . $order->delivery_docket_file)): ?>
                                                            <a href="<?php echo \Uri::create('admin/order/send_email/' . $order->user_id . '/' . 'delivery_docket' . '/' . $order->id); ?>" class="btn btn-primary">Send to Customer</a>
                                                        <?php endif; ?>

                                                        <input type="submit" name="upload" value="Upload" class="btn btn-primary mt-5px right">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            <?php echo \Form::close(); ?>--><!-- EOF Document File Delivery Docket -->


                        </div>
                    </div>


                    <?php if($partial_payment_list): ?>


                        <div role="tabpanel" class="tab-pane" id="payments">
                            <h4 class="title-legend">Payments</h4>
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr class="blueTableHead">
                                    <th scope="col">ID</th>
                                    <th scope="col">Gateway</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($partial_payment_list as $payment): ?>
                                    <tr>
                                        <td><?php echo $payment->id; ?></td>
                                        <td><?php echo Inflector::humanize($payment->method); ?></td>
                                        <td>$<?php echo number_format(round($payment->total_price, 2), 2); ?></td>
                                        <td><?php echo Inflector::humanize($payment->status); ?></td>
                                        <td><?php echo date('d/m/Y', $payment->updated_at).'<br />'.date('H:i:s', $payment->updated_at); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>

                    <?php endif; ?>
                    <div role="tabpanel" class="tab-pane" id="historylog">
                        <h4 class="title-legend">Log</h4>

                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr class="blueTableHead">
                                <th class="center" scope="col">Date</th>
                                <th class="center" scope="col">User</th>
                                <th class="center" scope="col">Event</th>
                                <th class="center" scope="col">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($order->history)): ?>
                                <?php foreach ($order->history as $event): ?>
                                    <tr>
                                        <td class="noresize">
                                            <?php echo date('d/m/Y H:i:s', $event->created_at); ?>
                                        </td>
                                        <td class="noresize">
                                            <?php echo $get_user($event->user_created)? $get_user($event->user_created)->email: 'N/A id:[' . $event->user_created . ']'; ?>
                                        </td>
                                        <td>
                                            <?php echo $event->event; ?>
                                        </td>
                                        <td class="icon center">
                                            <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete order history?" href="<?php echo \Uri::create('admin/order/delete_history/' . $event->id) ?>"><span class="fa fa-remove"></span> Delete</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <tr>
                                    <td colspan="4">
                                        Order history is empty.
                                    </td>
                                </tr>
                            <?php endif; ?>

                            </tbody>
                        </table>


                    </div>
                </div>

            </div>



            <div class="form-footer-actions">
                <a class="btn btn-danger text-danger confirmation-pop-up" data-message="Are you sure you want to delete order?" href="<?php echo \Uri::create('admin/order/delete/' . $order->id); ?>" class="text-danger" rel="tooltip" data-original-title="Delete">Delete</a>
            </div>

        </div>


    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $('.country_select').on('change', function(){

            var $this = $(this);
            var $state_select = $this.attr('data-state');

            $.ajax({
                url: uri_base + '/order/get_states/' + $this.val() ,
                success: function(data){
                    el = $('.' + $state_select);
                    el.select2('destroy');
                    $('select.' + $state_select).html(data);
                    $('select.' + $state_select).select2();
                },
                dataType: 'html'
            });
        });

        $('#same_as_billing').click(function(e){

            if($(this).is(':checked'))
            {
                $('select[name="shipping_title"]').val($('select[name="title"]').val());
                $('input[name="shipping_first_name"]').val($('input[name="first_name"]').val());
                $('input[name="shipping_last_name"]').val($('input[name="last_name"]').val());
                $('input[name="shipping_company"]').val($('input[name="company"]').val());
                $('input[name="shipping_address"]').val($('input[name="address"]').val());
                $('input[name="shipping_suburb"]').val($('input[name="suburb"]').val());
                $('select[name="shipping_country"]').val($('select[name="country"]').val());
                $('select[name="shipping_state"]').val($('select[name="state"]').val());
                $('input[name="shipping_postcode"]').val($('input[name="postcode"]').val());
                $('input[name="shipping_phone"]').val($('input[name="phone"]').val());
            }
            else
            {
                $('select[name="shipping_title"]').val($('select[name="shipping_title"]').attr('data-original'));
                $('input[name="shipping_first_name"]').val($('input[name="shipping_first_name"]').attr('data-original'));
                $('input[name="shipping_last_name"]').val($('input[name="shipping_last_name"]').attr('data-original'));
                $('input[name="shipping_company"]').val($('input[name="shipping_company"]').attr('data-original'));
                $('input[name="shipping_address"]').val($('input[name="shipping_address"]').attr('data-original'));
                $('input[name="shipping_suburb"]').val($('input[name="shipping_suburb"]').attr('data-original'));
                $('select[name="shipping_country"]').val($('select[name="shipping_country"]').attr('data-original'));
                $('select[name="shipping_state"]').val($('select[name="shipping_state"]').attr('data-original'));
                $('input[name="shipping_postcode"]').val($('input[name="shipping_postcode"]').attr('data-original'));
                $('input[name="shipping_phone"]').val($('input[name="shipping_phone"]').attr('data-original'));
            }
        });

        var time_list = [];
        var get_hourtime_options = '';
        $('select[name="delivery_time_hourminute"]').find('option').each(function(index, elem){
            get_hourtime_options += $(elem).get(0).outerHTML.replace('selected="selected"', '');
        });
        // var get_hour_options = '';
        // $('select[name="delivery_timehour"]').find('option').each(function(index, elem){
        //     get_hour_options += $(elem).get(0).outerHTML.replace('selected="selected"', '');
        // });
        // var get_minute_options = '';
        // $('select[name="delivery_timeminute"]').find('option').each(function(index, elem){
        //     get_minute_options += $(elem).get(0).outerHTML.replace('selected="selected"', '');
        // });

        var countTime = <?php echo $counter_time; ?>;

        for(var e = 1; e <= countTime; e++)
        {
            if($('select[data-order="'+e+'"]').val() != '-')
                time_list.push($('select[data-order="'+e+'"]').val());
            else
                time_list.push('Time '+e);
        }

        $("#add-time-button").click(function(e){
            e.preventDefault();

            countTime += 1;

            var select_time_hourminute = '<select data-order="'+countTime+'" class="form-control additional-time-select" style="margin-left: -10px;margin-bottom:5px;display: inline;width:150px;padding: 5px;margin-right: 5px;">';
            select_time_hourminute += get_hourtime_options;
            select_time_hourminute += '</select>';

            // var select_hour = '<select data-order="'+countTime+'" data-type="hour" class="form-control additional-time-select" style="margin-left: 5px;margin-bottom:5px;display: inline;width:90px;padding: 5px;margin-right: 5px;">';
            // select_hour += get_hour_options;
            // select_hour += '</select>';

            // var select_minute = '<select data-order="'+countTime+'" data-type="minute" class="form-control additional-time-select" style="margin-bottom:5px;display: inline;width:90px;margin-left: 5px;padding: 5px;margin-right: 5px;">';
            // select_minute += get_minute_options;
            // select_minute += '</select>';

            var time = 'Time '+countTime;
            $('#hold-additional-time').append('<div data-order="'+countTime+'" class="additional-time-group" style="display:inline-block;"><label style="margin-left: 10px;"></label>'+select_time_hourminute+' <a class="remove-time" href="#"><i class="fa fa-close"></i></a></div>');

            time_list.push(time);
            generate_delivery_datetime_list();
        });
        $( "div" ).on( "click", "a.remove-time", function(e) {
            e.preventDefault();
            var getOrder = $(this).closest('div.additional-time-group').data('order');
            time_list.splice((getOrder-1), 1);
            $(this).closest('div.additional-time-group').remove();
            $('select[name*="product_time"]').each(function(index, elem){
                $(elem).find('option').eq(getOrder-1).remove();
            });
            countTime -= 1;
            var count = 2;
            $('#hold-additional-time').find('div.additional-time-group').each(function(index, elem){
                $(elem).attr('data-order', count);
                $(elem).find('select.additional-time-select').each(function(ind, ele){
                    $(ele).attr('data-order', count);
                });
                count += 1;
            });
            generate_delivery_datetime_list();
        });
        $( "div" ).on( "change", "select.additional-time-select", function(e) {
            var getOrder = $(this).data('order');
            if(time_list.length)
            {
                if($('select[data-order="'+getOrder+'"].additional-time-select').val() != '-')
                {
                    var time = $('select[data-order="'+getOrder+'"].additional-time-select').val();
                    time_list[getOrder-1] = time;
                }
                else
                {
                    time_list[getOrder-1] = 'Time '+getOrder;
                }
            }
            generate_delivery_datetime_list();
        });

        function generate_delivery_datetime_list()
        {
            if(time_list.length)
            {
                var datetime_list = '';
                for(var i=0; i < time_list.length; i++)
                {
                    if(time_list[i].indexOf(':') > -1)
                        datetime_list += time_list[i]+';';
                }
                datetime_list = datetime_list.substring(0, datetime_list.length - 1);
                $('input[name="delivery_datetime_list"]').val(datetime_list);
            }
        }
    });
</script>




