<style type="text/css">
	.content .panel{
		width: 99%;
	}
	input[type=email]{
		width: 96%;
	}

	#step_guide li.active a {
		color: #0088cc;
	}

	#loading_area_item img, #loading_area_discount img, #loading_area_customer img  {
		width: 20px;
	}

	#loading_area_discount {
		float: right;
		display: inline-block;
	}

	#loading_area_customer {
		display: inline-block;
		margin-left: 10px;
	}
</style>
<?php
$app_countries = \App\Countries::forge();
$countries = $app_countries->getCountries();
$app_states = \App\States::forge();
$states = $app_states->getStateProvinceArray('AU');
$payment_settings = \Config::load('payment_settings.db');
$hold_enable_partial_payment = (isset($payment_settings['enable_partial_payment']) ? $payment_settings['enable_partial_payment'] : 0);
?>

<div class="main-content">

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Orders');
		\Breadcrumb::set('Manage Orders', 'admin/order/list');
		\Breadcrumb::set('Add New');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner">
			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid', 'id' => 'add_order_form')); ?>

			<header class="main-content-heading">
				<div class="pull-left">
					<h4>Add Order</h4>
				</div>

				<div class="pull-right">
					<ul class="list-inline">
						<li style="vertical-align: top;">
							<div class="row" style="width:300px">
								<label class="col-sm-4 control-label text-right" style="padding-right:0;">Status</label>
								<div class="col-sm-8">
									<?php echo \Form::select('status', \Input::post('status', 'approved'), \Config::get('details.status', array()), array('class' => 'form-control')); ?>
								</div>
							</div>
						</li>
					</ul>


					<?php //echo \Theme::instance()->view('views/order/_action_links', array('hide_add_new'=>1)); ?>

				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/_navbar_links'); ?>

			<div class="panel panel-default">


				<div class="panel-heading">
					<h3 class="panel-title pull-left">Details</h3>




				</div>
				<div class="panel-body">

					<input type="hidden" name="partial_payment" value="<?php echo $hold_enable_partial_payment; ?>">

					<!-- Main Content Holder -->
					<div class="content">

						<ul id="step_guide" class="nav nav-tabs" role="tablist" id="tabbed-step" >
							<li id="step_one_tab" class="<?php echo (\Input::get('step') == 3 || \Input::get('step') == 2) ? 'disabled is_disabled' : 'active is_disabled';?>" role="presentation">
								<a href="#step_one" role="tab" data-toggle="tab"><span>Step 1: Order Details</span></a>
							</li>
							<li id="step_two_tab" class="<?php echo (\Input::get('step') == 2) ? 'active is_disabled' : 'disabled is_disabled';?>"  role="presentation">
								<a href="#step_two" role="tab" data-toggle="tab"><span>Step 2: Products</span></a>
							</li>
							<li id="step_three_tab" class="<?php echo (\Input::get('step') == 3) ? 'active is_disabled' : 'disabled is_disabled';?>"  role="presentation" >
								<a href="#step_three" role="tab" data-toggle="tab"><span>Step 3: Payment</span></a>
							</li>
							<li class="pull-right">
								<button id="back_button_top" class="btn btn-default" value="1" >Back</button>
								<button id="continue_button_top" class="btn btn-primary" value="1" >Continue</button>
								<?php echo \Form::button('save_order_only_button', '<i class="fa fa-edit"></i> Submit Order', array('id'=> '','class' => 'save_order_only_button btn btn-success', 'type' => 'submit', 'value' => '1')); ?>
							</li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active <?php echo (\Input::get('step') == 3 || \Input::get('step') == 2) ? '' : 'active';?>" id="step_one">

								<br>


								<div class="row">
									<div class="col-sm-6">
										<div class="row form-group">
											<label class="col-sm-3 control-label">&nbsp;</label>
											<div class="col-sm-9">
												<h3 class="title-legend">Select Customer</h3>
											</div>
										</div>
									</div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">&nbsp;</label>
                                            <div class="col-sm-9">
                                                <h3 class="title-legend">Order Details</h3>
                                            </div>
                                        </div>
                                    </div>
								</div>

								<div class="row form-horizontal">
									<div class="col-sm-6">

										<div class="form-group">
											<label class="col-sm-3 control-label">Customer Name: <span class="text-danger">*</span></label>
											<div class="col-sm-9">
												<div class="form-search">
													<select name="customer_id"></select><div class="is-loading" id="loading_area_customer"></div>
												</div>
											</div>
										</div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Email Address:</label>
                                            <div class="col-sm-9">
                                                <input class="form-control customer_details" id="customer_details_email" disabled>
                                            </div>
                                        </div>

									</div>
									<div class="col-sm-6">
                                        <div class="form-group ">
                                            <label class="col-sm-3 control-label">Cater For</label>
                                            <div class="col-sm-9">
                                                <?php echo \Form::input('cater_for', \Input::post('cater_for'), array('class' => 'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label class="col-sm-3 control-label">Client PO</label>
                                            <div class="col-sm-9">
                                                <?php echo \Form::input('client_po', \Input::post('client_po'), array('class' => 'form-control')); ?>
                                            </div>
                                        </div>
									</div>
								</div>

								<hr>

								<div class="row form-horizontal">
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-3 control-label">&nbsp;</label>
											<div class="col-sm-9">
												<h3 class="title-legend">Billing Details</h3>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Title:</label>
											<div class="col-sm-9">
												<div class="row">
													<div class="col-sm-6">
														<input class="form-control customer_details" id="customer_details_title" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Name:</label>
											<div class="col-sm-9">
												<div class="row">
													<div class="col-sm-6">
														<input class="form-control customer_details" id="customer_details_first_name" placeholder="First Name" disabled>
													</div>
													<div class="col-sm-6">
														<input class="form-control customer_details" id="customer_details_last_name" placeholder="Last Name"  disabled>
													</div>
												</div>

											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Company:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_company" disabled>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Address:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_address"  placeholder="Address"  disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Address 2:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_address2"  placeholder="Address 2"  disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Suburb:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_suburb" disabled>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">&nbsp;</label>
											<div class="col-sm-9">
												<div class="row">
													<div class="col-sm-6">
														<input class="form-control customer_details" id="customer_details_state" placeholder="State" disabled>
													</div>
													<div class="col-sm-6">
														<input class="form-control customer_details" id="customer_details_postcode" placeholder="Postcode" disabled>
													</div>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Country:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_country" disabled>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Telephone:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_phone" disabled>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Order Notes:</label>
											<div class="col-sm-9">
												<textarea class="form-control" id="customer_details_notes" name="notes"></textarea>
											</div>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<label class="col-sm-3 control-label">&nbsp;</label>
											<div class="col-sm-9">
												<h3 class="title-legend pull-left">Shipping Details</h3>
												<label class="control-label pull-right" style="margin-top:5px;"><input type="checkbox" id="same_as_billing" checked> Same as Billing</label>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Title:</label>
											<div class="col-sm-9">
												<div class="row">
													<div class="col-sm-6">
														<?php echo \Form::select('shipping_title', \Input::post('shipping_title', ''), \Config::get('user.titles', array()), array('class' => 'form-control', 'id' => 'customer_details_shipping_title')); ?>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Name:</label>
											<div class="col-sm-9">
												<div class="row">
													<div class="col-sm-6">
														<input class="form-control customer_details" id="customer_details_shipping_first_name" name="shipping_first_name" placeholder="First Name">
													</div>
													<div class="col-sm-6">
														<input class="form-control customer_details" id="customer_details_shipping_last_name" name="shipping_last_name" placeholder="Last Name">
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Company:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_shipping_company" name="shipping_company" >
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Address:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_shipping_address" name="shipping_address" placeholder="Address">

											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Address 2:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_shipping_address2" name="shipping_address2" placeholder="Address 2">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Suburb</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_shipping_suburb" name="shipping_suburb" placeholder="Suburb">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">&nbsp;</label>
											<div class="col-sm-9">
												<div class="row">
													<div class="col-sm-6">
														<?php echo \Form::select('shipping_state', \Input::post('shipping_state', ''), is_array($states) ? array('' => 'Select State') + $states : array('' => '-'), array('class' => 'form-control customer_details', 'id' => 'customer_details_shipping_state','placeholder'=>'State')); ?>
													</div>
													<div class="col-sm-6">
														<input class="form-control customer_details" id="customer_details_shipping_postcode" name="shipping_postcode" placeholder="Postcode">
													</div>
												</div>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Country:</label>
											<div class="col-sm-9">
												<?php echo \Form::select('shipping_country', \Input::post('shipping_country', ''), array('' => 'Select country') + $countries, array('class' => 'form-control customer_details', 'data-state' => 'shipping_state', 'id' => 'customer_details_shipping_country')); ?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-3 control-label">Telephone:</label>
											<div class="col-sm-9">
												<input class="form-control customer_details" id="customer_details_shipping_phone" name="shipping_phone" placeholder="Telephone" >
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-3 control-label">Delivery Notes:</label>
											<div class="col-sm-9">
												<textarea class="form-control" id="customer_delivery_notes" name="delivery_notes"></textarea>
											</div>
										</div>
									</div>
								</div>


								<div class="save_button_holder text-right">
									<?php echo \Form::button('continue', 'Continue', array('id'=> 'continue_button_down', 'class' => 'btn btn-primary', 'value' => '1')); ?>
								</div>

							</div>
							<div role="tabpanel" class="tab-pane <?php echo (\Input::get('step') == 2) ? 'active' : '';?>" id="step_two">
								<div class="row">
									<div class="col-sm-9">
										<h3 class="title-legend">Products</h3>

										<div id="add_item_form">
											<div class="row">
												<?php
												$delivery_date = '';
												$delivery_timehour = '';
												$delivery_timeminute = '';
												$delivery_time_hourminute = '';
												?>
												<div class="col-sm-12">
													<label>Delivery Date and Time:  <span class="text-danger"></span></label>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<label class="datepicker-holder-control" style="width: 100%;"><?php echo \Form::input('delivery_date', $delivery_date, array('class' => 'form-control deliveryDate')); ?></label>
														<input type="hidden" name="delivery_datetime_list" >
													</div>
												</div>
												<div class="col-sm-9">
													<div class="form-group">
														<?php //echo \Form::select('delivery_time_hourminute', $delivery_time_hourminute, \Config::get('details.time.hour_list'), array('data-order' => 1,'data-type' => 'hour','class' => 'form-control additional-time-select', 'style' => 'padding: 5px;margin-bottom: 5px;display: inline;width: 110px;')); ?>
														<?php /*echo \Form::select('delivery_timehour', $delivery_timehour, \Config::get('details.time.hours'), array('data-order' => 1,'data-type' => 'hour','class' => 'form-control additional-time-select', 'style' => 'padding: 5px;margin-bottom: 5px;display: inline;width: 90px;')); ?>
					                                    <?php echo \Form::select('delivery_timeminute', $delivery_timeminute, \Config::get('details.time.minutes'), array('data-order' => 1,'data-type' => 'minute','class' => 'form-control additional-time-select', 'style' => 'padding: 5px;margin-bottom: 5px;display: inline;width: 90px;margin-left: 5px;'));*/ ?>

														<?php
														echo \Form::input('delivery_time_hourminute', \Input::post('delivery_time_hourminute', '12:00 PM' ), array('class' => 'form-control time_field', 'style'=>"padding: 5px;margin-bottom: 5px;display: inline;width: 110px;"));
														?>

														<div id="hold-additional-time" style="display:inline;">
														</div>
														<a id="add-time-button" href="#" class="btn btn-primary">Add Time</a>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-5">
													<div class="form-group">
														<?php echo \Form::hidden('product_id'); ?>
														<?php echo \Form::hidden('product_code'); ?>
														<?php echo \Form::hidden('attributeid'); ?>
														<?php echo \Form::hidden('uri',\Uri::create('product/product_data/')); ?>
														<input type="hidden" id="minimum_order">
														<label>Products:  <span class="text-danger">*</span></label>
														<div class="input_holder">
															<?php
															$products = \Product\Model_Product::find(function($query){
																$table_name = 'product';
																$query->where(function($query) use ($table_name){
																	$query->where($table_name . '.status', 1);
																	$query->or_where(function($query) use ($table_name){
																		$query->where($table_name . '.status', 2);
																		$query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
																		$query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
																	});
																});
																$query->order_by('id', 'asc');
															});

															$product_list = array('' => 'Please select product');
															foreach ($products as $product)
															{
																$product_list += array($product->id => $product->title);
															}
															echo \Form::select('product', \Input::post('product'),  $product_list, array('class' => 'form-control state require'));
															?>
															<div id="product-attributes-edit-holder"></div>
															<div id="product-pack-options-edit-holder"></div>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group">
														<label>Price:  <span class="text-danger">*</span></label>
														<div class="input_holder">
															<?php
															echo \Form::input('price', \Input::post('price', '' ), array('class' => 'form-control require'));
															?>
														</div>
													</div>
												</div>
												<div class="col-sm-2">
													<div class="form-group">
														<label>Quantity:  <span class="text-danger">*</span></label>
														<div class="input_holder">
															<?php echo \Form::input('quantity', \Input::post('quantity', 1), array('class' => 'form-control require','type' => 'number')); ?>
														</div>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<label>&nbsp;</label>
														<div class="form-search" id="add-product-group">
															<?php echo \Form::button('add', 'Add', array('id' => 'add_item_button', 'class' => 'btn btn-primary btn-block', 'value' => '1')); ?>
															<span class="is-loading text-white" id="loading_area_item"></span>
														</div>
														<div class="form-search hide" id="edit-product-group">
															<?php echo \Form::button('save', 'Save', array('id' => 'save_item_button', 'class' => 'btn btn-success ', 'value' => '1')); ?>
															<?php echo \Form::button('cancel', 'Cancel', array('id' => 'cancel_item_button', 'class' => 'btn btn-primary ', 'value' => '1')); ?>
														</div>

													</div>
												</div>


											</div>
											<div class="row">
												<div id="prod-attribute-list" class="col-sm-5">
												</div>
												<div id="pack-options" class="col-sm-7">
												</div>
											</div>
										</div>
										<table id="item_list_table" class="table table-striped table-bordered">
											<thead>
											<tr class="blueTableHead">
												<th scope="col" style="width: 135px;" class="center" scope="col">Delivery Time</th>
												<th scope="col">Product Name</th>
												<th scope="col">Product Code</th>
												<th style="width: 100px;" class="center" scope="col">Unit Price</th>
												<th style="width: 100px;" class="center" scope="col">Qty</th>
												<th style="width: 100px;" class="center" scope="col">Total Price</th>
												<!--th style="width: 100px;" class="center" scope="col">Delivery Time</th-->
												<th style="width: 50px;" class="center" scope="col">Edit</th>
												<th style="width: 50px;" class="center" scope="col">Delete</th>
											</tr>
											</thead>
											<tbody>
											<tr id="no_items"><td colspan="8">There are no items.</td></tr>
											</tbody>
										</table>
									</div>
									<div class="col-sm-3">
										<h3 class="title-legend">Order Summary</h3>
										<div class="form-group">
											<label>
												Discount Code <div id="loading_area_discount"></div>
											</label>
											<input id="discount_action" type="hidden" value="Apply" name="discount_action" data-code="" data-value="">
											<?php echo \Form::input('discount_code', \Input::post('discount_code'), array('class' => 'form-control')); ?>
											<?php echo \Form::button('discount_button', 'Apply', array('id'=> '', 'class' => 'btn btn-primary mt-5px', 'value' => '1')); ?>
										</div>
										<div class="form-group">
											<?php echo \Form::label('Products Total'); ?>
											<?php echo \Form::input('products_total', false, array('class' => 'form-control', 'disabled')); ?>
											<input type="hidden" name="products_total_hidden">
										</div>
										<div class="form-group">
											<?php echo \Form::label('Shipping'); ?>
											<?php echo \Form::input('shipping', false, array('class' => 'form-control')); ?>
											<input type="hidden" name="shipping_hidden">
										</div>
										<div class="form-group">
											<?php echo \Form::label('Extra Delivery Charge'); ?>
											<div class="input_holder">
												<?php echo \Form::input('delivery_charge', false, array('class' => 'form-control', 'disabled')); ?>
												<input type="hidden" name="delivery_charge_hidden">
											</div>
										</div>
										<div class="form-group">
											<?php echo \Form::label('Discount'); ?>
											<?php echo \Form::input('discount', false, array('class' => 'form-control', 'disabled')); ?>
											<input type="hidden" name="discount_hidden">
										</div>
										<div class="form-group">
											<?php echo \Form::label('Grand Total'); ?>
											<?php echo \Form::input('grand_total', false, array('class' => 'form-control', 'disabled')); ?>
											<input type="hidden" name="grand_total_hidden">
										</div>
										<?php
										$hold_gst = (isset($settings['gst']) ? 1 : 0);
										$hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
										?>
										<?php if($hold_gst && $hold_gst_percentage > 0): ?>
											<input type="hidden" id="gst_percentage" value="<?php echo $settings['gst_percentage'];?>"/>
											<div class="form-group">
												<?php echo \Form::label('GST Tax '.$hold_gst_percentage.'% (Included)'); ?>
												<div class="input_holder">
													<?php echo \Form::input('gst_amount', false, array('class' => 'form-control', 'disabled')); ?>
													<input type="hidden" name="gst_amount_hidden">
												</div>
											</div>
										<?php endif;?>
									</div>
								</div>

								<div class="save_button_holder text-right">
									<input type="hidden" name="create_form" value="1">
									<?php echo \Form::button('back', 'Back', array('id'=> 'back_button_down', 'class' => 'btn btn-default', 'value' => '1')); ?>
									<?php echo \Form::button('continue', 'Continue', array('id'=> 'continue_button_down2', 'class' => 'btn btn-primary', 'value' => '1')); ?>
									<?php echo \Form::button('save_order_only_button', '<i class="fa fa-edit"></i> Submit Order', array('id'=> '','class' => 'save_order_only_button btn btn-success', 'type' => 'submit', 'value' => '1')); ?>


								</div>
							</div>

							<div role="tabpanel" class="tab-pane <?php echo (\Input::get('step') == 3) ? 'active' : '';?>" id="step_three">

								<div class="row">
									<div class="col-sm-6">
										<div class="form-horizontal">

											<div class="form-group">
												<label class="col-sm-4 control-label">&nbsp;</label>
												<div class="col-sm-8">
													<h3 class="title-legend">Payment</h3>
												</div>
											</div>

											<div class="form-group ">
												<label class="col-sm-4 control-label">Payment Method</label>
												<div class="col-sm-8">
													<input type="hidden" name="payment_type" id="payment_type">
													<?php echo \Form::select('payment_method', \Input::post('payment_method'), \Config::get('details.payment_method', array()), array('class' => 'form-control')); ?>
												</div>
											</div>
											<input type="hidden" name="payment_status" value="completed">
											<?php if($hold_enable_partial_payment): ?>
												<div id="partial-payment-section">
													<div class="form-group ">
														<label class="col-sm-4 control-label">Amount</label>
														<div class="col-sm-8">
															<input class="form-control" name="ppamount" required="" type="number" min="1" step="0.01">
														</div>
													</div>
												</div>
											<?php endif; ?>
											<div id="total-payment-section">

											</div>
											<div class="text-right">
												<?php echo \Form::button('create', 'Add Payment', array('id'=> 'save_button_down', 'id'=> 'save_button_down', 'type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-primary', 'value' => '1')); ?>
											</div>
										</div>
									</div>
								</div>

								<div class="save_button_holder text-right">
									<input type="hidden" name="payment_form" value="1" disabled>
									<?php echo \Form::button('back', 'Back', array('id'=> 'back_button_down2', 'class' => 'btn btn-default', 'value' => '1')); ?>
									<?php echo \Form::button('save_order_only_button', '<i class="fa fa-edit"></i> Submit Order', array('id'=> '','class' => 'save_order_only_button btn btn-success', 'type' => 'submit', 'value' => '1')); ?>
								</div>
							</div>
						</div>
					</div><!-- EOF Main Content Holder -->

					<div class="clear"></div>
				</div>


			</div>
			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
<?php
echo \Theme::instance()->asset->css('plugins/select2-4.0.3.min.css');
echo \Theme::instance()->asset->js('plugins/select2-4.0.3.min.js');

echo \Theme::instance()->asset->js('plugins/jquery.plugin.js');
echo \Theme::instance()->asset->js('plugins/jquery.timeentry.js');
?>

<div id="product_time_template" style="display:none;">
	<select name="delivery_time[]" class="form-control" style="width:110px;display:inline;">
	</select>
</div>
<script type="text/javascript">
	var customer_list = [];
	var filtered_items = [], items = [], shipping_value = 0;
	var securePayCode = '', eWayCode = '';
	<?php
	// Get all users and remove admin users from array
	$users = \SentryAdmin::user()->all('front');

	// get object
	foreach($users as $key=> $user)
	{
		$users[$key] = \SentryAdmin::user((int)$user['id']);
	}

	// filter only activated accounts
	foreach($users as $number => $user)
	{
		if($user->activated != '1') unset($users[$number]);
	}

	foreach ($users as $user)
	{
	?>
	customer_list.push({id: <?php echo $user->id; ?>, text: "<?php echo $user->get('metadata.first_name').' '.$user->get('metadata.last_name'); ?>" });
	<?php
	}

	if(isset(\Config::load('securePay.db')['code']))
	{
	?>
	securePayCode = "<?php echo \Config::load('securePay.db')['code']; ?>";
	<?php
	}
	if(isset(\Config::load('eWay.db')['code']))
	{
	?>
	eWayCode = "<?php echo \Config::load('eWay.db')['code']; ?>";
	<?php
	}
	?>
	$(document).ready(function(){
		var time_list = [];
		var get_hour_options = '';
		var get_hourtime_options = '';
		$('select[name="delivery_time_hourminute"]').find('option').each(function(index, elem){
			get_hourtime_options += $(elem).get(0).outerHTML.replace('selected="selected"', '');
		});
		// $('select[name="delivery_timehour"]').find('option').each(function(index, elem){
		//     get_hour_options += $(elem).get(0).outerHTML.replace('selected="selected"', '');
		// });
		// var get_minute_options = '';
		// $('select[name="delivery_timeminute"]').find('option').each(function(index, elem){
		//     get_minute_options += $(elem).get(0).outerHTML.replace('selected="selected"', '');
		// });
		update_time_list();
		generate_delivery_datetime_list();
		$('.time_field').timeEntry({spinnerImage: '', ampmPrefix: ' '}).change(function(){
			update_time_list();
			generate_delivery_datetime_list();
		});

		var countTime = 1;
		$("#add-time-button").click(function(e){
			e.preventDefault();

			if(countTime == 1)
			{
				if($('input[name="delivery_time_hourminute"]').val() != '-')
					time_list.push($('input[name="delivery_time_hourminute"]').val());
				else
					time_list.push('Time '+countTime);
			}

			countTime += 1;

			var select_time_hourminute = "<input name='delivery_time_hourminute' value='12:00 PM' class='form-control time_field' style='padding: 5px;margin-bottom: 5px;display: inline;width: 110px;'/>";

			var time = 'Time '+countTime;
			$('#hold-additional-time').append('<div data-order="'+countTime+'" class="additional-time-group" style="display:inline-block;"><label style="margin-left: 10px;">and &nbsp;&nbsp;</label>'+select_time_hourminute+' <a class="remove-time" href="#"><i class="fa fa-close"></i></a></div>');
			$('.time_field').timeEntry({spinnerImage: '', ampmPrefix: ' '}).change(function() {
				update_time_list();
				generate_delivery_datetime_list();
			});
			time_list.push(time);
			update_time_list();
			generate_delivery_datetime_list();

		});


		$( "div" ).on( "click", "a.remove-time", function(e) {
			e.preventDefault();
			var getOrder = $(this).closest('div.additional-time-group').data('order');
			time_list.splice((getOrder-1), 1);
			$(this).closest('div.additional-time-group').remove();
			$('#product_time_template').find('select[name="delivery_time[]"]').find('option').eq(getOrder-1).remove();
			countTime -= 1;
			var count = 2;
			$('#hold-additional-time').find('div.additional-time-group').each(function(index, elem){
				$(elem).attr('data-order', count);
				$(elem).find('select.additional-time-select').each(function(ind, ele){
					$(ele).attr('data-order', count);
				});
				count += 1;
			});
			generate_delivery_datetime_list();

			// $('select[name="delivery_time[]"]').each(function(index, elem) {
			//     $(elem).find('option').each(function(ind, ele){

			//         if($(ele).attr('value').indexOf(':') < 0)
			//         {
			//             var getOrderOpt = ind+1;
			//             $(ele).attr('value', 'Time '+getOrderOpt);
			//             $(ele).html('Time '+getOrderOpt);
			//         }
			//     });
			// });
		});
		$( "div" ).on( "change", "select.additional-time-select", function(e) {
			var getOrder = $(this).data('order');
			if(time_list.length)
			{
				if($('select[data-order="'+getOrder+'"].additional-time-select').val() != '-')
				{
					var time = $('select[data-order="'+getOrder+'"].additional-time-select').val();
					time_list[getOrder-1] = time;
				}
				else
				{
					time_list[getOrder-1] = 'Time '+getOrder;
				}
			}
			else if(countTime == 1)
			{
				if($('select[name="delivery_timehour"]').val() != '-' && $('select[name="delivery_timeminute"]').val() != '-')
					time_list.push($('select[name="delivery_timehour"]').val()+':'+$('select[name="delivery_timeminute"]').val());
				else
					time_list.push('Time '+countTime);
			}
			generate_delivery_datetime_list();
		});

		function update_time_list(){
			time_list = [];
			$('input[name="delivery_time_hourminute"]').each(function(){
				if($(this).val() != ''){
					time_list.push($(this).val());
				}
			});
		}

		function generate_delivery_datetime_list()
		{
			if(time_list.length)
			{
				var datetime_list = '';
				var delivery_time_options = '';
				for(var i=0; i < time_list.length; i++)
				{
					if(time_list[i].indexOf(':') > -1)
					{
						if(datetime_list.indexOf(time_list[i]) < 0)
						{
							datetime_list += time_list[i]+';';
							delivery_time_options += '<option value="'+time_list[i]+'">'+time_list[i]+'</option>';
						}
					}
				}
				datetime_list = datetime_list.substring(0, datetime_list.length - 1);
				// $('input[name="delivery_datetime_list"]').val(datetime_list);
				$('#product_time_template').find('select[name="delivery_time[]"]').html(delivery_time_options);

				if($('#item_list_table').find('select[name="delivery_time[]"]').length)
				{
					$('#item_list_table').find('select[name="delivery_time[]"]').each(function(index, elem){
						var _time = $('#product_time_template select').clone();
						var get_index_selected = $(this).find("option:selected").index();
						_time.find('option').eq(get_index_selected).attr('selected', 'selected');
						$(this).html(_time.html());
					});

					$('#item_list_table').find('select[name*="pack_delivery_time"]').each(function(index, elem){
						var _time = $('#product_time_template select').clone();
						var get_index_selected = $(this).find("option:selected").index();
						_time.find('option').eq(get_index_selected).attr('selected', 'selected');
						_time.attr('name', $(this).attr('name'));
						$(this).html(_time.html());
					});
				}
			}
		}
	});
</script>
<?php echo \Theme::instance()->asset->js('order/create.js'); ?>
