<div class="top-filter-holder">
    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form')); ?>
    <?php
    isset($filters) or $filters = true;

    if($filters)
    {
        echo \Theme::instance()->view('views/_partials/search_filters', array(
            'pagination' => $pagination,
            'module' => 'Order ID, email and full',
            'options' => array(
            ),
        ), false);
    }
    ?>
    <?php echo \Form::close(); ?>

    <div class="form-inline pull-right">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>

<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'data_form')); ?>
    <table class="table table-striped table-bordered">
        <thead>
            <tr class="blueTableHead">
                <th scope="col" class="center" style="width: 90px;">ID</th>
                <th scope="col">Date</th>
                <th scope="col">Full Name</th>
                <th scope="col">Email</th>
                <th scope="col">Order Total</th>
                <th scope="col" class="center" width="150">Actions</th>
            </tr>
        </thead>
        <tbody>

            <?php foreach($items as $item): ?>

                <tr>
                    <td class="center">
                        <a href="<?php echo \Uri::create('admin/order/abandon_cart_view/' . $item->id); ?>">
                            <strong><?php echo $item->id; ?></strong>
                        </a>
                    </td>
                    <td title="<?php echo date('d/m/Y \a\t H:i:s', $item->created_at); ?>"><?php echo date('d/m/Y', $item->created_at).'<br />'.date('H:i:s', $item->created_at); ?></td>
                    <?php
                        $user = false;
                        if(\SentryAdmin::user_exists((int)$item->user_id))
                            $user = \SentryAdmin::user((int)$item->user_id);
                    ?>
                    <td><?php echo $user? $user->get('metadata.first_name') . ' ' . $user->get('metadata.last_name') : ''; ?></td>
                    <td><?php echo $user? $user->email : ''; ?></td>
                    <?php
                        $total = 0;
                        $total_delivery_charge = 0;
                        $discount_amount = 0;
                        $shipping_price = 0;
                        $item_exists = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'order_id' => $item->id ) ) );
                        foreach ($item_exists as $product)
                        {
                            $total += ($product->price * $product->quantity);
                            $total_delivery_charge += $product->delivery_charge;
                        }

                        $total_order = $total - $discount_amount + $shipping_price + $total_delivery_charge;
                    ?>
                    <td style="text-align:right">$<?php echo number_format(round($total_order, 2), 2); ?></td>
                    <td width="110">
                        <ul class="table-action-inline">
                            <li>
                                <a href="<?php echo \Uri::create('admin/order/abandon_cart_view/' . $item->id); ?>">View</a>
                            </li>
                            <li>
                                <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete abandon cart?" href="<?php echo \Uri::create('admin/order/delete/' . $item->id); ?>">Delete</a>
                            </li>
                        </ul>
                    </td>
                </tr>

            <?php endforeach; ?>

            <?php if(empty($items)): ?>

                <tr class="nodrag nodrop">
                    <td colspan="11" class="center"><strong>There are no orders.</strong></td>
                </tr>

            <?php endif; ?>

        </tbody>
    </table>

    <div class="pagination-holder">
        <?php echo $pagination->render(); ?>
    </div>
<?php echo \Form::close(); ?>



                            