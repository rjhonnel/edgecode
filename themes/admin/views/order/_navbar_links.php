<?php if(isset($user)): // Used on "UPDATE" pages?>

    <script type="text/javascript">
        // Select current tab
        $(document).ready(function(){
            if($(".second_menu").length > 0)
            {
                $(".second_menu ul li a.active").removeClass('active');
                $(".second_menu ul li a").each(function(){
                    if($(this).attr("href") == uri_current)
                    {
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>

    <div class="second_menu">
        <ul>
            <li><a href="<?php echo \Uri::create('admin/user/update/' . $group->id); ?>" class="circle_information" rel="tooltip" title="General Information"></a></li>
        </ul>
    </div>
<?php else: // Used on "CREATE" page ?>
    <?php if(FALSE): ?>
        <div class="second_menu">
            <ul>
                <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
            </ul>
        </div>
    <?php endif; ?>
<?php endif; ?>