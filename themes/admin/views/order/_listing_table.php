<?php $settings = \Config::load('autoresponder.db'); ?>

<?php
// Configs
$invoice = array('' => '-') + \Config::get('details.invoice', array());
$status = array('' => '-') + \Config::get('details.status', array());
$delivery = array('' => '-') + \Config::get('details.delivery', array());
?>
<div class="top-filter-holder">
    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form')); ?>
    <?php
    isset($filters) or $filters = true;

    if($filters)
    {
        echo \Theme::instance()->view('views/_partials/search_filters', array(
            'pagination' => $pagination,
            'module' => 'Order ID, company and full',
            'options' => array(
                'order_date',
                'order_total',
                'order_status',
                // 'user_group',
                'tracking_no',
                // 'payment_method',
                // 'payment_status',
                // 'shipping_status',
                // 'country',
                // 'state',
                // 'product_category',
                //'discount_coupon',
            ),
        ), false);
    }
    ?>
    <?php echo \Form::close(); ?>

    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
    <div class="form-inline pull-right">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
    <?php echo \Form::close(); ?>
</div>
<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'data_form')); ?>
    <table class="table table-hover table-striped2 table-bordered">
        <thead>
        <tr class="blueTableHead">
            <th scope="col" class="center" width="60">ID</th>
            <th scope="col" width="120">Date</th>
            <th scope="col" width="130">Delivery</th>
            <th scope="col">Customer Name</th>
            <th scope="col" width="200">Company Name</th>
            <th scope="col" width="50">Item</th>
            <th scope="col">Total</th>
            <th scope="col">Status</th>
            <th  scope="col" width="125" class="text-right">Actions</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach($items as $item): ?>
            
            <tr>
                <td class="quickview">
                    <a href="javascript:;"  rel="tooltip" data-original-title="Quick View" title="Quick View">
                        <strong><?php echo $item->id; ?></strong>
                    </a>
                </td>
                <td class="quickview"  style="font-size:14px" title="<?php echo date('d/m/Y \a\t H:i:s', $item->created_at); ?>"><?php echo date('d/m/Y', $item->created_at).'<br />'.date('H:i:s', $item->created_at); ?>
                </td>
                <td   class="quickview" style="font-size:14px" title="<?php echo $item->delivery_datetime?(date('d/m/Y \a\t h:i a', strtotime($item->delivery_datetime))):''; ?>"><?php echo $item->delivery_datetime?(date('d/m/Y', strtotime($item->delivery_datetime)).'<br />'.date('h:i a', strtotime($item->delivery_datetime))):''; ?> <?php echo $item->delivery_datetime_list?(count(explode(';', $item->delivery_datetime_list))>1?($item->delivery_datetime?'+':explode(';', $item->delivery_datetime_list)[0].' +'):''):''; ?></td>

                <td>
                    <a href="<?php echo \Uri::create('admin/order/update/' . $item->id); ?>" rel="tooltip" data-original-title="Edit / View">
                        <strong><?php echo $item->first_name . ' ' . $item->last_name; ?></strong>
                    </a>
                </td>
                <td class="quickview" <?php echo (strlen($item->company) > 20) ? 'rel="tooltip" title="'.$item->company.'"' : '';?> ><?php echo (strlen($item->company) > 20) ? substr($item->company, 0, 20).'...' : $item->company;?></td>
                <td class="quickview"><?php if($order_info = $item->order_info($item->id)) echo $order_info['quantity']; ?></td>
                <td class="quickview">$<?php echo number_format(round($item->total_price(), 2), 2); ?></td>
                <td class="quickview"><?php echo Inflector::humanize($item->status); ?></td>
                <td>
                    <ul class="table-action-inline text-right">
                        <li>
                            <a href="<?php echo \Uri::create('admin/order/update/' . $item->id); ?>">
                                View
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo \Uri::create('admin/order/edit/' . $item->id); ?>">
                                Edit
                            </a>
                        </li>
                    </ul>
                </td>
            </tr>
            <tr hidden>
                <td colspan="9" class="table-quick-view">
                    <span class="table-quick-view-arrow"></span>
                    <h4 class="title-legend pull-left">
                        <span>ID: <?php echo $item->id ?></span>
                        <span><?php echo $item->client_po ? 'PO: '.$item->client_po: ''; ?></span>
                    </h4>
                    <ul class="table-heading-actions">
                        <li>
                            <?php
                                // get next status
                                // pending > approved > invoiced, paid
                                $get_current_status = $item->status;
                                $get_next_status = false;
                                $get_next_status_human = false;
                                if($get_current_status)
                                {
                                    if($get_current_status == 'pending' || $get_current_status == 'new')
                                    {
                                        $get_next_status = 'approved';
                                        $get_next_status_human = 'Approve';
                                    }
                                    else if($get_current_status == 'approved')
                                    {
                                        $get_next_status = 'invoiced';
                                        $get_next_status_human = 'Invoice';
                                    }
                                    else if($get_current_status == 'invoiced' || $get_current_status == 'partially_paid')
                                        $get_next_status = false;
                                        //$get_next_status = 'paid';
                                    else if($get_current_status == 'paid' || $get_current_status == 'cancelled')
                                        $get_next_status = false;
                                    else
                                        $get_next_status = 'new';
                                }
                                else
                                    $get_next_status = 'new';
                            ?>
                            <?php if($get_next_status): $human_status = $get_next_status_human ? : (isset(\Config::get('details.status')[$get_next_status])?\Config::get('details.status')[$get_next_status]:''); ?>
                                <a href="<?php echo \Uri::create('admin/order/update_status/' . $item->id); ?>?status=<?php echo $get_next_status; ?>" class="confirmation-pop-up" data-message="Are you sure you want to update status of this order to <?php echo $human_status; ?>?" rel="tooltip" data-original-title="Update status to <?php echo $human_status; ?>" title="Update status to <?php echo $human_status; ?>"><i class="fa fa-file-o"></i> <?php echo $human_status; ?></a>
                            <?php endif; ?>
                        </li>
                        <li>
                            <a href="#" class="email-order-list" data-email="<?php echo $item->email; ?>" data-name="<?php echo $item->first_name.' '.$item->last_name; ?>" data-orderid="<?php echo $item->id; ?>" data-company="<?php echo $settings['company_name']; ?>" data-phone="<?php echo $settings['phone']; ?>"><i class="fa fa-envelope-o"></i> Email</a>
                        </li>
                        <li>
                            <a href="<?php echo \Uri::create('admin/order/pdf/' . $item->id); ?>" target="_blank"><i class="fa fa-print"></i> Print</a>
                        </li>
                    </ul>

                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr class="blueTableHead">
                            <th class="center" scope="col">Code</th>
                            <th scope="col" >Product Name</th>
                            <th scope="col">Attributes</th>
                            <th class="center" scope="col">Price Type</th>
                            <th class="center" scope="col">Delivery Time</th>
                            <th  class="center" scope="col" style="width: 100px;">Unit Price</th>
                            <th class="center" scope="col" style="width: 50px;">Qty</th>
                            <th class="right" scope="col" style="width: 100px;">Total Price</th>
                        </tr>
                        </thead>
                        <tbody>
                       
                        <?php if(!empty($item->products)): $total = 0; ?>
                            <?php foreach($item->products as $product): ?>
                                <tr>
                                    <?php
                                    $total += ($product->price * $product->quantity);
                                    ?>
                                    <td class="center"><a href="<?php echo \Uri::create('admin/product/update/' . $product->product_id); ?>"><?php echo $product->code; ?></a></td>
                                    <td><a href="<?php echo \Uri::create('admin/product/update/' . $product->product_id); ?>"><?php echo $product->title; ?></a></td>
                                    <td><?php echo $get_attributes($product->attributes)? $get_attributes($product->attributes): 'N/A'; ?></td>
                                    <td class="center"><?php echo $product->price_type=='sale_price'?\Inflector::humanize($product->price_type):''; ?></td>
                                    <td style="font-size:14px" title="<?php echo $item->delivery_datetime_list?(date('h:i a', strtotime($product->delivery_time))):($item->delivery_datetime?(date('h:i a', strtotime($item->delivery_datetime))):''); ?>"><?php echo $item->delivery_datetime_list?(date('h:i a', strtotime($product->delivery_time))):($item->delivery_datetime?(date('h:i a', strtotime($item->delivery_datetime))):''); ?></td>
                                    <td class="center">$<?php echo nf($product->price); ?></td>
                                    <td class="center"><?php echo $product->quantity; ?></td>
                                    <td class="center">$<?php echo nf($product->price * $product->quantity); ?></td>
                                </tr>
                                  <?php if($product->packs): $packs = json_decode($product->packs, true); ?>
                                    <?php foreach($packs as $pack): ?>
                                        <tr>
                                            <td></td>
                                            <td colspan="3" style="padding-left: 30px;">
                                                <?php
                                                    echo $pack['name'];
                                                    if($pack['products'])
                                                    {
                                                        echo ' ( ';
                                                        foreach ($pack['products'] as $key => $prdct)
                                                        {
                                                            echo $prdct['name'];
                                                            if($key != (count($pack['products']) - 1))
                                                                echo ', ';
                                                        }
                                                        echo ' ) ';
                                                    }
                                                ?>
                                            </td>
                                            <td colspan="4">
                                                <?php
                                                    echo date('h:i a', strtotime($pack['time']));
                                                ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                  <?php endif; ?>
                            <?php endforeach; ?>


                        <?php else: ?>
                            <tr><td colspan="9"><small class="alert alert-info">There are no items.</small></td></tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                    <div style="margin-top:15px;">
                        <h4 class="title-legend pull-left">Order Notes</h4>
                        <p style="background:#fff;clear:left;padding: 10px 15px;"><?php echo ($item->notes)? $item->notes: 'N/A';?></p>

                        <h4 class="title-legend pull-left">Delivery Notes</h4>
                        <p style="background:#fff;clear:left;padding: 10px 15px;"><?php echo ($item->delivery_notes)? $item->delivery_notes: 'N/A';?>  
                        </p>

                        <h4 class="title-legend">Client Notes</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title pull-left">Notes <?php echo '- ' . $item->first_name. ' ' . $item->last_name; ?></h3>
                            </div>
                        <div class="panel-body">
                            <div class="notes">
                                <?php if($item->client_notes): ?>
                                    <?php foreach($item->client_notes as $note): ?>
                                        <div class="note">
                                            <div class="note-indicator"></div>
                                            <div class="note-item-content">
                                                <div class="note-content" style="white-space: pre-wrap;"><?php echo $note->note; ?></div>
                                                <div class="note-footer">
                                                    <span class="note-updated"> <?php echo date('d/m/Y h:i:s a', $note->created_at); ?> </span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <small class="alert alert-info">There are no notes.</small>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>

        <?php endforeach; ?>

        <?php if(empty($items)): ?>

            <tr class="nodrag nodrop">
                <td colspan="11" class="center"><strong>There are no orders.</strong></td>
            </tr>

        <?php endif; ?>

        </tbody>
    </table>

    <div class="pagination-holder">
        <?php echo $pagination->render(); ?>
    </div>
<?php echo \Form::close(); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('.quickview').click(function(e){
            e.preventDefault();
           
            if($(this).closest('tr').next('tr').hasClass('active')){
                   $(this).closest('tr').next('tr').removeClass('active').hide();
            }else{
                $('table tbody tr').each(function(){
         
                    if($(this).hasClass('active')){
                        $(this).hide();
                        $(this).removeAttr('class')
                    }
              
                });
                $(this).closest('tr').next('tr').addClass('active').show();
                $('html,body').animate({
                scrollTop: $(this).offset().top - 65},
                'slow');
            }
           

        });

    });
</script>
<style type="text/css">
    .table-striped2 > tbody > tr:nth-of-type(4n+1){
        background: #f9f9f9;
    }
</style>

<div class="modal fade" id="order-email-list" tabindex="-1" role="dialog" aria-labelledby="order-emailLabel">
  <div class="modal-dialog modal-lg" role="document">
    <form action="<?php echo \Uri::create('admin/order/pdf_send_email/'); ?>" method="post">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="order-emailLabel">Order </h4>
        </div>
        <div class="modal-body form-horizontal">

          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>To* </i></label>
            <div class="col-sm-11"><input type="email" required name="email_to" class="form-control" value="" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>CC</i></label>
            <div class="col-sm-11"><input type="email" required name="email_cc" class="form-control" placeholder="Separate with comma for more than one email"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>From* </i></label>
            <div class="col-sm-11"><input required type="email" required name="email_from" class="form-control" value="<?php echo $settings['company_name'];?> <<?php echo $settings['sender_email_address']; ?>>" placeholder="name <example@mail.com> OR example@mail.com"></div>
          </div>
          <div class="row form-group">
            <label class="col-sm-1 control-label"><i>Subject*</i></label>
            <div class="col-sm-11"><input type="text" required name="email_subject" class="form-control" placeholder="Enter subject here" value="<?php echo $settings['company_name']; ?> Order"></div>
          </div>
          <hr>
          <label class="control-label"><i>Message* </i></label>
          <textarea name="message" required class="form-control" rows="10">Dear ,
          
See attached in PDF your (order). 
          
Should you have any questions, please do not hesitate to contact us.
          
Kind regards
          
<?php echo $settings['company_name']; ?>

<?php echo $settings['phone']; ?>
          </textarea>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary pdf-send-email-submit">Send email</button>
        </div>
      </div>
    </form>
  </div>
</div>