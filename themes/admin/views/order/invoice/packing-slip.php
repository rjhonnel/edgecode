<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=320, target-densitydpi=device-dpi">
        <style type="text/css">
            body { width: 100% !important; font-size: 12px; font-weight: normal;}
            body { background-color: #fff; margin: 0; padding: 0; }
            img { outline: none; text-decoration: none; display: block;}
            body, td { font-family: Arial, Helvetica, sans-serif; color: #000; text-decoration: none; font-size: 12px; font-weight: normal;}
            a{
                color: #000; font-weight:bold; text-decoration:none;
            }
            table {

            }
            th {
                font-weight: bold;
                border-bottom: 1px solid #cccccc;
                padding: 0 10px;
            }
            .text-right { text-align: right; }
            .bold { font-weight:bold; }
        </style>
    </head>
    <body>
        <?php $settings = \Config::load('autoresponder.db'); ?>
        <?php $data = $content['content'];   ?>
        <table width="100%" cellpadding="10" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td><img src="<?php echo \Uri::create('/') ?>media/images/<?php echo $settings['logo_url']; ?>"></td>
                    <td><strong><?php echo $settings['company_name']; ?></strong> <br/>
                        <?php echo nl2br($settings['address']); ?>
                    </td>
                    <td><?php echo $settings['email_address']; ?> <br/>
                        <?php echo $settings['website']; ?> <br/>
                        Phone: <?php echo $settings['phone']; ?>
                    </td>
                    <td><span style="font-weight:bold;font-size:18px">Packing Slip</span> <br/>
                        Order No: <?php echo $data->id; ?> <br/>
                        Delivery date: <?php /*no delivery date data found*/ ?>
                    </td>
                </tr>
            </tbody>
        </table>

        <table width="100%" cellpadding="0" cellspacing="10">
            <thead>
                <tr>
                    <th>Deliver to</th>
                    <th>Invoice to</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo ($data->shipping_first_name?:$data->first_name).' '.($data->shipping_last_name?:$data->last_name); ?>
                        <?php echo $data->shipping_company?'<br/>'.$data->shipping_company:($data->company?'<br/>'.$data->company:null); ?>
                        <?php echo $data->shipping_address?'<br/>'.$data->shipping_address:($data->address?'<br/>'.$data->address:null); ?>
                        <?php echo $data->shipping_suburb?'<br/>'.$data->shipping_suburb:($data->suburb?'<br/>'.$data->suburb:null); ?>
                        <?php echo $data->shipping_state?'<br/>'.$data->shipping_state:($data->state?'<br/>'.$data->state:null); ?> <?php echo $data->shipping_country?$data->shipping_country:($data->country?$data->country:null); ?> <?php echo $data->shipping_postcode?$data->shipping_postcode:($data->postcode?$data->postcode:null); ?>
                        <br/><?php echo $data->shipping_phone?'Phone: '.$data->shipping_phone:($data->phone?'Phone: '.$data->phone:null); ?>
                    </td>
                    <td><?php echo $data->first_name.' '.$data->last_name; ?>
                        <?php echo $data->company?'<br/>'.$data->company:null; ?>
                        <?php echo $data->address?'<br/>'.$data->address:null; ?>
                        <?php echo $data->suburb?'<br/>'.$data->suburb:null; ?>
                        <?php echo $data->state?'<br/>'.$data->state:null; ?> <?php echo $data->country?$data->country:null; ?> <?php echo $data->postcode?$data->postcode:null; ?>
                    </td>
                </tr>
            </tbody>
        </table>

        <table width="100%" cellpadding="0" cellspacing="10">
            <thead>
                <tr>
                    <th colspan="2">Deliver info</th>
                    <th colspan="2">Order info</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Date:</td> <td><?php /*no delivery date data found*/ ?></td>
                    <td>Order No:</td> <td><?php echo $data->id; ?></td>
                </tr>
               
                <tr>
                    <td>Instructions:</td>
                    <td><?php echo $data->notes?$data->notes:null; ?>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" cellpadding="0" cellspacing="3">
            <tbody>
                <tr><td>
                    <!-- start item list table -->
                    <table width="100%">
                        <thead>
                            <tr><th colspan="2">Items</th></tr>
                        </thead>
                        <tbody>
                            <tr><td colspan="2"><strong><?php echo date('h:i A',$data->created_at); ?></strong> <br/></td></tr>
                            <tr>
                                <td class="bold">Quantity</td>
                                <td class="bold">Item</td>
                            </tr>

                            <!-- items -->
                            <?php if ( $data->products ):?>
                                <?php foreach($data->products as $product): ?>
                                    <tr>
                                        <td><?php echo $product->quantity;?> </td>
                                        <td ><?php echo $product->title;?> </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php else: ?>
                                <tr><td colspan="2"> There are no products found on this order. </td></tr>
                            <?php endif; ?>
                            <!-- end of items -->
                        </tbody>
                    </table>
                    <!-- end item list table -->
                </td></tr>
            </tbody>
        </table>

    </body>
</html>