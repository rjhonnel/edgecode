<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=320, target-densitydpi=device-dpi">
        <style type="text/css">
            body { width: 100% !important; font-size: 12px; font-weight: normal;}
            body { background-color: #fff; margin: 0; padding: 0; }
            img { outline: none; text-decoration: none; display: block;}
            body, td { font-family: Arial, Helvetica, sans-serif; color: #000; text-decoration: none; font-size: 11px; font-weight: normal;}
            a{
                color: #000; font-weight:bold; text-decoration:none;
            }
            table {
                margin: 0; padding: 0;
            }
            th {
                font-weight: bold;
                border-bottom: 1px solid #cccccc;
                padding: 0 10px;
            }
            td {
                vertical-align: middle;
            }
            .text-right { text-align: right; }
            .bold { font-weight:bold; }
            .bordered { border: 1px solid #cccccc; }
            .bordered-bottom { border-bottom: 1px solid #cccccc; }
            .box { border: 1px solid #cccccc; width: 5px; height: 5px; display: inline-block;}
        </style>
    </head>
    <body>
        <?php $settings = \Config::load('autoresponder.db'); ?>
        <?php $data = $content['content'];   ?>
        <table width="100%" cellpadding="10" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td><img src="<?php echo \Uri::create('/') ?>media/images/<?php echo $settings['logo_url']; ?>"></td>
                    <td><strong><?php echo $settings['company_name']; ?></strong> <br/>
                        <?php echo nl2br($settings['address']); ?>
                    </td>
                    <td><?php echo $settings['email_address']; ?> <br/>
                        <?php echo $settings['website']; ?> <br/>
                        Phone: <?php echo $settings['phone']; ?>
                    </td>
                    <td><span style="font-weight:bold;font-size:18px">Tax Invoice</span> <br/>
                        Invoice No: <?php echo $data->id; ?> <br/>
                        Invoice date: <?php /*no delivery date data found*/ ?>
                    </td>
                </tr>
            </tbody>
        </table>

        <table width="100%" cellpadding="0" cellspacing="10">
            <thead>
                <tr>
                    <th>Deliver to</th>
                    <th>Invoice to</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?php echo ($data->shipping_first_name?:$data->first_name).' '.($data->shipping_last_name?:$data->last_name); ?>
                        <?php echo $data->shipping_company?'<br/>'.$data->shipping_company:($data->company?'<br/>'.$data->company:null); ?>
                        <?php echo $data->shipping_address?'<br/>'.$data->shipping_address:($data->address?'<br/>'.$data->address:null); ?>
                        <?php echo $data->shipping_suburb?'<br/>'.$data->shipping_suburb:($data->suburb?'<br/>'.$data->suburb:null); ?>
                        <?php echo $data->shipping_state?'<br/>'.$data->shipping_state:($data->state?'<br/>'.$data->state:null); ?> <?php echo $data->shipping_country?$data->shipping_country:($data->country?$data->country:null); ?> <?php echo $data->shipping_postcode?$data->shipping_postcode:($data->postcode?$data->postcode:null); ?>
                        <br/><?php echo $data->shipping_phone?'Phone: '.$data->shipping_phone:($data->phone?'Phone: '.$data->phone:null); ?>
                    </td>
                    <td><?php echo $data->first_name.' '.$data->last_name; ?>
                        <?php echo $data->company?'<br/>'.$data->company:null; ?>
                        <?php echo $data->address?'<br/>'.$data->address:null; ?>
                        <?php echo $data->suburb?'<br/>'.$data->suburb:null; ?>
                        <?php echo $data->state?'<br/>'.$data->state:null; ?> <?php echo $data->country?$data->country:null; ?> <?php echo $data->postcode?$data->postcode:null; ?>
                    </td>
                </tr>
            </tbody>
        </table>

        <table width="100%" cellpadding="0" cellspacing="10">
            <thead>
                <tr>
                    <th colspan="2">Deliver info</th>
                    <th colspan="2">Order info</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Date:</td> <td><?php /*no delivery date data found*/ ?></td>
                    <td>Order No:</td> <td><?php echo $data->id; ?></td>
                </tr>
               
                <tr>
                    <td>Instructions:</td>
                    <td><?php echo $data->notes?$data->notes:null; ?>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <table width="100%" cellpadding="0" cellspacing="3">
            <tbody>
                <tr><td>
                    <!-- start item list table -->
                    <table width="100%">
                        <thead>
                            <tr><th colspan="4">Items</th></tr>
                        </thead>
                        <tbody>
                            <tr><td colspan="4"><strong><?php echo date('h:i A',$data->created_at); ?></strong> <br/></td></tr>
                            <tr>
                                <td class="bold" >Item</td>
                                <td class="text-right bold">Unit Price</td>
                                <td class="text-right bold">Quantity</td>
                                <td class="text-right bold">Total Price</td>
                            </tr>

                            <!-- items -->
                            <?php $total = 0; if ( $data->products ):?>
                                <?php foreach($data->products as $product): $total += ($product->price * $product->quantity); ?>
                                    <tr>
                                        <td ><?php echo $product->title;?> </td>
                                        <td class="text-right">$<?php echo number_format($product->price,2);?> </td>
                                        <td class="text-right"><?php echo $product->quantity;?> </td>
                                        <td class="text-right">$<?php echo number_format($product->quantity * $product->price, 2);?> </td>
                                    </tr>
                                <?php endforeach;?>
                            <?php else: ?>
                                <tr><td colspan="4"> There are no products found on this order. </td></tr>
                            <?php endif; ?>
                            <!-- end of items -->

                            <tr><td colspan="4">&nbsp;</td></tr>
                            <tr>
                                <td class="text-right" colspan="3">Products Total:</td>
                                <td class="text-right">$<?php echo number_format($total, 2);?></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="3">Shipping:</td>
                                <td class="text-right">$<?php echo number_format($data->shipping_price, 2);?></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="3">Extra Delivery Charge:</td>
                                <td class="text-right">$<?php echo number_format($data->total_delivery_charge, 2);?></td>
                            </tr>
                            <tr>
                                <td class="text-right" colspan="3">Discount:</td>
                                <td class="text-right">-$<?php echo number_format($data->discount_amount, 2);?></td>
                            </tr>
                            <tr>
                                <td class="text-right bold" colspan="3">AUD Total:</td>
                                <td class="text-right bold">$<?php echo number_format($data->total_price(), 2);?></td>
                            </tr>
                            <?php 
                                $hold_gst = (isset($settings['gst']) ? 1 : 0);
                                $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
                            ?>
                            <?php if($hold_gst && $hold_gst_percentage > 0): ?>
                                <tr>
                                    <td class="text-right" colspan="3">GST Tax <?php echo $hold_gst_percentage; ?>% (Included):</td>
                                    <td class="text-right">$<?php echo number_format(\Helper::calculateGST($data->total_price(), $hold_gst_percentage), 2);?></td>
                                </tr>
                            <?php endif;?>
                            <tr>
                                <td class="text-right bold" colspan="3">Amount payable:</td>
                                <td class="text-right bold">$<?php echo number_format($data->total_price(), 2);?></td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- end item list table -->
                </td></tr>
            </tbody>
        </table>

        <table cellspacing="10" width="100%">
            <tr><td></td></tr>
            <tr><td class="bordered-bottom">&nbsp;</td></tr>
        </table>

        <table width="100%" cellpadding="0" cellspacing="10">
            <tr>
                <td width="40%"><span style="font-weight:bold;font-size:15px">Remittance Advice</span></td>
                <td width="60%"><strong>For credit card payments:</strong></td>
            </tr>
            <tr>
                <td><table width="100%" cellpadding="2" cellspacing="0">
                        <tr><td>Invoice No:</td><td><?php echo $data->id; ?></td></tr>
                        <tr><td>Invoice date:</td><td><?php ?></td></tr>
                        <tr><td>Customer:</td><td><?php echo $data->first_name; echo $data->last_name?'<br>'.$data->last_name:null; echo $data->company?'<br>'.$data->company:null; ?></td></tr>
                        <tr><td>Amount payable AUD:</td><td class="bordered"><span style="font-weight:bold;font-size:14px">$<?php echo number_format($data->total_price + $data->gst_price, 2);?></span></td></tr>
                        <tr><td>Amount paid:</td><td class="bordered"></td></tr>
                        <tr><td>Date of payment:</td><td class="bordered"></td></tr>
                    </table></td>
                <td><table cellpadding="2" cellspacing="0" width="100%">
                        <tr><td width="25%">Card type:</td><td width="75%">Visa &nbsp;Mastercard &nbsp;Amex &nbsp;Diners</td></tr>
                        <tr><td>Credit card No:</td>
                            <td><table>
                                    <tr> <td class="bordered"></td><td class="bordered"></td><td class="bordered"></td><td class="bordered"></td> <td></td>  <td class="bordered"></td><td class="bordered"></td><td class="bordered"></td><td class="bordered"></td> <td></td> <td class="bordered"></td><td class="bordered"></td><td class="bordered"></td><td class="bordered"></td> <td></td> <td class="bordered"></td><td class="bordered"></td><td class="bordered"></td><td class="bordered"></td>  </tr>
                                </table>
                            </td></tr>
                        <tr><td>Name on card:</td><td class="bordered-bottom"></td></tr>
                        <tr><td>Signature:</td><td class="bordered-bottom"></td></tr>
                        <tr><td></td><td></td></tr>
                    </table>

                    <table>
                        <tr> <td><strong>For EFT payments:</strong></td> <td><strong>For cheque payments:</strong></td> </tr>
                        <tr>
                            <td><?php echo nl2br($settings['bank_details']); ?></td>
                            <td>All payments made payable to:<br/>"<?php echo $settings['company_name'];?>"</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>