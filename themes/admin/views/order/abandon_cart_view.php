                                <?php
// Configs
$invoice = array('' => '-') + \Config::get('details.invoice', array());
$status = array('' => '-') + \Config::get('details.status', array());
$delivery = array('' => '-') + \Config::get('details.delivery', array());

$app_countries = \App\Countries::forge();
$countries = $app_countries->getCountries();
$app_states = \App\States::forge();
?>

<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Orders');
        \Breadcrumb::set('Abandoned Cart Details', 'admin/order/abandon_cart');
        \Breadcrumb::set('ID: ' . $order->id);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Abandoned Cart</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/order/_action_links_abandon'); ?>
                </div>
            </header>

            <?php echo \Form::open(array('action' => \Uri::admin(), 'method' => 'post')); ?>
            <?php echo \Form::hidden('order_details', 1); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Cart Details</h3>

                </div>
                <div class="panel-body form-horizontal">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="form-group mb-0">
                                <label class="col-sm-3 control-label">ID</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="name" value="#<?php echo $order->id; ?>" type="text" id="form_name" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group mb-0">
                                <label class="col-sm-3 control-label">Date Submited</label>
                                <div class="col-sm-9">
                                    <input class="form-control" name="name" value="<?php echo date('d/m/Y H:i:s', $order->created_at); ?>" type="text" id="form_name" disabled>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="save_button_holder text-right">
                <button id="save_order_info" name="save" value="1" class="btn btn-primary" type="submit" style="display:none"><i class="fa fa-edit"></i> Save</button>
            </div>

            <h3 class="title-legend">Order Details</h3>
            <table class="table table-striped table-bordered">
                <thead>
                <tr class="blueTableHead">
                    <th style="width: 40px;" class="center" scope="col">Code</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Attributes</th>
                    <th style="width: 100px;" class="center" scope="col">Price Type</th>
                    <th style="width: 100px;" class="center" scope="col">Unit Price</th>
                    <th style="width: 100px;" class="center" scope="col">Qty</th>
                    <th style="width: 100px;" class="center" scope="col">Total Price</th>
                </tr>
                </thead>
                <tbody>
                <?php if(!empty($products)): $total = 0; $total_delivery_charge = 0; $discount_amount = 0; $total_category_discount = 0; ?>
                    <?php foreach($products as $product): ?>
                        <tr>
                            <?php
                            $total += ($product->price * $product->quantity);
                            $total_delivery_charge += $product->delivery_charge;

                            $total_category_discount += \Discountcode\Model_Discountcode::getCategoryDiscount($product->product_id, ($product->quantity * $product->price));
                            ?>
                            <td class="center"><?php echo $product->code; ?></td>
                            <td><?php echo $product->title; ?></td>
                            <td><?php echo $get_attributes($product->attributes)? $get_attributes($product->attributes): 'N/A'; ?></td>

                            <td class="center"><?php echo $product->price_type=='sale_price'?\Inflector::humanize($product->price_type):''; ?></td>
                            <td class="center">$<?php echo nf($product->price); ?></td>
                            <td class="center"><?php echo $product->quantity; ?></td>
                            <td class="center" style="text-align:right">$<?php echo nf($product->quantity * $product->price); ?></td>
                        </tr>
                          <?php if($product->packs): $packs = json_decode($product->packs, true); ?>
                            <?php foreach($packs as $pack): ?>
                                <tr>
                                    <td></td>
                                    <td colspan="6" style="padding-left: 30px;">
                                        <?php 
                                            echo $pack['name'];
                                            if($pack['products'])
                                            {
                                                echo ' ( ';
                                                foreach ($pack['products'] as $key => $product)
                                                {
                                                    echo $product['name'];
                                                    if($key != (count($pack['products']) - 1))
                                                        echo ', ';
                                                }
                                                echo ' ) ';
                                            }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                          <?php endif; ?>
                    <?php endforeach; $discount_amount += $total_category_discount; ?>



                <?php else: ?>
                    <tr><td colspan="6">There are no items.</td></tr>
                <?php endif; ?>
                </tbody>
            </table>
            <div class="row">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <table class="table table-striped table-bordered">
                        <tr class="order_total">
                            <td class="">Products Total</strong></td>
                            <td class="center " style="text-align:right">$<span class="sub_total"><?php echo nf($total); ?></span></td>
                        </tr>
                        <tr class="order_total">
                            <td class="">Shipping</strong></td>
                            <td class="center " style="text-align:right">$<span class="gst"><?php echo nf($shipping_price); ?></span></td>
                        </tr>
                        <tr class="order_total">
                            <td class="">Extra Delivery Charge</strong></td>
                            <td class="center " style="text-align:right">$<span class="gst"><?php echo nf($total_delivery_charge); ?></span></td>
                        </tr>
                        <tr class="order_total">
                            <td class="">Discount</strong></td>
                            <td class="center " style="text-align:right">-$<span class="sub_total"><?php echo nf($discount_amount); ?></span></td>
                        </tr>
                        <?php $sub_total = $total - $discount_amount + $shipping_price + $total_delivery_charge; ?>
                        <tr class="order_total">
                            <td class=""><strong>Grand Total</strong></td>
                            <td class="center order_dark_price" style="text-align:right"><strong class="grand_total_box">$<span class="grand_total"><?php echo nf($sub_total); ?></span></strong></td>
                        </tr>
                        <?php
                            $settings = \Config::load('autoresponder.db');

                            $hold_gst = (isset($settings['gst']) ? 1 : 0);
                            $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
                        ?>
                        <?php if($hold_gst && $hold_gst_percentage > 0): ?>
                            <tr class="order_total">
                                <td class="">GST Tax <?php echo $hold_gst_percentage; ?>% (Included)</strong></td>
                                <td class="center " style="text-align:right">$<span class="gst"><?php echo nf(\Helper::calculateGST($sub_total, $hold_gst_percentage)); ?></span></td>
                            </tr>
                        <?php endif;?>
                    </table>
                </div>
            </div>






            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Customer Details</h3>

                </div>
                <div class="panel-body form-horizontal">

                    <?php echo \Form::hidden('customer_details', 1); ?>
                    <?php if($user): ?>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Full Name:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $user->get('metadata.first_name') . ' ' . $user->get('metadata.last_name'); ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $user->get('email'); ?>" disabled>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $user->get('metadata.company'); ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Address:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $user->get('metadata.address'); ?>" disabled>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Suburb:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $user->get('metadata.suburb'); ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Country:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $app_countries->getCountryName($user->get('metadata.country')); ?>" disabled>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">State:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $app_states->getStateProvinceName($user->get('metadata.state'), $user->get('metadata.country'));  ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Postcode:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $user->get('metadata.postcode'); ?>" disabled>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Telephone:</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" value="<?php echo $user->get('metadata.phone'); ?>" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Profile:</label>
                            <div class="col-sm-5">
                                <a class="btn btn-mini btn-primary" href="<?php echo \Uri::create('admin/user/update/' . $user->get('id')); ?>"><i class="icon icon-user icon-white"></i> View Profile</a>
                            </div>
                        </div>
                    <?php else: ?>
                        <small class="alert alert-info">There is no active user with the given ID.</small>
                    <?php endif; ?>

                </div>
            </div>



            <?php echo \Form::close(); ?>


        </div>
    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>