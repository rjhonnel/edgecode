								
<div class="panelContent">
    <?php if(isset($panel_title)): ?>
        <h4>
            <?php echo $panel_title; ?>
            <?php echo isset($duplicate_order->number) ? $duplicate_order->number : $order->number;  ?>
        </h4>
    <?php endif; ?>
    <table class="grid greyTable2 separated" width="100%" id="<?php echo $type; ?>">
        <thead>
            <tr class="blueTableHead">
                <th scope="col" class="center" style="width: 100px;">Code</th>
                <th scope="col">Description</th>
                <th scope="col">Size</th>
                <th scope="col" class="center" style="width: 100px;">Quantity</th>
                <th scope="col" class="center" style="width: 100px;">Unit Price</th>
                <th scope="col" class="center" style="width: 100px;">Price</th>
                <th scope="col" class="center" style="width: 40px;">Delete</th>
            </tr>
        </thead>
        <tbody>

            <?php 
                $subtotal = 0; 
                if($type == 'duplicate_copy') $prefix = 'duplicate_';
                else $prefix = '';
            ?>
            <?php foreach($items as $item): ?>
                <tr> 
                    <td class="center"><?php echo $item->title; ?></td>
                    <td><?php echo $item->description; ?></td>
                    <td><?php echo $item->size; ?></td>
                    <td class="center"><?php echo \Form::input($prefix . "quantity[{$item->id}]", \Input::post($prefix . "quantity[{$item->id}]", $item->quantity), array('class' => 'qty_field')); ?></td>
                    <td class="center"><?php echo \Form::input($prefix . "price[{$item->id}]", \Input::post($prefix . "price[{$item->id}]", $item->price)); ?></td>
                    <td class="center"><?php echo \Form::input($prefix . "subtotal[{$item->id}]", \Input::post($prefix . "subtotal[{$item->id}]", $item->subtotal), array('class' => 'subtotal')); ?></td>
                    <td class="icon center">
                        <a href="<?php echo \Uri::create('admin/order/delete_product/' . $order->id . '/' . $item->id); ?>" class="<?php echo $type .'_delete' ?>">
                            Delete
                        </a>
                    </td>
                </tr>

            <?php 
                $subtotal += $item->subtotal;
                endforeach; 
            ?>

            <?php if(!empty($items)): ?>
                
                <?php $order_details = \Order\Model_Order::order_info($order->id); ?>
                
                <tr class="order_total">
                    <td class="order_invisible">&nbsp;</td>
                    <td class="order_invisible">&nbsp;</td>
                    <td class="order_invisible" colspan="2" align="right">
                        <?php if(isset($panel_title)): ?>
                            <a href="<?php echo \Uri::create('admin/order/update/' . (isset($duplicate_order->id) ? $duplicate_order->id : $order->id)); ?>" class="btn btn-mini btn-primary">
                                Full Edit Order <?php echo isset($duplicate_order->number) ? $duplicate_order->number : $order->number; ?>
                            </a>
                        <?php endif; ?>
                    </td>
                    <td class="order_text"><strong>Sub Total</strong></td>
                    <td class="center order_price">$<span class="sub_total"><?php echo number_format($subtotal, 2); ?></span></td>
                    <td class="order_price"><a href="#" class="btn btn-mini btn-primary refresh">Refresh</a></td>
                </tr>
                <tr class="order_total">
                    <td class="order_invisible">&nbsp;</td>
                    <td class="order_invisible">&nbsp;</td>
                    <td class="order_invisible">&nbsp;</td>
                    <td class="order_invisible">&nbsp;</td>
                    <td class="order_text"><strong>GST</strong></td>
                    <td class="center order_price">$<span class="gst"><?php echo number_format($subtotal * 1.1 - $subtotal, 2); ?></span></td>
                    <td class="order_price">&nbsp;</td>
                </tr>
                <tr class="order_total">
                    <td class="order_invisible">&nbsp;</td>
                    <td class="order_invisible" colspan="2"></td>
                    <td class="order_invisible">&nbsp;</td>
                    <td class="order_dark_text"><strong>Grand Total</strong></td>
                    <td class="center order_dark_price"><strong class="grand_total_box">$<span class="grand_total"><?php echo number_format($subtotal * 1.1, 2); ?></span></strong></td>
                    <td class="order_dark_price">&nbsp;</td>
                </tr>
                
            <?php else: ?>
                
                <tr class="nodrag nodrop">
                    <td colspan="6" class="center"><strong>There are no items.</strong></td>
                </tr>

            <?php endif; ?>

        </tbody>
    </table>
    
    <div class="clear"></div>
</div>

<script type="text/javascript">
    var $gst = 1.1;
    var delay = (function(){
        var timer = 0;
        return function(callback, ms){
          clearTimeout (timer);
          timer = setTimeout(callback, ms);
        };
    })();

    function blink($el, $k){
        $el.animate({opacity:0},200,"linear",function(){
            $(this).animate({opacity:1},200);
            $k++;    
            if($k <= 2) blink($el, $k);
        });
        
    }
    
    function refresh_price($type)
    {
        var $reg = '#' + $type;
        var $subtotal = 0;
        $('.subtotal', $reg).each(function(index){
            $subtotal += parseFloat($(this).val());
        });
        $('.sub_total', $reg).text(price_format($subtotal));
        $('.grand_total', $reg).text(price_format($subtotal * $gst));
        $('.gst', $reg).text(price_format($subtotal * $gst - $subtotal));
        blink($('.grand_total_box', $reg), 1);
    }
    
    $(document).ready(function(){
        /*$("img[src*='recycleBin']", "#duplicate_copy").unbind();*/
        $('.subtotal').on('keyup', function(){
            var $type = $(this).closest('table').attr('id');
            delay(function(){
                refresh_price($type);
            }, 800 );
        })
        
        $('.refresh').on('click', function(e){
            e.preventDefault();
            var $type = $(this).closest('table').attr('id');
            refresh_price($type);
        })
        
        /*
        $("img[src*='recycleBin']", "#duplicate_copy").each(function(){
            $(this).click(function(e) {
                e.preventDefault();
                var $sum = $('.qty_field', "#duplicate_copy").length; 
                var $remove = $(this).closest('tr');
                var $out = '<h4>You are about to delete this item. Are you sure?</h4>';
                var $href = $(this).parents('a').first().attr('href'); 
                var $msg = $(this).attr('data-msg');    
                if(typeof $msg != "undefined") {
                    $out = $msg;
                }
                bootbox.setIcons({
                    "OK"      : "icon-ok icon-white",
                    "CANCEL"  : "icon-remove",
                    "CONFIRM" : "icon-ok icon-white"
                });
                bootbox.confirm($out, function(confirmed) {
                    if(confirmed){
                       if($sum > 1) $remove.remove();
                    }                        
                });
            });
        });  
        */
        
        
    });

</script>
