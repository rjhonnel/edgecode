<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Orders');
        \Breadcrumb::set('Manage Abandoned Carts', 'admin/order/abandon_cart');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Abandoned Carts</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/order/_action_links_abandon', array('hide_show_all' => 1)); ?>
                </div>
            </header>

            <?php
            if(isset($user))
                echo \Theme::instance()->view('views/user/_navbar_links', array('user' => $user));
            ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>

            <?php
            // Load page listing table
            echo \Theme::instance()->view('views/order/_listing_table_abandon_cart',
                array(
                    'pagination' 	=> $pagination,
                    'items'			=> $items,
                ),
                false
            );
            ?>

            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
