<?php 
// Configs
$invoice = array('' => '-') + \Config::get('details.invoice');
$status = array('' => '-') + \Config::get('details.status');
$delivery = array('' => '-') + \Config::get('details.delivery');
?>	
                <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Orders')); ?>
                
                <?php echo \Theme::instance()->view('views/order/_navbar_links'); ?>
	            
	            <!-- Content -->
			    <div class="content_wrapper">
			    	<div class="content_holder">
			    		
			    		<div class="elements_holder">
			    		
                            <div class="row-fluid breadcrumbs_holder">
			    				<div class="breadcrumbs_position">
                                    <?php 
                                        \Breadcrumb::set('Home', 'admin/dashboard');
                                        \Breadcrumb::set('Orders');
                                        \Breadcrumb::set('Manage Orders', 'admin/order/list');
                                        \Breadcrumb::set('Edit Orders');
                                        \Breadcrumb::set('ID: ' . $order->id . ' Duplicate ID:' . $duplicate_order->id);

                                        echo \Breadcrumb::create_links();
                                    ?>
		                            
                                    <?php echo \Theme::instance()->view('views/order/_action_links'); ?>
                                </div>
                            </div>
                            
						    <div class="row-fluid">
						    	<?php echo \Theme::instance()->view('views/_partials/navigation', array('orders' => $orders), false); ?>
                                
		                    	<!-- Main Content Holder -->
                                <div class="content">

                                    <!-- Accordions Panel -->
                                    <div class="panel">
                                        <div class="panelHeader">
                                            <h4><i class="panel_information"></i>Order Products <?php if(!isset($duplicate_order)) echo $order->number; ?></h4>
                                        </div>

                                        <?php echo \Form::open(\Uri::admin('current'), array('order_edit' => 1, 'order_id' => $order->id)); ?>
                                            <?php
                                                // Load page listing table
                                                echo \Theme::instance()->view('views/order/_listing_table_products', 
                                                    array('order' => $order, 'items' => $order->products, 'type' => 'duplicate_original', 'panel_title' => 'Original Order'),
                                                    false
                                                );
                                                
                                                 // Load page listing table
                                                echo \Theme::instance()->view('views/order/_listing_table_products', 
                                                    array('order' => $duplicate_order, 'items' => $duplicate_order->products, 'type' => 'duplicate_copy', 'panel_title' => 'Duplicated Order'),
                                                    false
                                                );
                                            ?>
                                            <div class="save_button_holder m_b_10">
                                                <?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
                                                <?php echo \Form::button('exit', 'Save & Exit', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
                                            </div>
                                        <?php echo \Form::close(); ?>

                                    </div><!-- EOF Accordions Panel -->
                                    
                                    
                                    <!-- Accordions Panel -->
                                    <div class="panel">
                                        <div class="panelHeader">
                                            <h4><i class="panel_customer_details"></i> Order Details</h4>
                                        </div>
                                        <div class="panelContent">

                                              <table width="100%" cellspacing="0" cellpadding="0" border="0" class="greyTable2 separated three_col_inputs">
                                                <thead>
                                                    <tr class="blueTableHead">
                                                        <th scope="col">Original Order Information</th>
                                                        <th scope="col">Account Information</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td valign="top" width="50%">
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Order ID:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold"><?php echo $order->number; ?></label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Purchase Order:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold"><?php echo $order->purchase_number; ?></label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Order Date:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold"><?php echo date('d/m/Y', $order->created_at); ?></label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Requested Delivery Date:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold">
                                                                    <?php echo empty($order->delivery_date) ? '' : date('d/m/Y', $order->delivery_date); ?>
                                                                </label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Order Status:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold">
                                                                     <?php echo isset($status[$order->status]) ? $status[$order->status] : $order->status; ?>
                                                                </label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('PPI Sch. Delivery Date:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold">
                                                                    <?php echo empty($order->sch_delivery) ? '' : date('d/m/Y', $order->sch_delivery); ?>
                                                                </label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Delivery Status:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold">
                                                                     <?php echo isset($delivery[$order->delivery_status]) ? $delivery[$order->delivery_status] : $order->delivery_status; ?>
                                                                </label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Invoice Status:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold">
                                                                    <?php echo isset($invoice[$order->invoice_status]) ? $invoice[$order->invoice_status] : $order->invoice_status; ?>
                                                                </label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Customer Comments:', null, array('style' => 'width: 150px;')); ?>
                                                                <label class="no_bold">
                                                                    <?php echo $order->comment; ?>
                                                                </label>
                                                            </div>
                                                            
                                                            <div class="clear"></div>
                                                            
                                                        </td>
                                                        <td valign="top">
                                                            
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Entity Name:'); ?>
                                                                <label class="no_bold"><?php echo $order->entity; ?></label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Email:'); ?>
                                                                <label class="no_bold"><?php echo $order->email; ?></label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('Representative Name:'); ?>
                                                                <label class="no_bold"><?php echo $order->representative; ?></label>
                                                            </div>
                                                            <div class="formRow">
                                                                <?php echo \Form::label('User Type:'); ?>
                                                                <label class="no_bold">
                                                                    <?php
                                                                        if($user->groups()):
                                                                           $user_group = $user->groups();
                                                                           $user_group = $user_group[0]; 
                                                                           echo $user_group['name'];
                                                                        endif;
                                                                    ?>
                                                                </label>
                                                            </div>
                                                            
                                                            <div class="formRow">
                                                                <br>
                                                                <a class="btn btn-primary" href="<?php echo \Uri::create('admin/user/update/' . $order->user_id); ?>">View Account</a>
                                                            </div>
                                                            
                                                            <div class="clear"></div>
                                                        </td>
                                                        
                                                    </tr>
                                                </tbody>                    
                                            </table>
                                        </div>
                                    </div><!-- EOF Accordions Panel -->
                                    
                                    
                                </div><!-- EOF Main Content Holder -->

                                <div class="clear"></div>
                                
						    </div>
			    		</div>
			    	</div>
			    </div>
			    <!-- EOF Content -->