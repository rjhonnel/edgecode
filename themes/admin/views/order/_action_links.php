<div class="action-list">
<?php if(isset($approved_order)): ?>
	<a href="<?php echo \Uri::create($approved_order); ?>" class="btn btn-primary btn-sm">Approve Order</a>
<?php endif; ?>
<?php if(isset($save) && $save==1): ?>
<a id="save_order" href="#" class="btn btn-success btn-sm">Save</a>
<?php endif; ?> 
<?php if(!isset($hide_add_new)): ?>
	<a href="<?php echo \Uri::create('admin/order/create'); ?>" class="btn btn-primary btn-sm">New Order</a>
<?php endif; ?> 
<?php if(isset($show_edit_order)): ?>
	<a href="<?php echo \Uri::create($show_edit_order); ?>" class="btn btn-primary btn-sm">Edit Order</a>
<?php endif; ?> 
<?php if(isset($show_edit_payment)): ?>
	<a href="<?php echo \Uri::create($show_edit_payment); ?>" class="btn btn-warning btn-sm">Edit Payment</a>

	<a class="btn btn-default btn-sm confirmation-pop-up" href="<?php echo \Uri::create('admin/order/duplicate/list/' . $order_id); ?>" rel="tooltip" data-message="Are you sure you want to clone this order?">Clone</a>
<?php endif; ?> 
<?php if(isset($view_order)): ?>
	<a href="<?php echo \Uri::create('admin/order/update/'.$order_id); ?>" class="btn btn-info btn-sm">View Order</a>
<?php endif; ?>




<?php if(!isset($hide_show_all)): ?>
	<a href="<?php echo \Uri::create('admin/order/list'); ?>" class="btn btn-default btn-sm">Show all</a>
<?php endif; ?>

<?php if(isset($show_import_to_myob) || isset($show_import_xero)): $accounting_api_settings = \Config::load('accounting_api.db'); $accounting_api = (isset($accounting_api_settings['accounting_api']) ? $accounting_api_settings['accounting_api'] : ''); ?>
	<?php if(isset($show_import_to_myob)):  ?>
		<?php if($accounting_api == 'myob'): ?>
			<a href="<?php echo \Uri::create('admin/order/import_to_myob'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Import to MYOB</a>
		<?php endif; ?>
	<?php endif; ?> 
	<?php if(isset($show_import_xero)): ?>
		<?php if($accounting_api == 'xero'): ?>
	    	<a href="<?php echo \Uri::create('admin/order/import_to_xero'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Import to Xero</a>
	    <?php endif; ?>
	<?php endif; ?>
<?php endif; ?>

</div>