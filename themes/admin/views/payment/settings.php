<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Payment', 'admin/payment/settings');
        \Breadcrumb::set('Settings', 'admin/payment/settings');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Payment: Settings</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/settings/option/_action_links'); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/payment/_navbar_links'); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'form-horizontal')); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Settings</h3>
                </div>
                <div class="panel-body">
                    <!-- Main Content Holder -->
                    <?php
                        $hold_enable_partial_payment = (isset($settings['enable_partial_payment']) ? $settings['enable_partial_payment'] : 0);
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Enable partial payment: </label>
                        <div class="col-md-4">
                            <label><?php echo \Form::checkbox('enable_partial_payment', 1, \Input::post('enable_partial_payment', $hold_enable_partial_payment )); ?> Yes</label>
                        </div>
                    </div>
                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success right" type="submit" name="save" value="1"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <?php echo \Form::close(); ?>

        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>




