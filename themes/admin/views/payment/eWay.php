<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Payment', 'admin/payment/eWay');
        \Breadcrumb::set('eWay', 'admin/payment/eWay');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Payment: eWay</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/settings/option/_action_links'); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/payment/_navbar_links'); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'form-horizontal')); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">eWay Details</h3>
                </div>
                <div class="panel-body">
                    <!-- Main Content Holder -->
                    <?php
                    $hold_activate = (isset($settings['activate']) ? $settings['activate'] : 0);
                    $hold_name = (isset($settings['name']) ? $settings['name'] : '');
                    $hold_code = (isset($settings['code']) ? $settings['code'] : '');
                    $hold_mode = (isset($settings['mode']) ? $settings['mode'] : 1);
                    $hold_customerId_test = (isset($settings['customerId_test']) ? $settings['customerId_test'] : '');
                    $hold_username_test = (isset($settings['username_test']) ? $settings['username_test'] : '');
                    $hold_password_test = (isset($settings['password_test']) ? $settings['password_test'] : '');
                    $hold_customerId_live = (isset($settings['customerId_live']) ? $settings['customerId_live'] : '');
                    $hold_username_live = (isset($settings['username_live']) ? $settings['username_live'] : '');
                    $hold_password_live = (isset($settings['password_live']) ? $settings['password_live'] : '');
                    ?>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Activate: </label>
                        <div class="col-md-4">
                            <label><?php echo \Form::checkbox('activate', 1, \Input::post('activate', $hold_activate )); ?> Yes</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Name:  <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input class="form-control" name="name" value="<?php echo \Input::post('name', $hold_name ) ?>" type="text" id="form_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Code:  <span class="text-danger">*</span></label>
                        <div class="col-md-4">
                            <input class="form-control" name="code" value="<?php echo \Input::post('code', $hold_code ) ?>" type="text" id="form_code">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Mode: </label>
                        <div class="col-md-4">
                           <div class="form-control" style="height:auto;">
                               <ul class="list-inline">
                                   <li>
                                       <label class="radio radio-inline">
                                           <input  name="mode" value="<?php echo \Input::post('mode', 0, $hold_mode) ?>" <?php echo \Input::post('mode') ? '' : 'checked="checked"'; ?> type="radio" id="form_mode"> Test
                                       </label>
                                   </li>
                                   <li>
                                       <label class="radio radio-inline">
                                           <?php echo \Form::radio('mode', 1, \Input::post('mode', $hold_mode)); ?>Live
                                       </label>
                                   </li>
                               </ul>
                           </div>
                        </div>
                    </div>
                    <div class="credentials 0" <?php echo \Input::post('mode', $hold_mode) == 0 ? 'style="display:block"' : 'style="display:none"'; ?> >
                        <div class="form-group">
                            <label class="col-md-3 control-label">Customer ID:  <span class="text-danger">*</span></label>
                            <div class="col-md-4">
                                <input class="form-control" name="customerId_test" value="<?php echo \Input::post('customerId_test', $hold_customerId_test) ?>" type="text" id="form_customerId_test">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">API Key:  <span class="text-danger">*</span></label>
                            <div class="col-md-4">
                                <input class="form-control" name="username_test" value="<?php echo \Input::post('username_test', $hold_username_test) ?>" type="text" id="form_username_test">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">API Password:  <span class="text-danger">*</span></label>
                            <div class="col-md-4">
                                <input class="form-control" name="password_test" value="<?php echo \Input::post('password_test', $hold_password_test) ?>" type="text" id="form_password_test">
                            </div>
                        </div>
                    </div>
                    <div class="credentials 1" <?php echo \Input::post('mode', $hold_mode) == 1 ? 'style="display:block"' : 'style="display:none"'; ?> >
                        <div class="form-group">
                            <label class="col-md-3 control-label">Customer ID:  <span class="text-danger">*</span></label>
                            <div class="col-md-4">
                                <input class="form-control" name="customerId_live" value="<?php echo \Input::post('customerId_live', $hold_customerId_live) ?>" type="text" id="form_customerId_live">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">API Key:  <span class="text-danger">*</span></label>
                            <div class="col-md-4">
                                <input class="form-control" name="username_live" value="<?php echo \Input::post('username_live', $hold_username_live) ?>" type="text" id="form_username_live">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">API Password:  <span class="text-danger">*</span></label>
                            <div class="col-md-4">
                                <input class="form-control" name="password_live" value="<?php echo \Input::post('password_live', $hold_password_live) ?>" type="text" id="form_password_live">
                            </div>
                        </div>
                    </div>
                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success right" type="submit"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <?php echo \Form::close(); ?>

        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>




