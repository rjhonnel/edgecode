<div class="panel-nav-holder">
    <div class="btn-group">
        <a href="<?php echo \Uri::create('admin/payment/paypal'); ?>" class="btn btn-lg btn-default active"><i class="fa fa-paypal"></i> Paypal</a>
        <a href="<?php echo \Uri::create('admin/payment/securePay'); ?>" class="btn btn-lg btn-default"><i class="fa fa-lock"></i> SecurePay</a>
        <a href="<?php echo \Uri::create('admin/payment/eWay'); ?>" class="btn btn-lg btn-default"><i class="fa fa-credit-card"></i> eWay</a>
        <a href="<?php echo \Uri::create('admin/payment/settings'); ?>" class="btn btn-lg btn-default"><i class="fa fa-gear"></i> Settings</a>
    </div>
</div>

<style>
    .panel-nav-holder .btn {
        min-width: 160px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
        if($(".btn-group").length > 0)
        {
            $(".btn-group a.active").removeClass('active');
            $(".btn-group a").each(function(){
                if(($(this).attr("href") == uri_current) ||
                    (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                {
                    $(this).addClass('active');
                }
            });
        }
    });
</script>