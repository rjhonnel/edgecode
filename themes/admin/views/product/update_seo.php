<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Product Manager', 'admin/product/list');
		\Breadcrumb::set($product->title, 'admin/product/update/' . $product->id);
		\Breadcrumb::set('Meta Content');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Product: Meta Content</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/_action_links', array('set_save_seo' => 1 ) ); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Page Optimisation / SEO</h3>

				</div>
				<div class="panel-body form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label">H1 Tag</label>
						<div class="col-sm-10">
							<?php echo \Form::input('h1_tag', \Input::post('h1_tag', $product->seo->h1_tag ?: $product->title), array('class'=>'form-control')); ?>
							<small class="form-instruction">Product Name is H1 by default. This will replace the Product Name as H1. </small>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Title  <span class="text-danger">*</span></label>
						<div class="col-sm-10">
							<?php echo \Form::input('meta_title', \Input::post('meta_title', $product->seo->meta_title ?: $product->title), array('class'=>'form-control')); ?>
							<small class="form-instruction">Recommendation: under 70 characters.</small>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Page URL</label>
						<div class="col-sm-10">
							<?php echo \Form::input('slug', \Input::post('slug', $product->seo->slug), array('class' => 'slug_source slug_target form-control')); ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Keywords</label>
						<div class="col-sm-10">
							<?php echo \Form::textarea('meta_keywords', \Input::post('meta_keywords', $product->seo->meta_keywords), array('rows' => 2, 'class'=>'form-control')); ?>
							<small class="form-instruction">Recommenation: 10 keywords separated by comma.</small><br>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10">
							<?php echo \Form::textarea('meta_description', \Input::post('meta_description', $product->seo->meta_description), array('rows' => 2, 'class'=>'form-control')); ?>
							<small class="form-instruction">Description of what the content of this page is about. Best practices:<br />&bull; Recommendation: 155 characters or less.<br />&bull; Include your main targeted keyword.<br />&bull; Ensure description is unique.</small><br>
						</div>
					</div>

				</div>
			</div>

			<!--div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Redirects</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label>301 Redirect from:</label>
						<?php echo \Form::input('redirect_301', \Input::post('redirect_301', $product->seo->redirect_301), array('class'=>'form-control')); ?>
					</div>
					<div class="form-group">
						<label> To (Canonical Links):</label>
						<?php echo \Form::input('canonical_links', \Input::post('canonical_links', $product->seo->canonical_links), array('class'=>'form-control')); ?>
					</div>
				</div>
			</div-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Robots</h3>
				</div>
				<div class="panel-body">
					<div class="form-group">
						<label>Robots Tag</label>
						<div class="form-control" style="height:auto">
							<ul class="list-inline">
								<li><label class="radio radio-inline"><?php echo \Form::radio('meta_robots_index', 1, \Input::post('meta_robots_index', $product->seo->meta_robots_index)); ?>Index (Default)</label></li>

								<li><label class="radio radio-inline"><?php echo \Form::radio('meta_robots_index', 0, \Input::post('meta_robots_index', $product->seo->meta_robots_index)); ?>No Index</label></li>
							</ul>
						</div>
					</div>
					<div class="form-group">
						<label>Robots Follow</label>
						<div class="form-control" style="height:auto">
							<ul class="list-inline">
								<li><label class="radio radio-inline"><?php echo \Form::radio('meta_robots_follow', 1, \Input::post('meta_robots_follow', $product->seo->meta_robots_follow)); ?>Follow (Default)</label></li>
								<li><label class="radio radio-inline"><?php echo \Form::radio('meta_robots_follow', 0, \Input::post('meta_robots_follow', $product->seo->meta_robots_follow)); ?>No Follow</label></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id' => 'save_button_down_seo', 'class' => 'btn btn-success')); ?>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
