<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Stock');
        \Breadcrumb::set('Stock Management Options', 'admin/product/stock/list');

        // If viewing category products show category title
        if(isset($category))
            \Breadcrumb::set($category->title);


        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Stock Options</h4>

                <div class="pull-right">
                </div>
            </header>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'stock-option')); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Options</h3>

                </div>
                <div class="panel-body">
                    <?php
                        $hold_manage_stock = (isset($option['manage_stock']) ? 1 : 0);
                        $hold_hide_out_of_stock = (isset($option['hide_out_of_stock']) ? 1 : 0);
                        $hold_do_not_allow_buy_out_of_stock = (isset($option['do_not_allow_buy_out_of_stock']) ? 1 : 0);
                    ?>

                    <div class="row">
                        <span class="col-sm-2">&nbsp;</span>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <label>
                                    <?php echo \Form::checkbox('manage_stock', 1, \Input::post('manage_stock', $hold_manage_stock)); ?>
                                    Manage Stock:
                                </label>
                            </div>

                            <div id="manage_stock_option_box" >
                                <div class="form-group">
                                    <label>
                                        <?php echo \Form::checkbox('hide_out_of_stock', 1, \Input::post('hide_out_of_stock', $hold_hide_out_of_stock)); ?>
                                        Hide Out of Stock products
                                    </label>
                                </div>

                                <div class="form-group">
                                    <label>
                                        <?php echo \Form::checkbox('do_not_allow_buy_out_of_stock', 1, \Input::post('do_not_allow_buy_out_of_stock', $hold_do_not_allow_buy_out_of_stock)); ?>
                                        Do not allow customer to buy if Out of Stock
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>



                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-2">&nbsp;</label>
                            <div class="col-sm-4">
                                <button class="btn btn-success" type="submit" name="save" value="1"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>


                </div>
            </div>

            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
