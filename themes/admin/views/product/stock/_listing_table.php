<div class="top-filter-holder">
    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
    <?php
    echo \Theme::instance()->view('views/_partials/search_filters', array(
        'pagination' => $pagination,
        'status' => $status,
        'module' => 'product code or',
        'options' => array('category_id'),
    ), false);
    ?>
    <?php echo \Form::close(); ?>

    <div class="form-inline show-table-filter">
        <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
            <label>Show entries:</label>
            <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
        <?php echo \Form::close(); ?>
    </div>

</div>

<?php echo \Form::open(array('action' => \Uri::create(\Uri::admin('current'), array(), \Input::get()), 'method' => 'post')); ?>

<table rel="<?php echo \Uri::create('admin/product/sort/product'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Product Name</th>
        <th scope="col" class="center" style="width: 12%;">Category</th>
        <th scope="col">Code</th>
        <th scope="col" class="center">Attribute</th>
        <th scope="col" class="center" style="width: 70px;">Qty</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item):

        $item = (Object)$item;

        //if(\Input::get() && isset($_GET['title']))
        //$check_attribute = DB::query('SELECT * FROM product_attributes a where a.product_code like "%'.$_GET['title'].'%" and a.product_id = '.$item->id)->execute();
        //else
        $check_attribute = DB::query('SELECT * FROM product_attributes a where a.product_id = '.$item->id)->execute();

        $hold_category = DB::query('SELECT b.title FROM product_to_categories a join product_categories b on a.category_id = b.id where a.product_id = '.$item->id)->execute();

        if(count($check_attribute)==1 && $check_attribute[0]['attribute_group_id']==0):

            $hold_stock = DB::query('SELECT b.id, b.stock_quantity FROM product a join product_attributes b on a.id = b.product_id where a.id = '.$item->id)->execute();
            ?>

            <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
                <td><?php echo $item->title; ?></td>
                <td><?php echo $hold_category[0]['title'] ?></td>
                <td><?php echo $item->code; ?></td>
                <td  width="300"><?php echo 'No Attributes'; ?></td>
                <td width="20">
                    <?php
                    echo \Form::input('stock_quantity['.$hold_stock[0]['id'].']', $hold_stock[0]['stock_quantity'], array('class' => 'medium_txt_input ba_retail_price form-control'));
                    ?>
                </td>
            </tr>
        <?php else:

            $count = 0;
            foreach ($check_attribute as $key => $value):
                ?>
                <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
                    <td><?php echo $count==0 ? $item->title : ''; ?></td>
                    <td><?php echo $count==0 ? $hold_category[0]['title'] : ''; ?></td>
                    <td class="noresize"><?php echo $value['product_code']; ?></td>
                    <td>
                        <?php
                        $attribute_list = (json_decode($value['attributes'], TRUE));
                        $attribute_list_hold = array();

                        foreach ($attribute_list as $key => $value1) {
                            $attribute_list_hold[] = DB::query('SELECT a.title FROM attribute_options a where a.attribute_id = '.$key.' and a.id = '.$value1)->execute();
                        }

                        $count_attr = count($attribute_list_hold);
                        $count_attr_count = 0;
                        foreach ($attribute_list_hold as $key => $value3) {
                            foreach ($value3[0] as $key => $value2) {
                                echo $value2;
                                if($count_attr-1 != $count_attr_count)
                                    echo " | ";
                                $count_attr_count++;
                            }
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        echo \Form::input('stock_quantity['.$value['id'].']', $value['stock_quantity'], array('class' => 'medium_txt_input ba_retail_price form-control'));
                        ?>
                    </td>
                </tr>

                <?php
                $count++;
            endforeach;
            ?>

        <?php endif; ?>

    <?php endforeach; ?>

    <?php if(empty($items)): ?>

        <tr class="nodrag nodrop">
            <td colspan="6" class="center"><strong>There are no items.</strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>

<div class="save_button_holder text-right">
    <?php echo \Form::button('update_stock', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down_stock', 'class' => 'btn btn-success', 'value' => '1')); ?>
</div>
<br>

<?php echo \Form::close(); ?>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>
