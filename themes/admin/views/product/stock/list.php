<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body <?php echo !isset($options['manage_stock']) || $options['manage_stock'] == 0 ? 'disable-edit' : ''  ?>">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Stock');
		\Breadcrumb::set('Stock Control', 'admin/product/stock/list');

		// If viewing category products show category title
		if(isset($category))
			\Breadcrumb::set($category->title);


		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Stock Control</h4>
                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/product/stock/_action_links', array('stock' => 1)); ?>
                </div>
			</header>



				<div class="<?php echo !isset($options['manage_stock']) || $options['manage_stock'] == 0 ? 'disable-edit' : ''  ?>">

					<?php
					// Load product listing table
					echo \Theme::instance()->view('views/product/stock/_listing_table',
						array(
							'pagination' 	=> $pagination,
							'items'			=> $items,
							'status'		=> $status,
						),
						false
					);
					?>

				</div>

				<?php echo \Theme::instance()->view('views/product/_tree_links'); ?>
			</div>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
