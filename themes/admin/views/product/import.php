<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Catalogue');
        \Breadcrumb::set('Product Manager', 'admin/product/list');
        \Breadcrumb::set('Bulk Add Products');
        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Bulk Add Products</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/product/_action_links', array()); ?>
                </div>
            </header>
            <?php echo \Form::open(array('action' => \Uri::admin('current'),'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>
            
            <p>You can use csv template provided. <a href="<?php echo \Uri::create('import/product.csv'); ?>" download>Download it here</a></p>

            <div class="form-group btn-file">
                <?php echo \Form::file('file',array('class' => 'form-control')); ?>
            </div>
            <div class="form-group">
                 <?php echo \Form::button('import', 'Import', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
            </div>
            <?php echo \Form::close(); ?>
        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>








