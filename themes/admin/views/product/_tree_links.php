						<?php 
							// Get categories
							$categories = \Product\Model_Category::find(function($query){ 
								$query->order_by('sort', 'asc');
								$query->order_by('id', 'asc');
							});

                            // Get brands
                            $brands = \Product\Model_Brand::find(function($query){
                                $query->order_by('sort', 'asc');
                                $query->order_by('id', 'asc');
                            });

							$product = isset($product) ? $product : false;
							$link = isset($link) ? $link : 'update';
							$selected = isset($selected) ? $selected : false;

                            // Define selected categories
							$selected_items = isset($selected_items) && is_array($selected_items) ? $selected_items : (isset($product->categories) ? $product->categories : array());
							
							// Prepare array values
							foreach($selected_items as $key => $selected_item)
							{
								$selected_items[$key] = is_object($selected_item) ? $selected_item->id : $selected_item;
							}

                            // Define selected brands
                            $selected_items_brand = isset($selected_items_brand) && is_array($selected_items_brand) ? $selected_items_brand : (isset($product->brands) ? $product->brands : array());

                            // Prepare array values
                            foreach($selected_items_brand as $key => $selected_item)
                            {
                                $selected_items_brand[$key] = is_object($selected_item) ? $selected_item->id : $selected_item;
                            }

                            // Allow adding products only in last child category
                            $add_to_last_child = false;
                            
                            if($add_to_last_child) $last_child = array('style' => 'display: none;', 'disabled' => 'disabled');
                            else $last_child = array();
                           
						?>
						
						<div class="panel panel-default" style="<?php echo $product ? '' : 'display:none;'; ?>">
                            <div class="panel-heading">
                                <h4 class="panel-title pull-left"><?php echo $product ? 'Categories' : 'Nested Products'; ?></h4>
                                <div id="sidetreecontrol" class="sidetreecontrol pull-right"><a href="#">Collapse All</a><a href="#">Expand All</a></div>
                            </div>
                            <div class="panel-body">
                                <div id="sidetree">
                                
                                	<?php if(!$product && empty($categories)): ?>
                                		<div class="wide"><span class="req">Note: </span> There are no categories yet.</div>
                                	<?php else: ?>
                                	
	                                    <ul class="treeview" id="tree">
	                                    	
	                                    	<?php
	                                    		if(!empty($categories))
	                                    		{
		                                    		$list_subcategories = function($category_item) use ($link, &$list_subcategories, $product, $selected, $selected_items, $last_child)
		                                    		{
		                                    			?><ul><?php
		                                    			foreach($category_item->children as $child)
		                                    			{
		                                    				?>
		                                    					<li>
		                                    						<?php echo !empty($child->children) ? '<div class="hitarea"></div>' : ''; ?>
		                                    						<div class="checkbox_link_holder">
			                                    						<?php if($product): ?>
			                                    							<?php echo \Form::checkbox('cat_ids[]', $child->id, in_array($child->id, \Input::post('cat_ids', $selected_items)), !empty($child->children) ? $last_child : array()); ?>
			                                    						<?php endif; ?>
			                                    						<a href="<?php echo \Uri::create('admin/product/list/' . $child->id.'/category'); ?>" <?php echo $selected == $child->id ? 'class="active"' : ''; ?>>
			                                    							<?php echo $child->title; ?><?php echo !empty($child->products) ? ' <span class="tree_count">('.count($child->products).')</span>' : ''; ?>
			                                    						</a>
			                                    					</div><?php
		                                    				if(!empty($child->children)) 
		                                    					$list_subcategories($child);
		                                    				else
		                                    					?></li><?php
		                                    					
		                                    			}
		                                    			?></ul><?php
		                                    		};
		                                    	
		                                    		foreach($categories as $key => $category_item)
		                                    		{
		                                    			// Only root categories in first pass
		                                    			if($category_item->parent_id == 0)
		                                    			{
			                                    			?>
			                                    				<li>
			                                    					<?php echo !empty($category_item->children) ? '<div class="hitarea"></div>' : ''; ?>
			                                    					<div class="checkbox_link_holder">
				                                    					<?php if($product || true): ?>
			                                    							<?php echo \Form::checkbox('cat_ids[]', $category_item->id, in_array($category_item->id, \Input::post('cat_ids', $selected_items)), !empty($category_item->children) ? $last_child : array()); ?>
				                                    					<?php endif; ?>
			                                    						<a href="<?php echo \Uri::create('admin/product/list/' . $category_item->id.'/category'); ?>" <?php echo $selected == $category_item->id ? 'class="active"' : ''; ?>>
			                                    							<?php echo $category_item->title; ?><?php echo !empty($category_item->products) ? ' <span class="tree_count">('.count($category_item->products).')</span>' : ''; ?>
			                                    						</a>
			                                    					</div><?php
		                                    				if(!empty($category_item->children)) 
		                                    					$list_subcategories($category_item);
		                                    				else
		                                    					?></li><?php
		                                    			}
		                                    		}
	                                    		}
	                                    	?>
	                                        
	                                    </ul>
                                        

									<?php endif; ?>
                                </div>


                            </div>
                            <?php if($product): ?>
                                <div class="panel-footer">
                                    <small class="wide"><span class="req">Note: </span> Please select categories for the product. You may assign product to any number of categories.</small>
                                </div>
                            <?php endif; ?>

                        </div>

                        <div class="panel panel-default" style="<?php echo $product ? '' : 'display:none;'; ?>">
                            <div class="panel-heading">
                                <h4 class="panel-title pull-left"><?php echo $product ? 'Brands' : 'Nested Products'; ?></h4>
                            </div>

                            <div class="panel-body">
                                <div id="sidetree-brand">

                                    <?php if(!$product && empty($brands)): ?>
                                        <div class="wide"><span class="req">Note: </span> There are no brands yet.</div>
                                    <?php else: ?>

                                        <ul class="treeview" id="tree-brand">

                                            <?php
                                                if(!empty($brands))
                                                {
                                                    $list_subcategories = function($brand_item) use ($link, &$list_subcategories, $product, $selected, $selected_items_brand, $last_child)
                                                    {
                                                        ?><ul><?php
                                                        foreach($brand_item->children as $child)
                                                        {
                                                            ?>
                                                                <li>
                                                                    <?php echo !empty($child->children) ? '<div class="hitarea"></div>' : ''; ?>
                                                                    <div class="checkbox_link_holder">
                                                                        <?php if($product): ?>
                                                                            <?php echo \Form::checkbox('brand_ids[]', $child->id, in_array($child->id, \Input::post('brand_ids', $selected_items_brand)), !empty($child->children) ? $last_child : array()); ?>
                                                                        <?php endif; ?>
                                                                        <a href="<?php echo \Uri::create('admin/product/list/' . $child->id.'/brand'); ?>" <?php echo $selected == $child->id ? 'class="active"' : ''; ?>>
                                                                            <?php echo $child->title; ?><?php echo !empty($child->products) ? ' <span class="tree_count">('.count($child->products).')</span>' : ''; ?>
                                                                        </a>
                                                                    </div><?php
                                                            if(!empty($child->children))
                                                                $list_subcategories($child);
                                                            else
                                                                ?></li><?php

                                                        }
                                                        ?></ul><?php
                                                    };

                                                    foreach($brands as $key => $brand_item)
                                                    {
                                                        // Only root brands in first pass
                                                        if($brand_item->parent_id == 0)
                                                        {
                                                            ?>
                                                                <li>
                                                                    <?php echo !empty($brand_item->children) ? '<div class="hitarea"></div>' : ''; ?>
                                                                    <div class="checkbox_link_holder">
                                                                        <?php if($product || true): ?>
                                                                            <?php echo \Form::checkbox('brand_ids[]', $brand_item->id, in_array($brand_item->id, \Input::post('brand_ids', $selected_items_brand)), !empty($brand_item->children) ? $last_child : array()); ?>
                                                                        <?php endif; ?>
                                                                        <a href="<?php echo \Uri::create('admin/product/list/' . $brand_item->id.'/brand'); ?>" <?php echo $selected == $brand_item->id ? 'class="active"' : ''; ?>>
                                                                            <?php echo $brand_item->title; ?><?php echo !empty($brand_item->products) ? ' <span class="tree_count">('.count($brand_item->products).')</span>' : ''; ?>
                                                                        </a>
                                                                    </div><?php
                                                            if(!empty($brand_item->children))
                                                                $list_subcategories($brand_item);
                                                            else
                                                                ?></li><?php
                                                        }
                                                    }
                                                }
                                            ?>

                                        </ul>

                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <?php if($product): ?>

                            <?php $property_groups = \Product\Model_Group::find_by_type('property'); ?>
                            
                            <div class="side_tree_holder" style="display: none;">
                                <div class="tree_heading">
                                    <h4>Property Groups</h4>
                                    <div id="sidetreecontrol2" class="sidetreecontrol"><a href="#">Collapse All</a><a href="#">Expand All</a></div>
                                </div>
                                <div class="tree_content">
                                    <div id="sidetree">

                                        <?php if(!$product && empty($property_groups)): ?>
                                            <div class="wide"><span class="req">Note: </span> There are no property groups yet.</div>
                                        <?php else: ?>

                                            <ul class="treeview" id="tree2">

                                                <?php
                                                    if(!empty($property_groups))
                                                    {
                                                        
                                                        // Define selected categories
                                                        $selected_items = $product ? $product->groups : array();

                                                        // Prepare array values
                                                        foreach($selected_items as $key => $selected_item)
                                                        {
                                                            $selected_items[$key] = is_object($selected_item) ? $selected_item->id : $selected_item;
                                                        }
                                                        
                                                        $list_subgroups = function($group_item) use ($link, &$list_subgroups, $product, $selected, $selected_items)
                                                        {
                                                            ?><ul><?php
                                                            foreach($group_item->children as $child)
                                                            {
                                                                ?>
                                                                    <li>
                                                                        <?php echo !empty($child->children) ? '<div class="hitarea"></div>' : ''; ?>
                                                                        <div class="checkbox_link_holder">
                                                                            <?php if($product): ?>
                                                                                <?php echo \Form::checkbox('group_ids[]', $child->id, in_array($child->id, \Input::post('group_ids', $selected_items)), !empty($child->children) ? array('style' => 'display: none;', 'disabled' => 'disabled') : array()); ?>
                                                                            <?php endif; ?>
                                                                            <a href="<?php echo \Uri::create('admin/product/list/' . $child->id); ?>" <?php echo $selected == $child->id ? 'class="active"' : ''; ?>>
                                                                                <?php echo $child->title; ?><?php echo !empty($child->products) ? ' <span class="tree_count">('.count($child->products).')</span>' : ''; ?>
                                                                            </a>
                                                                        </div><?php
                                                                if(!empty($child->children)) 
                                                                    $list_subgroups($child);
                                                                else
                                                                    ?></li><?php

                                                            }
                                                            ?></ul><?php
                                                        };

                                                        foreach($property_groups as $key => $group_item)
                                                        {
                                                            // Only root categories in first pass
                                                            if($group_item->parent_id == 0)
                                                            {
                                                                ?>
                                                                    <li>
                                                                        <?php echo !empty($group_item->children) ? '<div class="hitarea"></div>' : ''; ?>
                                                                        <div class="checkbox_link_holder">
                                                                            <?php if($product): ?>
                                                                                <?php echo \Form::checkbox('group_ids[]', $group_item->id, in_array($group_item->id, \Input::post('group_ids', $selected_items)), !empty($group_item->children) ? array('style' => 'display: none;', 'disabled' => 'disabled') : array()); ?>
                                                                            <?php endif; ?>
                                                                            <a href="<?php echo \Uri::create('admin/product/list/' . $group_item->id); ?>" <?php echo $selected == $group_item->id ? 'class="active"' : ''; ?>>
                                                                                <?php echo $group_item->title; ?><?php echo !empty($group_item->products) ? ' <span class="tree_count">('.count($group_item->products).')</span>' : ''; ?>
                                                                            </a>
                                                                        </div><?php
                                                                if(!empty($group_item->children)) 
                                                                    $list_subgroups($group_item);
                                                                else
                                                                    ?></li><?php
                                                            }
                                                        }
                                                    }
                                                ?>

                                            </ul>

                                            <?php if($product): ?>
                                                <div class="wide"><span class="req">Note: </span> Please select property groups for the product. You may assign product to any number of property groups.</div>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="side_tree_holder">
                                <div class="tree_heading">
                                    <h4>Pricing Groups</h4>
                                </div>
                                <div class="tree_content">
                                    <?php //echo \Form::select('group', \Input::post('group', $product->pricing_group ? $product->pricing_group->id : ''), \Product\Model_Group::fetch_pair('id', 'title', array(), 'Select', \Product\Model_Group::find_by_type('pricing')), array('class' => 'select_init')); ?>
                                </div>
                            </div> -->

                        <?php endif;  ?>