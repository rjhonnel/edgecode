            <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Products')); ?>
            
            <?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>
                
            <!-- Content -->
		    <div class="content_wrapper">
		    	<div class="content_holder">
		    		
		    		<div class="elements_holder">
		    		
                        <div class="row-fluid breadcrumbs_holder">
                            <div class="breadcrumbs_position">
                                <?php 
                                    \Breadcrumb::set('Home', 'admin/dashboard');
                                    \Breadcrumb::set('Catalogue');
                                    \Breadcrumb::set('Manage Products', 'admin/product/list');
                                    \Breadcrumb::set('Edit Product');
                                    \Breadcrumb::set($product->title, 'admin/product/update/' . $product->id);
                                    \Breadcrumb::set('Upsell Products');

                                    echo \Breadcrumb::create_links();
                                ?>
                                
                                <?php echo \Theme::instance()->view('views/product/_action_links'); ?>
                            </div>
                        </div>
					    
					    <div class="row-fluid">
					    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                            
		                    <!-- Main Content Holder -->
		                    <div class="content">
								
		                        <div class="panel">
		                        	<div class="panelHeader">
		                                <h4>Edit Upsell Products</h4>
		                            </div>
		                            <div class="panelContent">
		                            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_b_10">
		                            		<tr>
		                            			<td class="span7 p_r_10">
		                            				 <span class="text-danger">*</span> You can select and drag items from left to right, or select and click on button above.<br /><br />
		                            			</td>
		                            			<td class="span5 sort_message_container"></td>
		                            		</tr>
		                            	</table>
	                            	
                                        <table width="100%">
                                            <tr>
                                                <td width="49.5%" valign="top" class="dual_filters">

                                                    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
                                                        <table class="controls">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <h4>Select Upsell Products</h4>

                                                                        <div class="items_per_page_holder">
                                                                            <label>Show entries:</label>
                                                                            <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'select_init items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>

                                                        <?php 
                                                            echo \Theme::instance()->view('views/_partials/search_filters', array(
                                                                'pagination' => $pagination,
                                                                'status' => $status,
                                                                'module' => 'product',
                                                                'options' => array('status', 'category_id'),
                                                            ), false);
                                                        ?>
                                                    <?php echo \Form::close(); ?>
                                                </td>

                                                <td width="1%" class="noresize"></td>

                                                <td width="49.5%" valign="top" class="wide_search_filters gray_gradient search_holder">
                                                    <table class="controls">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    <h4>Upsell Products</h4>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                    <?php 
                                                        echo \Theme::instance()->view('views/_partials/search_filters', array(
                                                            'pagination' => $pagination,
                                                            'status' => $status,
                                                            'module' => 'product',
                                                            'options' => array(),
                                                            'show_reset' => false,
                                                        ), false);
                                                    ?>

                                                    <div class="formRow info_table_loading" style="display: none;">
                                                        <div class="search_loader">
                                                            <!-- This is loaded while user is typing -->
                                                            <div class="clear"></div>
                                                            <div class="m_t_15"></div>
                                                            <center style="margin: 0px 0 19px 0;">Searching for products...</center>
                                                            <center><i class="fa fa-spinner fa-spin"></i></center>
                                                        </div>
                                                    </div>
                                                </td>	
                                            </tr>
                                            <tr>
                                                <?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>
                                                    <td width="49.5%" valign="top">
                                                        <div class="row-fluid wide_all">

                                                            <ul class="default_list">
                                                                <li><a href="" onclick="return false;" class="select_all">Select All</a></li>
                                                                <li class="separator">/</li>
                                                                <li><a href="" onclick="return false;" class="unselect_all">Unselect All</a></li>
                                                                <li class="separator">/</li>
                                                                <li><a href="" onclick="return false;" class="select_qty"><span>0</span> items selected</a></li>
                                                            </ul>

                                                            <button name="add" value="add" type="submit" class="btn btn-small btn-primary pull-right">
                                                                <i class="icon-plus icon-white"></i>Add Selected
                                                            </button>
                                                        </div>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 separated scrollable_body">
                                                            <thead>
                                                                <tr class="blueTableHead">
                                                                    <th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'products[add]'); ?></th>
                                                                    <th scope="col" class="noresize">Code</th>
                                                                    <th scope="col">Title</th>
                                                                    <th scope="col" class="noresize">Price</th>
                                                                    <th scope="col" class="noresize show_on_add" style="display: none;">Discount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="drag_table remove">
                                                                <?php if(!empty($product->not_upsell_products)): ?>
                                                                    <?php foreach($product->not_upsell_products as $not_upsell): ?>

                                                                        <tr>
                                                                            <td class="noresize"><?php echo \Form::checkbox('products[add][]', $not_upsell->id); ?></td>
                                                                            <td class="noresize">11554</td>
                                                                            <td><?php echo $not_upsell->title; ?></td>
                                                                            <td class="noresize">$150.00</td>
                                                                            <td class="show_on_add noresize" style="display: none;">
                                                                                <div style="white-space: nowrap;">
                                                                                    % <?php echo \Form::input('discount[' . $not_upsell->id . ']', '', array('class' => 'w_40', 'disabled' => 'disabled')); ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                    <?php endforeach; ?>
                                                                    <?php $no_items = 'style="display: none;"'?>
                                                                <?php else: ?>
                                                                    <?php $no_items = ''?>
                                                                <?php endif; ?>

                                                                <tr class="no_items" <?php echo $no_items; ?>>
                                                                    <td class="noresize center" colspan="4">There are no more products to add</td>
                                                                </tr>

                                                            </tbody>                    
                                                        </table>

                                                        <div class="pagination_holder">
                                                            <div class="pagination pull-right">
                                                                <?php echo $pagination->render(); ?>
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </td>

                                                    <td width="1%" class="noresize"></td>

                                                    <td width="49.5%" valign="top">
                                                        <div class="row-fluid wide_all">
                                                            <ul class="default_list">
                                                                <li><a href="" onclick="return false;" class="select_all">Select All</a></li>
                                                                <li class="separator">/</li>
                                                                <li><a href="" onclick="return false;" class="unselect_all">Unselect All</a></li>
                                                                <li class="separator">/</li>
                                                                <li><a href="" onclick="return false;" class="select_qty"><span>0</span> items selected</a></li>
                                                            </ul>

                                                            <button name="remove" value="remove" type="submit" class="btn btn-small btn-primary pull-right">
                                                                <i class="icon-minus icon-white"></i>Remove Selected
                                                            </button>
                                                        </div>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 separated scrollable_body search_table">
                                                            <thead>
                                                                <tr class="blueTableHead">
                                                                    <th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'products[remove]'); ?></th>
                                                                    <th scope="col" class="noresize">Code</th>
                                                                    <th scope="col">Title</th>
                                                                    <th scope="col" class="noresize">Price</th>
                                                                    <th scope="col" class="noresize show_on_add">Discount</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="drag_table add">
                                                                <?php if(!empty($product->upsell_products)): ?>
                                                                    <?php foreach($product->upsell_products as $upsell): ?>
                                                                        <tr>
                                                                            <td class="noresize"><?php echo \Form::checkbox('products[remove][]', $upsell->id); ?></td>
                                                                            <td class="noresize">11554</td>
                                                                            <td><?php echo $upsell->title; ?></td>
                                                                            <td class="noresize">$150.00</td>
                                                                            <td class="show_on_add noresize">
                                                                                <div style="white-space: nowrap;">
                                                                                    % <?php echo \Form::input('discount[' . $upsell->id . ']', $upsell->discount, array('class' => 'w_40')); ?>
                                                                                </div>
                                                                            </td>
                                                                        </tr>

                                                                    <?php endforeach; ?>
                                                                    <?php $no_items = 'style="display: none;"'; ?>
                                                                <?php else: ?>
                                                                    <?php $no_items = ''; ?>
                                                                <?php endif; ?>

                                                                <tr class="no_items" <?php echo $no_items; ?>>
                                                                    <td class="noresize center" colspan="5">There are no upsell products</td>
                                                                </tr>
                                                            </tbody>                    
                                                        </table>
                                                        <button name="save" value="save" type="submit" class="m_t_15 btn btn-success btn-small pull-right">
                                                            <i class="icon-check icon-white"></i> Save discounts
                                                        </button>
                                                    </td>
                                                <?php echo \Form::close(); ?>
                                            </tr>
                                        </table>
		                            </div>
		                        </div>
		                        
		                    </div><!-- EOF Main Content Holder -->
		                    
                            <!-- Sidebar Holder -->
<!--		                    <div class="sidebar">
		                        <?php echo \Theme::instance()->view('views/product/category/_tree_links'); ?>
		                    </div>-->
                            <!-- EOF Sidebar Holder -->
                            
		                    <div class="clear"></div>
					    	
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <!-- EOF Content -->
			    
		    <script>
				var product_id = <?php echo $product->id; ?>;
			</script>
			<?php echo \Theme::instance()->asset->js('product/upsell/drag_lists.js'); ?>
           	