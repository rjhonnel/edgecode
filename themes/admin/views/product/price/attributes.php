<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Catalogue');
        \Breadcrumb::set('Product Manager', 'admin/product/list');
        \Breadcrumb::set($product->title, 'admin/product/update/' . $product->id);
        \Breadcrumb::set('Prices and Attributes');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Edit Product: Prices and Attributes</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/product/_action_links', array( 'set_save_price' => 1 ) ); ?>
                </div>
            </header>



            <?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>

            <div class="well clearfix" style="padding: 10px;margin-bottom: 10px;">
                <p class="pull-left mt-5px">Active Attribute Group: <u><?php echo $product->active_attribute_group ? $product->active_attribute_group[0]->title : 'N/A'; ?></u> &nbsp;|&nbsp; Combinations: <u><?php echo $number_of_combinations; ?></u></p>
                <div class="form-inline pull-right">
                    <?php
                        echo \Form::open(array(
                            'action' =>  \Uri::create(\Uri::admin('current'), array(), \Input::get()),
                            'enctype' => 'multipart/form-data',
                            'class' => 'main_form'
                        ));
                        echo \Form::hidden('product_id', $product->id);
                        echo \Form::hidden('attribute_group_id', \Input::get('attribute_group', 0));
                    ?>
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1">Price</span>
                            <input type="text" class="form-control" placeholder="Price" id="all_price_attr" name="price_apply_all" />
                        </div>
                        <div class="form-control" style="padding: 0 10px">
                            <label class="checkbox enable_price_attr_btn" style="position: relative;top: 2px;">
                                <?php echo \Form::checkbox('enable_all_price_attr', 1, 0); ?> Enable
                            </label>
                        </div>
                        <input type="hidden" name="apply_all_save" value="0">
                        <button name="apply_all" value="apply_all" type="button" class="btn btn-sm btn-default apply_all" data-type="">
                            <i class="fa fa-check"></i> Apply to All
                        </button>
                    <?php echo \Form::close(); ?>
                </div>

            </div>

            
                <?php echo \Form::open(array('action' =>  \Uri::admin('current'), 'method' => 'get')); ?>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="filter-holder">
                            <div class="panel-utility form-inline">
                                    <?php echo \Form::select('attribute_group', \Input::get('attribute_group'), $attribute_groups_select, array('class' => 'form-control onchange_submit')); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-inline pull-right">
                            <label>Show entries:</label>
                            <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                        </div>
                    </div>
                    </div>
                <?php echo \Form::close(); ?>
            

            <?php
                echo \Form::open(array(
                    'action' =>  \Uri::create(\Uri::admin('current'), array(), \Input::get()),
                    'enctype' => 'multipart/form-data',
                    'class' => 'main_form'
                ));
            ?>
            <?php if(!empty($delete_items)) echo \Form::hidden('delete_items', $delete_items);   ?>

            <?php // echo \Theme::instance()->view('views/_partials/action_header'); ?>

            <?php
            echo \Form::hidden('product_id', $product->id);
            echo \Form::hidden('attribute_group_id', \Input::get('attribute_group', 0));
            // echo \Form::hidden('user_group_id', \Input::get('user_group'));
            echo \Form::hidden('product_group_discount_id', \Input::get('tier_price'));
            echo \Form::hidden('price_type', $price_field);
            ?>

            <table class="table table-striped table-bordered">
                <thead>
                <tr class="blueTableHead">
                    <th width="10" scope="col">Default</th>
                    <th width="120">Code</th>
                    <th scope="col">Attributes</th>
                    <th width="90">Price</th>
                    <th width="90">
                        <?php echo \Arr::get($price_select, \Input::get('special', 3), null); ?>
                    </th>
                    <th width="90">Stock Qty</th>
                    <th width="120">Extra Delivery<br/> Charge</th>
                    <th width="30">Image</th>
                    <th width="30">Enable</th>
                </tr>
                </thead>
                <tbody>
                <?php if($listing): ?>
                    <?php if(!empty($combinations)): // && \Input::get('user_group') > 0 ?>
                        <?php foreach ($combinations as $key => $combination): ?>

                            <?php
                            $update = isset($update_items[$key]) && isset($items_db[$update_items[$key]]) ? $items_db[$update_items[$key]] : false ;
                            $attribute_price_id = false;
                            if(isset($update_items[$key])):
                                if($update && !empty($update->{$price_field})):
                                    $attribute_price_id = $update->{$price_field};
                                    $attribute_price_id = is_array($attribute_price_id) ? reset($attribute_price_id) : false;
                                    $attribute_price_id = $attribute_price_id ? $attribute_price_id->id : false;
                                endif;
                            endif;
                            ?>
                            <tr>
                                <td class="" align="center">
                                    <?php
                                    if($update):
                                        echo \Form::radio('default', $key, $update->default == 1 ? : false);
                                    else:
                                        echo \Form::radio('default', $key);
                                    endif;
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    if($update):
                                        echo \Form::input('product_code[' . $key . ']', $update->product_code, array('class' => 'form-control ba_product_code'));
                                    else:
                                        $hold_code = $product->code ? $product->code.'-'.($key+1) : null;
                                        echo \Form::input('product_code_new[' . $key . ']', $hold_code, array('class' => 'form-control ba_product_code'));
                                    endif;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if(isset($update_items[$key])) echo \Form::hidden('update_items[' . $key . ']', $update_items[$key]);
                                    echo \Form::hidden('attributes[' . $key . ']', $combinations_data[$key]);
                                    if(!empty($combination) && is_array($combination)):
                                        echo '<div class="attribute_values"><span>' . implode('</span> | <span>', $combination) . '</span></div>';
                                    endif;
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    if($update):
                                        echo \Form::input('retail_price[' . $key . ']', $update->retail_price, array('class' => 'form-control ba_retail_price'));
                                    else:
                                        echo \Form::input('retail_price_new[' . $key . ']', 0, array('class' => 'form-control ba_retail_price'));
                                    endif;
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    if($update):
                                        echo \Form::input($price_field . '[' . $key . ']', $update->sale_price, array('class' => 'form-control ba_special_price'));
                                    else:
                                        echo \Form::input($price_field . '_new[' . $key . ']', 0, array('class' => 'form-control ba_special_price'));
                                    endif;
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    if($update):
                                        echo \Form::input('stock_quantity[' . $key . ']', $update->stock_quantity, array('class' => 'form-control'));
                                    else:
                                        echo \Form::input('stock_quantity_new[' . $key . ']', 0, array('class' => 'form-control'));
                                    endif;
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    if($update):
                                        echo \Form::input('delivery_charge[' . $key . ']', $update->delivery_charge, array('class' => 'form-control ba_retail_price'));
                                    else:
                                        echo \Form::input('delivery_charge_new[' . $key . ']', 0, array('class' => 'form-control ba_retail_price'));
                                    endif;
                                    ?>
                                </td>
                                <td class="">
                                    <div class="td-thumb">
                                        <a class="btn-edit-sm edit_btn" data-id="<?php if(isset($update_items[$key])) echo $update_items[$key]; ?>" data-key="<?php echo $key; ?>">
                                            <?php if($update && !empty($update->cover_image)): ?>
                                                <img src="<?php echo \Helper::amazonFileURL('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $update->cover_image[0]->image); ?>" width="50" height="50" alt="<?php echo $update->cover_image[0]->alt_text; ?>"/>
                                            <?php else: ?>
                                                <i class="fa fa-picture-o"></i>
                                            <?php endif; ?>
                                        </a>
                                    </div>

                                </td>
                                <td class="">
                                    <label>
                                        <?php
                                        if($attribute_price_id):
                                            echo \Form::hidden('active[' . $key . ']', 0);
                                            echo \Form::checkbox('active[' . $key . ']', 1, isset($update->active) && $update->active ? true : false, array('class' => 'ba_active'));
                                        else:
                                            echo \Form::hidden('active_new[' . $key . ']', 0);
                                            echo \Form::checkbox('active_new[' . $key . ']', 1, isset($update) && (isset($update->active) && $update->active) ? true : false, array('class' => 'ba_active'));
                                        endif;
                                        ?>
                                        Yes
                                    </label>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>

                    <?php if(\Input::get('attribute_group', 0) == 0): // && \Input::get('user_group') > 0 ?>

                        <?php if(!empty($items_db)): ?>

                            <?php
                            $attribute_price_id = reset($items_db);
                            if(!empty($attribute_price_id->{$price_field})):
                                $attribute_price_id = $attribute_price_id->{$price_field};
                                $attribute_price_id = is_array($attribute_price_id) ? reset($attribute_price_id) : false;
                                $attribute_price_id = $attribute_price_id ? $attribute_price_id->id : false;
                            else:
                                $attribute_price_id = false;
                            endif;
                            ?>
                            <?php foreach ($items_db as $item_db): ?>

                                <tr>
                                    <td class="" align="center">
                                        <?php
                                        //echo \Form::radio('default', $item_db->id, $item_db->default == 1 ? : false);
                                        echo \Form::radio('default', $item_db->id, true);
                                        ?>
                                    </td>
                                    <td class="">
                                        <?php
                                        echo \Form::input('product_code[' . $item_db->id . ']', $product->code, array('class' => 'ba_product_code', 'disabled' => 'disabled' , 'style' => 'width:100%;'));
                                        ?>
                                    <td>
                                        <?php
                                        echo \Form::hidden('update_items[' . $item_db->id . ']', $item_db->id);
                                        echo \Form::hidden('attributes[' . $item_db->id . ']', null);
                                        ?>
                                    </td>
                                    <td class="">
                                        <?php
                                        echo \Form::input('retail_price[' . $item_db->id . ']', $item_db->retail_price, array('class' => 'form-control ba_retail_price'));
                                        ?>
                                    </td>
                                    <td class="">
                                        <?php
                                        //if($attribute_price_id):
                                        echo \Form::input($price_field . '[' . $item_db->id . ']', $item_db->sale_price, array('class' => 'form-control ba_special_price'));
                                        //else:
                                        //    echo \Form::input($price_field . '_new[' . $item_db->id . ']', null, array('class' => 'form-control ba_special_price'));
                                        //endif;
                                        ?>
                                    </td>
                                    <td class="">
                                        <?php
                                        echo \Form::input('stock_quantity[' . $item_db->id . ']', $item_db->stock_quantity, array('class' => 'form-control','style' => 'width:100%;'));
                                        ?>
                                    </td>
                                    <td class="">
                                        <?php
                                        echo \Form::input('delivery_charge[' . $item_db->id . ']', $item_db->delivery_charge, array('class' => 'form-control'));
                                        ?>
                                    </td>
                                    <td>
                                        <div class="small_image_preview">
                                            <a class="btn-edit-sm edit_btn" data-id="<?php echo $item_db->id; ?>" data-key="0">
                                                <?php if(!empty($item_db->cover_image)): ?>
                                                    <img src="<?php echo \Helper::amazonFileURL('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $item_db->cover_image[0]->image); ?>" width="50" height="50" alt="<?php echo $item_db->cover_image[0]->alt_text; ?>"/>
                                                <?php else: ?>
                                                    <span class="fa fa-picture-o"></span>
                                                <?php endif; ?>
                                            </a>
                                        
                                        </div>



                                    </td>
                                    <td class="">
                                        <div class="custom_checkbox">
                                            <?php
                                            echo \Form::hidden('active[' . $item_db->id . ']', 1);
                                            // echo \Form::checkbox('active[' . $item_db->id . ']', 1, null, array('class' => 'ba_active'));
                                            ?>
                                        </div>
                                    </td>
                                </tr>

                                <tr style="display: none;">
                                    <td colspan="9">

                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/page/sort/image/'); ?>" class="greyTable2 sortable files_table separated ">

                                            <?php if(\Config::get('details.image.multiple', false) || empty($accessory->images)): ?>
                                                <tr class="nodrop nodrag">
                                                    <td class="td-thumb">
                                                        <i class="fa fa-picture-o"></i>
                                                    </td>
                                                    <td class="upload">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td class="">Alt Text</td>
                                                                <td>
                                                                    <div class="input_holder">
                                                                        <?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1')); ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="">Choose Image</td>
                                                                <td>
                                                                    <?php echo \Form::file('image_new_1'); ?>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php endif; ?>

                                        </table>

                                    </td>
                                </tr>

                            <?php endforeach; ?>

                        <?php else: $key = 0; ?>

                            <tr>
                                <td class="" align="center">
                                    <?php
                                    //echo \Form::radio('default', $key);
                                    echo \Form::radio('default', $key, true);
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    echo \Form::input('product_code_new[' . $key . ']', $product->code, array('class' => 'ba_product_code', 'style' => 'width:85%;', 'disabled' => 'disabled'));
                                    ?>
                                <td>
                                    <?php
                                    echo \Form::hidden('attributes[' . $key . ']', null);
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    echo \Form::input('retail_price_new[' . $key . ']', 0, array('class' => 'form-control ba_retail_price'));
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    echo \Form::input($price_field . '_new[' . $key . ']', 0, array('class' => 'form-control ba_special_price'));
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    echo \Form::input('stock_quantity_new[' . $key . ']', 0, array('class' => 'form-control ba_retail_price'));
                                    ?>
                                </td>
                                <td class="">
                                    <?php
                                    echo \Form::input('delivery_charge_new[' . $key . ']', 0, array('class' => 'form-control ba_retail_price'));
                                    ?>
                                </td>
                                <td>
                                    <div class="td-thumb">
                                        <a class="btn-edit-sm edit_btn" data-id="" data-key="0">
                                            <i class="fa fa-picture-o"></i>
                                        </a>
                                    </div>

                                </td>
                                <td class="">
                                    <div class="custom_checkbox">
                                        <?php
                                        echo \Form::hidden('active_new[' . $key . ']', 1);
                                        //echo \Form::checkbox('active_new[' . $key . ']', 1, null, array('class' => 'ba_active'));
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td colspan="9">

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/page/sort/image/'); ?>" class="greyTable2 sortable files_table separated ">

                                        <?php if(\Config::get('details.image.multiple', false) || empty($accessory->images)): ?>
                                            <tr class="nodrop nodrag">
                                                <td class="td-thumb">
                                                    <i class="fa fa-picture-o"></i>
                                                </td>
                                                <td class="upload">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="">Alt Text</td>
                                                            <td>
                                                                <div class="input_holder">
                                                                    <?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1')); ?>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="">Choose Image</td>
                                                            <td>
                                                                <?php echo \Form::file('image_new_1'); ?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                    </table>
                                </td>
                            </tr>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>
                </tbody>

            </table>

            <div class="pagination-holder">
                <?php echo $pagination->render(); ?>
            </div>

            <?php if($listing): ?>

                <div class="save_button_holder">
                    <button id="save_button_down_price" type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>Save</button>
                    <button type="submit" class="btn btn-success" name="exit" value="1"><i class="fa fa-check"></i> Save &amp; Exit</button>
                </div>
            <?php endif; ?>

            <?php echo \Form::close(); ?>

        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>



<script type="text/javascript">
    $(window).load(function() {
        count_items();
    });

    $(document).ready(function(){

        $('.bulk_actions').on('change', function(){
            var $value = $(this).val();
            var $add_input = ['product_code', 'retail_price', 'special_price'];
            if($.inArray($value, $add_input) >= 0) {
                $('.bulk_input_box').show();
            } else {
                $('.bulk_input_box').hide();
            }
        });

        $('.apply_bulk_actions').on('click', function(e){
            e.preventDefault();
            var $submit = true;
            var $value = $('.bulk_actions').select2('val');
            var $input = $('.bulk_input').val();

            $('.check_active').each(function (index){
                if($(this).is(":checked")){
                    var $id = $(this).attr('data-id');
                    var $parent = $(this).parents('tr').first();
                    switch($value)
                    {
                        case 'active_enable':
                            $parent.find('.ba_active').attr('checked', 'checked');
                            //$('.custom_checkbox :checkbox').iphoneStyle('refresh');
                            break;
                        case 'active_disable':
                            $parent.find('.ba_active').removeAttr('checked');
                            //$('.custom_checkbox :checkbox').iphoneStyle('refresh');
                            break;
                        case 'product_code':
                            $parent.find('.ba_product_code').val($input);
                            break;
                        case 'retail_price':
                            if(!$.isNumeric($input)) $submit = false;
                            else $parent.find('.ba_retail_price').val($input);
                            break;
                        case 'special_price':
                            if(!$.isNumeric($input)) $submit = false;
                            else $parent.find('.ba_special_price').val($input);
                            break;

                    }
                }
            })

            if($submit) $(this).closest('form').submit();

        });

        
        // Open image box
        $(document).on('click', '.edit_btn', function(){
            var $this = $(this);
            
             // Close if opened
            if($this.closest('tr').next('tr').hasClass('image_box')){
                $this.closest('tr').next('tr').remove();
                return false;
            }
            
            var $id = $(this).attr('data-id');
            if(!$.isNumeric($id)) $id = 0;
            var $key = $(this).attr('data-key');
            var $tr = $this.closest('tr');
            var $attribute_group = '<?php echo \Input::get('attribute_group', 0);?>';
            
            $.get('<?php echo \Uri::create('admin/product/price/get_image_box');?>/' + $id + '/' + $key + '/' + $attribute_group, function(data) {
                $('.image_box').remove();
                $tr.after(data);
                
                var $attr_html = $tr.find('.attribute_values').html();
                var $attr_box = $tr.next().find('.attr_box');
                $attr_box.html($attr_html);
                $attr_box.find('span').each(function(index){
                    $(this).before('<input type="checkbox" value="' + (index + 1) + '" class="attr_check"/>&nbsp;');
                });
               
                // Init sortable
                if($(".sortable").length > 0)
                {
                   $( "table.sortable" ).sortable({
                       //helper: fixHelper,
                       items: "tr:not(.nodrop.nodrag)",
                       handle: '.dragHandle',
                       start: function(e, ui){
                           table    = $(this);
                           // Clear previous messages if exists
                           $(".header_messages").html('<div class="fa fa-spinner fa-spin"></div>');
                           $(".ui-sortable-placeholder", table).height($("tr[id^='sort_']:first", table).height());
                       },
                       update: function(e, ui){
                           serial   = $(this).sortable('serialize');
                           table    = $(this);
                           url      = table.attr('rel');

                           // Serialize data
                           var serial = $(this).sortable("toArray");
                           var serialized = new Array();
                           $.each(serial, function(key, value){
                               if(value.indexOf('sort_') != -1)
                               {
                                   serialized.push(value.replace('sort_', ''));
                               }
                           });

                           if(url != undefined)
                           {
                               $.ajax({
                                   type: 'POST',
                                   url: url,
                                   data: {'sort': serialized},
                                   success: function(data) {
                                       $(".header_messages").html(data);
                                   },
                                   async: false
                               });
                           }
                       },
                       stop: function(e, ui){
                           // Remove left over loader image
                           $(".loader_image").remove();
                       }
                   });
               };               
            });
        });
        
        <?php if($show_all): ?>
            // Show / hide attribute value selector
            $('body').on('click', '.apply_image', function(e){
                var $checked = $(this).is(':checked');
                if($checked){
                   $(this).next('.attr_box').show();
                }else{
                   $(this).next('.attr_box').hide();
                }
            });
        <?php endif; ?>
        
        // Select combinations by attribute value
        $('.panelContent').on('click', '.attr_check', function(){
            var $cliked = $(this).index()/2;            
            $(this).parent().find('input').each(function(index){
                
                var $checked = $(this).is(':checked');
                var $attr_str = $(this).next('span').text();
                var $index = $(this).closest('.attr_box').find('input').index($(this));
                
                if($cliked != index && !$checked) return;

                $('.attribute_values').each(function(index){
                    var $attr_str_tmp = $(this).find('span')[$index];
                    if($($attr_str_tmp).text() == $attr_str){
                        $(this).closest('tr').find('.check_active').prop('checked', $checked);
                    }
                });
                
            });
            
        });
       
    });


    $(document).ready(function(){
 
        $('.apply_all').click( function(){
            
            /*var priceAttr = $('#all_price_attr').val();

            $('.ba_retail_price').each(function() {

                $(this).val(priceAttr);
            });


 
            $("input[name*='active_new']").each(function (){
                var this_box = $(this);
                if ($('.enable_price_attr_btn :checkbox').is(':checked')) {
                    this_box.prop('checked', true);
                } else {
                    this_box.prop('checked', false);
                }
            });*/
            $('#applyAllModalPopUp').modal('show');
        });

        $('#applyAllModalPopUp .modal-yes').click( function(){
            $('input[name="apply_all_save"]').val(1);
            $('input[name="apply_all_save"]').closest('form').submit();

        });
    });
</script>

<!-- Delete Modal Popup -->
<div class="modal modal-notify fade" id="applyAllModalPopUp" tabindex="-1" role="dialog" aria-labelledby="applyAllModalPopUp">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Message</h4>
            </div>
            <div class="modal-body text-center">
                <p class="modal-message">Are you sure you want to apply changes to all attributes?</p>
                <a class="btn btn-danger modal-yes">Yes</a>
                <a class="btn btn-default modal-no" data-dismiss="modal">No</a>
            </div>
        </div>
    </div>
</div>