<?php if(empty($item)):?>
    <tr class="image_box">
        <td colspan="9">
            <?php echo \Form::hidden('return_to', $key); ?>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 files_table separated">
                <tr class="nodrop nodrag blueTableHead">
                    <th scope="col" class="noresize">Image</th>
                    <th scope="col">Image Properties</th>
                </tr>

                <?php if(\Config::get('details.image.multiple', false)): ?>
                    <tr class="nodrop nodrag">
                        <td class="td-thumb">
                            <i class="fa fa-picture-o"></i>
                        </td>
                        <td class="upload">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="noresize">Alt Texts</td>
                                    <td>
                                        <div class="input_holder">
                                            <?php //echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1')); ?>
                                            <?php echo \Form::input('_alt_text_new_' . $key, \Input::post('_alt_text_new_' . $key), array('class'=>'form-control')); ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noresize">Choose Image</td>
                                    <td>
                                        <?php echo \Form::file('_image_new_' . $key , array('class'=>'form-control h-auto')); ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                <?php endif; ?>
                    <tr class="nodrop nodrag">
                        <td></td>
                        <td colspan="3">
                            <?php if($attribute_group > 0): ?>
                                <?php echo \Input::get('attribute_group'); ?>
                                <?php echo \Form::checkbox('apply_image', 1, null, array('class' => 'apply_image')); ?>
                                Apply image to combinations selected
                                <div class="attr_box" style="display: none;"></div>
                            <?php endif; ?>
                            <div class="save_button_holder text-right">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>Save</button>
                            </div>
                        </td>
                    </tr>

            </table>
        </td>
    </tr>
<?php else: ?>
    <tr class="image_box">
        <td colspan="9">
            <?php echo \Form::hidden('return_to', $key); ?>
            <div class="header_messages"></div>
            <table class="table table-striped table-bordered sortable" rel="<?php echo \Uri::create('admin/product/price/sort/image/' . $item->id); ?>">
                <tr class="nodrop nodrag blueTableHead">
                    <th scope="col" class="noresize">Image</th>
                    <th scope="col">Image Properties</th>
                    <?php if((!\Config::get('details.image.required', false) && !empty($item->images)) || count($item->images) > 1): ?>
                        <?php if(count($item->images) > 1): ?>
                            <th>Main Image</th>
                            <th>Re-order</th>
                        <?php endif; ?>
                        <th scope="col" class="center">Delete</th>
                    <?php endif; ?>
                </tr>

                <?php if(is_array($item->images)): ?>
                    <?php foreach($item->images as $image): ?>

                        <tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
                            <td class="center noresize">
                                <a href="<?php echo \Uri::create('media/images/' . $image->image); ?>" class="preview-image-popup">
                                    <img src="<?php echo \Uri::create('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $image->image); ?>" width="74" height="74" alt="<?php echo $image->alt_text; ?>"/>
                                </a>
                            </td>
                            <td class="upload">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="noresize">Alt Text</td>
                                        <td>
                                            <div class="input_holder">
                                                <?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class'=>'form-control')); ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="noresize">Replace Image</td>
                                        <td>
                                            <?php echo \Form::file('image_'.$image->id, array('class'=>'form-control h-auto')); ?>
                                            <?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <?php if(count($item->images) > 1): ?>
                                <td width="110" class="text-center">
                                    <input type="radio" name="cover_image" value="<?php echo $image->id; ?>" <?php echo $image->cover?'checked="checked"':''; ?>>
                                </td>
                                <td width="110" class="dragHandle">
                                    <ul class="table-action-inline">
                                        <li class="dragHandle">
                                            <a href="" onclick="return false;">Re-order</a>
                                        </li>
                                    </ul>
                                </td>
                            <?php endif; ?>
                            <td width="110">
                                <ul class="table-action-inline">
                                    <?php if((!\Config::get('details.image.required', false) && !empty($item->images)) || count($item->images) > 1): ?>
                                        <li>
                                            <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/product/price/delete_image/' . $image->id . '/' . $item->id); ?>">
                                                Delete
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                </ul>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                <?php endif; ?>

                <?php if(\Config::get('details.image.multiple', false) || empty($item->images)): ?>
                    <tr class="nodrop nodrag">
                        <td class="td-thumb">
                            <i class="fa fa-picture-o"></i>
                        </td>
                        <td class="upload">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td class="noresize">Alt Text</td>
                                    <td>
                                        <div class="input_holder">
                                            <?php echo \Form::input('alt_text_new_' . $item->id, \Input::post('alt_text_new_' . $item->id), array('class'=>'form-control')); ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="noresize">Choose Image</td>
                                    <td>
                                        <?php echo \Form::file('image_new_' . $item->id, array('class'=>'form-control h-auto')); ?>

                                    </td>
                                </tr>
                            </table>
                        </td>
                        <?php if(count($item->images) > 1): ?>
                            <td class="icon center"></td>
                            <td class="icon center"></td>
                            <td class="icon center"></td>
                        <?php endif; ?>
                        <?php if(count($item->images) == 1): ?>
                            <td class="icon center"></td>
                        <?php endif; ?>
                    </tr>
                <?php endif; ?>
                    <tr class="nodrop nodrag">
                        <td></td>
                        <td colspan="4">
                            <?php if($attribute_group > 0): ?>
                                <?php echo \Form::checkbox('apply_image', 1, null, array('class' => 'apply_image')); ?>
                                Apply image to combinations selected
                                <div class="attr_box" style="display: none;"></div>
                            <?php endif; ?>
                            <div class="pull-right text-right">
                                <button type="submit" class="btn btn-success"><i class="fa fa-edit"></i>Save</button>
                            </div>
                        </td>
                    </tr>

            </table>

        </td>
    </tr>
<?php endif; ?>
