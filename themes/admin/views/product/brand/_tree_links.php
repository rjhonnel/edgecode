<?php 
	// Get brands
	$brands = \Product\Model_Brand::find(function($query){ 
		$query->order_by('sort', 'asc');
		$query->order_by('id', 'asc');
	});
	$brand = isset($brand) ? $brand : false;
	$link = isset($link) ? $link : 'update';
	$selected = isset($selected) ? $selected : false;
?>

<?php
if($brand)
{
?>

<div class="side_tree_holder">
    <div class="tree_heading">
        <h4>Product Brand</h4>
        <div id="sidetreecontrol" class="sidetreecontrol"><a href="#">Collapse All</a><a href="#">Expand All</a></div>
    </div>
    <div class="tree_content">
        <div id="sidetree">
        
        	<?php if(!$brand && empty($brands)): ?>
        		<div class="wide"><span class="req">Note: </span> There are no brands yet.</div>
        	<?php else: ?>
        	
                <ul class="treeview" id="tree">
                	
                	<?php if($brand): ?>
                		<li>
                			<div class="radio_link_holder">
                    			<?php echo \Form::radio('parent_id', 0, \Input::post('parent_id', $brand->parent_id)); ?>
                    			<a href="#" onclick="return false;">ROOT</a>
                    		</div>	
                		</li>
                	<?php endif; ?>
                	
                	<?php
                		if(!empty($brands))
                		{
                    		// If brand parent_id or id is in this array than dont show radio input
                    		$hide_radio = $brand ? array($brand->id) : array();
                    		
                    		$list_subcategories = function($brand_item) use (&$hide_radio, $link, &$list_subcategories, $brand, $selected)
                    		{
                    			?><ul><?php
                    			foreach($brand_item->children as $child)
                    			{
                    				if($brand && (in_array($child->id, $hide_radio) || in_array($child->parent_id, $hide_radio))) 
                    					array_push($hide_radio, $child->id);
                    					
                    				?>
                    					<li>
                    						<?php echo !empty($child->children) ? '<div class="hitarea"></div>' : ''; ?>
                    						<div class="radio_link_holder">
                        						<?php if($brand): ?>
                        							<?php echo \Form::radio('parent_id', $child->id, \Input::post('parent_id', $selected !== false ? $selected : $brand->parent_id), (in_array($child->id, $hide_radio) ? array('style' => 'display: none;', 'disabled' => 'diabled') : array())); ?>
                        						<?php endif; ?>
                        						<a href="<?php echo \Uri::create('admin/product/brand/' . $link . '/' . $child->id); ?>" <?php echo $selected == $child->id ? 'class="active"' : ''; ?>>
                        							<?php echo $child->title; ?><?php echo !empty($child->children) ? ' <span class="tree_count">('.count($child->children).')</span>' : ''; ?>
                        						</a>
                        					</div><?php
                    				if(!empty($child->children)) 
                    					$list_subcategories($child);
                    				else
                    					?></li><?php
                    					
                    			}
                    			?></ul><?php
                    		};
                    	
                    		foreach($brands as $key => $brand_item)
                    		{
                    			// Only root brands in first pass
                    			if($brand_item->parent_id == 0)
                    			{
                    				if($brand && in_array($brand_item->id, $hide_radio)) 
                    					array_push($hide_radio, $brand_item->id);
                    					
                        			?>
                        				<li>
                        					<?php echo !empty($brand_item->children) ? '<div class="hitarea"></div>' : ''; ?>
                        					<div class="radio_link_holder">
                            					<?php if($brand): ?>
                        							<?php echo \Form::radio('parent_id', $brand_item->id, \Input::post('parent_id', $selected !== false ? $selected : $brand->parent_id), (in_array($brand_item->id, $hide_radio) ? array('style' => 'display: none;', 'disabled' => 'diabled') : array())); ?>
                            					<?php endif; ?>
                        						<a href="<?php echo \Uri::create('admin/product/brand/' . $link . '/' . $brand_item->id); ?>" <?php echo $selected == $brand_item->id ? 'class="active"' : ''; ?>>
                        							<?php echo $brand_item->title; ?><?php echo !empty($brand_item->children) ? ' <span class="tree_count">('.count($brand_item->children).')</span>' : ''; ?>
                        						</a>
                        					</div><?php
                    				if(!empty($brand_item->children)) 
                    					$list_subcategories($brand_item);
                    				else
                    					?></li><?php
                    			}
                    		}
                		}
                	?>
                    
                </ul>
                
                <?php if($brand): ?>
            		<div class="wide"><span class="req">Note: </span> Select parent brand.</div>
            	<?php endif; ?>
                
			<?php endif; ?>
        </div>
    </div>
</div>

<?php
}
?>