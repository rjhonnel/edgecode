<!-- Content -->
<div class="layout-content main-content" data-scrollable>
	<div class="main-content-body">
		
        <?php 
            \Breadcrumb::set('Home', 'admin/dashboard');
            \Breadcrumb::set('Catalogue');
            \Breadcrumb::set('Brand Manager', 'admin/product/brand/list');
            \Breadcrumb::set($brand->title, 'admin/product/brand/update/' . $brand->id);

            echo \Breadcrumb::create_links();
        ?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit <?php echo $brand->parent_id > 0? 'Sub ': ''; ?>Brand: General Information</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/brand/_action_links', array('parent_id' => $brand->id)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/brand/_navbar_links', array('brand' => $brand)); ?>

            
                
		    	<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>
		    	
                    <!-- Main Content Holder -->
                    <div class="row">

	                	<div class="col-sm-12">
	                	
		                    <!-- Accordions Panel -->
		                    <div class="panel panel-default">
		                    	<div class="panel-heading">
		                            <h3 class="panel-title pull-left">General Information</h3>
		                        </div>
		                        <div class="panel-body">
		                        	<div class="form-horizontal">
										<div class="form-group">
											<label class="col-sm-2 control-label">Brand Name  <span class="text-danger">*</span></label>
											<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title', $brand->title), array('class' => 'form-control')); ?></div>
										</div>
										<div class="form-group">
											<?php echo \Form::label('Status', null, array('class' => 'col-sm-2 control-label text_right m_r_15')); ?>
											<div class="col-sm-10">
												<?php echo \Form::select('status', \Input::post('status', $brand->status), array(
													'1' => 'Active',
													'0' => 'Inactive',
													'2' => 'Active in Period',
												), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
											</div>

										</div>

										<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

											<?php echo \Form::label('Dates Active', null, array('class' => 'col-sm-2 control-label dateLabel')); ?>
											<div class="col-sm-10">
												<div class="datepicker-holder-control">
													<?php echo \Form::input('active_from', \Input::post('active_from', !is_null($brand->active_from) ? date('d/m/Y', $brand->active_from) : ''), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
												</div>
												<div class="datepicker-holder-control mt-5px">
													<?php echo \Form::input('active_to', \Input::post('active_to', !is_null($brand->active_to) ? date('d/m/Y', $brand->active_to) : ''), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
												</div>
											</div>
										</div>
			                            
			                            <div class="form-group">
			                                <label class="col-sm-2 control-label">Small Description</label>
			                                <div class="col-sm-10"><?php echo \Form::textarea('description_intro', \Input::post('description_intro', $brand->description_intro), array('class' => 'form-control')); ?></div>
			                            </div>
		                            </div>
		                            
									<div class="form-group">
										<?php echo \Form::label('Description'); ?>
										<?php echo \Form::textarea('description_full', \Input::post('description_full', $brand->description_intro), array('class' => 'form-control ck_editor')); ?>
									</div>
		                        </div>
		                    </div><!-- EOF Accordions Panel -->
		                
		                    <!-- Images Panel -->
		                    <div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title">Images</h3>
								</div>
		                        <div class="panel-body">
		                        	<div class="header_messages"></div>
	                            	<table rel="<?php echo \Uri::create('admin/product/brand/sort/image'); ?>" class="table table-striped table-bordered sortable">
	                                    <tr class="nodrop nodrag blueTableHead">
	                                        <th scope="col" class="noresize">Image</th>
	                                    	<th scope="col">Image Properties</th>
	                                    	<?php if(count($brand->images) > 1): ?>
												<th scope="col" class="center">Main Image</th>
	                                    		<th scope="col" class="center">Re-order</th>
	                                		<?php endif; ?>
	                                		<?php if((!\Config::get('details.image.required', false) && !empty($brand->images)) || count($brand->images) > 1): ?>
	                                        	<th scope="col" class="center">Delete</th>
	                                		<?php endif; ?>
	                                    </tr>
	                                    
	                                    <?php if(is_array($brand->images)): ?>
	                                        <?php foreach($brand->images as $image): ?>
	                                        	
		                                        <tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
		                                            <td class="center noresize">
		                                            	<a href="<?php echo \Helper::amazonFileURL('media/images/' . $image->image); ?>" class="preview-image-popup">
			                                        		<img src="<?php echo \Helper::amazonFileURL('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $image->image); ?>" width="130" height="130" alt="<?php echo $image->alt_text; ?>"/>
			                                        	</a>
			                                        </td>
		                                            <td class="upload btn-file">
		                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                                                    <tr>
		                                                        <td class="noresize">Alt Text</td>
		                                                        <td>
		                                                        	<div class="input_holder">
		                                                        		<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class' => 'form-control')); ?>
		                                                        	</div>
		                                                        </td>
		                                                    </tr>
		                                                    <tr>
		                                                        <td class="noresize">Replace Image</td>
		                                                        <td>
		                                                        	<?php echo \Form::file('image_'.$image->id); ?>
		                                                        	<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
		                                                        </td>
		                                                    </tr>
		                                                </table>
		                                            </td>
		                                            
		                                            <?php if(count($brand->images) > 1): ?>
														<td width="110" class="text-center">
															<input type="radio" name="cover_image" value="<?php echo $image->id; ?>" <?php echo $image->cover?'checked="checked"':''; ?>>
														</td>
														<td width="110" class="dragHandle">
															<ul class="table-action-inline">
																<li class="dragHandle">
																	<a href="" onclick="return false;">Re-order</a>
																</li>
															</ul>
														</td>
		                                            <?php endif; ?>
		                                            
		                                            <?php if((!\Config::get('details.image.required', false) && !empty($brand->images)) || count($brand->images) > 1): ?>
			                                            <td width="110">
															<ul class="table-action-inline">
																<li>
																	<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/product/brand/delete_image/' . $image->id . '/' . $brand->id); ?>">
																		Delete
																	</a>
																</li>
															</ul>
														</td>
		                                            <?php endif; ?>
		                                        </tr>
		                                        
	                                        <?php endforeach; ?>
										<?php endif; ?>
	                                    
	                                    <?php if(\Config::get('details.image.multiple', false) || empty($brand->images)): ?>
	                                        <tr class="nodrop nodrag">
												<td class="td-thumb">
													<i class="fa fa-picture-o"></i>
												</td>
	                                            <td class="upload">
	                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                                    <tr>
	                                                        <td class="noresize">Alt Text</td>
	                                                        <td>
	                                                        	<div class="input_holder">
			                                                        <?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1'), array('class' => 'form-control')); ?>
	                                                        	</div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="noresize">Choose Image</td>
	                                                        <td>
	                                                        	<?php echo \Form::file('image_new_1'); ?>
	                                                        </td>
	                                                    </tr>
	                                                </table>
	                                            </td>
	                                            <?php if(count($brand->images) > 1): ?>
	                                            	<td class="icon center"></td>
												<td class="icon center"></td>
	                                            <?php endif; ?>
	                                            <?php if((!\Config::get('details.image.required', false) && !empty($brand->images)) || count($brand->images) > 1): ?>
	                                            	<td class="icon center"></td>
	                                            <?php endif; ?>
	                                        </tr>
	                                    <?php endif; ?>
	                                    
	                                </table>
		                        </div>
		                    </div><!-- EOF Images Panel -->
	                    </div>
                    </div><!-- EOF Main Content Holder -->

        			<div class="save_button_holder text-right">
                    	<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
                    	<?php echo \Form::button('exit', '<i class="fa fa-check"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
                    </div>
                
		    	<?php echo \Form::close(); ?>
		    	
		</div>
	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
<!-- EOF Content -->

<?php echo ckeditor_replace('ck_editor'); ?>