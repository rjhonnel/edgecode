<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Product Manager', 'admin/product/list');
		\Breadcrumb::set($deal->product->title, 'admin/product/update/' . $deal->product->id);
		\Breadcrumb::set('Deals');
		\Breadcrumb::set('Add New');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Product: Edit Deal</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/_action_links', array('set_save_specials' => 1 ) ); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">General Information</h3>

				</div>
				<div class="panel-body">
					<div class="form-group">
						<div class="span7">
							<div class="form-group">
								<?php echo \Form::label('Deal Title' . '  <span class="text-danger">*</span>'); ?>
								<div class="input_holder"><?php echo \Form::input('title', \Input::post('title', $deal->title), array('class' => 'form-control')); ?></div>
							</div>
						</div>
						<div class="span4 right">
							<div class="form-group">
                                <?php echo \Form::label('Status', null, array('class' => 'text_right')); ?>
                            	<?php echo \Form::select('status', \Input::post('status', $deal->status), array(
                                	'1' => 'Active',
                                	'0' => 'Inactive',
                                	'2' => 'Active in Period',
                                ), array('class' => 'toggle_dates select_init form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
                                
                            	<div class="toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;margin-top:15px;"' : 'style="margin-top:15px;"'; ?>>
                            	
                                	<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
                                	<div class="row">
                                        <div class="col-md-6">
                                            <div class="datepicker-holder-control">
                                              <?php echo \Form::input('active_from', \Input::post('active_from', !is_null($deal->active_from) ? date('d/m/Y', $deal->active_from) : ''), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="datepicker-holder-control">
                                              <?php echo \Form::input('active_to', \Input::post('active_to', !is_null($deal->active_to) ? date('d/m/Y', $deal->active_to) : ''), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
                                            </div>
                                        </div>
                                    </div>
                                	
                                </div>
                            </div>
						</div>
					</div>
					<div class="form-group">
						<label>Short Description </label>
						<?php echo \Form::textarea('short_description', \Input::post('short_description', $deal->short_description), array('rows' => 2, 'class'=>'form-control')); ?>
					</div>
					<div class="form-group">
						<label >Full Description <span class="text-danger">*</span></label>
							<?php echo \Form::textarea('full_description', \Input::post('full_description', $deal->full_description), array('rows' => 2, 'class'=>'ck_editor')); ?>
					</div>
				</div>
			</div>

			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id' => 'save_button_down_specials', 'class' => 'btn btn-success')); ?>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<?php echo ckeditor_replace('ck_editor'); ?>