<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>

<?php
    // Prep some values needed for grid
    $status = array(
        'false' => 'Select',
        '1' => 'Active',
        '0' => 'Inactive',
        '2' => 'Active in period',
    );
?>
    <div class="top-filter-holder">
        <div class="filter-holder">
            <div class="text-right">
                <a class="btn btn-primary" href="<?php echo \Uri::create('admin/product/deal/create/' . $product->id); ?>"><i class="icon-plus icon-white"></i>Add New Deal</a>
            </div>
        </div>
        <!--div class="form-inline pull-right">
            <label>Show entries:</label>
            <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
        </div-->
    </div>
    <div class="header_messages"></div>
    <table rel="<?php echo \Uri::create('admin/product/deal/sort/deal'); ?>" class="table table-striped table-bordered sortable_rows" width="100%">
        <thead>
        <tr class="blueTableHead">
            <th scope="col">Deal Name</th>
            <th scope="col" class="center" style="width: 150px;">Status</th>
            <th scope="col" class="center" style="width: 40px;">Edit</th>
            <th scope="col" class="center" style="width: 40px;">Delete</th>
        </tr>
        </thead>
        <tbody>

        <?php if ($items): ?>

            <?php foreach ($items as $item): ?>
                <?php $item = (Object) $item; ?>

                <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
                    <td><a href="<?php echo \Uri::create('admin/product/deal/update/' . $item->product_id .'/'. $item->id); ?>"><?php echo $item->title; ?></a>
                    </td>
                    <td>
                        <?php
                        // If page is active from certain date
                        if($item->status == 2)
                        {
                            $dates = array();
                            !is_null($item->active_from) and array_push($dates, date('d/m/Y', $item->active_from));
                            !is_null($item->active_to) and array_push($dates, date('d/m/Y', $item->active_to));

                            if(true)
                            {
                                ?>
                                Active
                                <a class="activeDate" rel="tooltip" title="<?php echo implode(' - ', $dates); ?>">
                                    <?php echo \Theme::instance()->asset->img('icon-calendar.png', array('width' => 16, 'height' => 16)); ?>
                                </a>
                                <?php
                            }
                        }
                        else
                        {
                            echo $status[$item->status];
                        }
                        ?>
                    </td>
                    <td class="icon center">
                        <a href="<?php echo \Uri::create('admin/product/deal/update/' . $item->product_id .'/'. $item->id); ?>">
                            Edit
                        </a>
                    </td>
                    <td class="icon center">
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete deal?" href="<?php echo \Uri::create('admin/product/deal/delete/' . $item->product_id .'/'. $item->id); ?>">
                            Delete
                        </a>
                    </td>
                </tr>

            <?php endforeach; ?>

        <?php else: ?>

            <tr class="nodrag nodrop">
                <td colspan="4" class="center"><strong>There are no items.</strong></td>
            </tr>

        <?php endif; ?>

        </tbody>
    </table>


    <div class="pagination-holder">
        <?php echo $pagination->render(); ?>
    </div>

<?php echo \Form::close(); ?>