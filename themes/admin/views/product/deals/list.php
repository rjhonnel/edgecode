<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Product Manager', 'admin/product/list');
		\Breadcrumb::set($product->title, 'admin/product/update/' . $product->id);
		\Breadcrumb::set('Deals', 'admin/product/deals/list/' . $product->id);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Product: Deals</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>

			<?php
				// Load page listing table
				echo \Theme::instance()->view('views/product/deals/_listing_table',
					array(
						'product'		=> $product,
						'items'			=> $items,
						'pagination'	=> $pagination,
					),
					false
				);
			?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>