<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Catalogue');
        \Breadcrumb::set('Product Manager', 'admin/product/list');

        // If viewing category products show category title
        if(isset($category))
            \Breadcrumb::set($category->title);


        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Products</h4>
                <div class="pull-right">
                    <?php
                        if(isset($category))
                            echo \Theme::instance()->view('views/product/_action_links');
                        else
                            echo \Theme::instance()->view('views/product/_action_links', array('hide_show_all' => 1, 'show_bulk_add' => 1));
                    ?>
                </div>
            </header>

            <?php
                if(isset($category) && $update_category)
                {
                    echo \Theme::instance()->view('views/product/category/_navbar_links', array('category' => $category));
                }
                elseif(isset($brand) && $update_category)
                {
                    echo \Theme::instance()->view('views/product/brand/_navbar_links', array('brand' => $brand));
                }
            ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>


                <?php
                    // Load product listing table
                    echo \Theme::instance()->view('views/product/_listing_table',
                        array(
                            'pagination' 	=> $pagination,
                            'items'			=> $items,
                            'status'		=> $status,
                        ),
                        false
                    );
                ?>
            <?php echo \Form::close(); ?>

            <?php echo \Theme::instance()->view('views/product/_tree_links'); ?>

        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>








