<div class="layout-content main-content" data-scrollable>
	<div class="main-content-body">
            <?php 
                \Breadcrumb::set('Home', 'admin/dashboard');
                \Breadcrumb::set('Catalogue');
                \Breadcrumb::set('Product Manager', 'admin/product/list');
                \Breadcrumb::set($product->title, 'admin/product/update/' . $product->id);
                \Breadcrumb::set('Info Tabs', 'admin/product/infotab_list/' . $product->id);
                \Breadcrumb::set('Edit Info Tab');
                \Breadcrumb::set($infotab->title);

                echo \Breadcrumb::create_links();
            ?>
            <div class="main-content-body-inner layout-content" data-scrollable>
				<header class="main-content-heading">
					<h4 class="pull-left">Edit Product: Edit Info Tab</h4>

					<div class="pull-right">
						<?php echo \Theme::instance()->view('views/product/_action_links'); ?>
					</div>
				</header>
				<?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>
            
	            
	            <!-- Content -->
		    	<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>
		    	
                    <!-- Main Content Holder -->
                    <div class="row">
                    	<div class="col-sm-12">
	                        <!-- Accordions Panel -->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title pull-left">General Information</h3>

								</div>
	                            <div class="panel-body">
	                                <div class="row">
	                                	<!-- TABLE GENERATION -->
	                                	<div class="col-md-6">
		                                    <div class="formRow">
		                                        <?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
		                                        <div class="clear"></div>
		                                        <?php echo \Form::textarea('description', \Input::post('description', $infotab->description), array('style' => 'min-height:84px; line-height: 14px;', 'class' => 'form-control info_table')); ?>
		                                    </div>
		                                    <div class="formRow">
		                                    	
		                                    </div>
	                                    </div>
	                                    <!-- END OF: TABLE GENERATION -->
	                                    
	                                    <!-- TABLE GENERATION PREVIEW -->
	                                	<div class="col-md-6">
		                                    <?php echo \Form::label('Preview'); ?>
	                                        <div class="clear"></div>
	                                        <div class="formRow info_table_preview" style="min-height: 52px; border: solid 1px #CCCCCC; border-radius: 3px; padding: 20px 0;">
	                                        	<!-- Loaded via ajax -->
		                                    </div>
		                                    <div class="formRow info_table_loading" style="display: none;">
		                                    	<!-- This is loaded while user is typing -->
		                                    	<center style="margin: 0px 0 19px 0;">Generating table template...</center>
		                                    	<center>
													<div class="fa fa-spinner fa-spin"></div>
												</center>
		                                    </div>
		                                    <div class="formRow info_table_empty" style="display: none;">
		                                    	<!-- This is loaded if description is empty -->
		                                    	<center style="margin: 18px 0 19px 0;">Your description is empty</center>
		                                    </div>
	                                    </div>
	                                    <!-- END OF: TABLE GENERATION PREVIEW -->
	                                </div>
	                                <div class="row">
	                                	<!-- TABLE GENERATION LEGEND -->
	                                	<div class="col-md-6">
	                                		<?php echo \Form::label('Legend'); ?>
	                                        <div class="clear"></div>
	                                        <table class="table table-bordered table-condensed">
	                                			<thead>
	                                				<tr style="background-color: #F9F9F9;">
	                                					<th>Description</th>
	                                        			<th>Tag</th>
	                                        			<th>Usage</th>
	                                				</tr>
	                                			</thead>
	                                			<tbody>
	                                				<tr>
	                                					<td><strong>Begin Table</strong></td>
	                                					<td>==table==</td>
	                                					<td rowspan="2">Table tag must be in separate line. It can start or end with any number of "=" characters (not just two like in this example)</td>
	                            					</tr>
	                                				<tr>
	                                					<td><strong>End Table</strong></td>
	                                					<td>==table==</td>
	                            					</tr>
	                                				<tr>
	                                					<td><strong>Table Row</strong></td>
	                                					<td colspan="2">Table rows are separated wih empty line (two new lines)</td>
	                            					</tr>
	                                				<tr>
	                                					<td><strong>Table Columns</strong></td>
	                                					<td colspan="2">Table columns are everything between two empty lines (table rows). Each table column is separated by new line (next line)</td>
	                            					</tr>
	                                				<tr>
	                                					<td><strong>Table Heading</strong></td>
	                                					<td>#Heading</td>
	                                					<td>Table headings <strong>must</strong> be defined as first table columns and start with hash tag "#"</td>
	                            					</tr>
	                            					<tr>
	                                					<td><strong>Standard Text</strong></td>
	                                					<td colspan="2">Standard text is everything outside of two enclosing table tags (==table==). You can use any HTML tag and it will be correctly rendered</td>
	                            					</tr>
	                            				</tbody>
	                            			</table>
	                                    </div>
	                                    <!-- END OF: TABLE GENERATION LEGEND -->
	                                	
	                                	<!-- TABLE GENERATION EXAMPLE -->
	                                	<div class="col-md-6">
	                                		<?php echo \Form::label('Example'); ?>
	                                        <div class="clear"></div>
	                                        <table class="table table-bordered table-condensed">
	                                        	<thead>
	                                				<tr style="background-color: #F9F9F9;">
	                                					<th>Example that combines all of above</th>
	                                        			<th>Example result</th>
	                                				</tr>
	                                			</thead>
	                                			<tbody>
	                            					<tr>
	                                					<td>
	                                						<?php
	                                							$template = 'Standard text before table with headings:' . "\n\n";
	                                    						$template .= '==table==' . "\n";
	                                    						$template .= '#Heading 1' . "\n";
	                                    						$template .= '#Heading 2' . "\n\n";
	                                    						$template .= 'Value 1' . "\n";
	                                    						$template .= 'Value 2' . "\n\n";
	                                    						$template .= 'Value 3' . "\n";
	                                    						$template .= 'Value 4' . "\n";
	                                    						$template .= '=====table=====' . "\n\n";
	                                    						$template .= 'And more standard text here before another table without table headings:' . "\n\n";
	                                    						$template .= '=table=' . "\n";
	                                    						$template .= 'Option 1' . "\n";
	                                    						$template .= 'Option 2' . "\n\n";
	                                    						$template .= 'Option 3' . "\n";
	                                    						$template .= 'Option 4' . "\n";
	                                    						$template .= 'table' . "\n\n";
	                                							
	                                    						echo nl2br($template);
	                                						?>
	                                						
	                                					</td>
	                                					<td>
	                                						<?php 
	                                							\Request::forge('admin/product/infotab/parse_info_table')->execute(array('table' => $template));
	                                						?>
	                                					</td>
	                            					</tr>
	                            				</tbody>
	                            			</table>
	                                    </div>
	                                    <!-- END OF: TABLE GENERATION EXAMPLE -->
	                                </div>
		                            <div class="clear"></div>
	                            </div>
	                        </div><!-- EOF Accordions Panel -->
	                    
	                        <!-- Images Panel -->
	                        <div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title pull-left">Images</h3>

								</div>
	                            <div class="panel-body">
										<div class="header_messages"></div>
	                            	
	                            	<table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/product/infotab/sort/image/' . $infotab->unique_id); ?>" class="table table-striped table-bordered sortable ">
	                                    <tr class="nodrop nodrag blueTableHead">
	                                        <th scope="col" class="noresize">Image</th>
	                                    	<th scope="col">Image Properties</th>
	                                    	<?php if(count($infotab->images) > 1): ?>
	                                    		<th scope="col" class="center">Re-order</th>
	                                		<?php endif; ?>
	                                		<?php if((!\Config::get('infotab.image.required', false) && !empty($infotab->images)) || count($infotab->images) > 1): ?>
	                                        	<th scope="col" class="center">Delete</th>
	                                		<?php endif; ?>
	                                    </tr>
	                                    
	                                    <?php if(is_array($infotab->images)): ?>
	                                        <?php foreach($infotab->images as $image): ?>
	                                        	
		                                        <tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
		                                            <td class="center noresize">
			                                        	<img src="<?php echo \Helper::amazonFileURL('media/images/' . key(\Config::get('infotab.image.resize', array('' => ''))) . $image->image); ?>" width="74" height="74" alt="<?php echo $image->alt_text; ?>"/>
			                                        </td>
		                                            <td class="upload">
		                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                                                    <tr>
		                                                        <td class="noresize">Alt Text</td>
		                                                        <td>
		                                                        	<div class="input_holder">
		                                                        		<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class' => 'form-control')); ?>
		                                                        	</div>
		                                                        </td>
		                                                    </tr>
		                                                    <tr>
		                                                        <td class="noresize">Replace Image</td>
		                                                        <td>
		                                                        	<?php echo \Form::file('image_'.$image->id); ?>
		                                                        	<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
		                                                        </td>
		                                                    </tr>
		                                                </table>
		                                            </td>

													<?php if(count($infotab->images) > 1): ?>
														<td width="110" class="dragHandle">
															<ul class="table-action-inline">
																	<li class="dragHandle">
																		<a href="" onclick="return false;">Re-order</a>
																	</li>
															</ul>
														</td>
													<?php endif; ?>

													<td width="110">
														<ul class="table-action-inline">
															<?php if((!\Config::get('infotab.image.required', false) && !empty($infotab->images)) || count($infotab->images) > 1): ?>
																<li>
																	<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/product/delete_infotab_image/' . $image->id . '/' . $infotab->unique_id); ?>">
																		Delete
																	</a>
																</li>
															<?php endif; ?>
														</ul>
													</td>

		                                        </tr>
		                                        
	                                        <?php endforeach; ?>
										<?php endif; ?>
	                                    
	                                    <?php if(\Config::get('infotab.image.multiple', false) || empty($infotab->images)): ?>
	                                        <tr class="nodrop nodrag">
												<td class="td-thumb">
													<i class="fa fa-picture-o"></i>
												</td>
	                                            <td class="upload">
	                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                                    <tr>
	                                                        <td class="noresize">Alt Text</td>
	                                                        <td>
	                                                        	<div class="input_holder">
			                                                        <?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1'), array('class' => 'form-control')); ?>
	                                                        	</div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="noresize">Choose Image</td>
	                                                        <td>
	                                                        	<?php echo \Form::file('image_new_1'); ?>
	                                                        </td>
	                                                    </tr>
	                                                </table>
	                                            </td>
	                                           <?php if(count($infotab->images) > 1): ?>
	                                            	<td class="icon center"></td>
	                                            <?php endif; ?>
	                                            <?php if((!\Config::get('infotab.image.required', false) && !empty($infotab->images)) || count($infotab->images) > 1): ?>
	                                            	<td class="icon center"></td>
	                                            <?php endif; ?>
	                                        </tr>
	                                    <?php endif; ?>
	                                    
	                                </table>
	                            	
	                            </div>
	                        </div><!-- EOF Images Panel -->
	                   	</div>     
                    </div><!-- EOF Main Content Holder -->
                        
        			<div class="save_button_holder text-right">
                        <?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
                    	<?php echo \Form::button('exit', 'Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
                    </div>
                
		    	<?php echo \Form::close(); ?>
			    <!-- EOF Content -->
			    
			    <?php echo \Theme::instance()->asset->js('product/table_parser.js'); ?>

			</div>
	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>