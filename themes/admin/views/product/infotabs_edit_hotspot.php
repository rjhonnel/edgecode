<div class="layout-content main-content" data-scrollable>
	<div class="main-content-body">
        <?php 
            \Breadcrumb::set('Home', 'admin/dashboard');
            \Breadcrumb::set('Catalogue');
            \Breadcrumb::set('Product Manager', 'admin/product/list');
            \Breadcrumb::set($product->title, 'admin/product/update/' . $product->id);
            \Breadcrumb::set('Info Tabs', 'admin/product/infotab_list/' . $product->id);
            \Breadcrumb::set('Edit Info Tab');
            \Breadcrumb::set($infotab->title);

            echo \Breadcrumb::create_links();
        ?>
        <div class="main-content-body-inner layout-content" data-scrollable>
			<header class="main-content-heading">
				<h4 class="pull-left">Edit Product: Edit Info Tab</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/_action_links'); ?>
				</div>
			</header>
			<?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>

            <!-- Content -->
        	<?php echo \Form::open(array('action' => \Uri::create('admin/product/infotab_edit_hotspot/' . $product->id . '/' . $infotab->unique_id), 'enctype' => 'multipart/form-data')); ?>
	        	<div class="row">
	        		<div class="col-sm-12">
		                <!-- Accordions Panel -->
		                <div class="panel panel-default">
							<div class="panel-heading panelHeader">
								<h3 class="panel-title pull-left">Hotspot Image</h3>
								<div class="pull-right togglePanel <?php echo $infotab->image ? 'closed' : ''; ?>">
		                			<a><i class="fa fa-minus"></i></a><a><i class="fa fa-plus"></i></a></div>
							</div>
		                    <div class="panel-body panelContent">
		                        <table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/product/infotab/sort/image/' . $infotab->unique_id); ?>" class="table table-striped table-bordered sortable">
		                            <tr class="nodrop nodrag blueTableHead">
		                                <th scope="col" class="noresize">Image</th>
		                            	<th scope="col">Image Properties</th>
		                            	<?php if(!empty($infotab->image)): ?>
		                            		<th scope="col" class="center">Delete</th>
		                            	<?php endif; ?>
		                            </tr>
		                            
		                            <tr class="nodrop nodrag">
		                                <td class="td-thumb hotspot_image">
		                                	<?php if(!empty($infotab->image)): ?>
		                                    	<?php echo \Html::img(\Helper::amazonFileURL('media/images/' . key(\Config::get('infotab.image.resize', array('' => ''))) . $infotab->image), array('width' => '80', 'height' => '80', 'class' => 'default')); ?>
		                                	<?php else: ?>
												<i class="fa fa-picture-o"></i>
		                                	<?php endif; ?>
		                                	
		                                	<?php if(!empty($product->images)): ?>
		                                		<?php $image_path = 'media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $product->images[0]->image; ?>
		                                		
		                                    	<?php echo \Html::img(\Helper::amazonFileURL($image_path), array('width' => '80', 'height' => '80', 'class' => 'cover', 'style' => 'display: none;')); ?>
		                                    	<div style="margin-top: 5px; position: relative;">
		                                    		<?php echo \Form::checkbox('use_cover_image', 1, \Input::post('use_cover_image'), array('class' => 'use_cover_image', 'style' => 'margin: 0;')); ?>
		                                    		<?php echo \Form::hidden('cover_image', $product->images[0]->image); ?>
		                                    		<span style="font-size: 9px;">Use cover image?</span>
		                                		</div>
		                                	<?php endif; ?>
		                                </td>
		                                <td class="upload">
		                                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                                        <tr>
		                                            <td class="noresize">Alt Text</td>
		                                            <td>
		                                            	<div class="input_holder">
		                                                    <?php echo \Form::input('alt_text', \Input::post('alt_text', $infotab->alt_text), array('class' => 'form-control')); ?>
		                                            	</div>
		                                            </td>
		                                        </tr>
		                                        <tr>
		                                            <td class="noresize">Choose Image</td>
		                                            <td class="btn-file">
		                                            	<?php echo \Form::file('image'); ?>
		                                            	<?php if(!empty($infotab->image)): ?>
		                                            		<?php echo \Form::hidden('image_db', $infotab->image); ?>
		                                            	<?php endif; ?>
		                                            </td>
		                                        </tr>
		                                    </table>
		                                </td>
		                                <?php if(!empty($infotab->image)): ?>
		                            		<td class="icon center">
		                                    	<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/product/infotab/delete_image/' . $infotab->unique_id); ?>">Delete</a>
		                                    </td>
		                            	<?php endif; ?>
		                            </tr>
		                            
		                        </table>
		                        
		                        <div class="save_button_holder" style="margin-bottom: 10px;">
		                            <?php echo \Form::button('image', '<i class="fa fa-edit"></i>Save Image', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
		                        </div>
		                    </div>
		                </div><!-- EOF Accordions Panel -->
	        		</div>
	        	</div>
            <?php echo \Form::close(); ?>
            
            <?php if(!empty($infotab->image)): ?>
            	
                <!-- Accordions Panel -->
            	<div class="panel panel-default">
                	<div class="panel-heading panelHeader">
						<h3 class="panel-title pull-left">Hotspot Image</h3>
                		<div class="togglePanel pull-right"><a><i class="fa fa-minus"></i></a><a><i class="fa fa-plus"></i></a></div>
                    </div>
                    <div class="panel-body panelContent">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_b_10">
                    		<tr>
                    			<td class="span7 p_r_10">
                    				<small><span class="text-danger">*</span> Click on image to create new hotspot. You may drag and reposition the hotspot as you see fit.
									Edit the content of hotspot via the fields bellow the image. 
									You may insert an image in the hotspot or link the hotspot to a video.</small>
                    			</td>
                    			<td class="span5 sort_message_container" valign="top">
                    			</td>
                    		</tr>
                    	</table>
                        <div class="clear"></div>
                            
                        <div style="display: block; text-align: center;">
                            <div class="hotspot_cover_holder" style="position: relative; display: inline-block;" >
                        		<?php echo \Html::img(\Helper::amazonFileURL('media/' . \Config::get('infotab.image.location.folder', '') . '/' . $infotab->image), array('class' => 'hotspot_cover', 'style' => 'border: solid 1px #D0D0D0;')); ?>
                            	<?php echo \Theme::instance()->asset->img('hotspot/recycle_bin.png', array('class' => 'trash', 'style' => 'position: absolute; bottom: 0; right: 0; padding: 5px;', 'title' => 'Drag hotspots here to delete them.')); ?>
                            	
                            	<?php if(!empty($infotab->images)): ?>
                            		<?php foreach($infotab->images as $image): ?>
                            			
                            			<?php if(!is_null($image->position_x) && !is_null($image->position_y)): ?>
                            			
                            				<?php 
                            					$image_name = isset($hotspot) && $image->id == $hotspot->id ? 'hotspot_red.png' : 'hotspot_blue.png';
                            				?>
                            				
                            				<a class="hotspot" rel="<?php echo $image->id; ?>" style="position: absolute; top: <?php echo $image->position_y; ?>px; left: <?php echo $image->position_x; ?>px;" href="<?php echo \Uri::create('admin/product/infotab_edit/' . $product->id . '/' . $infotab->unique_id . '/' . $image->id); ?>">
                        						<?php //echo \Theme::instance()->asset->img('hotspot/'.$image_name, array('class' => 'hotspot', 'style' => 'position: absolute; top: '.$image->position_y.'px; left: '.$image->position_x.'px;')); ?>
                        						<?php echo \Theme::instance()->asset->img('hotspot/'.$image_name); ?>
                            				</a>
                            			<?php endif; ?>
                            			
                            		<?php endforeach; ?>
                            	<?php endif; ?>
                            	
                        	</div>
                        	<div class="clear"></div>
                    	</div>
                    </div>
                </div>
                
                <!-- HOTSPOT FORM -->
                <?php if(isset($hotspot)): ?>
                    
                    <?php echo \Form::open(array('action' => \Uri::create('admin/product/infotab_hotspot/' . $product->id . '/' . $infotab->unique_id . '/' . $hotspot->id), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>
                    
	                    <!-- Accordions Panel -->
	                    <div class="panel panel-default">
							<div class="panel-heading panelHeader">
								<h3 class="panel-title pull-left">Hotspot Details</h3>
								<div class="pull-right togglePanel ">
		                			<a><i class="fa fa-minus"></i></a><a><i class="fa fa-plus"></i></a></div>
                            </div>
                            <div class="panel-body panelContent">        
								<!-- DESCRIPTION PANEL -->
								<div class="form-group">
                                	<?php echo \Form::label('Hotspot Title' . '  <span class="text-danger">*</span>'); ?>
                                    <div class="input_holder"><?php echo \Form::input('title', \Input::post('title', $hotspot->title), array('class'=> 'form-control')); ?></div>
                                </div>
                                <div class="form-group">
                                    <?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
                                    <div class="clear"></div>
                                    <?php echo \Form::textarea('description', \Input::post('description', $hotspot->description), array('class' => 'form-control ck_editor')); ?>
                                </div>
								
								<!-- END OF: DESCRIPTION PANEL -->
	                            
	                            <!-- IMAGE PANEL -->
	                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered sortable">
                                    <tr class="nodrop nodrag blueTableHead">
                                        <th scope="col" class="noresize">Image</th>
                                    	<th scope="col">Image Properties</th>
                                    	<?php if(!empty($hotspot->image)): ?>
                                    		<th scope="col" class="center">Delete</th>
                                    	<?php endif; ?>
                                    </tr>
                                    
                                    <tr class="nodrop nodrag">
                                        <td class="td-thumb hotspot_image">
                                        	<?php if(!empty($hotspot->image)): ?>
	                                        	<?php echo \Html::img(\Helper::amazonFileURL('media/images/' . key(\Config::get('infotab.image.resize', array('' => ''))) . $hotspot->image), array('width' => '80', 'height' => '80', 'class' => 'default')); ?>
                                        	<?php else: ?>
												<i class="fa fa-picture-o"></i>
                                        	<?php endif; ?>
                                        </td>
                                        <td class="upload">
                                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td class="noresize">Alt Text</td>
                                                    <td>
                                                    	<div class="input_holder">
	                                                        <?php echo \Form::input('alt_text', \Input::post('alt_text', $hotspot->alt_text), array('class'=> 'form-control')); ?>
                                                    	</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="noresize">Choose Image</td>
                                                    <td>
                                                    	<?php echo \Form::file('image'); ?>
                                                    	<?php if(!empty($hotspot->image)): ?>
                                                    		<?php echo \Form::hidden('image_db', $hotspot->image); ?>
                                                    	<?php endif; ?>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <?php if(!empty($hotspot->image)): ?>
                                    		<td class="icon center">
                                            	<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/product/delete_infotab_image/' . $hotspot->id . '/' . $infotab->unique_id . '/hotspot/image'); ?>">Delete</a>
                                            </td>
                                    	<?php endif; ?>
                                    </tr>
                                    
                                </table>
	                            <div class="clear"></div>
                                <!-- END OF: IMAGE PANEL -->
	                            
                                <!-- VIDEO PANEL -->
                             	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_b_10">
                            		<tr>
                            			<td class="span7 p_r_10">
                            				<small><span class="text-danger">*</span> Video instructions always placed here; For example: insert a Youtube URL link
                                    		Default document will be the top image. You can rearange the order of documents by performing a simple drag and drop function.</small>
                            			</td>
                            		</tr>
                            		<tr>
                            			<td class="span5 sort_message_container"></td>
                            		</tr>
                            	</table>
                                <div class="clear"></div>
                                
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" video="<?php echo \Uri::create('admin/product/video/'); ?>" class="table table-striped table-bordered sortable ">
                                    <tr class="nodrop nodrag blueTableHead">
                                        <th scope="col" class="noresize">Video File</th>
                                        <th scope="col">Video File Properties</th>
                                        <?php if(!empty($hotspot->video)): ?>
                                            <th scope="col" class="center">Delete</th>
                                        <?php endif; ?>
                                    </tr>
                                    
                                    <?php if($hotspot->video): ?>
                                    	<?php
                                    		$youtube = \App\Youtube::forge(); 
                                    		$hotspot->video_details = $youtube->parse($hotspot->video)->get(); 
                                    	?>
                                    <?php endif; ?>
                                    
                                    <tr class="nodrop nodrag">
                                        <td class="td-thumb">
                                        	
		                                    <?php if($hotspot->video): ?>
                                        		<img src="<?php echo $hotspot->video_details['thumbnail']['small']; ?>" class="default" width="80"/>
                                        	<?php else: ?>
												<i class="fa fa-youtube"></i>
	                                    	<?php endif; ?>
                                        </td>
                                        <td class="upload">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            	<tr>
                                                    <td class="noresize">Video URL</td>
                                                    <td>
                                                    	<div class="input_holder">
                                                    		<?php echo \Form::input('video_url', \Input::post('video_url', $hotspot->video), array('class' => 'video_url form-control')); ?>
                                                    	</div>
                                                    </td>
                                                    <td class="noresize v_top_exception">
                                                    	<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="noresize">Video Title</td>
                                                    <td colspan="2">
                                                    	<div class="input_holder">
                                                    		<?php echo \Form::input('video_title', \Input::post('video_title', $hotspot->video_title), array('class' => 'video_title form-control')); ?>
                                                    	</div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <?php if(!empty($hotspot->video)): ?>
                                            <td class="icon center">
                                            	<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete video?" href="<?php echo \Uri::create('admin/product/delete_infotab_image/' . $hotspot->id . '/' . $infotab->unique_id . '/hotspot/video'); ?>">Delete</a>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
                                    
                                </table>
                                
                                <?php \Theme::instance()->asset->js('product/videos.js', array(), 'basic'); ?>
	                            <!-- END OF: VIDEO PANEL -->
                            </div>
                        </div><!-- EOF Accordions Panel -->
						
                        <div class="save_button_holder text-right">
                            <?php echo \Form::button('update', '<i class="fa fa-edit"></i>Save Hotspot', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
                        </div>
                        
                    <?php echo \Form::close(); ?>
					<!-- END OF: HOTSPOT FORM -->
				<?php endif; ?>
                
			<?php endif; ?>
		    <!-- EOF Content -->
		</div>
			    <script>
                    var product_id = <?php echo $product->id; ?>;
                    var infotab_id = <?php echo $infotab->unique_id; ?>;
					var hotspot_id = <?php echo isset($hotspot) ? $hotspot->id : 'false'; ?>;
				</script>
				<?php echo \Theme::instance()->asset->js('product/hotspots.js'); ?>
			    
			    <?php echo ckeditor_replace('ck_editor'); ?>
	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
<?php echo \Theme::instance()->asset->js('product/videos.js'); ?>