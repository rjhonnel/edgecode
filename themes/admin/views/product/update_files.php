<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Product Manager', 'admin/product/list');
		\Breadcrumb::set($product->title, 'admin/product/update/' . $product->id);
		\Breadcrumb::set('Files and Links');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Product: Documents & Videos</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>


			<?php if(\Config::get('details.file.enabled', false)): ?>
			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
			<?php echo \Form::hidden('file_upload', 1); ?>
				<div class="panel panel-default">

					<div class="panel-heading">
						<h3 class="panel-title pull-left">File Manager<?php echo \Config::get('details.file.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>
					</div>
					<div class="panel-body">
						<small class="alert alert-info">
							 <span class="text-danger">*</span> List of acceptable files types: MS Word, PDF, PowerPoint, MS Access.<br>
							You can rearrange the order of the files by performing drag and drop.
						</small>
						<div class="span5 sort_message_container"></div>
						<div class="header_messages"></div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/product/sort/file/' . $product->id); ?>" class="sortable table table-striped table-bordered">
							<tr class="nodrop nodrag blueTableHead">
								<th scope="col" class="">File</th>
								<th scope="col">File Properties</th>
								<?php if((!\Config::get('details.file.required', false) && !empty($product->files)) || count($product->files) > 1): ?>
									<?php if(count($product->files) > 1): ?>
										<th scope="col" class="center">Re-order</th>
									<?php endif; ?>
									<th scope="col" class="center">Delete</th>
								<?php endif; ?>
							</tr>

							<?php if(is_array($product->files)): ?>
								<?php foreach($product->files as $file): ?>
									<?php
									// Get file extension
									$extension = strtolower(pathinfo($file->file, PATHINFO_EXTENSION));
									?>
									<tr id="sort_<?php echo $file->id . '_' . $file->sort; ?>">
										<td class="td-thumb ">
											<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
												<i class="fa fa-file-<?php echo $extension; ?>-o"></i>
											</a>
										</td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="120"><div class="input_holder">File Name</div></td>
													<td>
														<div class="input_holder"><i>
																<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
																	<?php echo $file->file; ?>
																</a>
															</i></div>
													</td>
												</tr>
												<tr>
													<td width="120">Title</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('title_'.$file->id, \Input::post('title_' . $file->id, $file->title), array('class'=>'form-control')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td width="120">Replace File</td>
													<td>
														<?php echo \Form::file('file_'.$file->id); ?>
														<?php echo \Form::hidden('file_db_'.$file->id, $file->file); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if(count($product->files) > 1): ?>
											<td width="110" class="dragHandle">
												<ul class="table-action-inline">
													<li class="dragHandle">
														<a href="" onclick="return false;">Re-order</a>
													</li>
												</ul>
											</td>
										<?php endif; ?>
										<td width="110">
											<ul class="table-action-inline">
												<li>
													<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete file?" href="<?php echo \Uri::create('admin/product/delete_file/' . $file->id . '/' . $product->id); ?>">
														Delete
													</a>
												</li>
											</ul>
										</td>
									</tr>

								<?php endforeach; ?>
							<?php endif; ?>

							<?php if(\Config::get('details.file.multiple', false) || empty($product->files)): ?>
								<tr class="nodrop nodrag">
									<td class="td-thumb">
										<div class="fa fa-file-text-o"></div>
									</td>
									<td class="upload">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td >Title</td>
												<td>
													<div class="input_holder">
														<?php echo \Form::input('title_new_1', \Input::post('title_new_1'), array('class'=>'form-control')); ?>
													</div>

												</td>
											</tr>
											<tr>
												<td width="120">Choose File</td>
												<td>
													<?php echo \Form::file('file_new_1'); ?>
												</td>
											</tr>
										</table>
									</td>
									<?php if(count($product->files) > 1): ?>
										<td class="icon center"></td>
									<?php endif; ?>
									<?php if(count($product->files)): ?>
										<td class="icon center"></td>
									<?php endif; ?>
								</tr>
							<?php endif; ?>

						</table>
					</div>
					<div class="panel-footer text-right">
						<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success')); ?>
					</div>
				</div>

				<?php echo \Form::close(); ?>
				<?php endif; ?>


				<?php if(\Config::get('details.video.enabled', false)): ?>
					<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
					<?php echo \Form::hidden('video_upload', 1); ?>
					<div class="panel panel-default">

						<div class="panel-heading">
							<h3 class="panel-title pull-left">Video Manager</h3>
						</div>

						<div class="panel-body">

							<small class="alert alert-info">
								 <span class="text-danger">*</span> Insert a Youtube or Vimeo URL link
							</small>

							<span class="sort_message_container"></span>
							<div class="header_messages"></div>
							<table video="<?php echo \Uri::create('admin/product/video/'); ?>" rel="<?php echo \Uri::create('admin/product/sort/video/' . $product->id); ?>" class="table table-striped table-bordered sortable ">
								<tr class="nodrop nodrag blueTableHead">
									<th scope="col" class="">Video</th>
									<th scope="col">Video Properties</th>
									<?php if((!\Config::get('details.video.required', false) && !empty($product->videos)) || count($product->videos) > 1): ?>
										<?php if(count($product->videos) > 1): ?>
											<th scope="col" class="center">Re-order</th>
										<?php endif; ?>
										<th scope="col" class="center">Delete</th>
									<?php endif; ?>
								</tr>

								<?php if(is_array($product->videos)): ?>
									<?php foreach($product->videos as $video): ?>
										<?php
										$youtube = \App\Youtube::forge();
										$video->details = $youtube->parse($video->url)->get();
										?>
										<tr id="sort_<?php echo $video->id . '_' . $video->sort; ?>">
											<td class="center video_thumbnail" width="100">
												<?php if($video->thumbnail): ?>
													<img src="<?php echo \Helper::amazonFileURL('media/videos/' . $video->thumbnail); ?>" class="default" width="80"/>
													<div style="margin-top: 5px; position: relative;">
														<?php echo \Form::checkbox('video_delete_image_' . $video->id, 1, \Input::post('video_delete_image_' . $video->id), array('class' => 'video_delete_image', 'style' => 'margin: 0;')); ?>
														<span style="font-size: 9px;">Use YouTube?</span>
													</div>
												<?php else: ?>
													<img src="<?php echo $video->details['thumbnail']['small']; ?>" class="default" width="80"/>
												<?php endif; ?>
											</td>

											<td class="upload">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="120">Video URL</td>
														<td>
															<div class="input_holder">
																<?php echo \Form::input('video_url_'.$video->id, \Input::post('video_url_' . $video->id, $video->url), array('class' => 'video_url form-control')); ?>
															</div>
														</td>
														<td valign="top">
															<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
														</td>
													</tr>
													<tr>
														<td width="120">Video Title</td>
														<td colspan="2">
															<div class="input_holder">
																<?php echo \Form::input('video_title_'.$video->id, \Input::post('video_title_' . $video->id, $video->title), array('class' => 'video_title form-control')); ?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="">Thumbnail Image</td>
														<td>
															<?php echo \Form::file('video_file_'.$video->id); ?>
															<?php if($video->thumbnail) : ?>
																<?php echo \Form::hidden('video_file_db_'.$video->id, $video->thumbnail); ?>
															<?php endif; ?>
														</td>
													</tr>
												</table>
											</td>
											<?php if(count($product->videos) > 1): ?>
												<td width="110" class="dragHandle">
													<ul class="table-action-inline">
														<li class="dragHandle">
															<a href="" onclick="return false;">Re-order</a>
														</li>
													</ul>
												</td>
											<?php endif; ?>
											<td width="110">
												<ul class="table-action-inline">
													<?php if((!\Config::get('details.video.required', false) && !empty($product->videos)) || count($product->videos) > 1): ?>
														<li>
															<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete video?" href="<?php echo \Uri::create('admin/product/delete_video/' . $video->id . '/' . $product->id); ?>">
																Delete
															</a>
														</li>
													<?php endif; ?>
												</ul>
											</td>
										</tr>

									<?php endforeach; ?>
								<?php endif; ?>

								<?php if(\Config::get('details.video.multiple', false) || empty($product->videos)): ?>
									<tr class="nodrop nodrag">
										<td width="100" class="td-thumb"><i class="fa fa-youtube fa-lg"></i></td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="100">Video URL</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('video_url_new_1', \Input::post('video_url_new_1'), array('class' => 'form-control video_url')); ?>
														</div>
													</td>
													<td valign="top">
														<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
													</td>
												</tr>
												<tr>
													<td width="120">Video Title</td>
													<td colspan="2">
														<div class="input_holder">
															<?php echo \Form::input('video_title_new_1', \Input::post('video_title_new_1'), array('class' => 'form-control video_title')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td class="">Thumbnail Image</td>
													<td>
														<?php echo \Form::file('video_file_new_1'); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if((!\Config::get('details.video.required', false) && !empty($product->videos)) || count($product->videos) > 1): ?>
											<?php if(count($product->videos) > 1): ?>
												<td class="icon center"></td>
											<?php endif; ?>
											<td class="icon center"></td>
										<?php endif; ?>
									</tr>
								<?php endif; ?>

							</table>

						</div>

						<div class="panel-footer text-right">
							<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success')); ?>
						</div>
					</div>

					<?php \Theme::instance()->asset->js('product/videos.js', array(), 'basic'); ?>


					<?php echo \Form::close(); ?>
				<?php endif; ?>



		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<?php echo \Theme::instance()->asset->js('product/videos.js'); ?>

            
            
            
			