<?php \Config::load('product::product', 'product', true); ?>

<?php if(isset($product)): // Used on "UPDATE" products?>

<div class="panel-nav-holder">
    <div class="btn-group">
        <a href="<?php echo \Uri::create('admin/product/update/' . $product->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-info-circle"></i> General Information</a>
        <a href="<?php echo \Uri::create('admin/product/price/update/' . $product->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-tag"></i> Prices and Attributes</a>
        <a href="<?php echo \Uri::create('admin/product/related/list/' . $product->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-share-alt"></i> Related Products</a>
        <a href="<?php echo \Uri::create('admin/product/infotab_list/' . $product->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-list"></i> Info Tabs</a>
        <a href="<?php echo \Uri::create('admin/product/update_seo/' . $product->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-check-circle-o"></i> Meta Content</a>
        <a href="<?php echo \Uri::create('admin/product/deal/list/' . $product->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-tags"></i> Deals</a>
    </div>
</div>

<?php else: // Used on "CREATE" product ?>
    <?php if(FALSE): ?>
        <div class="second_menu_wrapper">
            <div class="second_menu">
                <ul>
                    <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
                </ul>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>

<script type="text/javascript">
    $(document).ready(function(){
        if($(".btn-group").length > 0)
        {
            $(".btn-group a.active").removeClass('active');
            $(".btn-group a").each(function(){
                if(($(this).attr("href") == uri_current) ||
                    ((uri_current.indexOf("deal") != -1 && $(this).attr("href").indexOf("deal") != -1) || (uri_current.indexOf("infotab") != -1 && $(this).attr("href").indexOf("infotab") != -1)))
                {
                    $(this).addClass('active');
                }
            });
        }
    });
</script>
