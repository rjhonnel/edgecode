<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Customers');
		\Breadcrumb::set('Pricing Group Manager', 'admin/product/group/list/pricing');
		\Breadcrumb::set($group->title, 'admin/product/group/update/' . $group->id);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Pricing Group: General Information</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/group/_action_links', array('group' => $group, 'update'=>1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/group/_navbar_links', array('group' => $group)); ?>


			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
			<?php echo \Form::hidden('details', 1); ?>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">General Information</h3>

				</div>
				<div class="panel-body">
					<div class="form-group" style="margin:0">
						<?php echo \Form::label('Pricing Group Name' . '  <span class="text-danger">*</span>'); ?>
						<div class="input_holder"><?php echo \Form::input('title', \Input::post('title', $group->title),array('class'=>'form-control')); ?></div>
					</div>

					<?php if($group->type == 'property'): ?>
						<div class="form-group">
							<?php echo \Form::label('Description'); ?>
							<?php echo \Form::textarea('description_full', \Input::post('description_full', $group->description_full), array('class' => 'form-control ck_editor')); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Assign Customer Group to Pricing Group</h3>

				</div>
				<div class="panel-body">
					<table class="table table-striped table-bordered sortable">
						<thead>
						<tr class="blueTableHead">
							<th scope="col">Customer Groups</th>
							<th scope="col">Active</th>
						</tr>
						</thead>

						<tbody>
						<?php if(!empty($a_groups)): ?>
							<?php foreach($a_groups as $product_group):
								if($product_group['user_group'] != ''){
									?>
									<tr <?php echo isset($product_group['disabled']) && $product_group['disabled'] ? 'class="o50"' : ''; ?>>

										<td><?php echo $product_group['user_group']; ?><?php //if(count($product_group->products) > 0) echo ' (' . count($product_group->products > 0) . ')'; ?></td>
										<td class="btn_able_to_buy" width="80">
											<?php echo \Form::hidden('action[' . $product_group['user_group_id'] . ']', 0); ?>
											<?php // echo \Form::hidden('able_to_buy[' . $product_group['user_group_id'] . ']', 0); ?>
											<?php echo \Form::radio('rdo_group', $product_group['user_group_id'], $product_group['active'] ?  true : null, array('class' => 'check_active hide', 'data-id' => $product_group['id'], 'disabled' => (isset($product_group['disabled']) && $product_group['disabled']? true : false))); ?>
											<?php if (!isset($product_group['disabled'])): ?>
												<label><?php echo \Form::checkbox('able_to_buy[' . $product_group['user_group_id'] . ']', 1, isset($product_group['able_to_buy']) ? $product_group['able_to_buy'] : 0, array('data-value' => $product_group['user_group_id'])); ?> Yes</label>
											<?php endif;?>
										</td>

									</tr>

									<tr class="highlighted_row" style="display: none;">
										<td colspan="7">
											<?php echo \Form::hidden('apply_tier_to_sale[' . $product_group['user_group_id'] . ']', 0); ?>
											<?php echo \Form::checkbox('apply_tier_to_sale[' . $product_group['user_group_id'] . ']', 1, isset($product_group['apply_tier_to_sale']) ? $product_group['apply_tier_to_sale'] : 0); ?>
											&nbsp;Apply additional tier pricing discount to items to sale
											<div class="discount_container_holder">
												<?php if(isset($product_group['discounts']) && count($product_group['discounts']) > 1): ?>
													<?php foreach(array_slice($product_group['discounts'], 1) as $discount): ?>
														<?php if($discount->qty > 0): ?>
															<div class="discount_add_fields">
																<table>
																	<thead>
																	<tr>
																		<td>QTY +</td>
																		<td>Discount</td>
																	</tr>
																	</thead>
																	<tbody class="discount_container">
																	<tr>
																		<td>
																			<?php echo \Form::input('qty[' . $product_group['user_group_id'] . '][' . $discount->id . ']', $discount->qty, array('class' => 'medium_txt_input')); ?>
																		</td>
																		<td>
																			<span class="percentage_discount">%</span>
																			<?php echo \Form::input('discount[' . $product_group['user_group_id']. '][' . $discount->id . ']', $discount->discount, array('class' => 'small_txt_input')); ?>
																			<a href="#" class="close_discount"></a>
																		</td>
																	</tr>
																	</tbody>
																</table>
															</div>
														<?php endif; ?>
													<?php endforeach; ?>
												<?php endif; ?>
											</div>

											<div class="wide m_t_10">
												<div class="right">
													<button class="btn btn-primary btn-small m_r_15 left add_discount" type="button"><i class="icon-plus icon-white"></i> Add</button>
													<button class="btn btn-primary btn-small m_r_15 left close_tier" type="button"><i class="icon-remove icon-white"></i> Close</button>
													<?php /*<button class="btn btn-success btn-small left" type="button"><i class="icon-ok icon-white"></i> Save</button>*/ ?>

													<div class="discount_template" style="display: none;">
														<div class="discount_add_fields">
															<table>
																<thead>
																<tr>
																	<td>QTY +</td>
																	<td>Discount</td>
																</tr>
																</thead>
																<tbody class="discount_container">
																<tr>
																	<td>
																		<?php echo \Form::input('new_qty[' . $product_group['user_group_id'] . '][]', '', array('class' => 'medium_txt_input', 'disabled' => 'disabled')); ?>
																	</td>
																	<td>
																		<span class="percentage_discount">%</span>
																		<?php echo \Form::input('new_discount[' . $product_group['user_group_id'] . '][]', '', array('class' => 'small_txt_input', 'disabled' => 'disabled')); ?>
																		<a href="#" class="close_discount"></a>
																	</td>
																</tr>
																</tbody>
															</table>
														</div>
													</div>

												</div>
											</div>
										</td>
									</tr>

								<?php }
							endforeach; ?>
						<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>

			<?php if($group->type == 'property'): ?>
				<!-- Images Panel -->
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h4>
					</div>
					<div class="panel-body">
						<div class="span4 sort_message_container"></div>
						<div class="clear"></div>

						<table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/product/group/sort/image/' . $group->id); ?>" class="greyTable2 sortable files_table separated ">
							<tr class="nodrop nodrag blueTableHead">
								<th scope="col" class="noresize">Image</th>
								<th scope="col">Image Properties</th>
								<?php if(count($group->images) > 1): ?>
									<th scope="col" class="center">Re-order</th>
								<?php endif; ?>
								<?php if((!\Config::get('details.image.required', false) && !empty($group->images)) || count($group->images) > 1): ?>
									<th scope="col" class="center">Delete</th>
								<?php endif; ?>
							</tr>

							<?php if(is_array($group->images)): ?>
								<?php foreach($group->images as $image): ?>

									<tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
										<td class="center noresize">
											<img src="<?php echo \Uri::create('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $image->image); ?>" width="74" height="74" alt="<?php echo $image->alt_text; ?>"/>
										</td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td class="noresize">Alt Text</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text)); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td class="noresize">Replace Image</td>
													<td>
														<?php echo \Form::file('image_'.$image->id); ?>
														<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
													</td>
												</tr>
											</table>
										</td>

										<td width="110">
											<ul class="table-action-inline">
												<?php if(count($group->images) > 1): ?>
													<li>
														<a href="" onclick="return false;">Order</a>
													</li>
												<?php endif; ?>
												<?php if((!\Config::get('details.image.required', false) && !empty($group->images)) || count($group->images) > 1): ?>
													<li>
														<a href="<?php echo \Uri::create('admin/product/group/delete_image/' . $image->id . '/' . $group->id); ?>">
															Delete
														</a>
													</li>
												<?php endif; ?>
											</ul>
										</td>
									</tr>

								<?php endforeach; ?>
							<?php endif; ?>

							<?php if(\Config::get('details.image.multiple', false) || empty($group->images)): ?>
								<tr class="nodrop nodrag">
									<td class="td-thumb">
										<i class="fa fa-picture-o"></i>
									</td>
									<td class="upload">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="noresize">Alt Text</td>
												<td>
													<div class="input_holder">
														<?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1')); ?>
													</div>
												</td>
											</tr>
											<tr>
												<td class="noresize">Choose Image</td>
												<td>
													<?php echo \Form::file('image_new_1'); ?>
												</td>
											</tr>
										</table>
									</td>
									<?php if(count($group->images) > 1): ?>
										<td class="icon center"></td>
									<?php endif; ?>
									<?php if((!\Config::get('details.image.required', false) && !empty($group->images)) || count($group->images) > 1): ?>
										<td class="icon center"></td>
									<?php endif; ?>
								</tr>
							<?php endif; ?>

						</table>

					</div>
				</div><!-- EOF Images Panel -->
			<?php endif; ?>


			<?php if($group->type != 'pricing'): // Show treeview only on property groups ?>
				<?php echo \Theme::instance()->view('views/product/group/_tree_links', array('group' => $group)); ?>-- EOF Sidebar Holder -->
			<?php endif; ?>


			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1', 'id' => 'save_button_down')); ?>
				<?php echo \Form::button('exit', '<i class="fa fa-check"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<script>
	var product_id = <?php echo isset($product) ? $product->id : '0'; ?>;
</script>
<?php echo \Theme::instance()->asset->js('product/related/drag_lists.js'); ?>

<?php echo ckeditor_replace('ck_editor'); ?>

<script type="text/javascript">
	$(window).load(function() {
		count_items();
	});

	$(document).ready(function(){
		$(".add_discount").on('click', function(e){
			e.preventDefault();
			var content = $(this).parents().first().find('div').html();
			var container = $(this).parents('td').first().find('.discount_container_holder');
			container.append(content);
			container.find('input').removeAttr('disabled');
		});
		$(document).on('click', '.close_discount', function(e){
			e.preventDefault();
			$(this).parents('.discount_add_fields').first().remove();
		});
		$(document).on('click', '.edit_btn', function(e){
			e.preventDefault();
			element = $(this).parents('tr').first().next();

			if(element.hasClass('highlighted_row'))
				element.slideToggle();
		});
		$(document).on('click', '.close_tier', function(e){
			e.preventDefault();
			element = $(this).parents('tr').first();
			element.slideToggle();
		});

		$('.btn_able_to_buy input[type="checkbox"]').change(function() {
			if ($(this).prop('checked')) {
				$("input:radio[name=rdo_group][value=" + $(this).data("value") + "]").attr('checked', true);
				$('.btn_able_to_buy :checkbox').not('input:checkbox[id="'+$(this).attr('id')+'"]').prop('checked', false);
			} else {
				$("input:radio[name=rdo_group][value=" + $(this).data("value") + "]").attr('checked', false);
			}
		});

	});
</script>