<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Customers');
        \Breadcrumb::set('Pricing Group Manager', 'admin/product/group/list/pricing');
        \Breadcrumb::set($group->title, 'admin/product/group/update/' . $group->id);
        \Breadcrumb::set('All Products');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Edit Pricing Group: All Products</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/product/group/_action_links', array('group' => $group)); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/product/group/_navbar_links', array('group' => $group)); ?>

            <div class="top-filter-holder">
                <div class="filter-holder">
                <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
                <?php
                echo \Theme::instance()->view('views/product/group/_search_filters', array(
                    'pagination' => $pagination,
                    'status' => $status,
                    'module' => 'product code or',
                    'options' => array('status', 'category_id'),
                    'layout' => '2',
                ), false);
                ?>
                <?php echo \Form::close(); ?>    
                </div>
            </div>
            


            <?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>
            <div class="table-utility">
                <button name="add" value="add" type="submit" class="btn btn-sm btn-primary pull-right">
                    <i class="fa fa-plus"></i> Assign to Pricing Group
                </button>
                <?php if( !isset($show_reset) || $show_reset === true ): ?>
                    <button class="btn btn-sm btn-default pull-left check_em_all" type="button"><i class="fa fa-check"></i> Select All</button>
                <?php endif; ?>
            </div>
            <table class="table table-striped table-bordered">
                <thead>
                <tr class="blueTableHead">
                    <th class="all_unselected noresize" scope="col" style="width: 2%;"><?php echo \Form::checkbox('select_all', 'products[add]'); ?></th>
                    <th scope="col" class="noresize" style="width: 11.7%;">Code</th>
                    <th scope="col">Product Name</th>
                    <th scope="col" class="noresize" style="width: 19.2%;">Category</th>
                    <th scope="col" class="noresize" style="width: 8%;">Price</th>
                    <th scope="col" class="noresize" style="width: 7%;">Status</th>
                </tr>
                </thead>
                <tbody class="remove">
                <?php if(!empty($items)): ?>
                    <?php foreach($items as $not_related): ?>
                        <?php $product_data = $not_related->data; ?>
                        <tr>
                            <td class="noresize"><?php echo \Form::checkbox('products[add][]', $not_related->id); ?></td>
                            <td class="noresize" style="width: 11.7%;"><?php echo $product_data['code']; ?></td>
                            <td><?php echo $not_related->title; ?></td>
                            <td class="noresize" style="width: 19.2%;"><?php echo $product_data['category']; ?></td>
                            <td class="noresize" style="width: 8%;"><?php echo $product_data['price'] ? nf($product_data['price']) : '-';  ?></td>
                            <td class="noresize" style="width: 7%;"><?php echo $not_related->status == 0 ? 'Inactive' : 'Active'; ?></td>
                        </tr>

                    <?php endforeach; ?>
                    <?php $no_items = 'style="display: none;"'?>
                <?php else: ?>
                    <?php $no_items = ''?>
                <?php endif; ?>

                <tr class="no_items" <?php echo $no_items; ?>>
                    <td class="noresize center" colspan="6">There are no more products to add</td>
                </tr>

                </tbody>
            </table>
            <?php echo \Form::close(); ?>


            <div class="pagination-holder">
                <?php echo $pagination->render(); ?>
            </div>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>


<script>
	// var product_id = <?php // echo $product->id; ?>;

    $(document).ready(function(){
        var b_changed_enable_all = false;
        // var onchange_checkbox = ($('.btn_enable_all :checkbox')).iphoneStyle({
        //     onChange: function(elem, value) {
        //         b_changed_enable_all = true;
        //     }
        // });
        $('.apply_all').click( function(){
            if ($("input[name='discount_all']").val()) {
                $("input[name*='products[update][discount]']").val($("input[name='discount_all']").val());
            }
            if (b_changed_enable_all) {
                $("input[name*='products[update][able_discount]']").each(function (){
                    var this_box = $(this).iphoneStyle();
                    if ($('.btn_enable_all :checkbox').is(':checked')) {
                        this_box.prop('checked', true).iphoneStyle("refresh");
                    } else {
                        this_box.prop('checked', false).iphoneStyle("refresh");
                    }
                });
            }

            $("input[name='discount_all']").val('');
            // onchange_checkbox.prop('checked', false).iphoneStyle("refresh");
        });

        $('.check_em_all').click( function(){
            $(".all_unselected input[name='select_all']").prop('checked', true);
            $("input[name*='products[add]']").each(function (){
                $(this).prop('checked', true);
            });
        });
        $('input[name="select_all"]').click( function(){
            var _this = $(this);
            $("input[name*='products[add]']").each(function (index, elem){
                $(elem).prop('checked', _this.is(':checked'));
            });
        });
    });

</script>
<?php echo \Theme::instance()->asset->js('product/related/drag_lists.js'); ?>
