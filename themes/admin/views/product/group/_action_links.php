<?php 
    $group_type = isset($group_type) ? $group_type : (isset($group) ? $group->type : 'property'); 
?>


<?php if(isset($save) && $save==1): ?>
<a id="save_product_assigned" href="#" class="btn btn-success"><i class="fa fa-edit"></i> Save</a>
<?php endif; ?>
<?php if(isset($update) && $update==1): ?>
<a id="save_button_up" href="#" class="btn btn-success"><i class="fa fa-edit"></i> Save</a>
<?php endif; ?>
<?php if(!isset($hide_show_all)): ?>
	<a href="<?php echo \Uri::create('admin/product/group/list/' . $group_type); ?>" class="btn btn-sm btn-default"><i class="fa fa-list"></i> Show All</a>
<?php endif; ?>