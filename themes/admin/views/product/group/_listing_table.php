<div class="top-filter-holder">
	<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
	<?php
	echo \Theme::instance()->view('views/_partials/search_filters', array(
		'pagination' => $pagination,
		'status' => array(),
		'module' => 'group',
		'options' => $group_type != 'pricing' ? array('group_id') : array(),
	), false);
	?>
	<?php echo \Form::close(); ?>

	<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
	<div class="form-inline pull-right">
		<label>Show entries:</label>
		<?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
	</div>
	<?php echo \Form::close(); ?>
</div>

<table class="table table-striped table-bordered">
	<thead>
	<tr class="blueTableHead">
		<th scope="col">Pricing Group Name</th>
		<th scope="col" class="center" style="width: 40px;">Products</th>
		<td scope="col" class="center" width="150">Actions</td>
	</tr>
	</thead>
	<tbody>

	<?php foreach($items as $item): ?>
		<?php $item = (Object)$item; ?>

		<tr>
			<td>
				<a href="<?php echo \Uri::create('admin/product/group/update/' . $item->id); ?>">
					<strong><?php echo $item->title; ?></strong>
				</a>
			</td>
			<td class="center">
				<a href="<?php echo \Uri::create('admin/product/group/products/' . $item->id); ?>">
					<strong><?php echo count($item->products) == 0 ? '/' : count($item->products); ?></strong>
				</a>
			</td>
			<td width="110">
				<ul class="table-action-inline">
					<li>
						<a href="<?php echo \Uri::create('admin/product/group/update/' . $item->id); ?>">Edit</a>
					</li>
					<li>
						<a href="<?php echo \Uri::create('admin/product/group/delete/' . $item->id); ?>" class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete pricing group?">Delete</a>
					</li>
				</ul>
			</td>
		</tr>

	<?php endforeach; ?>

	<?php if(empty($items)): ?>

		<tr class="nodrag nodrop">
			<td colspan="4" class="center"><strong>There are no items.</strong></td>
		</tr>

	<?php endif; ?>

	</tbody>
</table>

<div class="pagination-holder">
	<?php echo $pagination->render(); ?>
</div>