<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Customer');
        \Breadcrumb::set('Pricing Group Manager', 'admin/product/group/list/pricing');
        //     \Breadcrumb::set(ucfirst($group_type) . ' Groups', 'admin/product/group/list/' . $group_type);

        // If viewing subgroups display parent group title
        if(isset($group))
            \Breadcrumb::set($group->title);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Pricing Groups</h4>

                <div class="pull-right">
                    <?php
                        if(isset($group))
                            echo \Theme::instance()->view('views/product/group/_action_links');
                        else
                            echo \Theme::instance()->view('views/product/group/_action_links', array('hide_show_all' => 1));
                    ?>
                </div>
            </header>

            <?php
            // Load page listing table
            echo \Theme::instance()->view('views/product/group/_listing_table',
                array(
                    'pagination' 	=> $pagination,
                    'items'			=> $items,
                    'group_type'	=> $group_type,
                ),
                false
            );
            ?>

            <?php echo \Form::open(array('action' => \Uri::create('admin/product/group/create/'.$group_type))); ?>
                <div class="panel panel-default panel-body mt-20px">
                    <div class="row">
                        <div class="col-sm-10">
                            <?php echo \Form::hidden('type', $group_type); ?>
                            <?php echo \Form::input('title', \Input::post('title'), array('placeholder' => 'Enter Group Title', 'class' => 'form-control')); ?>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary btn-block">Add New</button>
                        </div>
                    </div>
                </div>
            <?php echo \Form::close(); ?>

            <?php echo \Theme::instance()->view('views/product/group/_tree_links', array('group_type' => $group_type)); ?>

        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
