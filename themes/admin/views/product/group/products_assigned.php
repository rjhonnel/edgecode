<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Customers');
        \Breadcrumb::set('Pricing Group Manager', 'admin/product/group/list/pricing');
        \Breadcrumb::set($group->title, 'admin/product/group/update/' . $group->id);
        \Breadcrumb::set('Products Assigned');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Edit Pricing Group: Product Assigned</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/product/group/_action_links', array('group' => $group, 'save'=>1)); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/product/group/_navbar_links', array('group' => $group)); ?>

            <div class="top-filter-holder">


                <div class="filter-holder">
                    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
                    <?php echo \Theme::instance()->view('views/product/group/_search_filters', array(
                        // 'pagination' => $pagination,
                        'status' => $status,
                        'module' => 'product code or', // 'search_text' => 'Product name',
                        'options' => array('status', 'category_id'),
                        'show_reset' => false,
                        'layout' => '2',
                    ), false);
                    ?>
                    <?php echo \Form::close(); ?>
                </div>




                <div class="pull-right">
                    <div class="form-inline pull-right">
                        <div class="form-group" style="padding: 0 10px">

                            <div class="form-control">
                                <label style="position: relative;top: -3px;" class="checkbox btn_enable_all">
                                    <?php echo \Form::checkbox('enable_all_discount', 1, 0); ?>
                                    Enable All Discount
                                </label>
                            </div>
                        </div>

                        <div class="input-group">
                            <span class="input-group-addon">Discount</span>
                            <input class="form-control" type="text" name="discount_all" value="" style="width:90px">
                            <span class="input-group-addon">%</span>
                        </div>
                        <button name="apply_all" value="apply_all" type="button" class="btn btn-small btn-default apply_all">
                            <i class="fa fa-check"></i> Apply to All
                        </button>
                    </div>
                </div>
            </div>

            <?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>

            <table class="table table-striped table-bordered sortable">
                <thead>
                <tr class="blueTableHead">
                    <th scope="col" class="noresize"><?php echo \Form::checkbox('select_all', 'products[remove]'); ?></th>
                    <th scope="col">Code</th>
                    <th scope="col" colspan="2">Product Name</th>
                    <th scope="col" style="width:20%;" >Category</th>
                    <th scope="col" style="width:30%;" >Attribute</th>
                    <th scope="col" class="noresize">Discount</th>
                    <th scope="col" >% Discount</th>
                    <th scope="col" class="noresize">Fixed Price</th>
                </tr>
                </thead>
                <tbody class="search_table" style="min-height: <?php echo count($group_list)*56; ?>px">
                <?php if(!empty($group_list)): ?>

                    <?php foreach($group_list as $related): ?>

                        <?php $product_data = $related->data;

                        if(isset($product_data['with_attr'])){
                            $i_ctr = 0;
                            foreach($product_data['with_attr']['attribute'] as $key => $attribute){

                                echo \Form::hidden('products[update][attr_id]['.$related->id.'][]', $key);
                                ?>
                                <tr>
                                    <td><?php echo $i_ctr == 0 ? \Form::checkbox('products[remove][]', $related->id) : ''; ?></td>
                                    <td><?php echo $i_ctr == 0 ? $product_data['code'] : \Product\Model_Attribute::get_product_code($key); ?></td>
                                    <td colspan="2"><?php echo $i_ctr == 0 ? $related->title : ''; ?></td>
                                    <td><?php echo $i_ctr == 0 ? $product_data['category'] : ''; ?></td>
                                    <td><?php echo $attribute; ?></td>
                                    <td>
                                        <label><?php echo \Form::checkbox('products[update][able_discount]['.$related->id.']['.$key.']', 1, $product_data['with_attr']['able_discount'][$key]); ?> Yes</label>
                                    </td>
                                    <td><input class="form-control" type="text" name="products[update][discount][<?php echo $related->id; ?>][<?php echo $key; ?>]" value="<?php echo isset($product_data['with_attr']['discount']) && $product_data['with_attr']['discount'][$key] ? $product_data['with_attr']['discount'][$key] : '';  ?>"></td>
                                    <td><input class="form-control" type="text" name="products[update][price][<?php echo $related->id; ?>][<?php echo $key; ?>]" value="<?php echo isset($product_data['with_attr']['price']) && $product_data['with_attr']['price'][$key] ? nf($product_data['with_attr']['price'][$key]) : '-';  ?>"></td>
                                </tr>
                                <?php
                                $i_ctr++;
                            }
                        } else { ?>
                            <tr>
                                <td><?php echo \Form::checkbox('products[remove][]', $related->id); ?></td>
                                <td><?php echo $product_data['code']; ?></td>
                                <td colspan="2"><?php echo $related->title; ?></td>
                                <td><?php echo $product_data['category']; ?></td>
                                <td>No Attribute</td>
                                <td>
                                    <label><?php echo \Form::checkbox('products[update][able_discount]['.$related->id.']', 1, (isset($product_data['able_discount'])?$product_data['able_discount']:0)); ?> Yes</label>
                                </td>
                                <td><input class="form-control" type="text" name="products[update][discount][<?php echo $related->id; ?>]" value="<?php echo $product_data['sale'] ? $product_data['sale'] : '';  ?>"></td>
                                <td><input  class="form-control"  type="text" name="products[update][price][<?php echo $related->id; ?>]" value="<?php echo $product_data['price'] ? nf($product_data['price']) : '';  ?>"></td>
                            </tr>
                            <?php
                        }
                        ?>

                    <?php endforeach; ?>
                    <?php $no_items = 'style="display: none;"'; ?>
                <?php else: ?>
                    <?php $no_items = ''; ?>
                <?php endif; ?>

                <tr class="no_items_result" <?php echo $no_items; ?>>
                    <td class="noresize center" colspan="8">There are no related products</td>
                </tr>
                </tbody>
            </table>
            <?php if(!empty($group_list)): ?>
                <div class="pagination-holder">
                    <?php echo $group_pagination->render(); ?>
                </div>
            <?php endif; ?>

            <div class="save_button_holder" style="margin: 5px 0 20px 0;">
                <?php
                echo \Form::button('remove', '<i class="fa fa-remove"></i> Remove Selected', array('type' => 'submit', 'class' => 'btn btn-danger pull-left', 'value' => 'remove'));

                echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id' => 'save_product_assigned_click', 'class' => 'btn btn-success', 'value' => '1'));
                ?>
            </div>

            <?php echo \Form::close(); ?>






        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>



    
<script>
	// var product_id = <?php // echo $product->id; ?>;

    $(document).ready(function(){
        var b_changed_enable_all = false;
        $( ".btn_enable_all :checkbox" ).change(function() {
            b_changed_enable_all = true;
        });

        $('.apply_all').click( function(){
            if ($("input[name='discount_all']").val()) {
                $("input[name*='products[update][discount]']").val($("input[name='discount_all']").val());   
            }
            if (b_changed_enable_all) {
                $("input[name*='products[update][able_discount]']").each(function (){
                    var this_box = $(this);
                    if ($('.btn_enable_all :checkbox').is(':checked')) {
                        this_box.prop('checked', true);
                    } else {
                        this_box.prop('checked', false);
                    }
                });
            }

            $("input[name='discount_all']").val('');
            // onchange_checkbox.prop('checked', false).iphoneStyle("refresh");
        });

        $('.check_em_all').click( function(){
            $(".all_unselected input[name='select_all']").prop('checked', true);
            $("input[name*='products[add]']").each(function (){
                $(this).prop('checked', true);
            });
        });
        $('input[name="select_all"]').click( function(){
            var _this = $(this);
            $("input[name*='products[remove]']").each(function (index, elem){
                $(elem).prop('checked', _this.is(':checked'));
            });
        });
    });

</script>
<?php echo \Theme::instance()->asset->js('product/related/drag_lists.js'); ?>
           	
                            