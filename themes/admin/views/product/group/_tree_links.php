						<?php 
							
							// Define group type (property, pricing, etc.)
							$group_type = isset($group_type) ? $group_type : (isset($group) ? $group->type : 'property');
						
							// Get groups
							$groups = \Product\Model_Group::find(function($query) use ($group_type){ 
								$query->where('type', $group_type);
								$query->order_by('sort', 'asc');
								$query->order_by('id', 'asc');
							});
							$group = isset($group) ? $group : false;
							$link = isset($link) ? $link : 'update';
							$selected = isset($selected) ? $selected : false;
							
							// hide radio buttons on non property groups
							if($group && $group->type == 'pricing') $group = false;
							// show radio buttons only on update page
							if($group && $group->type == 'property' && $link != 'update') $group = false;
								
						?>
						<?php if($group): ?>
                        <div class="side_tree_holder">
                            <div class="tree_heading">
                                <h4><?php echo ucfirst($group_type); ?> Product Groups</h4>
                                <div id="sidetreecontrol" class="sidetreecontrol"><a href="#">Collapse All</a><a href="#">Expand All</a></div>
                            </div>
                            <div class="tree_content">
                                <div id="sidetree">
                                
                                	<?php if(!$group && empty($groups)): ?>
                                		<div class="wide"><span class="req">Note: </span> There are no <?php echo $group_type; ?> product groups yet.</div>
                                	<?php else: ?>
                                	
	                                    <ul class="treeview" id="tree">
	                                    	
	                                    	<?php if($group): ?>
	                                    		<li>
                                                    <div class="radio_link_holder">
                                                        <?php echo \Form::radio('parent_id', 0, \Input::post('parent_id', $group->parent_id)); ?>
                                                        <a href="#" onclick="return false;">ROOT</a>
                                                    </div>
	                                    		</li>
	                                    	<?php endif; ?>
	                                    	
	                                    	<?php
	                                    		if(!empty($groups))
	                                    		{
		                                    		// If group parent_id or id is in this array than dont show radio input
		                                    		$hide_radio = $group ? array($group->id) : array();
		                                    		
		                                    		$list_subgroups = function($group_item) use (&$hide_radio, $link, &$list_subgroups, $group, $selected)
		                                    		{
		                                    			?><ul><?php
		                                    			foreach($group_item->children as $child)
		                                    			{
		                                    				if($group && (in_array($child->id, $hide_radio) || in_array($child->parent_id, $hide_radio))) 
		                                    					array_push($hide_radio, $child->id);
		                                    					
		                                    				?>
		                                    					<li>
		                                    						<?php echo !empty($child->children) ? '<div class="hitarea"></div>' : ''; ?>
		                                    						<div class="radio_link_holder">
			                                    						<?php if($group): ?>
			                                    							<?php echo \Form::radio('parent_id', $child->id, \Input::post('parent_id', $group->parent_id), (in_array($child->id, $hide_radio) ? array('style' => 'display: none;', 'disabled' => 'diabled') : array())); ?>
			                                    						<?php endif; ?>
			                                    						<a href="<?php echo \Uri::create('admin/product/group/' . $link . '/' . $child->id); ?>" <?php echo $selected == $child->id ? 'class="active"' : ''; ?>>
			                                    							<?php echo $child->title; ?><?php echo !empty($child->children) ? ' <span class="tree_count">('.count($child->children).')</span>' : ''; ?>
			                                    						</a>
			                                    					</div><?php
		                                    				if(!empty($child->children)) 
		                                    					$list_subgroups($child);
		                                    				else
		                                    					?></li><?php
		                                    					
		                                    			}
		                                    			?></ul><?php
		                                    		};
		                                    	
		                                    		foreach($groups as $key => $group_item)
		                                    		{
		                                    			// Only root groups in first pass
		                                    			if($group_item->parent_id == 0)
		                                    			{
		                                    				if($group && in_array($group_item->id, $hide_radio)) 
		                                    					array_push($hide_radio, $group_item->id);
		                                    					
			                                    			?>
			                                    				<li>
			                                    					<?php echo !empty($group_item->children) ? '<div class="hitarea"></div>' : ''; ?>
			                                    					<div class="radio_link_holder">
				                                    					<?php if($group): ?>
			                                    							<?php echo \Form::radio('parent_id', $group_item->id, \Input::post('parent_id', $group->parent_id), (in_array($group_item->id, $hide_radio) ? array('style' => 'display: none;', 'disabled' => 'diabled') : array())); ?>
				                                    					<?php endif; ?>
			                                    						<a href="<?php echo \Uri::create('admin/product/group/' . $link . '/' . $group_item->id); ?>" <?php echo $selected == $group_item->id ? 'class="active"' : ''; ?>>
			                                    							<?php echo $group_item->title; ?><?php echo !empty($group_item->children) ? ' <span class="tree_count">('.count($group_item->children).')</span>' : ''; ?>
			                                    						</a>
			                                    					</div><?php
		                                    				if(!empty($group_item->children)) 
		                                    					$list_subgroups($group_item);
		                                    				else
		                                    					?></li><?php
		                                    			}
		                                    		}
	                                    		}
	                                    	?>
	                                        
	                                    </ul>
                                        
                                        <?php if($group): ?>
	                                		<div class="wide"><span class="req">Note: </span> Please select parent group above.</div>
	                                	<?php endif; ?>
                                        
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
