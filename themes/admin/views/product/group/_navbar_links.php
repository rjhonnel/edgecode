<?php if(isset($group)): // Used on "UPDATE" pages?>


    <div class="panel-nav-holder">
        <div class="btn-group">
            <a href="<?php echo \Uri::create('admin/product/group/update/' . $group->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-info-circle"></i> General Information</a>
            <?php if($group->type == 'property'): ?>
                <li><a href="" class="circle_sub_categories" rel="tooltip" title="Sub Groups"></a></li>
                <a href="<?php echo \Uri::create('admin/product/group/subgroups/' . $group->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-lock"></i> <?php echo $group->children ? count($group->children) : ''; ?></a>
            <?php endif; ?>


            <a href="<?php echo \Uri::create('admin/product/group/products_assigned/' . $group->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-credit-card"></i> Products Assigned <?php echo $group->products ? '<label class="label label-primary">' . count($group->products) . '</label>' : ''; ?></a>
            <a href="<?php echo \Uri::create('admin/product/group/products/' . $group->id); ?>" class="btn btn-lg btn-default"><i class="fa fa-credit-card"></i> All Products</a>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function(){
            if($(".btn-group").length > 0)
            {
                $(".btn-group a.active").removeClass('active');
                $(".btn-group a").each(function(){
                    if(($(this).attr("href") == uri_current) ||
                        (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                    {
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>

<?php else: // Used on "CREATE" page ?>
    <?php if(FALSE): ?>
        <div class="second_menu">
            <ul>
                <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
            </ul>
        </div>
    <?php endif; ?>
<?php endif; ?>