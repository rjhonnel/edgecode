            <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Product Groups')); ?>
            
            <?php echo \Theme::instance()->view('views/product/group/_navbar_links', array('group' => $group)); ?>
            
            <!-- Content -->
		    <div class="content_wrapper">
		    	<div class="content_holder">
		    		
		    		<div class="elements_holder">
		    		
                        <div class="row-fluid breadcrumbs_holder">
                            <div class="breadcrumbs_position">
                                <?php 
                                    \Breadcrumb::set('Home', 'admin/dashboard');
                                        \Breadcrumb::set('Catalogue');
                                        \Breadcrumb::set('Manage Product Groups', 'admin/product/group/list');
                                        \Breadcrumb::set('Edit Group');
                                        \Breadcrumb::set($group->title, 'admin/product/group/update/' . $group->id);
                                    \Breadcrumb::set('Sub Groups');

                                    echo \Breadcrumb::create_links();
                                ?>
		                            
                                <?php echo \Theme::instance()->view('views/product/group/_action_links'); ?>
                            </div>
                        </div>
                        
					    <div class="row-fluid">
					    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                            
		                    <!-- Main Content Holder -->
		                    <div class="content">
		                    	
                                <!-- Accordions Panel -->
                                <div class="panel">
                                    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
                                        <div class="panelHeader">
                                            <h4><?php echo ucfirst($group_type); ?> Group Manager</h4>
                                            
                                            <div class="items_per_page_holder">
                                                <label>Show entries:</label>
                                                <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'select_init items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                                            </div>
                                        </div>

                                        <?php
                                            // Load page listing table
                                            echo \Theme::instance()->view('views/product/group/_listing_table', 
                                                array(
                                                    'pagination' 	=> $pagination,
                                                    'items'			=> $items,
                                                ), 
                                                false
                                            ); 
                                        ?>

                                    <?php echo \Form::close(); ?>
                                    
                                    <div class="panelContent p_t_0">
                                        <table class="grid greyTable2 separated" width="100%">
                                            <tbody>
                                                <tr class="adding_tr nodrag nodrop">
                                                    <td colspan="4">
                                                        <?php echo \Form::open(array('action' => \Uri::create('admin/product/group/create/'.$group_type))); ?>
                                                            <table width="100%" class="greyTable2">
                                                                <tr class="nodrag nodrop">
                                                                    <td>
                                                                        <?php echo \Form::hidden('type', $group_type); ?>
                                                                        <?php echo \Form::hidden('parent_id', $group->id); ?>
                                                                        <?php echo \Form::input('title', \Input::post('title'), array('placeholder' => 'Enter Sub Group Title', 'class' => 'right m_r_5')); ?>
                                                                    </td>
                                                                    <td class="noresize"> 
                                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Add New</button>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        <?php echo \Form::close(); ?>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    
                                </div><!-- EOF Accordions Panel -->
		                        
		                    </div><!-- EOF Main Content Holder -->
		                    
                            <!-- Sidebar Holder -->
		                    <div class="sidebar">
		                    	<?php echo \Theme::instance()->view('views/product/group/_tree_links', array('group_type' => $group_type)); ?>
		                    </div><!-- EOF Sidebar Holder -->
                            
		                    <div class="clear"></div>
					    	
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <!-- EOF Content -->
            
            