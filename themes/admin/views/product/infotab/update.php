<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Info Tabs Manager', 'admin/product/infotab/list');
		\Breadcrumb::set($infotab->title);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Info Tab</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/infotab/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/infotab/_navbar_links'); ?>


			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title pull-left">Tab Info</h3>

				</div>
				<div class="panel-body form-horizontal">

					<div class="form-group">
						<label class="col-sm-2 control-label">Info Tab Name   <span class="text-danger">*</span></label>
						<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title', $infotab->title), array('class'=>'form-control')); ?></div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Info Tab Properties</label>
						<div class="col-sm-10">
							<label class="pull_left" style="width: auto; margin-left: 0;">
								<?php echo \Form::checkbox('global', 1, \Input::post('global', $infotab->global), array('style' => 'margin-top: 0;')); ?>
								Assign to all products by default
							</label>

							<label class="pull_left" style="width: auto; margin-left: 10px;">
								<?php echo \Form::radio('type', 'normal', \Input::post('type', $infotab->type), array('style' => 'margin-top: 0;')); ?>
								1. Default
							</label>

							<label class="pull_left" style="width: auto; margin-left: 10px;">
								<?php echo \Form::radio('type', 'hotspot', \Input::post('type', $infotab->type), array('style' => 'margin-top: 0;')); ?>
								2. Hotspot Image
							</label>

							<label class="pull_left" style="width: auto; margin-left: 10px;">
								<?php echo \Form::radio('type', 'table', \Input::post('type', $infotab->type), array('style' => 'margin-top: 0;')); ?>
								3. Tables
							</label>
						</div>
					</div>

				</div>
			</div>

			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
				<?php echo \Form::button('exit', '<i class="fa fa-check"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>
			<?php echo \Form::close(); ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

	            
