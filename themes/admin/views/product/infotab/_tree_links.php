						<?php 
							// Get categories
							$categories = \Product\Model_Category::find(function($query){ 
								$query->order_by('sort', 'asc');
								$query->order_by('id', 'asc');
							});
							$category = isset($category) ? $category : false;
							$link = isset($link) ? $link : 'update';
							$selected = isset($selected) ? $selected : false;
						?>
						
                        <div class="side_tree_holder">
                            <div class="tree_heading">
                                <h4>Product Categories</h4>
                                <div id="sidetreecontrol" class="sidetreecontrol"><a href="#">Collapse All</a><a href="#">Expand All</a></div>
                            </div>
                            <div class="tree_content">
                                <div id="sidetree">
                                
                                	<?php if(!$category && empty($categories)): ?>
                                		<div class="wide"><span class="req">Note: </span> There are no categories yet.</div>
                                	<?php else: ?>
                                	
	                                    <ul class="treeview" id="tree">
	                                    	
	                                    	<?php if($category): ?>
	                                    		<li>
                                                    <div class="radio_link_holder">
                                                        <?php echo \Form::radio('parent_id', 0, \Input::post('parent_id', $category->parent_id)); ?>
                                                        <a href="#" onclick="return false;">ROOT</a>
                                                    </div>
	                                    		</li>
	                                    	<?php endif; ?>
	                                    	
	                                    	<?php
	                                    		if(!empty($categories))
	                                    		{
		                                    		// If category parent_id or id is in this array than dont show radio input
		                                    		$hide_radio = $category ? array($category->id) : array();
		                                    		
		                                    		$list_subcategories = function($category_item) use (&$hide_radio, $link, &$list_subcategories, $category, $selected)
		                                    		{
		                                    			?><ul><?php
		                                    			foreach($category_item->children as $child)
		                                    			{
		                                    				if($category && (in_array($child->id, $hide_radio) || in_array($child->parent_id, $hide_radio))) 
		                                    					array_push($hide_radio, $child->id);
		                                    					
		                                    				?>
		                                    					<li>
		                                    						<?php echo !empty($child->children) ? '<div class="hitarea"></div>' : ''; ?>
		                                    						<div class="radio_link_holder">
			                                    						<?php if($category): ?>
			                                    							<?php echo \Form::radio('parent_id', $child->id, \Input::post('parent_id', $category->parent_id), (in_array($child->id, $hide_radio) ? array('style' => 'display: none;', 'disabled' => 'diabled') : array())); ?>
			                                    						<?php endif; ?>
			                                    						<a href="<?php echo \Uri::create('admin/product/category/' . $link . '/' . $child->id); ?>" <?php echo $selected == $child->id ? 'class="active"' : ''; ?>>
			                                    							<?php echo $child->title; ?><?php echo !empty($child->children) ? ' <span class="tree_count">('.count($child->children).')</span>' : ''; ?>
			                                    						</a>
			                                    					</div><?php
		                                    				if(!empty($child->children)) 
		                                    					$list_subcategories($child);
		                                    				else
		                                    					?></li><?php
		                                    					
		                                    			}
		                                    			?></ul><?php
		                                    		};
		                                    	
		                                    		foreach($categories as $key => $category_item)
		                                    		{
		                                    			// Only root categories in first pass
		                                    			if($category_item->parent_id == 0)
		                                    			{
		                                    				if($category && in_array($category_item->id, $hide_radio)) 
		                                    					array_push($hide_radio, $category_item->id);
		                                    					
			                                    			?>
			                                    				<li>
			                                    					<?php echo !empty($category_item->children) ? '<div class="hitarea"></div>' : ''; ?>
			                                    					<div class="radio_link_holder">
				                                    					<?php if($category): ?>
			                                    							<?php echo \Form::radio('parent_id', $category_item->id, \Input::post('parent_id', $category->parent_id), (in_array($category_item->id, $hide_radio) ? array('style' => 'display: none;', 'disabled' => 'diabled') : array())); ?>
				                                    					<?php endif; ?>
			                                    						<a href="<?php echo \Uri::create('admin/product/category/' . $link . '/' . $category_item->id); ?>" <?php echo $selected == $category_item->id ? 'class="active"' : ''; ?>>
			                                    							<?php echo $category_item->title; ?><?php echo !empty($category_item->children) ? ' <span class="tree_count">('.count($category_item->children).')</span>' : ''; ?>
			                                    						</a>
			                                    					</div><?php
		                                    				if(!empty($category_item->children)) 
		                                    					$list_subcategories($category_item);
		                                    				else
		                                    					?></li><?php
		                                    			}
		                                    		}
	                                    		}
	                                    	?>
	                                        
	                                    </ul>
                                        
                                        <?php if($category): ?>
	                                		<div class="wide"><span class="req">Note: </span> Please select parent category above.</div>
	                                	<?php endif; ?>
                                            
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
