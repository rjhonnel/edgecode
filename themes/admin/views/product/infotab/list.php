<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Info Tabs Manager', 'admin/product/infotab/list');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Info Tabs</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/infotab/_action_links', array('hide_show_all' => 1)); ?>
				</div>
			</header>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>

				<div class="top-filter-holder">

				    <div class="form-inline show-table-filter">
				        <label>Show entries:</label>
				        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
				    </div>
				</div>
			<?php echo \Form::close(); ?>

			<?php
			// Load page listing table
			echo \Theme::instance()->view('views/product/infotab/_listing_table',
				array(
					'pagination' 	=> $pagination,
					'items'			=> $items,
				),
				false
			);
			?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>