<table rel="<?php echo \Uri::create('admin/product/infotab/sort/infotab'); ?>" class="table table-striped table-bordered sortable_rows" width="100%">
	<thead>
	<tr class="blueTableHead">
		<th scope="col">Info Tab Name</th>
		<td scope="col" class="center" width="150">Actions</td>
	</tr>
	</thead>
	<tbody>

	<?php foreach($items as $item): ?>
		<?php $item = (Object)$item; ?>

		<tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
			<td>
				<a href="<?php echo \Uri::create('admin/product/infotab/update/' . $item->id); ?>">
					<strong><?php echo $item->title; ?></strong>
				</a>

			</td>
			<td width="110">
				<ul class="table-action-inline">
					<li>
						<a href="<?php echo \Uri::create('admin/product/infotab/update/' . $item->id); ?>">Edit</a>
					</li>
					<li>
						<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete item?" href="<?php echo \Uri::create('admin/product/infotab/delete/' . $item->id); ?>">Delete</a>
					</li>
				</ul>
			</td>
		</tr>

	<?php endforeach; ?>

	<?php if(empty($items)): ?>

		<tr class="nodrag nodrop">
			<td colspan="4" class="center"><strong>There are no items.</strong></td>
		</tr>

	<?php endif; ?>

	</tbody>
</table>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>

<?php echo \Form::open(array('action' => \Uri::create('admin/product/infotab/create'))); ?>
<div class="panel panel-default panel-body mt-20px">
	<div class="row">
		<div class="col-sm-10">
			<?php echo \Form::input('title', \Input::post('title'), array('placeholder' => 'Enter Info Tab Title', 'class' => 'form-control')); ?>
		</div>
		<div class="col-sm-2">
			<button type="submit" class="btn btn-primary btn-block">Add New</button>
		</div>
	</div>
</div>
<?php echo \Form::close(); ?>