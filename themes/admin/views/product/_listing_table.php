
<?php
    echo \Theme::instance()->view('views/_partials/search_filters', array(
        'pagination' => $pagination,
        'status' => $status,
        'module' => 'product code or',
        'options' => array('status', 'category_id', 'brand_id'),
    ), false);
?>

<div class="listing-actions">
    <div class="pull-left">
        <?php
        echo \Theme::instance()->view('views/_partials/action_header', array('action_name' => 'product'));
        ?>
    </div>

    <div class="form-inline pull-right">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>
<?php echo \Form::button('remove', '<i class="icon-remove icon-white"></i> Remove Selected', array('type' => 'submit', 'class' => 'btn btn-danger left', 'value' => 'remove', 'style' => 'display:none', 'id'=>'bulk_delete')); ?>

<div class="header_messages"></div>
<input type="hidden" name="orderby" value="<?php echo \Input::get('orderby'); ?>">
<input type="hidden" name="order" value="<?php echo \Input::get('order'); ?>">
<table rel="<?php echo \Uri::create('admin/product/sort/product'); ?>" class="table table-striped table-bordered sortable_rows">
    <thead>
    <tr class="blueTableHead">
        <th scope="col" style="width: 40px;"></th>
        <th scope="col">Product Code</th>
        <th scope="col"><a class="sort-table-column" data-column="title" data-order="<?php echo \Input::get('orderby')=='title'?(\Input::get('order') == 'asc'? 'desc': 'asc'):'asc'; ?>" href="#">Product Name <?php echo \Input::get('orderby')=='title'?(\Input::get('order') == 'asc'? '<i class="fa fa-caret-up"></i>': '<i class="fa fa-caret-down"></i>'):''; ?></a></th>
        <th scope="col" class="center">Attribute Group</th>
        <th scope="col" class="center">Status</th>
        <th scope="col" class="center"><a class="sort-table-column" data-column="product_type" data-order="<?php echo \Input::get('orderby')=='product_type'?(\Input::get('order') == 'asc'? 'desc': 'asc'):'asc'; ?>" href="#">Product Type <?php echo \Input::get('orderby')=='product_type'?(\Input::get('order') == 'asc'? '<i class="fa fa-caret-up"></i>': '<i class="fa fa-caret-down"></i>'):''; ?></a></th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>

        <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">

            <td class="noresize">
                <?php
                echo \Form::hidden('action[' . $item->id . ']', 0);
                echo \Form::checkbox('product[' . $item->id . ']', 1, null, array('class' => 'check_active', 'data-value' => 'product', 'data-id' => $item->id));
                ?>
            </td>

            <td>
                <a href="<?php echo \Uri::create('admin/product/update/' . $item->id); ?>">
                    <strong><?php echo $item->code; ?></strong>
                </a>
            </td>

            <td>
                <a href="<?php echo \Uri::create('admin/product/update/' . $item->id); ?>">
                    <strong><?php echo $item->title; ?></strong>
                </a>
            </td>
            <td><?php echo $item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A'; ?></td>
            <td>
                <?php
                // If page is active from certain date
                if($item->status == 2)
                {
                    $dates = array();
                    !is_null($item->active_from) and array_push($dates, date('d/m/Y', $item->active_from));
                    !is_null($item->active_to) and array_push($dates, date('d/m/Y', $item->active_to));

                    if(true)
                    {
                        ?>
                        Active
                        <a class="activeDate" rel="tooltip" title="<?php echo implode(' - ', $dates); ?>">
                            <?php echo \Theme::instance()->asset->img('icon-calendar.png', array('width' => 16, 'height' => 16)); ?>
                        </a>
                        <?php
                    }
                }
                else
                {
                    echo $status[$item->status];
                }
                ?>
            </td>
            <td><?php echo isset(\Config::get('details.product_type')[$item->product_type])?\Config::get('details.product_type')[$item->product_type]:ucfirst($item->product_type); ?></td>
            <td width="110">
                <ul class="table-action-inline">
                    <li>
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete product?" href="<?php echo \Uri::create('admin/product/delete/' . $item->id); ?>">Delete</a>
                    </li>
                </ul>
            </td>
        </tr>

    <?php endforeach; ?>

    <?php if(empty($items)): ?>

        <tr class="nodrag nodrop">
            <td colspan="7" class="center"><strong>There are no items.</strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>

<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>