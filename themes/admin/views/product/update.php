<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Product Manager', 'admin/product/list');
		\Breadcrumb::set($product->title, 'admin/product/update/' . $product->code);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner">

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Product: <?php echo $product->title; ?></h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/_action_links', array('show_save' => 1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/_navbar_links', array('product' => $product)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="row">
				<div class="col-sm-9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">General Information</h3>

						</div>
						<div class="panel-body ">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">Product Name  <span class="text-danger">*</span></label>
									<div class="col-sm-10">
										<?php echo \Form::input('title', \Input::post('title', $product->title),array('class' => 'form-control')); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Product Code</label>
									<div class="col-sm-10">
										<?php echo \Form::input('code', \Input::post('code', $product->code), array('class' => 'form-control')); ?>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Bar Code</label>
									<div class="col-sm-10">
										<?php echo \Form::input('bar_code', \Input::post('bar_code', $product->bar_code), array('class' => 'form-control')); ?>
									</div>
								</div>

								<div class="form-group">

									<label class="col-sm-2 control-label">Minimum Order</label>

									<div class="col-sm-10">

										<?php echo \Form::input('minimum_order', \Input::post('minimum_order', $product->minimum_order), array('class' => 'form-control')); ?>

									</div>

								</div>
							</div>


							<div class="form-group">
								<?php echo \Form::label('Description'); ?>
								<div class="clear"></div>
								<?php echo \Form::textarea('description_full', \Input::post('description_full', $product->description_full), array('class' => 'form-control ck_editor')); ?>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>

						</div>
						<div class="panel-body">
							<div class="header_messages"></div>
							<table rel="<?php echo \Uri::create('admin/product/sort/image/' . $product->id); ?>" class="table table-striped table-bordered sortable ">
								<tr class="nodrop nodrag blueTableHead">
									<th scope="col" class="noresize">Image</th>
									<th scope="col">Image Properties</th>
									<?php if($product->images): ?>
										<?php if(count($product->images) > 1): ?>
											<th>Main Image</th>
											<th>Re-order</th>
										<?php endif; ?>
										<th>Delete</th>
									<?php endif; ?>
								</tr>

								<?php if(is_array($product->images)): ?>
									<?php foreach($product->images as $image): ?>

										<tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
											<td class="currenter noresize">
												<a href="<?php echo \Helper::amazonFileURL('media/images/'. $image->image); ?>" class="preview-image-popup">
													<img src="<?php echo \Helper::amazonFileURL('media/images/medium/'. $image->image); ?>" width="130" height="130" alt="<?php echo $image->alt_text; ?>"/>
												</a>
											</td>
											<td class="upload">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="110">Alt Text</td>
														<td>
															<div class="input_holder">
																<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class' => 'form-control')); ?>
															</div>
														</td>
													</tr>
													<tr>
														<td width="110">Replace Image</td>
														<td class="btn-file">
															<?php echo \Form::file('image_'.$image->id); ?>
															<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
														</td>
													</tr>
												</table>
											</td>
											<?php if(count($product->images) > 1): ?>
												<td width="110" class="text-center">
													<input type="radio" name="cover_image" value="<?php echo $image->id; ?>" <?php echo $image->cover?'checked="checked"':''; ?>>
												</td>
												<td width="110" class="dragHandle">
													<ul class="table-action-inline">
														<li class="dragHandle">
															<a href="" onclick="return false;">Re-order</a>
														</li>
													</ul>
												</td>
											<?php endif; ?>
											<td width="110">
												<ul class="table-action-inline">
													<?php if((!\Config::get('details.image.required', false) && !empty($product->images)) || count($product->images) > 1): ?>
														<li>
															<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/product/delete_image/' . $image->id . '/' . $product->id); ?>">Delete</a>
														</li>
													<?php endif; ?>
												</ul>
											</td>
										</tr>

									<?php endforeach; ?>
								<?php endif; ?>

								<?php if(\Config::get('details.image.multiple', false) || empty($product->images)): ?>
									<tr class="nodrop nodrag">
										<td class="td-thumb">
											<i class="fa fa-picture-o"></i>
										</td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="110">Alt Text</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1'), array('class' => 'form-control')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td width="110">Choose Image</td>
													<td class="btn-file">
														<?php echo \Form::file('image_new_1'); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if(count($product->images) > 1): ?>
											<td class="icon center"></td>
											<td class="icon center"></td>
										<?php endif; ?>
										<?php if((!\Config::get('details.image.required', false) && !empty($product->images)) || count($product->images) > 1): ?>
											<td class="icon center"></td>
										<?php endif; ?>
									</tr>
								<?php endif; ?>

							</table>
						</div>
					</div>

					<div id="pack-section" class="panel panel-default" style="display:none;">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">Pack Options</h3>
						</div>
						<div class="panel-body">
							<?php if($packs = \Input::post()?\Input::post('packs'):$pack_list): ?>
								<?php foreach ($packs as $key => $pack): ?>
									<?php if($key == 1): ?>
										<div id="pack-additional-selection">
									<?php endif; ?>

									<div data-order="<?php echo $key; ?>" class="panel-body table-bordered mb-10px selection-group">
										<?php if($key > 0): ?>
											<div class="row">
												<div class="col-md-12">
												<a href="#" class="pull-right remove-pack"><i class="fa fa-close"></i></a>
												</div>
											</div>
										<?php endif; ?>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Option Name <span class="text-danger">*</span></label>
													<?php echo \Form::input('packs['.$key.'][name]', $pack['name'],array('class' => 'form-control')); ?>
													<input type="hidden" value="<?php echo $pack['id']; ?>" name="packs[<?php echo $key; ?>][pack_id]">
												</div>
												<div class="form-group">
													<label class="control-label">Option Description</label>
													<?php echo \Form::textarea('packs['.$key.'][description]', $pack['description'], array('class' => 'form-control')); ?>
												</div>
												<div class="form-group included" <?php echo $pack['type']=='extras'?'style="display:none"':''; ?>>
													<label class="control-label">Product Selection Limit <span class="text-danger">*</span></label>
													<?php echo \Form::input('packs['.$key.'][selection_limit]', ($pack['selection_limit']?:''),array('class' => 'form-control', 'type' => 'number', 'min' => 1)); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Option Type</label>
													<?php echo \Form::select('packs['.$key.'][type]', $pack['type'], \Config::get('details.pack.type'), array('class' => 'form-control pack_type')); ?>
												</div>
												<div class="form-group btn-file">
													<label class="control-label">Option Image</label>
													<?php echo \Form::input('packs['.$key.'][image]', false ,array('class' => 'form-control', 'type' => 'file')); ?>
												</div>
												<?php if(isset($pack['image'])): ?>
													<?php if($pack['image']): ?>
														<input type="hidden" value="<?php echo $pack['image_id']; ?>" name="packs[<?php echo $key; ?>][pack_image_id]">
														<input type="hidden" value="<?php echo $pack['image']; ?>" name="packs[<?php echo $key; ?>][pack_image_name]">
														<a href="<?php echo \Helper::amazonFileURL('media/images/'. $pack['image']); ?>" class="preview-image-popup">
															<img src="<?php echo \Helper::amazonFileURL('media/images/medium/'. $pack['image']); ?>" height="50" />
														</a>
														<a href="<?php echo \Uri::create('admin/product/delete_pack_image/' . $pack['image_id'] . '/' . $pack['id']); ?>" class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?"><i class="fa fa-close"></i></a>
													<?php endif; ?>
												<?php endif; ?>
											</div>
											<div class="col-md-12">
												<label class="control-label">Products <span class="text-danger">*</span></label>
												<table class="table table-striped table-bordered">
													<thead>
														<tr>
															<td>
																<?php echo \Form::select('pack_product_list', false,  $product_list, array('class' => 'form-control')); ?>
															</td>
															<td>
																<a href="#" class="pull-right add-items-to-selection"><i class="fa fa-plus"></i></a>
															</td>
														</tr>
													</thead>
													<tbody class="item-list-section">
														<?php if(isset($pack['products'])): ?>
															<?php foreach ($pack['products'] as $pack_product): ?>
																<tr data-value="<?php echo $pack_product; ?>">
																	<td>
																		<?php
																			if($get_item = \Product\Model_Product::find_one_by_id($pack_product))
																				echo $get_item->title;
																		?>
																		<input type="hidden" value="<?php echo $pack_product; ?>" name="packs[<?php echo $key; ?>][products][]">
																	</td>
																	<td>
																		<a href="#" class="pull-right remove-items-to-selection"><i class="fa fa-close"></i></a>
																	</td>
																</tr>
															<?php endforeach; ?>
														<?php endif; ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>

									<?php if((count($packs)-1) == $key && $key > 0): ?>
										</div>
									<?php endif; ?>

									<?php if(count($packs) == 1): ?>
										<div id="pack-additional-selection">
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php else: ?>
								<div data-order="0" class="panel-body table-bordered mb-10px selection-group">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Pack Name <span class="text-danger">*</span></label>
												<?php echo \Form::input('packs[0][name]', false,array('class' => 'form-control')); ?>
											</div>
											<div class="form-group">
												<label class="control-label">Pack Description</label>
												<?php echo \Form::textarea('packs[0][description]', false, array('class' => 'form-control')); ?>
											</div>
											<div class="form-group included">
												<label class="control-label">Product Selection Limit <span class="text-danger">*</span></label>
												<?php echo \Form::input('packs[0][selection_limit]', false,array('class' => 'form-control', 'type' => 'number', 'min' => 1)); ?>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Pack Type</label>
												<?php echo \Form::select('packs[0][type]', false, \Config::get('details.pack.type'), array('class' => 'form-control pack_type')); ?>
											</div>
											<div class="form-group btn-file">
												<label class="control-label">Pack Image</label>
												<?php echo \Form::input('packs[0][image]', false ,array('class' => 'form-control', 'type' => 'file')); ?>
											</div>
										</div>
										<div class="col-md-12">
											<label class="control-label">Products <span class="text-danger">*</span></label>
											<table class="table table-striped table-bordered">
												<thead>
													<tr>
														<td>
															<?php echo \Form::select('pack_product_list', false,  $product_list, array('class' => 'form-control')); ?>
														</td>
														<td>
															<a href="#" class="pull-right add-items-to-selection"><i class="fa fa-plus"></i></a>
														</td>
													</tr>
												</thead>
												<tbody class="item-list-section">
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div id="pack-additional-selection">
								</div>
							<?php endif; ?>
							<a id="add-another-pack-selection" href="#" class="btn btn-primary pull-right">Add Another Selection <i class="fa fa-plus"></i></a>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<?php echo \Form::label('Product Type', null, array('class' => 'text_right m_r_15')); ?>
						<?php echo \Form::select('product_type', \Input::post('product_type', $product->product_type), \Config::get('details.product_type'), array('class' => 'form-control')); ?>
					</div>
					<div class="form-group">
						<?php //echo \Form::label('Status', null, array('class' => 'text_right')); ?>
						<?php echo \Form::label('Status', null, array('class' => 'text_right m_r_15')); ?>
						<?php echo \Form::select('status', \Input::post('status', $product->status), array(
							'1' => 'Active',
							'0' => 'Inactive',
							'2' => 'Active in Period',
						), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
					</div>
					<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

						<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
						<div class="datepicker-holder-control">
							<?php echo \Form::input('active_from', \Input::post('active_from', !is_null($product->active_from) ? date('d/m/Y', $product->active_from) : ''), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
						</div>
						<div class="datepicker-holder-control mt-5px">
							<?php echo \Form::input('active_to', \Input::post('active_to', !is_null($product->active_to) ? date('d/m/Y', $product->active_to) : ''), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
						</div>


					</div>

					<div class="form-group">
						<label><?php echo \Form::checkbox('featured', 1, $product->featured); ?> Featured Product</label>
					</div>
					<div class="form-group">
						<label>Supplier</label>
						<?php echo \Form::select('supplier', \Input::post('supplier', $product->supplier), $suppliers, array('class' => 'form-control')); ?>
					</div>
					<?php echo \Theme::instance()->view('views/product/_tree_links', array('product' => $product)); ?>
				</div>
			</div>

			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
				<?php echo \Form::button('exit', 'Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>

			<?php if(\Config::get('details.file.enabled', false)): ?>
			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
			<?php echo \Form::hidden('file_upload', 1); ?>
				<div class="panel panel-default" style="margin-top: 10px;">

					<div class="panel-heading">
						<h3 class="panel-title pull-left">File Manager<?php echo \Config::get('details.file.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>
					</div>
					<div class="panel-body">
						<div class="span5 sort_message_container"></div>
						<div class="header_messages"></div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/product/sort/file/' . $product->id); ?>" class="sortable table table-striped table-bordered">
							<tr class="nodrop nodrag blueTableHead">
								<th scope="col" class="">File</th>
								<th scope="col">File Properties</th>
								<?php if((!\Config::get('details.file.required', false) && !empty($product->files)) || count($product->files) > 1): ?>
									<?php if(count($product->files) > 1): ?>
										<th scope="col" class="center">Re-order</th>
									<?php endif; ?>
									<th scope="col" class="center">Delete</th>
								<?php endif; ?>
							</tr>

							<?php if(is_array($product->files)): ?>
								<?php foreach($product->files as $file): ?>
									<?php
									// Get file extension
									$extension = strtolower(pathinfo($file->file, PATHINFO_EXTENSION));
									?>
									<tr id="sort_<?php echo $file->id . '_' . $file->sort; ?>">
										<td class="td-thumb ">
											<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
												<i class="fa fa-file-<?php echo $extension; ?>-o"></i>
											</a>
										</td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="120"><div class="input_holder">File Name</div></td>
													<td>
														<div class="input_holder"><i>
																<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
																	<?php echo $file->file; ?>
																</a>
															</i></div>
													</td>
												</tr>
												<tr>
													<td width="120">Title</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('title_'.$file->id, \Input::post('title_' . $file->id, $file->title), array('class'=>'form-control')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td width="120">Replace File</td>
													<td class="btn-file">
														<?php echo \Form::file('file_'.$file->id); ?>
														<?php echo \Form::hidden('file_db_'.$file->id, $file->file); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if(count($product->files) > 1): ?>
											<td width="110" class="dragHandle">
												<ul class="table-action-inline">
													<li class="dragHandle">
														<a href="" onclick="return false;">Re-order</a>
													</li>
												</ul>
											</td>
										<?php endif; ?>
										<td width="110">
											<ul class="table-action-inline">
												<li>
													<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete file?" href="<?php echo \Uri::create('admin/product/delete_file/' . $file->id . '/' . $product->id); ?>">
														Delete
													</a>
												</li>
											</ul>
										</td>
									</tr>

								<?php endforeach; ?>
							<?php endif; ?>

							<?php if(\Config::get('details.file.multiple', false) || empty($product->files)): ?>
								<tr class="nodrop nodrag">
									<td class="td-thumb">
										<div class="fa fa-file-text-o"></div>
									</td>
									<td class="upload">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td >Title</td>
												<td>
													<div class="input_holder">
														<?php echo \Form::input('title_new_1', \Input::post('title_new_1'), array('class'=>'form-control')); ?>
													</div>

												</td>
											</tr>
											<tr>
												<td width="120">Choose File</td>
												<td class="btn-file">
													<?php echo \Form::file('file_new_1'); ?>
												</td>
											</tr>
										</table>
									</td>
									<?php if(count($product->files) > 1): ?>
										<td class="icon center"></td>
									<?php endif; ?>
									<?php if(count($product->files)): ?>
										<td class="icon center"></td>
									<?php endif; ?>
								</tr>
							<?php endif; ?>

						</table>
						<p style="padding-left: 15px; font-size: 13px;">
							<small><span class="text-danger">*</span> List of acceptable files types: MS Word, PDF, PowerPoint, MS Access.<br>
							You can rearrange the order of the files by performing drag and drop.</small>
						</p>
					</div>
					<div class="panel-footer text-right">
						<?php echo \Form::button('save', '<i class="fa fa-upload"></i> Upload', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
					</div>
				</div>

				<?php echo \Form::close(); ?>
				<?php endif; ?>


				<?php if(\Config::get('details.video.enabled', false)): ?>
					<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
					<?php echo \Form::hidden('video_upload', 1); ?>
					<div class="panel panel-default">

						<div class="panel-heading">
							<h3 class="panel-title pull-left">Video Manager</h3>
						</div>

						<div class="panel-body">

							<span class="sort_message_container"></span>
							<div class="header_messages"></div>
							<table video="<?php echo \Uri::create('admin/product/video/'); ?>" rel="<?php echo \Uri::create('admin/product/sort/video/' . $product->id); ?>" class="table table-striped table-bordered sortable ">
								<tr class="nodrop nodrag blueTableHead">
									<th scope="col" class="">Video</th>
									<th scope="col">Video Properties</th>
									<?php if((!\Config::get('details.video.required', false) && !empty($product->videos)) || count($product->videos) > 1): ?>
										<?php if(count($product->videos) > 1): ?>
											<th scope="col" class="center">Re-order</th>
										<?php endif; ?>
										<th scope="col" class="center">Delete</th>
									<?php endif; ?>
								</tr>

								<?php if(is_array($product->videos)): ?>
									<?php foreach($product->videos as $video): ?>
										<?php
										$youtube = \App\Youtube::forge();
										$video->details = $youtube->parse($video->url)->get();
										?>
										<tr id="sort_<?php echo $video->id . '_' . $video->sort; ?>">
											<td class="center video_thumbnail" width="100">
												<?php if($video->thumbnail): ?>
													<img src="<?php echo \Helper::amazonFileURL('media/videos/' . $video->thumbnail); ?>" class="default" width="80"/>
													<div style="margin-top: 5px; position: relative;">
														<?php echo \Form::checkbox('video_delete_image_' . $video->id, 1, \Input::post('video_delete_image_' . $video->id), array('class' => 'video_delete_image', 'style' => 'margin: 0;')); ?>
														<span style="font-size: 9px;">Use YouTube?</span>
													</div>
												<?php else: ?>
													<img src="<?php echo $video->details['thumbnail']['small']; ?>" class="default" width="80"/>
												<?php endif; ?>
											</td>

											<td class="upload">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="120">Video URL</td>
														<td>
															<div class="input_holder">
																<?php echo \Form::input('video_url_'.$video->id, \Input::post('video_url_' . $video->id, $video->url), array('class' => 'video_url form-control')); ?>
															</div>
														</td>
														<td valign="top">
															<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
														</td>
													</tr>
													<tr>
														<td width="120">Video Title</td>
														<td colspan="2">
															<div class="input_holder">
																<?php echo \Form::input('video_title_'.$video->id, \Input::post('video_title_' . $video->id, $video->title), array('class' => 'video_title form-control')); ?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="">Thumbnail Image</td>
														<td class="btn-file">
															<?php echo \Form::file('video_file_'.$video->id); ?>
															<?php if($video->thumbnail) : ?>
																<?php echo \Form::hidden('video_file_db_'.$video->id, $video->thumbnail); ?>
															<?php endif; ?>
														</td>
													</tr>
												</table>
											</td>
											<?php if(count($product->videos) > 1): ?>
												<td width="110" class="dragHandle">
													<ul class="table-action-inline">
														<li class="dragHandle">
															<a href="" onclick="return false;">Re-order</a>
														</li>
													</ul>
												</td>
											<?php endif; ?>
											<td width="110">
												<ul class="table-action-inline">
													<?php if((!\Config::get('details.video.required', false) && !empty($product->videos)) || count($product->videos) > 1): ?>
														<li>
															<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete video?" href="<?php echo \Uri::create('admin/product/delete_video/' . $video->id . '/' . $product->id); ?>">
																Delete
															</a>
														</li>
													<?php endif; ?>
												</ul>
											</td>
										</tr>

									<?php endforeach; ?>
								<?php endif; ?>

								<?php if(\Config::get('details.video.multiple', false) || empty($product->videos)): ?>
									<tr class="nodrop nodrag">
										<td width="100" class="td-thumb"><i class="fa fa-youtube fa-lg"></i></td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="100">Video URL</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('video_url_new_1', \Input::post('video_url_new_1'), array('class' => 'form-control video_url')); ?>
														</div>
													</td>
													<td valign="top">
														<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
													</td>
												</tr>
												<tr>
													<td width="120">Video Title</td>
													<td colspan="2">
														<div class="input_holder">
															<?php echo \Form::input('video_title_new_1', \Input::post('video_title_new_1'), array('class' => 'form-control video_title')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td class="">Thumbnail Image</td>
													<td class="btn-file">
														<?php echo \Form::file('video_file_new_1'); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if((!\Config::get('details.video.required', false) && !empty($product->videos)) || count($product->videos) > 1): ?>
											<?php if(count($product->videos) > 1): ?>
												<td class="icon center"></td>
											<?php endif; ?>
											<td class="icon center"></td>
										<?php endif; ?>
									</tr>
								<?php endif; ?>

							</table>
							<p style="padding-left: 15px; font-size: 13px;">
								<small>
								 	<span class="text-danger">*</span> Insert a Youtube or Vimeo URL link
								</small>
							</p>
						</div>

						<div class="panel-footer text-right">
							<?php echo \Form::button('save', '<i class="fa fa-upload"></i> Upload', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
						</div>
					</div>

					<?php \Theme::instance()->asset->js('product/videos.js', array(), 'basic'); ?>


					<?php echo \Form::close(); ?>
				<?php endif; ?>

		</div>
	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<div id="product-template" class="hide">
	<?php echo \Form::select('pack_product_list', false,  $product_list); ?>
</div>

<?php
	echo ckeditor_replace('ck_editor');
	echo \Theme::instance()->asset->css('plugins/select2-4.0.3.min.css');
	echo \Theme::instance()->asset->js('plugins/select2-4.0.3.min.js');
	echo \Theme::instance()->asset->js('product/product.js');
	echo \Theme::instance()->asset->js('product/videos.js');
?>