<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Product Manager', 'admin/product/list');
		\Breadcrumb::set('Add New');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner">

			<header class="main-content-heading">
				<h4 class="pull-left">Add Product</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/_action_links', array('show_save' => 1, 'show_add_new' => 1, 'hide_add_new' => 1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/_navbar_links'); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="row">
				<div class="col-sm-9">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">General Information</h3>
						</div>
						<div class="panel-body">

							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">Product Name  <span class="text-danger">*</span></label>
									<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title'),array('class' => 'form-control')); ?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Product Code</label>
									<div class="col-sm-10"><?php echo \Form::input('code', \Input::post('code'),array('class' => 'form-control')); ?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Bar Code</label>
									<div class="col-sm-10"><?php echo \Form::input('bar_code', \Input::post('bar_code'),array('class' => 'form-control')); ?></div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Minimum Order</label>
									<div class="col-sm-10"><?php echo \Form::input('minimum_order', \Input::post('minimum_order'),array('class' => 'form-control')); ?></div>
								</div>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Description'); ?>
								<div class="clear"></div>
								<?php echo \Form::textarea('description_full', \Input::post('description_full'), array('class' => 'ck_editor')); ?>
							</div>
						</div>
					</div>




					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>

						</div>
						<div class="panel-body">
							<table class="table table-striped table-bordered">
								<tr class="nodrop nodrag blueTableHead">
									<th scope="col">Image</th>
									<th scope="col">Image Properties</th>
								</tr>
								<tr>
									<td class="td-thumb">
										<i class="fa fa-picture-o"></i>
									</td>
									<td class="upload">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="110">Alt Text</td>
												<td>
													<div class="input_holder">
														<?php echo \Form::input('alt_text', \Input::post('alt_text'), array('class' => 'form-control')); ?>
													</div>
												</td>
											</tr>
											<tr>
												<td width="110">Replace Image</td>
												<td class="btn-file">
													<?php echo \Form::file('image'); ?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</div>

					<div id="pack-section" class="panel panel-default" style="display:none;">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">Pack Options</h3>
						</div>
						<div class="panel-body">
							<?php if($packs = \Input::post('packs')): ?>
								<?php foreach ($packs as $key => $pack): ?>
									<?php if($key == 1): ?>
										<div id="pack-additional-selection">
									<?php endif; ?>

									<div data-order="<?php echo $key; ?>" class="panel-body table-bordered mb-10px selection-group">
										<?php if($key > 0): ?>
											<div class="row">
												<div class="col-md-12">
												<a href="#" class="pull-right remove-pack"><i class="fa fa-close"></i></a>
												</div>
											</div>
										<?php endif; ?>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Option Name <span class="text-danger">*</span></label>
													<?php echo \Form::input('packs['.$key.'][name]', $pack['name'],array('class' => 'form-control')); ?>
												</div>
												<div class="form-group">
													<label class="control-label">Option Description</label>
													<?php echo \Form::textarea('packs['.$key.'][description]', $pack['description'], array('class' => 'form-control')); ?>
												</div>
												<div class="form-group included" <?php echo $pack['type']=='extras'?'style="display:none"':''; ?>>
													<label class="control-label">Product Selection Limit <span class="text-danger">*</span></label>
													<?php echo \Form::input('packs['.$key.'][selection_limit]', $pack['selection_limit'],array('class' => 'form-control', 'type' => 'number', 'min' => 1)); ?>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label">Option Type</label>
													<?php echo \Form::select('packs['.$key.'][type]', $pack['type'], \Config::get('details.pack.type'), array('class' => 'form-control pack_type')); ?>
												</div>
												<div class="form-group btn-file">
													<label class="control-label">Option Image</label>
													<?php echo \Form::input('packs['.$key.'][image]', false ,array('class' => 'form-control', 'type' => 'file')); ?>
												</div>
											</div>
											<div class="col-md-12">
												<label class="control-label">Products <span class="text-danger">*</span></label>
												<table class="table table-striped table-bordered">
													<thead>
														<tr>
															<td>
																<?php echo \Form::select('pack_product_list', false,  $product_list, array('class' => 'form-control')); ?>
															</td>
															<td>
																<a href="#" class="pull-right add-items-to-selection"><i class="fa fa-plus"></i></a>
															</td>
														</tr>
													</thead>
													<tbody class="item-list-section">
														<?php if(isset($pack['products'])): ?>
															<?php foreach ($pack['products'] as $pack_product): ?>
																<tr data-value="<?php echo $pack_product; ?>">
																	<td>
																		<?php
																			if($get_item = \Product\Model_Product::find_one_by_id($pack_product))
																				echo $get_item->title;
																		?>
																		<input type="hidden" value="<?php echo $pack_product; ?>" name="packs[<?php echo $key; ?>][products][]">
																	</td>
																	<td>
																		<a href="#" class="pull-right remove-items-to-selection"><i class="fa fa-close"></i></a>
																	</td>
																</tr>
															<?php endforeach; ?>
														<?php endif; ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>

									<?php if((count($packs)-1) == $key && $key > 0): ?>
										</div>
									<?php endif; ?>

									<?php if(count($packs) == 1): ?>
										<div id="pack-additional-selection">
										</div>
									<?php endif; ?>
								<?php endforeach; ?>
							<?php else: ?>
								<div data-order="0" class="panel-body table-bordered mb-10px selection-group">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Option Name <span class="text-danger">*</span></label>
												<?php echo \Form::input('packs[0][name]', false,array('class' => 'form-control')); ?>
											</div>
											<div class="form-group">
												<label class="control-label">Option Description</label>
												<?php echo \Form::textarea('packs[0][description]', false, array('class' => 'form-control')); ?>
											</div>
											<div class="form-group included">
												<label class="control-label">Product Selection Limit <span class="text-danger">*</span></label>
												<?php echo \Form::input('packs[0][selection_limit]', false,array('class' => 'form-control', 'type' => 'number', 'min' => 1)); ?>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label class="control-label">Option Type</label>
												<?php echo \Form::select('packs[0][type]', false, \Config::get('details.pack.type'), array('class' => 'form-control pack_type')); ?>
											</div>
											<div class="form-group btn-file">
												<label class="control-label">Option Image</label>
												<?php echo \Form::input('packs[0][image]', false ,array('class' => 'form-control', 'type' => 'file')); ?>
											</div>
										</div>
										<div class="col-md-12">
											<label class="control-label">Products <span class="text-danger">*</span></label>
											<table class="table table-striped table-bordered">
												<thead>
													<tr>
														<td>
															<?php echo \Form::select('pack_product_list', false,  $product_list, array('class' => 'form-control')); ?>
														</td>
														<td>
															<a href="#" class="pull-right add-items-to-selection"><i class="fa fa-plus"></i></a>
														</td>
													</tr>
												</thead>
												<tbody class="item-list-section">
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div id="pack-additional-selection">
								</div>
							<?php endif; ?>
							<a id="add-another-pack-selection" href="#" class="btn btn-primary pull-right">Add Another Selection <i class="fa fa-plus"></i></a>
						</div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						<?php echo \Form::label('Product Type', null, array('class' => 'text_right m_r_15')); ?>
						<?php echo \Form::select('product_type', \Input::post('product_type'), \Config::get('details.product_type'), array('class' => 'form-control')); ?>
					</div>
					<div class="form-group">
						<?php echo \Form::label('Status', null, array('class' => 'text_right m_r_15')); ?>
						<?php echo \Form::select('status', \Input::post('status', '1'), array(
							'1' => 'Active',
							'0' => 'Inactive',
							'2' => 'Active in Period',
						), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
					</div>

					<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

						<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
						<div class="datepicker-holder-control">
							<?php echo \Form::input('active_from', \Input::post('active_from'), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
						</div>
						<div class="datepicker-holder-control mt-5px">
							<?php echo \Form::input('active_to', \Input::post('active_to'), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
						</div>
					</div>
					<div class="form-group">
						<label><?php echo \Form::checkbox('featured', 1, \Input::post('featured', false)); ?> Featured Product</label>
					</div>
					<div class="form-group">
						<label>Supplier</label>
						<?php echo \Form::select('supplier', \Input::post('supplier', 0), $suppliers, array('class' => 'form-control')); ?>
					</div>



					<?php
					$product = new stdClass();
					$product->id 			= 0;
					$product->categories 	= array();
					$product->groups        = array();
					$product->pricing_group = array();
					?>
					<?php echo \Theme::instance()->view('views/product/_tree_links', array('product' => $product, 'selected_items' => \Uri::segment(5) ? array(\Uri::segment(5)) : false)); ?>
				</div>
			</div>

			<div class="save_button_holder text-right">
				<?php echo \Form::button('update', '<i class="fa fa-edit"></i> Save & Continue', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
				<?php echo \Form::button('save', '<i class="fa fa-check"></i> Save & Add New', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>
		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

<div id="product-template" class="hide">
	<?php echo \Form::select('pack_product_list', false,  $product_list); ?>
</div>

<?php
	echo ckeditor_replace('ck_editor');
	echo \Theme::instance()->asset->css('plugins/select2-4.0.3.min.css');
	echo \Theme::instance()->asset->js('plugins/select2-4.0.3.min.js');
	echo \Theme::instance()->asset->js('product/product.js');
?>