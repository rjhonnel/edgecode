<div class="action-list">
	<?php if( isset($set_save_price) && $set_save_price == 1 ): ?>
		<a id="save_button_up_price" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Save</a>
	<?php endif; ?>
	<?php if( isset($set_save_seo) && $set_save_seo == 1 ): ?>
		<a id="save_button_up_seo" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Save</a>
	<?php endif; ?>
	<?php if( isset($set_save_specials) && $set_save_specials == 1 ): ?>
		<a id="save_button_up_specials" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Save</a>
	<?php endif; ?>

	<?php if(isset($show_save)): ?>
		<a id="save_button_up" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Save</a>
	<?php endif; ?>
	<?php if(!isset($hide_add_new)): ?>
		<a href="<?php echo \Uri::create('admin/product/create'); ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> New Product</a>
	<?php endif; ?>

	<?php if(isset($show_bulk_add)): ?>
        <a href="<?php echo \Uri::create('admin/product/import'); ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Bulk Add</a>
    <?php endif; ?>

	<?php if(!isset($hide_show_all)): ?>
		<a href="<?php echo \Uri::create('admin/product/list'); ?>" class="btn btn-default btn-sm"><i class="fa fa-list"></i> Show All</a>
	<?php endif; ?>
</div>