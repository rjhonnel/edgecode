<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Category Manager', 'admin/product/category/list');
		\Breadcrumb::set('Add New');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Add Category</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/category/_action_links', array('parent_id' => \Uri::segment(5), 'create_form'=>1, 'hide_add_new' => 1 )); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/category/_navbar_links'); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

			<div class="row">
				<div class="col-sm-9">

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title pull-left">General Information</h3>

						</div>
						<div class="panel-body">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">Category Name  <span class="text-danger">*</span></label>
									<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title'), array('class' => 'form-control')); ?></div>
								</div>

								<div class="form-group">
									<label class="col-sm-2 control-label">Small Description</label>
									<div class="col-sm-10"><?php echo \Form::textarea('description_intro', \Input::post('description_intro'), array('class' => 'form-control')); ?></div>
								</div>
							</div>

							<div class="form-group">
								<?php echo \Form::label('Description'); ?>
								<?php echo \Form::textarea('description_full', \Input::post('description_full'), array('class' => 'form-control ck_editor')); ?>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Images</h3>
						</div>
						<div class="panel-body">

							<table class="table table-striped table-bordered">
								<tr class="nodrop nodrag blueTableHead">
									<th scope="col" class="noresize">Image</th>
									<th scope="col">Image Properties</th>
								</tr>
								<tr>
									<td class="td-thumb">
										<i class="fa fa-picture-o"></i>
									</td>
									<td class="upload">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td class="noresize">Alt Text</td>
												<td>
													<div class="input_holder">
														<?php echo \Form::input('alt_text', \Input::post('alt_text'), array('class' => 'form-control')); ?>
													</div>
												</td>
											</tr>
											<tr>
												<td class="noresize">Replace Image</td>
												<td>
													<?php echo \Form::file('image'); ?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</div>
					</div>


				</div>
				<div class="col-sm-3">

					<div class="form-group">
						<?php echo \Form::label('Status', null, array('class' => 'text_right m_r_15')); ?>
						<?php echo \Form::select('status', \Input::post('status', '1'), array(
							'1' => 'Active',
							'0' => 'Inactive',
							'2' => 'Active in Period',
						), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>


					</div>

					<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

						<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
						<div class="datepicker-holder-control">
							<?php echo \Form::input('active_from', \Input::post('active_from'), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
						</div>
						<div class="datepicker-holder-control mt-5px">
							<?php echo \Form::input('active_to', \Input::post('active_to'), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
						</div>
					</div>


					<?php
					$category = new stdClass();
					$category->id 			= 0;
					$category->parent_id 	= 0;
					?>
					<?php echo \Theme::instance()->view('views/product/category/_tree_links', array('category' => $category, 'selected' => \Uri::segment(5))); ?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>Customer Group</h4>
						</div>
						<div class="panel-body">
							<ul class="checkradio-block">
								<?php for($x=0;$x<sizeof($groups);$x++):?>
									<li>
										<label class="control-label"><?php echo \Form::checkbox('user_group['.$x.']', $groups[$x]['id'], \Input::post('user_group['.$x.']', '')); ?> <?php echo $groups[$x]['name'];?></label>
									</li>
								<?php endfor;?>
							</ul>
						</div>
						<div class="panel-footer"><small><span class="req">Note: </span> Category exclusive to customer group.</small></div>
					</div>
				</div>
			</div>


			<div class="save_button_holder text-right">
				<?php echo \Form::button('update', '<i class="fa fa-edit"></i>Save & Continue', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
				<?php echo \Form::button('save', 'Save & Add New', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>


<?php echo ckeditor_replace('ck_editor'); ?>
      	

                            
                            