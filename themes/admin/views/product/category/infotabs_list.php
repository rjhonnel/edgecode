<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Category Manager', 'admin/product/category/list');
		\Breadcrumb::set($category->title, 'admin/product/category/update/' . $category->id);
		\Breadcrumb::set('Info Tabs');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Category: Info Tabs</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/category/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/category/_navbar_links', array('category' => $category)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>

			<div class="sort_message_container"></div>

			<div class="panel panel-default panel-col2">
				<div class="panel-heading">

					<div class="row">
						<div class="col-sm-6">
							<h4 class="panel-title pull-left mt-5px">All Info Tabs</h4>
							<button name="add" value="add" type="submit" class="btn btn-small btn-primary pull-right">
								<i class="fa fa-plus"></i> Add selected
							</button>
						</div>

						<div class="col-sm-6">
							<h4 class="panel-title pull-left mt-5px">Info Tabs Assigned to Category</h4>
							<button name="remove" value="remove" type="submit" class="btn btn-small btn-primary pull-right">
								<i class="fa fa-minus"></i> Remove selected
							</button>
						</div>
					</div>

				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-6">
							<table class="table table-striped table-bordered">
								<thead>
								<tr class="blueTableHead">
									<th width="30" scope="col"><?php echo \Form::checkbox('select_all', 'infotabs[add][]'); ?></th>
									<th scope="col">Info Tab Name</th>
								</tr>
								</thead>
								<tbody class="drag_table remove">
								<?php if(!empty($category->not_related_infotabs)): ?>
									<?php foreach($category->not_related_infotabs as $infotab): ?>

										<tr>
											<td class="noresize"><?php echo \Form::checkbox('infotabs[add][]', $infotab->id); ?></td>
											<td>
												<a rel="tooltip" data-original-title="Edit" data-placement="right" href="<?php echo \Uri::create('admin/product/category/infotab/update/' . $infotab->id); ?>">
													<strong><?php echo $infotab->title; ?></strong>
												</a>
											</td>
										</tr>

									<?php endforeach; ?>
									<?php $no_items = 'style="display: none;"'; ?>
								<?php else: ?>
									<?php $no_items = ''; ?>
								<?php endif; ?>

								<tr class="no_items" <?php echo $no_items; ?>>
									<td class="noresize center" colspan="3">There are no more info tabs to add</td>
								</tr>

								</tbody>
							</table>
						</div>

						<div class="col-sm-6">
							<table class="table table-striped table-bordered">
								<thead>
								<tr class="blueTableHead">
									<th width="30" scope="col"><?php echo \Form::checkbox('select_all', 'infotabs[remove][]'); ?></th>
									<th scope="col">Info Tab Name</th>
								</tr>
								</thead>
								<tbody class="drag_table add">
								<?php if(!empty($category->infotabs)): ?>
									<?php foreach($category->infotabs as $infotab): ?>

										<tr>
											<td class="noresize"><?php echo \Form::checkbox('infotabs[remove][]', $infotab->unique_id); ?></td>
											<td>
												<a rel="tooltip" data-original-title="Edit" data-placement="right" href="<?php echo \Uri::create('admin/product/category/infotab_edit/' . $category->id . '/' . $infotab->unique_id); ?>">
													<strong><?php echo $infotab->title; ?></strong>
												</a>
											</td>
										</tr>

									<?php endforeach; ?>
									<?php $no_items = 'style="display: none;"'; ?>
								<?php else: ?>
									<?php $no_items = ''; ?>
								<?php endif; ?>

								<tr class="no_items" <?php echo $no_items; ?>>
									<td class="noresize center" colspan="3">There are no added info tabs</td>
								</tr>

								</tbody>
							</table>
						</div>
					</div>


				</div>
			</div>

			<?php echo \Form::close(); ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>