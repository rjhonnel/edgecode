<div class="action-list">
<?php if( isset($set_save_seo) && $set_save_seo == 1 ): ?>
	<a id="save_button_up_seo" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Save</a>
<?php endif; ?>
<?php if((isset($parent_id) && $parent_id !== false && is_numeric($parent_id)) || (isset($create_form) && $create_form==1)): ?>
	<a id="save_button_up" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Save</a>
	<?php if(!isset($hide_add_new)): ?>
		<a href="<?php echo \Uri::create('admin/product/category/create/' . $parent_id); ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New</a>
	<?php endif; ?>
<?php else: ?>
	<?php if(!isset($hide_add_new)): ?>
    	<a href="<?php echo \Uri::create('admin/product/category/create'); ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New</a>
	<?php endif; ?>
<?php endif; ?>
<?php if(!isset($hide_show_all)): ?>
	<a href="<?php echo \Uri::create('admin/product/category/list'); ?>" class="btn btn-sm btn-default"><i class="fa fa-list"></i> Show All</a>
<?php endif; ?>
</div>