<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Category Manager', 'admin/product/category/list');
		\Breadcrumb::set($category->title, 'admin/product/category/update/' . $category->id);

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit <?php echo $category->parent_id > 0? 'Sub ': ''; ?>Category: General Information</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/category/_action_links', array('parent_id' => $category->id)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/category/_navbar_links', array('category' => $category)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>

				<div class="row">
					<div class="col-sm-9">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title pull-left">General Information</h3>

							</div>
							<div class="panel-body">
								<div class="form-horizontal">
									<div class="form-group">
										<label class="col-sm-2 control-label">Category Name  <span class="text-danger">*</span></label>
										<?php echo \Form::label(''); ?>
										<div class="col-sm-10"><?php echo \Form::input('title', \Input::post('title', $category->title), array('class' => 'form-control')); ?></div>
									</div>

									<div class="form-group">
										<label class="col-sm-2 control-label">Small Description</label>
										<div class="col-sm-10"><?php echo \Form::textarea('description_intro', \Input::post('description_intro', $category->description_intro), array('class' => 'form-control')); ?></div>
									</div>
								</div>

								<div class="form-group">
									<?php echo \Form::label('Description' ); ?>
									<div class="clear"></div>
									<?php echo \Form::textarea('description_full', \Input::post('description_full', $category->description_full), array('class' => 'form-control ck_editor')); ?>
								</div>
							</div>
						</div>


						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">Images <?php echo \Config::get('details.image.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h4>
							</div>
							<div class="panel-body">

								<div class="header_messages"></div>
								<table rel="<?php echo \Uri::create('admin/product/category/sort/image'); ?>" class="table table-striped table-bordered sortable">
									<tr class="nodrop nodrag blueTableHead">
										<th scope="col" class="noresize">Image</th>
										<th scope="col">Image Properties</th>
										<?php if(count($category->images) > 1): ?>
											<th scope="col" class="center">Main Image</th>
											<th scope="col" class="center">Re-order</th>
										<?php endif; ?>
										<?php if((!\Config::get('details.image.required', false) && !empty($category->images)) || count($category->images) > 1): ?>
											<th scope="col" class="center">Delete</th>
										<?php endif; ?>
									</tr>

									<?php if(is_array($category->images)): ?>
										<?php foreach($category->images as $image): ?>

											<tr id="sort_<?php echo $image->id . '_' . $image->sort; ?>">
												<td class="center noresize">
													<a href="<?php echo \Helper::amazonFileURL('media/images/' . $image->image); ?>" class="preview-image-popup">
														<img src="<?php echo \Helper::amazonFileURL('media/images/' . key(\Config::get('details.image.resize', array('' => ''))) . $image->image); ?>" width="130" height="130" alt="<?php echo $image->alt_text; ?>"/>
													</a>
												</td>
												<td class="upload btn-file">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="110">Alt Text</td>
															<td>
																<div class="input_holder">
																	<?php echo \Form::input('alt_text_'.$image->id, \Input::post('alt_text_'.$image->id, $image->alt_text), array('class' => 'form-control')); ?>
																</div>
															</td>
														</tr>
														<tr>
															<td>Replace Image</td>
															<td>
																<?php echo \Form::file('image_'.$image->id); ?>
																<?php echo \Form::hidden('image_db_'.$image->id, $image->image); ?>
															</td>
														</tr>
													</table>
												</td>

												<?php if(count($category->images) > 1): ?>
													<td width="110" class="text-center">
														<input type="radio" name="cover_image" value="<?php echo $image->id; ?>" <?php echo $image->cover?'checked="checked"':''; ?>>
													</td>
													<td width="110" class="dragHandle">
														<ul class="table-action-inline">
															<li class="dragHandle">
																<a href="" onclick="return false;">Re-order</a>
															</li>
														</ul>
													</td>
												<?php endif; ?>
												<?php if((!\Config::get('details.image.required', false) && !empty($category->images)) || count($category->images) > 1): ?>
													<td width="110">
														<ul class="table-action-inline">
															<li>
																<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete image?" href="<?php echo \Uri::create('admin/product/category/delete_image/' . $image->id . '/' . $category->id); ?>">
																	Delete
																</a>
															</li>
														</ul>
													</td>
												<?php endif; ?>
											</tr>

										<?php endforeach; ?>
									<?php endif; ?>

									<?php if(\Config::get('details.image.multiple', false) || empty($category->images)): ?>
										<tr class="nodrop nodrag">
											<td class="td-thumb">
												<i class="fa fa-picture-o"></i>
											</td>
											<td class="upload">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="110">Alt Text</td>
														<td>
															<div class="input_holder">
																<?php echo \Form::input('alt_text_new_1', \Input::post('alt_text_new_1'), array('class' => 'form-control')); ?>
															</div>
														</td>
													</tr>
													<tr>
														<td>Choose Image</td>
														<td>
															<?php echo \Form::file('image_new_1'); ?>
														</td>
													</tr>
												</table>
											</td>
											<?php if(count($category->images) > 1): ?>
												<td class="icon center"></td>
												<td class="icon center"></td>
											<?php endif; ?>
											<?php if((!\Config::get('details.image.required', false) && !empty($category->images)) || count($category->images) > 1): ?>
												<td class="icon center"></td>
											<?php endif; ?>
										</tr>
									<?php endif; ?>

								</table>

							</div>
						</div>


					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<?php echo \Form::label('Status', null, array('class' => 'text_right m_r_15')); ?>
							<?php echo \Form::select('status', \Input::post('status', $category->status), array(
								'1' => 'Active',
								'0' => 'Inactive',
								'2' => 'Active in Period',
							), array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>


						</div>

						<div class="form-group toggable_dates date_content" <?php echo \Input::post('status') != '2' ? 'style="display: none;"' : ''; ?>>

							<?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
							<div class="datepicker-holder-control">
								<?php echo \Form::input('active_from', \Input::post('active_from', !is_null($category->active_from) ? date('d/m/Y', $category->active_from) : ''), array('id' => 'from', 'class' => 'form-control dateInput', 'placeholder' => 'From')); ?>
							</div>
							<div class="datepicker-holder-control mt-5px">
								<?php echo \Form::input('active_to', \Input::post('active_to', !is_null($category->active_to) ? date('d/m/Y', $category->active_to) : ''), array('id' => 'to', 'class' => 'form-control dateInput', 'placeholder' => 'To')); ?>
							</div>


						</div>

						<?php echo \Theme::instance()->view('views/product/category/_tree_links', array('category' => $category)); ?>

						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title pull-left">Customer Group</h4>
							</div>

							<div class="panel-body">

								<ul class="checkradio-block">
									<?php for($x=0;$x<sizeof($groups);$x++):?>
										<?php $val = isset(json_decode($category->user_group)->$x)? json_decode($category->user_group)->$x: '';?>
										<li>
											<label class="control-label"><?php echo \Form::checkbox('user_group['.$x.']', $groups[$x]['id'], \Input::post('user_group['.$x.']', $val)); ?> <?php echo $groups[$x]['name'];?></label>
										</li>
									<?php endfor;?>
								</ul>
							</div>

							<div class="panel-footer"><small><span class="req">Note: </span> Category exclusive to customer group.</small></div>

						</div>
					</div>
				</div>



			<div class="save_button_holder text-right">
				<?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'id'=> 'save_button_down', 'class' => 'btn btn-success', 'value' => '1')); ?>
				<?php echo \Form::button('exit', '<i class="fa fa-check"></i> Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
			</div>

			<?php echo \Form::close(); ?>


			<?php if(\Config::get('details.file.enabled', false)): ?>
			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
			<?php echo \Form::hidden('file_upload', 1); ?>
				<div class="panel panel-default" style="margin-top: 10px;">

					<div class="panel-heading">
						<h3 class="panel-title pull-left">File Manager<?php echo \Config::get('details.file.required', false) ? '<span class="star-required">*</span> ' : ''; ?></h3>
					</div>
					<div class="panel-body">
						<div class="span5 sort_message_container"></div>
						<div class="header_messages"></div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0" rel="<?php echo \Uri::create('admin/product/category/sort/file/' . $category->id); ?>" class="sortable table table-striped table-bordered">
							<tr class="nodrop nodrag blueTableHead">
								<th scope="col" class="">File</th>
								<th scope="col">File Properties</th>
								<?php if((!\Config::get('details.file.required', false) && !empty($category->files)) || count($category->files) > 1): ?>
									<?php if(count($category->files) > 1): ?>
										<th scope="col" class="center">Re-order</th>
									<?php endif; ?>
									<th scope="col" class="center">Delete</th>
								<?php endif; ?>
							</tr>

							<?php if(is_array($category->files)): ?>
								<?php foreach($category->files as $file): ?>
									<?php
									// Get file extension
									$extension = strtolower(pathinfo($file->file, PATHINFO_EXTENSION));
									?>
									<tr id="sort_<?php echo $file->id . '_' . $file->sort; ?>">
										<td class="td-thumb ">
											<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
												<i class="fa fa-file-<?php echo $extension; ?>-o"></i>
											</a>
										</td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="120"><div class="input_holder">File Name</div></td>
													<td>
														<div class="input_holder"><i>
																<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
																	<?php echo $file->file; ?>
																</a>
															</i></div>
													</td>
												</tr>
												<tr>
													<td width="120">Title</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('title_'.$file->id, \Input::post('title_' . $file->id, $file->title), array('class'=>'form-control')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td width="120">Replace File</td>
													<td class="btn-file">
														<?php echo \Form::file('file_'.$file->id); ?>
														<?php echo \Form::hidden('file_db_'.$file->id, $file->file); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if(count($category->files) > 1): ?>
											<td width="110" class="dragHandle">
												<ul class="table-action-inline">
													<li class="dragHandle">
														<a href="" onclick="return false;">Re-order</a>
													</li>
												</ul>
											</td>
										<?php endif; ?>
										<td width="110">
											<ul class="table-action-inline">
												<li>
													<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete file?" href="<?php echo \Uri::create('admin/product/category/delete_file/' . $file->id . '/' . $category->id); ?>">
														Delete
													</a>
												</li>
											</ul>
										</td>
									</tr>

								<?php endforeach; ?>
							<?php endif; ?>

							<?php if(\Config::get('details.file.multiple', false) || empty($category->files)): ?>
								<tr class="nodrop nodrag">
									<td class="td-thumb">
										<div class="fa fa-file-text-o"></div>
									</td>
									<td class="upload">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td >Title</td>
												<td>
													<div class="input_holder">
														<?php echo \Form::input('title_new_1', \Input::post('title_new_1'), array('class'=>'form-control')); ?>
													</div>

												</td>
											</tr>
											<tr>
												<td width="120">Choose File</td>
												<td class="btn-file">
													<?php echo \Form::file('file_new_1'); ?>
												</td>
											</tr>
										</table>
									</td>
									<?php if(count($category->files) > 1): ?>
										<td class="icon center"></td>
									<?php endif; ?>
									<?php if(count($category->files)): ?>
										<td class="icon center"></td>
									<?php endif; ?>
								</tr>
							<?php endif; ?>

						</table>
						<p style="padding-left: 15px; font-size: 13px;">
							<small><span class="text-danger">*</span> List of acceptable files types: MS Word, PDF, PowerPoint, MS Access.<br>
							You can rearrange the order of the files by performing drag and drop.</small>
						</p>
					</div>
					<div class="panel-footer text-right">
						<?php echo \Form::button('save', '<i class="fa fa-upload"></i> Upload', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
					</div>
				</div>

				<?php echo \Form::close(); ?>
				<?php endif; ?>


				<?php if(\Config::get('details.video.enabled', false)): ?>
					<?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
					<?php echo \Form::hidden('video_upload', 1); ?>
					<div class="panel panel-default">

						<div class="panel-heading">
							<h3 class="panel-title pull-left">Video Manager</h3>
						</div>

						<div class="panel-body">

							<span class="sort_message_container"></span>
							<div class="header_messages"></div>
							<table video="<?php echo \Uri::create('admin/product/category/video/'); ?>" rel="<?php echo \Uri::create('admin/product/category/sort/video/' . $category->id); ?>" class="table table-striped table-bordered sortable ">
								<tr class="nodrop nodrag blueTableHead">
									<th scope="col" class="">Video</th>
									<th scope="col">Video Properties</th>
									<?php if((!\Config::get('details.video.required', false) && !empty($category->videos)) || count($category->videos) > 1): ?>
										<?php if(count($category->videos) > 1): ?>
											<th scope="col" class="center">Re-order</th>
										<?php endif; ?>
										<th scope="col" class="center">Delete</th>
									<?php endif; ?>
								</tr>

								<?php if(is_array($category->videos)): ?>
									<?php foreach($category->videos as $video): ?>
										<?php
										$youtube = \App\Youtube::forge();
										$video->details = $youtube->parse($video->url)->get();
										?>
										<tr id="sort_<?php echo $video->id . '_' . $video->sort; ?>">
											<td class="center video_thumbnail" width="100">
												<?php if($video->thumbnail): ?>
													<img src="<?php echo \Helper::amazonFileURL('media/videos/' . $video->thumbnail); ?>" class="default" width="80"/>
													<div style="margin-top: 5px; position: relative;">
														<?php echo \Form::checkbox('video_delete_image_' . $video->id, 1, \Input::post('video_delete_image_' . $video->id), array('class' => 'video_delete_image', 'style' => 'margin: 0;')); ?>
														<span style="font-size: 9px;">Use YouTube?</span>
													</div>
												<?php else: ?>
													<img src="<?php echo $video->details['thumbnail']['small']; ?>" class="default" width="80"/>
												<?php endif; ?>
											</td>

											<td class="upload">
												<table width="100%" border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="120">Video URL</td>
														<td>
															<div class="input_holder">
																<?php echo \Form::input('video_url_'.$video->id, \Input::post('video_url_' . $video->id, $video->url), array('class' => 'video_url form-control')); ?>
															</div>
														</td>
														<td valign="top">
															<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
														</td>
													</tr>
													<tr>
														<td width="120">Video Title</td>
														<td colspan="2">
															<div class="input_holder">
																<?php echo \Form::input('video_title_'.$video->id, \Input::post('video_title_' . $video->id, $video->title), array('class' => 'video_title form-control')); ?>
															</div>
														</td>
													</tr>
													<tr>
														<td class="">Thumbnail Image</td>
														<td class="btn-file">
															<?php echo \Form::file('video_file_'.$video->id); ?>
															<?php if($video->thumbnail) : ?>
																<?php echo \Form::hidden('video_file_db_'.$video->id, $video->thumbnail); ?>
															<?php endif; ?>
														</td>
													</tr>
												</table>
											</td>
											<?php if(count($category->videos) > 1): ?>
												<td width="110" class="dragHandle">
													<ul class="table-action-inline">
														<li class="dragHandle">
															<a href="" onclick="return false;">Re-order</a>
														</li>
													</ul>
												</td>
											<?php endif; ?>
											<td width="110">
												<ul class="table-action-inline">
													<?php if((!\Config::get('details.video.required', false) && !empty($category->videos)) || count($category->videos) > 1): ?>
														<li>
															<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete video?" href="<?php echo \Uri::create('admin/product/category/delete_video/' . $video->id . '/' . $category->id); ?>">
																Delete
															</a>
														</li>
													<?php endif; ?>
												</ul>
											</td>
										</tr>

									<?php endforeach; ?>
								<?php endif; ?>

								<?php if(\Config::get('details.video.multiple', false) || empty($category->videos)): ?>
									<tr class="nodrop nodrag">
										<td width="100" class="td-thumb"><i class="fa fa-youtube fa-lg"></i></td>
										<td class="upload">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="100">Video URL</td>
													<td>
														<div class="input_holder">
															<?php echo \Form::input('video_url_new_1', \Input::post('video_url_new_1'), array('class' => 'form-control video_url')); ?>
														</div>
													</td>
													<td valign="top">
														<?php echo \Form::submit('save', 'Apply', array('class' => 'video_submit btn btn-primary btn-block')); ?>
													</td>
												</tr>
												<tr>
													<td width="120">Video Title</td>
													<td colspan="2">
														<div class="input_holder">
															<?php echo \Form::input('video_title_new_1', \Input::post('video_title_new_1'), array('class' => 'form-control video_title')); ?>
														</div>
													</td>
												</tr>
												<tr>
													<td class="">Thumbnail Image</td>
													<td class="btn-file">
														<?php echo \Form::file('video_file_new_1'); ?>
													</td>
												</tr>
											</table>
										</td>
										<?php if((!\Config::get('details.video.required', false) && !empty($category->videos)) || count($category->videos) > 1): ?>
											<?php if(count($category->videos) > 1): ?>
												<td class="icon center"></td>
											<?php endif; ?>
											<td class="icon center"></td>
										<?php endif; ?>
									</tr>
								<?php endif; ?>

							</table>
							<p style="padding-left: 15px; font-size: 13px;">
								<small>
								 	<span class="text-danger">*</span> Insert a Youtube or Vimeo URL link
								</small>
							</p>
						</div>

						<div class="panel-footer text-right">
							<?php echo \Form::button('save', '<i class="fa fa-upload"></i> Upload', array('type' => 'submit', 'class' => 'btn btn-primary')); ?>
						</div>
					</div>

					<?php \Theme::instance()->asset->js('product/videos.js', array(), 'basic'); ?>


					<?php echo \Form::close(); ?>
				<?php endif; ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>


<?php
	echo ckeditor_replace('ck_editor');
	echo \Theme::instance()->asset->js('product/videos.js');
?>


