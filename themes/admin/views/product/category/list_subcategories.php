<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Category Manager', 'admin/product/category/list');
		\Breadcrumb::set($category->title, 'admin/product/category/update/' . $category->id);
		\Breadcrumb::set('Sub Categories');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit <?php echo $category->parent_id > 0? 'Sub ': ''; ?>Category: Sub Categories</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/category/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/category/_navbar_links', array('category' => $category)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>

			<div class="row">
				<div class="col-sm-12">

					<?php
					echo \Theme::instance()->view('views/product/category/_listing_table',
						array(
							'pagination' 	=> $pagination,
							'items'			=> $items,
							'status'		=> $status,
						),
						false
					);
					?>

				</div>
			</div>

			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

