<?php if(isset($category)): // Used on "UPDATE" pages ?>
<div class="panel-nav-holder">
    <div class="btn-group">
        <a href="<?php echo \Uri::create('admin/product/category/update/' . $category->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-info-circle"></i> General Information</a>
        <a href="<?php echo \Uri::create('admin/product/category/subcategories/' . $category->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-folder-open"></i> Sub Categories
            <label for="" class="label label-primary"><?php echo $category->children ? count($category->children) : ''; ?></label></a>
        <a href="<?php echo \Uri::create('admin/product/list/' . $category->id . '/category'); ?>" class="btn btn-lg btn-default active"><i class="fa fa-cube"></i> Products</a>
        <a href="<?php echo \Uri::create('admin/product/category/infotab_list/' . $category->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-list"></i> Info Tabs</a>
        <a href="<?php echo \Uri::create('admin/product/category/update_seo/' . $category->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-check-circle"></i> Meta Content</a>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        if($(".btn-group").length > 0)
        {
            $(".btn-group a.active").removeClass('active');
            $(".btn-group a").each(function(){
                if(($(this).attr("href") == uri_current) ||
                    (uri_current.indexOf("infotab") != -1 && $(this).attr("href").indexOf("infotab") != -1))
                {
                    $(this).addClass('active');
                }
            });
        }
    });
</script>

<?php else: // Used on "CREATE" page ?>
    <?php if(FALSE): ?>
        <div class="second_menu_wrapper">
            <div class="second_menu">
                <ul>
                    <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
                </ul>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
