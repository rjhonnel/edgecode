<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Catalogue');
		\Breadcrumb::set('Category Manager', 'admin/product/category/list');
		\Breadcrumb::set($category->title, 'admin/product/category/update/' . $category->id);
		\Breadcrumb::set('Related Categories');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Category: Related Categories</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/product/category/_action_links'); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/product/category/_navbar_links', array('category' => $category)); ?>

            <div class="panel panel-default panel-col2">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="panel-title pull-left mt-5px">All Categories</h3>

                            <div class="form-inline pull-right">
                                <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
                                    <label>Show entries:</label>
                                    <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                                <?php echo \Form::close(); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <h3 class="panel-title pull-left mt-5px">Related Categories</h3>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
                                <?php
                                    echo \Theme::instance()->view('views/_partials/search_filters', array(
                                        'pagination' => $pagination,
                                        'status' => $status,
                                        'module' => 'category',
                                        'options' => array('status'),
                                        'layout' => '2',
                                    ), false);
                                ?>
                            <?php echo \Form::close(); ?>
                        </div>
                        <div class="col-sm-6 search_holder">
                            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
                                <?php
                                    echo \Theme::instance()->view('views/_partials/search_filters', array(
                                        'pagination' => $pagination,
                                        'status' => $status,
                                        'module' => 'category',
                                        'options' => array(),
                                    ), false);
                                ?>
                            <?php echo \Form::close(); ?>

                            <div class="formRow info_table_loading" style="display: none;">
                                <div class="search_loader">
                                    <!-- This is loaded while user is typing -->
                                    <div class="clear"></div>
                                    <div class="m_t_15"></div>
                                    <center style="margin: 0px 0 19px 0;">Searching for categories...</center>
                                    <center>
                                        <i class="fa fa-spinner fa-span"></i>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'POST')); ?>

            <div class="row">
                <div class="col-sm-6">
                    <div class="clearfix">
                        <ul class="default_list">
                            <li><a href="" onclick="return false;" class="select_all">Select All</a></li>
                            <li class="separator">/</li>
                            <li><a href="" onclick="return false;" class="unselect_all">Unselect All</a></li>
                            <li class="separator">/</li>
                            <li><a href="" onclick="return false;" class="select_qty"><span>0</span> items selected</a></li>
                        </ul>

                        <button name="add" value="add" type="submit" class="btn btn-small btn-primary pull-right">
                            <i class="fa fa-plus"></i> Add Selected
                        </button>

                    </div>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered  mt-5px">
                        <thead>
                        <tr class="blueTableHead">
                            <th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'categories[add][]'); ?></th>
                            <th scope="col">Title</th>
                        </tr>
                        </thead>
                        <tbody class="drag_table remove">
                        <?php if(!empty($category->not_related_categories)): ?>
                            <?php foreach($category->not_related_categories as $not_related): ?>
                                <tr>
                                    <td class="noresize"><?php echo \Form::checkbox('categories[add][]', $not_related->id); ?></td>
                                    <td><?php echo $not_related->title; ?></td>
                                </tr>

                            <?php endforeach; ?>
                            <?php $no_items = 'style="display: none;"'?>
                        <?php else: ?>
                            <?php $no_items = ''?>
                        <?php endif; ?>

                        <tr class="no_items" <?php echo $no_items; ?>>
                            <td class="noresize center" colspan="4">There are no more products to add</td>
                        </tr>

                        </tbody>
                    </table>

                    <div class="pagination-holder">
                        <?php echo $pagination->render(); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="clearfix">
                        <ul class="default_list">
                            <li><a href="" onclick="return false;" class="select_all">Select All</a></li>
                            <li class="separator">/</li>
                            <li><a href="" onclick="return false;" class="unselect_all">Unselect All</a></li>
                            <li class="separator">/</li>
                            <li><a href="" onclick="return false;" class="select_qty"><span>0</span> items selected</a></li>
                        </ul>

                        <button name="remove" value="remove" type="submit" class="btn btn-small btn-primary pull-right">
                            <i class="fa fa-minus"></i> Remove Selected
                        </button>
                    </div>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped table-bordered mt-5px search_table">
                        <thead>
                        <tr class="blueTableHead">
                            <th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'categories[remove][]'); ?></th>
                            <th scope="col">Title</th>
                        </tr>
                        </thead>
                        <tbody class="drag_table add" style="min-height: <?php echo count($category->not_related_categories)*56; ?>px">
                        <?php if(!empty($category->related_categories)): ?>
                            <?php foreach($category->related_categories as $related): ?>
                                <tr>
                                    <td class="noresize"><?php echo \Form::checkbox('categories[remove][]', $related->id); ?></td>
                                    <td><?php echo $related->title; ?></td>
                                </tr>

                            <?php endforeach; ?>
                            <?php $no_items = 'style="display: none;"'; ?>
                        <?php else: ?>
                            <?php $no_items = ''; ?>
                        <?php endif; ?>

                        <tr class="no_items" <?php echo $no_items; ?>>
                            <td class="noresize center" colspan="4">There are no related products</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php echo \Form::close(); ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

