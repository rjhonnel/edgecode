<div class="layout-content main-content" data-scrollable>
	<div class="main-content-body">
        <?php 
            \Breadcrumb::set('Home', 'admin/dashboard');
            \Breadcrumb::set('Catalogue');
            \Breadcrumb::set('Category Manager', 'admin/product/category/list');
            \Breadcrumb::set($category->title, 'admin/product/category/update/' . $category->id);
            \Breadcrumb::set('Info Tabs', 'admin/product/category/infotab_list/' . $category->id);
            \Breadcrumb::set('Edit Info Tab');
            \Breadcrumb::set($infotab->title);

            echo \Breadcrumb::create_links();
        ?>
		<div class="main-content-body-inner layout-content" data-scrollable>

				<header class="main-content-heading">
					<h4 class="pull-left">Edit Category: Edit Info Tab</h4>

					<div class="pull-right">
						<?php echo \Theme::instance()->view('views/product/category/_action_links'); ?>
					</div>
				</header>
				<?php echo \Theme::instance()->view('views/product/category/_navbar_links', array('category' => $category)); ?>

			    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data', 'class' => 'row-fluid')); ?>
				    <div class="row">
						<div class="col-sm-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title pull-left">General Information</h3>

								</div>
								<div class="panel-body ">
									<div class="form-group">
										<?php echo \Form::label('Description' . '  <span class="text-danger">*</span>'); ?>
										<div class="clear"></div>
										<?php echo \Form::textarea('description', \Input::post('description', $infotab->description), array('class' => 'form-control ck_editor')); ?>
									</div>
								</div>
							</div>

							<div class="panel panel-default">
								<div class="panel-heading">
									<h3 class="panel-title pull-left">File Manager</h3>

								</div>
								<div class="panel-body">
									<input type="hidden" name="to_upload" value="files">
									<small><span class="text-danger">*</span> List of acceptable files types: PDF.</small>
									<div class="header_messages"></div>
	                            	<table class="table table-striped table-bordered sortable">
	                                    <tr class="nodrop nodrag blueTableHead">
	                                        <th scope="col" class="noresize">File</th>
                                            <th scope="col">File Properties</th>
                                        	<?php if((!\Config::get('details.file.required', false) && !empty($infotab->files)) || count($infotab->files) > 1): ?>
	                                            <th scope="col" class="center">Delete</th>
                                        	<?php endif; ?>
	                                    </tr>
	                                    
	                                    <?php if(is_array($infotab->files)): ?>
	                                        <?php foreach($infotab->files as $file): ?>
	                                        	<?php 
	                                        		// Get file extension
	                                        		$extension = strtolower(pathinfo($file->file, PATHINFO_EXTENSION));
	                                        	?>
		                                        <tr id="sort_<?php echo $file->id . '_' . $file->sort; ?>">
		                                            <td class="td-thumb">
		                                            	<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
		                                            		<?php 
		                                            			$icon_list = array('doc', 'docx', 'pdf', 'txt', 'xls', 'xlsx');
		                                            			$extension = in_array($extension, $icon_list) ? $extension : 'txt';
		                                            		?>
															<i class="fa fa-file-<?php echo $extension; ?>-o"></i>
		                                            	</a>
		                                            </td>
		                                            <td class="upload">
		                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		                                                    <tr>
		                                                        <td class="noresize"><div class="input_holder">File Name</div></td>
		                                                        <td>
	                                                        		<div class="input_holder"><i>
	                                                        			<a target="_blank" href="<?php echo \Helper::amazonFileURL('media/files/' . $file->file); ?>" title="<?php echo $file->title ?: $file->file; ?>">
	                                                        				<?php echo $file->file; ?>
	                                                        			</a>
	                                                        		</i></div>
		                                                        </td>
		                                                    </tr>
		                                                    <tr>
		                                                        <td class="noresize">Title</td>
		                                                        <td>
		                                                        	<div class="input_holder">
		                                                        		<?php echo \Form::input('title_'.$file->id, \Input::post('title_' . $file->id, $file->title), array('class' => 'form-control')); ?>
		                                                        	</div>
		                                                        </td>
		                                                    </tr>
		                                                    <tr>
		                                                        <td class="noresize">Replace File</td>
		                                                        <td>
		                                                        	<?php echo \Form::file('file_'.$file->id); ?>
		                                                        	<?php echo \Form::hidden('file_db_'.$file->id, $file->file); ?>
		                                                        </td>
		                                                    </tr>
		                                                </table>
		                                            </td> 
			                                        <?php if((!\Config::get('details.file.required', false) && !empty($infotab->files)) || count($infotab->files) > 1): ?>  
			                                            <td class="icon center">
			                                            	<a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete file?" href="<?php echo \Uri::create('admin/product/category/delete_infotab_file/' . $file->id . '/' . $infotab->id); ?>">Delete</a>
			                                            </td>
													<?php endif; ?>
		                                        </tr>
		                                        
	                                        <?php endforeach; ?>
										<?php endif; ?>
                                        
                                        <?php if(\Config::get('details.file.multiple', false) || empty($infotab->files)): ?>
	                                        <tr class="nodrop nodrag">
												<td class="td-thumb">
													<div class="fa fa-file-text-o"></div>
												</td>
	                                            <td class="upload">
	                                            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	                                                    <tr>
	                                                        <td class="noresize">Title</td>
	                                                        <td>
	                                                        	<div class="input_holder">
	                                                        		<?php echo \Form::input('title_new_1', \Input::post('title_new_1'), array('class' => 'form-control')); ?>
	                                                        	</div>
	                                                        </td>
	                                                    </tr>
	                                                    <tr>
	                                                        <td class="noresize">Choose File</td>
	                                                        <td>
	                                                        	<?php echo \Form::file('file_new_1'); ?>
	                                                        </td>
	                                                    </tr>
	                                                </table>
	                                            </td>
		                                        <?php if((!\Config::get('details.file.required', false) && !empty($infotab->files)) || count($infotab->files) > 1): ?>
		                                            <td class="icon center"></td>
		                                        <?php endif; ?>
	                                        </tr>
                                        <?php endif; ?>
	                                </table>
								</div>
							</div>
						</div>
					</div>
					<div class="save_button_holder text-right">
                        <?php echo \Form::button('save', '<i class="fa fa-edit"></i>Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
                    	<?php echo \Form::button('exit', 'Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
		            </div>
				<?php echo \Form::close(); ?>
	            
		</div>
	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
<?php echo ckeditor_replace('ck_editor'); ?>
	            
            	