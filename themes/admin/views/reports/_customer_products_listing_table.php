<?php
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
$status = array(
    'false' => 'All',
    '1' => 'Active',
    '0' => 'Inactive',
);
?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <?php
            $customer_list = \SentryAdmin::user()->all('front');
            $customer_id_list = array();
            foreach ($customer_list as $customer) {
                $meta_data = \SentryAdmin::user((int)$customer['id'])['metadata'];
                $customer_id_list += array($meta_data['first_name'].' '.$meta_data['last_name'] => $meta_data['first_name'].' '.$meta_data['last_name']);
            }
            echo \Form::label('Customer');
            echo \Form::select('title', \Input::get('title'), array('customer_order_select' => 'Select Customer') + $customer_id_list, array('class' => 'form-control'));
            ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="">&nbsp;</label>
            <ul class="list-inline" style="margin:0;">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<?php echo \Form::close(); ?>

<table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Product Code</th>
        <th scope="col">Product Name</th>
        <th scope="col">Attribute Group</th>
        <th scope="col">Status</th>
        <th scope="col">Stock</th>
        <th scope="col">Price Paid</th>
    </tr>
    </thead>
    <tbody>
    <?php $check_product = 0; ?>
    <?php if( !is_null(\Input::get('title')) && \Input::get('title') != 'customer_order_select'): ?>
        <?php foreach($items as $item): ?>
            <?php $item = (Object)$item; ?>

            <?php
            foreach($item->products as $product):
                $prdct = \Product\Model_Product::find_one_by_id($product->product_id);
                ?>
                <tr>
                    <td><?php echo $product->code; ?></td>
                    <td><?php echo $product->title; ?></td>
                    <td><?php echo isset($prdct->active_attribute_group[0]) ? $prdct->active_attribute_group[0]->title : 'N/A'; ?></td>
                    <td>
                        <?php
                        // If page is active from certain date
                        if(isset($prdct->status))
                        {
                            if($prdct->status == 2)
                            {
                                $dates = array();
                                !is_null($prdct->active_from) and array_push($dates, date('m/d/Y', $prdct->active_from));
                                !is_null($prdct->active_to) and array_push($dates, date('m/d/Y', $prdct->active_to));

                                if(true)
                                {
                                    ?>
                                    Active
                                    <a class="activeDate" rel="tooltip" title="<?php echo implode(' - ', $dates); ?>">
                                        <?php echo \Theme::instance()->asset->img('icon-calendar.png', array('width' => 16, 'height' => 16)); ?>
                                    </a>
                                    <?php
                                }
                            }
                            else
                            {
                                echo $status[$prdct->status];
                            }
                        }
                        ?>
                    </td>
                    <td><?php echo $product->quantity; ?></td>
                    <td>$<?php echo number_format($product->price, 2); ?></td>
                </tr>
                <?php $check_product = 1; ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if(!$check_product): ?>

        <tr class="nodrag nodrop">
            <td colspan="6" class="center"><strong><?php echo is_null(\Input::get('title')) || \Input::get('title') == 'customer_order_select' ? 'Select customer name.' : 'There are no items.'; ?></strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>
<a href="#" class="export_table btn btn-default btn-sm" data-href="<?php echo \Uri::create('admin/reports/export_customer_products').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>