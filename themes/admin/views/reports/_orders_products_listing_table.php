<?php
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
$margin = \Str::alternator('m_r_15', 'm_r_15', 'm_r_15', '');
?>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <?php echo \Form::label('Delivery Date', null, array('class' => 'dateLabel')); ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('delivery_date', \Input::get('delivery_date'), array('class' => 'form-control from_white dateInput')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label for="">&nbsp;</label>
            <ul class="list-inline" style="float: none;">
                <li>
                    <?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary')); ?>
                </li>
                <li>
                    <a class="btn btn-sm btn-default searchDateToday" data-href="/admin/reports/orders_products" data-name="delivery_date" href="/admin/reports/orders_products">Today</a>
                </li>
                <li>
                    <a class="btn btn-sm btn-default searchDateTomorrow" data-href="/admin/reports/orders_products" data-name="delivery_date" href="/admin/reports/orders_products">Tomorrow</a>
                </li>
                <li>
                    <a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php echo \Form::close(); ?>

<table id="print_table_area_1" rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
        <?php if($items): ?>
            <tr class="blueTableHead">
                <th scope="col" style="width:100px;">Delivery Time</th>
                <?php foreach($items as $item): ?>
                    <th scope="col"><?php echo $item->delivery_datetime?(date('h:i:s a', strtotime($item->delivery_datetime))):''; ?></th>
                <?php endforeach; ?>
                <th scope="col" style="width:100px;">Total</th>
            </tr>
            <tr class="blueTableHead">
                <th scope="col" style="width:100px;">Order No.</th>
                <?php foreach($items as $item): ?>
                    <th scope="col"><?php echo $item->id; ?></th>
                <?php endforeach; ?>
                <th scope="col" style="width:100px;"></th>
            </tr>
            <tr class="blueTableHead">
                <th scope="col" style="width:100px;">Customer</th>
                <?php foreach($items as $item): ?>
                    <th scope="col"><?php echo $item->first_name . ' ' . $item->last_name; ?></th>
                <?php endforeach; ?>
                <th scope="col" style="width:100px;"></th>
            </tr>
        <?php else: ?>
            <tr class="blueTableHead">
                <th scope="col">Delivery Time</th>
                <th scope="col" style="width:100px;">Total</th>
            </tr>
            <tr class="blueTableHead">
                <th scope="col">Order No.</th>
                <th scope="col" style="width:100px;"></th>
            </tr>
            <tr class="blueTableHead">
                <th scope="col">Customer</th>
                <th scope="col" style="width:100px;"></th>
            </tr>
        <?php endif; ?>
    </thead>
    <tbody>
    <?php if($items): ?>
        <?php foreach($parent_categories as $category): ?>
            <?php if($all_ordered_products = $category->get_categories_ordered_products($items, $category)): ?>
                <?php
                    $hold_product_list_td = '';
                    foreach($all_ordered_products as $product): $line_total_quantity = 0;

                        // if product has more than one category only show in first category
                        if(count($product->categories) > 1)
                        {
                            $arrayKeys = array_keys($product->categories);
                            // if($product->categories[$arrayKeys[0]]->id != $category->id)
                            if(!in_array($category->id, $arrayKeys))
                                continue;
                        }

                        $hold_product_list_td .= '<tr class="nodrag nodrop">';
                        $hold_product_list_td .= '<td>'.$product->title.'</td>';

                        foreach($items as $item):
                            $quantity = $item->in_order_get_product_quantity_by_id($item->id, $product->id);
                            $line_total_quantity += $quantity;

                            $hold_product_list_td .= '<td scope="col">';
                            if($quantity)
                                $hold_product_list_td .= $quantity;
                            $hold_product_list_td .= '</td>';
                        endforeach;

                        $hold_product_list_td .= '<td scope="col" style="width:100px;">';
                        if($line_total_quantity)
                            $hold_product_list_td .= $line_total_quantity;
                        $hold_product_list_td .= '</td>';
                        $hold_product_list_td .= '</tr>';
                    endforeach;
                ?>
                <?php if($hold_product_list_td): ?>
                    <tr class="nodrag nodrop">
                        <th colspan="<?php echo count($items)+2; ?>" ><?php echo $category->title; ?></th>
                    </tr>
                    <?php echo $hold_product_list_td; ?>
                <?php endif; ?>
            <?php endif; ?>
        <?php endforeach; ?>
        <tr class="nodrag nodrop">
            <th>Total</th>
            <?php $over_all_total = 0; foreach($items as $item): ?>
                <td>
                    <?php
                        $total = $item->order_info_active_products($item->id)['quantity'];
                        $over_all_total += $total;
                        echo $total;
                    ?>
                </td>
            <?php endforeach; ?>
            <td><?php echo $over_all_total; ?></td>
        </tr>
    <?php endif; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td class="center" colspan="2"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td class="center" colspan="2"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<div class="form-group">
    <a href="#" class="export_table btn btn-default btn-sm" data-href="<?php echo \Uri::create('admin/reports/export_orders_products').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>
    <a href="<?php echo \Uri::create('admin/reports/orders_products_print').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>" target="_blank" print_table class="btn btn-default btn-sm" data-table="#print_table_area_1">Print</a>
    <?php echo \Theme::instance()->asset->js('plugins/printThis.js'); ?>
</div>

<h4 class="pull-left">Notes</h4>
<table id="print_table_area_2" class="table table-striped table-bordered print_table_area_2">
    <thead>
    <tr class="blueTableHead">
        <th scope="col" style="width:250px">Customer</th>
        <th scope="col" style="width:200px">Company</th>
        <th scope="col" style="width:100px">Order No.</th>
        <th scope="col">Notes</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>
            <td><?php echo $item->first_name . ' ' . $item->last_name; ?></td>
            <td><?php echo $item->company; ?></td>
            <td><?php echo $item->id; ?></td>
            <td><?php echo $item->notes; ?></td>
        </tr>

    <?php endforeach; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="4" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td colspan="4" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<div class="form-group">
    <a href="#" class="export_table btn btn-default btn-sm" data-href="<?php echo \Uri::create('admin/reports/export_orders_products_notes').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>
    <a href="#" class="print_table btn btn-default btn-sm" data-table="#print_table_area_2">Print</a>
    <?php echo \Theme::instance()->asset->js('plugins/printThis.js'); ?>
</div>