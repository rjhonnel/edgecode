<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Reports');
		\Breadcrumb::set('Sales x Year', 'admin/report/sales');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Sales By Year Report</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/settings/user/_action_links'); ?>
				</div>
			</header>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
			<?php
			// Load reports listing table
			echo \Theme::instance()->view('views/reports/_sales_by_month_listing_table',
				array(
					'items'			=> $items,
					'years'         => $years,
				),
				false
			);
			?>
			<?php echo \Form::close(); ?>

		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>