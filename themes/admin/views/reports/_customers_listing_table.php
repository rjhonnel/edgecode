<?php
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
$margin = \Str::alternator('m_r_15', 'm_r_15', 'm_r_15', '');
$status = array(
    'false' => 'All',
    '1' => 'Active',
    '0' => 'Inactive',
);
?>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <?php echo \Form::label('User Status', null, array('class' => 'm_r_15')); ?>
            <?php echo \Form::select('activated', \Input::get('activated', isset($values['activated']) ? $values['activated'] : false ), array('select' => 'Select') + array('false' => 'All') + array(0 => 'Inactive', 1 => 'Active'), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="#">&nbsp;</label>
            <ul class="list-inline">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>

        </div>
    </div>
</div>

<?php echo \Form::close(); ?>
<table class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <?php /*
            <th scope="col" style="width: 40px;"></th>
            */ ?>
        <th scope="col">Customer ID</th>
        <th scope="col">Customer Name</th>
        <th scope="col">Company Name</th>
        <th scope="col">Customer Group</th>
        <th scope="col">Email</th>
        <th scope="col">Phone</th>
        <th scope="col">State</th>
        <th scope="col">Activated</th>
        <th scope="col">Account Created</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>
        <?php
        $user_groups = $item->groups();
        $user_group = current($user_groups);
        ?>
        <tr>
            <td><?php if($item->metadata) echo $item->get('metadata.user_id'); ?></td>
            <td><?php if($item->metadata) echo $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name'); ?></td>
            <td><?php if($item->metadata) echo $item->get('metadata.business_name'); ?></td>
            <td><?php echo $user_group['name']; ?></td>
            <td>
                <?php echo $item->get('email'); ?>
            </td>
            <td><?php if($item->metadata) echo $item->get('metadata.phone'); ?></td>
            <td><?php if($item->metadata) echo $item->get('metadata.state'); ?></td>
            <td class="noresize">
                <?php echo $item->get('activated') == 1 ? 'Yes' : 'No'; ?>
            </td>
            <td><?php echo date('d/m/Y', $item->created_at); ?></td>
        </tr>

    <?php endforeach; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="9" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td colspan="9" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<a href="#" class="export_table btn btn-default btn-sm" data-href="<?php echo \Uri::create('admin/reports/export_customers').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>