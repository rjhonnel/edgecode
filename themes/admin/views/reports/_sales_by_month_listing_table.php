<?php
    $settings = \Config::load('autoresponder.db');
    $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
?>
<?php
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
$margin = \Str::alternator('m_r_15', 'm_r_15', 'm_r_15', '');
?>

<div class="row">
    <div class="col-sm-5">
        <div class="form-group">
            <?php echo \Form::label('Year'); ?>
            <?php echo \Form::select('year', \Input::get('year'), array('select' => 'Select') + $years, array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <label for="#">&nbsp;</label>
            <ul class="list-inline">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<?php echo \Form::close(); ?>

<table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Month</th>
        <th scope="col">Number of Orders</th>
        <th scope="col">Total Sales (without gst)</th>
        <th scope="col">Total Sales (with gst)</th>
        <th scope="col">GST <?php echo $hold_gst_percentage; ?>% (Included)</th>
    </tr>
    </thead>
    <tbody>

    <?php  foreach($items as $item): ?>
        <tr>
            <td><?php echo $item['month']['name']; ?></td>
            <td><?php echo $item['month']['num_orders']; ?></td>
            <td>$<?php echo number_format(floatval(str_replace(',', '', $item['month']['without_gst'])),2);?></td>
            <td>$<?php echo number_format(floatval(str_replace(',', '', $item['month']['amount'])),2);?></td>
            <td>$<?php echo number_format(floatval(str_replace(',', '', $item['month']['gst'])),2);?></td>
        </tr>

    <?php endforeach; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<a href="#" class="btn btn-default btn-sm export_table" data-href="<?php echo \Uri::create('admin/reports/export_sales_by_month').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>