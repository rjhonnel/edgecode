<?php
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
$status = array(
    'false' => 'All',
    '1' => 'Active',
    '0' => 'Inactive',
);
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <?php echo \Form::label('Delivery Period', null, array('class' => 'dateLabel')); ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_from', \Input::get('date_from'), array('class' => 'form-control from_white dateInput', 'placeholder' => 'From')); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'form-control  to_white dateInput', 'placeholder' => 'To')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <?php
            $customer_list = \SentryAdmin::user()->all('front');
            $customer_id_list = array();
            foreach ($customer_list as $customer) {
                $meta_data = \SentryAdmin::user((int)$customer['id'])['metadata'];
                $customer_id_list += array((int)$customer['id'] => $meta_data['first_name'].' '.$meta_data['last_name']);
            }
            echo \Form::label('Customer');
            echo \Form::select('user_id', \Input::get('user_id'), array('' => 'Select Customer','all' => 'All') + $customer_id_list, array('class' => 'form-control'));
            ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label for="">&nbsp;</label>
            <ul class="list-inline" style="margin:0;">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<?php echo \Form::close(); ?>

<table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col" style="width:250px">Customer</th>
        <th scope="col" style="width:200px">Company</th>
        <th scope="col" style="width:100px">Order No.</th>
        <th scope="col">Notes</th>
        <th scope="col">Delivery Notes</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>

        <tr>
            <td><?php echo $item->first_name . ' ' . $item->last_name; ?></td>
            <td><?php echo $item->company; ?></td>
            <td><?php echo $item->id; ?></td>
            <td><?php echo $item->notes; ?></td>
            <td><?php echo $item->delivery_notes; ?></td>
        </tr>

    <?php endforeach; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<a href="#" class="export_table btn btn-default btn-sm" data-href="<?php echo \Uri::create('admin/reports/export_order_notes').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>