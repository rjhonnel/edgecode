<?php
    $settings = \Config::load('autoresponder.db');
    $hold_gst = (isset($settings['gst']) ? 1 : 0);
    $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
    echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
?>
<div class="row">
    <div class="col-sm-5">
        <div class="form-group">
            <?php echo \Form::label('Order Period', null, array('class' => 'dateLabel')); ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_from', \Input::get('date_from'), array('class' => 'form-control from_white dateInput', 'placeholder' => 'From')); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'form-control to_white dateInput', 'placeholder' => 'To')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="#">&nbsp;</label>
            <ul class="list-inline">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<?php echo \Form::close(); ?>
<table class="table table-striped table-bordered">
    <thead>
        <tr class="blueTableHead">
            <th scope="col">Item</th>
            <th scope="col">Number of Sold</th>
            <th scope="col">Total Sales</th>
            <th scope="col">GST <?php echo $hold_gst_percentage; ?>% (Included)</th>
        </tr>
    </thead>
    <tbody>
        <?php if($items): ?>
            <?php foreach($parent_categories as $category): ?>
                <?php if($all_ordered_products = $category->get_categories_ordered_products($items, $category)): ?>
                    <?php
                        $hold_product_list_td = '';
                        foreach($all_ordered_products as $product): $line_total_quantity = 0; $line_total_sales = 0; $final_gst = 0;
                            // if product has more than one category only show in first category
                            if(count($product->categories) > 1)
                            {
                                $arrayKeys = array_keys($product->categories);
                                if(!in_array($category->id, $arrayKeys))
                                    continue;
                            }

                            $hold_product_list_td .= '<tr>';
                            $hold_product_list_td .= '<td>'.$product->title.'</td>';

                            foreach($items as $item):
                                $quantity = $item->in_order_get_product_quantity_by_id($item->id, $product->id);
                                $price = $item->in_order_get_product_price_by_id($item->id, $product->id);
                                $line_total_quantity += $quantity;
                                $line_total_sales += $price;
                            endforeach;

                            $hold_product_list_td .= '<td>'.$line_total_quantity.'</td>';
                            $hold_product_list_td .= '<td>$'.nf($line_total_sales).'</td>';
                            
                            if($hold_gst && $hold_gst_percentage > 0 && $line_total_sales > 0)
                                $final_gst = \Helper::calculateGST($line_total_sales, $hold_gst_percentage);

                            $hold_product_list_td .= '<td>$'.nf($final_gst).'</td>';
                            $hold_product_list_td .= '</tr>';
                        endforeach;
                    ?>
                    <?php if($hold_product_list_td): ?>
                        <tr>
                            <th colspan="4"><?php echo $category->title; ?></th>
                        </tr>
                        <?php echo $hold_product_list_td; ?>
                    <?php endif; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>

        <?php if(!\Input::get()): ?>
            <tr class="nodrag nodrop">
                <td class="center" colspan="4"><strong>Select option first.</strong></td>
            </tr>
        <?php elseif(empty($items)): ?>
            <tr class="nodrag nodrop">
                <td class="center" colspan="4"><strong>There are no items.</strong></td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>

<a href="#" class="btn btn-default btn-sm export_table" data-href="<?php echo \Uri::create('admin/reports/export_sales_by_item').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>