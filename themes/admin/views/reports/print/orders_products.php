<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=320, target-densitydpi=device-dpi">
        <style type="text/css">
            body { width: 100% !important; font-size: 12px; font-weight: normal;}
            body { background-color: #fff; margin: 0; padding: 0; }
            img { outline: none; text-decoration: none; display: block;}
            body, td { font-family: Arial, Helvetica, sans-serif; color: #000; text-decoration: none; font-size: 12px; font-weight: normal;}
            a{
                color: #000; font-weight:bold; text-decoration:none;
            }
            table {

            }
            th {
                font-weight: bold;
                border-bottom: 1px solid #cccccc;
                padding: 0 10px;
            }
            .text-right { text-align: right; }
            .bold { font-weight:bold; }
        </style>
    </head>
    <body>
        <?php
            $items = $content['items'];
            $parent_categories = $content['parent_categories'];
            $counter = 0;
        ?>
        <?php if($items && \Input::get()): ?>
            <?php if($parent_categories): ?>
                <?php foreach($parent_categories as $category): ?>
                    <?php if($all_ordered_products = $category->get_categories_ordered_products($items, $category)): ?>
                        <?php echo $counter > 0?'<div pagebreak="true"></div>':''; ?>
                        <table width="100%" cellpadding="10" cellspacing="0" border="1">
                            <thead>
                                <?php if($items): ?>
                                    <tr>
                                        <th>Delivery Time</th>
                                        <?php foreach($items as $item): ?>
                                            <th><?php echo $item->delivery_datetime?(date('h:i:s a', strtotime($item->delivery_datetime))):''; ?></th>
                                        <?php endforeach; ?>
                                        <th>Total</th>
                                    </tr>
                                    <tr>
                                        <th>Order No.</th>
                                        <?php foreach($items as $item): ?>
                                            <th><?php echo $item->id; ?></th>
                                        <?php endforeach; ?>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th>Customer</th>
                                        <?php foreach($items as $item): ?>
                                            <th><?php echo $item->first_name . ' ' . $item->last_name; ?></th>
                                        <?php endforeach; ?>
                                        <th></th>
                                    </tr>
                                <?php endif; ?>
                            </thead>
                            <tbody>
                            <?php if($items): ?>
                                <?php
                                    $hold_product_list_td = '';
                                    foreach($all_ordered_products as $product): $line_total_quantity = 0;

                                        // if product has more than one category only show in first category
                                        if(count($product->categories) > 1)
                                        {
                                            $arrayKeys = array_keys($product->categories);
                                            // if($product->categories[$arrayKeys[0]]->id != $category->id)
                                            if(!in_array($category->id, $arrayKeys))
                                                continue;
                                        }

                                        $hold_product_list_td .= '<tr>';
                                        $hold_product_list_td .= '<td>'.$product->title.'</td>';

                                        foreach($items as $item):
                                            $quantity = $item->in_order_get_product_quantity_by_id($item->id, $product->id);
                                            $line_total_quantity += $quantity;

                                            $hold_product_list_td .= '<td>';
                                            if($quantity)
                                                $hold_product_list_td .= $quantity;
                                            $hold_product_list_td .= '</td>';
                                        endforeach;

                                        $hold_product_list_td .= '<td>';
                                        if($line_total_quantity)
                                            $hold_product_list_td .= $line_total_quantity;
                                        $hold_product_list_td .= '</td>';
                                        $hold_product_list_td .= '</tr>';
                                    endforeach;
                                ?>
                                <?php if($hold_product_list_td): ?>
                                    <tr>
                                        <th colspan="<?php echo count($items)+2; ?>" ><?php echo $category->title; ?></th>
                                    </tr>
                                    <?php echo $hold_product_list_td; ?>
                                <?php endif; ?>
                                <tr>
                                    <th>Total</th>
                                    <?php $over_all_total = 0; foreach($items as $item): ?>
                                        <?php
                                            $total = $item->order_info_active_products($item->id, $category)['quantity'];
                                            $over_all_total += $total;
                                        ?>
                                        <td><?php echo $total?:''; ?></td>
                                    <?php endforeach; ?>
                                    <td><?php echo $over_all_total; ?></td>
                                </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    <?php $counter++; endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        <?php else: ?>
            <table width="100%" cellpadding="10" cellspacing="0" border="1">
                <thead>
                    <tr>
                        <th>Delivery Time</th>
                        <th>Total</th>
                    </tr>
                    <tr>
                        <th>Order No.</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Customer</th>
                        <th></th>
                    </tr>
                </thead>

                <?php if(!\Input::get()): ?>
                    <tr>
                        <td class="center" colspan="2"><strong>Select option first.</strong></td>
                    </tr>
                <?php elseif(empty($items)): ?>
                    <tr>
                        <td class="center" colspan="2"><strong>There are no items.</strong></td>
                    </tr>
                <?php endif; ?>

                </tbody>
            </table>
        <?php endif; ?>
    </body>
</html>