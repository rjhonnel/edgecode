<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form')); ?>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <?php
            $customer_list = \SentryAdmin::user()->all('front');
            $customer_id_list = array();
            foreach ($customer_list as $customer) {
                $meta_data = \SentryAdmin::user((int)$customer['id'])['metadata'];
                $customer_id_list += array($meta_data['first_name'].' '.$meta_data['last_name'] => $meta_data['first_name'].' '.$meta_data['last_name']);
            }
            echo \Form::label('Customer');
            echo \Form::select('title', \Input::get('title'), array('customer_order_select' => 'Select Customer') + $customer_id_list, array('class' => 'form-control'));
            ?>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <div class="date_content">
                <?php echo \Form::label('Order Period', null, array('class' => 'dateLabel')); ?>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="datepicker-holder-control">
                            <?php echo \Form::input('date_from', \Input::get('date_from'), array('class' => 'from_white dateInput form-control', 'placeholder' => 'From')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="datepicker-holder-control">
                            <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'to_white dateInput form-control', 'placeholder' => 'To')); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo \Form::label('Order Status'); ?>
            <?php echo \Form::select('status', \Input::get('status'), array('false' => 'Select') + \Config::get('order.status', array()), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label for="">&nbsp;</label>
            <ul class="list-inline" style="float: none;">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<?php echo \Form::close(); ?>

<table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Order Date</th>
        <th scope="col">Order Amount</th>
        <th scope="col">Order Status</th>
        <th scope="col">Payment Status</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>

        <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
            <td><?php echo date('d/m/Y', $item->created_at).' '.date('H:i:s', $item->created_at); ?></td>
            <td>$<?php echo number_format(round($item->total_price(), 2), 2); ?></td>
            <td><?php echo Inflector::humanize($item->status); ?></td>
            <td><?php echo isset($item->last_payment->status_detail) ?  $item->last_payment->status_detail : '' ?></td>
        </tr>

    <?php endforeach; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="4" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td colspan="4" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>
<a href="#" class="export_table btn btn-default btn-sm" data-href="<?php echo \Uri::create('admin/reports/export_customer_orders').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>