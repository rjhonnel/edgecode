<?php
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
$status = array(
    'false' => 'All',
    '1' => 'Active',
    '0' => 'Inactive',
);
?>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <?php
            $customer_list = \SentryAdmin::user()->all('front');
            $customer_id_list = array();
            foreach ($customer_list as $customer) {
                $meta_data = \SentryAdmin::user((int)$customer['id'])['metadata'];
                $customer_id_list += array((int)$customer['id'] => $meta_data['first_name'].' '.$meta_data['last_name']);
            }
            echo \Form::label('Customer');
            echo \Form::select('user_id', \Input::get('user_id'), array('' => 'Select Customer','all' => 'All') + $customer_id_list, array('class' => 'form-control'));
            ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label for="">&nbsp;</label>
            <ul class="list-inline" style="margin:0;">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<?php echo \Form::close(); ?>
<table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Customer</th>
        <th scope="col">Total Number of Order</th>
        <th scope="col">Total Amount of Order</th>
    </tr>
    </thead>
    <tbody>
    <?php if($sales_customer): ?>
        <?php foreach($sales_customer as $ctm): ?>
            <tr>
                <td><?php echo $ctm['first_name'].' '.$ctm['last_name']; ?></td>
                <td><?php echo $ctm['total_order']; ?></td>
                <td>$<?php echo nf($ctm['total_amount']); ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="3" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($sales_customer)): ?>
        <tr class="nodrag nodrop">
            <td colspan="3" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<a href="#" class="btn btn-default btn-sm export_table" data-href="<?php echo \Uri::create('admin/reports/export_sales_by_customer').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>