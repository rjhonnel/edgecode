<?php
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
$status = array(
    'false' => 'All',
    '1' => 'Active',
    '0' => 'Inactive',
);
?>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            <div class="double_select">
                <?php echo \Form::label('Status'); ?>
                <?php echo \Form::select('status', \Input::get('status'), array('select' => 'Select') + $status, array('class' => 'toggle_dates form-control', 'onchange' => "show_dates($(this), 2, $('.toggable_dates'));")); ?>
            </div>

            <div class="toggable_dates date_content" <?php echo \Input::get('status') != '2' ? 'style="display: none;"' : ''; ?>>

                <?php echo \Form::label('Dates Active', null, array('class' => 'dateLabel')); ?>
                <?php echo \Form::input('active_from', \Input::get('active_from'), array('class' => 'from_white dateInput', 'placeholder' => 'From')); ?>
                <div class="left spacing_double">
                    <?php echo \Form::input('active_to', \Input::get('active_to'), array('class' => 'to_white dateInput', 'placeholder' => 'To')); ?>
                </div>

            </div>
        </div>
    </div>
    <div class="col-sm-5">
        <div class="form-group">
            <label for="#">&nbsp;</label>
            <ul class="list-inline" style="margin:0">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btns-m')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>

<?php echo \Form::close(); ?>

<table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Product Code</th>
        <th scope="col">Product Name</th>
        <th scope="col">Attribute Group</th>
        <th scope="col">Attribute</th>
        <th scope="col">Status</th>
        <th scope="col">Stock</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php
        $item = (Object)$item;

        $check_attribute = DB::query('SELECT * FROM product_attributes a where a.product_id = '.$item->id)->execute();

        if(count($check_attribute)==1 && $check_attribute[0]['attribute_group_id']==0):
            ?>

            <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
                <td><?php echo $item->code; ?></td>
                <td><?php echo $item->title; ?></td>
                <td><?php echo $item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A'; ?></td>
                <td><?php echo $item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A'; ?></td>
                <td>
                    <?php
                    // If page is active from certain date
                    if($item->status == 2)
                    {
                        $dates = array();
                        !is_null($item->active_from) and array_push($dates, date('m/d/Y', $item->active_from));
                        !is_null($item->active_to) and array_push($dates, date('m/d/Y', $item->active_to));

                        if(true)
                        {
                            ?>
                            Active
                            <a class="activeDate" rel="tooltip" title="<?php echo implode(' - ', $dates); ?>">
                                <?php echo \Theme::instance()->asset->img('icon-calendar.png', array('width' => 16, 'height' => 16)); ?>
                            </a>
                            <?php
                        }
                    }
                    else
                    {
                        echo $status[$item->status];
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $hold_stock = DB::query('SELECT b.id, b.stock_quantity FROM product a join product_attributes b on a.id = b.product_id where a.id = '.$item->id)->execute();
                    echo $hold_stock[0]['stock_quantity'];
                    ?>
                </td>
            </tr>

            <?php
        else:
            $count = 0;
            foreach ($check_attribute as $key => $value):
                ?>

                <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
                    <td><?php echo $value['product_code']; ?></td>
                    <td><?php echo $count==0 ? $item->title : ''; ?></td>
                    <td><?php echo $count==0 ? ($item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A') : ''; ?></td>
                    <td>
                        <?php
                        $attribute_list = (json_decode($value['attributes'], TRUE));
                        $attribute_list_hold = array();

                        foreach ($attribute_list as $key => $value1) {
                            $attribute_list_hold[] = DB::query('SELECT a.title FROM attribute_options a where a.attribute_id = '.$key.' and a.id = '.$value1)->execute();
                        }

                        $count_attr = count($attribute_list_hold);
                        $count_attr_count = 0;
                        foreach ($attribute_list_hold as $key => $value3) {
                            foreach ($value3[0] as $key => $value2) {
                                echo $value2;
                                if($count_attr-1 != $count_attr_count)
                                    echo " | ";
                                $count_attr_count++;
                            }
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if($count == 0)
                        {
                            // If page is active from certain date
                            if($item->status == 2)
                            {
                                $dates = array();
                                !is_null($item->active_from) and array_push($dates, date('m/d/Y', $item->active_from));
                                !is_null($item->active_to) and array_push($dates, date('m/d/Y', $item->active_to));

                                if(true)
                                {
                                    ?>
                                    Active
                                    <a class="activeDate" rel="tooltip" title="<?php echo implode(' - ', $dates); ?>">
                                        <?php echo \Theme::instance()->asset->img('icon-calendar.png', array('width' => 16, 'height' => 16)); ?>
                                    </a>
                                    <?php
                                }
                            }
                            else
                            {
                                echo $status[$item->status];
                            }
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        echo $value['stock_quantity'];
                        ?>
                    </td>
                </tr>
                <?php
                $count++;
            endforeach;
        endif;
        ?>

    <?php endforeach; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="6" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td colspan="6" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<a href="#" class="export_table btn btn-default btn-sm" data-href="<?php echo \Uri::create('admin/reports/export_products').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>