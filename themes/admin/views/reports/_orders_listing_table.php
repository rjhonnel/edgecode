<?php
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
$margin = \Str::alternator('m_r_15', 'm_r_15', 'm_r_15', '');
?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <?php echo \Form::label('Order Period', null, array('class' => 'dateLabel')); ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_from', \Input::get('date_from'), array('class' => 'form-control from_white dateInput', 'placeholder' => 'From')); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'form-control  to_white dateInput', 'placeholder' => 'To')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo \Form::label('Order Status'); ?>
            <?php echo \Form::select('status', \Input::get('status'), array('false' => 'Select') + \Config::get('order.status', array()), array('class' => 'form-control')); ?>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="form-group">
            <label for="">&nbsp;</label>
            <ul class="list-inline" style="float: none;">
                <li>
                    <?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary')); ?>
                </li>
                <li>
                    <a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php echo \Form::close(); ?>

<table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Order Date</th>
        <th scope="col">Client ID</th>
        <th scope="col">Client Name</th>
        <th scope="col">Order Amount</th>
        <th scope="col">Order Status</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>

        <tr id="sort_<?php echo $item->id . '_' . $item->sort; ?>">
            <td><?php echo date('d/m/Y', $item->created_at).' '.date('H:i:s', $item->created_at); ?></td>
            <td><?php echo $item->user_id; ?></td>
            <td><?php echo $item->first_name . ' ' . $item->last_name; ?></td>
            <td>$<?php echo number_format(round($item->total_price(), 2), 2); ?></td>
            <td><?php echo Inflector::humanize($item->status); ?></td>
        </tr>

    <?php endforeach; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<a href="#" class="export_table btn btn-default btn-sm" data-href="<?php echo \Uri::create('admin/reports/export_orders').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>