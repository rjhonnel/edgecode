<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Reports');
		\Breadcrumb::set('Orders', 'admin/report/orders');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Orders Report</h4>
			</header>

			<?php
			// Load reports listing table
			echo \Theme::instance()->view('views/reports/_orders_listing_table',
				array(
					'items'			=> $items,
					'pagination'	=> $pagination,
				),
				false
			);
			?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>