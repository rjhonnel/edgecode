<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Reports');
		\Breadcrumb::set('Customers', 'admin/report/customers');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Customers Report</h4>

				<div class="pull-right"></div>
			</header>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
			<?php
			// Load reports listing table
			echo \Theme::instance()->view('views/reports/_customers_listing_table',
				array(
					'items'			=> $items,
					'pagination'	=> $pagination,
				),
				false
			);
			?>
			<?php echo \Form::close(); ?>
		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>