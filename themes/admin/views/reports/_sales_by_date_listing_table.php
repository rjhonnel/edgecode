<?php
    $settings = \Config::load('autoresponder.db');
    $hold_gst = (isset($settings['gst']) ? 1 : 0);
    $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
    echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
?>
<div class="row">
    <div class="col-sm-5">
        <div class="form-group">
            <?php echo \Form::label('Order Period', null, array('class' => 'dateLabel')); ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_from', \Input::get('date_from'), array('class' => 'form-control from_white dateInput', 'placeholder' => 'From')); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'form-control to_white dateInput', 'placeholder' => 'To')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="#">&nbsp;</label>
            <ul class="list-inline">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<?php echo \Form::close(); ?>
<table rel="<?php echo \Uri::create('admin/team/sort/team'); ?>" class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Date</th>
        <th scope="col">Number of Orders</th>
        <th scope="col">Total Sales (without gst)</th>
        <th scope="col">Total Sales (with gst)</th>
        <th scope="col">GST <?php echo $hold_gst_percentage; ?>% (Included)</th>
    </tr>
    </thead>
    <tbody>
    <?php if($items): ?>
        <?php foreach($items as $item): ?>
            <?php
                $number_of_orders = 0;
                $total_sales = 0;
                $gst = 0;

                $orders = \Order\Model_Order::find(function($query){
                    $query->where('finished', '1');
                    $query->order_by('delivery_datetime', 'asc');
                });

                if($orders)
                {
                    // convert format date to m/d/Y
                    $parts = explode('/', date('d/m/Y', strtotime($item['dt'])));
                    $value = $parts[1].'/'.$parts[0].'/'.$parts[2];
                    if($date = strtotime($value))
                    {
                        foreach($orders as $number => $ord)
                        {
                            if(strtotime(date('m/d/Y', strtotime($ord->delivery_datetime))) == $date)
                            {
                                $number_of_orders += 1;
                                $total_sales += $ord->total_price();
                            }
                        }
                        $gst = \Helper::calculateGST($total_sales, $hold_gst_percentage);
                    }
                }
            ?>
            <tr>
                <td><?php echo date('d/m/Y', strtotime($item['dt'])); ?></td>
                <td><?php echo $number_of_orders; ?></td>
                <td>$<?php echo nf($total_sales - $gst); ?></td>
                <td>$<?php echo nf($total_sales); ?></td>
                <td>$<?php echo nf($gst); ?></td>
            </tr>
        <?php endforeach; ?>
    <?php endif; ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td colspan="5" class="center"><strong>There are no items.</strong></td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>

<a href="#" class="btn btn-default btn-sm export_table" data-href="<?php echo \Uri::create('admin/reports/export_sales_by_day').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>