<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Reports');
        \Breadcrumb::set('Print', 'admin/report/print');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Print Report</h4>
            </header>

            <?php
            echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form','target' => 'blank'));
            $status = array(
                'false' => 'All',
                '1' => 'Active',
                '0' => 'Inactive',
            );
            ?>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo \Form::label('Delivery Period', null, array('class' => 'dateLabel')); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="datepicker-holder-control">
                                    <?php echo \Form::input('date_from', \Input::get('date_from'), array('class' => 'form-control from_white dateInput', 'placeholder' => 'From')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="datepicker-holder-control">
                                    <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'form-control  to_white dateInput', 'placeholder' => 'To')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?php
                        $customer_list = \SentryAdmin::user()->all('front');
                        $customer_id_list = array();
                        foreach ($customer_list as $customer) {
                            $meta_data = \SentryAdmin::user((int)$customer['id'])['metadata'];
                            $customer_id_list += array((int)$customer['id'] => $meta_data['first_name'].' '.$meta_data['last_name']);
                        }
                        echo \Form::label('Customer');
                        echo \Form::select('user_id', \Input::get('user_id'), array('' => 'Select Customer','all' => 'All') + $customer_id_list, array('class' => 'form-control'));
                        ?>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <?php
                        echo \Form::label('Type');
                        echo \Form::select('type', \Input::get('type'), array('order_notes' => 'Order Notes','delivery_notes' => 'Delivery Notes'), array('class' => 'form-control'));
                        ?>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="">&nbsp;</label>
                        <ul class="list-inline" style="margin:0;">
                            <li><?php echo \Form::submit('search', 'Print', array('class' => 'btn btn-primary btn-sm')); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
          


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>