<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Reports');
		\Breadcrumb::set('Orders x Products', 'admin/report/orders_products');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Orders x Products Report</h4>
			</header>

			<?php
			// Load reports listing table
			echo \Theme::instance()->view('views/reports/_orders_products_listing_table',
				array(
					'items'				=> $items,
					'parent_categories'	=> $parent_categories,
					'pagination'		=> $pagination,
				),
				false
			);
			?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>