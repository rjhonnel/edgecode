<?php
$settings = \Config::load('autoresponder.db');
$hold_gst = (isset($settings['gst']) ? 1 : 0);
$hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get', 'class' => 'filter_form'));
?>
<div class="row">
    <div class="col-sm-5">
        <div class="form-group">
            <?php echo \Form::label('Order Period', null, array('class' => 'dateLabel')); ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_from', \Input::get('date_from'), array('class' => 'form-control from_white dateInput', 'placeholder' => 'From')); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="datepicker-holder-control">
                        <?php echo \Form::input('date_to', \Input::get('date_to'), array('class' => 'form-control to_white dateInput', 'placeholder' => 'To')); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <?php
            $customer_list = \SentryAdmin::user()->all('front');
            $customer_id_list = array();
            foreach ($customer_list as $customer) {
                $meta_data = \SentryAdmin::user((int)$customer['id'])['metadata'];
                $customer_id_list += array((int)$customer['id'] => $meta_data['first_name'].' '.$meta_data['last_name']);
            }
            echo \Form::label('Customer');
            echo \Form::select('user_id', \Input::get('user_id'), array('' => 'All Customer') + $customer_id_list, array('class' => 'form-control'));
            ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <label for="#">&nbsp;</label>
            <ul class="list-inline">
                <li><?php echo \Form::submit('search', 'Filter', array('class' => 'btn btn-primary btn-sm')); ?></li>
                <li><a class="btn btn-sm btn-default filters-reset" href="<?php echo \Uri::create(\Uri::admin('string')); ?>"><i class="fa fa-refresh"></i></a></li>
            </ul>
        </div>
    </div>
</div>
<?php echo \Form::close(); ?>
<div class="panel panel-default">
    <div class="chart">
        <div class="panel-heading clearfix">Statistics</div>
        <div class="panel-body clearfix"></div>
    </div>
</div>
<table class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Date</th>
        <th scope="col">Number of Orders</th>
        <th scope="col">Total Sales (without gst)</th>
        <th scope="col">Total Sales (with gst)</th>
        <th scope="col">GST <?php echo $hold_gst_percentage; ?>% (Included)</th>
    </tr>
    </thead>
    <tbody>
    <?php

        $labels = "";
        $data = "";
        if(count($items) > 0) {
            foreach ($items as $yr => $itm_val){
                $orders_num = 0;
                $without_gst = 0;
                $amount = 0;
                $gst = 0;
                $quarters['months'] = array(
                    1 => array(1=>'January',2=>'February',3=>'March'),
                    2 => array(1=>'April', 2=>'May', 3=>'June'),
                    3 => array(1=>'July', 2=>'August', 3=>'September',),
                    4 => array(1=>'October', 2=>'November', 3=>'December')

                );
                $month_count = 0;
                foreach ($itm_val as $item) {
                    $month_count++;
                    $comma = ($labels != "")?',':'';
                    $labels .= $comma."'".date('M', strtotime($item['month']['name'])).' '.$yr."'";
                    $comma2 = ($data != "")?',':'';
                    $data .= $comma2.str_replace(',','',$item['month']['without_gst']);
                    echo "<tr>
                               <td>" . $item['month']['name'] . "</td>
                               <td>" . $item['num_orders'] . "</td>
                               <td>$" . number_format(floatval(str_replace(',', '', $item['month']['without_gst'])), 2) . "</td>
                               <td>$" . number_format(floatval(str_replace(',', '', $item['month']['amount'])), 2) . "</td>
                               <td>$" . number_format(floatval(str_replace(',', '', $item['month']['gst'])), 2) . "</td>                        
                        </tr>";

                    $orders_num += str_replace(',', '', $item['num_orders']);
                    $without_gst += str_replace(',', '', $item['month']['without_gst']);
                    $amount += str_replace(',', '', $item['month']['amount']);
                    $gst += str_replace(',', '', $item['month']['gst']);

                    $quarter_key ='';

                    $q1 = array_column($quarters, 1);
                    $q2 = array_column($quarters, 2);
                    $q3 = array_column($quarters, 3);
                    $q4 = array_column($quarters, 4);
                    $quarter_key = (array_search($item['month']['name'], $q1[0]))?1:$quarter_key;
                    $quarter_key = (array_search($item['month']['name'], $q2[0]))?2:$quarter_key;
                    $quarter_key = (array_search($item['month']['name'], $q3[0]))?3:$quarter_key;
                    $quarter_key = (array_search($item['month']['name'], $q4[0]))?4:$quarter_key;

                    if (count($itm_val) == $month_count OR $item['month']['name'] == end($q1[0]) OR $item['month']['name'] == end($q2[0]) OR $item['month']['name'] == end($q3[0]) OR $item['month']['name'] == end($q4[0])) {
                        echo "<tr style='font-weight: bold;'>
                                <td>Total for Q" . $quarter_key ." ".$yr."</td>
                                   <td>" . $orders_num ."</td>
                                   <td>$" . number_format(floatval($without_gst), 2) . "</td>
                                   <td>$" . number_format(floatval($amount), 2) . "</td>
                                   <td>$" . number_format(floatval($gst), 2) . "</td>                        
                            </tr>";
                        $orders_num = 0;
                        $without_gst = 0;
                        $amount = 0;
                        $gst = 0;
                        $nn=0;
                    }
                }
            }
        }

    ?>

    <?php if(!\Input::get()): ?>
        <tr class="nodrag nodrop">
            <td class="center" colspan="5"><strong>Select option first.</strong></td>
        </tr>
    <?php elseif(empty($items)): ?>
        <tr class="nodrag nodrop">
            <td class="center" colspan="5"><strong>There are no orders.</strong></td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>

<a href="#" class="btn btn-default btn-sm export_table" data-href="<?php echo \Uri::create('admin/reports/export_sales_by_month_report').($_SERVER['QUERY_STRING']?'?'.$_SERVER['QUERY_STRING']:''); ?>">Export to CSV</a>

<?php echo \Theme::instance()->asset->js('plugins/Chart.min.js'); ?>
<script>
    <?php
        $date_ranges = '';
        if(\Input::get('date_from') AND \Input::get('date_to')) {
            $date_from = \DateTime::createFromFormat('d/m/Y', \Input::get('date_from'));
            $date_to = \DateTime::createFromFormat('d/m/Y', \Input::get('date_to'));
            $date_ranges = $date_from->format('F d, Y').' to '.$date_to->format('F d, Y');
        }
    ?>
    var ctx = document.getElementById("barchart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [<?php echo $labels; ?>],
            datasets: [{
                label: 'Sales by Month from <?php echo $date_ranges; ?>',
                data: [<?php echo $data; ?>],
                backgroundColor: '#e4f3fc',
                borderColor: '#53b0ef',
                borderWidth: 1
        }]
        }
    });
</script>