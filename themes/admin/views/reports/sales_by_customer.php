<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Reports');
		\Breadcrumb::set('Sales x Customer', 'admin/report/sales_by_customer');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Sales x Customer Report</h4>
			</header>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
			<?php
			// Load reports listing table
			echo \Theme::instance()->view('views/reports/_sales_by_customer_listing_table',
				array(
					'sales_customer'		=> $sales_customer,
				),
				false
			);
			?>
			<?php echo \Form::close(); ?>
		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
