<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=320, target-densitydpi=device-dpi">
        <style type="text/css">
            body { width: 100% !important; font-size: 12px; font-weight: normal;}
            body { background-color: #fff; margin: 0; padding: 0; }
            img { outline: none; text-decoration: none; display: block;}
            body, td { font-family: Arial, Helvetica, sans-serif; color: #000; text-decoration: none; font-size: 12px; font-weight: normal;}
            a{
                color: #000; font-weight:bold; text-decoration:none;
            }
        </style>
    </head>
    <body>
        <?php $settings = \Config::load('autoresponder.db'); ?>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td align="center" bgcolor="#fff">
                        <table style="margin:0 10px;" width="640" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr><td  width="640" height="20"></td></tr>
                                <?php $hold_logo_url = (isset($settings['logo_url']) ? $settings['logo_url'] : false); ?>
                                <?php if($hold_logo_url): ?>
                                    <tr>
                                        <td width="640" align="center" bgcolor="#fff">
                                            <table width="640" cellpadding="0" cellspacing="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td width="320">
                                                            <a href="<?php echo \Uri::create('/') ?>">
                                                                <img src="<?php echo \Uri::create('media/images/' . $hold_logo_url); ?>">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr><td width="320" height="15"></td><td width="320"></td></tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                <?php endif; ?>

                                <tr><td width="640" height="30" bgcolor="#ffffff"></td></tr>
                                <tr>
                                    <td width="640" bgcolor="#ffffff">
                                        <?php
                                            if(isset($message)):
                                                echo nl2br($message);
                                        ?>
                                        <?php else: ?>
                                            Dear <?php echo $order->first_name.' '.$order->last_name; ?>, <br/>
                                            <br/>
                                            See attached in PDF your (<?php echo strtolower($type); ?>). <br/>
                                            <br/>
                                            Should you have any questions, please do not hesitate to contact us. <br/>
                                            <br/>
                                            <br/>
                                            Kind regards<br/>
                                            <br/>
                                            <strong><?php echo $settings['company_name']; ?></strong><br/>
                                            <?php echo $settings['phone']; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <tr><td width="640" height="15" style="border-bottom: 1px solid #dddddd;"></td></tr>
                                <tr><td  width="640" height="15" bgcolor="#ffffff"></td></tr>
                                <tr><td  width="640" height="15" bgcolor="#fff"></td></tr>

                                <tr>
                                    <td width="640">
                                        <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#fff">
                                            <tbody>
                                                <tr>
                                                    <td width="320">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <?php if($hold_logo_url): ?>
                                                                        <td width="70">
                                                                            <a href="<?php echo \Uri::create('/') ?>">
                                                                                <img src="<?php echo \Uri::create('media/images/' . $hold_logo_url); ?>" >
                                                                            </a>
                                                                        </td>
                                                                    <?php endif; ?>
                                                                    <td>
                                                                        <?php echo $settings['address']; ?><br>
                                                                        <a href="<?php echo \Uri::create('/') ?>"><?php echo $settings['website']; ?></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>