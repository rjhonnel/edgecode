<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=320, target-densitydpi=device-dpi">
        <style type="text/css">
            body { width: 100% !important; font-size: 12px; font-weight: normal;}
            body { background-color: #fff; margin: 0; padding: 0; }
            img { outline: none; text-decoration: none; display: block;}
            body, td { font-family: Arial, Helvetica, sans-serif; color: #000; text-decoration: none; font-size: 12px; font-weight: normal;}
            a{
                color: #000; font-weight:bold; text-decoration:none;
            }
        </style>
    </head>
    <body>
        <?php $settings = \Config::load('autoresponder.db'); ?>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tbody>
                <tr>
                    <td align="center" bgcolor="#fff">
                        <table style="margin:0 10px;" width="640" cellpadding="0" cellspacing="0" border="0">
                            <tbody>
                                <tr><td  width="640" height="20"></td></tr>
                                <?php $hold_logo_url = (isset($settings['logo_url']) ? $settings['logo_url'] : false); ?>
                                <?php if($hold_logo_url): ?>
                                    <tr>
                                        <td width="640" align="center" bgcolor="#fff">
                                            <table width="640" cellpadding="0" cellspacing="0" border="0">
                                                <tbody>
                                                    <tr>
                                                        <td width="320">
                                                            <a href="<?php echo \Uri::create('/') ?>">
                                                                <img src="<?php echo \Uri::create('media/images/' . $hold_logo_url); ?>">
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr><td width="320" height="15"></td><td width="320"></td></tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                <?php endif; ?>

                                <tr><td width="640" height="30" bgcolor="#ffffff"></td></tr>
                                <tr>
                                    <td width="640" bgcolor="#ffffff">
                                        <table width="640" cellpadding="0" cellspacing="0" border="0">
                                            <tbody>
                                                <tr>
                                                    <td width="30"></td>
                                                    <td width="580">
                                                        <table width="580" cellpadding="0" cellspacing="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="580">
                                                                        <?php if(isset($content) && !empty($content)): ?>

                                                                            <?php 
                                                                            if(isset($content['content']) && !empty($content['content'])):
                                                                            $data = $content['content'];   
                                                                            ?>
                                                                        
                                                                            <h1 style="font-weight: normal; margin: 0; padding: 0 0 10px 0; border-bottom: 1px solid #dddddd;">
                                                                                Order Confirmation <?php echo $data->id; ?>
                                                                            </h1>
                                                                                <p align="left">
                                                                                    
                                                                                    <span style="font-weight: bold; font-size: 14px;">Order Date: <?php echo date('d/m/Y H:i', time()); ?></span><br><br>

                                                                                    <span style="font-weight: bold; font-size: 17px;">Order Number: <?php echo $data->id; ?></span><br><br>

                                                                                    Hi <?php echo $data->first_name; ?> <?php echo $data->last_name; ?>,<br><br>
                                                                                    
                                                                                    Thank you for your order! <br /><br />
                                                                                    
                                                                                    <?php if(!empty($data->products)): ?>
                                                                                    
                                                                                        <table width="100%" cellpadding="10" cellspacing="0" border="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th align="left" style="border: 1px solid #dadada; background: #f2f2f2; font-weight: bold; text-transform: uppercase; font-size: 14px;">
                                                                                                        code
                                                                                                    </th>
                                                                                                    <th align="left" style="border: 1px solid #dadada; background: #f2f2f2; font-weight: bold; text-transform: uppercase; font-size: 14px;">
                                                                                                        product
                                                                                                    </th>
                                                                                                    <th align="left" style="border: 1px solid #dadada; background: #f2f2f2; font-weight: bold; text-transform: uppercase; font-size: 14px;">
                                                                                                        category
                                                                                                    </th>
                                                                                                    <th align="left" style="border: 1px solid #dadada; background: #f2f2f2; font-weight: bold; text-transform: uppercase; font-size: 14px;">
                                                                                                        unit price
                                                                                                    </th>
                                                                                                    <th align="left" style="border: 1px solid #dadada; background: #f2f2f2; font-weight: bold; text-transform: uppercase; font-size: 14px;">
                                                                                                        delivery time
                                                                                                    </th>
                                                                                                    <th align="left" style="border: 1px solid #dadada; background: #f2f2f2; font-weight: bold; text-transform: uppercase; font-size: 14px;">
                                                                                                        qty
                                                                                                    </th>
                                                                                                    <th align="left" style="border: 1px solid #dadada; background: #f2f2f2; font-weight: bold; text-transform: uppercase; font-size: 14px;">
                                                                                                        total price
                                                                                                    </th>
                                                                                                </tr>
                                                                                                    
                                                                                                <?php 
                                                                                                $arrCodes = array();
                                                                                                foreach($data->products as $product): 

                                                                                                   // if ( !in_array( $product->code, $arrCodes ) ):
                                                                                                       // $arrCodes[] = $product->code; ?>
                                                                                                    <tr>
                                                                                                        <td style="border: 1px solid #dadada;"><?php echo $product->code; ?></td>
                                                                                                        <td style="border: 1px solid #dadada;"><?php echo $product->title; ?></td>
                                                                                                        <td style="border: 1px solid #dadada;"><?php echo $product->product_category; ?></td>
                                                                                                        <td style="border: 1px solid #dadada;">$<?php echo number_format($product->price, 2); ?></td>
                                                                                                        <td style="border: 1px solid #dadada;">
                                                                                                        <?php echo $data->delivery_datetime_list?(date('h:i a', strtotime($product->delivery_time))):($data->delivery_datetime?(date('h:i a', strtotime($data->delivery_datetime))):''); ?>    
                                                                                                        </td>
                                                                                                        <td style="border: 1px solid #dadada;"><?php echo $product->quantity; ?></td>
                                                                                                        <td style="border: 1px solid #dadada;">$<?php echo number_format(($product->quantity * $product->price), 2); ?></td>
                                                                                                    </tr>
                                                                                                <?php 
                                                                                                    //endif;
                                                                                                endforeach; ?>
                                                                                            </tbody>
                                                                                        </table>

                                                                                        <br><br>
                                                                                        <?php $a_order = $data->to_array(); ?>
                                                                                        <?php $total = \Helper::total_price(array($a_order['total_price'], $a_order['shipping_price'])); ?>
                                                                                        <table align="right" cellpadding="3" cellspacing="0" border="0">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <td align="right" style="font-weight: bold; font-size: 14px;">Products Total</td>
                                                                                                    <td align="right" style="font-size: 14px;">$<?php echo number_format($a_order['total_price'], 2); ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="right" style="font-weight: bold; font-size: 14px;">Shipping</td>
                                                                                                    <td align="right" style="font-size: 14px;">$<?php echo number_format($a_order['shipping_price'], 2); ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="right" style="font-weight: bold; font-size: 14px;">Extra Delivery Charge</td>
                                                                                                    <td align="right" style="font-size: 14px;">$<?php echo number_format($a_order['total_delivery_charge'], 2); ?></td>
                                                                                                </tr>
                                                                                                <tr>
                                                                                                    <td align="right" style="font-weight: bold; font-size: 14px;">Discount</td>
                                                                                                    <td align="right" style="font-size: 14px;">-$<?php echo number_format($a_order['discount_amount'], 2); ?></td>
                                                                                                </tr>
                                                                                                <?php 
                                                                                                    $hold_gst = (isset($settings['gst']) ? 1 : 0);
                                                                                                    $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

                                                                                                    $grand_total = $a_order['total_price'] - $a_order['discount_amount'] + $a_order['shipping_price'] + $a_order['total_delivery_charge'];
                                                                                                ?>
                                                                                                <tr>
                                                                                                    <td align="right" style="font-weight: bold; font-size: 14px;">Grand Total</td>
                                                                                                    <td align="right" style="font-size: 14px;">$<?php echo number_format($grand_total, 2); ?></td>
                                                                                                </tr>
                                                                                                <?php if($hold_gst && $hold_gst_percentage > 0): ?>
                                                                                                    <tr>
                                                                                                        <td align="right" style="font-weight: bold; font-size: 14px;">GST Tax <?php echo $hold_gst_percentage; ?>%   (Included)</td>
                                                                                                        <td align="right" style="font-size: 14px;">$<?php echo number_format(\Helper::calculateGST($grand_total, $hold_gst_percentage), 2); ?></td>
                                                                                                    </tr>
                                                                                                <?php endif;?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    
                                                                                    <?php endif; ?>

                                                                                </p>
                                                                            
                                                                            <?php endif; ?>
                                                                        <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr><td width="580" height="10"></td></tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="30"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr><td width="640" height="15" style="border-bottom: 1px solid #dddddd;"></td></tr>
                                <tr><td  width="640" height="15" bgcolor="#ffffff"></td></tr>
                                <tr><td  width="640" height="15" bgcolor="#fff"></td></tr>

                                <tr>
                                    <td width="640">
                                        <table width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#fff">
                                            <tbody>
                                                <tr>
                                                    <td width="320">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <?php if($hold_logo_url): ?>
                                                                        <td width="70">
                                                                            <a href="<?php echo \Uri::create('/') ?>">
                                                                                <img src="<?php echo \Uri::create('media/images/' . $hold_logo_url); ?>" >
                                                                            </a>
                                                                        </td>
                                                                    <?php endif; ?>
                                                                    <td>
                                                                        <?php echo $settings['address']; ?><br>
                                                                        <a href="<?php echo \Uri::create('/') ?>"><?php echo $settings['website']; ?></a>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>