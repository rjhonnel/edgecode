<div class="panel panel-default">
    <div class="panel-body form-horizontal">

        <div class="row">
            <div class="col-sm-6">
                <div class="clearfix">
                    <label class="col-sm-3 control-label">Customer Group:</label>
                    <div class="col-sm-9"><?php echo \Form::select('user_group', \Input::post('user_group', isset($user) ? $user->group['id'] : ''), \User\Model_Group::fetch_pair('id', 'name', array(), false, $groups), array('class' => 'form-control')); ?></div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="clearfix">
                    <?php echo \Form::hidden('activated', 0); ?>
                    <label><?php echo \Form::checkbox('activated', 1, \Input::post('activated', isset($user) ? $user->get('activated') : '')); ?> Activate</label>
                    &nbsp;&nbsp;
                    <?php echo \Form::hidden('master', 0); ?>
                    <label><?php echo \Form::checkbox('master', 1, \Input::post('master', isset($user) ? $user->get('metadata.master') : '')); ?> Master User</label>
                </div>
            </div>
        </div>

    </div>
</div>



<div class="panel panel-default panel-col2">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-6"><h3 class="panel-title">Billing Information</h3></div>
            <div class="col-sm-6">
                <h3 class="panel-title pull-left">Shipping Information</h3>
                <label class="pull-right"><input type="checkbox" id="same_billing_id"> Same as Billing</label>
            </div>
        </div>
    </div>
    <div class="panel-body form-horizontal">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Salutation:</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php echo \Form::input('title', \Input::post('title', isset($user) ? $user->get('metadata.title') : ''), array('class'=>'form-control')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Name: <span class="text-danger">*</span></label>
                    <div class="col-sm-9">

                        <div class="row">
                            <div class="col-sm-6">
                                <?php echo \Form::input('first_name', \Input::post('first_name', isset($user) ? $user->get('metadata.first_name') : ''), array('class'=>'form-control', 'placeholder'=>'First Name')); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php echo \Form::input('last_name', \Input::post('last_name', isset($user) ? $user->get('metadata.last_name') : ''), array('class'=>'form-control', 'placeholder'=>'Last Name')); ?>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Name:</label>
                    <div class="col-sm-9">
                        <label class="control-label" style="padding-bottom: 10px;"><input type="checkbox" name="on_account" id="form_on_account" <?php echo isset($user) ? ($user->get('metadata.on_account') == true)?"checked=checked":'' : ''?>/> Choose company list.</label>
                        <select name="company" class="form-control" id="form_company">
                            <option value="" disabled>-- please select --</option>   
                            <?php if($companies): ?>
                                <?php foreach($companies as $company):?> 
                                <option value="<?php echo $company->id?>" <?php echo isset($user) ? ($user->get('metadata.company') == $company->id)?"selected=selected":'' : ''?>><?php echo $company->company_name;?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                        <input type="hidden" name="business_name" id="form_business_name" value="<?php echo isset($user) ? $user->get('metadata.business_name'): '';?>"> 
                        <input type="text" name="business_name2" id="form_business_name2" class="form-control" value="<?php echo isset($user) ? $user->get('metadata.business_name'): '';?>">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Title:</label>
                    <div class="col-sm-9"><?php echo \Form::input('job_title', \Input::post('job_title', isset($user) ? $user->get('metadata.job_title') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address:  <span class="text-danger">*</span></label>
                    <div class="col-sm-9"><?php echo \Form::input('address', \Input::post('address', isset($user) ? $user->get('metadata.address') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address 2:</label>
                    <div class="col-sm-9"><?php echo \Form::input('address2', \Input::post('address2', isset($user) ? $user->get('metadata.address2') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Suburb:  <span class="text-danger">*</span></label>
                    <div class="col-sm-9"><?php echo \Form::input('suburb', \Input::post('suburb', isset($user) ? $user->get('metadata.suburb') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">State:  <span class="text-danger">*</span></label>
                    <div class="col-sm-9">


                        <div class="row">
                            <div class="col-sm-6">
                                <?php if(!\App\States::forge()->getStateProvinceArray(\Input::post('country', 'AU'))): ?>
                                    <?php echo \Form::input('state', \Input::post('state', isset($user) ? $user->get('metadata.state') : ''), array('class' => 'state_select state_input')); ?>
                                <?php else: ?>
                                    <?php echo \Form::select('state', \Input::post('state', isset($user) ? $user->get('metadata.state') : ''), \App\States::forge()->getStateProvinceArray(\Input::post('country', 'AU')), array('class' => 'form-control state_select')); ?>
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-6">
                                <?php echo \Form::input('postcode', \Input::post('postcode', isset($user) ? $user->get('metadata.postcode') : ''), array('class' => 'form-control', 'placeholder' => 'Postcode')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Country:  <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <?php echo \Form::select('country', \Input::post('country', isset($user) ? $user->get('metadata.country') : ''), \App\Countries::forge()->getCountries(), array('class' => 'form-control two_col_wide_input country_select')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Phone:  <span class="text-danger">*</span></label>
                    <div class="col-sm-9"><?php echo \Form::input('phone', \Input::post('phone', isset($user) ? $user->get('metadata.phone') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Direct Phone:</label>
                    <div class="col-sm-9"><?php echo \Form::input('direct_phone', \Input::post('direct_phone', isset($user) ? $user->get('metadata.direct_phone') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mobile:</label>
                    <div class="col-sm-9"><?php echo \Form::input('mobile', \Input::post('mobile', isset($user) ? $user->get('metadata.mobile') : ''), array('class'=>'form-control')); ?></div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Salutation:</label>
                    <div class="col-sm-9">

                        <div class="row">
                            <div class="col-sm-6"><?php echo \Form::input('shipping_title', \Input::post('shipping_title', isset($user) ? $user->get('metadata.shipping_title') : ''), array('class'=>'form-control')); ?></div>
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Name:</label>
                    <div class="col-sm-9">

                        <div class="row">
                            <div class="col-sm-6">
                                <?php echo \Form::input('shipping_first_name', \Input::post('shipping_first_name', isset($user) ? $user->get('metadata.shipping_first_name') : ''), array('class'=>'form-control', 'placeholder'=>'First name')); ?>
                            </div>
                            <div class="col-sm-6">
                                <?php echo \Form::input('shipping_last_name', \Input::post('shipping_last_name', isset($user) ? $user->get('metadata.shipping_last_name') : ''), array('class'=>'form-control','placeholder'=>'Last name')); ?>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Name:</label>
                    <div class="col-sm-9">
                    <label class="control-label" style="padding-bottom: 10px;"><input type="checkbox" name="on_shipp_account" id="form_on_shipp_account" <?php echo isset($user) ? ($user->get('metadata.on_shipp_account') == true)?"checked=checked":'' : ''?>/> Choose company list.</label>
                    <select name="shipping_company" class="form-control" id="form_shipping_company">
                            <option value="" disabled>-- please select --</option> 
                            <?php if($companies): ?>
                                <?php foreach($companies as $company):?>
                                <option value="<?php echo $company->id?>"  <?php echo isset($user) ? ($user->get('metadata.shipping_company') == $company->id)?"selected=selected":'' : ''?>><?php echo $company->company_name;?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        </select>
                    <input type="hidden" name="shipping_business_name" id="form_shipping_business_name" value="<?php echo isset($user) ? $user->get('metadata.shipping_business_name'): '';?>">  
                    <input type="text" name="shipping_business_name2" id="form_shipping_business_name2" class="form-control" value="<?php echo isset($user) ? $user->get('metadata.shipping_business_name'): '';?>">     
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Title:</label>
                    <div class="col-sm-9"><?php echo \Form::input('shipping_job_title', \Input::post('shipping_job_title', isset($user) ? $user->get('metadata.shipping_job_title') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address:</label>
                    <div class="col-sm-9"><?php echo \Form::input('shipping_address', \Input::post('shipping_address', isset($user) ? $user->get('metadata.shipping_address') : ''), array('class'=>'form-control')); ?></div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label">Address 2:</label>
                    <div class="col-sm-9"><?php echo \Form::input('shipping_address2', \Input::post('shipping_address2', isset($user) ? $user->get('metadata.shipping_address2') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Suburb:</label>
                    <div class="col-sm-9"><?php echo \Form::input('shipping_suburb', \Input::post('shipping_suburb', isset($user) ? $user->get('metadata.shipping_suburb') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">State:</label>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="col-sm-6">
                                <?php if(!\App\States::forge()->getStateProvinceArray(\Input::post('shipping_country', 'AU'))): ?>
                                    <?php echo \Form::input('shipping_state', \Input::post('shipping_state', isset($user) ? $user->get('metadata.shipping_state') : ''), array('class' => 'form-control state_input')); ?>
                                <?php else: ?>
                                    <?php echo \Form::select('shipping_state', \Input::post('shipping_state', isset($user) ? $user->get('metadata.shipping_state') : ''), \App\States::forge()->getStateProvinceArray(\Input::post('shipping_country', 'AU')), array('class' => 'form-control state_select')); ?>
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-6">
                                <?php echo \Form::input('shipping_postcode', \Input::post('shipping_postcode', isset($user) ? $user->get('metadata.shipping_postcode') : ''), array('class' => 'form-control', 'placeholder' => 'Postcode')); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Country:</label>
                    <div class="col-sm-9">
                        <?php echo \Form::select('shipping_country', \Input::post('shipping_country', isset($user) ? $user->get('metadata.shipping_country') : ''), \App\Countries::forge()->getCountries(), array('class' => 'form-control two_col_wide_input country_select')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Phone:</label>
                    <div class="col-sm-9"><?php echo \Form::input('shipping_phone', \Input::post('shipping_phone', isset($user) ? $user->get('metadata.shipping_phone') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Direct Phone:</label>
                    <div class="col-sm-9"><?php echo \Form::input('shipping_direct_phone', \Input::post('shipping_direct_phone', isset($user) ? $user->get('metadata.shipping_direct_phone') : ''), array('class'=>'form-control')); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Mobile:</label>
                    <div class="col-sm-9"><?php echo \Form::input('shipping_mobile', \Input::post('shipping_mobile', isset($user) ? $user->get('metadata.shipping_mobile') : ''), array('class'=>'form-control')); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-default panel-col2">
    <div class="panel-heading">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="panel-title pull-left">Credit Account Status</h3>
                <?php echo \Form::hidden('credit_account', 0); ?>
                <label class="pull-right"><?php echo \Form::checkbox('credit_account', 1, \Input::post('credit_account', isset($user) ? $user->get('metadata.credit_account') : '')); ?> Account Authorise</label>
            </div>
            <div class="col-sm-6"><h3 class="panel-title">Login Information</h3></div>
        </div>
    </div>
    <div class="panel-body form-horizontal">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">

                    <label class="col-sm-3 control-label">Purchase Limit:</label>

                    <div class="col-sm-9">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <?php echo \Form::input('purchase_limit_value', \Input::post('purchase_limit_value', isset($user) ? $user->get('metadata.purchase_limit_value') : ''), array('class' => 'form-control')); ?>
                        </div>
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Over Period:</label>
                    <div class="col-sm-9">
                        <?php echo \Form::select('purchase_limit_period', \Input::post('purchase_limit_period', isset($user) ? $user->get('metadata.purchase_limit_period') : ''), array(0 => 'Disabled') + \Helper::numbers_array(1, 12, 'asc', 0, false, 'Months'), array('class' => 'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">CC Email for Invoice: </label>
                    <div class="col-sm-9">
                        <?php echo \Form::input('cc_email', \Input::post('cc_email', isset($user) ? $user->get('metadata.cc_email') : ''), array('class'=>'form-control')); ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">

                <div class="form-group">
                    <label class="col-sm-3 control-label">Email:  <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <?php echo \Form::input('email', \Input::post('email', isset($user) ? $user->get('email') : ''), array('class'=>'form-control')); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Password:</label>
                    <div class="col-sm-9">
                        <?php echo \Form::password('password', \Input::post('password'), array('class'=>'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Confirm Password:</label>
                    <div class="col-sm-9">
                        <?php echo \Form::password('confirm_password', \Input::post('confirm_password'), array('class'=>'form-control')); ?>
                    </div>
                </div>
                <div class="form-group">

                    <label class="col-sm-3 control-label">&nbsp;</label>
                    <div class="col-sm-9">
                        <label>
                            <?php echo \Form::hidden('email_client', 0); ?>
                            <?php echo \Form::checkbox('email_client', 1, \Input::post('email_client')); ?>
                            Email New Password to Customer?
                        </label>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <?php if(isset($user)): ?>
        <div class="panel-footer clearfix">
            <div class="form-group pull-left m-0">
                Account Created: <label><span class="text-primary"><?php echo date(\Config::get('date.patterns.us'), $user->created_at); ?></span></label>
            </div>
            <div class="form-group pull-right m-0">
                Last Logged in: <label><span><?php echo $user->last_login > 0 ? date(\Config::get('date.patterns.us'), $user->last_login) : 'Never'; ?></span></label>
            </div>
        </div>
    <?php endif; ?>
</div>
