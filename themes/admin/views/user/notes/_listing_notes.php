<div class="panel-body">
    <div class="notes">
        <?php if($notes): ?>
            <?php foreach($notes as $note): ?>
                <div class="note">
                    <div class="note-indicator"></div>
                    <div class="note-item-content">
                        <div class="note-content" style="white-space: pre-wrap;"><?php echo $note->note; ?></div>
                        <div class="note-footer">
                            <span class="note-updated"> <?php echo date('d/m/Y H:i:s', $note->created_at); ?> </span>
                            <span class="note-options"> <a href="javascript:void()" class="edit-note"> Edit </a> / <a class="text-danger confirmation-pop-up" href="<?php echo \Uri::create('admin/user/delete_notes/'.$user->get('id').'/'.$note->id); ?>" data-message="Are you sure you want to delete note?"> Delete </a> </span>
                        </div>
                    </div>
                    <div class="note-state-edit">
                        <div class="note-content">
                            <textarea class="form-control"><?php echo $note->note; ?></textarea>
                        </div>
                        <div class="note-footer">
                            <span class="note-updated"> <?php echo date('d/m/Y H:i:s', $note->created_at); ?> </span>
                            <span class="note-options"> <a href="javascript:void()" class="cancel-edit-note"> Cancel </a> / <a href="javascript:void()" class="update-edit-note" data-url="<?php echo \Uri::create('admin/user/update_notes/'.$user->get('id').'/'.$note->id); ?>"> Update </a> </span>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <small class="alert alert-info">There are no items.</small>
        <?php endif; ?>
    </div>

    <div class="pagination-holder">
        <?php echo $pagination->render(); ?>
    </div>
</div>


<div class="panel-footer">
    <?php echo \Form::open(array('action' => \Uri::create('admin/user/create_notes/'.$user->get('id')))); ?>
        <textarea rows="7" class="form-control" name="note" ></textarea>
        <div class="text-right">
            <button type="submit" class="btn btn-primary mt-5px"><i class="fa fa-plus"></i> Add note</button>
        </div>
    <?php echo \Form::close(); ?>
</div>