<div class="main-content">

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Customers');
        \Breadcrumb::set('Customer Manager', 'admin/user/list');
        \Breadcrumb::set('Notes');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner">

            <header class="main-content-heading">
                <h4 class="pull-left">Edit Customer: Notes</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/user/_action_links', array('hide_add_new' => 1)); ?>

                </div>
            </header>

            <?php echo \Theme::instance()->view('views/user/_navbar_links', array('user' => $user)); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Notes <?php echo isset($user) ? '- ' . $user->get('metadata.first_name') . ' ' . $user->get('metadata.last_name') : ''; ?></h3>

                    <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
                        <div class="form-inline pull-right">
                            <label>Show entries:</label>
                            <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                        </div>
                    <?php echo \Form::close(); ?>

                </div>
                <?php
                // Load page listing table
                echo \Theme::instance()->view('views/user/notes/_listing_notes',
                    array(
                        'pagination'    => $pagination,
                        'notes'         => $notes,
                        'user'          => $user,
                    ),
                    false
                );
                ?>
            </div>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>