<div class="action-list">
	<?php if(isset($login_as_customer)): ?>
    	<a id="loginAsCustomer" href="<?php echo \Uri::create('admin/user/login/'.$id); ?>" class="btn btn-sm btn-primary"><i class="fa fa-lock"></i> Login as Customer</a>
    <?php endif; ?>
    <?php if(isset($create_form)): ?>
        <a id="save_button_up" class="btn btn-sm btn-success"><i class="fa fa-edit"></i> Save</a>
    <?php endif; ?>
    <?php if(!isset($hide_add_new)): ?>
    	<a href="<?php echo \Uri::create('admin/user/create'); ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> New Customer</a>
    <?php endif; ?>
    <?php if(isset($show_bulk_add)): ?>
        <a href="<?php echo \Uri::create('admin/user/import'); ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Bulk Add</a>
    <?php endif; ?>
    <?php if(!isset($hide_show_all)): ?>
    	<a href="<?php echo \Uri::create('admin/user/list'); ?>" class="btn btn-sm btn-default"><i class="fa fa-list"></i> Show All</a>
    <?php endif; ?>
</div>