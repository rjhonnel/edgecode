<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
            \Breadcrumb::set('Home', 'admin/dashboard');
            \Breadcrumb::set('Customers');
            \Breadcrumb::set('Customer Manager', 'admin/user/list');

            // If viewing category products show category title
            if(isset($group))
                \Breadcrumb::set($group->get('name'));

            echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Customers</h4>

                <div class="pull-right">
                    <?php
                        if(isset($group))
                            echo \Theme::instance()->view('views/user/_action_links');
                        else
                            echo \Theme::instance()->view('views/user/_action_links', array('hide_show_all' => 1, 'show_bulk_add' => 1));
                    ?>
                </div>
            </header>

            <?php 
                if(isset($group))
                    echo \Theme::instance()->view('views/user/group/_navbar_links', array('group' => $group)); 
            ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>

            <?php
            // Load page listing table
            echo \Theme::instance()->view('views/user/_listing_table',
                array(
                    'pagination' 	=> $pagination,
                    'items'			=> $items,
                ),
                false
            );
            ?>
            <?php echo \Form::close(); ?>
        </div>
    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>