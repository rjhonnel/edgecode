<?php 
// Configs
$invoice = array('' => '-') + \Config::get('details.invoice', array());
$status = array('' => '-') + \Config::get('details.status', array());
$delivery = array('' => '-') + \Config::get('details.delivery', array());
?>								
<div class="panelContent">

    <?php 
        isset($filters) or $filters = true;
        
        if($filters)
        {
            echo \Theme::instance()->view('views/_partials/search_filters', array(
                'pagination' => $pagination,
                'module' => 'company and full',
                'options' => array(
                    'product_status',  
                    'product_category',
                    'accessories_date', 
                    'accessories_price', 
                    ),
            ), false);
        }
    ?>
    
    <table class="grid greyTable2 separated m_t_10" width="100%">
        <thead>
            <tr class="blueTableHead">
                <th scope="col" class="noresize">Code</th>
                <th scope="col">Product Name</th>
                <th scope="col" class="noresize">Date Added</th>
                <th scope="col" class="center" style="width: 40px;">Edit</th>
            </tr>
        </thead>
        <tbody>

            <?php // var_dump($items); ?>
            
            <?php foreach($items as $item): ?>

                <?php $item_info = \Order\Model_Order::order_info($item->id); ?>
                <tr>
                    <td><?php echo $item->purchase_number; ?></td>
                    <td>
                        <?php 
                            $names = array('Red Eleven Slim Fit Jacket', 'Bellfield Checked Shirt', 'Brogues In multi Color', 'Dark Jacket'); 
                            echo $names[(int)rand(0, 3)];
                        ?>
                    </td> 
                    <td title="<?php echo date('d/m/Y \a\t H:i:s', $item->created_at); ?>"><?php echo date('d/m/Y', $item->created_at); ?></td>
                    <td class="icon center">
                        <a href="<?php echo \Uri::create('admin/discountcode/update/' . $item->id); ?>">
                            Update
                        </a>
                    </td>
                </tr>

            <?php endforeach; ?>

            <?php if(empty($items)): ?>

                <tr class="nodrag nodrop">
                    <td colspan="10" class="center"><strong>There are no orders.</strong></td>
                </tr>

            <?php endif; ?>

        </tbody>
    </table>
    
    <div class="pagination_holder">
        <?php echo $pagination->render(); ?>
    </div>
</div>