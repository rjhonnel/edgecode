						<?php 
							
                            // Get all groups and remove admin groups from array
                            $groups = \SentryAdmin::group()->all('front');
                        
							$group = isset($group) ? $group : false;
							$link = isset($link) ? $link : 'update';
							$selected = isset($selected) ? $selected : false;
							
						?>
						
                        <div class="side_tree_holder" style="display:none;">
                            <div class="tree_heading">
                                <h4>User Groups</h4>
                                <div id="sidetreecontrol" class="sidetreecontrol"><a href="#">Collapse All</a><a href="#">Expand All</a></div>
                            </div>
                            <div class="tree_content">
                                <div id="sidetree">
                                
                                	<?php if(!$group && empty($groups)): ?>
                                		<div class="wide"><span class="req">Note: </span> There are no user groups yet.</div>
                                	<?php else: ?>
                                	
	                                    <ul class="treeview" id="tree">
	                                    	
	                                    	<?php
	                                    		if(!empty($groups))
	                                    		{
		                                    		foreach($groups as $key => $group_item)
		                                    		{
                                                        $group_item = (Object)$group_item;
                                                        ?>
                                                            <li>
                                                                <div class="radio_link_holder">
                                                                    <a href="<?php echo \Uri::create('admin/user/group/' . $link . '/' . $group_item->id); ?>" <?php echo $selected == $group_item->id ? 'class="active"' : ''; ?>>
                                                                        <?php echo $group_item->name; ?>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        <?php
		                                    		}
	                                    		}
	                                    	?>
	                                        
	                                    </ul>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
