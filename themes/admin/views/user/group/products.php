				<?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage User Groups')); ?>
	            
                <?php echo \Theme::instance()->view('views/product/group/_navbar_links', array('group' => $group)); ?>
	            
	            <!-- Content -->
			    <div class="content_wrapper">
			    	<div class="content_holder">
			    		
			    		<div class="elements_holder">
			    		
                            <div class="row-fluid breadcrumbs_holder">
			    				<div class="breadcrumbs_position">
                                    <?php 
                                        \Breadcrumb::set('Home', 'admin/dashboard');
                                        \Breadcrumb::set('Catalogue');
                                        \Breadcrumb::set('Manage User Groups', 'admin/user/group/list');
                                        \Breadcrumb::set('Edit Group');
                                        \Breadcrumb::set($group->name, 'admin/product/group/update/' . $group->id);
                                        \Breadcrumb::set('Group Products');

                                        echo \Breadcrumb::create_links();
                                    ?>
		                            
                                    <?php echo \Theme::instance()->view('views/user/group/_action_links'); ?>
                                </div>
                            </div>
                            
						    <div class="row-fluid">
						    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                                
			                    <!-- Main Content Holder -->
			                    <div class="content">
								
			                        <div class="panel">
			                        	<div class="panelHeader">
			                                <h4>Group Products</h4>
			                            </div>
			                            <div class="panelContent">
			                            	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_b_10">
			                            		<tr>
			                            			<td class="span7 p_r_10">
			                            				 <span class="text-danger">*</span> You can select and drag items from left to right, or select and click on button above.<br /><br />
			                            			</td>
			                            			<td class="span5 sort_message_container"></td>
			                            		</tr>
			                            	</table>
		                            	
			                            	<?php echo \Form::open(array('action' => \Uri::admin('current'))); ?>
				                            	<table width="100%">
				                            		<tr>
				                            			<td width="49.5%" valign="top">
				                            				<table class="controls">
							                                    <tbody>
							                                        <tr>
							                                            <td>
							                                            	<h4>Products not in group</h4>
							                                            </td>
							                                            <td>
							                                            	<button name="add" value="add" type="submit" class="button green small right">
																				<span>Add selected</span>
																			</button>
							                                            </td>
							                                        </tr>
							                                    </tbody>
							                                </table>	
							                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 separated scrollable_body">
							                                	<thead>
							                                        <tr class="blueTableHead">
							                                            <th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'products[add]'); ?></th>
							                                            <th scope="col" class="noresize">Code</th>
							                                            <th scope="col">Title</th>
							                                            <th scope="col" class="noresize">Price</th>
							                                        </tr>
							                                    </thead>
							                                    <tbody class="drag_table remove">
							                                    	<?php if(!empty($group->not_related_products)): ?>
								                                    	<?php foreach($group->not_related_products as $not_related): ?>
								                                    		
								                                        	<tr>
									                                            <td class="noresize"><?php echo \Form::checkbox('products[add][]', $not_related->id); ?></td>
									                                            <td class="noresize">11554</td>
									                                            <td><?php echo $not_related->title; ?></td>
									                                            <td class="noresize">$150.00</td>
								                                        	</tr>
							                                        		
							                                        	<?php endforeach; ?>
							                                        	<?php $no_items = 'style="display: none;"'?>
							                                        <?php else: ?>
							                                        	<?php $no_items = ''?>
							                                        <?php endif; ?>
							                                        
							                                        <tr class="no_items" <?php echo $no_items; ?>>
							                                            <td class="noresize center" colspan="4">There are no more products to add</td>
						                                        	</tr>
							                                    </tbody>                    
							                                </table>
				                            			</td>
				                            			
				                            			<td width="1%" class="noresize"></td>
				                            			
				                            			<td width="49.5%" valign="top">
				                            				<table class="controls">
							                                    <tbody>
							                                        <tr>
							                                            <td>
							                                            	<h4>Products assigned to group</h4>
							                                            </td>
							                                            <td>
							                                            	<button name="remove" value="remove" type="submit" class="button blue right">
																				<span>Remove selected</span>
																			</button>
							                                            </td>
							                                        </tr>
							                                    </tbody>
							                                </table>
							                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="greyTable2 separated scrollable_body">
							                                	<thead>
							                                        <tr class="blueTableHead">
							                                            <th class="noresize" scope="col"><?php echo \Form::checkbox('select_all', 'products[remove]'); ?></th>
							                                            <th scope="col" class="noresize">Code</th>
							                                            <th scope="col">Title</th>
							                                            <th scope="col" class="noresize">Price</th>
							                                        </tr>
							                                    </thead>
							                                    <tbody class="drag_table add">
							                                    	<?php if(!empty($group->products)): ?>
								                                    	<?php foreach($group->products as $related): ?>
								                                        	<tr>
									                                            <td class="noresize"><?php echo \Form::checkbox('products[remove][]', $related->id); ?></td>
									                                            <td class="noresize">11554</td>
									                                            <td><?php echo $related->title; ?></td>
									                                            <td class="noresize">$150.00</td>
								                                        	</tr>
							                                        	
							                                        	<?php endforeach; ?>
							                                        	<?php $no_items = 'style="display: none;"'; ?>
							                                        <?php else: ?>
							                                        	<?php $no_items = ''; ?>
							                                        <?php endif; ?>
							                                        
							                                        <tr class="no_items" <?php echo $no_items; ?>>
							                                            <td class="noresize center" colspan="4">There are no products in this group</td>
						                                        	</tr>
							                                    </tbody>                    
							                                </table>
				                            			</td>
				                            		</tr>
				                            	</table>
			                            	<?php echo \Form::close(); ?>
			                            </div>
			                        </div>
					                        
			                    </div><!-- EOF Main Content Holder -->
				                
						    	<!-- Sidebar Holder -->
			                    <div class="sidebar">
			                        <?php echo \Theme::instance()->view('views/product/group/_tree_links', array('group' => $group, 'link' => 'products')); ?>
			                    </div><!-- EOF Sidebar Holder -->
			                    
			                    <div class="clear"></div>
			                    
						    </div>
			    		</div>
			    	</div>
			    </div>
			    <!-- EOF Content -->

			    <script>
					var product_id = <?php echo $product->id; ?>;
				</script>
				<?php echo \Theme::instance()->asset->js('product/related/drag_lists.js'); ?>
