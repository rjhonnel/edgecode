<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Customers');
        \Breadcrumb::set('Customer Group Manager', 'admin/user/group/list');
        \Breadcrumb::set($group->name);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Edit Customer Group </h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/user/group/_action_links'); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/user/group/_navbar_links', array('group' => $group)); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'enctype' => 'multipart/form-data')); ?>
                <?php echo \Form::hidden('details', 1); ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title pull-left">Customer Group Manager</h3>

                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <?php echo \Form::label('Group Name' . '  <span class="text-danger">*</span>'); ?>
                            <div class="input_holder"><?php echo \Form::input('title', \Input::post('title', $group->get('name')), array('class'=>'form-control')); ?></div>
                        </div>



                    </div>
                </div>

                <div class="save_button_holder text-right">
                    <?php echo \Form::button('save', '<i class="fa fa-edit"></i> Save', array('type' => 'submit', 'class' => 'btn btn-success', 'value' => '1')); ?>
                    <?php echo \Form::button('exit', '<i class="fa fa-check"></i>  Save & Exit', array('type' => 'submit', 'class' => 'btn btn-primary', 'value' => '1')); ?>
                </div>


            <?php echo \Theme::instance()->view('views/user/group/_tree_links', array('group' => $group)); ?>

            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>




                
               <script type="text/javascript">
                    $(window).load(function() {
                        count_items();
                    });
                    
                    $(document).ready(function(){
                        $(".add_discount").on('click', function(e){
                            e.preventDefault();
                            var content = $(this).parents().first().find('div').html();
                            var container = $(this).parents('td').first().find('.discount_container_holder');
                            container.append(content);
                            container.find('input').removeAttr('disabled');
                        });
                        $(document).on('click', '.close_discount', function(e){
                            e.preventDefault();
                            $(this).parents('.discount_add_fields').first().remove();
                        });
                        $(document).on('click', '.edit_btn', function(e){
                            e.preventDefault();
                            element = $(this).parents('tr').first().next();
                            
                            if(element.hasClass('highlighted_row'))
                                element.slideToggle();
                        });
                        $(document).on('click', '.close_tier', function(e){
                            e.preventDefault();
                            element = $(this).parents('tr').first();
                            element.slideToggle();
                        });
                        
                        $('.bulk_actions').on('change', function(){
                            var $value = $(this).val();
                            var $add_input = ['discount', 'sale_discount'];
                            if($.inArray($value, $add_input) >= 0) {
                                $('.bulk_input_box').show();
                            } else {
                                $('.bulk_input_box').hide();
                            }
                        });
                
                        $('.apply_bulk_actions').on('click', function(e){
                            e.preventDefault();
                            var $submit = true;
                            var $value = $('.bulk_actions').select2('val');
                            var $input = $('.bulk_input').val();
                            
                            $('.check_active').each(function (index){
                                if($(this).is(":checked")){
                                    var $id = $(this).attr('data-id');
                                    switch($value)
                                    {
            
                                        case 'able_to_buy_enable':
                                            $("input[name='able_to_buy[" + $id + "]']").attr('checked', 'checked');
                                            //$('.custom_checkbox :checkbox').iphoneStyle('refresh');
                                            break;
                                        case 'able_to_buy_disable':
                                            $("input[name='able_to_buy[" + $id + "]']").removeAttr('checked');
                                            //$('.custom_checkbox :checkbox').iphoneStyle('refresh');
                                            break;
                                        case 'able_to_view_enable':
                                            $("input[name='able_to_view[" + $id + "]']").attr('checked', 'checked');
                                            //$('.custom_checkbox :checkbox').iphoneStyle('refresh');
                                            break;
                                        case 'able_to_view_disable':
                                            $("input[name='able_to_view[" + $id + "]']").removeAttr('checked');
                                            //$('.custom_checkbox :checkbox').iphoneStyle('refresh');
                                            break;
                                        case 'discount':
                                            if(!$.isNumeric($input))
                                                {
                                                    $submit = false;
                                                } 
                                            else 
                                                {
                                                    $("input[name^='discount[" + $id + "]']").first().val($input);
                                                }
                                            break;
                                        case 'sale_discount':
                                            if(!$.isNumeric($input)) $submit = false;
                                            else $("input[name='sale_discount[" + $id + "]']").val($input);
                                            break;
                                       
                                    }
                                }
                            })
                            
                            if($submit) $(this).closest('form').submit();
                            
                        });
                    });
                </script>