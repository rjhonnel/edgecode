    
<!-- Navigation bar -->			
<?php if(isset($group)): // Used on "UPDATE" pages?>

    <script type="text/javascript">
        // Select current tab
        $(document).ready(function(){
            if($(".btn-group").length > 0)
            {
                $(".btn-group a.active").removeClass('active');
                $(".btn-group a").each(function(){
                    if($(this).attr("href") == uri_current)
                    {
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>
    
    <div class="panel-nav-holder">
        <div class="btn-group">
            <a href="<?php echo \Uri::create('admin/user/group/update/' . $group->id); ?>" class="btn btn-lg btn-default active" ><i class="fa fa-id-card-o"></i> General Information</a>
            <a href="<?php echo \Uri::create('admin/user/list/' . $group->id); ?>" class="btn btn-lg btn-default active" ><i class="fa fa-user"></i> Users <?php echo $group->users() ? '<label for="" class="label label-primary">' . count($group->users()) . '</label>' : ''; ?></a>
        </div>
    </div>
<?php else: // Used on "CREATE" page ?>
    <?php if(FALSE): ?>
        <div class="panel-nav-holder">
            <div class="btn-group">
                <a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>
<!-- EOF Navigation bar -->