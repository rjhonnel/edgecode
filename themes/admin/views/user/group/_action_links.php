<?php if(!isset($hide_show_all)): ?>
	<a href="<?php echo \Uri::create('admin/user/group/list'); ?>" class="btn btn-sm btn-default"><i class="fa fa-list"></i> Show All</a>
<?php endif; ?>