						<?php 
							
                            // Get all groups and remove admin groups from array
                            $groups = \SentryAdmin::group()->all();
                            foreach ($groups as $key => $group) 
                            {
                                if($group['is_admin']) unset($groups[$key]);
                            }
                        
							$group = isset($group) ? $group : false;
							$selected = isset($selected) ? $selected : false;
							
						?>
						
                        <div class="side_tree_holder">
                            <div class="tree_heading">
                                <h4>User Groups</h4>
                                <div id="sidetreecontrol" class="sidetreecontrol"><a href="#">Collapse All</a><a href="#">Expand All</a></div>
                            </div>
                            <div class="tree_content">
                                <div id="sidetree">
                                
                                	<?php if(!$group && empty($groups)): ?>
                                		<div class="wide"><span class="req">Note: </span> There are no user groups yet.</div>
                                	<?php else: ?>
                                	
	                                    <ul class="treeview" id="tree">
	                                    	
	                                    	<?php
	                                    		if(!empty($groups))
	                                    		{
		                                    		foreach($groups as $key => $group_item)
		                                    		{
                                                        $group_item = (Object)$group_item;
                                                        $users = SentryAdmin::group($group_item->id)->users();
                                                        ?>
                                                            <li>
                                                                <div class="radio_link_holder">
                                                                    <a href="<?php echo \Uri::create('admin/user/list/' . $group_item->id); ?>" <?php echo $selected == $group_item->id ? 'class="active"' : ''; ?>>
                                                                        <?php echo $group_item->name; ?>
                                                                        <?php echo $users ? '<span class="tree_count">(' . count($users) . ')</span>' : ''; ?>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        <?php
		                                    		}
	                                    		}
	                                    	?>
	                                        
	                                    </ul>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
