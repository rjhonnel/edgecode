<div class="layout-content main-content" data-scrollable>

	<div class="main-content-body">
		<?php
		\Breadcrumb::set('Home', 'admin/dashboard');
		\Breadcrumb::set('Customers');
		\Breadcrumb::set('Customer Manager', 'admin/user/list');
		\Breadcrumb::set('Referrals');

		echo \Breadcrumb::create_links();
		?>

		<div class="main-content-body-inner layout-content" data-scrollable>

			<header class="main-content-heading">
				<h4 class="pull-left">Edit Customer: Referrals</h4>

				<div class="pull-right">
					<?php echo \Theme::instance()->view('views/user/_action_links', array('hide_add_new' => 1)); ?>
				</div>
			</header>

			<?php echo \Theme::instance()->view('views/user/_navbar_links', array('user' => $user)); ?>

			<?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'get')); ?>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title pull-left mt-5px">Referrals</h3>
						
						<div class="form-inline pull-right">
							<label>Show entries:</label>
							<?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
						</div>
					</div>

					<?php
					echo \Theme::instance()->view('views/user/_listing_table_referal',
						array(
							'pagination'	=> $pagination,
							'items'			=> $items,
						),
						false
					);
					?>
				</div>
			<?php echo \Form::close(); ?>


		</div>

	</div>
	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>


