<div class="panel-body">
    <table class="table table-striped table-bordered">
        <thead>
        <tr class="blueTableHead">
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Suburb</th>
            <th scope="col">Delete</th>
        </tr>
        </thead>
        <tbody>
        <?php if(empty($items)): ?>
            <tr class="nodrag nodrop">
                <td colspan="5" class="center"><strong>There are no items.</strong></td>
            </tr>
        <?php else: ?>
            <?php foreach($items as $item): ?>
                <?php $item = (Object)$item; ?>
                <tr>
                    <td><?php echo $item->name; ?></td>
                    <td><?php echo $item->email; ?></td>
                    <td><?php echo $item->phone; ?></td>
                    <td><?php echo $item->suburb; ?></td>
                    <td class="icon center">
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete referrals?" href="<?php echo \Uri::create('admin/user/delete_referral/' . $item->id); ?>">
                            Delete
                        </a>
                    </td>
                </tr>

            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
    <div class="pagination-holder">
        <?php echo $pagination->render(); ?>
    </div>
</div>