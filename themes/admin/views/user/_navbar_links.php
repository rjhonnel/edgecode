<?php if($user): // Used on "UPDATE" pages?>

    <div class="panel-nav-holder">
        <div class="btn-group">
            <a href="<?php echo \Uri::create('admin/user/update/' . $user->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-id-card-o"></i> Customer Details</a>
            <a href="<?php echo \Uri::create('admin/user/orderhistory/'. $user->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-history"></i> Order History</a>
            <a href="<?php echo \Uri::create('admin/user/notes/' . $user->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-sticky-note"></i> Notes</a>
            <a href="<?php echo \Uri::create('admin/user/referrals/' . $user->id); ?>" class="btn btn-lg btn-default active"><i class="fa fa-share"></i> Referrals</a>
        </div>
    </div>



    <script type="text/javascript">
        $(document).ready(function(){
            if($(".btn-group").length > 0)
            {
                $(".btn-group a.active").removeClass('active');
                $(".btn-group a").each(function(){
                    if(($(this).attr("href") == uri_current) ||
                        (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                    {
                        $(this).addClass('active');
                    }
                });
            }
        });
    </script>


<?php else: // Used on "CREATE" page ?>
    <?php if(FALSE): ?>
        <div class="second_menu_wrapper">
            <div class="second_menu">
                <ul>
                    <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
                </ul>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>