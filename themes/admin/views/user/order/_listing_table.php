<?php 
// Configs
$invoice = array('' => '-') + \Config::get('details.invoice', array());
$status = array('' => '-') + \Config::get('details.status', array());
$delivery = array('' => '-') + \Config::get('details.delivery', array());
?>								
<div class="panelContent">

    <?php 
        isset($filters) or $filters = true;
        
        if($filters)
        {
            echo \Theme::instance()->view('views/_partials/search_filters', array(
                'pagination' => $pagination,
                'module' => 'company and full',
                'options' => array(
                    'payment_status',
                    'order_status', 
                    'shipping_status',
                    'order_total', 
                    'product_category',
                    'discount_coupon',
                    'order_date', 
                    ),
            ), false);
        }
    ?>
    
    <?php 
        echo \Theme::instance()->view('views/_partials/action_header', array('bulk_actions' => array(
            '0' => 'Select Action',
            '1' => 'Export Orders',
            '2' => 'Delete',
            '3' => 'Change Order Status',
            '4' => 'Change Payment Status',
            '5' => 'Change Shipping Status',
        ))); 
    ?>

    <table class="grid greyTable2 separated" width="100%">
        <thead>
            <tr class="blueTableHead">
                <th scope="col" style="width: 40px;"></th>
                <th scope="col" class="center" style="width: 40px;">Order ID</th>
<!--                <th scope="col" class="center" style="width: 80px;">Purchase Order</th>-->
                <th scope="col">Date</th>
                <th scope="col">Full Name</th>
                <th scope="col">Email</th>
                <th scope="col">Order Status</th>
                <th scope="col">Payment Status</th>
                <th scope="col">Order Total</th>
                <th scope="col" class="center" style="width: 40px;">Invoice</th>
                <th scope="col" class="center" style="width: 40px;">Actions</th>
            </tr>
        </thead>
        <tbody>

            <?php // var_dump($items); ?>
            
            <?php foreach($items as $item): ?>

                <?php $item_info = \Order\Model_Order::order_info($item->id); ?>
                <tr>
                    <td class="noresize">
                        <?php 
                            echo \Form::hidden('action[' . $item->id . ']', 0);
                            echo \Form::checkbox('action[' . $item->id . ']', 1, null, array('class' => 'check_active', 'data-id' => $item->id));
                        ?>
                    </td>
<!--                    <td class="center"><?php// echo $item->number; ?></td> -->
                    <td class="center"><?php echo $item->purchase_number; ?></td>
                    <td title="<?php echo date('d/m/Y \a\t H:i:s', $item->created_at); ?>"><?php echo date('d/m/Y', $item->created_at); ?></td>
                    <td><?php echo $item->entity; ?></td>
                    <td>customer@gmail.com</td>
                    <td>Paid</td>
                    <td>Pending</td>
                    <td>$250.00</td>
                    <td class="icon center">
                        <a href="<?php echo \Uri::create('admin/order/update/' . $item->id); ?>">
                            <span class="fa fa-pdf"></span>
                        </a>
                    </td>
                    <td width="110">
                        <ul class="table-action-inline">
                            <li>
                                <a href="<?php echo \Uri::create('admin/order/update/' . $item->id); ?>">Edit</a>
                            </li>
                        </ul>
                    </td>
                </tr>

            <?php endforeach; ?>

            <?php if(empty($items)): ?>

                <tr class="nodrag nodrop">
                    <td colspan="10" class="center"><strong>There are no orders.</strong></td>
                </tr>

            <?php endif; ?>

        </tbody>
    </table>
    
    <div class="pagination_holder">
        <?php echo $pagination->render(); ?>
    </div>
    <div class="clear"></div>
</div>