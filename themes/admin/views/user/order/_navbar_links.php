    
    <!-- Second menu -->
    <div class="second_menu_wrapper">
        <div class="second_menu_holder">
            <div class="elements_holder">
                <!-- Navigation bar -->					
				<?php if(1 == 1): // Used on "UPDATE" pages ?>
					<script type="text/javascript">
						// Select current tab
						$(document).ready(function(){
                            if($(".second_menu").length > 0)
                            {
                                $(".second_menu ul li a.active").removeClass('active');
                                $(".second_menu ul li a").each(function(){
                                    console.log($(this).attr("href"));
                                    console.log(uri_current);
                                    if(($(this).attr("href") == uri_current) || 
                                        (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                                    {
                                        $(this).addClass('active');
                                    }
                                });
                            }
						});
					</script>
					
                    <div class="second_menu">
			            <ul>
			                <li><a href="<?php echo \Uri::create('admin/discountcode/update'); ?>" class="circle_information active" rel="tooltip" title="General Information"></a></li>
			                <li><a href="<?php echo \Uri::create('admin/discountcode/assign_customer_groups'); ?>" class="circle_users" rel="tooltip" title="Assign Customer Groups"></a></li>
                            <li><a href="<?php echo \Uri::create('admin/discountcode/assign_product_groups'); ?>" class="circle_products" rel="tooltip" title="Assign Products"></a></li>
			            </ul>
			        </div>
				<?php else: // Used on "CREATE" page ?>
                    <?php if(FALSE): ?>
                        <div class="second_menu">
                            <ul>
                                <li><a href="" onclick="return false;" class="circle_information active" rel="tooltip" title="General Information"></a></li>
                            </ul>
                        </div>
                    <?php endif; ?>
				<?php endif; ?>
                <!-- EOF Navigation bar -->
            </div>
        </div>
    </div>
    <!-- EOF Second menu -->