 <?php echo \Theme::instance()->view('views/_partials/blue_header', array('title' => 'Manage Discount Codes')); ?>          

 <?php echo \Theme::instance()->view('views/discountcode/_navbar_links'); ?>

            <!-- Content -->
		    <div class="content_wrapper">
		    	<div class="content_holder">
		    		
		    		<div class="elements_holder">
		    		
                        <div class="row-fluid breadcrumbs_holder">
                            <div class="breadcrumbs_position">
                                <?php 
                                    \Breadcrumb::set('Home', 'admin/dashboard');
                                    \Breadcrumb::set('Discount Codes');
                                    \Breadcrumb::set('Discount Codes List', 'admin/discountcodes/list');

                                    // If viewing category products show category title
                                    if(isset($category))
                                        \Breadcrumb::set($category->title);
                                    
                                    echo \Breadcrumb::create_links();
                                ?>
		                            
                                <?php echo \Theme::instance()->view('views/discountcode/_action_links'); ?>
                            </div>
                        </div>
                        
					    <div class="row-fluid">
					    	<?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
                            
		                    <!-- Main Content Holder -->
		                    <div class="content">
								
                                <div class="panel">
                                    <div class="panelHeader">
                                        <h4>General Information</h4>
                                    </div>
                                    <div class="panelContent">
                                        <div class="span3">
                                            <div class="formRow">
                                                <label>Discount Code:</label>					                                       
                                                <div class="input_holder"><input type="text" placeholder="41534"></div>
                                            </div>
                                            <div class="formRow">
                                                <label>Discount Type:</label>					                                       
                                                <div class="input_holder">
                                                    <select class="select_init" style="width: 102%;">
                                                        <option>Value</option>
                                                        <option>Percentage</option>
                                                        <option>Free Shipping</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span3 right m_r_8">
                                            <div class="formRow">
                                                <label>Status:</label>					                                       
                                                <div class="input_holder">
                                                    <select class="select_init">
                                                        <option>Active</option>
                                                        <option>Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="formRow">
                                                <?php echo \Form::label('Expiry Date', null, array('class' => '')); ?>				                                       
                                                <div class="input_holder">
                                                    <div class="toggable_dates date_content left" <?php echo \Input::post('status') != '2' ? 'style="display: block; margin-top:0; margin-bottom:0;"' : ''; ?>>
		                                                	
                                                        <?php echo \Form::input('active_from', \Input::post('active_from'), array('id' => 'from', 'class' => 'dateInput', 'placeholder' => 'From')); ?>
                                                        <?php echo \Form::input('active_to', \Input::post('active_to'), array('id' => 'to', 'class' => 'dateInput', 'placeholder' => 'To')); ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                    
                                <div class="panel">
                                    <div class="panelHeader">
                                        <h4>Discount Properties (Value)</h4>
                                    </div>
                                    <div class="panelContent">
                                        <div class="span3">
                                            <div class="formRow">
                                                <label>Discount Amount:</label>					                                       
                                                <div class="input_holder"><input type="text" placeholder=""></div>
                                            </div>
                                            <div class="formRow">
                                                <label style="line-height: 13px; margin-top: 0;">Minimum Order Qty:</label>					                                       
                                                <div class="input_holder"><input type="text" placeholder=""></div>
                                            </div>
                                            <div class="formRow">
                                                <label>Order Value:</label>					                                       
                                                <div class="input_holder">
                                                    <input type="text" placeholder="From" style="float: left; width: 103px;">
                                                    <input type="text" placeholder="To" style="float: right; width: 103px; margin-right: -4px;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span3 right m_r_8">
                                            <div class="formRow">
                                                <label>Type:</label>					                                       
                                                <div class="input_holder">
                                                    <select class="select_init">
                                                        <option>Single Use</option>
                                                        <option>Multi Use</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                               
                                <div class="panel">
                                    <div class="panelHeader">
                                        <h4>Discount Properties (Percentage)</h4>
                                    </div>
                                    <div class="panelContent">
                                        <div class="span3">
                                            <div class="formRow">
                                                <label style="line-height: 13px; margin-top: 0;">Discount Percentage:</label>					                                       
                                                <div class="input_holder"><input type="text" placeholder=""></div>
                                            </div>
                                            <div class="formRow">
                                                <label style="line-height: 13px; margin-top: 0;">Minimum Order Qty:</label>					                                       
                                                <div class="input_holder"><input type="text" placeholder=""></div>
                                            </div>
                                            <div class="formRow">
                                                <label>Order Value:</label>					                                       
                                                <div class="input_holder">
                                                    <input type="text" placeholder="From" style="float: left; width: 103px;">
                                                    <input type="text" placeholder="To" style="float: right; width: 103px; margin-right: -4px;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span3 right m_r_8">
                                            <div class="formRow">
                                                <label>Type:</label>					                                       
                                                <div class="input_holder">
                                                    <select class="select_init">
                                                        <option>Single Use</option>
                                                        <option>Multi Use</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                               
                                <div class="panel">
                                    <div class="panelHeader">
                                        <h4>Discount Properties (Free Shipping)</h4>
                                    </div>
                                    <div class="panelContent">
                                        <div class="span3">
                                            <div class="formRow">
                                                <label style="line-height: 13px; margin-top: 0;"> Minimum Order Qty:</label>					                                       
                                                <div class="input_holder"><input type="text" placeholder=""></div>
                                            </div>
                                            <div class="formRow">
                                                <label>Order Value:</label>					                                       
                                                <div class="input_holder">
                                                    <input type="text" placeholder="From" style="float: left; width: 103px;">
                                                    <input type="text" placeholder="To" style="float: right; width: 103px; margin-right: -4px;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span3 right m_r_8">
                                            <div class="formRow">
                                                <label>Type:</label>					                                       
                                                <div class="input_holder">
                                                    <select class="select_init">
                                                        <option>Single Use</option>
                                                        <option>Multi Use</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                               
                                    
		                    </div><!-- EOF Main Content Holder -->
		                    
                            
		                    <div class="clear"></div>
					    	
					    </div>
		    		</div>
		    	</div>
		    </div>
		    <!-- EOF Content -->
           	