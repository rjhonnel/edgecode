<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Customers');
        \Breadcrumb::set('Customer Manager', 'admin/user/list');
        \Breadcrumb::set('Order History - ' . (isset($user) ?  $user->get('metadata.first_name') . ' ' . $user->get('metadata.last_name') : ''));

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">View and Edit Customer: Order History</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/order/_action_links'); ?>
                </div>
            </header>


            <?php echo \Theme::instance()->view('views/user/_navbar_links', array('user' => $user)); ?>
            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'GET')); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left mt-5px">Order History Details</h3>
                    <div class="form-inline pull-right">
                        <label>Show entries:</label>
                        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
                    </div>
                </div>
                <div class="panel-body">
                    <?php
                    // Load page listing table
                    echo \Theme::instance()->view('views/user/_listing_table_orders',
                        array(
                            'pagination'    => $pagination,
                            'items'         => $items,
                            'user'          => $user,
                            'get_attributes' => $get_attributes,
                        ),
                        false
                    );
                    ?>
                </div>
            </div>
            <?php echo \Form::close(); ?>
        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>

