<div class="panel-nav-holder">
    <div class="btn-group">
        <a href="<?php echo \Uri::create('admin/settings'); ?>" class="btn btn-lg btn-default active"><i class="fa fa-info-circle"></i> General Settings</a>
        <a href="<?php echo \Uri::create('admin/settings/amazons3'); ?>" class="btn btn-lg btn-default active"><i class="fa fa-amazon"></i> Amazon S3</a>
        <a href="<?php echo \Uri::create('admin/settings/homepage_option'); ?>" class="btn btn-lg btn-default active"><i class="fa fa-home"></i> Homepage Option</a>
        <a href="<?php echo \Uri::create('admin/settings/accounting_api'); ?>" class="btn btn-lg btn-default active"><i class="fa fa-calculator"></i> Accounting API</a>
    </div>
</div>

<style>
    .panel-nav-holder .btn {
        min-width: 160px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
        if($(".btn-group").length > 0)
        {
            $(".btn-group a.active").removeClass('active');
            $(".btn-group a").each(function(){
                if(($(this).attr("href") == uri_current) ||
                    (uri_current.indexOf("accordion") != -1 && $(this).attr("href").indexOf("accordion") != -1))
                {
                    $(this).addClass('active');
                }
            });
        }
    });
</script>