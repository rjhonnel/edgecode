<div class="main-content">

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Settings', 'admin/settings/');

        if(isset($group))
            \Breadcrumb::set($group['name']);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner">

            <header class="main-content-heading">
                <h4 class="pull-left">Settings</h4>
                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/settings/option/_action_links'); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/settings/option/_navbar_links'); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'settings')); ?>
            <!-- Main Content Holder -->
            <?php
            $hold_logo_url = (isset($settings['logo_url']) ? $settings['logo_url'] : '');
            $hold_company_name = (isset($settings['company_name']) ? $settings['company_name'] : '');
            $hold_address = (isset($settings['address']) ? $settings['address'] : '');
            $hold_website = (isset($settings['website']) ? $settings['website'] : '');
            $hold_phone = (isset($settings['phone']) ? $settings['phone'] : '');
            $hold_email_address = (isset($settings['email_address']) ? $settings['email_address'] : '');
            $hold_bank_details = (isset($settings['bank_details']) ? $settings['bank_details'] : '');
            $hold_sender_email_address = (isset($settings['sender_email_address']) ? $settings['sender_email_address'] : '');
            $hold_contact_us_email_address = (isset($settings['contact_us_email_address']) ? $settings['contact_us_email_address'] : '');
            $hold_facebook_account_name = (isset($settings['facebook_account_name']) ? $settings['facebook_account_name'] : '');
            $hold_instagram_account_name = (isset($settings['instagram_account_name']) ? $settings['instagram_account_name'] : '');
            $hold_gst = (isset($settings['gst']) ? 1 : 0);
            $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : '');
            $hold_delay_sending_invoice = (isset($settings['delay_sending_invoice']) ? $settings['delay_sending_invoice'] : '');
            ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">General Settings</h3>

                </div>
                <div class="panel-body form-horizontal">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Logo:</label>
                                <div class="col-sm-8">
                                    <?php echo \Form::file('image'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Company Name:  <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="company_name" value="<?php echo \Input::post('company_name', $hold_company_name) ?>" type="text" id="form_company_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Address: <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="address" type="text" id="form_address"><?php echo \Input::post('address', $hold_address) ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Website:  <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="website" value="<?php echo \Input::post('website', $hold_website) ?>" type="text" id="form_website">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Phone: <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input class="form-control"t name="phone" value="<?php echo \Input::post('phone', $hold_phone) ?>" type="text" id="form_phone">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Bank Details:</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="bank_details" type="text" id="form_bank_details"><?php echo \Input::post('bank_details', $hold_bank_details) ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email Address: <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="email_address" value="<?php echo \Input::post('email_address', $hold_email_address) ?>" type="email" id="form_email_address">
                                    <small>Email address to receive all orders.
                                    </small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Sender email address:  <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="sender_email_address" value="<?php echo \Input::post('sender_email_address', $hold_sender_email_address) ?>" type="email" id="form_sender_email_address">
                                    <small>Email address for Welcome, Confirm New Password.</small>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Email for Contact form:  <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="contact_us_email_address" value="<?php echo \Input::post('contact_us_email_address', $hold_contact_us_email_address) ?>" type="email" id="form_contact_us_email_address">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Facebook Account:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="facebook_account_name" value="<?php echo \Input::post('facebook_account_name', $hold_facebook_account_name) ?>" type="text" id="form_facebook_account_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Instagram Account:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="instagram_account_name" value="<?php echo \Input::post('instagram_account_name', $hold_instagram_account_name) ?>" type="text" id="form_instagram_account_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">GST:</label>
                                <div class="col-sm-8">
                                    <?php echo \Form::checkbox('gst', 1, \Input::post('gst', $hold_gst)); ?>
                                </div>
                            </div>
                            <div class="form-group gst_percentage_fld">
                                <label class="col-sm-4 control-label">GST Percentage: <span class="text-danger">*</span></label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="gst_percentage" value="<?php echo \Input::post('gst_percentage', $hold_gst_percentage) ?>" type="number" id="form_gst_percentage" min="1" step="0.01">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Delay sending invoice: </label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="delay_sending_invoice" value="<?php echo \Input::post('delay_sending_invoice', $hold_delay_sending_invoice) ?>" type="number" id="form_delay_sending_invoice" min="0" step="1">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <?php if($hold_logo_url): ?>
                                <a class="thumbnail thumbnail-profile" target="_blank" href="<?php echo \Uri::create('media/images/' . $hold_logo_url); ?>">
                                    <img src="<?php echo \Uri::create('media/images/' . $hold_logo_url); ?>" alt="<?php echo $hold_logo_url; ?>">
                                </a>
                                <ul class="table-action-inline pull-right">
                                    <li>
                                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete logo?" href="<?php echo \Uri::admin('current'); ?>/delete_logo?name=<?php echo $hold_logo_url; ?>" ><i class="fa fa-remove"></i> Delete</a>
                                    </li>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success" type="submit"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    if($('#form_gst').is(':checked')){
        $('.gst_percentage_fld').show();
    }
    else{
        $('.gst_percentage_fld').hide();
    }
    $('#form_gst').click(function(){
        if($(this).is(':checked')){
            $('.gst_percentage_fld').show();
        }
        else{
            $('.gst_percentage_fld').hide();
        }
    });
});
</script>