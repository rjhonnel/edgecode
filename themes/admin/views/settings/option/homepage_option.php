<div class="main-content">

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Settings', 'admin/settings/');
        \Breadcrumb::set('Homepage Option');

        if(isset($group))
            \Breadcrumb::set($group['name']);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner">

            <header class="main-content-heading">
                <h4 class="pull-left">Homepage Option</h4>
                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/settings/option/_action_links'); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/settings/option/_navbar_links'); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'settings')); ?>
            <!-- Main Content Holder -->
            <?php
                $hold_header_hide = (isset($settings['header_hide']) ? 1 : 0);
                $hold_categories_hide = (isset($settings['categories_hide']) ? 1 : 0);
                $hold_recommended_products_hide = (isset($settings['recommended_products_hide']) ? 1 : 0);
            ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Homepage Option</h3>

                </div>
                <div class="panel-body form-horizontal">
                    <div class="row">
                        <div class="col-sm-9">
                            <input type="hidden" name="hidden">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Hide Header:</label>
                                <div class="col-sm-8">
                                    <?php echo \Form::checkbox('header_hide', 1, \Input::post('header_hide', $hold_header_hide)); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Hide Categories:</label>
                                <div class="col-sm-8">
                                    <?php echo \Form::checkbox('categories_hide', 1, \Input::post('categories_hide', $hold_categories_hide)); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Hide Recommended Products:</label>
                                <div class="col-sm-8">
                                    <?php echo \Form::checkbox('recommended_products_hide', 1, \Input::post('recommended_products_hide', $hold_recommended_products_hide)); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success" type="submit"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
