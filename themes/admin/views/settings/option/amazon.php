<div class="main-content">

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Settings', 'admin/settings/');
        \Breadcrumb::set('Amazon S3');

        if(isset($group))
            \Breadcrumb::set($group['name']);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner">

            <header class="main-content-heading">
                <h4 class="pull-left">Amazon S3</h4>
                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/settings/option/_action_links'); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/settings/option/_navbar_links'); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'settings')); ?>
            <!-- Main Content Holder -->
            <?php
                $hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
                $hold_amazon_key = (isset($settings['amazon_key']) ? $settings['amazon_key'] : '');
                $hold_amazon_secret = (isset($settings['amazon_secret']) ? $settings['amazon_secret'] : '');
                $hold_amazon_site = (isset($settings['amazon_site']) ? $settings['amazon_site'] : '');
                $hold_amazon_bucket = (isset($settings['amazon_bucket']) ? $settings['amazon_bucket'] : '');
                $hold_amazon_region = (isset($settings['amazon_region']) ? $settings['amazon_region'] : '');
            ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Amazon S3</h3>

                </div>
                <div class="panel-body form-horizontal">
                    <div class="row">
                        <div class="col-sm-9">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Enable Amazon S3:</label>
                                <div class="col-sm-8">
                                    <?php echo \Form::checkbox('amazon_enable', 1, \Input::post('amazon_enable', $hold_amazon_enable)); ?>
                                </div>
                            </div>
                            <div id="amazon_fields">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Amazon Key:  <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="amazon_key" value="<?php echo \Input::post('amazon_key', $hold_amazon_key) ?>" type="text" id="form_amazon_key">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Amazon Secret:  <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="amazon_secret" value="<?php echo \Input::post('amazon_secret', $hold_amazon_secret) ?>" type="text" id="form_amazon_secret">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Amazon Site:  <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="amazon_site" value="<?php echo \Input::post('amazon_site', $hold_amazon_site) ?>" type="text" id="form_amazon_site">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Amazon Bucket:  <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="amazon_bucket" value="<?php echo \Input::post('amazon_bucket', $hold_amazon_bucket) ?>" type="text" id="form_amazon_bucket">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Amazon Region:  <span class="text-danger">*</span></label>
                                    <div class="col-sm-8">
                                        <input class="form-control" name="amazon_region" value="<?php echo \Input::post('amazon_region', $hold_amazon_region) ?>" type="text" id="form_amazon_region">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success" type="submit"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
<script type="text/javascript">
$(document).ready(function(){
    function amazon_enable()
    {
        if($('#form_amazon_enable').is(':checked')){
            $('#amazon_fields').show();
        }
        else{
            $('#amazon_fields').hide();
        }
    }
    amazon_enable();
    $('#form_amazon_enable').click(function(){
        amazon_enable();
    });
});
</script>