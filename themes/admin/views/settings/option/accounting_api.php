<div class="main-content">

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Settings', 'admin/settings/');
        \Breadcrumb::set('Accounting API');

        if(isset($group))
            \Breadcrumb::set($group['name']);

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner">

            <header class="main-content-heading">
                <h4 class="pull-left">Accounting API</h4>
                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/settings/option/_action_links'); ?>
                </div>
            </header>

            <?php echo \Theme::instance()->view('views/settings/option/_navbar_links'); ?>

            <?php echo \Form::open(array('action' => \Uri::admin('current'), 'method' => 'post', 'class' => 'settings')); ?>
            <!-- Main Content Holder -->
            <?php
                $accounting_api = (isset($settings['accounting_api']) ? $settings['accounting_api'] : '');
            ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Accounting API</h3>

                </div>
                <div class="panel-body form-horizontal">
                    <div class="row">
                        <div class="col-sm-9">
                            <input type="hidden" name="hidden">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Enable Import to MYOB:</label>
                                <div class="col-sm-8">
                                    <?php echo \Form::radio('accounting_api', 'myob', \Input::post('accounting_api', $accounting_api)); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Enable Import to Xero:</label>
                                <div class="col-sm-8">
                                    <?php echo \Form::radio('accounting_api', 'xero', \Input::post('accounting_api', $accounting_api)); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success" type="submit"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo \Form::close(); ?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>
