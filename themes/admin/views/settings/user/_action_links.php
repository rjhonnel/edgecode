<?php if(\Sentry::user()->in_group('Super Admin') ): ?>
	<div class="action-list">
		<?php if(!isset($hide_add_new)): ?>
	    	<a href="<?php echo \Uri::create('admin/settings/user/create'); ?>" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New</a>
	    <?php endif; ?>
	    <?php if(!isset($hide_show_all)): ?>
	    	<a href="<?php echo \Uri::create('admin/settings/user/list'); ?>" class="btn btn-sm btn-default"><i class="fa fa-list"></i> Show All</a>
		<?php endif; ?>
	</div>
<?php endif; ?>
