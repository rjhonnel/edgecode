<div class="top-filter-holder">
    <?php
    echo \Theme::instance()->view('views/_partials/search_filters', array(
        'pagination' => $pagination,
        'module' => 'full',
        'options' => array(
            'user_group_admin'
        ),
    ), false);
    ?>
    <div class="pull-right form-inline">
        <label>Show entries:</label>
        <?php echo \Form::select('per_page', \Input::get('per_page', $pagination->per_page), \Config::get('per_page'), array('class' => 'form-control items_per_page', 'onchange' => "$(this).parents('form').submit();")); ?>
    </div>
</div>

<table class="table table-striped table-bordered">
    <thead>
    <tr class="blueTableHead">
        <th scope="col">Username</th>
        <th scope="col">User Group</th>
        <th scope="col">Full Name</th>
        <th scope="col">Email</th>
        <th scope="col" class="center" width="150">Actions</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach($items as $item): ?>
        <?php $item = (Object)$item; ?>
        <?php
        $user_groups = $item->groups();
        $user_group = current($user_groups);
        ?>
        <tr>
            <td>
                <a href="<?php echo \Uri::create('admin/settings/user/update/' . $item->id); ?>">
                    <strong><?php echo $item->get('username'); ?></strong>
                </a>
            </td>
            <td><?php echo $user_group['name']; ?></td>
            <td><?php if($item->metadata) echo $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name'); ?></td>
            <td>
                <?php echo $item->get('email'); ?>
            </td>
            <td width="110">
                <ul class="table-action-inline">
                    <li>
                        <a href="<?php echo \Uri::create('admin/settings/user/update/' . $item->id); ?>">Edit</a>
                    </li>
                    <li>
                        <a class="text-danger confirmation-pop-up" data-message="Are you sure you want to delete?" href="<?php echo \Uri::create('admin/user/delete/' . $item->id); ?>">Delete</a>
                    </li>
                </ul>
            </td>
        </tr>

    <?php endforeach; ?>

    <?php if(empty($items)): ?>

        <tr class="nodrag nodrop">
            <td colspan="7" class="center"><strong>There are no items.</strong></td>
        </tr>

    <?php endif; ?>

    </tbody>
</table>
<div class="pagination-holder">
    <?php echo $pagination->render(); ?>
</div>