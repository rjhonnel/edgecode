<div class="layout-content main-content" data-scrollable>

    <div class="main-content-body">
        <?php
        \Breadcrumb::set('Home', 'admin/dashboard');
        \Breadcrumb::set('Admin Users', 'admin/settings/user/list');
        \Breadcrumb::set('Update');

        echo \Breadcrumb::create_links();
        ?>

        <div class="main-content-body-inner layout-content" data-scrollable>

            <header class="main-content-heading">
                <h4 class="pull-left">Update Admin User</h4>

                <div class="pull-right">
                    <?php echo \Theme::instance()->view('views/settings/user/_action_links'); ?>
                </div>
            </header>

            <?php echo \Form::open(array('action'=> \Uri::admin('current'),'enctype' => 'multipart/form-data'));?>

            <div class="panel panel-default">

                <div class="panel-heading">
                    <h3 class="panel-title pull-left">Profile</h3>

                </div>
                <div class="panel-body form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Username  <span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <?php echo Form::input('username',($user_data['username']) ? $user_data['username'] : \Input::post('username'), array('class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">First Name  <span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <?php echo Form::input('first_name',($user_data['first_name']) ? $user_data['first_name'] : \Input::post('first_name'), array('class' => 'form-control','placeholder' => 'First Name')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Last Name  <span class="text-danger">*</span></label>
                        <div class="col-sm-5">
                            <?php echo Form::input('last_name',($user_data['first_name']) ? $user_data['last_name'] : \Input::post('last_name'), array('class' => 'form-control','placeholder' => 'Last Name')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-5">
                            <?php echo Form::input('email', ($user_data['email']) ? $user_data['email'] : \Input::post('email'), array('class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">User Group</label>
                        <div class="col-sm-2">
                            <?php
                                $user = \SentryAdmin::user($user_data['id']);
                                $user_group = current($user->groups());

                                echo \Form::select('user_group', \Input::get('user_group', $user_group ? $user_group['id'] : false ),  \User\Model_Group::fetch_pair('id', 'name', array(), false, \SentryAdmin::group()->all('admin')), array('style' => '', 'class' => 'form-control'));
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-5">
                            <?php echo Form::password('password', \Input::post('password'), array('class' => 'form-control', 'placeholder'=>'New Password')); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Confirm Password   <span class="text-danger">*</span></label>

                        <div class="col-sm-5">
                            <?php echo Form::password('confirm_password', \Input::post('confirm_password'), array('class' => 'form-control', 'placeholder'=>'Confirm Password')); ?>
                        </div>
                    </div>

                    <div class="save_button_holder">
                        <div class="row">
                            <label for="" class="col-sm-3">&nbsp;</label>
                            <div class="col-sm-5">
                                <button class="btn btn-success" type="submit"><i class="fa fa-edit"></i> Save</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <?php echo \Form::close();?>


        </div>

    </div>
    <?php echo \Theme::instance()->view('views/_partials/navigation'); ?>
</div>