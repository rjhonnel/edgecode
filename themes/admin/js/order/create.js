

$(document).ready(function(){
	$('#form_product').on('change',function(e){

		e.preventDefault();

		var is_edit = $(this).attr('data-edit');


		$('#form_product_id').val($(this).val());

		$('#prod-attribute-list').html('');
		$('#pack-options').html('');

		$.ajax({

	        type: 'get',

	        dataType:'json',

	        url: uri_base + '/order/get_attr/' + $(this).val(),

	        success: function(result)
	        {
	        	var attributes = result.attributes;
	        	var pack_options = result.pack_options;

	        	$('#minimum_order').val(attributes.minimum_order);

	        	if(attributes.datas['sx'])
	        	{

	        		var select = '';

		        	$.each(attributes.datas['sx'],function(i,obj){

		        		var option = '';

		        		var sx_index = i;

		        		var sx_obj = obj;


		        		$.each(attributes.datas['select'][obj],function(i,obj){

		        			var is_selected = '';

		        			if(is_edit == 'yes' && $('#product-attributes-edit-holder').length > 0)

		        			{

			        			if($('#product-attributes-edit-holder').find('input[name="select_attributes['+$('#product-attributes-edit-holder').attr('data-index')+']['+sx_obj+']"]').val() == i)

			        				is_selected = 'selected="selected"';

		        			}

		        			else

		        			{

			        			if(attributes.datas['current_attributes'][sx_index].option.id == i)

			        				is_selected = 'selected="selected"';

		        			}


		        			option += '<option value="'+i+'" '+is_selected+'>'+obj+'</option>';

		        			

		        		});



		        		select += '<div class="form-group"><label>'+attributes.datas['select_name'][obj]+'</label><select name="select['+obj+']" data-attributeid='+obj+' id="form_select['+obj+']" class="form-control attribute_selection" tab-index="-1" area-hidden="true">'+option+'</select></div>';

		        	});

		          


		        	$('#prod-attribute-list').html(select);

		        	$('#prod-attribute-list').on('change','.attribute_selection',function(e){    

		        		$('#form_attributeid').val($(this).attr('data-attributeid'));   


					    $.ajax({

				            dataType: 'json',

				            type: 'post',

				            url: $('#form_uri').val() + $('#form_product_id').val(),

				            data:$(this).closest('form').serialize(),

				            success: function(attributes){

				            	$('#form_price').val(attributes.price);

				            	if(is_edit != 'yes')

				            		$('#form_product_code').val(attributes.code);

				            }
				        });

				    }); 

	        	}

	        	if(is_edit != 'yes')
					$('#form_product_code').val(attributes.code);

				// pack options
				var hold_edit_pack_option_list = [];
				if(is_edit == 'yes')
				{
					if($('#edit-selected-pack-options').length)
					{
						var hold_pack_options = $('#edit-selected-pack-options').val().split(',');
						for(var z = 0; z < hold_pack_options.length; z++)
						{
							var h = hold_pack_options[z].split(':');
							hold_edit_pack_option_list[h[0]] = h[1].split('-');
						}
					}
				}
				var html = '';
				if(pack_options)
				{
					html += '<label>Pack Options:</label>';

					for(var i = 0; i < pack_options.length; i++)
					{
						html += '<div class="form-group" data-pack_id="'+pack_options[i].id+'" data-selection_limit="'+pack_options[i].selection_limit+'" data-type="'+pack_options[i].type+'"><div class="checkbox" style="margin-left: 0;"><label class="pack-title" style="padding: 0;font-weight:bold;">'+pack_options[i].name+'</label>';
					    if(pack_options[i].type == 'included')
							html += ' - Select '+pack_options[i].selection_limit+' item(s)';
						html += '</div>';
						for(var j = 0; j < pack_options[i].products.length; j++)
						{
							var check_pack_product = '';
							if(is_edit == 'yes')
							{
								if(hold_edit_pack_option_list[pack_options[i].id])
								{
									if(hold_edit_pack_option_list[pack_options[i].id].indexOf(pack_options[i].products[j].id) > -1)
										check_pack_product = 'checked="checked"';
								}
							}

							html += '<label class="checkbox-inline" '+(j==0?'style="margin-left: 10px;margin-bottom: 10px;"':'style="margin-bottom: 10px;"')+' data-product_title="'+pack_options[i].products[j].title+'"><input type="checkbox" data-pack_id="'+pack_options[i].id+'" name="packs['+pack_options[i].id+']['+j+']" value="'+pack_options[i].products[j].id+'" '+check_pack_product+'> '+pack_options[i].products[j].title;

							if(pack_options[i].type == 'extras')
								html += ' <input type="number" data-pack_id="'+pack_options[i].id+'" data-minimum_order="'+pack_options[i].products[j].minimum_order+'" data-price="'+pack_options[i].products[j].price+'" data-code="'+pack_options[i].products[j].code+'" data-select="'+pack_options[i].products[j].select+'" name="quantity_extra['+pack_options[i].id+']['+j+']" class="form-control" min="1" style="width: 70px;display: inline;">';

							html += '</label>';
						}
						html += '</div>';
					}
				}
				$('#pack-options').html(html);


				$('#prod-attribute-list').addClass('col-sm-5');
				if(!attributes.datas['sx'] && !pack_options)
				{
					$('#prod-attribute-list').html("<p>No attribute for this product.</p>");
				}
				else if(!attributes.datas['sx'] && pack_options)
				{
					$('#prod-attribute-list').removeClass('col-sm-5');
				}
	        }

        });

	});


	$('select[name="product"]').select2();
	// refresh value of customer Id
	$('select[name="customer_id"]').val('');

	// use select2 in customer name list
	$('select[name="customer_id"]').select2({
	    width: '50%',
	    allowClear: true,
	    multiple: true,
	    maximumSelectionLength: 1,
	    placeholder: "Type Customer Name",
	    data: customer_list
	});
	// add class "require" after select2 has been generated to avoid the class to be copied in the select2 generated elements
	$('select[name="customer_id"]').addClass('require');

	// update customer details when customer name is selected
	$(document).on('change', 'select[name="customer_id"]', function(){
		var value = $('select[name="customer_id"] option:selected').attr('value');
		if(value)
		{
		    $.ajax({
		        dataType: 'json',
		        type: 'post',
		        data: {
		            customer_id: value
		        },
		        url: uri_base + '/order/get_customer_details',
		        beforeSend: function()
		        {
		        	$('#loading_area_customer').html('<i class="fa fa-spinner fa-spin"></i>');
		        },
		        success: function(data)
		        {
		        	if(data.user == false)
		        	{
		        		$('.customer_details').each(function(index, elem){
		        			$(elem).empty();
		        		});
		        	}
		        	else
		        	{
		        		$('#customer_details_email').val(data.user.email);
		        		$('#customer_details_title').val(data.user.title);
		        		$('#customer_details_first_name').val(data.user.first_name);
		        		$('#customer_details_last_name').val(data.user.last_name);
		        		$('#customer_details_company').val(data.user.company);
		        		$('#customer_details_address').val(data.user.address);
		        		$('#customer_details_address2').val(data.user.address2);
		        		$('#customer_details_suburb').val(data.user.suburb);
		        		$('#customer_details_country').val(data.user.country);
		        		$('#customer_details_state').val(data.user.state);
		        		$('#customer_details_postcode').val(data.user.postcode);
		        		$('#customer_details_phone').val(data.user.phone);

		        		$('#customer_details_shipping_title').val(data.user.title);
		        		$('#customer_details_shipping_first_name').val(data.user.first_name);
		        		$('#customer_details_shipping_last_name').val(data.user.last_name);
		        		$('#customer_details_shipping_company').val(data.user.company);
		        		$('#customer_details_shipping_address').val(data.user.address);
		        		$('#customer_details_shipping_address2').val(data.user.address2);
		        		$('#customer_details_shipping_suburb').val(data.user.suburb);
		        		$('#customer_details_shipping_country').val(data.user.country);
		        		$('#customer_details_shipping_state').val(data.user.state);
		        		$('#customer_details_shipping_postcode').val(data.user.postcode);
		        		$('#customer_details_shipping_phone').val(data.user.phone);

		        		$('#customer_details_shipping_title').attr('data-original', data.user.title);
		        		$('#customer_details_shipping_first_name').attr('data-original', data.user.first_name);
		        		$('#customer_details_shipping_last_name').attr('data-original', data.user.last_name);
		        		$('#customer_details_shipping_company').attr('data-original', data.user.company);
		        		$('#customer_details_shipping_address').attr('data-original', data.user.address);
		        		$('#customer_details_shipping_suburb').attr('data-original', data.user.suburb);
		        		$('#customer_details_shipping_country').attr('data-original', data.user.country);
		        		$('#customer_details_shipping_state').attr('data-original', data.user.state);
		        		$('#customer_details_shipping_postcode').attr('data-original', data.user.postcode);
		        		$('#customer_details_shipping_phone').attr('data-original', data.user.phone);

		        		$('#same_as_billing').attr('checked', 'checked');
		        	}

		        	$('#loading_area_customer').empty();
		        }
		    });
		}
		else
		{
			$('.customer_details').each(function(index, elem){
    			$(elem).empty();
    		});
		}
	});
	$(document).on('change', 'select[name="product"]', function(){
		var is_edit = $('select[name="product"]').attr('data-edit');
		if(is_edit != 'yes')
		{
			var value = $('select[name="product"] option:selected').attr('value');
			if(value)
			{
				$.ajax({
			        dataType: 'json',
			        type: 'post',
			        data: {
			            product_id: value
			        },
			        url: uri_base + '/order/get_product_data/',
			        success: function(data)
			        {
			        	$('input[name="price"]').val(data.product.price);

			        	if(data.product.has_attributes)
			        	{
			        		setTimeout(function() {
			        			$('#prod-attribute-list').find('select.attribute_selection').trigger('change');
			        		}, 100);
			        	}
			        }
			    });
			}
		}
	});

	topContinueBackButton();

	// back to step 1
	$('#back_button_down').click(function(e){
		e.preventDefault();

		$('#step_one_tab').removeClass('is_disabled');
		$('#step_one_tab a').trigger('click');
		$('#step_one_tab').addClass('is_disabled');
		$('#step_two_tab').addClass('disabled');

		topContinueBackButton();
	});

	// back to step 2
	$('#back_button_down2').click(function(e){
		e.preventDefault();

		$('#step_two_tab').removeClass('is_disabled');
		$('#step_two_tab a').trigger('click');
		$('#step_tow_tab').addClass('is_disabled');
		$('#step_three_tab').addClass('disabled');
		
		topContinueBackButton();
	});

	// go to step 2
	$('#continue_button_down').click(function(e){
		e.preventDefault();

		var returnVal = validate_step_1();

		if(returnVal)
		{
			$('#step_two_tab').addClass('is_disabled');
            bootbox.alert('<h4>Please fill all required data with valid value!</h4>');
		}
		else
		{
			$('#step_two_tab').removeClass('is_disabled');
			$('#step_two_tab a').trigger('click');
			$('#step_two_tab').addClass('is_disabled');
			$('#step_one_tab').addClass('disabled');
		}
		
		topContinueBackButton();
	});

	// go to step 3
	$('#continue_button_down2').click(function(e){
		e.preventDefault();

		if( !$('#item_list_table').find('tbody').find('tr#no_items').length )
		{
			// check if delivery date and time is filled in create page
			if($('input[name="delivery_datetime_list"]').length)
			{
				// if($('input[name="delivery_date"]').val())
				// {
					if($('input[name="delivery_datetime_list"]').val())
					{
						$('#step_one_tab').addClass('disabled');

						$('#step_three_tab').removeClass('is_disabled');
						$('#step_three_tab a').trigger('click');
						$('#step_three_tab').addClass('is_disabled');
						$('#step_two_tab').addClass('disabled');
					}
					else
					{
						// bootbox.alert('<h4>Select delivery time first!</h4>');
                        $('#step_one_tab').addClass('disabled');

                        $('#step_three_tab').removeClass('is_disabled');
                        $('#step_three_tab a').trigger('click');
                        $('#step_three_tab').addClass('is_disabled');
                        $('#step_two_tab').addClass('disabled');
					}
				// }
				// else
				// {
				// 	bootbox.alert('<h4>Fill delivery date first!</h4>');
				// }
			}
			else
			{
				$('#step_one_tab').addClass('disabled');

				$('#step_three_tab').removeClass('is_disabled');
				$('#step_three_tab a').trigger('click');
				$('#step_three_tab').addClass('is_disabled');
				$('#step_two_tab').addClass('disabled');
			}
			/*if($('input[name="delivery_datetime_list"]').length)
			{
				if(returnVal == true)
					return returnVal;

				// check if hour is not equal to -
				$('select[data-type="hour"]').each(function(index, elem){
					if($(elem).val() == '-')
					{
						returnVal = true;
						return false;
					}
					else
						returnVal = false;
				});

				if(returnVal == true)
					return returnVal;

				// check if minute is not equal to -
				$('select[data-type="minute"]').each(function(index, elem){
					if($(elem).val() == '-')
					{
						returnVal = true;
						return false;
					}
					else
						returnVal = false;
				});
			}*/
		}
		else
			bootbox.alert('<h4>Select item first!</h4>');
		
		topContinueBackButton();
	});

	// top back
	$('#back_button_top').click(function(e){
		e.preventDefault();
		
		var active_tab_content_id = $('#step_guide').find('li.active').find('a').attr('href');
		var has_back = $(active_tab_content_id).find('button[name="back"]').length;
		if(has_back)
			$(active_tab_content_id).find('button[name="back"]').trigger('click');
	});

	// top continue
	$('#continue_button_top').click(function(e){
		e.preventDefault();

		var active_tab_content_id = $('#step_guide').find('li.active').find('a').attr('href');
		var has_continue = $(active_tab_content_id).find('button[name="continue"]').length;
		if(has_continue)
			$(active_tab_content_id).find('button[name="continue"]').trigger('click');
	});

	// add new product in the order
	$('#add_item_button').click(function(e){
		e.preventDefault();
		
		var tot = 0;
		
		$('#item_list_table tr').each(function(){
			if($(this).attr('id') == $('select[name="product"]').val()){
				tot = parseInt($(this).attr('id')) + parseInt($('select[name="product"]').val());
				if(tot < $('#minimum_order').val()){
					bootbox.alert('<h4>Minimum order of this product is '+ $('#minimum_order').val() +' !</h4>');
				}
			}
		});
		var returnVal = validate_add_item();
		var retValPackOptions = validate_pack_options();

		if(returnVal)
		{
            bootbox.alert('<h4>Please fill a valid data in required fields!</h4>');
		}
		else if(parseInt($('input[name="quantity"]').val()) < parseInt($('#minimum_order').val())){
			bootbox.alert('<h4>Minimum order of this product is '+ $('#minimum_order').val() +' !</h4>');
		}
		else if(retValPackOptions)
		{
			bootbox.alert('<h4>'+retValPackOptions+'</h4>');
		}
		else
		{
			var get_selected_attributes = {};
			var delivery_time = '';
			
			if($('#prod-attribute-list').find('.attribute_selection').length)
			{
				$('#prod-attribute-list').find('.attribute_selection').each(function(index, elem){
					var get_name = $(elem).attr('name').replace('select', '').replace('[', '').replace(']', '');
					get_selected_attributes[get_name] = $(elem).val();
				});
			}

			var pack_list = [];
			if($('#pack-options').html().length)
			{
				$('#pack-options').find('div.form-group[data-type="included"]').each(function(index, elem){
					var pack_products = [];
					$(elem).find('input[type="checkbox"]:checked').each(function(ind, ele){
						pack_products.push({
							'id' : $(ele).val(),
							'name' : $(ele).closest('label').attr('data-product_title'),
							'time' : '',
						});
					});

					pack_list.push({
                        'id' : $(elem).attr('data-pack_id'),
                        'name' : $(elem).find('label.pack-title').text(),
                        'types' : $(elem).attr('data-type'),
                        'selection_limit' : $(elem).attr('data-selection_limit'),
                        'products' : pack_products,
					});
				});
			}

			item = { product_id : $('select[name="product"]').val(), product_quantity : $('input[name="quantity"]').val(), product_price: $('input[name="price"]').val(), product_code: $('input[name="product_code"]').val(), select: get_selected_attributes, delivery_time: delivery_time, packs: pack_list};
			items.push(item);

			// extra products
			if($('#pack-options').html().length)
			{
				$('#pack-options').find('div.form-group[data-type="extras"]').find('input[type="checkbox"]:checked').each(function(index, elem){
					var pack_select = {};
					if($(elem).closest('label').find('input[type="number"]').attr('data-select'))
					{
						var hold_select = $(elem).closest('label').find('input[type="number"]').attr('data-select').split(",");
						for(var e = 0; e < hold_select.length; e++)
						{
							var hold_select_inside = hold_select[e].split(':');
							pack_select[hold_select_inside[0]] = hold_select_inside[1];
						}
					}

					item = { product_id : $(elem).val(), product_quantity : $(elem).closest('label').find('input[type="number"]').val(), product_price: $(elem).closest('label').find('input[type="number"]').attr('data-price'), product_code: $(elem).closest('label').find('input[type="number"]').attr('data-code'), select: pack_select, delivery_time: delivery_time, packs: []};
					items.push(item);
				});
			}
			
			// check first if it has old product added to update date
			for (var i = 0; i < items.length; i++) {
				if($('select[name="product"]').val() == items[i].product_id  && JSON.stringify(get_selected_attributes) === JSON.stringify(items[i].select))
				{
					items[i].product_price = $('input[name="price"]').val();
					
				}
			}

			var hold_items = items;
		
			filtered_items = [];
			for (var i = 0; i < items.length; i++) {
				var hold_id = items[i].product_id;
				var delivery_time = items[i].delivery_time;
				var packs = items[i].packs;

				var hold_quantity = 0;
				var hold_price = 0;
				var hold_attributes = hold_items[i].select;
				for (var j = 0; j < hold_items.length; j++) {
					var hold_id_sub = hold_items[j].product_id;
					var hold_attributes_sub = hold_items[j].select;
					if(hold_id == hold_id_sub && JSON.stringify(hold_attributes) === JSON.stringify(hold_attributes_sub)) // check if product id is existing and check attribute is already existing
					{
						hold_quantity += parseInt(hold_items[j].product_quantity);
						hold_price = parseFloat(hold_items[j].product_price);
					}
				}

				var filtered_item = { product_id : hold_id, product_quantity : hold_quantity, product_price : hold_price, select: hold_attributes,delivery_time:delivery_time,packs:packs};
				// check if product id is existing and if not the item will be added
				if(returnIndexContainsObject(filtered_item, filtered_items) == -1)
					filtered_items.push(filtered_item);
			}

			getCartData(filtered_items);
			clear_add_products();
		}

	});

   	$( "body" ).on( "change", 'select[name="delivery_time[]"]', function(e) {
   		items[$(this).closest('tr').data('index')].delivery_time = $(this).val();
	});
	
	// remove products in table
	$( 'body' ).on("click", 'a[href="#remove_product"]', function(e) {
		e.preventDefault();

		var get_selected_attributes = {}; // save
		if($(this).parent('td').parent('tr').find('.product-attributes-holder').find('input').length)
		{
			var get_index = $(this).parent('td').parent('tr').attr('data-index');
			$(this).parent('td').parent('tr').find('.product-attributes-holder').find('input').each(function(index, elem){
				var get_name = $(elem).attr('name').replace('select_attributes', '').replace('['+get_index+']', '').replace('[', '').replace(']', '');
				get_selected_attributes[get_name] = $(elem).val();
			});
		}

		var filtered_item = { product_id : $(this).parent('td').parent('tr').find('input[name="product_id[]"]').val(), product_quantity : $(this).parent('td').parent('tr').find('input[name="product_quantity[]"]').val(), select: get_selected_attributes };

		//remove prducts in item variable
		var removeIndex = returnIndexContainsObject(filtered_item, filtered_items);
		filtered_items.splice(removeIndex, 1);

		//remove prducts in item variable
		var removeIndexItems = returnIndexContainsObject(filtered_item, items);
		while(removeIndexItems!= -1) 
		{
			items.splice(removeIndex, 1);
			removeIndexItems = returnIndexContainsObject(filtered_item, items);
		}

		getCartData(filtered_items);
	});
	$( 'body' ).on("click", 'a[href="#edit_product"]', function(e) {
		e.preventDefault();
		var _tr = $(this).parent('td').parent('tr');

		$('select[name="product"]').attr('data-edit', 'yes');
		$('#product-attributes-edit-holder').html(_tr.find('.product-attributes-holder').html());
		$('#product-attributes-edit-holder').attr('data-index', _tr.attr('data-index'));

		$('select[name="product"]').val(_tr.find('input[name="product_id[]"]').val()).trigger('change');
		$('input[name="price"]').val(_tr.find('input[name="product_price[]"]').val());
		$('input[name="quantity"]').val(_tr.find('input[name="product_quantity[]"]').val());
		
		$('#save_item_button').attr('orig-product', _tr.find('input[name="product_id[]"]').val());
		$('#save_item_button').attr('orig-quantity', _tr.find('input[name="product_quantity[]"]').val());
		$('#save_item_button').attr('orig-code', _tr.find('input[name="product_code[]"]').val());

		$('#add-product-group').addClass('hide');
		$('#edit-product-group').removeClass('hide');

		$('input[name="product_code"]').val(_tr.find('input[name="product_code[]"]').val());

		if($(this).closest('table').find('tr[data-index="'+_tr.attr('data-index')+'"][data-pack_options="true"]').length)
		{
			var hold_edit_pack_option = [];
			$(this).closest('table').find('tr[data-index="'+_tr.attr('data-index')+'"][data-pack_options="true"]').each(function(index, elem){
				var hold_prd = [];
				$(elem).find('input[data-pack_option_product]').each(function(ind, ele){
					hold_prd.push($(ele).attr('data-pack_option_product'));
				});
				hold_edit_pack_option.push($(elem).attr('data-pack_id')+':'+hold_prd.join('-'));
			});
			$('#product-pack-options-edit-holder').html('<input type="hidden" id="edit-selected-pack-options" value="'+hold_edit_pack_option.join()+'">');
			$('#product-pack-options-edit-holder').attr('data-index', _tr.attr('data-index'));
		}
		else
		{
			$('#product-pack-options-edit-holder').removeAttr('data-index');
			$('#product-pack-options-edit-holder').html('');
		}
	});
	$( 'body' ).on("click", '#save_item_button', function(e) {
		e.preventDefault();

		var returnVal = validate_add_item();
		var retValPackOptions = validate_pack_options();

		if(returnVal)
		{
            bootbox.alert('<h4>Please fill a valid data in required fields!</h4>');
		}
		else if(parseInt($('input[name="quantity"]').val()) < parseInt($('#minimum_order').val())){
			bootbox.alert('<h4>Minimum order of this product is '+ $('#minimum_order').val() +' !</h4>');
		}
		else if(retValPackOptions)
		{
			bootbox.alert('<h4>'+retValPackOptions+'</h4>');
		}
		else
		{
			var get_selected_attributes_orig = {}; // remove
			if($('#product-attributes-edit-holder').find('input').length)
			{
				$('#product-attributes-edit-holder').find('input').each(function(index, elem){
					var get_name = $(elem).attr('name').replace('select_attributes', '').replace('['+$('#product-attributes-edit-holder').attr('data-index')+']', '').replace('[', '').replace(']', '');
					get_selected_attributes_orig[get_name] = $(elem).val();
				});
			}
			//remove
			var filtered_item = { product_id : $('#save_item_button').attr('orig-product'), product_quantity : $('#save_item_button').attr('orig-quantity'), product_code: $('#save_item_button').attr('orig-code'), select: get_selected_attributes_orig };
			//remove prducts in filtered_items variable
			var removeIndex = returnIndexContainsObject(filtered_item, filtered_items);
			filtered_items.splice(removeIndex, 1);

			//remove prducts in item variable
			var removeIndexItems = returnIndexContainsObject(filtered_item, items);
			items.splice(removeIndex, 1);

			// getCartData(filtered_items);


			//add
			$('#add-product-group').removeClass('hide');
			$('#edit-product-group').addClass('hide');
			$('select[name="product"]').removeAttr('data-edit');


			var get_selected_attributes = {}; // save
			if($('#prod-attribute-list').find('.attribute_selection').length)
			{
				$('#prod-attribute-list').find('.attribute_selection').each(function(index, elem){
					var get_name = $(elem).attr('name').replace('select', '').replace('[', '').replace(']', '');
					get_selected_attributes[get_name] = $(elem).val();
				});
			}
			var delivery_time = $('tr[data-index="'+$('#product-attributes-edit-holder').attr('data-index')+'"]').find('select[name="delivery_time[]"]').val();

			// check if change attribute when edit
			if(JSON.stringify(get_selected_attributes_orig) !== JSON.stringify(get_selected_attributes))
			{
				// remove if there is existing added attribute already
				//remove
				var filtered_item = { product_id : $('select[name="product"]').val(), product_quantity : $('input[name="quantity"]').val(), product_code: $('input[name="product_code"]').val(), select: get_selected_attributes,delivery_time:delivery_time};
				//remove prducts in filtered_items variable
				var removeIndex = returnIndexContainsObject(filtered_item, filtered_items);
				filtered_items.splice(removeIndex, 1);

				//remove prducts in item variable
				var removeIndexItems = returnIndexContainsObject(filtered_item, items);
				items.splice(removeIndex, 1);
			}

			var pack_list = [];
			if($('#pack-options').html().length)
			{
				$('#pack-options').find('div.form-group[data-type="included"]').each(function(index, elem){
					var pack_products = [];
					$(elem).find('input[type="checkbox"]:checked').each(function(ind, ele){
						pack_products.push({
							'id' : $(ele).val(),
							'name' : $(ele).closest('label').attr('data-product_title'),
							'time' : '',
						});
					});

					pack_list.push({
                        'id' : $(elem).attr('data-pack_id'),
                        'name' : $(elem).find('label.pack-title').text(),
                        'types' : $(elem).attr('data-type'),
                        'selection_limit' : $(elem).attr('data-selection_limit'),
                        'products' : pack_products,
					});
				});
			}

			item = { product_id : $('select[name="product"]').val(), product_quantity : $('input[name="quantity"]').val(), product_price: $('input[name="price"]').val(), product_code: $('input[name="product_code"]').val(), select: get_selected_attributes,delivery_time:delivery_time, packs: pack_list};
			items.push(item);


			// extra products
			if($('#pack-options').html().length)
			{
				$('#pack-options').find('div.form-group[data-type="extras"]').find('input[type="checkbox"]:checked').each(function(index, elem){
					var pack_select = {};
					if($(elem).closest('label').find('input[type="number"]').attr('data-select'))
					{
						var hold_select = $(elem).closest('label').find('input[type="number"]').attr('data-select').split(",");
						for(var e = 0; e < hold_select.length; e++)
						{
							var hold_select_inside = hold_select[e].split(':');
							pack_select[hold_select_inside[0]] = hold_select_inside[1];
						}
					}

					item = { product_id : $(elem).val(), product_quantity : $(elem).closest('label').find('input[type="number"]').val(), product_price: $(elem).closest('label').find('input[type="number"]').attr('data-price'), product_code: $(elem).closest('label').find('input[type="number"]').attr('data-code'), select: pack_select, delivery_time: delivery_time, packs: []};
					items.push(item);
				});
			}

			// check first if it has old product added to update date
			for (var i = 0; i < items.length; i++) {
				if($('select[name="product"]').val() == items[i].product_id  && JSON.stringify(get_selected_attributes) === JSON.stringify(items[i].select))
				{
					items[i].product_price = $('input[name="price"]').val();
				}
			}
						
			var hold_items = items;
			filtered_items = [];
			for (var i = 0; i < items.length; i++) {
				var hold_id = items[i].product_id;
				var delivery_time = items[i].delivery_time;
				var packs = items[i].packs;

				var hold_quantity = 0;
				var hold_price = 0;
				var hold_attributes = hold_items[i].select;
				for (var j = 0; j < hold_items.length; j++) {
					var hold_id_sub = hold_items[j].product_id;
					var hold_attributes_sub = hold_items[j].select;
					if(hold_id == hold_id_sub && JSON.stringify(hold_attributes) === JSON.stringify(hold_attributes_sub))
					{
						hold_quantity += parseInt(hold_items[j].product_quantity);
						hold_price = parseFloat(hold_items[j].product_price);
					}
				}


				var filtered_item = { product_id : hold_id, product_quantity : hold_quantity, product_price : hold_price, select: hold_attributes,delivery_time: delivery_time,packs:packs };
				// check if product id is existing and if not the item will be added
				if(returnIndexContainsObject(filtered_item, filtered_items) == -1)
					filtered_items.push(filtered_item);
			}
			getCartData(filtered_items);
			clear_add_products();
		}

		$('#save_item_button').attr('orig-product', '');
		$('#save_item_button').attr('orig-quantity', '');
		$('#save_item_button').attr('orig-code', '');
		$('#product-attributes-edit-holder').html('');
		$('#product-attributes-edit-holder').removeAttr('data-index');
		$('input[name="product_code"]').val('');
	});
	$( 'body' ).on("click", '#cancel_item_button', function(e) {
		e.preventDefault();
		$('#add-product-group').removeClass('hide');
		$('#edit-product-group').addClass('hide');
		$('select[name="product"]').removeAttr('data-edit');
		$('input[name="quantity"]').val(1);

		$('#save_item_button').attr('orig-product', '');
		$('#save_item_button').attr('orig-quantity', '');
		$('#save_item_button').attr('orig-code', '');
		$('#product-attributes-edit-holder').html('');
		$('#product-attributes-edit-holder').removeAttr('data-index');
		$('input[name="product_code"]').val('');

		$('#product-pack-options-edit-holder').removeAttr('data-index');
		$('#product-pack-options-edit-holder').html('');
		// var value = $('select[name="product"] option:selected').attr('value');
		// if(value)
		// {
		// 	$.ajax({
		//         dataType: 'json',
		//         type: 'post',
		//         data: {
		//             product_id: value
		//         },
		//         url: uri_base + '/order/get_product_data/',
		//         success: function(data)
		//         {
		//         	$('input[name="price"]').val(data.product.price);
		//         }
		//     });
		// }
		clear_add_products();
	});
	$( 'body' ).on("keyup", 'input[name="shipping"]', function(e) {
		var value = $(this).val();
		value = value.replace('$', '');
		$('input[name="shipping_hidden"]').val(value);
		shipping_value = value;

		setTimeout(function(){ getCartData(filtered_items); }, 500);
	});

	//discount
	$('button[name="discount_button"]').click(function(e){
		e.preventDefault();

		if( !$('#item_list_table').find('tbody').find('tr#no_items').length )
		{
		    $.ajax({
		        dataType: 'json',
		        type: 'post',
		        data: {
		            discount_action: $('input[name="discount_action"]').val(),
		            discount_code: $('input[name="discount_code"]').val(),
		            total_price: $('input[name="products_total_hidden"]').val(),
		            gst_price: $('input[name="gst_amount_hidden"]').val(),
		            delivery_charge: $('input[name="delivery_charge_hidden"]').val(),
		            shipping: $('input[name="shipping_hidden"]').val()
		        },
		        url: uri_base + '/order/get_discount_value',
		        beforeSend: function()
		        {
		        	$('#loading_area_discount').html('<i class="fa fa-spinner fa-spin"></i>');
		        },
		        success: function(data)
		        {
		        	$('input[name="discount_action"]').val(data.discount_action);
		        	$('button[name="discount_button"]').html(data.discount_action);

		        	if(data.discount_action == 'Remove')
		        	{
		        		$('input[name="discount_action"]').attr('data-code', $('input[name="discount_code"]').val());
		        		$('input[name="discount_action"]').attr('data-value', data.discount_amount);
		        	}
		        	else
		        	{
		        		$('input[name="discount_action"]').attr('data-code', '');
		        		$('input[name="discount_action"]').attr('data-value', 0);
		        	}

		        	var total_discount = parseFloat(data.discount_amount) + parseFloat($('input[name="discount_hidden"]').attr('data-categoryDiscount'));

					if(data.success)
					{
						var grand_total = parseFloat($('input[name="products_total_hidden"]').val()) + parseFloat($('input[name="shipping_hidden"]').val()) + parseFloat($('input[name="delivery_charge_hidden"]').val()) - parseFloat(total_discount);

						$('input[name="discount"]').val('$'+parseFloat(total_discount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
						$('input[name="grand_total"]').val('$'+parseFloat(grand_total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

						$('input[name="discount_hidden"]').val(total_discount);
						$('input[name="grand_total_hidden"]').val(grand_total);

						$('input[name="gst_amount"]').val('$'+parseFloat(data.gst_price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
						$('input[name="gst_amount_hidden"]').val(data.gst_price);

						// update max amount in ppamount if enabled
						if($('input[name="ppamount"]').length)
						{
							$('input[name="ppamount"]').attr('max', grand_total);
							$('input[name="ppamount"]').val('');
						}
						showGrandTotalBalanceDue();
					}
					else
						bootbox.alert('<h4>Please enter a valid discount code!</h4>');

        			$('#loading_area_discount').empty();
		        }
		    });
		}
		else
			bootbox.alert('<h4>Select item first!</h4>');
	});

	// save order only
	$('.save_order_only_button').click(function(e){
		e.preventDefault();

		var is_submit = false;

		// check if there are items
		if( !$('#item_list_table').find('tbody').find('tr#no_items').length )
			is_submit = true;
		else
		{
			is_submit = false;
			bootbox.alert('<h4>Select item first!</h4>');
			return is_submit;
		}

		if(is_submit)
		{
			$('.save_order_only_button').after('<input type="hidden" name="save_order_only" value="1">');
			$('form#add_order_form').submit();
		}
		else
		{
			return is_submit;
		}
	});

	// save new order with payment
	$('#save_button_down').click(function(e){
		e.preventDefault();

		var is_submit = false;

		// check if there are items
		if( !$('#item_list_table').find('tbody').find('tr#no_items').length )
			is_submit = true;
		else
		{
			is_submit = false;
			bootbox.alert('<h4>Select item first!</h4>');
			return is_submit;
		}

		// check if partial payment amount is correct
		if($('input[name="ppamount"]').length)
		{
			var min_amount = parseFloat($('input[name="ppamount"]').attr('min'));
			var max_amount = parseFloat($('input[name="ppamount"]').attr('max'));
			var get_amount = parseFloat($('input[name="ppamount"]').val());

			if($('input[name="pp_total_paid_amount"]').length)
			{
				var pp_total_paid_amount = parseFloat($('input[name="pp_total_paid_amount"]').val());
				max_amount = parseFloat(parseFloat(max_amount - pp_total_paid_amount).toFixed(2));

				if(max_amount <= 0)
				{
					is_submit = false;
					bootbox.alert('<h4>Payment is already completed.</h4>');
					/*bootbox.confirm({
					    message: "<h4>Payment is already completed.</h4>",
					    buttons: {
					        confirm: { // returns true
					            label: 'Save changes only',
					            className: 'btn-success'
					        },
					        cancel: { // returns false
					            label: 'OK',
					            className: 'btn-primary'
					        }
					    },
					    callback: function (result) {
					        // result will return boolean
					        if(result)
					        {
					        	$('#save_button_down').after('<input type="hidden" name="save_order_only" value="1">');
					        	$('#save_button_down').closest('form').submit();
					        }
					    }
					});*/
					return is_submit;
				}
			}

			if(get_amount >= min_amount && get_amount <= max_amount)
			{
				is_submit = true;
			}
			else
			{
				is_submit = false;
				bootbox.alert('<h4>Partial payment amount must between $'+min_amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+' - $'+max_amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</h4>');
				return is_submit;
			}
		}
		else
		{
			is_submit = true;
		}

		// check if creditcart form are showing
		if(($('#form_payment_method').val() == securePayCode || $('#form_payment_method').val() == eWayCode) && $('#total-payment-section').html().length)
		{

			if($('input[name="ccnumber"]').val() == '' || $('input[name="exmonth"]').val() == '' || $('input[name="exyear"]').val() == '' || $('input[name="cvv"]').val() == '')
			{
				is_submit = false;
				bootbox.alert('<h4>Fill required fields in credit card details section!</h4>');
				return is_submit;
			}
			else
				is_submit = true;
		}

		if(is_submit)
		{
			$('form#add_order_form').submit();
		}
		else
		{
			return is_submit;
		}
	});

	// http://jsfiddle.net/de8QK/
	// start - disable tab
    $('#step_one_tab').click(function(event){
    	if($(this).hasClass('is_disabled'))
    		return false;
    	else
    		return true;
    });
    $('#step_two_tab').click(function(event){
    	if($(this).hasClass('is_disabled'))
    		return false;
    	else
    		return true;
    });

     $('#step_three_tab').click(function(event){
    	if($(this).hasClass('is_disabled'))
    		return false;
    	else
    		return true;
    });
    
	payment_form();
	$('#form_payment_method').change(function(e){
		e.preventDefault();

		payment_form();
	});

	$('#back_button_down2').click(function(e){
		e.preventDefault();

		$('#step_two_tab').removeClass('is_disabled');
		$('#step_two_tab a').trigger('click');
		$('#step_two_tab').addClass('is_disabled');
		$('#step_three_tab').addClass('disabled');
		$('#step_three_tab').addClass('is_disabled');
	});

    $('#same_as_billing').click(function(e){

        if($(this).is(':checked'))
        {
            $('select[name="shipping_title"]').val($('select[name="shipping_title"]').attr('data-original'));
            $('input[name="shipping_first_name"]').val($('input[name="shipping_first_name"]').attr('data-original'));
            $('input[name="shipping_last_name"]').val($('input[name="shipping_last_name"]').attr('data-original'));
            $('input[name="shipping_company"]').val($('input[name="shipping_company"]').attr('data-original'));
            $('input[name="shipping_address"]').val($('input[name="shipping_address"]').attr('data-original'));
            $('input[name="shipping_suburb"]').val($('input[name="shipping_suburb"]').attr('data-original'));
            $('select[name="shipping_country"]').val($('select[name="shipping_country"]').attr('data-original'));
            $('select[name="shipping_state"]').val($('select[name="shipping_state"]').attr('data-original'));
            $('input[name="shipping_postcode"]').val($('input[name="shipping_postcode"]').attr('data-original'));
            $('input[name="shipping_phone"]').val($('input[name="shipping_phone"]').attr('data-original'));
        }
        else
        {
            $('select[name="shipping_title"]').val('');
            $('input[name="shipping_first_name"]').val('');
            $('input[name="shipping_last_name"]').val('');
            $('input[name="shipping_company"]').val('');
            $('input[name="shipping_address"]').val('');
            $('input[name="shipping_suburb"]').val('');
            $('select[name="shipping_country"]').val('');
            $('select[name="shipping_state"]').val('');
            $('input[name="shipping_postcode"]').val('');
            $('input[name="shipping_phone"]').val('');
        }
    });

    $('#same_as_billing_edit').click(function(e){

        if($(this).is(':checked'))
        {
            $('select[name="shipping_title"]').val($('select[name="shipping_title"]').attr('data-customer'));
            $('input[name="shipping_first_name"]').val($('input[name="shipping_first_name"]').attr('data-customer'));
            $('input[name="shipping_last_name"]').val($('input[name="shipping_last_name"]').attr('data-customer'));
            $('input[name="shipping_company"]').val($('input[name="shipping_company"]').attr('data-customer'));
            $('input[name="shipping_address"]').val($('input[name="shipping_address"]').attr('data-customer'));
            $('input[name="shipping_suburb"]').val($('input[name="shipping_suburb"]').attr('data-customer'));
            $('select[name="shipping_country"]').val($('select[name="shipping_country"]').attr('data-customer'));
            $('select[name="shipping_state"]').val($('select[name="shipping_state"]').attr('data-customer'));
            $('input[name="shipping_postcode"]').val($('input[name="shipping_postcode"]').attr('data-customer'));
            $('input[name="shipping_phone"]').val($('input[name="shipping_phone"]').attr('data-customer'));
        }
        else
        {
            $('select[name="shipping_title"]').val($('select[name="shipping_title"]').attr('data-original'));
            $('input[name="shipping_first_name"]').val($('input[name="shipping_first_name"]').attr('data-original'));
            $('input[name="shipping_last_name"]').val($('input[name="shipping_last_name"]').attr('data-original'));
            $('input[name="shipping_company"]').val($('input[name="shipping_company"]').attr('data-original'));
            $('input[name="shipping_address"]').val($('input[name="shipping_address"]').attr('data-original'));
            $('input[name="shipping_suburb"]').val($('input[name="shipping_suburb"]').attr('data-original'));
            $('select[name="shipping_country"]').val($('select[name="shipping_country"]').attr('data-original'));
            $('select[name="shipping_state"]').val($('select[name="shipping_state"]').attr('data-original'));
            $('input[name="shipping_postcode"]').val($('input[name="shipping_postcode"]').attr('data-original'));
            $('input[name="shipping_phone"]').val($('input[name="shipping_phone"]').attr('data-original'));
        }
    });
});

function clear_add_products()
{
	$('select[name="product"]').val('').trigger('change');
	$('input[name="price"]').val('');
	$('input[name="quantity"]').val(1);
	$('input[name="product_code"]').val('');
}

function getCartData(filtered_items)
{
    $.ajax({
        dataType: 'json',
        type: 'post',
        data: {
            items: filtered_items,
            shipping_value: shipping_value,
            discount_value: $('input[name="discount_action"]').attr('data-value')
        },
        url: uri_base + '/order/get_cart_data',
        beforeSend: function()
        {
        	$('#loading_area_item').html('<i class="fa fa-spinner fa-spin"></i>');
        },
		error: function(e){
            bootbox.alert('<h4>There are old products does not exist in the database, please try again.</h4>');
            $('#loading_area_item').empty();
		},
        success: function(data)
        {
        	
			replaceDataInOrderTable(data.items);

			// order summary
			$('input[name="products_total"]').val('$'+parseFloat(data.products_total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$('input[name="discount"]').val('$'+parseFloat(data.discount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$('input[name="shipping"]').val('$'+parseFloat(data.shipping).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$('input[name="gst_amount"]').val('$'+parseFloat(data.gst_amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$('input[name="delivery_charge"]').val('$'+parseFloat(data.delivery_charge).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$('input[name="sub_total"]').val('$'+parseFloat(data.sub_total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$('input[name="grand_total"]').val('$'+parseFloat(data.grand_total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

			$('input[name="products_total_hidden"]').val(data.products_total);
			$('input[name="discount_hidden"]').val(data.discount);
			$('input[name="discount_hidden"]').attr('data-categoryDiscount', data.category_discount);
			$('input[name="shipping_hidden"]').val(data.shipping);
			$('input[name="gst_amount_hidden"]').val(data.gst_amount);
			$('input[name="delivery_charge_hidden"]').val(data.delivery_charge);
			$('input[name="sub_total_hidden"]').val(data.sub_total);
			$('input[name="grand_total_hidden"]').val(data.grand_total);

			// update max amount in ppamount if enabled
			if($('input[name="ppamount"]').length)
			{
				$('input[name="ppamount"]').attr('max', data.grand_total);
				$('input[name="ppamount"]').val('');
			}
			showGrandTotalBalanceDue();

			// check if there are still row in the table, and if not replace a place holder
			if( $('#item_list_table').find('tbody').find('tr').length == 0 )
			{
				$('#item_list_table').find('tbody').html('<tr id="no_items"><td colspan="7">There are no items.</td></tr>');
			}

        	$('#loading_area_item').empty();
        }
    });
}

function replaceDataInOrderTable(filtered_items)
{
	$('#item_list_table').find('tbody').html('');
	
	var tbody = '';
	for (var i = 0; i < filtered_items.length; i++) {
		var quantity = filtered_items[i].product_quantity;
		var price = filtered_items[i].price;
		var attributes = '';
		for (var x in filtered_items[i].select)
			attributes += '<input type="hidden" name="select_attributes['+i+']['+x+']" value="'+filtered_items[i].select[x]+'">';

		if(!price)
			price = 0;

		var time_element = '';
		if($('#product_time_template select').length)
		{
			var _time = $('#product_time_template select').clone();
			if(filtered_items[i].delivery_time != '' || filtered_items[i].delivery_time != 'N/A')
			{
				_time.find('option[value="'+filtered_items[i].delivery_time+'"]').attr('selected', 'selected');
			}
			time_element = _time.get(0).outerHTML;
		}

		tbody += '<tr data-index="'+i+'"><td>'+time_element+'</td><td><input type="hidden" name="product_id[]" value="'+filtered_items[i].product_id+'">'+filtered_items[i].title+'</td><td><div class="product-attributes-holder">'+attributes+'</div><input type="hidden" name="product_code[]" value="'+filtered_items[i].code+'">'+filtered_items[i].code+'</td><td><input type="hidden" name="product_price[]" value="'+price+'">$'+parseFloat(price).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'</td><td><input type="hidden" name="product_quantity[]" value="'+filtered_items[i].product_quantity+'">'+filtered_items[i].product_quantity+'</td><td>$'+parseFloat(price*quantity).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')+'<input type="hidden" name="product_delivery_charge[]" value="'+filtered_items[i].delivery_charge+'"></td><td class="icon center"><a class="text-success" href="#edit_product">Edit</a></td><td class="icon center"><a class="text-danger" href="#remove_product">Delete</a></td></tr>';

		// add pack options
		if(filtered_items[i].packs)
		{
			var packs = filtered_items[i].packs;
			for(var a = 0; a < packs.length; a++)
			{
				tbody += '<tr data-index="'+i+'" data-pack_options="true" data-pack_id="'+packs[a].id+'"> <td>'+time_element.replace('delivery_time[]', 'pack_delivery_time['+filtered_items[i].product_id+']['+packs[a].id+']')+'</td> <td colspan="7"> <input type="hidden" name="packs['+filtered_items[i].product_id+']['+packs[a].id+'][name]" value="'+packs[a].name+'"> <input type="hidden" name="packs['+filtered_items[i].product_id+']['+packs[a].id+'][types]" value="'+packs[a].types+'"> <input type="hidden" name="packs['+filtered_items[i].product_id+']['+packs[a].id+'][selection_limit]" value="'+packs[a].selection_limit+'"> '+packs[a].name+' ';
				if(packs[a].products)
				{
					var hold_products = [];
					var hold_products_html = "";
					for(var b = 0; b < packs[a].products.length; b++)
					{
						hold_products.push(packs[a].products[b].name);
						hold_products_html += '<input data-pack_option_product="'+packs[a].products[b].id+'" type="hidden" name="packs['+filtered_items[i].product_id+']['+packs[a].id+'][products]['+packs[a].products[b].id+']" value="'+packs[a].products[b].name+'">';
					}

					if(hold_products)
					{
						tbody += ' ( '+hold_products.join(', ')+' ) '+hold_products_html;
					}
				}
				tbody += '</td></tr>';
			}
		}
	}

	$('#item_list_table').find('tbody').html(tbody);
}

function returnIndexContainsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i].product_id === obj.product_id  && JSON.stringify(list[i].select) === JSON.stringify(obj.select)) {
            return i;
        }
    }

    return -1;
}

function validate_step_1()
{
	var returnVal = false;

	$('#step_one').find('.require').each(function(index, elem){
		if($(elem).val() <= 0) 
		{
			returnVal = true;
		} 
		else 
		{
			if($(elem).attr('type') == 'email' && !isEmail($(elem).val()))  
			{
				returnVal = true;
			}
		}
	});

	return returnVal;
}

function validate_add_item()
{
	var returnVal = false;

	$('#add_item_form').find('.require').each(function(index, elem){
		if($(elem).val() <= 0) 
		{
			returnVal = true;
		} 
		else 
		{
			if( $(elem).attr('type') == 'number' && $(elem).val() < 1 )  
			{
				returnVal = true;
			}
		}
	});

	return returnVal;
}

function validate_pack_options()
{
	if($('#pack-options').html().length)
	{
		if($('#pack-options').find('div.form-group').find('input[type="checkbox"]:checked').length)
		{
			var check_selection_limit_per_pack = false;
			$('#pack-options').find('div.form-group[data-type="included"]').each(function(index, elem){
				if($(elem).find('input[type="checkbox"]:checked').length == 0)
				{
					check_selection_limit_per_pack = 'Select '+parseInt($(elem).attr('data-selection_limit'))+' item(s) for '+$(elem).find('label.pack-title').text();
					return false;
				}
				if($(elem).find('input[type="checkbox"]:checked').length > parseInt($(elem).attr('data-selection_limit')))
				{
					check_selection_limit_per_pack = 'Selection limit is '+parseInt($(elem).attr('data-selection_limit'))+' for '+$(elem).find('label.pack-title').text();
					return false;
				}
			});
			if(check_selection_limit_per_pack)
				return check_selection_limit_per_pack;
			
			// if type is extras check if product is checked and check the minimum_order of product
			var check_minimum_order_product = false;
			$('#pack-options').find('div.form-group[data-type="extras"]').find('input[type="checkbox"]:checked').each(function(index, elem){
				if($(elem).closest('label').find('input[type="number"]').val() < parseInt($(elem).closest('label').find('input[type="number"]').attr('data-minimum_order')))
				{
					check_minimum_order_product = 'Minimum order of '+$(elem).closest('label').attr('data-product_title')+' is '+parseInt($(elem).closest('label').find('input[type="number"]').attr('data-minimum_order'));
					return false;
				}
			});
			if(check_minimum_order_product)
				return check_minimum_order_product;
		}
		else
			return "Please select product pack(s)";
	}

	return false;
}

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function payment_form() {
	if($('#form_payment_method').val() == 'cash_on_delivery' || $('#form_payment_method').val() == 'cash')
	{
		$('input[name="payment_form"]').attr('disabled', 'disabled');
	}
	else
	{
		$('input[name="payment_form"]').removeAttr('disabled');
	}

	$('#payment_type').val($('#form_payment_method').val());



	var html = '';
	if($('#form_payment_method').val() == securePayCode || $('#form_payment_method').val() == eWayCode)
	{
		var monthNames = ["January", "February", "March", "April", "May", "June",
		  "July", "August", "September", "October", "November", "December"
		];
		var option;
		var option2;

		$.each(monthNames,function(i,obj){
			option += '<option value="'+(i+1)+'">'+obj+'</option>';
		});

		var currentYear = new Date().getFullYear();
		for(var x = currentYear; x < (currentYear + 30); x++){
			option2 += '<option value="'+x+'">'+x+'</option>';
		}

		html += ' <div class="form-group "> <label class="col-sm-4 control-label">&nbsp;</label> <div class="col-sm-8"> <h3 class="title-legend">Credit Card Details</h3> </div> </div> <div class="form-group "> <label class="col-sm-4 control-label">Card Number <span class="text-danger">*</span></label> <div class="col-sm-8"> <input class="form-control" name="ccnumber" required autocomplete="off"> </div> </div> <div class="form-group "> <label class="col-sm-4 control-label">Expiration Date (MO / YEAR) <span class="text-danger">*</span></label> <div class="col-sm-8" style="padding:0;"> <div class="col-sm-6"> <select name="exmonth" class="form-control" autocomplete="off">' + option + '</select> </div> <div class="col-sm-6"> <select name="exyear" class="form-control" autocomplete="off">' + option2 + '</select> </div> </div> </div> <div class="form-group "> <label class="col-sm-4 control-label">CVV <span class="text-danger">*</span></label> <div class="col-sm-8"> <input class="form-control" name="cvv" required maxlength="4" autocomplete="off"> </div> </div>';
	}
	$('#total-payment-section').html(html);
}

showGrandTotalBalanceDue();
function showGrandTotalBalanceDue()
{
	var grand_total = parseFloat($('input[name="grand_total_hidden"]').val().replace(',', ''));
	var amount_paid = parseFloat($('#show_amount_paid').text().replace(',', ''));
	var balance_due = parseFloat(grand_total - amount_paid);

	$('#show_grand_total').text(grand_total.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
	$('#show_balance_due').text(balance_due.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

	if(balance_due <= 0)
	{
		$('input[name="ppamount"]').attr('disabled', 'disabled');
	}
	else
	{
		$('input[name="ppamount"]').removeAttr('disabled');
	}
}

function topContinueBackButton()
{
	var active_tab_content_id = $('#step_guide').find('li.active').find('a').attr('href');
	var has_back = $(active_tab_content_id).find('button[name="back"]').length;
	var has_continue = $(active_tab_content_id).find('button[name="continue"]').length;
	if(has_back)
		$('#back_button_top, .save_order_only_button').show();
	else
		$('#back_button_top, .save_order_only_button').hide();
	if(has_continue)
		$('#continue_button_top').show();
	else
		$('#continue_button_top').hide();
}