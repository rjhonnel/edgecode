$(document).ready(function(){

	if($(".use_cover_image").length)
	{
		$(".use_cover_image").each(function(){
			
			$(this).click(function(e){
	
				var column = $(this).parents('tr').first();
				var url = $(this).attr('rel');
	
				// Show loader
				$(this).css('visibility', 'hidden');
				// column.find('.hotspot_image div').prepend('<i class="fa fa-spinner fa-spin"></i>');
                
				if($(this).is(":checked"))
				{
					column.find('.hotspot_image .default').hide();
					column.find('.hotspot_image .cover').show();
				}
				else
				{
					column.find('.hotspot_image .default').show();
					column.find('.hotspot_image .cover').hide();
				}
	
				// Remove left over loader image
		    	$(".loader_image").remove();
		    	$(this).css('visibility', 'visible');
			});
			
		});
	}
	
	$(".hotspot_cover_holder").on('click', function(e){

		// Note that we substract half of image widh and height si image will be center positioned (10px in this case)sss
		var posX = e.pageX - $(this).position().left - 10;
		var posY = e.pageY - $(this).position().top - 10;

		// Plase hotspot image at clicked position
		$(this).append('<i class="fa fa-spinner fa-spin"></i>');
		//$('img.hotspot:last').draggable({ containment: "parent" });

		var create_url 		= uri_base + '/application/hotspot/hotspot/' + application_id;
		var redirect_url 	= uri_base + '/application/hotspot/show/' + application_id + '/';
		
		$.ajax({
			type: 'POST',
			url: create_url,
			data: {
				'position_x': posX,
				'position_y': posY,
				'create': 1
			},
			success: function(data) {
				console.log(data);
				data = jQuery.parseJSON(data);
				if(data.hotspot_id)
				{
					window.location = redirect_url + data.hotspot_id;
				}
			},
		  	async: false
		});
		
	});

	// Prevent creating hotspots over recycle bin and existing hotspots
	$(".hotspot_cover_holder .trash, .hotspot_cover_holder .hotspot").on('click', function(e){
		e.stopPropagation();
	});

	if($('.hotspot').length)
	{
		// Bind event to handle dragging of hotspots
		$('.hotspot').draggable({ 
			containment: "parent",
			start: function(e, ui){
				// Clear previous messages if exists
//				ui.helper.parents('.panelContent').first().find('.sort_message_container').first().html('<img class="loader_image" src="'+theme_path+'images/loading.gif" style="float: right; width: 25px; height: 25px; margin: -5px 0 0 0; "/>');
				$(".header_messages").html('<i class="fa fa-spinner fa-spin"></i>');
			},
			stop: function(e, ui){
	
				var posX = ui.helper.position().left;
				var posY = ui.helper.position().top;
	
				var update_url = uri_base + '/application/hotspot/hotspot/' + application_id + '/' + ui.helper.attr('rel');
				
				$.ajax({
					type: 'POST',
					url: update_url,
					data: {
						'position_x': posX,
						'position_y': posY,
						'update': 1
					},
					success: function(data) {
						// Remove left over loader image
				    	$(".loader_image").remove();
				    	
						//data = jQuery.parseJSON(data);
//						ui.helper.parents('.panelContent').first().find('.sort_message_container').first().html(data);
						$(".header_messages").html(data);
					},
					error: function(){
						// Remove left over loader image
				    	$(".loader_image").remove();
					},
				  	async: false
				});
			} 
		});
	}
	
	if($('.trash').length)
	{
		// Bind event to handle droping of hotspots inside
		$(".trash").droppable({
	        accept: ".hotspot_cover_holder > .hotspot",
	        drop: function( e, ui ) {
	        	delete_hotspot( ui.draggable );
	        }
	    });
	}

	// Delete hotspot function
    var delete_hotspot = function(hotspot){
    	var delete_url 		= uri_base + '/application/hotspot/delete_infotab_image/' + hotspot.attr("rel") + '/' + application_id;
    	var redirect_url 	= uri_base + '/application/hotspot/show/' + application_id;
    	
    	$.ajax({
			type: 'POST',
			url: delete_url,
			success: function(data) {
	    		// Remove left over loader image
		    	$(".loader_image").remove();
	    	
        		//data = jQuery.parseJSON(data);
//				hotspot.parents('.panelContent').first().find('.sort_message_container').first().html(data);
				$(".header_messages").html(data);
                
				// We need to remove href attribute because 
				// othervise page will reload after ajax call
        		hotspot.removeAttr('href');
    			hotspot.remove();

    			if(hotspot.attr("rel") == hotspot_id)
        			window.location = redirect_url;
			},
			error: function(){
				// Remove left over loader image
		    	$(".loader_image").remove();
			},
		  	async: false
		});
    };
	 
});