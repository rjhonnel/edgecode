$(document).ready(function(){
    
    // Live search
    var search_table = function(){
        var value = $(".search_holder ").find("input[name=title]").val();
        
        var table = $(".search_table tbody");
        
        $("tr td:nth-child(3)", table).each(function(){
            if($(this).html().toLowerCase().indexOf(value) >= 0)
            {
                $(this).parents("tr:first").show();
            }
            else
            {
                $(this).parents("tr:first").hide();
            }
        });
        
        if($('tr:visible', table).not(".no_items").length == 0)
            $(".no_items", table).show();
        else
            $(".no_items", table).hide();
        
        $(".info_table_loading").hide();
    };
    
    var initiate_search = function(){
        
        // Disable browser autocomplete on search field
        $(".search_holder ").find("input[name=title]").attr("autocomplete", "off");
        
        $(".info_table_loading").show();
        
		var timer 	= $(".search_holder ").find("input[name=title]").data('timeout');
		var n 		= 2;// the number of seconds to wait

        if(timer) {
			clearTimeout(timer);
			$(".search_holder ").find("input[name=title]").removeData('timeout');
		}

        $(".search_holder ").find("input[name=title]").data('timeout', setTimeout(search_table, 600 * n));
	};
	
	$(".search_holder ").find("input[name=title]").on('keyup', function(e){
		initiate_search();
	});
});