$(document).ready(function(){
	var parse_info_table = function(){

		var value 	= $(".info_table").val();

		$.ajax({
			type: 'POST',
	  		url: uri_base + '/product/infotab/parse_info_table',
	  		data: {
				table: value
		  	},
	  		success: function(data) {
		  		console.log(data);

		  		if(data != '')
    				$('.info_table_preview').html(data);
		  		else
    				$('.info_table_preview').html($(".info_table_empty").html());
	  		}
		});
	};

	var initiate_parsing = function(){
        
		$('.info_table_preview').html($(".info_table_loading").html());
			
		var timer 	= $('.info_table').data('timeout');
		var n 		= 2;// the number of seconds to wait

        if(timer) {
			clearTimeout(timer);
			$('.info_table').removeData('timeout');
		}

        $('.info_table').data('timeout', setTimeout(parse_info_table, 600 * n));
	};
	
	$(".info_table").on('keyup', function(){
		initiate_parsing();
	});

	initiate_parsing();
});