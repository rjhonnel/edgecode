(function($){

	checkCurrentProductType();

	$('#pack-section').find('select[name="pack_product_list"]').select2();

	$('select[name="product_type"]').change(function(){
		checkCurrentProductTypeAnimate();
	});

	var selection_order = 1;
	if($('#pack-additional-selection').find('div.selection-group').length > 0)
		selection_order = $('#pack-additional-selection').find('div.selection-group').length + 1;

	$('#add-another-pack-selection').click(function(e){
		e.preventDefault();
		var html = ' <div data-order="'+selection_order+'" class="panel-body table-bordered mb-10px selection-group"> <div class="row"> <div class="col-md-12"> <a href="#" class="pull-right remove-pack"><i class="fa fa-close"></i></a> </div> </div> <div class="row"> <div class="col-md-6"> <div class="form-group"> <label class="control-label">Option Name <span class="text-danger">*</span></label> <input class="form-control" name="packs['+selection_order+'][name]" type="text" > </div> <div class="form-group"> <label class="control-label">Option Description</label> <textarea class="form-control" name="packs['+selection_order+'][description]"></textarea> </div> <div class="form-group included"> <label class="control-label">Product Selection Limit <span class="text-danger">*</span></label> <input class="form-control" type="number" min="1" name="packs['+selection_order+'][selection_limit]"> </div> </div> <div class="col-md-6"> <div class="form-group"> <label class="control-label">Option Type</label> <select class="form-control pack_type" name="packs['+selection_order+'][type]"> <option value="included">Included (no additional cost)</option> <option value="extras">Extras (selections charged at item price)</option> </select> </div> <div class="form-group btn-file"> <label class="control-label">Option Image</label> <input class="form-control" type="file" name="packs['+selection_order+'][image]"> </div> </div> <div class="col-md-12"> <label class="control-label">Products <span class="text-danger">*</span></label> <table class="table table-striped table-bordered"> <thead> <tr> <td>'+$('#product-template').html()+'</td> <td> <a href="#" class="pull-right add-items-to-selection"><i class="fa fa-plus"></i></a> </td> </tr> </thead> <tbody class="item-list-section"> </tbody> </table> </div> </div> </div> ';
		$('#pack-additional-selection').append(html);

		$('#pack-additional-selection').find('div.selection-group[data-order="'+selection_order+'"]').find('select[name="pack_product_list"]').select2();
		
		/******* CUSTOM FILE INPUT *******/
		$('#pack-additional-selection').find('div.selection-group[data-order="'+selection_order+'"]').find('input[type="file"]').wrap('<div class="file input-group">');
		$('#pack-additional-selection').find('div.selection-group[data-order="'+selection_order+'"]').find('input[type="file"]').after('<input type="text" class="form-control file_place" /><span class="input-group-addon">Browse</span>');
		$('#pack-additional-selection').find('div.selection-group[data-order="'+selection_order+'"]').find('input[type="file"]').bind('change focus click', function(){
			$(this).fileName();
		});
		/********* END OF: CUSTOM FILE INPUT ********/
		selection_order++;
	});

	$('body').on('click','a.remove-pack', function(e){
		e.preventDefault();
		$(this).closest('.selection-group').remove();
		selection_order-=1;

		var count_selection_order = 1;
		$('#pack-additional-selection').find('div.selection-group').each(function(index, elem){
			$(elem).attr('data-order', count_selection_order);
			count_selection_order++;
		});
	});

	$('body').on('click', 'a.add-items-to-selection', function(e){
		e.preventDefault();
		if($(this).closest('table').find('select[name="pack_product_list"]').val())
		{
			var option_text = $(this).closest('table').find('select[name="pack_product_list"] option:selected').text();
			var get_val = $(this).closest('table').find('select[name="pack_product_list"]').val();

			if($(this).closest('table').find('tbody.item-list-section').find('tr[data-value="'+get_val+'"]').length == 0)
			{
				var html = ' <tr data-value="'+get_val+'"> <td> '+option_text+' <input type="hidden" value="'+get_val+'" name="packs['+$(this).closest('div.selection-group').attr('data-order')+'][products][]"> </td> <td> <a href="#" class="pull-right remove-items-to-selection"><i class="fa fa-close"></i></a> </td> </tr> ';
				$(this).closest('table').find('tbody.item-list-section').append(html);
			}
		}
	});
	$('body').on('click', 'a.remove-items-to-selection', function(e){
		e.preventDefault();
		$(this).closest('tr').remove();
	});
	$('body').on('click', 'select.pack_type', function(e){
		if($(this).val() == 'included')
		{
			$(this).closest('div.selection-group').find('.included').show();
		}
		else
		{
			$(this).closest('div.selection-group').find('.included').hide();
			$(this).closest('div.selection-group').find('.included').find('input').val('');
		}
	});

	function checkCurrentProductType()
	{
		var product_type = $('select[name="product_type"]').val();
		if(product_type == 'pack')
			$('#pack-section').show();
		else
			$('#pack-section').hide();
	}

	function checkCurrentProductTypeAnimate()
	{
		var product_type = $('select[name="product_type"]').val();
		if(product_type == 'pack')
		{
			$('#pack-section').show();
			$('html,body').animate({
		        scrollTop: $("#pack-section").offset().top
		    },'slow');
		}
		else
			$('#pack-section').hide();
	}

})(jQuery);