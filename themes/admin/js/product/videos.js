$(document).ready(function(){
	if($(".video_submit").length)
	{
		$(".video_submit").each(function(){
			$(this).click(function(e){
				e.preventDefault();
				var table = $(this).parents('table').last();
				var column = $(this).parents('tr').first().parents('tr').first();
				var url = table.attr('video');
				var data = $(this).parents('tr').first().find('.video_url').first().val();
	
				// Show loader
				table.parents('.panelContent').first().find('.sort_message_container').first().html('<i class="fa fa-spinner fa-spin"></i>');
				
				$.ajax({
					type: 'POST',
					url: url,
					data: {'url': data},
					success: function(data) {
						data = jQuery.parseJSON(data);
						table.parents('.panelContent').first().find('.sort_message_container').first().html(data.message);
	
						if(data.response)
						{
							column.find('.video_thumbnail .default').hide();
							column.find('.video_thumbnail .new').remove();
							// Populate form with return data
							column.find('.video_title').val(data.response.title);
							column.find('.video_thumbnail').prepend('<img class="new" width="80" src="' + data.response.thumbnail.small + '" />');
	
							// Check "Use YouTube?" checkbox if exist
							column.find(".video_delete_image").attr("checked", "checed");
						}
						else
						{
							// Remove inserted values
							column.find('.video_thumbnail .default').show();
							column.find('.video_thumbnail .new').remove();
							column.find('.video_title').val('');
						}
					},
				  	async: false
				});
	
				// Remove left over loader image
		    	$(".loader_image").remove();
			});
			
		});
	}

	if($(".video_delete_image").length)
	{
		$(".video_delete_image").each(function(){
	
			$(this).click(function(e){
	
				var table = $(this).parents('table').last();
				var column = $(this).parents('tr').first();
				var url = table.attr('video');
				var data = $(this).parents('tr').first().find('.video_url').first().val();
	
				// Show loader
				$(this).css('visibility', 'hidden');
				column.find('.video_thumbnail div').prepend('<i class="fa fa-spinner fa-spin"></i>');
				
				if($(this).is(":checked"))
				{
					$.ajax({
						type: 'POST',
						url: url,
						data: {'url': data},
						success: function(data) {
							data = jQuery.parseJSON(data);
	
							if(data.response)
							{
								column.find('.video_thumbnail .default').hide();
								column.find('.video_thumbnail .new').remove();
								// Populate form with return data
								column.find('.video_thumbnail').prepend('<img class="new" width="80" src="' + data.response.thumbnail.small + '" />');
							}
							else
							{
								// Remove inserted values
								column.find('.video_thumbnail .default').show();
								column.find('.video_thumbnail .new').remove();
							}
						},
					  	async: false
					});
				}
				else
				{
					column.find('.video_thumbnail .default').show();
					column.find('.video_thumbnail .new').remove();
				}
	
				// Remove left over loader image
		    	$(".loader_image").remove();
		    	$(this).css('visibility', 'visible');
			});
			
		});
	}
	
});