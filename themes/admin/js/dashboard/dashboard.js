$(document).ready(function() {
    var chartName = 'dashboardChart';
    var ctx = document.getElementById("dashboardChart");
    var data = {
        labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
        datasets: [
            {
                label: "Sales",
                tension: 0.2,
                backgroundColor: "rgba(75, 172, 238, 0.15)",
                borderWidth:2,
                borderColor: "#4bacee",
                pointBorderColor: "#fff",
                pointBackgroundColor: "#4bacee",
                pointRadius: 4,
                pointHoverBackgroundColor: "#4bacee",
                pointHoverBorderColor: "#4bacee",
                data: sales_chart,
            },
            {
                label: "Visits",
                tension: 0.2,
                backgroundColor: "rgba(167, 181, 197, 0.15)",
                borderWidth:2,
                borderColor: "#a7b5c5",
                pointBorderColor: "#fff",
                pointBackgroundColor: "#a7b5c5",
                pointRadius: 4,
                pointHoverBackgroundColor: "#a7b5c5",
                pointHoverBorderColor: "#a7b5c5",
                data: visits_chart,
            }
        ]
    };
    var myLineChart = new Chart(ctx, {
        type: 'line',
        data: data,
        responsive: true
    });

});