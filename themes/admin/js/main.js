                                                                                                                                /**
 * Hide/Show dates where active in period is applicable
 */
function show_dates(element, match, toggle){
	if(toggle.length > 0)
		element.val() == match ? toggle.show() : toggle.hide();
};

function count_items($value){
    $value = typeof $value !== 'undefined' ? $value : 'action';
    $('.items_selected').text($("input[name^='" + $value + "']:checked").length);
}

$(document).ready(function() {
	
    // Show and hide filters on listing pages
    // Also save filters state in cookie to preserve it on page refresh
    if($(".toggle-filters").length > 0)
    {
        $(".toggle-filters").click(function(e){
            e.preventDefault();
            
            $('.blue_filters').toggleClass('is-show');
            
        });
    }
    // End of listing filters
    
    // Change page layout if there is no sidebar on page
    // Because pages need to be 100% width all the time
	if ( $(".elements_holder .sidebar").length < 1 ) {
		
  		$(".elements_holder .content").addClass("no_sidebar");
  		$(".elements_holder .breadcrumbs_position").addClass("no_sidebar");
  	};
	// End of sidebar
	
    $("#sidetreecontrol a").first().hide();
    $("#sidetreecontrol a").click(function(e){
        e.preventDefault();
        $("#sidetreecontrol a").toggle();
    });
    $("#sidetreecontrol2 a").first().hide();
    $("#sidetreecontrol2 a").click(function(e){
        e.preventDefault();
        $("#sidetreecontrol2 a").toggle();
    });
    
    // Display tooltips on all elements that have attribute rel of tooltip
    // Text for from title attribute will be used as tooltip text
    $("[rel='tooltip']").tooltip();
    


	//fancybox
	if($('.fancybox').length > 0)
	{
		$(".fancybox").fancybox();
	}

	
	// quick add tab
	if($('.quickTab').length > 0)
	{
		$(".quickTab").click(function () {
			$("#quickPanel").animate({width:'toggle'},350);
		});
	}


	
	// treeview
	if($('#tree').length > 0)
	{
		$("#tree").treeview({
			collapsed: true,
			animated: "fast",
			control:"#sidetreecontrol",
			persist: "location"
		});
	}
	
	if($('#tree2').length > 0)
	{
		$("#tree2").treeview({
			collapsed: true,
			animated: "fast",
			control:"#sidetreecontrol2",
			persist: "location"
		});
	}
	
	if($("input[name='cat_ids[]']").length > 0)
	{
		// Excpand checked input parent
		$("input[name='cat_ids[]']").each(function(){
			if($(this).is(":checked"))
			{
				$(this).parent().parent().parent().show();
				$(this).parent().parent().parent().parent().removeClass('expandable').addClass('collapsable');
				$(this).parent().parent().parent().siblings(".hitarea").removeClass('expandable-hitarea').addClass('collapsable-hitarea');
			}
		});
	
		// Expand lists that have expanded sub-lists
		$($("li.expandable").get().reverse()).each(function(){
			if($(this).children('ul').children('li.collapsable').length > 0)
			{
				$(this).children('ul').show();
				$(this).removeClass('expandable').addClass('collapsable');
				$(this).children(".hitarea").removeClass('expandable-hitarea').addClass('collapsable-hitarea');
			}
		});
	}
    
	if($("input[name='group_ids[]']").length > 0)
	{
		// Excpand checked input parent
		$("input[name='group_ids[]']").each(function(){
			if($(this).is(":checked"))
			{
				$(this).parent().parent().parent().show();
				$(this).parent().parent().parent().parent().removeClass('expandable').addClass('collapsable');
				$(this).parent().parent().parent().siblings(".hitarea").removeClass('expandable-hitarea').addClass('collapsable-hitarea');
			}
		});
	
		// Expand lists that have expanded sub-lists
		$($("li.expandable").get().reverse()).each(function(){
			if($(this).children('ul').children('li.collapsable').length > 0)
			{
				$(this).children('ul').show();
				$(this).removeClass('expandable').addClass('collapsable');
				$(this).children(".hitarea").removeClass('expandable-hitarea').addClass('collapsable-hitarea');
			}
		});
	}
	
    // Helper function used for sortable plugin
    var fixHelper = function(e, ui) {
		ui.children().each(function() {
			$(this).width($(this).width());
		});
		return ui;
	};
	
	// sortable
	if($(".sortable").length > 0)
	{
		$( "table.sortable" ).sortable({
			helper: fixHelper,
			items: "tr:not(.nodrop.nodrag, tr tr)",
			handle: '.dragHandle',
			start: function(e, ui){
				table	= $(this);
				// Clear previous messages if exists
                table.prev(".header_messages").html('<i class="fa fa-spinner fa-spin"></i>');
				$(".ui-sortable-placeholder", table).height($("tr[id^='sort_']:first", table).height());
		    },
			update: function(e, ui){
				serial 	= $(this).sortable('serialize');
				table	= $(this);
				url		= table.attr('rel');
				
				// Serialize data
				var serial = $(this).sortable("toArray");
				var serialized = new Array();
				$.each(serial, function(key, value){
					if(value.indexOf('sort_') != -1)
					{
						serialized.push(value.replace('sort_', ''));
					}
				});
				
				if(url != undefined)
				{
					$.ajax({
						type: 'POST',
						url: url,
						data: {'sort': serialized},
						success: function(data) {
							table.prev(".header_messages").html(data);
						},
					  	async: false
					});
				}
		    },
		    stop: function(e, ui){
				// Remove left over loader image
                if(table.prev(".header_messages").has('i').length)
	               table.prev(".header_messages").html('');
		    }
		});
	}
    
	if($(".sortable_rows").length > 0)
	{
		$( ".sortable_rows tbody" ).sortable({
			helper: fixHelper,
			items: "tr:not(.nodrop.nodrag, tr tr)",
			start: function(e, ui){
				table	= $(this).parents('table').first();
				// Clear previous messages if exists

				table.prev(".header_messages").html('<i class="fa fa-spinner fa-spin"></i>');
		    },
			update: function(e, ui){
		    	serial 	= $(this).sortable("serialize");
		    	table	= $(this).parents('table').first();
				url		= table.attr('rel');
				
				// Serialize data
				var serial = $(this).sortable("toArray");
				var serialized = new Array();
				$.each(serial, function(key, value){
					if(value.indexOf('sort_') != -1)
					{
						serialized.push(value.replace('sort_', ''));
					}
				});
				
				if(url != undefined)
				{
					$.ajax({
						type: 'POST',
						url: url,
						data: {'sort': serialized},
						success: function(data) {
							table.prev(".header_messages").html(data);
						},
					  	async: false
					});
				}
		    },
		    stop: function(e, ui){
				// Remove left over loader image
                if(table.prev(".header_messages").has('i').length)
                   table.prev(".header_messages").html('');
		    }
		});
	}
	
    /******** DATEPICKER INIT ********/
    $(function() {
    	var dates = $( "#from, #to" ).datepicker({
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true,
    		changeMonth: false,
    		showOtherMonths: true,
    		selectOtherMonths: true,
    		numberOfMonths: 1,
    		firstDay: 1,
    		dateFormat: "dd/mm/yy",
    		onSelect: function( selectedDate ) {
    			var option = this.id == "from" ? "minDate" : "maxDate",
    				instance = $( this ).data( "datepicker" );
    				date = $.datepicker.parseDate(
    					instance.settings.dateFormat ||
    					$.datepicker._defaults.dateFormat,
    					selectedDate, instance.settings );
    			dates.not( this ).datepicker( "option", option, date );
    		}
    	});
    });
    
    $(function() {
    	var dates = $( ".from_white, .to_white" ).datepicker({
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true,
    		changeMonth: false,
    		showOtherMonths: true,
    		selectOtherMonths: true,
    		numberOfMonths: 1,
    		firstDay: 1,
    		dateFormat: "dd/mm/yy",
    		onSelect: function( selectedDate ) {
    			var option = $(this).hasClass("from_white") ? "minDate" : "maxDate",
    				instance = $( this ).data( "datepicker" );
    				date = $.datepicker.parseDate(
    					instance.settings.dateFormat ||
    					$.datepicker._defaults.dateFormat,
    					selectedDate, instance.settings );
                        
                var parent = $(this).parents(".date_content").first();
                $(".from_white, .to_white", parent).not($(this)).datepicker( "option", option, date );
                
    		}
    	});
    });
    
    $(function() {
    	var dates = $( "#from_white, #to_white" ).datepicker({
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true,
    		changeMonth: false,
    		showOtherMonths: true,
    		selectOtherMonths: true,
    		numberOfMonths: 1,
    		firstDay: 1,
    		dateFormat: "mm/dd/yy",
    		onSelect: function( selectedDate ) {
    			var option = this.id == "from" ? "minDate" : "maxDate",
    				instance = $( this ).data( "datepicker" );
    				date = $.datepicker.parseDate(
    					instance.settings.dateFormat ||
    					$.datepicker._defaults.dateFormat,
    					selectedDate, instance.settings );
    			dates.not( this ).datepicker( "option", option, date );
    		}
    	});
    });

    $(function() {
    	var dates = $( "#filterFrom, #filterTo" ).datepicker({
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true,
    		changeMonth: false,
    		showOtherMonths: true,
    		selectOtherMonths: true,
    		numberOfMonths: 1,
    		firstDay: 1,
    		dateFormat: "mm/dd/yy",
    		onSelect: function( selectedDate ) {
    			var option = this.id == "filterFrom" ? "minDate" : "maxDate",
    				instance = $( this ).data( "datepicker" );
    				date = $.datepicker.parseDate(
    					instance.settings.dateFormat ||
    					$.datepicker._defaults.dateFormat,
    					selectedDate, instance.settings );
    			dates.not( this ).datepicker( "option", option, date );
    		}
    	});
    });

    $(function() {
    	var dates = $( "#from2, #to2" ).datepicker({
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true,
    		changeMonth: true,
    		numberOfMonths: 2,
    		dateFormat: "mm/dd/yy",
    		onSelect: function( selectedDate ) {
    			var option = this.id == "from" ? "minDate" : "maxDate",
    				instance = $( this ).data( "datepicker" );
    				date = $.datepicker.parseDate(
    					instance.settings.dateFormat ||
    					$.datepicker._defaults.dateFormat,
    					selectedDate, instance.settings );
    			dates.not( this ).datepicker( "option", option, date );
    		}
    	});
    });

    $(function() {
    	var dates = $( "#from3, #to3" ).datepicker({
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true,
    		changeMonth: true,
    		numberOfMonths: 2,
    		dateFormat: "mm/dd/yy",
    		onSelect: function( selectedDate ) {
    			var option = this.id == "from" ? "minDate" : "maxDate",
    				instance = $( this ).data( "datepicker" );
    				date = $.datepicker.parseDate(
    					instance.settings.dateFormat ||
    					$.datepicker._defaults.dateFormat,
    					selectedDate, instance.settings );
    			dates.not( this ).datepicker( "option", option, date );
    		}
    	});
    });

    $(function() {
        $( ".singleDate").datepicker({
            dateFormat: "dd/mm/yy",
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true,
    	});
        
    	$( "#singleDate" ).datepicker({
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true
    	});
        $( ".deliveryDate").datepicker({
            dateFormat: "dd/mm/yy",
            minDate: new Date(),
            showOn: "button",
            buttonImage: theme_path + "images/icon-calendar.png",
            buttonImageOnly: true,
        });
    });

    $(function() {
    	$( "#singleDateFilter" ).datepicker({
    		showOn: "button",
    		buttonImage: theme_path + "images/icon-calendar.png",
    		buttonImageOnly: true
    	});
    });
    /******** END OF: DATEPICKER INIT ********/
    
    
    // toggle panels
	$(".togglePanel a + a").hide();
	$(".togglePanel a").click(function(e){
		$(this).toggle();
		$(this).siblings().toggle();
		$(this).parents(".panelHeader").next().toggle(200);
		e.preventDefault();
	});
	$(".togglePanel.closed").find('a:first').click();
    
    
	/******* CUSTOM FILE INPUT *******/
	$.fn.fileName = function() {
		var $this = $(this),
		$val = $this.val(),
		valArray = $val.split('\\'),
		newVal = valArray[valArray.length-1],
		$button = $this.siblings('.button');
		if(newVal !== '') {
			$(this).next('.file_place').val(newVal);
	  	}
	};
	
	$('input[type=file]').each(function(){
		
		$(this).wrap('<div class="file input-group">');
		$(this).after('<input type="text" class="form-control file_place" /><span class="input-group-addon">Browse</span>');
		
		$(this).bind('change focus click', function(){
			$(this).fileName();
		});
		
	});
	/********* END OF: CUSTOM FILE INPUT ********/
	
	if($('select.toggle_dates').length > 0 && $('.toggable_dates').length > 0)
	{
		show_dates($('select.toggle_dates'), 2, $('.toggable_dates'));
	}
    
    $('.onchange_submit').on('change', function(){
        $(this).closest('form').submit();
    });
    
    $('.onclick_submit').on('click', function(e){
        e.preventDefault();
        var $form = $(this).attr('data-form');
        if($form) {
            $($form).submit();
        } else {
            $(this).closest('form').submit();
        }
    });
    
    /******* Action header links *******/
    $('.action_all').on('click', function(e){
        e.preventDefault();
        $value = $(this).attr('data-value');
        $action = $(this).attr('data-action');

        console.log($value);

        if($("input[name^='" + $value + "[]']").length)
        {
            $value += "[]";
        }

        switch($action)
        {
            case 'select_all':
                $("input[name^='" + $value + "']").prop("checked", "checked");
                break;
            case 'unselect_all':
                $("input[name^='" + $value + "']").removeAttr('checked');
                break;
            case 'select_inverse':
                $("input[name^='" + $value + "']").each(function (index){
                    if($(this).is(":checked")){
                        $(this).removeAttr('checked');
                    } else {
                        $(this).prop("checked", "checked");
                    }
                })
                break;
        }
        count_items($value);
    });

    $('.check_active').on('click', function (){
        count_items($(this).data('value'));
    });
     /******* End of action header links *******/
     
     // Search filters clear every 4
    $('.blue_filters_table td').first().children('div:nth-child(4n)').after('<div class="clear"></div>');

});


$(window).load(function() {

	//Payment > Paypal
	$('input[name="mode"]').change(function(){
		var value = $(this).val();
		$('.credentials').each(function(index, elem){
			if( $(elem).hasClass('credentials') && $(elem).hasClass(value) )
				$(this).attr('style', 'display:block');
			else
				$(this).attr('style', 'display:none');
		});
	});

    //Stock Management Options : show/hide div for Manage Stock choosen
    $('input#form_manage_stock').change(function(){
        if ( $(this).attr('id') == 'form_manage_stock') {
            var value = $(this).is(':checked');
            if (value) {
                $('#manage_stock_option_box').removeClass('hidden');

            } else {
                $('#manage_stock_option_box').addClass('hidden');
            }
        }
    });

    $('input#form_specials_page_enable').change(function(){
        if ( $(this).attr('id') == 'form_specials_page_enable') {
            var value = $(this).is(':checked');
            if (value) {
                $('#specials_page_option_box').removeClass('hidden');

            } else {
                $('#specials_page_option_box').addClass('hidden');
            }
        }
    });

	if ($('input#form_manage_stock').length)
  		$('input#form_manage_stock').trigger('change');
    
    if ($('input#form_specials_page_enable').length)
        $('input#form_specials_page_enable').trigger('change');

    $('div.alert').delay(5000).fadeOut();
});


$(document).ready(function (){
    $("#same_billing_id").click(function(){
        if($('#same_billing_id').is(':checked')){
            $('#form_shipping_title').val($('#form_title').val());
            $('#form_shipping_first_name').val($('#form_first_name').val());
            $('#form_shipping_last_name').val($('#form_last_name').val());
            $('#form_shipping_business_name').val($('#form_business_name').val());
            $('#form_shipping_business_name2').val($('#form_business_name2').val());
            $('#form_shipping_company').val($('#form_company').val());
            $('#form_shipping_job_title').val($('#form_job_title').val());
            $('#form_shipping_address').val($('#form_address').val());
            $('#form_shipping_address2').val($('#form_address2').val());
            $('#form_shipping_suburb').val($('#form_suburb').val());
            $('#s2id_form_shipping_country').val($('#form_country').val());
            $('#s2id_form_shipping_state').text($('#form_state').val());
            $('#form_shipping_postcode').val($('#form_postcode').val());
            $('#form_shipping_phone').val($('#form_phone').val());
            $('#form_shipping_direct_phone').val($('#form_direct_phone').val());
            $('#form_shipping_mobile').val($('#form_mobile').val());

            if($('#form_on_account').is(":checked")){
                $('#form_shipping_company').show();
                $('#form_shipping_business_name').show();
                $('#form_shipping_business_name2').hide();
                $("#form_on_shipp_account").prop("checked", true);
            }
            else{
                $('#form_shipping_company').hide();
                $('#form_shipping_business_name').hide();
                $('#form_shipping_business_name2').show();
            }
           
        } else {
            $("#form_on_shipp_account").prop("checked",false);
            $('#form_shipping_company').hide();
            $('#form_shipping_business_name').hide();
            $('#form_shipping_business_name2').show();
            $('#form_shipping_title').val("");
            $('#form_shipping_first_name').val("");
            $('#form_shipping_last_name').val("");
            $('#form_shipping_business_name').val("");
             $('#form_shipping_company').val("");
            $('#form_shipping_job_title').val("");
            $('#form_shipping_address').val("");
            $('#form_shipping_address2').val("");
            $('#form_shipping_suburb').val("");
            $('#s2id_form_shipping_country').val("Australia");
            $('#s2id_form_shipping_state').val("ACT");
            $('#form_shipping_postcode').val("");
            $('#form_shipping_phone').val("");
            $('#form_shipping_direct_phone').val("");
            $('#form_shipping_mobile').val("");
        }
    });
    $('#form_business_name').val($("#form_company :selected").text());
    $('#form_company').change(function(){
        $('#form_business_name').val($("#form_company :selected").text());
    });
    $('#form_shipping_business_name').val($("#form_shipping_company :selected").text());
    $('#form_shipping_company').change(function(){
        $('#form_shipping_business_name').val($("#form_shipping_company :selected").text());
    });

    $('#save_button_up').click(function(e){
        e.preventDefault();
        $('#save_button_down').trigger('click');
    });

    $('#save_button_up_price').click(function(e){
        e.preventDefault();
        $('#save_button_down_price').trigger('click');
    });

    $('#save_button_up_seo').click(function(e){
        e.preventDefault();
        $('#save_button_down_seo').trigger('click');
    });

    $('#save_button_up_specials').click(function(e){
        e.preventDefault();
        $('#save_button_down_specials').trigger('click');
    });

    $('#save_button_up_stock').click(function(e){
        e.preventDefault();
        $('#save_button_down_stock').trigger('click');
    });


    $('#save_product_assigned').click(function(e){
        e.preventDefault();
        $('#save_product_assigned_click').trigger('click');
    });
    
    $('#save_order').click(function(e){
        e.preventDefault();
        $('#save_order_notes').trigger('click');
    });

    $('#bulk_apply').click(function(e){
        if( $('#form_bulk_actions').val() == 0 || parseInt($('.items_selected').text()) == 0 )
        {
            var message = 'Please select action or item first.';
            var _modal = $('#deleteModalPopUp');
            _modal.find('.modal-message').html(message);
            _modal.find('.modal-yes').hide();
            _modal.find('.modal-no').hide();
            _modal.modal('show');
            e.preventDefault();
        }

    	if( $('#form_bulk_actions').val() == 'delete' && parseInt($('.items_selected').text()) >= 1 ){
            e.preventDefault();
    		var message = 'You are about to delete '+$('.items_selected').text()+' item(s).<br>Are you sure?';
            var _modal = $('#deleteModalPopUp');
            _modal.find('.modal-message').html(message);
            _modal.find('.modal-yes').attr('data-button', '#bulk_apply');
            _modal.find('.modal-yes').show();
            _modal.find('.modal-no').show();
            _modal.modal('show');
    	}
    });

});



(function($){

    // -------------------------------------------- CONFIRMATION POPUP --------------------------------------

    $( 'body' ).on( "click", "a.confirmation-pop-up", function(e) {
        e.preventDefault();

        var message = $(this).attr('data-message');
        if(!message)
            message = 'Are you sure?';

        var _modal = $('#deleteModalPopUp');
        _modal.find('.modal-message').html(message);
        _modal.find('.modal-yes').attr('href', $(this).attr('href'));
        _modal.find('.modal-yes').show();
        _modal.find('.modal-no').show();
        _modal.modal('show');
    });

    $( 'body' ).on( "click", "#deleteModalPopUp a.modal-yes[data-button='#bulk_apply']", function(e) {
        $('#bulk_apply').closest('form').submit();
    });


    // -------------------------------------------- SELECT ALL --------------------------------------

    $("a.select_all").click(function(){

        var holder = $(this).parents("div").first();
        var table = holder.next("table");
        
        $("input[type=checkbox]", table).prop("checked", "checked");
        
        // Recalculate selected checkboxes
        get_selected_checkboxes();
        
    });

    $("a.unselect_all").click(function(){

        var holder = $(this).parents("div").first();
        var table = holder.next("table");
        
        $("input[type=checkbox]", table).removeAttr("checked");
        
        // Recalculate selected checkboxes
        get_selected_checkboxes();
        
    });

    // Select links handler
    var get_selected_checkboxes = function(){
        
        $(".select_qty span").each(function(){
            var holder = $(this).parents("div").first();
            var table = holder.next("table");
            
            $(this).html($("input[type=checkbox]:checked", table).not("[name=select_all]").length);
        });
        
    };

    $(document).on('change', "input[type=checkbox]", function(){
        // Recalculate selected checkboxes
        get_selected_checkboxes();
    });


    $("input[name='select_all']").click(function(e){
        $("input[name='"+$(this).val()+"']").prop("checked", $(this).is(":checked"));
    });



    // ----- Send email in orders downloadable section
    $('.pdf-send-email-submit').click(function(e){
        e.preventDefault();

        var _form = $(this).closest('form');

        // validate first
        var to = _form.find('input[name="email_to"]').val();
        var is_valid_to = false;
        var from = _form.find('input[name="email_from"]').val();
        var is_valid_from = false;
        var message = _form.find('textarea[name="message"]').val();
        var is_valid_message = false;
        var subject = _form.find('input[name="email_subject"]').val();

        if(to != '')
        {
            if(to.indexOf(',') > -1)
            {
                var emails = to.split(',');
                for (var i = 0; i < emails.length; i++)
                {
                    var email = emails[i].trim();
                    if(email != '')
                    {
                        if(email.match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/))
                            is_valid_to = true;
                        else
                            is_valid_to = false;
                    }
                }
            }
            else
            {
                if(to.match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/))
                {
                    is_valid_to = true;
                }
            }
        }
        else
            is_valid_to = false;

        if(from != '')
        {
            if(from.indexOf('<') > -1 && from.indexOf('>') > -1)
            {
                var emails = from.split('<');
                var name = emails[0].trim();
                var email = emails[1].trim().replace(">", "");
                if(email != '' && name != '')
                {
                    if(email.match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/))
                        is_valid_from = true;
                    else
                        is_valid_from = false;
                }
            }
            else
            {
                if(from.match(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/))
                {
                    is_valid_from = true;
                }
            }
        }
        else
            is_valid_from = false;

        if(subject != '')
        {
            is_valid_from = true;
        }
        else
            is_valid_from = false;

        if(message != '')
            is_valid_message = true;
        else
            is_valid_message = false;

        if(is_valid_to && is_valid_from && is_valid_message)
        {
            _form.submit();
        }
        else
        {
            alert('Please fill fields correctly.');
        }
    });
    if($('#canlendarDisplay').length)
    {
        var date_list = [];
        var today = new Date();
        today.setHours(0,0,0,0);
        var month = today.getUTCMonth() + 1; 
        var day = today.getUTCDate();
        var year = today.getUTCFullYear();
        if($('#canlendarDisplay_delivery_datetime').val())
        {
            var split_date = $('#canlendarDisplay_delivery_datetime').val().split("/");
            var year = split_date[2];
            var month = split_date[1];
            var day = split_date[0];
        }
        $.ajax({
            type: 'GET',
            url: uri_base+'/reports/dateHasDeliveryOrderByMonth?month='+month+'&year='+year,
            dataType: 'json',
            success: function(data) {
                date_list = data.date_list;
            },
            async: false
        });
        $('#canlendarDisplay').datepicker({
            onChangeMonthYear: function(year, month, inst) {
                $.ajax({
                    type: 'GET',
                    url: uri_base+'/reports/dateHasDeliveryOrderByMonth?month='+month+'&year='+year,
                    dataType: 'json',
                    success: function(data) {
                        date_list = data.date_list;
                    },
                    async: false
                });
            },
            beforeShowDay: function(date) {
                var month = date.getUTCMonth() + 1; 
                var day = date.getUTCDate() + 1;
                var year = date.getUTCFullYear();
                
                var cur_date = year+'-'+month+'-'+day;

                if(date_list.indexOf(cur_date) > -1)
                    return [true, 'has-order-delivery'];
                
                return [true, ''];
            },
            buttonImageOnly: true,
            changeMonth: false,
            showOtherMonths: true,
            selectOtherMonths: true,
            numberOfMonths: 1,
            firstDay: 1,
            dateFormat: "dd/mm/yy",
            onSelect: function( selectedDate ) {
                window.location.href = uri_base+'/order/list?delivery_datetime='+selectedDate;
                /* $('input[name="delivery_datetime"]').val(selectedDate); */
            }
        });
        if($('#canlendarDisplay_delivery_datetime').val())
        {
            var split_date = $('#canlendarDisplay_delivery_datetime').val().split("/");
            var year = split_date[2];
            var month = split_date[1] - 1;
            var day = split_date[0];
            $('#canlendarDisplay').datepicker('setDate', new Date(year, month, day));
        }
        /*
        var dateDelivery = $('input[name="delivery_datetime"]').attr('data-delivery');
        if(dateDelivery)
            $("#canlendarDisplay").datepicker('setDate', dateDelivery);
        */
        // $('input[name="delivery_datetime"]').val($('#canlendarDisplay').val());
    }
    var tom = new Date();
    $('.searchDateToday').attr('href', $('.searchDateToday').attr('data-href')+'?'+$('.searchDateToday').attr('data-name')+'='+tom.getDate()+'/'+(tom.getMonth()+1)+'/'+tom.getFullYear());
    tom.setDate(tom.getDate() + 1);
    $('.searchDateTomorrow').attr('href', $('.searchDateToday').attr('data-href')+'?'+$('.searchDateToday').attr('data-name')+'='+tom.getDate()+'/'+(tom.getMonth()+1)+'/'+tom.getFullYear());


    //Print table report
    $('.print_table').click(function(e){
        e.preventDefault();
        $($(this).data('table')).printThis();
    });


    $( 'body' ).on( "click", "a.preview-image-popup", function(e) {
        e.preventDefault();
        var _modal = $('#imagePreviewModalPopUp');
        _modal.find('.modal-image-preview').attr('src', $(this).attr('href'));
        _modal.modal('show');
    });

    if($('#loginAsCustomer').length)
    {
        $('#loginAsCustomer').click(function(e){
            e.preventDefault();
            var url = $(this).attr('href');
            loginAsCustomerWindow = window.open(url, "login_as_customer_window");
            loginAsCustomerWindow.focus();
        });
    }

    if($('.email-order-list').length)
    {
        $('.email-order-list').click(function(e){
            e.preventDefault();
            $('#order-email-list').modal('show');

            $('#order-email-list').find('form').attr('action', $('#order-email-list').find('form').attr('action')+$(this).data('orderid')+'?redirect=order_list');
            $('#order-email-list').find('h4.modal-title').text('Order '+$(this).data('orderid'));
            $('#order-email-list').find('input[name="email_to"]').val($(this).data('email'));
            $('#order-email-list').find('input[name="email_subject"]').val($(this).data('company')+' Order');
            $('#order-email-list').find('textarea[name="message"]').val('Dear '+$(this).data('name')+',\n\nSee attached in PDF your (order).\n\nShould you have any questions, please do not hesitate to contact us.\n\nKind regards\n\n'+$(this).data('company')+'\n'+$(this).data('phone')+'');
        });
    }

    if($('#form_on_account').length)
    {
        if($('#form_on_account').is(":checked")){
            $('#form_company').show();
            $('#form_business_name').show();
            $('#form_business_name2').hide();
        }
        else{
            $('#form_company').hide();
            $('#form_business_name').hide();
            $('#form_business_name2').show();
        }

        $('#form_on_account').click(function(){
            if($(this).is(":checked")){
                $('#form_company').show();
                $('#form_business_name').show();
                $('#form_business_name2').hide();
            }
            else{
                $('#form_company').hide();
                $('#form_business_name').hide();
                $('#form_business_name2').show();
            }
        });
    }

    if($('#form_on_shipp_account').length)
    {
        if($('#form_on_shipp_account').is(":checked")){
            $('#form_shipping_company').show();
            $('#form_shipping_business_name').show();
            $('#form_shipping_business_name2').hide();
        }
        else{
            $('#form_shipping_company').hide();
            $('#form_shipping_business_name').hide();
            $('#form_shipping_business_name2').show();
        }
        
        $('#form_on_shipp_account').click(function(){
            if($(this).is(":checked")){
                $('#form_shipping_company').show();
                $('#form_shipping_business_name').show();
                $('#form_shipping_business_name2').hide();
            }
            else{
                $('#form_shipping_company').hide();
                $('#form_shipping_business_name').hide();
                $('#form_shipping_business_name2').show();
            }
        });
    }

    $('a.sort-table-column').click(function(e){
        e.preventDefault();
        $(this).closest('form').find('input[name="orderby"]').val($(this).attr('data-column'));
        $(this).closest('form').find('input[name="order"]').val($(this).attr('data-order'));
        $(this).closest('form').submit();
    });
})(jQuery);