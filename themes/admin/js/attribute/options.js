$(document).ready(function(){
	
	// Edit button
	if($(".toggle_options").length)
	{
		$(".toggle_options").on('click', function(){
			
			if($(this).val() == 'select')
			{
				$(".options").slideDown();
			}
			else
			{
				$(".options").slideUp();
			}
			
		});
		
		$(".toggle_options:checked").click();
	}
	
	// Save button
	if($(".live_update").length)
	{
		// Override event on ENTER button, and submit each form correctly
		$('td').on('keypress', '.disable_enter', function (e) {
			if (e.which == 13) {
				$(this).parent('strong').parents('td').first().find('.live_save').first().click();
			}
		});
		
		$(".live_update").on('click', function(){
			
			$element = $(this).parents("tr").first().find("td strong").first();
			$element_id = $element.attr('class');
			
			$input = $('<input/>').attr({ type: 'text', 'class': 'form-control disable_enter', value: $.trim($element.html()) });
			
			$(this).hide().siblings(".live_save").removeAttr('disabled').show();
			
			$element.html($input);
			
		});
		
		$(".live_save").on('click', function(e){
			
			e.preventDefault();
			
			$this		= $(this);
			$element 	= $(this).parents("tr").first().find("td strong").first();
			
			//Css fix
			// $element.css("display", "block");
			
			$element_id 	= $element.parent('td').attr('class');
			$element_value 	= $element.find("input").first().val();
			
			$element.append('<i class="fa fa-spinner fa-spin"></i>');
			// $element.parents('.panelContent').first().find('.sort_message_container').first().html('<i class="fa fa-spinner fa-spin"></i>');
			
			$.ajax({
				type: 'POST',
				url: uri_base + '/attribute/option/update/' + $element_id,
				data: {
					'option_title': $element_value
				},
				success: function(data) {
					
					data = jQuery.parseJSON(data);
					
					if(data.error == true)
					{
						// Remove loader image
						$(".loader_image").remove();
						$element.parents('.panelContent').first().find('.sort_message_container').first().html(data.message);
					}
					else
					{
						$element.parents('.panelContent').first().find('.sort_message_container').first().html(data.message);
						
						$this.attr('disabled', 'disabled').hide().siblings(".live_update").show();
						//Css fix
						$element.css("display", "");
						$element.html($.trim($element.find("input").first().val()));
					}
				},
			  	async: false
			});
			
		});
	}
	
});