<?php

define('EDGECMS_VERSION', '3.7');

// Bootstrap the framework DO NOT edit this

require COREPATH.'bootstrap.php';

// Register the autoloader
\Autoloader::register();

\Autoloader::add_classes(array(
	// Add classes you want to override here
	// Example: 'View' => APPPATH.'classes/view.php',

	'Str' => APPPATH . 'core/str.php',
	'Uri' => APPPATH . 'core/uri.php',
	'Input' => APPPATH . 'core/input.php',
	'Validation' => APPPATH . 'core/validation.php',
	'Arr' => APPPATH . 'core/arr.php',
	'Database_Query' => APPPATH . 'core/query.php',
	'Image_Driver' => APPPATH . 'core/driver.php',

	'App\\Curl' => APPPATH . 'vendor/curl.php',
	'App\\Youtube' => APPPATH . 'vendor/youtube.php',
	'App\\MYOB_API_OAUTH' => APPPATH . 'vendor/myob.php',

	// Country/State Classes
	'App\\Countries' 	=> APPPATH . 'vendor/countries.php',
	'App\\States' 		=> APPPATH . 'vendor/states.php',

	// Emailer class
	'App\\Emailer' 		=> APPPATH . 'vendor/emailer.php',

	'SessionStorage' => APPPATH . 'vendor/sessionstorage.php',

	'SecurePay'         => APPPATH . 'vendor/securepay.php',

	'Extendedcart\\Cart'                  => APPPATH . 'vendor/extendedcart/Cart.php',
	'Extendedcart\\Item'                  => APPPATH . 'vendor/extendedcart/Item.php',
	'Extendedcart\\Manager'                  => APPPATH . 'vendor/extendedcart/Manager.php',

	'PaymentProccess\\PaymentProccess'                  => APPPATH . 'vendor/paymentProccess/paymentProccess.php',
	'PaymentProccess\\PaymentProccessTypeBasic'         => APPPATH . 'vendor/paymentProccess/paymentProccessTypeBasic.php',
	'PaymentProccess\\PaymentProccessTypeBank'          => APPPATH . 'vendor/paymentProccess/paymentProccessTypeBank.php',
	'PaymentProccess\\PaymentProccessTypePaypal'        => APPPATH . 'vendor/paymentProccess/paymentProccessTypePaypal.php',
	'PaymentProccess\\PaymentProccessTypeSecurePay'     => APPPATH . 'vendor/paymentProccess/paymentProccessTypeSecurePay.php',
	'PaymentProccess\\PaymentProccessTypeEway'     => APPPATH . 'vendor/paymentProccess/paymentProccessTypeEway.php',
	'PaymentProccess\\PaymentProccessTypeCreditCard'    => APPPATH . 'vendor/paymentProccess/paymentProccessTypeCreditCard.php',
	'PaymentProccess\\PaymentProccessTypeStrategy'      => APPPATH . 'vendor/paymentProccess/paymentProccessTypeStrategy.php',
	'PaymentProccess\\PaymentProccessTypeFactory'       => APPPATH . 'vendor/paymentProccess/paymentProccessTypeFactory.php',
	'PaymentProccess\\PaymentSaver'                     => APPPATH . 'vendor/paymentProccess/paymentSaver.php',
	'PaymentProccess\\PaymentEmailer'                   => APPPATH . 'vendor/paymentProccess/paymentEmailer.php',
	'PaymentProccess\\PaymentForm\\PaymentFormPaypal'   => APPPATH . 'vendor/paymentProccess/paymentForm/paymentFormPaypal.php',
	'PaymentProccess\\PaymentForm\\PaymentForm'         => APPPATH . 'vendor/paymentProccess/paymentForm/paymentForm.php',

	'PaymentProccess\\Exception\\SecurePay\\SecurePayServerDownException'           => APPPATH . 'vendor/paymentProccess/exception/securePay/securePayServerDownException.php',
	'PaymentProccess\\Exception\\SecurePay\\SecurePayTransactionFailedException'    => APPPATH . 'vendor/paymentProccess/exception/securePay/securePayTransactionFailedException.php',
	'PaymentProccess\\Exception\\SecurePay\\SecurePayCustomerDataInvalidException'  => APPPATH . 'vendor/paymentProccess/exception/securePay/securePayCustomerDataInvalidException.php',

	// NRB-Gem: Class not found fix.
	'RestTransport'         => APPPATH . 'vendor/yousendit/lib/src/include/RestTransport.php',
	'Authentication'        => APPPATH . 'vendor/yousendit/lib/src/Authentication.php',
));

/**
 * Your environment.  Can be set to any of the following:
 *
 * Fuel::DEVELOPMENT
 * Fuel::TEST
 * Fuel::STAGING
 * Fuel::PRODUCTION
 */
$_SERVER['FUEL_ENV'] = \Fuel::DEVELOPMENT;
\Fuel::$env = (isset($_SERVER['FUEL_ENV']) ? $_SERVER['FUEL_ENV'] : \Fuel::DEVELOPMENT);

// Initialize the framework with the config file.
\Fuel::init('config.php');

class_alias('Cart\\Facade\\Manager',    'CartManager');
class_alias('Cart\\Facade\\Cart',       'Cart');