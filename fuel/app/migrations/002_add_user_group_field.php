<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_user_group_field
{

    function up()
    {
        if(\DBUtil::table_exists('product_categories'))
        {
            \DBUtil::add_fields('product_categories', array(
                'user_group' => array('constraint' => 255, 'type' => 'varchar'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('product_categories'))
        {
            \DBUtil::drop_fields('product_categories', 'user_group');
        }
    }
}

?>