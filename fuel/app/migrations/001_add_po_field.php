<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_po_field
{

    function up()
    {
        if(\DBUtil::table_exists('orders'))
        {
            \DBUtil::add_fields('orders', array(
                'client_po' => array('constraint' => 255, 'type' => 'varchar'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('orders'))
        {
            \DBUtil::drop_fields('orders', 'client_po');
        }
    }
}

?>