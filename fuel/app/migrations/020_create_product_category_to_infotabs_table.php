<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Create_product_category_to_infotabs_table
{

    function up()
    {
        \DBUtil::create_table('product_category_to_infotabs', array(
            'unique_id' => array('type' => 'int', 'auto_increment' => true),
            'infotab_id' => array('constraint' => 11, 'type' => 'int'),
            'category_id' => array('constraint' => 11, 'type' => 'int'),
            'description' => array('type' => 'text', 'null' => true),
            'image' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
            'alt_text' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
            'active' => array('constraint' => 11, 'type' => 'int'),
        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('product_category_to_infotabs');
    }
}

?>