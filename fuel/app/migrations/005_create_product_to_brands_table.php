<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Create_product_to_brands_table
{

    function up()
    {
        \DBUtil::create_table('product_to_brands', array(
            'id' => array('type' => 'int', 'auto_increment' => true),
            'product_id' => array('type' => 'int'),
            'brand_id' => array('type' => 'int'),
        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('product_to_brands');
    }
}

?>