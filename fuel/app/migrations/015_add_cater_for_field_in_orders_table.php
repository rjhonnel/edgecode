<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_cater_for_field_in_orders_table
{

    function up()
    {
        if(\DBUtil::table_exists('orders'))
        {
            \DBUtil::add_fields('orders', array(
                'cater_for' => array('constraint' => 11, 'type' => 'int'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('orders'))
        {
            \DBUtil::drop_fields('orders', 'cater_for');
        }
    }
}

?>