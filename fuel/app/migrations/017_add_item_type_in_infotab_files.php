<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_item_type_in_infotab_files
{

    function up()
    {
        if(\DBUtil::table_exists('infotab_files'))
        {
            \DBUtil::modify_fields('infotab_files', array(
                'product_id' => array('constraint' => 11, 'type' => 'int', 'name' => 'item_id'),
            ));
            \DBUtil::add_fields('infotab_files', array(
                'item_type' => array('constraint' => 100, 'type' => 'varchar'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('infotab_files'))
        {
            \DBUtil::modify_fields('infotab_files', array(
                'item_id' => array('constraint' => 11, 'type' => 'int', 'name' => 'product_id'),
            ));
            \DBUtil::drop_fields('infotab_files', 'item_type');
        }
    }
}

?>