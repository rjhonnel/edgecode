<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_minimum_order_field
{

    function up()
    {
        if(\DBUtil::table_exists('product'))
        {
            \DBUtil::add_fields('product', array(
                'minimum_order' => array('constraint' => 11, 'type' => 'int'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('product'))
        {
            \DBUtil::drop_fields('product', 'minimum_order');
        }
    }
}

?>