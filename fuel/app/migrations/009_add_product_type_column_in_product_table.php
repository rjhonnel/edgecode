<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_product_type_column_in_product_table
{

    function up()
    {
        if(\DBUtil::table_exists('product'))
        {
            \DBUtil::add_fields('product', array(
                'product_type' => array('constraint' => 100, 'type' => 'varchar', 'default' => 'simple'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('product'))
        {
            \DBUtil::drop_fields('product', 'product_type');
        }
    }
}

?>