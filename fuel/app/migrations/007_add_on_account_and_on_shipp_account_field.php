<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_on_account_and_on_shipp_account_field
{

    function up()
    {
        if(\DBUtil::table_exists('users_metadata'))
        {
            \DBUtil::add_fields('users_metadata', array(
                'on_account' => array('constraint' => 11, 'type' => 'int'),
                'on_shipp_account' => array('constraint' => 11, 'type' => 'int'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('users_metadata'))
        {
            \DBUtil::drop_fields('users_metadata', 'on_account');
            \DBUtil::drop_fields('users_metadata', 'on_shipp_account');
        }
    }
}

?>