<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_delayed_actions_table
{

    function up()
    {
        \DBUtil::create_table('delayed_actions', array(
            'id' => array('type' => 'int', 'auto_increment' => true),
            'item_id' => array('constraint' => 11, 'type' => 'int'),
            'action' => array('constraint' => 1000, 'type' => 'varchar'),
            'created_at' => array('type' => 'datetime'),
        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('delayed_actions');
    }
}

?>