<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Create_product_packs_table
{

    function up()
    {
        \DBUtil::create_table('product_packs', array(
            'id' => array('type' => 'int', 'auto_increment' => true),
            'product_id' => array('constraint' => 11, 'type' => 'int'),
            'name' => array('constraint' => 500, 'type' => 'varchar'),
            'description' => array('type' => 'text', 'null' => true),
            'type' => array('constraint' => 50, 'type' => 'varchar'),
            'selection_limit' => array('constraint' => 11, 'type' => 'int'),
            'products' => array('type' => 'text', 'null' => true),
        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('product_packs');
    }
}

?>