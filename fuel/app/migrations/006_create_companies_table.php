<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Create_companies_table
{

    function up()
    {
        \DBUtil::create_table('companies', array(
            'id' => array('type' => 'int', 'auto_increment' => true),
            'company_name' => array('constraint' => 255, 'type' => 'varchar'),
            'company_mobile' => array('constraint' => 255, 'type' => 'varchar'),
            'company_phone' => array('constraint' => 255, 'type' => 'varchar'),
            'company_email' => array('constraint' => 255, 'type' => 'varchar'),
            'company_address' => array('constraint' => 255, 'type' => 'varchar'),
            'company_suburb' => array('constraint' => 255, 'type' => 'varchar'),
            'company_state' => array('constraint' => 255, 'type' => 'varchar'),
            'company_postcode' => array('constraint' => 255, 'type' => 'varchar'),
            'company_country' => array('constraint' => 255, 'type' => 'varchar'),
            'created_at' => array('type' => 'int', 'null' => true),
            'updated_at' => array('type' => 'int', 'null' => true),
            'user_created' => array('type' => 'int'),
            'user_updated' => array('type' => 'int', 'null' => true),
            'sort' => array('type' => 'int'),
        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('companies');
    }
}

?>