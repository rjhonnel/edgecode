<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_cc_email_field_in_user_metadata
{

    function up()
    {
        if(\DBUtil::table_exists('users_metadata'))
        {
            \DBUtil::add_fields('users_metadata', array(
                'cc_email' => array('constraint' => 500, 'type' => 'varchar', 'null' => true),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('users_metadata'))
        {
            \DBUtil::drop_fields('users_metadata', 'cc_email');
        }
    }
}

?>