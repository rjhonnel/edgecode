<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Create_brand_table
{

    function up()
    {
        \DBUtil::create_table('product_brands', array(
            'id' => array('type' => 'int', 'auto_increment' => true),
            'parent_id' => array('type' => 'int', 'default' => 0),
            'title' => array('constraint' => 255, 'type' => 'varchar'),
            'description_intro' => array('type' => 'text', 'null' => true),
            'description_full' => array('type' => 'text', 'null' => true),
            'status' => array('constraint' => 4, 'type' => 'int', 'default' => 1),
            'active_from' => array('type' => 'int', 'null' => true),
            'active_to' => array('type' => 'int', 'null' => true),
            'published_at' => array('type' => 'int', 'null' => true),
            'created_at' => array('type' => 'int', 'null' => true),
            'updated_at' => array('type' => 'int', 'null' => true),
            'user_created' => array('type' => 'int'),
            'user_updated' => array('type' => 'int', 'null' => true),
            'sort' => array('type' => 'int'),
        ), array('id'));
    }

    function down()
    {
       \DBUtil::drop_table('product_brands');
    }
}

?>