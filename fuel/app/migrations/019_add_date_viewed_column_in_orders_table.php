<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_date_viewed_column_in_orders_table
{

    function up()
    {
        if(\DBUtil::table_exists('orders'))
        {
            \DBUtil::add_fields('orders', array(
                'date_viewed' => array('type' => 'datetime'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('orders'))
        {
            \DBUtil::drop_fields('orders', 'date_viewed');
        }
    }
}

?>