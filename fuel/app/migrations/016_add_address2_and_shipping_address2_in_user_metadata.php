<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_address2_and_shipping_address2_in_user_metadata
{

    function up()
    {
        if(\DBUtil::table_exists('users_metadata'))
        {
            \DBUtil::add_fields('users_metadata', array(
                'address2' => array('constraint' => 500, 'type' => 'varchar', 'null' => true),
                'shipping_address2' => array('constraint' => 500, 'type' => 'varchar', 'null' => true),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('users_metadata'))
        {
            \DBUtil::drop_fields('users_metadata', 'address2');
            \DBUtil::drop_fields('users_metadata', 'shipping_address2');
        }
    }
}

?>