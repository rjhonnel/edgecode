<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_pack_column_in_order_abandon_cart_products
{

    function up()
    {
        if(\DBUtil::table_exists('order_abandon_cart_products'))
        {
            \DBUtil::add_fields('order_abandon_cart_products', array(
                'packs' => array('type' => 'text', 'null' => true),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('order_abandon_cart_products'))
        {
            \DBUtil::drop_fields('order_abandon_cart_products', 'packs');
        }
    }
}

?>