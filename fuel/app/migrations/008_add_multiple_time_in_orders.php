<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_multiple_time_in_orders
{

    function up()
    {
        if(\DBUtil::table_exists('orders'))
        {
            \DBUtil::add_fields('orders', array(
                'delivery_datetime_list' => array('type' => 'text', 'null' => true),
            ));
        }
    }

    function down()
    {
        if(\DBUtil::table_exists('orders'))
        {
            \DBUtil::drop_fields('orders', 'delivery_datetime_list');
        }
    }
}

?>