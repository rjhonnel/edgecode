<?php
// php oil refine migrate:up command

namespace Fuel\Migrations;

class Add_item_type_in_infotab_images
{

    function up()
    {
        if(\DBUtil::table_exists('infotab_images'))
        {
            \DBUtil::add_fields('infotab_images', array(
                'item_type' => array('constraint' => 100, 'type' => 'varchar'),
            ));
        }
    }

    function down()
    {
       if(\DBUtil::table_exists('infotab_images'))
        {
            \DBUtil::drop_fields('infotab_images', 'item_type');
        }
    }
}

?>