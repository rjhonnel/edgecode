<?php

	namespace Page;

	class Model_Seo extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'content_seo';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'content_id',
		    'content_type',
		    'slug',
		    'meta_title',
		    'meta_keywords',
		    'meta_description',
		    'h1_tag',
		    'redirect_301',
		    'canonical_links',
		    'meta_robots_index',
		    'meta_robots_follow',
		);
		
		protected static $_defaults = array(
			'content_type' => 'content',
		    'meta_robots_index' => 1,
		    'meta_robots_follow' => 1,
		);
		
		 /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	    	$query->where('content_type', 'content');
	    }

		/**
	     * Validate Model fields
	     * 
	     * @param $factory 	= Validation name, if you want to have multiple validations
	     * @param $id		= ID if item that is being validated if we need to define some custom rules (like unique)
	     */
		public static function validate($factory, $id = false)
		{
			$val = \Validation::forge($factory);
			$val->add('slug', 'Slug')->add_rule('required')->add_rule('min_length', 3)->add_rule('max_length', 255)->add_rule('valid_string', array('alpha', 'numeric', 'dashes'))->add_rule('unique', array(self::$_table_name, 'slug', $id, 'content_id'));
			
			return $val;
		}
	}