<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Page;

class Controller_Admin_Accordion extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/page/accordion/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('page::accordion', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		throw new \HttpNotFoundException;
	}
	
	public function action_list($page_id = false)
	{
		if(!is_numeric($page_id)) throw new \HttpNotFoundException;
		
		// Get news item to edit
		if(!$page = Model_Page::find_one_by_id($page_id)) throw new \HttpNotFoundException;
        
         // Accordion from home page?
        if($page_id == 1) \Config::load('page::accordion_banner', 'details', true, true);
        \View::set_global('menu', 'admin/page/list');
		\View::set_global('title', 'List Pages Accordions');
		
		/************ Start generating query ***********/
		$items = Model_Accordion::find(function($query) use ($page){
			
			// Select only root pages
			$query->where('parent_id', $page->id);
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);

		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('page', $page)
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
	
	public function action_create($page_id = false)
	{
		if(!is_numeric($page_id)) throw new \HttpNotFoundException;
		
		// Get news item to edit
		if(!$page = Model_Page::find_one_by_id($page_id)) throw new \HttpNotFoundException;
        
        // Accordion from home page?
        if($page_id == 1) \Config::load('page::accordion_banner', 'details', true, true);
		
        \View::set_global('menu', 'admin/page/list');
		\View::set_global('title', 'Add New Accordion');
		
		if(\Input::post())
		{
			$val = Model_Accordion::validate('create');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->image))
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload accordion image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload accordion image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false)))
			{
				// Get POST values
				$insert = \Input::post();
				
				// Prepare some values
				$insert['parent_id'] 	= $page->id;
				$insert['active_from'] 	= !empty($insert['active_from']) ? \Helper::dmyToymd($insert['active_from'], '/') : NULL;
				$insert['active_to'] 	= !empty($insert['active_to']) ? \Helper::dmyToymd($insert['active_to'], '/') : NULL;
				if($insert['status'] != 2)
				{
					unset($insert['active_from']);
					unset($insert['active_to']);
				}
				
				$item = Model_Accordion::forge($insert);
				
				try{
					$item->save();
					
					// Insert page images
					if($this->_image_data)
					{
						$item_image = array(
							array(
								'id'	=> 0,
								'data'	=> array(
									'content_id'	=> $item->id,
									'image'			=> $this->_image_data[0]['saved_as'],
									'alt_text'		=> \Input::post('alt_text', ''),
									'cover'			=> 1,
									'sort'			=> 1,
								),
							),
						);
						
						Model_Accordion::bind_images($item_image);

						//save to amazon
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						if($hold_amazon_enable)
						{
							$folder_list = array('', 'large/', 'medium/', 'thumbs/');
							foreach ($folder_list as $folder)
							{
		                        $file_info = array(
		                                'savePath' => 'media/images/'.$folder.$this->_image_data[0]['saved_as'],
		                                'filePath' => $this->_image_data[0]['saved_to'].$folder.$this->_image_data[0]['saved_as'],
		                                'contentType' => mime_content_type($this->_image_data[0]['saved_to'].$folder.$this->_image_data[0]['saved_as']),
		                            );
								\Helper::uploadFileToAmazonS3($file_info, $settings);
								@unlink($this->_image_data[0]['saved_to'].$folder.$this->_image_data[0]['saved_as']);
							}
						}
						// EOL - save to amazon
					}
					
					\Messages::success('Accordion successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/page/accordion/update/' . $item->id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create accordion</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create accordion</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
			
			// Delete uploaded image if there is page saving error
			if(isset($this->_image_data))
				$this->delete_image($this->_image_data[0]['saved_as']);
		}
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'create')
			->set('page', $page);
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) throw new \HttpNotFoundException;
		
		// Get accordion item to edit
		if(!$item = Model_Accordion::find_one_by_id($id)) throw new \HttpNotFoundException;
        
        // Accordion from home page?
        if($item->parent_id == 1) \Config::load('page::accordion_banner', 'details', true, true);
		
        \View::set_global('menu', 'admin/page/list');
		\View::set_global('title', 'Edit Accordion');
		
		if(\Input::post())
		{
			$val = Model_Accordion::validate('update');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)) 
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload accordion image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload accordion image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
            
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)))
			{
				/** IMAGES **/
				// Get all alt texts to update if there is no image change
				foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
				{
					if(strpos($image_id, 'new_') === false)
					{
						$item_images[$image_id] = array(
							'id'	=> $image_id,
							'data'	=> array(
								'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
							),
						);
					}
				}
				
				// Save images if new files are submitted
				if(isset($this->_image_data))
				{
					$settings = \Config::load('amazons3.db');
					$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
					foreach($this->_image_data as $image_data)
					{
						$cover_count = count($item->images);
						if(strpos($image_data['field'], 'new_') === false)
						{
							// Update existing image
							if(str_replace('image_', '', $image_data['field']) != 0)
							{
								$image_id = (int)str_replace('image_', '', $image_data['field']);
								$cover_count--;
								
								$item_images[$image_id] = array(
									'id'	=> $image_id,
									'data'	=> array(
										'content_id'	=> $item->id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
									),
								);
								
								$this->delete_image(\Input::post('image_db_' . $image_id, ''));
							}
						}
						else
						{
							// Save new image
							$image_tmp = str_replace('image_new_', '', $image_data['field']);
							
							$item_images[0] = array(
								'id'	=> 0,
								'data'	=> array(
									'content_id'	=> $item->id,
									'image'			=> $image_data['saved_as'],
									'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
									'cover'			=> $cover_count == 0 ? 1 : 0,
									'sort'			=> $cover_count + 1,
								),
							);
						}

						//save to amazon
						if($hold_amazon_enable)
						{
							$folder_list = array('', 'large/', 'medium/', 'thumbs/');
							foreach ($folder_list as $folder)
							{
		                        $file_info = array(
		                                'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
		                                'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
		                                'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
		                            );
								\Helper::uploadFileToAmazonS3($file_info, $settings);
								@unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
							}
						}
						// EOL - save to amazon
					}
				}
				Model_Accordion::bind_images($item_images);
				/** END OF IMAGES **/
				
				// Get POST values
				$insert = \Input::post();
				
				// Prepare some values
				$insert['active_from'] 	= !empty($insert['active_from']) ? \Helper::dmyToymd($insert['active_from'], '/') : NULL;
				$insert['active_to'] 	= !empty($insert['active_to']) ? \Helper::dmyToymd($insert['active_to'], '/') : NULL;
				if($insert['status'] != 2)
				{
					unset($insert['active_from']);
					unset($insert['active_to']);
				}
				
				$item->set($insert);
				
				try{
					$item->save();
					
					\Messages::success('Accordion successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/page/accordion/list/' . $item->parent_id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update accordion</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				// Delete uploaded images if there is page saving error
				if(isset($this->_image_data))
					foreach($this->_image_data as $image_data)
						$this->delete_image($image_data['saved_as']);
						
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update accordion</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		$accordion 	= Model_Accordion::find_one_by_id($id);
		$page 		= Model_Page::find_one_by_id($accordion->parent_id);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')
			->set('page', $page)
			->set('accordion', $accordion);
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Accordion::find_one_by_id($id))
			{
				// Delete other content data like images, files, etc.
				if(!empty($item->images))
				{
					foreach($item->images as $image)
					{
						$this->delete_image($image->image);
						$image->delete();
					}
				}
				
				try{
					$item->delete();
					
					\Messages::success('Accordion successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete accordion</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		// If its a HMVC request than reset messages and do nothing
		\Request::is_hmvc() ? \Messages::reset() : \Response::redirect(\Input::referrer());
		
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
				
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Page::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'cover'	=> ($key == 0 ? 1 : 0),
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
	
	/****************************** CONTENT IMAGES ******************************/
	
	/**
	 * Upload all contet images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->image_upload_config = array(
		    'path' => \Config::get('details.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		);
		
		\Upload::process($this->image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save image
		    \Upload::save();
			$this->_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_image($image_id = false, $content_id = false)
	{
        // Accordion from home page?
        if($content_id == 1) \Config::load('page::accordion_banner', 'details', true, true);
        
		if($image_id && $content_id)
		{
			$images = Model_Image::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
					
					// If there is only one image and image is required
					if(count($images) == 1)
					{
						if(\Config::get('details.image.required', false))
						{
							\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
					else
					{
						if($image->cover == 1)
						{
							\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('Image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Content Image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
				//delete from amazon
				if($hold_amazon_enable)
				{
					if($content_type == 'image')
					{
						\Helper::deleteFileFromAmazonS3('media/images/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/thumbs/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/medium/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/large' . $name, $settings);
					}
				}
				// EOL - delete from amazon
				@unlink($folder . $name);
			}
		}
	}
	
}
