<?php

namespace Page;

class Controller_Page extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/page/';
	
	public $theme_view_path;
    
     // Global theme variables
    public $page_theme;
    
	public function before()
	{
		parent::before();
		
		\Config::load('page::page', 'details');
        \Config::load('product::product', 'product');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
		
		$this->theme_view_path = DOCROOT . \Theme::instance()->asset_path($this->view_dir);
	}
	
	/**
	 * Index page
	 * 
	 * @access  public
     * @param   $slug
	 */
	public function action_index($slug = false, $gallery_id = null)
    {
        $this->show($slug, $gallery_id);
    }

    protected function show($slug = false, $gallery_id = null)
    {
        // Get home page
        $home_page = \Config::get('details.locked_items.home_page');
        $gallery_page = \Config::get('details.locked_items.gallery');
        $theme_view_path = DOCROOT . \Theme::instance()->asset_path($this->view_dir);
        
        $item = false;

        if($slug == 'referrals' && strtolower(\Sentry::user()->groups()[0]['name']) != 'club members' )
        {
            \Messages::error('Only club members can access this page.');
            \Response::redirect('/');
        }
        // Find content by slug
        if($slug !== false)
        {
            $item = \Page\Model_Page::get_by_slug($slug);
        }
        elseif($slug === false)
        {
            $item = \Page\Model_Page::find_one_by_id($home_page);
        }
        
        if($item)
        {
            // Home page is always without slug so we redirect if someone tries to access home page with slug
            if($item->id == $home_page && $slug !== false) \Response::redirect(\Uri::base(false));
        }
        else
        {
            // Send to 404 page
            throw new \HttpNotFoundException;
        }

        $result = \Page\Model_Page::get_content_file($item->id);

        if($result){

            foreach($result as $rs){

                $item->content_file_pdf     = $rs->file; //pdf file
                $item->content_file_title   = $rs->title; //pdf title

            }

        }


        // Default view file
        $view_file = 'default';
        
        // Fill in home page slug if none is supplied
        if($slug === false && $item->id == $home_page) 
        {
            $slug = $item->seo->slug;
            
            // Home page has its standard template
            $view_file = 'home';
        }
        else
        {
            // Search for page specific template
            $files = \File::read_dir($theme_view_path);
            if(!empty($files))
            {
                foreach ($files as $file)
                {
                    $path_parts = pathinfo($file);
                    if(strpos($path_parts['filename'], '_') === 0)
                    {							
                        $file_id = substr($path_parts['filename'], strripos($path_parts['filename'], '_') + 1);
                        if(is_numeric($file_id) && ($item->id == $file_id))
                        {
                            $view_file = $path_parts['filename'];
                        }							
                    }						
                }
            }		
        }

        // TODO delete this
        srand($item->id);
        
        $this->page_theme = \Theme::instance()->set_partial('content', $this->view_dir . $view_file);
        $this->page_theme->set('item', $item, false);
        
        if($item->id == $gallery_page)
        {
            if($gallery_id && \Input::is_ajax())
            {
                // This will return gallery images only if it is ajax call
                $this->returnGalleryImages($gallery_id);
            }
        }
        
        // Data for home page
        if($view_file == 'home')
        {
            $featured_products = \Product\Model_Group::find_one_by_id(4);
            if($featured_products) $featured_products = $featured_products->products;
            
            $this->page_theme->set('featured_products', $featured_products, false);
            
            $categories = \Product\Model_Category::find(array(
                'where' => array(
                    'parent_id' => 0,
                    'status' => 1,
                    ),
                'order_by' => array('sort' => 'asc'),
                'limit' => 8
            ));
            $this->page_theme->set('categories', $categories, false);
            
            $applications = \Application\Model_Application::find(array(
                'where' => array(
                    'parent_id' => 0,
                    'status' => 1,
                ),
                'order_by' => array('sort' => 'asc'),
                'limit' => 8
            ));
            $this->page_theme->set('applications', $applications, false);
            
            // $this->page_theme->set('blog', $this->wp_blog(), false);
             
        }
        \View::set_global('title', ($item->seo->meta_title ?: $item->title));
        \View::set_global('meta_description', ($item->seo->meta_description ?: ''));
        \View::set_global('meta_keywords', ($item->seo->meta_keywords ?: ''));
        $robots = array('meta_robots_index' => ( $item->seo->meta_robots_index == 1 ? 'index' : 'noindex' ), 'meta_robots_follow' => ( $item->seo->meta_robots_follow == 1 ? 'follow' : 'nofollow' ) );
        \View::set_global('robots', $robots);
        \View::set_global('canonical', $item->seo->canonical_links);
        \View::set_global('h1_tag', $item->seo->h1_tag);
        if($item->seo->redirect_301){
            \Response::redirect($item->seo->redirect_301);
        }
	}
    
    /**
     * Return gallery images for gallery popup
     * Only use in ajax calls
     * 
     * @param type $gallery_id
     */
    private function returnGalleryImages($gallery_id)
    {
        $gallery = \Page\Model_Page::find_one_by_id($gallery_id);
        
        if($gallery)
        {
            echo \Theme::instance()->view($this->view_dir . 'gallery_images')
                    ->set('images', $gallery->accordions, false);
        }
        else
        {
            echo 'Gallery does not exist!! Plese try again.';
        }
        
        exit;
    }
    
    protected function wp_blog()
    {
       $db2 = \Database_Connection::instance('wpblog');
       
       $prefix = $db2->table_prefix();
       
       $query ='
        SELECT
                p1.*,
                wm2.meta_value
            FROM 
                ' . $prefix . 'posts p1
            LEFT JOIN 
                ' . $prefix . 'postmeta wm1
                ON (
                    wm1.post_id = p1.id 
                    AND wm1.meta_value IS NOT NULL
                    AND wm1.meta_key = "_thumbnail_id"              
                )
            LEFT JOIN 
                ' . $prefix . 'postmeta wm2
                ON (
                    wm1.meta_value = wm2.post_id
                    AND wm2.meta_key = "_wp_attached_file"
                    AND wm2.meta_value IS NOT NULL  
                )
            WHERE
                p1.post_status="publish" 
                AND p1.post_type="post"
            ORDER BY 
                p1.post_date DESC
            LIMIT 4
        ';
       
       $result = \DB::query($query)->execute($db2);
       
       return $result;
    }
    
    public function action_show_blog_image($filename = null)
    {
        \Config::load('image_presets', 'image_presets');
        if(!$filename) return false;
        
        $filename = base64_decode($filename);
        $file_url = 'blog/wp-content/uploads/' . $filename; 
        
        if (parse_url($file_url, PHP_URL_SCHEME)) 
        {
            throw new \HttpNotFoundException;
        }

        $image = \Image::forge(array('presets' => \Config::get('image_presets.blog', array())));
        $image->load(DOCROOT . $file_url);
        $image->preset('thumb');
        exit;
        
    }
    
    public function action_show_featured_image($filename = null)
    {
        \Config::load('image_presets', 'image_presets');
        if(!$filename) return false;
        
        $filename = base64_decode($filename);
        $file_url = 'media/images/' . $filename; 
        
        if (parse_url($file_url, PHP_URL_SCHEME)) 
        {
            throw new \HttpNotFoundException;
        }

        $image = \Image::forge(array('presets' => \Config::get('image_presets.featured', array())));
        $image->load(DOCROOT . $file_url);
        $image->preset('thumb');
        exit;
        
    }
    
    public function action_discuss_brief()
    {
        if(\Input::post()) 
        {
            // check for a valid CSRF token
            if (!\Security::check_token())
            {
                \Messages::error('CSRF attack or expired CSRF token.');
                \Response::redirect(\Input::referrer(\Uri::create('/')));
            } 
            
            $file = null;
            
            // Send autoresponder
            $autoresponder = \Autoresponder\Autoresponder::forge();
            
            $autoresponder->view_custom = 'discuss_brief';
            $autoresponder->view_admin = 'discuss_brief';
            
            $post = \Input::post();
            
            if($product = \Product\Model_Product::find_one_by_id(\Input::post('product')))
            {
                $post['product'] = $product;
            }
            
            $content['content'] = $post;
            
            $config = array(
                'path' => APPPATH.'tmp',
                'normalize' => true,
                'max_size' => 5242880,
                //'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
            );

            // Check if file uploaded
            if(isset($_FILES['fileUpload']['name']) && $_FILES['fileUpload']['name'] != '')
            {
                // process the uploaded files in $_FILES
                \Upload::process($config);

                // if there are any valid files
                if (\Upload::is_valid())
                {
                    // save them according to the config
                    \Upload::save(); 

                    $file = \Upload::get_files(0);
                }

                // Upload errors
                if(\Upload::get_errors() !== array())
                {
                    foreach (\Upload::get_errors() as $file)
                    {
                        foreach($file['errors'] as $key => $value)
                        {
                            \Messages::error($value['message']);
                        }
                    }

                    \Response::redirect(\Input::referrer(\Uri::create('/')));
                }
            }
            
            $attachment = array();
            if(isset($file['saved_to']) && is_file($file['saved_to'] . $file['saved_as']))
            {
                $attachment = array($file['saved_to'] . $file['saved_as']);
            }
            // echo 'test';
            // die;

            $content['subject'] = 'Thanks for contacting';
            $autoresponder->autoresponder_custom($content, \Input::post('email'), $attachment);
            
            $content['subject'] = 'Autoresponder Discuss Brief for Admin';
            $autoresponder->autoresponder_admin($content, \Config::get('auto_response_emails.discuss_brief'),  $attachment);
           
            if($autoresponder->send())
            {
                \Messages::success('Thank You for sending request.');
            }
            else
            {
                \Messages::error('There was an error while trying to submit request.');
            }
            
            // Delete uploaded files
            if(!empty($attachment))
            {
                foreach ($attachment as $file)
                {
                    if(is_file($file)) unlink($file);
                }
            }
                
            \Response::redirect(\Input::referrer(\Uri::create('/')));
        }
        
        if(\Input::is_ajax())
        {
            
            $products = \Product\Model_Product::fetch_pair('id', 'title', array(
                'order_by' => array(
                    'title' => 'asc',
                ),
            ));
            
            echo \Theme::instance()->view('views/_partials/discuss_brief')
                    ->set('products', $products, false);
            exit;
        }
        
        throw new \HttpNotFoundException;
        
    }

    public function action_send_contact_form()
    {
        // Validate required fields
        if(!\Input::post('name') || !\Input::post('email') || !\Input::post('subject') || !\Input::post('message'))
        {
            \Messages::error('Fill required fields.');
            \Response::redirect('contact-us');
        }

        if(\Input::post('g-recaptcha-response'))
        {
            // Verify Google reCaptcha
            $post_data = http_build_query(
                array(
                    'secret' => \Config::get('details.recaptcha.secret_key'),
                    'response' => \Input::post('g-recaptcha-response'),
                    'remoteip' => $_SERVER['REMOTE_ADDR']
                )
            );
            $opts = array('http' =>
                array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => $post_data
                ),
                'ssl' => array(
                    'verify_peer' => false,
                ),
            );
            $context  = stream_context_create($opts);
            $response = file_get_contents(\Config::get('details.recaptcha.siteverify_url'), false, $context);
            $result = json_decode($response);
            if (!$result->success) {
                \Messages::error('CAPTCHA verification failed');
                \Response::redirect('contact-us');
            }
            // EOF Verify Google reCaptcha

            $settings = \Config::load('autoresponder.db');
            // Send autoresponder
            $autoresponder = \Autoresponder\Autoresponder::forge();
            $autoresponder->view_user = 'contact_us';
            $content['subject'] = 'Contact Us : '. \Input::post('subject');
            $content['name'] = \Input::post('name');
            $content['email'] = \Input::post('email');
            $content['message'] = \Input::post('message');
            $content['address'] = $settings['address'];
            $content['website'] = $settings['website'];
            $content['company_name'] = $settings['company_name'];
            $autoresponder->autoresponder_custom_admin($content, $settings['contact_us_email_address']);

            if($autoresponder->send())
            {
                \Messages::success('The mail has been sent successfully.');
            }
            else
            {
                \Messages::error('There was an error while trying to sent email.');
            }
        }
        else
        {
            \Messages::error('Invalid Captcha');
        }

        \Response::redirect('contact-us');
    }


    public function action_add_referal()
    {
        if(\Input::post()) 
        {
            $val = \User\Model_Referal::validate('create');
            if($val->run())
            {
                $insert = \Input::post();
                $item = \User\Model_Referal::forge($insert);

                try{

                    $settings = \Config::load('autoresponder.db');
                    // Send autoresponder
                    $autoresponder = \Autoresponder\Autoresponder::forge();
                    $autoresponder->view_user = 'referral';
                    $content['subject'] = 'Referrals';
                    $content['name'] = \Sentry::user()['metadata']['first_name']." ".\Sentry::user()['metadata']['last_name'];
                    $content['email'] = \Sentry::user()['email'];
                    $content['referred_name'] = \Input::post('name');
                    $content['referred_email'] = \Input::post('email');
                    $content['phone'] = \Input::post('phone');
                    $content['suburb'] = \Input::post('suburb');
                    $content['address'] = $settings['address'];
                    $content['website'] = $settings['website'];
                    $content['company_name'] = $settings['company_name'];

                    // send to all admin
                    $admins = \Sentry::user()->all('admin');
                    $emails = array();
                    foreach ($admins as $key => $admin) {
                        array_push($emails, $admin['email']);
                    }
                    $autoresponder->autoresponder_custom_admin($content, $settings['sender_email_address']);//$emails);
                    // $autoresponder->autoresponder_custom_admin($content, \Config::get('auto_response_emails.default_emails'));

                    if($autoresponder->send())
                    {
                        $item->save();
                        \Messages::success('Successfully Submitted.');
                        \Response::redirect('referrals');
                    }
                    else
                    {
                        \Messages::error('There was an error while trying to submit the form.');
                    }
                }
                catch (\Database_Exception $e)
                {
                    // show validation errors
                    \Messages::error('There was an error while trying to submit the form.');
                    
                    // Uncomment lines below to show database errors
                    $errors = $e->getMessage();
                    \Messages::error($errors);
                }

                \Response::redirect('referrals');
            }
            else
            {
                if($val->error() != array())
                {
                    // show validation errors
                    \Messages::error('There was an error while trying to submit the form.');
                    foreach($val->error() as $e)
                    {
                        \Messages::error($e->get_message());
                    }
                }
                \Response::redirect('referrals');
            }
        }
    }

}
