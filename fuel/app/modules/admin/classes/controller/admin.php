<?php
/**
 * /fuel/app/modules/admin/classes/controller
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    1.0
 * @author     CMS Development Team
 */

namespace Admin;

/**
 * Administration dashboard
 */
class Controller_Admin extends Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/';
	
	/**
	 * The index action.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_index()
	{

		// Keep existing messages
		
		\Messages::instance()->shutdown();
		($this->check_access()) ? \Response::redirect('admin/order/list') : \Response::redirect('admin/dashboard');
	}

	/**
	 * Admin dashboard view
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_dashboard($type = 'm')
	{
		
		if($this->check_access()){
			\Response::redirect('admin/order/list'); 
		}
		else{
			$orders = \Dashboard::get_order($type);

			$visitors = \Dashboard::get_visits($type);
			

			$c_orders = \Dashboard::get_chart_order();
			$c_visits = \Dashboard::get_chart_visits();


			$sales_chart = '';
			$hold_c_orders = array();
			foreach($c_orders as $order)
				$hold_c_orders[$order['MNTH']] = $order['total_price'];

			$visits_chart = '';
			$hold_c_visits = array();
			foreach($c_visits as $visit)
				$hold_c_visits[$visit['MNTH']] = $visit['visits'];

			for ($i=1; $i <= 12; $i++)
			{
				if(isset($hold_c_orders[$i]))
					$sales_chart .= (float)$hold_c_orders[$i].',';
				else
					$sales_chart .= '0,';
				
				if(isset($hold_c_visits[$i]))
					$visits_chart .= (float)$hold_c_visits[$i].',';
				else
					$visits_chart .= '0,';
			}

			if ($type == 'd') {
	            $type = 'Today';
	        } elseif ($type == 'm') {
	            $type = 'This Month';
	        } elseif ($type == 'y') {
	            $type = 'This Year';
	        } else {
	            $type = 'This Week';
	        }


			$users 	= \Sentry::user()->all('front');

	        $items['orders'] 	= $orders[0]['total_order'];
	        $items['sales'] 	= $orders[0]['total_sales'];
	        $items['visits'] 	= $visitors[0]['visits'];
	        $items['users'] 	= count($users);
	        $items['sales_chart'] = $sales_chart;
	        $items['visits_chart'] = $visits_chart;
	        $items['type'] = $type;

			//\View::set_global('full_page', true);
			\View::set_global('title', 'Dashboard');
			\View::set_global('menu', 'admin/dashboard');

			\Theme::instance()->set_partial('content', $this->view_dir . 'dashboard')
				->set('items', $items);
		}
	}
	
	/**
	 * The index action.
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_settings()
	{
		\View::set_global('title', 'Settings');
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'settings');
	}


	public function check_access(){
		return \SentryAdmin::user()->is_production() || \SentryAdmin::user()->in_group('Admin');;
	}
}

/* End of file admin.php */