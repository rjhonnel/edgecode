<?php
/**
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    1.0
 * @author     CMS Development Team
 */

namespace Admin;

class Controller_Logout extends Controller_Base
{
	/**
	 * The module index
	 *
	 * @return  Response
	 */
	public function action_index()
	{
		\SentryAdmin::logout();
		\Messages::success('You have successfully logged out');
		\Response::redirect('admin/login');
	}
}
