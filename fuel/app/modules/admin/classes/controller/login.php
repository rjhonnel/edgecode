<?php
/**
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    1.0
 * @author     CMS Development Team
 */

namespace Admin;

class Controller_Login extends \Controller_Base_Public
{

	public function before()
	{
		// already logged in?
		if (\SentryAdmin::check() && \SentryAdmin::user()->is_admin())
		{
			if(\SentryAdmin::user()->is_production()){
				\Messages::info('You are already logged in');
				\Response::redirect('admin/order/list');
			}
			else{
				\Messages::info('You are already logged in');
				\Response::redirect('admin');
			}
			
		}

		parent::before();
	
		\Theme::instance()->active('admin');
		\Theme::instance()->set_template($this->template);
	}

	/**
	 * The module index
	 *
	 * @return  Response
	 */
	public function action_index()
	{
        \View::set_global('full_page', true);
        
		$this->data['title'] = 'Login';
		
		// create the form fieldset, do not add an {open}, a closing ul and a {close}, we have a custom form layout!
		$fieldset = \Fieldset::forge('login');
		$fieldset->add('username', 'Username', array('maxlength' => 50), array(array('required')))
			->add('password', 'Password', array('type' => 'password', 'maxlength' => 255), array(array('required'), array('min_length', 8)));

		// was the login form posted?
		if (\Input::post())
		{
			// run the form validation
			if ( ! $fieldset->validation()->run())
			{
				// set any error messages we need to display
				foreach ($fieldset->validation()->error() as $error)
				{
					\Messages::error($error);
				}
			}
			else
			{
				try
				{
					if(\SentryAdmin::user(\Input::param('username'))->is_admin())
					{
						// check the credentials.
						$valid_login = \SentryAdmin::login(\Input::param('username'), \Input::param('password'), true);
						
						if($valid_login)
						{
							\Messages::success('You have logged in successfully');
							
							if(\Session::get('redirect_to'))
							{
								$redirect = \Session::get('redirect_to');
								\Session::delete('redirect_to');
							}
							if(\SentryAdmin::user()->is_production()){
								\Response::redirect(isset($redirect) ? $redirect : 'admin/order/list');
							}
							else{
								\Response::redirect(isset($redirect) ? $redirect : 'admin');
							}
			
						}
						else
						{
							\Messages::error('Username and/or password is incorrect');
						}
					}
					else
					{
						\Messages::error('Username and/or password is incorrect');
					}
				}
				catch (\SentryAuthException $e)
				{
				    $errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
		}
		
		\Theme::instance()->set_partial('content', 'views/login')->set('fieldset', $fieldset, false);
	}
}
