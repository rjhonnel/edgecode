<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,

 	'status' => array(
        'in_progress'           => 'In Progress',
        'received'              => 'Received',
        'shipped'               => 'Shipped',
        'invoiced'              => 'Invoiced',
        'partially_paid'        => 'Partially Paid',
        'paid'                  => 'Paid',
        'cancelled'             => 'Canceled',
    ),
     
 );