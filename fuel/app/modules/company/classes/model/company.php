<?php

	namespace Company;

	class Model_Company extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
        
        public static $sort = 'asc';
        
        public static $sorting = array();
	
	    // Set the table to use
	    protected static $_table_name = 'companies';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'company_name',
		    'company_mobile',
		    'company_phone',
		    'company_email',
		    'company_address',
		    'company_suburb',
		    'company_state',
		    'company_postcode',
		    'company_country',
		    'user_created',
		    'user_updated',
		    'sort',
		);

		protected static $_defaults = array();
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';


	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('company_name', 'Company Name')->add_rule('required')->add_rule('min_length', 4)->add_rule('max_length', 255);

			return $val;
		}

		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				$vars['user_created']	= \Sentry::user()->id;
				// Auto increment sort column
				$vars['sort']			= \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}

		public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    		      
		    			// Get company users
	
						$item->get_customers = static::lazy_load(function() use ($item){
							
							$users = \SentryAdmin::user()->all('front');
			    			foreach($users as $key=> $user)
				            {
				                $users[$key] = \SentryAdmin::user((int)$user['id']);
				            }
							$data = array();
						
		    				foreach($users as $key => $user){
			    			
			    				if($user->metadata['company'] == $item->id){
		    						array_push($data,$users[$key]);
		    					}
			    			}

			    			return $data;

		    			}, $item->id, 'customers');

						// Get company orders

		    			$item->get_orders = static::lazy_load(function() use ($item){
		    				return \Order\Model_Order::find(array(
							    'where' => array(
							        'company_id' => $item->id,
							    ),
							));
		    			}, $item->id, 'orders');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }

		private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Company\Model_Company::$temp['lazy.'.$id.$type]))
		    	{
		    		\Company\Model_Company::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Company\Model_Company::$temp['lazy.'.$id.$type])) 
			    			\Company\Model_Company::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Company\Model_Company::$temp['lazy.'.$id.$type])) 
			    			\Company\Model_Company::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Company\Model_Company::$temp['lazy.'.$id.$type];
	    	};
	    }

	    public static function customers($company_id = false){
	    	$users = \SentryAdmin::user()->all_user_companies($company_id);
	    
			foreach($users as $key=> $user)
            {
                $users[$key] = \SentryAdmin::user((int)$user['user_id']);
            }
         	
         	return $users;
	    }

	    public static function orders($company_id = false){
	    	$users = self::customers($company_id);
	    	$orders = \Order\Model_Order::find(function($query){            
            	$query->where('finished', '1');
            	$query->order_by('id', 'desc');
	    	});
	    	$data = array();
	    	for($x=0;$x<sizeof($orders);$x++){
	  
	    		foreach ($users as $key => $value) {
	    			if($orders[$x]->user_id == $value->id){
	    				
	    				array_push($data, $orders[$x]);
	    			}
	    		}
	    		
	    	}
	    	return $data;
	    }

	    public static function notes($company_id = false){
	    	$users = self::customers($company_id);
	    	$notes = \User\Model_Notes::find();
	    	$data = array();
	    	
	    	for($x=0;$x<sizeof($notes);$x++){
	  
	    		foreach ($users as $key => $value) {
	    			if($notes[$x]->user_id == $value->id){
	    				$notes[$x]->name = $value->metadata['first_name'].' '.$value->metadata['last_name'];
	    				array_push($data, $notes[$x]);
	    			}
	    		}
	    		
	    	}

	    	return $data;
	    	
	    }
	}