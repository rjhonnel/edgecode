<?php

namespace Company;

class Controller_Admin_Order extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/company/order/';

	public function before()
	{
		parent::before();
		
		\Config::load('company::company', 'details');
        \Config::load('order::order', 'order');
        \Config::load('auto_response_emails', true);
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/company/list');
	}
	
	public function action_list($company_id = false)
	{
        \View::set_global('menu', 'admin/company/list');
		\View::set_global('title', 'List Orders');
		
		$this_class = $this;

		$get_attributes = function($attributes) use ($this_class)
        {
            return $this_class->attributes_to_string($attributes);
        };

        $search = $this->get_search_items($company_id);
		
        $items      = $search['items'];

        $pagination = $search['pagination'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('get_attributes', $get_attributes, false)
			->set('pagination', $pagination, false);
	}

	public function get_search_items($company_id = false){
		 // Override company if its a search
        $company_id = \Input::get('company_id', $company_id);
        
        if($company_id)
        {
            $company = \Company\Model_Company::find_one_by_id($company_id);
            if($company)
            {
                \View::set_global('company', $company);
            }
        }
     

		$items = Model_Company::orders($company_id);
	
     	foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
                        foreach($items as $number => $item)
                        {
                        	
                            $full_name = $item->first_name . ' ' . $item->last_name;
                           
                            $order_id = $item->id;

                            if(stripos($full_name, $value) === false && stripos($order_id, $value) === false ) unset($items[$number]);
                            
                        }
                        break;
					}
				}
			}
		
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        return array('items' => $items, 'pagination' => $pagination);
	}

	 public function attributes_to_string($attributes = false)
    {
        $out = false;
        if(json_decode($attributes, true) != null)
        {
            $attributes = json_decode($attributes, true);
        }
        if(is_array($attributes))
        {
            foreach($attributes as $key => $value)
            {
                $out[] = $key . ': ' . $value;
            }
        }
        if($out) $out = implode(' | ', $out); 
        return $out;
    }
}