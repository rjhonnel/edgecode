<?php

namespace Company;

class Controller_Admin_User extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/company/user/';
	
	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/company/list');
	}
	
	public function action_list($company_id = false)
	{
        \View::set_global('menu', 'admin/company/list');
		\View::set_global('title', 'List Customers');

        $search = $this->get_search_items($company_id);
		
        $items      = $search['items'];
        
        $pagination = $search['pagination'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}

	public function get_search_items($company_id = false){
		 // Override company if its a search
        $company_id = \Input::get('company_id', $company_id);
        
        if($company_id)
        {
            $company = \Company\Model_Company::find_one_by_id($company_id);
            if($company)
            {
                \View::set_global('company', $company);
            }
        }
     

		$items = Model_Company::customers($company_id);
     	
     	foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
                        foreach($items as $number => $item)
                        {
                        	
                            $full_name = $item->metadata['first_name'] . ' ' . $item->metadata['last_name'];
                           
                            $customer_id = $item->metadata['user_id'];

                            if(stripos($full_name, $value) === false && stripos($customer_id, $value) === false ) unset($items[$number]);
                            
                        }
                        break;
					}
				}
			}
		
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        return array('items' => $items, 'pagination' => $pagination);
	}
}