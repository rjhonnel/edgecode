<?php

namespace Company;

class Controller_Admin_Company extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/company/';
	
	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/company/list');
	}
	
	public function action_list($company_id = false)
	{
        \View::set_global('menu', 'admin/company/list');
		\View::set_global('title', 'List Companies');
		
        $search = $this->get_search_items($company_id);
        $items      = $search['items'];
        $pagination = $search['pagination'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}

	public function action_create(){
		\View::set_global('menu', 'admin/company/list');
		\View::set_global('title', 'Add New Company');

		if(\Input::post())
		{
			$val = Model_Company::validate('create');
			if($val->run()){
				$insert = \Input::post();
				$item = Model_Company::forge($insert);
				try{
					$item->save();
					\Messages::success('Company successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/company/update/' . $item->id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create company</strong>');
				}

			}
			else{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update company</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}

		}

		\Theme::instance()->set_partial('content', $this->view_dir . 'create');
	}

	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/company/list');
		
		// Get news item to edit
		if(!$item = Model_Company::find_one_by_id($id)) \Response::redirect('admin/company/list');
		
		\View::set_global('menu', 'admin/company/list');
		\View::set_global('title', 'Edit Company');

		if(\Input::post())
		{
			$val = Model_Company::validate('update');
			if($val->run()){
				$insert = \Input::post();
				$item->set($insert);
				try{
					$item->save();
					\Messages::success('Company successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/company/list/') : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create company</strong>');
				}
			}
			else{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update company</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}

		$company = Model_Company::find_one_by_id($id);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('company', $company);
	}	
	public function action_delete($id = false){
	
		if(is_numeric($id))
		{
			// Get company delete
			if($item = Model_Company::find_one_by_id($id))
			{
				// Delete item
				try{
					// Delete company
					$item->delete();
					
					\Messages::success('Company successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete company</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	public function get_search_items($company_id = false){
		 // Override company if its a search
        $company_id = \Input::get('company_id', $company_id);
        
        if($company_id)
        {
            $company = \Company\Model_Company::find_one_by_id($company_id);
            if($company)
            {
                \View::set_global('company', $company);
            }
        }
        
        /************ Start generating query ***********/
		$items = Model_Company::find(function($query) use ($company_id){
			
			// Get search filters
			foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
							$key = 'company_name';
							$query->where($key, 'like', "%$value%");
							break;
					}
				}
			}
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
        
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        return array('items' => $items, 'pagination' => $pagination);
	}
}