<?php
namespace Yousendit;

require_once (APPPATH . 'vendor/yousendit/lib/src/StorageAPIs.php');
class Base
{
    protected $errorcode;
    protected $errormessage;
    protected $session = array('sTr', 'sEmail', 'sPass', 'sToken', 'sApp', 'sHost', 'sUserAgent');
    
     /**
	 * @var	array	Pagination instances
	 */
	protected static $_instances = array();

	/**
	 * @var	array	Pagination default instance
	 */
	protected static $_instance = null;
    
    /**
	 * forge a new yousendit instance
	 *
	 * @return  a new yousendit instance
	 */
	public static function forge($name = 'default', $config = array())
	{
        \Config::load('yousendit::yousendit', 'yousendit');
         
		if ($exists = static::instance($name))
		{
			//\Error::notice('Yousendit instance with this name exists already, cannot be overwritten.');
			return $exists;
		}

		static::$_instances[$name] = new static($config);

		if ($name == 'default')
		{
			static::$_instance = static::$_instances[$name];
		}

		return static::$_instances[$name];
	}

	/**
	 * retrieve an existing yousendit instance
	 *
	 * @return	a existing yousendit instance
	 */
	public static function instance($name = null)
	{
		if ($name !== null)
		{
			if ( ! array_key_exists($name, static::$_instances))
			{
				return false;
			}

			return static::$_instances[$name];
		}

		if (static::$_instance === null)
		{
			static::$_instance = static::forge();
		}

		return static::$_instance;
	}
    
    public function check_session()
    {
        $out = true;
        foreach ($this->session as $session)
        {
            if(!\Session::get("ysi.{$session}", false)) $out = false;
        }
        return $out;
    }
    
    public function login()
    {
        if($this->check_session()) return true;
        
        $restTransportInstance = new \RestTransport(\Config::get('yousendit.host'),\Config::get('yousendit.api_key'));
        \Session::set('ysi.sTr', $restTransportInstance);

        $authInstance = new \Authentication(\Config::get('yousendit.host'), \Config::get('yousendit.api_key'), \Session::get('ysi.sTr'));
        $auth = $authInstance->login(\Config::get('yousendit.email'), \Config::get('yousendit.password'));

        $errorStatus = $auth->getErrorStatus();
        $token = $auth->getAuthToken();
        
        if (!empty($errorStatus)){
            $this->errorcode = $errorStatus->getMessage();
            $this->errormessage = $errorStatus->getCode();
            return false;
        }
        else if (empty($token))
        {
            $this->errormessage = 'Invalid apikey or hostname!';
            return false;
        }
        else
        {
             \Session::set('ysi.sEmail', \Config::get('yousendit.email'));
             \Session::set('ysi.sPass', \Config::get('yousendit.password'));
             \Session::set('ysi.sToken', $token);
             \Session::set('ysi.sApp', \Config::get('yousendit.api_key'));
             \Session::set('ysi.sHost', \Config::get('yousendit.host'));
             \Session::set('ysi.sUserAgent', 'we');
             return true;
        }
        return false;
    }
    
    public function logout()
    {
        \Session::delete('ysi');
        
        if(!\Session::get('ysi.sTr', null) && !\Session::get('ysi.sToken', null)) return true;
        return false;
    }
    
    public function get_errors()
    {
        return array(
            'errorcode' => $this->errorcode,
            'errormessage' => $this->errormessage,
        );
    }
    
    public function folder_info($folder_id = 'default', $include_files = true, $include_folders = true)
    {
        $folder = new \StorageAPIs(\Session::get('ysi.sHost'),\Session::get('ysi.sApp'), \Session::get('ysi.sTr'));
        $folder->setAuthToken(\Session::get('ysi.sToken'));
        
        if($folder_id == 'default')
        {
            $response = $folder->getFolderInfo(\Config::get('yousendit.default_folder_id'), $include_files, $include_folders);
        }
        else
        {
            $response = $folder->getFolderInfo($folder_id, $include_files, $include_folders);
        }
        
        $current = $response->getErrorStatus();
        
        if (!empty($current))
        {
            $this->errorcode = $current->getMessage();
            $this->errormessage = $current->getCode();
            return false;
        }
        else
        {
            return $response;
        }
        
    }
    
    public function get_folder($find, $folders_array)
    {
        if(is_string($find))
        {
            $method = 'getName';
        }
        else
        {
           $method = 'getId';
        }
        
        if(is_array($folders_array) && !empty($folders_array))
        {
            foreach ($folders_array as $folder)
            {
                if($find == $folder->{$method}()) return $folder;
            }
        }
        return false;
    }
    
    public function create_folder($name = null, $parentid = 'default')
    {
        $out = array();
        
        if(strlen($name) == 0)
        {
            $this->errormessage = 'Empty mandatory field(s)!';
            return false;
        }
       
        $folder = new \StorageAPIs(\Session::get('ysi.sHost'),\Session::get('ysi.sApp'), \Session::get('ysi.sTr'));
        $folder->setAuthToken(\Session::get('ysi.sToken'));
        
        if($parentid == 'default')
        {
            $response = $folder->createFolder($name, \Config::get('yousendit.default_folder_id'));
        }
        elseif(is_numeric($parentid))
        {
            $response = $folder->createFolder($name, $parentid);
        }
        else
        {
            $response = $folder->createFolder($name);
        }
        
        $current = $response->getErrorStatus();
        
        if (!empty($current))
        {
            $this->errorcode = $current->getMessage();
            $this->errormessage = $current->getCode();
            return false;
        }
        else
        {
            return $response;
        }
        
        return false;
        
        exit;
        
        if (strlen($var2)==0){
            $responseObject = $folder->createFolder($var1);
        }else{
            $responseObject = $folder->createFolder($var1,$var2);
        }


        $current = $responseObject->getErrorStatus();
        if (!empty($current))
        {
            echo "Error Status: \n";
            echo " Error code: ".$current->getCode(). "\n";
            echo " Error message: ".$current->getMessage()."\n";
        }
        else
        {
            $current = $responseObject->getRevision();
            if (!empty($current))
            {
                echo " Revision: ".$current."\n";
            }
            else
            {
                echo " Revision: N/A\n";
            }
            $current = $responseObject->getId();
            if (!empty($current))
            {
                echo " Id: ".$current."\n";
            }
            else
            {
                echo " Id: N/A\n";
            }

            $current = $responseObject->getCreatedOn();
            if (!empty($current))
            {
                echo " Created On: ".$current."\n";
            }
            else
            {
                echo " Created On: N/A\n";
            }
            $current = $responseObject->getFileCount();
            if(strlen($current)==0)
            {
                echo " File Count: N/A\n";
            }
            else
            {
                echo " File Count: ".$current."\n";
            }

            $current = $responseObject->getFolderCount();
            if(strlen($current)==0)
            {
                echo " Folder Count: N/A\n";
            }
            else
            {
                echo " Folder Count: ".$current."\n";
            }

            $current = $responseObject->getName();
            if (strlen($current)==0)
            {
                echo " Name: N/A\n";
            }
            else
            {
                echo " Name: ".$current."\n";
            }
            $current = $responseObject->getParentId();
            if (strlen($current)==0)
            {
                echo " Parent Id: N/A\n";
            }
            else
            {
                echo " Parent Id: ".$current."\n";
            }
            $current = $responseObject->getReadable();
            if (strlen($current)==0)
            {
                echo " Readable: N/A\n";
            }
            else
            {
                echo " Readable: ".$current."\n";
            }

            $current = $responseObject->getSize();
            if (strlen($current)==0)
            {
                echo " Size: N/A\n";
            }
            else
            {
                echo " Size: ".$current."\n";
            }
            $current = $responseObject->getType();
            if (!empty($current))
            {
                echo " Type: ".$current."\n";
            }
            else
            {
                echo " Type: N/A\n";
            }
            $current = $responseObject->getUpdatedOn();
            if (!empty($current))
            {
                echo " Updated On: ".$current."\n";
            }
            else
            {
                echo " Updated On: N/A\n";
            }
            $current = $responseObject->getWritable();
            if (strlen($current)==0)
            {
                echo " Writable: N/A\n";
            }
            else
            {
                echo " Writable: ".$current."\n";
            }

            $files = $responseObject->getFiles();
            if (!empty($files)){
                echo "\nFiles: \n";
                foreach($files as $file){
                    $current = $file->getRevision();
                    if (!empty($current)){
                        echo " Revision: ".$current."\n";
                    }else{
                        echo " Revision: N/A\n";
                    }
                    $current = $file->getId();
                    if (!empty($current)){
                        echo " Id: ".$current."\n";
                    }else{
                        echo " Id: N/A\n";
                    }
                    $current = $file->getCreatedOn();
                    if (!empty($current)){
                        echo " Created On: ".$current."\n";
                    }else{
                        echo " Created On: N/A\n";
                    }
                    $current = $file->getName();
                    if (!empty($current)){
                        echo " Name: ".$current."\n";
                    }else{
                        echo " Name: N/A\n";
                    }
                    $current = $file->getParentId();
                    if (strlen($current)==0){
                        echo " Parent Id: N/A\n";
                    }else{
                        echo " Parent Id: ".$current."\n";
                    }
                    $current = $file->getSize();
                    if (!empty($current)){
                        echo " Size: ".$current."\n";
                    }else{
                        echo " Size: N/A\n";
                    }
                }
            }else{
                echo "\nFiles: N/A\n";
            }
        }
    }
    
    public function init_upload()
    {
        $out = array();
        $var0 = trim(\Input::post('token')," ");
        if (strlen($var0)==0)
        {
            $out = array(
                'errorcode'=>'', 
                'errormessage'=>'Empty Authentication Token field.'
                );
        }
        else 
        {
            // Check order
            if(!$folder_id = $this->prepare_folder())
            {
                echo json_encode($this->get_errors());
                exit;
            }
            
            $uploadObj = new \StorageAPIs(\Session::get('ysi.sHost'),\Session::get('ysi.sApp'), \Session::get('ysi.sTr'));
            $uploadObj->setAuthToken($var0);
            $initUpload = $uploadObj->initUpload();
            $current = $initUpload->getErrorStatus();
            if (!empty($current))
            {
                $out = array(
                    'errorcode' => $current->getCode(),
                    'errormessage' => $current->getMessage()
                );
            }
            else
            {
                $fileId = $initUpload->getFileId();
                if (!empty($fileId))
                {
                    $out = array('itemid'=>$fileId);
                }
                else
                {
                    $out = array('itemid'=>"N/A");
                }
                $uploadURL = $initUpload->getUploadUrl();
                if (!empty($uploadURL))
                {
                    $a[] = $uploadURL;
                    $out['URLs'] = $a;
                }
                else
                {
                    $a[] = "N/A";
                    $out['URLs'] = $a;
                }

            }
        }
        $out['met']='INIT UPLOAD';
        echo json_encode($out);
    }
    
    public function prepare_folder()
    {
        // Get info about orders folder
        if(!$response = $this->folder_info())
        {
            $this->errormessage = 'Internal server error, please try again 1.';
            return false;
        }
        
        if(!\Session::get('order.id'))
        {
            $this->errormessage = 'Internal server error, please try again 2.';
            return false;
        }
        
        $folders = $response->getFolders();

        if($folders)
        {
            $folder = $this->get_folder((string)\Session::get('order.id'), $response->getFolders());
            if(!$folder)
            {
                if($new_folder = $this->create_folder(\Session::get('order.id')))
                {
                    \Session::set('ysi.folderId', $new_folder->getId());
                    return $new_folder->getId();
                }
                else
                {
                    $this->errormessage = 'Internal server error, please try again 3.';
                    return false;
                }
            }
            else 
            {
                \Session::set('ysi.folderId', $folder->getId());
                return $folder->getId();
            }
        }
        else
        {
            if($new_folder = $this->create_folder(\Session::get('order.id')))
            {
                \Session::set('ysi.folderId', $new_folder->getId());
                return $new_folder->getId();
            }
            else
            {
                $this->errormessage = 'Internal server error, please try again 4.';
                return false;
            }
        }
        
        $this->errormessage = 'Internal server error, please try again 5.';
        return false;
    }
    
    public function commit_upload($parent_id = null)
    {
        $out = array();
        $uploadObj = new \StorageAPIs(\Session::get('ysi.sHost'),\Session::get('ysi.sApp'), \Session::get('ysi.sTr'));
        
        $var0 = trim(\Input::post('var0', \Input::post('token'))," ");
        $var1 = trim(\Input::post('var1')," ");
        $var2 = trim(\Input::post('var2', \Input::post('itemid'))," ");
        $product_id = trim(\Input::post('var3', \Input::post('product_id'))," ");
        $type = trim(\Input::post('var4', \Input::post('type'))," ");
        $cart_uid = trim(\Input::post('var5', \Input::post('uid'))," ");
        $quantity = trim(\Input::post('var6', \Input::post('artwork_quantity_new'))," ");
        
        if($parent_id) $var1 = $parent_id;
        
        //echo "\nCOMMIT UPLOAD: \n\n";
        if (strlen($var0)==0)
        {
            //echo "\n Error: Empty Authentication Token field!\n";
            $out = array(
                'errorcode' => '',
                'errormessage' => 'Internal server error, please try again.'
                );
        }
        else
        {
            $uploadObj->setAuthToken($var0);
            if ((strlen($var1)==0) || ($var1=='undefined'))
            {
                $commitUpload = $uploadObj->commitFileUpload($var2);
            }
            else
            {
                $commitUpload = $uploadObj->commitFileUpload($var2, $var1);
            }
            
            $current = $commitUpload->getErrorStatus();
            
            if (!empty($current))
            {
                $out = array(
                    'errorcode' => $current->getCode(),
                    'errormessage' => $current->getMessage(),
                    );
            }
            else
            {
                $current = $commitUpload->getRevision();
                if (!empty($current))
                {
                    $out['data']['revision'] = $current;
                }
                else
                {
                    $out['data']['revision'] = 'N/A';
                }
                
                $current = $commitUpload->getId();
                if (!empty($current))
                {
                    $out['data']['file_id'] = $current;
                }
                else
                {
                    $out['data']['file_id'] = 'N/A';
                }
                
                $current = $commitUpload->getCreatedOn();
                if (!empty($current))
                {
                    $out['data']['created_on'] = $current;
                }
                else
                {
                    $out['data']['created_on'] = 'N/A';
                }
                
                $current = $commitUpload->getName();
                if (!empty($current))
                {
                    $out['data']['name'] = $current;
                }
                else
                {
                    $out['data']['name'] = 'N/A';
                }
                
                $current = $commitUpload->getOwnedByStorage();
                if (!empty($current))
                {
                    $out['data']['owned_by_storage'] = $current;
                }
                else
                {
                    $out['data']['owned_by_storage'] = 'N/A';
                }
                
                $current = $commitUpload->getParentId();
                if (strlen($current)==0)
                {
                    $out['data']['parent_id'] = $current;
                }
                else
                {
                    $out['data']['parent_id'] = 'N/A';
                }

                $current = $commitUpload->getSize();
                if (!empty($current))
                {
                    $out['data']['size'] = $current;
                }
                else
                {
                    $out['data']['size'] = 'N/A';
                }
                
                $out['data']['product_id'] = $product_id;
                $out['data']['type'] = $type;
                $out['data']['cart_uid'] = $cart_uid; 
                $out['data']['quantity'] = $quantity;
                
                $this->save_artwork($out['data']);

            }
        }
        
        echo json_encode($out);
        
        exit;
    }
    
    public function save_artwork($data = array())
    {
        if(empty($data)) return false;
       
        if(is_numeric($data['size']) && $data['size'] > 0)
        {
            if(\Cart::exists($data['cart_uid']))
            {
                $cart_item = \Cart::item($data['cart_uid']);
                $data['unique_id'] = $cart_item->get('unique_id');
            }

            $data['order_id'] = \Session::get('order.id');
            $artwork = \Order\Model_Artwork::forge($data);
            $artwork->save();
        }
    }
    
    public function delete_artwork($file_id = null, $return = false)
    {
        $out = array();
        
        $storage = new \StorageAPIs(\Session::get('ysi.sHost'),\Session::get('ysi.sApp'), \Session::get('ysi.sTr'));

        if ((strlen(\Session::get('ysi.sToken'))==0)||(strlen($file_id)==0))
        {
             $out = array(
                'errorcode' => '',
                'errormessage' => 'Internal server error, please try again.'
                );
        }
        else
        {
            $storage->setAuthToken(\Session::get('ysi.sToken'));
            $responseObject = $storage->deleteFile($file_id);

            $current = $responseObject->getErrorStatus();
            if (!empty($current))
            {
                $out = array(
                    'errorcode' => '',
                    'errormessage' => 'Internal server error, please try again.'
                );
                
                //echo " Error code: ".$current->getCode(). "\n";
                //echo " Error message: ".$current->getMessage()."\n";
            }
            else
            {
                // Delete artwork from db
                if($artwork = \Order\Model_Artwork::find_one_by_file_id($file_id))
                {
                    $unique_id = $artwork->unique_id;
                    $order_id = $artwork->order_id;
                    if($artwork->delete())
                    {
                        // Reorder artworks
                        if($artworks = \Order\Model_Artwork::find(array('where' => array('unique_id' => $unique_id, 'order_id' => $order_id, 'deleted_at' => 0), 'order_by' => array('type' => 'asc'))))
                        {
                            foreach ($artworks as $key => $artwork_item)
                            {
                                $artwork_item->type = $key + 1;
                                $artwork_item->save();
                            }
                        }
                    }
                }
                
                $current = $responseObject->getStatus();
                if (!empty($current))
                {
                    $out['data']['file_id'] = $file_id;
                }
                else
                {
                    $out['data']['file_id'] = $file_id;
                }
            }
        }
        
        if($return) return $out;
        echo json_encode($out);
    }
    
    public function file_info($file_id = null)
    {
        $obj = new \StorageAPIs(\Session::get('ysi.sHost'),\Session::get('ysi.sApp'), \Session::get('ysi.sTr'));
        $obj->setAuthToken(\Session::get('ysi.sToken'));
        $file = $obj->getFileInfo($file_id);
        

        $current = $file->getErrorStatus();
        if (!empty($current))
        {
            $out = array(
                'errorcode' => $current->getCode(),
                'errormessage' => $current->getMessage()
            );
        }
        else
        {
            $out['data']['revision'] = $file->getRevision();
            $out['data']['id'] = $file->getId();
            $out['data']['clickable_download_url'] = $file->getClickableDownloadUrl();
            $out['data']['created_on'] = $file->getCreatedOn();
            $out['data']['download_url'] = $file->getDownloadUrl();
            $out['data']['name'] = $file->getName();
            $out['data']['owned_by_storage'] = $file->getOwnedByStorage();
            $out['data']['parent_id'] = $file->getParentId();
            $out['data']['size'] = $file->getSize();
        }
        
        return $out;
    }
    
    public function downloada_file($download_url = null)
    { 
        $ft = new \FileTransfer(\Session::get('ysi.sHost'),\Session::get('ysi.sApp'), \Session::get('ysi.sUserAgent'));
        $ft->setAuthToken(\Session::get('ysi.sToken'));
        
        $response = $ft->downloadaFileWS(trim($download_url));
       
        if ($response !== 'fail'){
            header("Location: $response");
        }
        else {
            echo '<h3>There was an error while downloading file.</h3>';
        }
        exit;
    }
}