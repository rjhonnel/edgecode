<?php

/**
 * The Accesory Admin Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Accessory;

class Controller_Admin_Accessory extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/accessory/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('accessory::accessory', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * Index
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/accessory/list');
	}
    
    /**
	 * Update
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_update()
	{
        \View::set_global('title', 'Edit Accessory');
        
        $accessory = \Page\Model_Page::find();
        $accessory = reset($accessory);
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')
                ->set('accessory', $accessory, false);
	}
    
    public function action_assign_customer_groups($user_id = false)
	{
        \View::set_global('title', 'Discount Codes Assign Customer Groups');
        
        $search = $this->get_search_items($user_id);
        
		$items      = $search['items'];
        $pagination = $search['pagination'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'assign_customer_groups')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
    
    public function action_assign_products()
	{
        \View::set_global('title', 'Discount Codes Assign Products');
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'assign_products');
	}
    
    public function action_assign_product_groups()
	{
        \View::set_global('title', 'Discount Codes Assign Product Groups');
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'assign_product_groups');
	}
    
    public function action_assign_product_categories()
	{
        \View::set_global('title', 'Discount Codes Assign Product Categories');
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'assign_product_categories');
	}
    
    
    public function get_search_items($user_id = false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \SentryAdmin::user_exists((int)$user_id))
        {
            $user = \SentryAdmin::user((int)$user_id);
        }
        
        $items = \Order\Model_Order::find(function($query){
            
            if(isset($user))
                $query->where('user_id', $user->id);
            
            // $query->order_by('main_number', 'desc');
            $query->order_by('id', 'asc');
            
        });
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        foreach($items as $number => $item)
                        {
                            $full_name = $item->first_name . ' ' . $item->last_name;
                            if(stripos($item->company, $value) === false)
                            {
                                if(stripos($full_name, $value) === false) unset($items[$number]);
                            }
                        }
                        break;
                    case 'email':
                        foreach($items as $number => $item)
                        {
                            if(stripos($item->email, $value) === false) unset($items[$number]);
                        }
                        break;
                    case 'custom_order_status':
                        if(array_key_exists($value, \Config::get('details.status', array())))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->status != $value) unset($items[$number]);
                            }
                        }
                        break;
                    case 'order_total_from':
                        is_numeric($value) or $value = 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if($item_details['total_price'] < $value) unset($items[$number]);
                        }
                        break;
                    case 'order_total_to':
                        is_numeric($value) or $value = 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if($item_details['total_price'] > $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at > $date) unset($items[$number]);
                            }
                        }
                        break;
                        
                    case 'sch_from':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->sch_delivery < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'sch_to':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->sch_delivery > $date) unset($items[$number]);
                            }
                        }
                        break;
                        
                    case 'status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->status, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'invoice_status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->invoice_status, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'delivery_status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->delivery_status, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'user_group':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            
                            if(!\SentryAdmin::user_exists((int)$item->user_id) || !\SentryAdmin::user((int)$item->user_id)->in_group($value))
                            {
                                unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
        return array('items' => $items, 'pagination' => $pagination);
    }
    
	
	/**
	 * List pages
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_list($user_id = false)
	{
        \View::set_global('title', 'Discount Codes List');
        
        $search = $this->get_search_items($user_id);
        
		$items      = $search['items'];
        $pagination = $search['pagination'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}

	
}
