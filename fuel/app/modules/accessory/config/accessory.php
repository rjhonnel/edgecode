<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
     
    'bulk_actions' => array(
        '0' => 'Select Action',
        'export' => 'Export Accesories',
        'delete' => 'Delete',
        'active_enable' => 'Active',
        'active_disable' => 'Inactive',
        'assign_to_product_category' => 'Assign to Product Category',
        'move_to_product_group' => 'Move to Product Group',
        'assign_product_label' => 'Assign Product Label',
     ),
 
 );