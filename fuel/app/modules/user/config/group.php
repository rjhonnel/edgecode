<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
     
    'bulk_actions' => array(
        '0' => 'Select Action',
        'able_to_buy_enable' => 'Able to Buy - Enable Selected',
        'able_to_buy_disable' => 'Able to Buy - Disable Selected',
        'able_to_view_enable' => 'Able to View - Enable Selected',
        'able_to_view_disable' => 'Able to View - Disable Selected',
        'discount' => 'Apply User Group RRP Discount',
        'sale_discount' => 'Apply On-Sale Discount',
     )
 	
 );