<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
     
    'bulk_actions' => array(
        '0' => 'Select Action',
        'export' => 'Export Orders',
        'delete' => 'Delete',
        'change_order_status' => 'Change Order Status',
        'change_payment_status' => 'Change Payment Status',
        'change_shipping_status' => 'Change Shipping Status',
     )
 	
 );