<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
     
    /**
     * User titles
     */
    'titles'  => array(
        'Mr.' => 'Mr.',
        'Mrs.' => 'Mrs.',
        'Ms.' => 'Ms.',
        'Miss.' => 'Miss.',
    ),
 	
    'states' => array(
        'ACT' => 'Australian Capital Territory',
        'NT' => 'Northern Territory',
        'NSW' => 'New South Wales',
        'QLD' => 'Queensland',
        'SA' => 'South Australia',
        'TAS' => 'Tasmania',
        'VIC' => 'Victoria',
        'WA' => 'Western Australia',
    ),

    'status' => array(
        'in_progress'           => 'In Progress',
        'received'              => 'Received',
        'shipped'               => 'Shipped',
        'invoiced'              => 'Invoiced',
        'partially_paid'        => 'Partially Paid',
        'paid'                  => 'Paid',
        'cancelled'             => 'Canceled',
    ),
     
 );