<?php

namespace User;

class Controller_User extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/user/';
	
	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_index()
	{
		//Keep existing messages 
		\Messages::instance()->shutdown();
		\Response::redirect(\Uri::front_create('user/login'));
	}
	
	/**
	 * Activate user account
	 * 
	 * @param unknown_type $email
	 * @param unknown_type $hash
	 */
	public function action_activate($email = false, $hash = false)
	{
        $settings = \Config::load('autoresponder.db');

		if($email && $hash)
		{
			//Keep existing messages
			\Messages::instance()->shutdown();
			
			try
			{
				if($user = \Sentry::activate_user($email, $hash))
				{
                    $email_data = array(
                        'site_title' 		=> \Config::get('site_title'),
                        'customer_name' 	=> ucwords($user->get('metadata.first_name')),
                    );

                    // Send email to user
                    $email_sent = \App\Emailer::send_email_to_user(
                        $user->email, 
                        $email_data['customer_name'],
                        'Welcome to '.$settings['website'],//'Registration with ' . $email_data['site_title'], 
                        '_email/user_register', 
                        $email_data
                    );
                    
                    // Send email to admins
                    \App\Emailer::send_email_to_group(
                        'register',
                        'Welcome to '.$settings['website'],//'Registration with ' . $email_data['site_title'], 
                        '_email/user_register', 
                        $email_data
                    );

                    if($email_sent)
                    {
                        // \Messages::success('Accout successfully activated. Please login and start using your account.');
                        \Messages::success('Welcome! Your account is now activated. Please log in.');
                    }
                    else
                    {
                        \Messages::error('Error while sending email.');
                    }
				}
				else
				{
					//\Messages::error('<h4>There was an error while trying activate user</h4>');
					\Messages::error('Wrong activation code. Please check your email and try again.');
				}
			}
			catch (\Sentry\SentryUserException $e)
			{
				// show validation errors
				//\Messages::error('<h4>There was an error while trying activate user</h4>');
		    	$errors = $e->getMessage();
		    	\Messages::error($errors);
			}
		}
		
		\Response::redirect(\Uri::front_create('user/login'));
	}
	
	/**
	 * User login
	 * 
	 * @access public
	 * @return void
	 */
	public function action_login()
	{
		
        if($this->check_logged_type() == 'guest') \Sentry::logout();
		if (!(\Sentry::check() && !\Sentry::user()->is_admin()))
		{
			\View::set_global('title', 'Login');
            
			if(\Input::post('login'))
			{
				$val = \User\Controller_Validate::forge('login');
				


				if($val->run())
				{
					try
				    {
				   	 	if(\Sentry::user_exists(\Input::param('identity')) && !\Sentry::user(\Input::param('identity'))->is_admin())
						{
							// check the credentials.
							$valid_login = \Sentry::login(\Input::param('identity'), \Input::param('password'), true);
							
							if($valid_login)
							{
                                if($this->check_logged_type() == 'guest') 
                                {
                                    \Messages::info('You cannot log on to with a guest account.');
                                    \Response::redirect(\Uri::front_create('user/account/login'));
                                }

								\Messages::success('You have logged in successfully');
                                \Response::redirect(\Uri::front_create('order/cart/edit_cart'));
							}
							else
							{
								// \Messages::error('Email and/or password is incorrect');
								\Messages::error('The email or password you entered is incorrect');
							}
						}
						else
						{
							\Messages::error('The email or password you entered is incorrect');
							// \Messages::error('Email and/or password is incorrect');
						}
					    
				    }
					catch (\Sentry\SentryAuthException $e)
				    {
					    // show validation errors
						//\Messages::error('<h4>There was an error while trying to login</h4>');
				    	$errors = $e->getMessage();
				    	\Messages::error($errors);
				    }
				    catch (\Sentry\SentryUserException $e)
				    {
					    // show validation errors
						//\Messages::error('<h4>There was an error while trying to login</h4>');
				    	$errors = $e->getMessage();
				    	\Messages::error($errors);
				    }
				}
				else
				{
					if($val->error() != array())
					{
						// show validation errors
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}
			else if(\Input::post('signup'))
			{
				$this->action_signup();
			}
			
			if(\Input::is_ajax())
			{
				echo \Theme::instance()->view($this->view_dir . 'login');
			}
			else
			{
				\Theme::instance()->set_partial('content', $this->view_dir . 'single_login');
			}
		}
		else
		{
			//Keep existing messages
			\Messages::instance()->shutdown();
			\Response::redirect(\Uri::front_create('/'));
		}
	}
	
	/**
	 * The module index
	 *
	 * @return  Response
	 */
	public function action_logout()
	{
		\Sentry::logout();
		// \Messages::success('You have successfully logged out');
		\Response::redirect(\Uri::front_create('user/login'));
	}
	
	/**
	 * Create user
	 * 
	 * @access public
	 * @return void
	 */
	public function action_signup()
	{
        $settings = \Config::load('autoresponder.db');

		//if (!(\Sentry::check() && !\Sentry::user()->is_admin()))
        if ($this->check_logged_type() != 'user')
		{
            if (\Sentry::check()) \Sentry::logout();
                
			\View::set_global('title', 'Registration');
			
			if(\Input::post('signup'))
			{
				$val = \User\Controller_Validate::forge('create');
                // $val->set_message('unique', 'It appears you have already registered using this email address.');


				if($val->run())
				{
					// Get POST values
					$insert = \Input::post();
                    array_walk($insert, create_function('&$val', '$val = trim($val);'));
                    
				    try
				    {
                        $username 	= $insert['email'];
                        $email 		= $insert['email'];
                        $password 	= $insert['password'];
                        $user_group	= $insert['user_group'];
                        unset($insert['signup'], $insert['user_group'], $insert['username'], $insert['email'], $insert['password'], $insert['confirm_password'], $insert['confirm_email'], $insert['terms']);

                        // create the user - no activation required
                        $vars = array(
                            'username' 	=> $username,
                            'email' 	=> $email,
                            'password' 	=> $password,
                            'metadata' 	=> $insert,
                        );

                        $user_registration = \Sentry::user()->register($vars);

                        $user = \Sentry::user($user_registration['id']);

                        // Add user to 'customer' group (id = 3)
                        if($user_registration and $user->add_to_group($user_group))
                        {
                            if(true)
                            {
                                $email_data = array(
                                    'site_title' 		=> \Config::get('site_title'),
                                    'customer_name' 	=> ucwords($insert['first_name']),
                                    'phone'				=> $insert['phone'],
                                    'activation_link'   => \Uri::front_create('user/activate/'.$user_registration['hash']),
                                );
                                
                                // Send email to user
                                $email_sent = \App\Emailer::send_email_to_user(
                                    $vars['email'], 
                                    $email_data['customer_name'],
                                    'Activate your account with '.$settings['website'], //'Registration with ' . $email_data['site_title'], 
                                    '_email/user_register_confirm', 
                                    $email_data
                                );
                                
                                // Send email to admins
                                \App\Emailer::send_email_to_group(
                                    'register',
                                    'Activate your account with '.$settings['website'], //'Registration with ' . $email_data['site_title'], 
                                    '_email/user_register_confirm', 
                                    $email_data
                                );
                                
                                if($email_sent)
                                {
                                    // \Messages::success('Welcome ' . $email_data['customer_name'] . ' to ' . $email_data['site_title'] . ', thank you for registering with us. An email has been sent to ' . $vars['email'] . ' with your account information.');
                                     \Messages::success('Thanks for registering. We have sent an email to your nominated address with a link to activate your account. Sometimes inboxes can be a little overprotective so you may need to check your junk or spam folders.');
                                }
//                                else
//                                {
//                                    \Messages::error('An error occurred while sending email, please try again later.');
//                                }
                                
                            }

                            \Response::redirect(\Input::referrer(\Uri::front_create('user/login')));
                        }
				    }
				    catch(\Sentry\SentryUserException $e)
				    {
				    	// show validation errors
						//\Messages::error('<h4>There was an error while trying to create user</h4>');
				    	$errors = $e->getMessage();
				    	\Messages::error($errors);
				    }
				}
				else
				{
					if($val->error() != array())
					{
						// show validation errors
						//\Messages::error('<h4>There was an error while trying to create user</h4>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}
			
			if(\Input::is_ajax())
			{
                // We need to save an user inputs in database for advertising use
                if(\Input::post())
                {
                    // Get POST values
                    $insert = \Input::post();
                    array_walk($insert, create_function('&$val', '$val = trim($val);'));

                    if($insert['first_name'] || $insert['last_name'] || $insert['email'])
                    {
                        $insert['created_at'] = time();
                        
                        \DB::insert('users_tmp')->set($insert)->execute();
                    }
                }
                
				echo \Theme::instance()->view($this->view_dir . 'signup');
			}
			else
			{
				\Theme::instance()->set_partial('content', $this->view_dir . 'single_signup');
			}
		}
		else
		{
			//Keep existing messages
			\Messages::instance()->shutdown();
			\Response::redirect(\Uri::front_create('user/account/dashboard'));
		}
	}
	
	/**
	 * Change user password
	 * 
	 * @access public
	 * @return void
	 */
	public function action_password()
	{
		\View::set_global('title', 'Forgot Password');
		
		if(\Input::post('forgot'))
		{
			$val = \User\Controller_Validate::forge('forgot_password');
			
			if($val->run())
			{
				// Get POST values
				$identity 	= \Input::post('identity', '');
				
				if(\Sentry::user_exists($identity))
				{
				    try
				    {
					    // reset the password
						$reset = \Sentry::reset_password($identity);
						if ($reset)
						{
							$customer_email = $reset['email'];
							
							// Load email package
							\Package::load('email');
							// Load email addresses from config (these will be bcc receivers)
							\Config::load('auto_response_emails', true);
								
							$bcc = \Config::get('autoresponders.forgot_password_emails');
							if( ! $bcc) $bcc= \Config::get('autoresponders.default_emails');
							
							$settings = \Config::load('autoresponder.db');
							$email_data = array(
								'site_title' 			=> $settings['company_name'],
								'customer_identity' 	=> $identity,
								'reset_link'			=> \Uri::front_create('user/reset_password/'.$reset['link'])
							);
							
							$email = \Email::forge();
							$email->to($customer_email);
                            $email->from(\Config::get('auto_response_emails.autoresponder_from_email'), $settings['company_name']);
							
							if($bcc) $email->bcc($bcc);
							
							$email->subject($email_data['site_title'] . ' - Forgot Password');
							
							$email_html = \Theme::instance()->view('views/_email/forgot_password' )
								->set('email_data', $email_data, false);
							
							$email->html_body($email_html);
							
							try
							{
								$email->send();
								\Messages::success('You have been sent an email to reset your password.');
							}
							catch(\EmailValidationFailedException $e)
							{
								\Messages::error('Error while sending email.');
							}
							catch(\EmailSendingFailedException $e)
							{
								\Messages::error('Error while sending email.');
							}
							
							\Response::redirect(\Input::referrer(\Uri::front_create('/')));
						}
						else
						{
							\Messages::error('There was a problem while trying to change your password. Please try again.');
						}
				    }
				    catch (\Sentry\SentryUserException $e)
				    {
				    	// show validation errors
						//\Messages::error('<h4>There was an error while trying to create user</h4>');
				    	$errors = $e->getMessage();
				    	\Messages::error($errors);
				    }
				}
				else
				{
					\Messages::error('There doesn`t appear to be an account associated with this email address. Try a different email address or register for a new account on the homepage.');
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					//\Messages::error('<h4>There was an error while trying to create user</h4>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		if(\Input::is_ajax())
		{
			echo \Theme::instance()->view($this->view_dir . 'forgot_password');
		}
		else
		{
            if(isset($val)) \View::set_global('validation', $val, false);
			\Theme::instance()->set_partial('content', $this->view_dir . 'single_forgot_password');
		}
	}
	
	/**
	 * Change user password
	 * 
	 * @param unknown_type $email
	 * @param unknown_type $hash
	 */
	public function action_reset_password($email = false, $hash = false)
	{
		if($email && $hash)
		{
			//Keep existing messages
			\Messages::instance()->shutdown();
			
			try
			{
				if(\Sentry::reset_password_confirm($email, $hash))
				{	
					if(\Input::post('new_password') && \Input::post('confirm_new_password'))
					{
						if(\Sentry::reset_password_save($email, \Input::post('new_password')))
						{
							\Messages::success('Password successfully changed. Please login and start using your account.');
							\Response::redirect(\Uri::front_create('user/login'));
						}
						else
						{
							\Messages::error('Password was not save.');	
							\Theme::instance()->set_partial('content', $this->view_dir . 'reset_password');
						}
					}
					else
					{
						\Theme::instance()->set_partial('content', $this->view_dir . 'reset_password');
					}
				}
				else
				{
					\Messages::error('Wrong reset code. Please check your email and try again.');		
					\Response::redirect(\Uri::front_create('user/login'));
				}
			}
			catch (\Sentry\SentryUserException $e)
			{
				// show validation errors
				//\Messages::error('<h4>There was an error while trying activate user</h4>');
		    	$errors = $e->getMessage();
		    	\Messages::error($errors);
			}
		}
	}

	
}