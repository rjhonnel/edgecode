<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace User;

class Controller_Admin_User extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/user/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('user::user', 'details');
        \Config::load('order::order', 'order');
        \Config::load('sentry', true);
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/user/list');
	}
	
	public function attributes_to_string($attributes = false)
    {
        $out = false;
        if(json_decode($attributes, true) != null)
        {
            $attributes = json_decode($attributes, true);
        }
        if(is_array($attributes))
        {
            foreach($attributes as $key => $value)
            {
                $out[] = $key . ': ' . $value;
            }
        }
        if($out) $out = implode(' | ', $out); 
        return $out;
    }

    public function get_search_items($group_id = false)
    {
        // Override group_id if its a search
        $group_id = \Input::get('user_group', $group_id);

        $activated = \Input::get('activated', false);


        
        if($group_id && \SentryAdmin::group_exists((int)$group_id))
        {
            // Get only group users
            \View::set_global('group', \SentryAdmin::group((int)$group_id));
            $items = \SentryAdmin::group((int)$group_id)->users();
        }
        else
        {
            // Get all users and remove admin users from array
            $items = \SentryAdmin::user()->all('front');
        }
        
        // Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
        
        // Get user objects
        if(!empty($items))
        {
            foreach($items as $key=> $item)
            {
                $items[$key] = \SentryAdmin::user((int)$item['id']);
            }
            
            // Get search filters
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title': 
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                $full_name = $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name');
                                $customer_id = $item->get('metadata.user_id');
                                $company = isset($item->metadata['company_name'])?$item->metadata['company_name'] : $item->get('metadata.business_name');

                                if(stripos($full_name, $value) === false && stripos($customer_id, $value) === false && stripos($company, $value) === false ) unset($items[$number]);
                            }
                            break;
                        case 'email':
                            foreach($items as $number => $item)
                            {
                                if(stripos($item->email, $value) === false) unset($items[$number]);
                            }
                            break;
                        case 'country':
                            if($value && $value !== 'false')
                            {
                                foreach($items as $number => $item)
                                {
                                    if(empty($item['metadata'])) 
                                    {
                                        unset($items[$number]);
                                        continue;
                                    }
                                    if(stripos($item->get('metadata.country'), $value) === false) unset($items[$number]);
                                }
                            }
                            break;
                        case 'postcode_from':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') < $value) unset($items[$number]);
                            }
                            break;
                        case 'postcode_to':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') > $value) unset($items[$number]);
                            }
                            break;
                        case 'activated':
                        	if($value !== 'false')
                            {
	                        	foreach($items as $number => $item)
	                            {
	                                if($item->activated != $value) unset($items[$number]);
	                            }
                        	}
                        	break;
                    }
                }
            }
        }

        // Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
        return array('items' => $items, 'pagination' => $pagination);
    }
    
    /**
     * List all front-end users
     * 
     * @param $group_id    = Filter users by group ID
     */
	public function action_list($group_id = false)
	{
        \View::set_global('menu', 'admin/user/list');
		\View::set_global('title', 'List Customers');
		
        $search = $this->get_search_items($group_id);
		
        $items      = $search['items'];
        $pagination = $search['pagination'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
    
    /**
     * List all front-end users
     * 
     * @param $group_id    = Filter users by group ID
     */
	public function action_wishlist($id = false)
	{
        if(!is_numeric($id)) \Response::redirect('admin/user/list');
		
		// Get user to edit
		if(!\SentryAdmin::user_exists((int)$id)) \Response::redirect('admin/user/list');
        
        $user = \SentryAdmin::user((int)$id);
        
		\View::set_global('title', 'Wish List');
		
        $search = $this->get_search_items2($id);
		
        $items      = $search['items'];
        $pagination = $search['pagination'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'wishlist/list')
			->set('items', $items)
			->set('pagination', $pagination, false)
            ->set('user', $user, false);
	}
    
    public function action_orderhistory($id = false)
	{

        \View::set_global('menu', 'admin/user/list');
		\View::set_global('title', 'Order History');

		if(!\SentryAdmin::user_exists((int)$id) || !is_numeric($id)) \Response::redirect('admin/user/list');

		$user = \SentryAdmin::user((int)$id);

		$search = $this->get_search_items2($id);
		$this_class = $this;
     
        $get_attributes = function($attributes) use ($this_class)
        {
            return $this_class->attributes_to_string($attributes);
        };
		$items      = $search['items'];
		$pagination = $search['pagination'];

		\Theme::instance()->set_partial('content', $this->view_dir . 'customer_orders_history')
			->set('items', $items)
			->set('pagination', $pagination, false)
			->set('user', $user, false)
			->set('get_attributes', $get_attributes, false);
	}

	public function action_import()
	{
        \View::set_global('menu', 'admin/user/list');
		\View::set_global('title', 'Bulk Add Customers');
		
		if(\Input::file())
		{
			if(\Input::file('file')['tmp_name'])
			{
	    		$handle = fopen(\Input::file('file')['tmp_name'], 'r');
				while( ( $data = fgetcsv($handle, 1000, ",") ) !== FALSE  )
				{
					if($data[0] == 'email(header)')
						continue;

					$insert['email'] = $data[0];
					if (!filter_var($insert['email'], FILTER_VALIDATE_EMAIL)) continue;

					$insert['business_name'] = $data[1];
					$insert['user_group'] = $data[2];
					$insert['first_name'] = $data[3];
					$insert['last_name'] = $data[4];
					$insert['address'] = $data[5];
					$insert['suburb'] = $data[6];
					$insert['country'] = $data[7];
					$insert['state'] = $data[8];
					$insert['postcode'] = $data[9];
					$insert['phone'] = $data[10];

	                $username 	  = $insert['email'];
			    	$email 		  = $insert['email'];
			    	$password 	  = $data[11];//'random' . \Str::random(unique);
			    	$user_group	  = $insert['user_group'];
			    	unset($insert['email'], $insert['password'], $insert['user_group']);

				    $vars = array(
					    'username' 	=> $username,
					    'email' 	=> $email,
					    'password' 	=> $password,
					    'metadata' 	=> $insert,
	                    'activated' => 1,
				    );
				    try
				    {
				    	// check if username is existing
				    	if(\Sentry::user_exists($username)) continue;

				    	// check if group is existing
				    	if(!\Sentry::group_exists($user_group)) continue;

					    $user_id 	= \Sentry::user()->create($vars);
					    if($user_id)
					    {
						    $user 		= \Sentry::user($user_id);
						    $user->add_to_group($user_group);
					    }
					}
					catch (\Database_Exception $e){ continue; }
					catch (\Exception $e){ continue; }
				}
				\Messages::success('Customers was successfully imported.');
			}
    		else
    		{
	    		\Messages::error('Select file to upload.');
    		}
		}

		 \Theme::instance()->set_partial('content', $this->view_dir . 'import');
	}
	
	public function action_create()
	{
        \View::set_global('menu', 'admin/user/list');
		\View::set_global('title', 'Add New Customer');
		
        // Get groups
        $groups = \SentryAdmin::group()->all('front');
        $companies = \Company\Model_Company::find();
        
		if(\Input::post())
		{
			$val = \User\Controller_Admin_Validate::forge('create');
			
			if($val->run())
			{
                // Get POST values
				$insert = \Input::post();
				unset($insert['business_name2']);
				unset($insert['shipping_business_name2']);
				
				if(!\Input::post('on_account')){
					$insert['company'] = '';
					$insert['on_account'] = 0;
					$insert['business_name'] = \Input::post('business_name2');
				}
				else{
					$insert['on_account'] = 1;
				}

				if(!\Input::post('on_shipp_account')){
					$insert['shipping_company'] = '';
					$insert['on_shipp_account'] = 0;
					$insert['shipping_business_name'] = \Input::post('shipping_business_name2');
				}
				else{
					$insert['on_shipp_account'] = 1;
				}

				array_walk($insert, create_function('&$val', '$val = trim($val);'));
				
			    try
			    {
                    // Generate random username
			    	//$username 	= 'user' . \Str::random('numeric', 16);
                    $username 	  = $insert['email'];
			    	$email 		  = $insert['email'];
			    	$password 	  = $insert['password'];
			    	$user_group	  = $insert['user_group'];
                    $activated	  = $insert['activated'];
                    $email_client = $insert['email_client'];
                    $insert['guest'] = $user_group == 3 ? 1: 0;
			    	unset($insert['email'], $insert['password'], 
                            $insert['confirm_password'], $insert['user_group'], 
                            $insert['details'], $insert['save'], 
                            $insert['update'], $insert['activated'],
                            $insert['email_client']);
			    	
                    $only_billing = array('business_name', 'purchase_limit_value' , 'purchase_limit_period', 'master', 'note', 'credit_account', 'guest');
                        
                    // Set shipping data to be same as billing by default
                    /*foreach($insert as $key => $value)
                    {
                        if(!in_array($key, $only_billing) && strpos($key, 'shipping_') === false)
                        {
                            if(empty($insert['shipping_'.$key]))
                                $insert['shipping_'.$key] = $value;
                        }
                    }*/
                    
				    // create the user - no activation required
				    $vars = array(
					    'username' 	=> $username,
					    'email' 	=> $email,
					    'password' 	=> $password,
					    'metadata' 	=> $insert,
                        'activated' => $activated,
				    );

				    $user_id 	= \SentryAdmin::user()->create($vars);
				    $user 		= \SentryAdmin::user($user_id);
                    
                    // Send email to user with new password
                    if($email_client == 1 && !empty($vars['password']))
                    {
                        $email_data = array(
                            'site_title' 			=> \Config::get('site_title'),
                            'customer_identity'     => ucwords($user->get('metadata.first_name').' '.$user->get('metadata.last_name')),
                            'new_password'			=> $vars['password']
                        );
                        $this->autoresponder($user, $email_data);
                    }
				    
				    // Add user to 'customer' group (id = 3)
				    if($user_id and $user->add_to_group($user_group))
				    {
				    	\Messages::success('User successfully created.');
                        \Response::redirect(\Input::post('update', false) ? \Uri::create('admin/user/update/' . $user->id) : \Uri::admin('current'));
				    }
				    else
				    {
				    	// show validation errors
						\Messages::error('<strong>' . 'There was an error while trying to create user' . '</strong>');
				    }
			    }
			    catch (\Sentry\SentryUserException $e)
			    {
			    	// show validation errors
					\Messages::error('<strong>' . 'There was an error while trying to create user' . '</strong>');
			    	$errors = $e->getMessage();
			    	\Messages::error($errors);
			    }
            }
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>' . 'There was an error while trying to create user' . '</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
        
        \Theme::instance()->set_partial('content', $this->view_dir . 'create')
            ->set('groups', $groups)
            ->set('companies', $companies);
	}
	
	public function action_update($id = false)
	{
        
		if(!is_numeric($id)) \Response::redirect('admin/user/list');
		
		// Get user to edit
		if(!\SentryAdmin::user_exists((int)$id)) \Response::redirect('admin/user/list');
        \View::set_global('menu', 'admin/user/list');
		\View::set_global('title', 'Edit Customer');
		
        // Get groups
        $groups = \SentryAdmin::group()->all('front');
        $companies = \Company\Model_Company::find();
       
		// Update group details
		if(\Input::post('details', false))
		{
            $item = new \Sentry_User((int)$id);
			$val = \User\Controller_Admin_Validate::forge('update', $item['id']);
			
			if($val->run())
			{
                // Get POST values
				$insert = \Input::post();
				unset($insert['business_name2']);
				unset($insert['shipping_business_name2']);
				if(!\Input::post('on_account')){
					$insert['company'] = '';
					$insert['on_account'] = 0;
					$insert['business_name'] = \Input::post('business_name2');
				}
				else{
					$insert['on_account'] = 1;
				}

				if(!\Input::post('on_shipp_account')){
					$insert['shipping_company'] = '';
					$insert['on_shipp_account'] = 0;
					$insert['shipping_business_name'] = \Input::post('shipping_business_name2');
				}
				else{
					$insert['on_shipp_account'] = 1;
				}

				array_walk($insert, create_function('&$val', '$val = trim($val);'));
				
			    try
			    {
                    // Generate random username
			    	//$username 	= 'user' . \Str::random('numeric', 16);
                    $username 	  = $insert['email'];
			    	$email 		  = $insert['email'];
			    	$password 	  = $insert['password'];
			    	$user_group	  = $insert['user_group'];
                    $activated	  = $insert['activated'];
                    $email_client = $insert['email_client'];
                    $insert['guest'] = $user_group == 3 ? 1: 0;
			    	unset($insert['email'], $insert['password'], 
                            $insert['confirm_password'], $insert['user_group'], 
                            $insert['details'], $insert['save'],  
                            $insert['exit'], $insert['activated'],
                            $insert['email_client']);
			    	
                    $only_billing = array('business_name', 'purchase_limit_value' , 'purchase_limit_period', 'master', 'note', 'credit_account', 'guest');
                        
                    // Set shipping data to be same as billing by default
                    /*foreach($insert as $key => $value)
                    {
                        if(!in_array($key, $only_billing) && strpos($key, 'shipping_') === false)
                        {
                            if(empty($insert['shipping_'.$key]))
                                $insert['shipping_'.$key] = $value;
                        }
                    }*/
                    
				    // create the user - no activation required
				    $vars = array(
					    'username' 	=> $username,
					    'email' 	=> $email,
					    'password' 	=> $password,
					    'metadata' 	=> $insert,
                        'activated' => $activated,
				    );
                    
                    // Send email to user with new password
                    if($email_client == 1 && !empty($vars['password']))
                    {
                        $email_data = array(
                            'site_title' 			=> \Config::get('site_title'),
                            'customer_identity'     => ucwords($item->get('metadata.first_name').' '.$item->get('metadata.last_name')),
                            'new_password'			=> $vars['password']
                        );
                        $this->autoresponder($item, $email_data);
                    }
                    
                    if(empty($vars['password'])) unset($vars['password']);
					
				    if($item->update($vars))
				    {
                        //Change user group if needed
                        $user_groups = $item->groups();
                        if(!empty($user_groups))
                        {
                            // Remove user from all other groups...
                            foreach($user_groups as $value)
                            {
                                $item->remove_from_group((int)$value['id']);
                            }
                        }
                        
                        $item = new \Sentry_User((int)$id);
                        // ...and add it to selected one
                        $item->add_to_group((int)$user_group);
                        
				    	\Messages::success('User successfully updated.');
                        \Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/user/list/') : \Uri::admin('current'));
				    }
				    else
				    {
				    	// show validation errors
						\Messages::error('<strong>' . 'There was an error while trying to update user' . '</strong>');
				    }
			    }
			    catch (\Sentry\SentryUserException $e)
			    {
			    	// show validation errors
					\Messages::error('<strong>' . 'There was an error while trying to update user' . '</strong>');
			    	$errors = $e->getMessage();
			    	\Messages::error($errors);
			    }
            }
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>' . 'There was an error while trying to update user' . '</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		$user = new \Sentry_User((int)$id);
		
        // Get single user group
        $user_group = $user->groups();
        $user_group = current($user_group);
        
        $user->group = $user_group;
        $user_company = \Company\Model_Company::find_one_by_id((int)$user->metadata['company']);
        if($user_company)
        	$user->company_email = $user_company->company_email;
     
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')
                ->set('user', $user)
                ->set('companies', $companies)
                ->set('groups', $groups);
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get user delete
			if($item = \SentryAdmin::user((int)$id))
			{
				// Delete item
				try{
					// Delete group
					$item->delete();
					
					\Messages::success('User successfully deleted.');
				}
				catch (\Sentry\SentryUserException $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete user</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
    /**
     * Validate Model fields
     * 
     * @param $factory = Validation name, if you want to have multiple validations
     */
    public static function validate($factory)
    {
       $val = \Validation::forge($factory);
       $val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);

       return $val;
    }
    
    public function get_search_items2($user_id = false)
    {
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \SentryAdmin::user_exists((int)$user_id))
        {
            $user = \SentryAdmin::user((int)$user_id);
            $_SESSION['get_search_items2_user'] = $user;
        }
        
        $items = \Order\Model_Order::find(function($query){
            $user = $_SESSION['get_search_items2_user'];

            if(isset($user))
                $query->where('user_id', $user->id);
            	
            $query->where('finished', '1');

            /* Commented by Henry - 
             * Error: Column not found: 1054 Unknown column 'main_number' in 'order clause' with query: "SELECT * FROM `orders` ORDER BY `main_number` DESC, `id` ASC"
             */
            // $query->order_by('main_number', 'desc');
            $query->order_by('id', 'desc');
            
            unset($_SESSION['get_search_items2_user']);
        });
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        foreach($items as $number => $item)
                        {
                            $full_name = $item->first_name . ' ' . $item->last_name;
                            if(stripos($item->company, $value) === false)
                            {
                                if(stripos($full_name, $value) === false) unset($items[$number]);
                            }
                        }
                        break;
                    case 'email':
                        foreach($items as $number => $item)
                        {
                            if(stripos($item->email, $value) === false) unset($items[$number]);
                        }
                        break;
                    case 'custom_order_status':
                        if(array_key_exists($value, \Config::get('details.status', array())))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->status != $value) unset($items[$number]);
                            }
                        }
                        break;
                    case 'order_total_from':
                        is_numeric($value) or $value = 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if($item_details['total_price'] < $value) unset($items[$number]);
                        }
                        break;
                    case 'order_total_to':
                        is_numeric($value) or $value = 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if($item_details['total_price'] > $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at > $date) unset($items[$number]);
                            }
                        }
                        break;
                        
                    case 'sch_from':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->sch_delivery < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'sch_to':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->sch_delivery > $date) unset($items[$number]);
                            }
                        }
                        break;
                        
                    case 'status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->status, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'invoice_status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->invoice_status, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'delivery_status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->delivery_status, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'user_group':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            
                            if(!\SentryAdmin::user_exists((int)$item->user_id) || !\SentryAdmin::user((int)$item->user_id)->in_group($value))
                            {
                                unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
        return array('items' => $items, 'pagination' => $pagination);
    }
    
    protected function autoresponder($user, $data)
    {
        \Theme::instance()->active('frontend');
        \View::set_global('theme', \Theme::instance(), false);
        
        // Send autoresponder
        $autoresponder = \Autoresponder\Autoresponder::forge();

        $autoresponder->user_id =  $user->id;

        $autoresponder->view_user = 'new_password';

        $content['subject'] = 'New Password';
        $content['content'] = $data;
        $autoresponder->autoresponder_user($content);


        if($autoresponder->send())
        {
            \Messages::success('The mail has been sent successfully.');
        }
        else
        {
            \Messages::error('<strong>' . 'There was an error while trying to sent email.' . '</strong>');
        }
        
        \Theme::instance()->active('admin');
    }
                 


	public function action_referrals($id = false)
	{
        \View::set_global('menu', 'admin/user/list');
		\View::set_global('title', 'Referrals');


		$user = new \Sentry_User((int)$id);

		$items = Model_Referal::find(function($query) use ($id){

			$query->where('user_added', $id);
			$query->order_by('id', 'desc');

		});

		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);

		\Theme::instance()->set_partial('content', $this->view_dir . 'referrals')
						->set('user', $user)
						->set('pagination', $pagination, false)
						->set('items', $items);
	}
	
	public function action_delete_referral($id = false)
	{
		if(is_numeric($id))
		{
			// Get user delete
			if($item = Model_Referal::find_one_by_id((int)$id))
			{
				// Delete item
				try{
					// Delete group
					$item->delete();
					
					\Messages::success('User successfully deleted.');
				}
				catch (\Sentry\SentryUserException $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete item</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}       

	public function action_notes($id = false) {
        \View::set_global('menu', 'admin/user/list');
		\View::set_global('title', 'Notes');

		if(!\SentryAdmin::user_exists((int)$id) || !is_numeric($id)) \Response::redirect('admin/user/list');

		$user = \SentryAdmin::user((int)$id);

		$notes = Model_Notes::find(function($query) use ($id){

      		$query->where('user_id', $id);
            $query->order_by('created_at', 'desc');
        });

        // Reset to empty array if there are no result found by query
		if(is_null($notes)) $notes_list = array();
		else $notes_list = $notes;
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($notes),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$notes_list = array_slice($notes_list, $pagination->offset, $pagination->per_page);

		\Theme::instance()->set_partial('content', $this->view_dir . 'notes/notes')
			->set('notes', $notes_list)
			->set('pagination', $pagination, false)
			->set('user', $user);
	}

	public function action_create_notes($id = false) {

		if(!\SentryAdmin::user_exists((int)$id) || !is_numeric($id)) \Response::redirect('admin/user/list');

		$val = Model_Notes::validate('create');
		if($val->run())
		{
			$insert = \Input::post();
			$insert['user_id'] = $id;
			$item = Model_Notes::forge($insert);
			try
			{
				$item->save();
				\Messages::success('Note successfully added.');
			}
			catch (\Database_Exception $e)
			{
				// show validation errors
				\Messages::error('<strong>There was an error while trying to create note</strong>');
			    
				// Uncomment lines below to show database errors
				$errors = $e->getMessage();
		    	\Messages::error($errors);
			}
		}
		else
		{
			if($val->error() != array())
			{
				// show validation errors
				\Messages::error('<strong>There was an error while trying to create note</strong>');
				foreach($val->error() as $e)
				{
					\Messages::error($e->get_message());
				}
			}
		}

		\Response::redirect(\Uri::create('admin/user/notes/') . $id);
	}

	public function action_delete_notes($id = false, $note_id = false) {

		if(!\SentryAdmin::user_exists((int)$id) || !is_numeric($id)) \Response::redirect('admin/user/list');

		if($item = Model_Notes::find_one_by_id($note_id))
		{
			try
			{
				$item->delete();
				\Messages::success('Note successfully deleted.');
			}
			catch (\Database_Exception $e)
			{
				\Messages::error('<strong>There was an error while trying to delete note</strong>');
			}
		}

		\Response::redirect(\Uri::create('admin/user/notes/') . $id);
	}

	public function action_update_notes($id = false, $note_id = false) {

		if(!\SentryAdmin::user_exists((int)$id) || !is_numeric($id)) \Response::redirect('admin/user/list');

		if($item = Model_Notes::find_one_by_id($note_id))
		{
			$insert = \Input::post();
			try
			{
				$item->set($insert);
				$item->save();

				echo 'Note successfully updated.';
			}
			catch (\Database_Exception $e)
			{
				echo '<strong>There was an error while trying to update note</strong>';
			}
		}

		exit;
	}

	public function action_login($id){
	
		 $user = $user = new \Sentry_User((int)$id);

		 if($user['metadata']['guest'] == 1){
		 		\Messages::info('You cannot log on to with a guest account.');
		 		\Response::redirect(\Uri::front_create('admin/user/list'));
		 }else{

		 	 $valid_login = \Sentry::login($user->email, $user->password, true, true);	
		 	 
		 	 if($valid_login){
			 	\Messages::success('You have logged in successfully');
				\Response::redirect(\Uri::front_create('user/account/dashboard'));
			 }
			 else{
			 	\Messages::error('Customer does not exist!');
			 	\Response::redirect(\Uri::front_create('admin/user/list'));
			 }
		 }
		
	}
}
                            
                            