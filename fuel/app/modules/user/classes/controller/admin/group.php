<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace User;

class Controller_Admin_Group extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/user/group/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('user::group', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/user/group/list');
	}
	
	public function action_list()
	{
        \View::set_global('menu', 'admin/user/group/list');
		\View::set_global('title', 'Customer groups');
		
        // Get all groups and remove admin groups from array
        $items = \SentryAdmin::group()->all('front');
		
        // Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
	
	public function action_create()
	{
		\View::set_global('title', 'Add New Group');
		
		if(\Input::post())
		{
			$val = $this->validate('create');
			
			if($val->run())
			{
                try
                {
                    $group_id = \SentryAdmin::group()->create(array(
                        'name'  => \Input::post('title'),
                        'level' => 80,
                    ));
                    
					\Messages::success('Group successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/user/group/update/' . $item->id) : \Uri::admin('current'));
                }
                catch (\Sentry\SentryGroupException $e)
                {
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create group</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
                }
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create group</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/user/group/list');
	}
	
	public function action_update($id = false)
	{
        \View::set_global('menu', 'admin/user/group/list');
		if(!is_numeric($id)) \Response::redirect('admin/user/group/list');
		
		// Get group to edit
		if(!$item = \SentryAdmin::group((int)$id)) \Response::redirect('admin/user/group/list');
		
		\View::set_global('title', 'Edit Group');
		
		// Update group details
		if(\Input::post('details', false))
		{
			$val = $this->validate('update');
			
			if($val->run())
			{
				try{
					$update = $item->update(array(
                        'name'  => \Input::post('title'),
                    ));

                    if ($update)
                    {
                        // Update discounts
                        foreach(\Input::post('action') as $product_group_id => $value)
                        {
                            /**
                             * OPTIONS
                             */
                            $options[$product_group_id]['user_group_id'] = $item->id;
                            $options[$product_group_id]['product_group_id'] = $product_group_id;
                            //$options[$product_group_id]['action'] = $value;
                            $options[$product_group_id]['able_to_buy'] = \Input::post('able_to_buy.'.$product_group_id);
                            $options[$product_group_id]['able_to_view'] = \Input::post('able_to_view.'.$product_group_id);
                            $options[$product_group_id]['sale_discount'] = \Input::post('sale_discount.'.$product_group_id);
                            $options[$product_group_id]['apply_tier_to_sale'] = \Input::post('apply_tier_to_sale.'.$product_group_id);
                            
                            // Insert or update option
                            $option = \Product\Model_Group_Options::find_by(array('user_group_id' => $item->id, 'product_group_id' => $product_group_id), null, null, 1);
                            if($option)
                            {
                                $option = $option[0];
                                $option->set($options[$product_group_id]);
                            }
                            else
                            {
                                $option = \Product\Model_Group_Options::forge($options[$product_group_id]);
                            }
                            
                            $option->save();
                            
                            /**
                             * DISCOUNTS
                             */
                            
                            // Delete old discounts
                            $all_discounts_id = array();
                            $all_discounts = \Product\Model_Group_Discounts::find_by(array('user_group_id' => $item->id, 'product_group_id' => $product_group_id), null, null, null);
                            //if($all_discounts) foreach($all_discounts as $discount) $discount->delete();
                            if($all_discounts) 
                            {
                                foreach($all_discounts as $discount)
                                {
                                    $all_discounts_id[$discount->id] = $discount;
                                }
                            }
                            
                            // Update
                            $discounts = array();
                            foreach(\Input::post('qty.'.$product_group_id, array()) as $key => $value)
                            {
                                // Ignore discounts with same qty. Only one discount per QTY number is allowed
                                // We will insert first QTY in list. All other will be ignired
                                if(!isset($discounts[$product_group_id][$value]))
                                {
                                    if(isset($all_discounts_id[$key])) unset($all_discounts_id[$key]);
                                    $discounts[$product_group_id][$value]['id'] = $key;
                                    $discounts[$product_group_id][$value]['user_group_id'] = $item->id;
                                    $discounts[$product_group_id][$value]['product_group_id'] = $product_group_id;
                                    $discounts[$product_group_id][$value]['qty'] = $value;
                                    $discounts[$product_group_id][$value]['discount'] = \Input::post('discount.'.$product_group_id.'.'.$key);
                                }
                            }
                            
                            // Delete
                            if($all_discounts_id) foreach ($all_discounts_id as $discount) $discount->delete();
                            
                            if(!empty($discounts))
                            {
                                foreach($discounts[$product_group_id] as $key => $value)
                                {
                                    $id = $discounts[$product_group_id][$key]['id'];
                                    $discount = \Product\Model_Group_Discounts::find_one_by_id($id);
                                    if($discount) 
                                    {
                                        $discount->set($discounts[$product_group_id][$key]);
                                        $discount->save();
                                    }
                                } 
                            }
                            
                            // Insert
                            $new_discounts = array();
                            foreach(\Input::post('new_qty.'.$product_group_id, array()) as $key => $value)
                            {
                                // Ignore discounts with same qty. Only one discount per QTY number is allowed
                                // We will insert first QTY in list. All other will be ignired
                                if(!isset($discounts[$product_group_id][$value]))
                                {
                                    $new_discounts[$product_group_id][$value]['user_group_id'] = $item->id;
                                    $new_discounts[$product_group_id][$value]['product_group_id'] = $product_group_id;
                                    $new_discounts[$product_group_id][$value]['qty'] = $value;
                                    $new_discounts[$product_group_id][$value]['discount'] = \Input::post('new_discount.'.$product_group_id.'.'.$key);
                                }
                            }
                            if(!empty($new_discounts))
                            {
                                foreach($new_discounts[$product_group_id] as $key => $value)
                                {
                                    // Insert discount
                                    $discount = \Product\Model_Group_Discounts::forge($new_discounts[$product_group_id][$key]);
                                    $discount->save();
                                } 
                            }
                        }

                        // group was updated
                        \Messages::success('Group successfully updated.');
                        \Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/user/group/list/') : \Uri::admin('current'));
                    }
                    else
                    {
                        // show validation errors
                        \Messages::error('<strong>There was an error while trying to update group</strong>');
                        \Messages::error('Please try again.');
                    }
				}
				catch (\Sentry\SentryGroupException $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update group</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update group</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		$group = \SentryAdmin::group((int)$id);
		
        // Find all product groups and set usergroup id
        if($group->product_groups = \Product\Model_Group::find_by_type('pricing'))
        {
            foreach($group->product_groups as $product_group)
            {
                $product_group->set_user_group_id($group->id);
            } 
        }
       
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('group', $group);
	}
    
    public function action_update_new($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/user/group/list');
		
		// Get group to edit
		if(!$item = \SentryAdmin::group((int)$id)) \Response::redirect('admin/user/group/list');
		
		\View::set_global('title', 'Edit Group');
		
		$group = \SentryAdmin::group((int)$id);
		
        // Find all product groups and set usergroup id
        $group->product_groups = \Product\Model_Group::find_by_type('pricing');
        foreach($group->product_groups as $product_group)
        {
            $product_group->set_user_group_id($group->id);
        }
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'update_new')->set('group', $group);
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = \SentryAdmin::group((int)$id))
			{
                // Some groups are not del
                if($item->deletable)
                {
                    if(!$item->users())
                    {
                        // Delete item
                        try{
                            // Delete group
                            $item->delete();

                            \Messages::success('Group successfully deleted.');
                        }
                        catch (\Sentry\SentryGroupException $e)
                        {
                            // show validation errors
                            \Messages::error('<strong>There was an error while trying to delete group</strong>');

                            // Uncomment lines below to show database errors
                            $errors = $e->getMessage();
                            \Messages::error($errors);
                        }
                    }
                    else
                    {
                        // show validation errors
                        \Messages::error('<strong>There was an error while trying to delete group</strong>');
                        \Messages::error('In order to delete this group first remove all members from it.');
                    }
                }
                else
                {
                    // show validation errors
                    \Messages::error('<strong>There was an error while trying to delete group</strong>');
                    \Messages::error('This group can\'t be deleted.');
                }
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
    /**
     * Validate Model fields
     * 
     * @param $factory = Validation name, if you want to have multiple validations
     */
    public static function validate($factory)
    {
       $val = \Validation::forge($factory);
       $val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);

       return $val;
    }
}
