<?php
/**
 * Part of Eximius Solutions CMS.
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace User
 */

namespace User;

class Controller_Validate
{
	/**
	 * Validate user forms
	 * 
	 * @param string $type 		= Validation type
	 * @param string $update_id = Updated item ID, used for some custom rules
	 * @return Validation object
	 */
	public static function forge($type, $update_id = false, $input_prefix = false)
	{
		$val = \Validation::forge($type);
		
        // Create new user
		if($type == 'create' || $type == 'update' || $type == 'guest' || $type == 'shipping' || $type == 'billing')
		{
			if($type != 'shipping')
            {
                // $val->add('business_name', 'Entity Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                if($type == 'guest')
                {
                    $val->add('email', 'Email')->add_rule('required')->add_rule('valid_email');
                }
                else
                {
                    //$val->add('email', 'Email')->add_rule('required')->add_rule('valid_email')->add_rule('unique', array('users', 'email', $update_id));
                    $val->add('email', 'Email')->add_rule('required')->add_rule('valid_email')->add_rule('unique', array('users', 'username', $update_id));
                }
            }
           
            if($type != 'shipping' && ($type == 'create' || ($type == 'update' && \Input::post('password', false))))
            {
                $val->add('password', 'Password')->add_rule('required')->add_rule('min_length', 6);
                $val->add('confirm_password', 'Confirm Password')->add_rule('required')->add_rule('match_field', 'password');
            }
			
            if($type != 'shipping')
            {
                if($input_prefix) $input_prefixes = array('', $input_prefix);
                else $input_prefixes = array('');
            }
            else
            {
               $input_prefixes = array($type); 
            }
            
            foreach($input_prefixes as $input_prefix)
            {
                // Set to different input prefix, so we are sure we covered both fieldset
                $name_prefix    = $input_prefix ? ucfirst($input_prefix) . ' ' : '';
                $input_prefix   = $input_prefix ? $input_prefix . '_' : '';
                
                //  $val->add($input_prefix . 'title', $name_prefix . 'Title')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                $val->add($input_prefix . 'first_name', $name_prefix . 'First Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                $val->add($input_prefix . 'last_name', $name_prefix . 'Last Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                //  $val->add($input_prefix . 'job_title', $name_prefix . 'Job Title')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
               $val->add($input_prefix . 'address', $name_prefix . 'Address')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
               $val->add($input_prefix . 'suburb', $name_prefix . 'Suburb')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                //  $val->add($input_prefix . 'country', $name_prefix . 'Country')->add_rule('required_not_zero');
                //  $val->add($input_prefix . 'state', $name_prefix . 'State')->add_rule('required_not_zero');
               $val->add($input_prefix . 'postcode', $name_prefix . 'Postcode')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                $val->add($input_prefix . 'phone', $name_prefix . 'Phone')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
            }
		}

		return $val;
	}
	
}