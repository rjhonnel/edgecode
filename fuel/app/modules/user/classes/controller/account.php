<?php

namespace User;

class Controller_Account extends \Controller_Base_User
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/user/account/';
	
	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_index()
	{
		//Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect(\Uri::front_create('user/account/dashboard'));
	}
    
	/**
	 * The user dashboard
	 * 
	 * @access public
	 * @return void
	 */
	public function action_dashboard()
	{
		\View::set_global('title', 'My Account');
        
        // Get current user
		$user = \Sentry::user();

        // Update abandon cart order user_id if user login and cart has products and order id session
        $order_id = \Session::get('order.id', false);
        if(is_numeric($order_id))
        {
            $items = \Cart::items();
            if($items)
            {
                if($order = \Order\Model_Order::find_one_by_id($order_id))
                {
                    $order->set(array('user_id' => $user->get('id')));
                    $order->save();
                }
            }
        }
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'dashboard')
            ->set('user', $user, false);
	}
	
	/**
	 * The user profile
	 * 
	 * @access public
	 * @return void
	 */
	public function action_profile($user_id = false)
	{
		\View::set_global('title', 'User profile');
        
        // Get current user
		$user = \Sentry::user();
        
        $master_user = false;
        if(isset($user['metadata']['master']) && $user['metadata']['master']) 
            $master_user = true;
        
        $user_group = $user->groups();
        $user_group = $user_group[0];
        $edit_user = false;
        $edit_user_group = false;
        
        // Get user for edit
        if($master_user && is_numeric($user_id))
        {
            try
            {
               // Find the user using the user id
               $edit_user = \Sentry::user((int)$user_id);
               
               $edit_user_group = $edit_user->groups();
               $edit_user_group = $edit_user_group[0];
            }
            catch (\Exception $e)
            {
                $errors = $e->getMessage();
                \Messages::error($errors);
                \Response::redirect(\Uri::front_create('user/account/users'));
            }
            
            if($edit_user && isset($edit_user_group['id']) && $edit_user_group['id'] == $user_group['id'])
            {
                $user = $edit_user;
            }
            else
            {
                \Messages::error("You don't have permissions to view this user");
                \Response::redirect(\Uri::front_create('user/account/users'));
            }
            
        }  
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'profile')
            ->set('user', $edit_user, false);
	}
	
    /**
     * Edit account details
     * 
	 * @access public
     */
	public function action_details($user_id = false)
	{
		\View::set_global('title', 'My Account');
        
		// Get current user
		$user = \Sentry::user();
        
        $master_user = false;
        if(isset($user['metadata']['master']) && $user['metadata']['master']) 
            $master_user = true;
        
        $user_group = $user->groups();
        $user_group = $user_group[0];
        $edit_user = false;
        $edit_user_group = false;
        
        // Get user for edit
        if($master_user && is_numeric($user_id))
        {
            try
            {
               // Find the user using the user id
               $edit_user = \Sentry::user((int)$user_id);
               
               $edit_user_group = $edit_user->groups();
               $edit_user_group = $edit_user_group[0];
            }
            catch (\Exception $e)
            {
                $errors = $e->getMessage();
                \Messages::error($errors);
                \Response::redirect(\Uri::front_create('user/account/users'));
            }
            
            if($edit_user && isset($edit_user_group['id']) && $edit_user_group['id'] == $user_group['id'])
            {
                $user = $edit_user;
            }
            else
            {
                \Messages::error("You don't have permissions to edit this user");
                \Response::redirect(\Uri::front_create('user/account/users'));
            }
            
        }        
		
        if(\Input::post('details'))
        {
            $val = \User\Controller_Validate::forge('update', $user->id);

            if($val->run())
            {
                // Get POST values
                $insert = \Input::post();
                array_walk($insert, create_function('&$val', '$val = trim($val);'));
                
                try
                {
                    $username 	= $insert['email'];
                    $email 		= $insert['email'];
                    $password 	= $insert['password'];
                    unset($insert['details'], $insert['user_group'], $insert['username'], $insert['email'], $insert['password'], $insert['confirm_password'], $insert['confirm_email']);

                    // create the user - no activation required
                    $vars = array(
                        'username' 	=> $username,
                        'email' 	=> $email,
                        'metadata' 	=> $insert,
                    );
                    
                    if(!empty($password))
                    {
                        $vars['password'] = $password;
                        $vars['metadata']['guest'] = null;
                    }

                    if($user->update($vars))
                    {
                        \Messages::success('Details successfully updated!');
                        \Response::redirect(\Input::referrer(\Uri::front_create('user/account/details')));
                    }
                    else
                    {
                        // This should never happen, but just in case
                        \Messages::error('There was an error updating. Please try again.');
                    }
                    
                }
                catch (\Sentry\SentryUserException $e)
                {
                    // show validation errors
                    //\Messages::error('<h4>There was an error while trying to create user</h4>');
                    $errors = $e->getMessage();
                    \Messages::error($errors);
                }
            }
            else
            {
                if($val->error() != array())
                {
                    // show validation errors
                    //\Messages::error('<h4>There was an error while trying to create user</h4>');
                    foreach($val->error() as $e)
                    {
                        \Messages::error($e->get_message());
                    }
                    
                    \Response::redirect(\Input::referrer(\Uri::front_create('user/account/details')));
                }
            }
        }

        if(\Input::is_ajax())
        {
            echo \Theme::instance()->view($this->view_dir . 'single_details')
                    ->set('user', $user, false);
        }
        else
        {
            \Theme::instance()->set_partial('content', $this->view_dir . 'single_details')
                ->set('user', $user, false);
        }
		
	}
    
    /**
     * Create sub user
     * 
	 * @access public
     */
	public function action_create()
	{
		\View::set_global('title', 'Create User');
        
		// Get current user
		$user = \Sentry::user();
        
        $master_user = false;
        if(isset($user['metadata']['master']) && $user['metadata']['master']) 
            $master_user = true;
        
        $user_group = $user->groups();
        $user_group = $user_group[0];
        $edit_user = false;
        $edit_user_group = false;
        
        // Check for permissions
        if(!$master_user)
        {
            \Messages::error("You don't have permission to create new user");
            \Response::redirect(\Uri::front_create('user/account'));
        }        
		
        if(\Input::post('details'))
        {
            $val = \User\Controller_Validate::forge('create');

            if($val->run())
            {
                // Get POST values
                $insert = \Input::post();
                array_walk($insert, create_function('&$val', '$val = trim($val);'));

                try
                {
                    $username 	= $insert['email'];
                    $email 		= $insert['email'];
                    $password 	= $insert['password'];
                    $user_group	= $insert['user_group'];
                    unset($insert['details'], $insert['user_group'], $insert['username'], $insert['email'], $insert['password'], $insert['confirm_password'], $insert['confirm_email'], $insert['terms']);

                    // create the user - no activation required
                    $vars = array(
                        'username' 	=> $username,
                        'email' 	=> $email,
                        'password' 	=> $password,
                        'metadata' 	=> $insert,
                    );

                    // Create user and activate it instantly
                    $user_registration = \Sentry::user()->create($vars, false);

                    $user = \Sentry::user($user_registration);

                    // Add user to 'customer' group (id = 3)
                    if($user_registration and $user->add_to_group($user_group))
                    {
                        \Messages::success('User successfully created.');
                        \Response::redirect(\Uri::front_create('user/account/profile/' . $user_registration));
                    }
                }
                catch(\Sentry\SentryUserException $e)
                {
                    // show validation errors
                    //\Messages::error('<h4>There was an error while trying to create user</h4>');
                    $errors = $e->getMessage();
                    \Messages::error($errors);
                }
            }
            else
            {
                if($val->error() != array())
                {
                    // show validation errors
                    //\Messages::error('<h4>There was an error while trying to create user</h4>');
                    foreach($val->error() as $e)
                    {
                        \Messages::error($e->get_message());
                    }
                }
            }
        }

        if(\Input::is_ajax())
        {
            echo \Theme::instance()->view($this->view_dir . 'create')
                    ->set('user', $user, false)
                    ->set('user_group', $user_group, false);
        }
        else
        {
            \Theme::instance()->set_partial('content', $this->view_dir . 'create')
                ->set('user', $user, false)
                ->set('user_group', $user_group, false);
        }
		
	}
		
	/**
	 * Change user details
	 * 
	 * @param $field = Field name (username, email, password)
	 * 
	 * @access public
	 * @return void
	 */
	public function action_change($field = 'password')
	{
		$field = strtolower($field);
		
		$allowed_changes = array('username', 'email', 'password', 'dob');
		
		if(!in_array($field, $allowed_changes))
		{
			\Response::redirect(\Uri::front_create('user'));
		}
		
		\View::set_global('title', __('Change :field_name', array('field_name' => ucfirst($field))));
		
		// Get current user
		$user = \Sentry::user();
		
		if(\Input::post())
		{
			$val = \User\Controller_Validate::forge('change_'.$field, $user->id);
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				
			    try
			    {
			    	// update the user
                    if($field == 'dob')
                    {
                        $user->update(array('metadata' => array(
                            'dob_year' 	=> $insert['dob_year'],
                            'dob_month' 	=> $insert['dob_month'],
                            'dob_day' 	=> $insert['dob_day'],
                        )));
                    }
				    else
                    {
                        $user->update(array($field 	=> $insert[$field]));
                    }
			    	
				    if($user->in_group(4))
				    {
					    $user->remove_from_group(4); // Remove from guest group
				    	$user->add_to_group(3); // Add to standard customers group
				    }
				    
			    	\Messages::success(__(':field_name successfully changed.', array('field_name' => ucfirst($field))));
					\Response::redirect(\Input::referrer(\Uri::front('current')));
			    }
			    catch (\Sentry\SentryUserException $e)
			    {
			    	// show validation errors
					//\Messages::error('<h4>There was an error while trying to create user</h4>');
			    	$errors = $e->getMessage();
			    	\Messages::error($errors);
			    }
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					//\Messages::error('<h4>There was an error while trying to create user</h4>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		if(\Input::is_ajax())
		{
			\View::set_global('language', $this->language);
			echo \Theme::instance()->view($this->view_dir . 'change_'.$field, array('user' => $user));
		}
		else
		{
            if(isset($val)) \View::set_global('validation', $val, false);
			\Theme::instance()->set_partial('content', $this->view_dir . 'single_change_'.$field);
		}
	}
	
	/**
	 * Order history
	 *
	 * @access public
	 * @return void
	 */
	public function action_orders()
	{
        \View::set_global('title', 'Orders');
        
		// Get current user
		$user = \Sentry::user();
		$user_ids = array($user->id);
        
        $master_user = false;
        if(isset($user['metadata']['master']) && $user['metadata']['master']) 
            $master_user = true;
        
        // Get all orders from subusers if this is master user
        if($master_user)
        {
            $user_group = $user->groups();
            $user_group = $user_group[0];

            $users = \Sentry::group($user_group['id'])->users();
            
            // Reset to empty array if there are no result found by query
            if(is_null($users)) $users = array();
            
            foreach($users as $subuser)
            {
                array_push($user_ids, $subuser['id']);
            }
        }
        
		// Load order config
		\Config::load('order::order', 'order');
		
		// Get orders
        $items = \Order\Model_Order::find(function($query) use ($user_ids){
            $query->where('user_id', 'IN', $user_ids);
            $query->where('finished', 1);
            $query->order_by('id', 'desc');
        });	
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
		));
		
		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'orders')
            ->set('items', $items, false)
            ->set('pagination', $pagination, false)
            ->set('order_status', \Config::get('order.status', array()), false);
	}
	
	/**
	 * Order details
	 *
	 * @access public
	 * @param  int		$id = Order ID
	 * @return void
	 */
	public function action_order($id = false)
	{
        \View::set_global('title', 'Order');
        
        \Config::load('order::order', 'order');
        
        // Get current user
		$user = \Sentry::user();
		$user_ids = array($user->id);
        
        $master_user = false;
        if(isset($user['metadata']['master']) && $user['metadata']['master']) 
            $master_user = true;
        
        // Get all orders from subusers if this is master user
        if($master_user)
        {
            $user_group = $user->groups();
            $user_group = $user_group[0];

            $users = \Sentry::group($user_group['id'])->users();
            
            // Reset to empty array if there are no result found by query
            if(is_null($users)) $users = array();
            
            foreach($users as $subuser)
            {
                array_push($user_ids, $subuser['id']);
            }
        }
        
		// Get order
        $order = \Order\Model_Order::find(function($query) use ($user_ids, $id){
            $query->where('id', $id);
            $query->and_where('user_id', 'IN', $user_ids);
            $query->and_where('finished', 1);
        });	
        
        if(!$order)
		{
			\Messages::error('Order with that ID does not exist or has been deleted.');
			\Response::redirect(\Uri::front_create('user/account/orders'));
		}	
		
		// Load order config
		\Config::load('order::order', 'order');
		
		// Get order products
		$items = \Order\Model_Products::find(array('where' => array('order_id' => $id)));	

		\Theme::instance()->set_partial('content', $this->view_dir . 'order')
		->set('items', $items, false)
		->set('order', $order[0], false)
		->set('order_status', \Config::get('order.status', array()), false)
        ->set('user', $user, false);
	}
    
    /**
	 * Users
	 *
	 * @access public
	 * @return void
	 */
	public function action_users()
	{
		\View::set_global('title', 'Users');
		
		// Get current user
		$user = \Sentry::user();
        
        $master_user = false;
        if(isset($user['metadata']['master']) && $user['metadata']['master']) 
            $master_user = true;
        
        if(!$master_user)
        {
            \Messages::error("You don't have permssion to view this page.");
			\Response::redirect(\Uri::front_create('user/account'));
        }
        
        $user_group = $user->groups();
        $user_group = $user_group[0];
        
        $users = \Sentry::group($user_group['id'])->users();
        
        // Reset to empty array if there are no result found by query
		if(is_null($users)) $users = array();
		
        // Remove current user
        foreach($users as $key => $group_user)
        {
            if($group_user['id'] == $user->id)
            {
                unset($users[$key]);
            }
        }
        
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($users),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
//            'template' => $this->pagination_template,
		));
        
        // Remove unwanted items, and show only required ones
		$users = array_slice($users, $pagination->offset, $pagination->per_page);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'users')
                   ->set('user', $user, false)
                   ->set('user_group', $user_group, false)
                   ->set('users', $users, false)
                   ->set('pagination', $pagination, false);
         
	}
    
    public function action_accept_order($id = null)
    {
        // Get order
        $order = \Order\Model_Order::find_one_by_id($id);
        
        if(!$order)
		{
			\Messages::error('Order with that ID does not exist or has been deleted.');
			\Response::redirect(\Uri::front_create('user/account/orders'));
		}	
        
        // Get current user
		$user = \Sentry::user();
        
        $master_user = false;
        if(isset($user['metadata']['master']) && $user['metadata']['master']) 
            $master_user = true;
        
        if(!$master_user)
        {
            \Messages::error("You don't have permssion for this action.");
			\Response::redirect(\Uri::front_create('user/account'));
        }
        
        try
        {
            $order->accepted = 1;
            $order->save();
            \Messages::success('Order successfully updated.');
            \Response::redirect(\Input::referrer(\Uri::front_create('user/account')));
        }
        catch (\Database_Exception $e)
        {
            \Messages::error('There was an error while trying to update order.');
        }
        
        
        
    }


    public function action_referrals()
    {
        if(strtolower(\Sentry::user()->groups()[0]['name']) != 'club members')
        {
            \Messages::error('Only club members can access this page.');
            \Response::redirect('/');
        }

        \View::set_global('title', 'Referrals');


        $id = \Sentry::user()['id'];

        $items = Model_Referal::find(function($query) use ($id){

            $query->where('user_added', $id);
            $query->order_by('id', 'desc');

        });

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        $items = array_slice($items, $pagination->offset, $pagination->per_page);

        \Theme::instance()->set_partial('content', $this->view_dir . 'referrals')
                        ->set('pagination', $pagination, false)
                        ->set('items', $items);
    }
    
    
    
}