<?php 

/**
 * CMS
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 *
 * @namespace User
 * @extends \Model_Base
 */

namespace User;

class Model_Notes extends \Model_Base
{
		// Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'users_note';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
            'id',
            'user_id',
            'note',
            'user_created',
            'user_updated',
		);
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    protected static $_defaults = array(

	    );
	    
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
				$vars['user_created'] = \Sentry::user()->id;
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}

		/**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('note', 'Note')->add_rule('required');
	
			return $val;
		}
	}