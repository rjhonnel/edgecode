<?php

	namespace User;

	class Model_Referal extends \Model_Base
	{	
	    // Set the table to use
	    protected static $_table_name = 'referals';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'user_added',
		    'name',
		    'phone',
		    'email',
		    'suburb',
		);
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';

	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('name', 'Name')->add_rule('required')->add_rule('max_length', 255);
			$val->add('phone', 'Phone')->add_rule('required')->add_rule('max_length', 255);
			$val->add('email', 'Email')->add_rule('required')->add_rule('unique', array(self::$_table_name, 'email'))->add_rule('max_length', 255);
			$val->add('suburb', 'Suburb')->add_rule('required')->add_rule('max_length', 255);
	
			return $val;
		}
	}
