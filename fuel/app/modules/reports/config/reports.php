<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
 		
    'status' => array(
        'in_progress'           => 'In Progress',
        'shipped'               => 'Shipped',
        'invoiced'              => 'Invoiced',
        'paid'                  => 'Paid',
        'cancelled'             => 'Canceled',
    ),
 );