<?php

/**
 * CMS
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Order
 * @extends \Controller_Base_Public
 */

namespace Reports;

class Controller_Admin_Reports extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/reports/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('reports::reports', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}

	public function action_orders()
	{
		\View::set_global('title', 'Order Reports');

		$search = $this->get_search_orders();
        
		$items      = $search['items'];

		\Theme::instance()->set_partial('content', $this->view_dir . 'orders')
			->set('items', $items);
	}
    
    public function get_search_orders($user_id = false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \Sentry::user_exists((int)$user_id))
        {
            $user = \Sentry::user((int)$user_id);
        }
        
        if( \Input::get() )
        {
            if( \Input::get('date_from') != "" || \Input::get('date_to') != "" || \Input::get('status') != 'false' )
            {
                $items = \Order\Model_Order::find(function($query){
                    
                    if(isset($user))
                        $query->where('user_id', $user->id);
                    
                    $query->where('finished', '1');
                    $query->order_by('id', 'desc');
                    
                });
            }
        }
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        foreach($items as $number => $item)
                        {
                            $full_name = $item->first_name . ' ' . $item->last_name;
                            if(stripos($item->company, $value) === false && stripos($item->id, $value) === false )
                            {
                                if(stripos($full_name, $value) === false) unset($items[$number]);
                            }
                        }
                        break;
                    case 'email':
                        foreach($items as $number => $item)
                        {
                            if(stripos($item->email, $value) === false) unset($items[$number]);
                        }
                        break;
                 
                    case 'order_total_from':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] < $value) unset($items[$number]);
                        }
                        break;
                    case 'order_total_to':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] > $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at > $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->status, $value) === false) unset($items[$number]);
                        }
                        break;
                   
                    case 'tracking_no':
                        foreach($items as $number => $item)
                        {
                            if(!$value != '') break;
                            if(stripos($item->tracking_no, $value) === false) unset($items[$number]);
                        }
                        break;
                    
                    case 'payment_method':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(!empty($item->last_payment))
                            {
                                if($item->last_payment->method != $value) 
                                {
                                    unset($items[$number]);
                                }
                            }
                        }
                        break;
                        
                    case 'user_group':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if($item->user_id && \Sentry::user_exists((int)$item->user_id))
                            {
                                $user = \Sentry::user((int)$item->user_id);
                                if ($user->in_group($value)) unset($items[$number]);
                            }
                        }
                        break;
                       
                    case 'country':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'state':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'product_category':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            // Get order products
                            if(!empty($item->products))
                            {
                                $exists = array();
                                foreach ($item->products as $product)
                                {
                                    // Find category
                                    if(\Product\Model_Product_To_Categories::find(array('where' => array('product_id' => $product->product_id, 'category_id' => $value))))
                                    {
                                        $exists[] = $product->id;
                                    }
                                }
                                if(empty($exists)) unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_orders()
    {
        $search = $this->get_search_orders();
        $items = $search['items'];

        $output = "Order Date,Client ID,Client Name,Order Amount, Order Status\n";
        $content = 'text/csv';
        $filename = 'Orders-Reports-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $output .=  implode(",",[
                date('d/m/Y', $item->created_at).' '.date('H:i:s', $item->created_at),
                $item->user_id,
                $item->first_name . ' ' . $item->last_name,
                '$'.round($item->total_price(), 2),
                \Inflector::humanize($item->status),
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_customer_orders()
    {
        \View::set_global('title', 'Customer Order Reports');

        $search = $this->get_search_customer_orders();
        
        $items      = $search['items'];

        \Theme::instance()->set_partial('content', $this->view_dir . 'customer_orders')
            ->set('items', $items);
    }
    
    public function get_search_customer_orders($user_id = false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \Sentry::user_exists((int)$user_id))
        {
            $user = \Sentry::user((int)$user_id);
        }

        if(\Input::get())
        {        
            if( ( \Input::get('date_from') != "" || \Input::get('date_to') != "" || \Input::get('status') != 'false' || \Input::get('title') != 'customer_order_select' ) )
            {
                $items = \Order\Model_Order::find(function($query){
                    
                    if(isset($user))
                        $query->where('user_id', $user->id);

                    //get customer only -- start
                    $customer_list = \Sentry::user()->all('front');
                    $customer_id_list = array();
                    foreach ($customer_list as $customer) {
                        array_push($customer_id_list, $customer['id']);
                    }

                    $query->where('user_id', 'in', $customer_id_list);
                    //get customer only -- end
                    
                    $query->where('finished', '1');
                    $query->order_by('id', 'desc');
                    
                });
            }
        }
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        if($value != 'customer_order_select')
                        {
                            foreach($items as $number => $item)
                            {
                                $full_name = $item->first_name . ' ' . $item->last_name;
                                if(stripos($item->company, $value) === false && stripos($item->id, $value) === false )
                                {
                                    if(stripos($full_name, $value) === false) unset($items[$number]);
                                }
                            }
                        }
                        break;
                    case 'email':
                        foreach($items as $number => $item)
                        {
                            if(stripos($item->email, $value) === false) unset($items[$number]);
                        }
                        break;
                 
                    case 'order_total_from':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] < $value) unset($items[$number]);
                        }
                        break;
                    case 'order_total_to':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] > $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at > $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->status, $value) === false) unset($items[$number]);
                        }
                        break;
                   
                    case 'tracking_no':
                        foreach($items as $number => $item)
                        {
                            if(!$value != '') break;
                            if(stripos($item->tracking_no, $value) === false) unset($items[$number]);
                        }
                        break;
                    
                    case 'payment_method':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(!empty($item->last_payment))
                            {
                                if($item->last_payment->method != $value) 
                                {
                                    unset($items[$number]);
                                }
                            }
                        }
                        break;
                        
                    case 'user_group':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if($item->user_id && \Sentry::user_exists((int)$item->user_id))
                            {
                                $user = \Sentry::user((int)$item->user_id);
                                if ($user->in_group($value)) unset($items[$number]);
                            }
                        }
                        break;
                       
                    case 'country':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'state':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'product_category':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            // Get order products
                            if(!empty($item->products))
                            {
                                $exists = array();
                                foreach ($item->products as $product)
                                {
                                    // Find category
                                    if(\Product\Model_Product_To_Categories::find(array('where' => array('product_id' => $product->product_id, 'category_id' => $value))))
                                    {
                                        $exists[] = $product->id;
                                    }
                                }
                                if(empty($exists)) unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
        return array('items' => $items);
    }

    public function action_export_customer_orders()
    {
        $search = $this->get_search_customer_orders();
        $items = $search['items'];

        $output = "Order Date,Order Amount,Order Status,Payment Status\n";
        $content = 'text/csv';
        $filename = 'Customer-x-Orders-Reports-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $output .=  implode(",",[
                date('d/m/Y', $item->created_at).' '.date('H:i:s', $item->created_at),
                '$'.round($item->total_price(), 2),
                \Inflector::humanize($item->status),
                $item->last_payment ?  $item->last_payment->status_detail : '',
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

	public function action_products()
	{
        \View::set_global('title', 'Product Reports');

        $search = $this->get_search_products();
        
        $items      = $search['items'];

		\Theme::instance()->set_partial('content', $this->view_dir . 'products')
            ->set('items', $items);
	}
    
    public function get_search_products($category_id = false)
    {
        // Override category_id if its a search
        $category_id = \Input::get('category_id', $category_id);
        
        $product_ids = array();
        
        // If we are viewing category products
        // We need to find all products from that and child categories
        if(\Input::get())
        {
            if(\Input::get('status') != 'select')
            {
                if(is_numeric($category_id))
                {
                    $category = \Product\Model_Category::find_one_by_id($category_id);
                    if($category)
                    { 
                        \View::set_global('category', $category);
                        if($category->all_products)
                        {
                            $product_ids = array_keys($category->all_products);
                        }
                    }
                }
            }
        }
        
        // If we are filtering products by category and there is nothing found
        // We return empty array of products without even going to database
        if(empty($product_ids) && is_numeric($category_id)) 
        { 
            $items = array();
        }
        else
        {
            /************ Start generating query ***********/

            if(\Input::get())
            {
                if(\Input::get('status') != 'select')
                {
                    $items = \Product\Model_Product::find(function($query) use ($product_ids){

                        if(!empty($product_ids))
                        {
                            $query->where('id', 'in', $product_ids);
                        }

                        // Get search filters
                        foreach(\Input::get() as $key => $value)
                        {
                            if(!empty($value) || $value == '0')
                            {
                                switch($key)
                                {
                                    case 'title':
                                        $query->where($key, 'like', "%$value%")
                                            ->or_where('code', 'like', "%$value%");
                                        break;
                                    case 'status':
                                        if(is_numeric($value))
                                            $query->where($key, $value);
                                        break;
                                    case 'active_from':
                                        $date = strtotime($value);
                                        if($date)
                                            $query->where($key, '>=', $date);
                                        break;
                                    case 'active_to':
                                        $date = strtotime($value);
                                        if($date)
                                            $query->where($key, '<=', $date);
                                        break;
                                }
                            }
                        }

                        // Order query
                        $query->order_by('sort', 'asc');
                        $query->order_by('id', 'asc');
                    });
                }
            }
        }
        
        /************ End generating query ***********/
        
        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_products()
    {
        $status = array(
            'false' => 'All',
            '1' => 'Active',
            '0' => 'Inactive',
        );

        $search = $this->get_search_products();
        $items = $search['items'];

        $output = "Product Code,Product Name,Attribute Group,Attribute,Status,Stock\n";
        $content = 'text/csv';
        $filename = 'Products-Reports-'.date('YmdHis').' Report.csv';


        foreach ($items as $item) {
            $check_attribute = \DB::query('SELECT * FROM product_attributes a where a.product_id = '.$item->id)->execute();
            if(count($check_attribute)==1 && $check_attribute[0]['attribute_group_id']==0)
            {
                $hold_stock = \DB::query('SELECT b.id, b.stock_quantity FROM product a join product_attributes b on a.id = b.product_id where a.id = '.$item->id)->execute();

                // If page is active from certain date
                $get_status = '';
                if($item->status == 2)
                {
                    $dates = array();
                    !is_null($item->active_from) and array_push($dates, date('m/d/Y', $item->active_from));
                    !is_null($item->active_to) and array_push($dates, date('m/d/Y', $item->active_to));

                    if(true)
                    {
                        $get_status = 'Active '.implode(' - ', $dates);
                    }
                }
                else
                {
                    $get_status = $status[$item->status];
                }

                $output .=  implode(",",[
                    $item->code,
                    $item->title,
                    $item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A',
                    $item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A',
                    $get_status,
                    $hold_stock[0]['stock_quantity'],
                ]). "\n";
            }
            else
            {
                foreach ($check_attribute as $key => $value)
                {
                    $get_attribute = '';
                    $attribute_list = (json_decode($value['attributes'], TRUE)); 
                    $attribute_list_hold = array();

                    foreach ($attribute_list as $key => $value1) {
                        $attribute_list_hold[] = \DB::query('SELECT a.title FROM attribute_options a where a.attribute_id = '.$key.' and a.id = '.$value1)->execute();
                    }

                    $count_attr = count($attribute_list_hold);
                    $count_attr_count = 0;
                    foreach ($attribute_list_hold as $key => $value3) {
                        foreach ($value3[0] as $key => $value2) {
                            $get_attribute .= $value2;
                            if($count_attr-1 != $count_attr_count)
                                $get_attribute .= " | ";
                            $count_attr_count++;
                        }
                    }

                    $get_status = '';
                    // If page is active from certain date
                    if($item->status == 2)
                    {
                        $dates = array();
                        !is_null($item->active_from) and array_push($dates, date('m/d/Y', $item->active_from));
                        !is_null($item->active_to) and array_push($dates, date('m/d/Y', $item->active_to));

                        if(true)
                        {
                            $get_status = 'Active '.implode(' - ', $dates);
                        }
                    }
                    else
                    {
                        $get_status = $status[$item->status];
                    }

                    $output .=  implode(",",[
                        $value['product_code'],
                        $item->title,
                        ($item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A'),
                        $get_attribute, 
                        $get_status,
                        $value['stock_quantity'],
                    ]). "\n";
                }
            }
        }

        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_customer_products()
    {
        \View::set_global('title', 'Customer Product Reports');

        $search = $this->get_search_customer_products();
        
        $items      = $search['items'];

        \Theme::instance()->set_partial('content', $this->view_dir . 'customer_products')
            ->set('items', $items);
    }
    
    public function get_search_customer_products($user_id = false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \Sentry::user_exists((int)$user_id))
        {
            $user = \Sentry::user((int)$user_id);
        }
        
        $items = \Order\Model_Order::find(function($query){
            
            if(isset($user))
                $query->where('user_id', $user->id);

            //get customer only -- start
            $customer_list = \Sentry::user()->all('front');
            $customer_id_list = array();
            foreach ($customer_list as $customer) {
                array_push($customer_id_list, $customer['id']);
            }

            $query->where('user_id', 'in', $customer_id_list);
            //get customer only -- end
            
            $query->where('finished', '1');
            $query->order_by('id', 'desc');
            
        });
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        if($value != 'customer_order_select')
                        {
                            foreach($items as $number => $item)
                            {
                                $full_name = $item->first_name . ' ' . $item->last_name;
                                if(stripos($item->company, $value) === false && stripos($item->id, $value) === false )
                                {
                                    if(stripos($full_name, $value) === false) unset($items[$number]);
                                }
                            }
                        }
                        break;
                    case 'email':
                        foreach($items as $number => $item)
                        {
                            if(stripos($item->email, $value) === false) unset($items[$number]);
                        }
                        break;
                 
                    case 'order_total_from':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] < $value) unset($items[$number]);
                        }
                        break;
                    case 'order_total_to':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] > $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at > $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->status, $value) === false) unset($items[$number]);
                        }
                        break;
                   
                    case 'tracking_no':
                        foreach($items as $number => $item)
                        {
                            if(!$value != '') break;
                            if(stripos($item->tracking_no, $value) === false) unset($items[$number]);
                        }
                        break;
                    
                    case 'payment_method':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(!empty($item->last_payment))
                            {
                                if($item->last_payment->method != $value) 
                                {
                                    unset($items[$number]);
                                }
                            }
                        }
                        break;
                        
                    case 'user_group':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if($item->user_id && \Sentry::user_exists((int)$item->user_id))
                            {
                                $user = \Sentry::user((int)$item->user_id);
                                if ($user->in_group($value)) unset($items[$number]);
                            }
                        }
                        break;
                       
                    case 'country':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'state':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'product_category':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            // Get order products
                            if(!empty($item->products))
                            {
                                $exists = array();
                                foreach ($item->products as $product)
                                {
                                    // Find category
                                    if(\Product\Model_Product_To_Categories::find(array('where' => array('product_id' => $product->product_id, 'category_id' => $value))))
                                    {
                                        $exists[] = $product->id;
                                    }
                                }
                                if(empty($exists)) unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_customer_products()
    {
        $status = array(
            'false' => 'All',
            '1' => 'Active',
            '0' => 'Inactive',
        );
        $search = $this->get_search_customer_products();
        $items = $search['items'];

        $output = "Product Code,Product Name,Attribute Group,Status,Stock,Price Paid\n";
        $content = 'text/csv';
        $filename = 'Customer-x-Products-Reports-'.date('YmdHis').' Report.csv';

        if( !is_null(\Input::get('title')) && \Input::get('title') != 'customer_order_select')
        {
            foreach ($items as $item) {
                foreach($item->products as $product)
                {
                    $prdct = \Product\Model_Product::find_one_by_id($product->product_id);
                    $hold_stock = \DB::query('SELECT b.id, b.stock_quantity FROM product a join product_attributes b on a.id = b.product_id where a.id = '.$prdct->id)->execute();
                    $get_status = '';
                    // If page is active from certain date
                    if($prdct->status == 2)
                    {
                        $dates = array();
                        !is_null($prdct->active_from) and array_push($dates, date('m/d/Y', $prdct->active_from));
                        !is_null($prdct->active_to) and array_push($dates, date('m/d/Y', $prdct->active_to));

                        if(true)
                        {
                            $get_status = 'Active '.implode(' - ', $dates);
                        }
                    }
                    else
                    {
                        $get_status = $status[$prdct->status];
                    }

                    $output .=  implode(",",[
                        $product->code,
                        $product->title,
                        $prdct->active_attribute_group ? $prdct->active_attribute_group[0]->title : 'N/A',
                        $get_status,
                        $hold_stock[0]['stock_quantity'],
                        '$'.$product->price,
                    ]). "\n";   
                }
            }
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

	public function action_customers()
	{
        \View::set_global('title', 'Customer Reports');

        $search = $this->get_search_customers();
        
        $items      = $search['items'];

		\Theme::instance()->set_partial('content', $this->view_dir . 'customers')
            ->set('items', $items);
	}
    
    public function get_search_customers($group_id = false)
    {
        // Override group_id if its a search
        $group_id = \Input::get('user_group', $group_id);

        $activated = \Input::get('activated', false);


        if(\Input::get())
        {
            if($group_id && \Sentry::group_exists((int)$group_id))
            {
                // Get only group users
                \View::set_global('group', \Sentry::group((int)$group_id));
                $items = \Sentry::group((int)$group_id)->users();
            }
            else
            {
                // Get all users and remove admin users from array
                $items = \Sentry::user()->all('front');
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Get user objects
        if(!empty($items))
        {
            foreach($items as $key=> $item)
            {
                $items[$key] = \Sentry::user((int)$item['id']);
            }
            
            // Get search filters
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title': 
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                $full_name = $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name');
                                $customer_id = $item->get('metadata.user_id');

                                if(stripos($full_name, $value) === false && stripos($customer_id, $value) === false ) unset($items[$number]);
                            }
                            break;
                        case 'email':
                            foreach($items as $number => $item)
                            {
                                if(stripos($item->email, $value) === false) unset($items[$number]);
                            }
                            break;
                        case 'country':
                            if($value && $value !== 'false')
                            {
                                foreach($items as $number => $item)
                                {
                                    if(empty($item['metadata'])) 
                                    {
                                        unset($items[$number]);
                                        continue;
                                    }
                                    if(stripos($item->get('metadata.country'), $value) === false) unset($items[$number]);
                                }
                            }
                            break;
                        case 'postcode_from':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') < $value) unset($items[$number]);
                            }
                            break;
                        case 'postcode_to':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') > $value) unset($items[$number]);
                            }
                            break;
                        case 'activated':
                            if($value !== 'false')
                            {
                                foreach($items as $number => $item)
                                {
                                    if($item->activated != $value) unset($items[$number]);
                                }
                            }
                            break;
                    }
                }
            }
        }

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_customers()
    {
        $search = $this->get_search_customers();
        $items = $search['items'];

        $output = "Customer ID,Customer Name,Company Name,Customer Group,Email,Phone,State,Activated,Account Created\n";
        $content = 'text/csv';
        $filename = 'Customers-Reports-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $output .=  implode(",",[
                ($item->metadata) ? $item->get('metadata.user_id') : '',
                ($item->metadata) ? $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name') : '',
                ($item->metadata) ? $item->get('metadata.business_name') : '',
                $user_group['name'],
                $item->get('email'),
                ($item->metadata) ? $item->get('metadata.phone') : '',
                ($item->metadata) ? $item->get('metadata.state') : '',
                $item->get('activated') == 1 ? 'Yes' : 'No',
                date('d/m/Y', $item->created_at),
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

	public function action_sales_by_month()
	{
        \View::set_global('title', 'Sales By Month Reports');

        $search = $this->get_search_sales_by_month();

        $items      = $search['items'];
        $years      = $search['years'];

		\Theme::instance()->set_partial('content', $this->view_dir . 'sales_by_month')
            ->set('items', $items)
            ->set('years', $years);
	}

    public function get_search_sales_by_month()
    {
        $years = \Order\Model_Order::find(array(
            'where' => array(
                array('total_price', 'is not', NULL),
            )
        ));

        $year_list = array();
        $month_list = array(); 
        foreach ($years as $year) {
            $year = date('Y', $year->created_at);

            if (!in_array($year, $year_list))
                $year_list += array($year => $year);
        } 

        $get_first_year = $year_list;
        rsort($get_first_year);

        krsort($year_list);
        if(\Input::get())
        {
            if(\Input::get('year')!='select')
            {
                $choose_year = \Input::get('year') ? \Input::get('year') : $get_first_year[0];

                for ($i=0; $i<12; $i++) 
                { 
                    $timestamp = mktime(0,0,1,date("m")+$i,date("d"),date("Y")); 
                    $month = date("F", $timestamp); 
                    $results = \DB::query('SELECT sum(total_price) as total_amount from orders where year(FROM_UNIXTIME(created_at)) = '.$choose_year.' and month(FROM_UNIXTIME(created_at)) = '.($i+1))->execute();
                    array_push($month_list, array('month' => array('name' => $month, 'amount' => number_format($results[0]['total_amount'], 2))));
                }
            }
        }

        return array('items' => $month_list, 'years' => $year_list);
    }

    public function action_export_sales_by_month()
    {
        $search = $this->get_search_sales_by_month();
        $items = $search['items'];

        $output = "Month,Amount\n";
        $content = 'text/csv';
        $filename = 'Sales-x-Year-Reports-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $item['month']['amount'] = str_replace(',', '', $item['month']['amount']);

            $output .=  implode(",",[
                $item['month']['name'],
                '$'.($item['month']['amount'] ? $item['month']['amount'] : '0'),
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_sales_by_date()
    {
        \View::set_global('title', 'Sales By Date Reports');

        $search = $this->get_search_sales_by_date();

        $dates      = $search['dates'];
        $months    = $search['months'];
        $years     = $search['years'];

        \Theme::instance()->set_partial('content', $this->view_dir . 'sales_by_date')
            ->set('dates', $dates)
            ->set('months', $months)
            ->set('years', $years);
    }

    public function get_search_sales_by_date()
    {
        $years = \Order\Model_Order::find(array(
            'where' => array(
                array('total_price', 'is not', NULL),
            )
        ));

        $year_list = array();
        foreach ($years as $year) {
            $year = date('Y', $year->created_at);

            if (!in_array($year, $year_list))
                $year_list += array($year => $year);
        }

        $get_first_year = $year_list;
        rsort($get_first_year);

        krsort($year_list);

        $choose_year = \Input::get('year') ? \Input::get('year') : $get_first_year[0];

        $month_list = array(); 
        for ($i=0; $i<12; $i++) 
        { 
            $timestamp = mktime(0,0,1,date("m")+$i,date("d"),date("Y")); 
            $month = date("F", $timestamp);
            $month_list += array($i+1 => $month);
        }

        $choose_month = \Input::get('month') ? \Input::get('month') : 1;

        $number_of_days = cal_days_in_month (null, $choose_month, $choose_year);

        $items = array();
        if(\Input::get())
        {
            for ($i=1; $i <= $number_of_days; $i++)
            {
                $results = \DB::query('SELECT sum(total_price) as total_price from orders where year(FROM_UNIXTIME(created_at)) = '.$choose_year.' and month(FROM_UNIXTIME(created_at)) = '.$choose_month.' and day(FROM_UNIXTIME(created_at)) = '.$i)->execute();
                array_push($items, array('amount'=>  $results[0]['total_price'], 'date' => $i.'/'.$choose_month.'/'.$choose_year));
            }
        }

        return array('months' => $month_list, 'years' => $year_list, 'dates' => $items);
    }

    public function action_export_sales_by_date()
    {
        $search = $this->get_search_sales_by_date();
        $dates = $search['dates'];

        $output = "Date,Amount\n";
        $content = 'text/csv';
        $filename = 'Sales-x-Date-Reports-'.date('YmdHis').' Report.csv';

        foreach ($dates as $date) {
            $item['amount'] = str_replace(',', '', $item['amount']);

            $output .=  implode(",",[
                $date['date'],
                '$'.($date['amount'] ? $date['amount']:'0'),
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }
}