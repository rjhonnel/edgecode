<?php

/**
 * CMS
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Order
 * @extends \Controller_Base_Public
 */

namespace Reports;

class Controller_Admin_Reports extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/reports/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('reports::reports', 'details');
        \Config::load('order::order', 'order');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
        if(!\Sentry::user()->in_group('Super Admin'))
        {
            $arr = array('orders_products','order_notes');
            if(!in_array(\Uri::segment(4), $arr)){
                \Response::redirect('admin/order/list'); 
            }
        }
	}

	public function action_orders()
	{

	    \View::set_global('menu', 'admin/reports/orders');
	    \View::set_global('title', 'Order Report');

		$search = $this->get_search_orders();
        
		$items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        // $items = array_slice($items, $pagination->offset, $pagination->per_page);

		\Theme::instance()->set_partial('content', $this->view_dir . 'orders')
			->set('items', $items)
            ->set('pagination', $pagination, false);
	}

    public function action_dateHasDeliveryOrderByMonth()
    {
        $date_list = array();
        
        // $date_from_parts = explode('/', \Input::get('date_from'));
        // $date_from_value = $date_from_parts[2].'-'.$date_from_parts[1].'-'.$date_from_parts[0];

        $results = \DB::query('SELECT concat(YEAR(delivery_datetime),"-",MONTH(delivery_datetime),"-",DAY(delivery_datetime)) as delivery FROM orders WHERE finished = 1 AND ( MONTH(delivery_datetime) = "'.intval(\Input::get('month')).'" AND YEAR(delivery_datetime) = "'.intval(\Input::get('year')).'" ) group by delivery')->execute()->as_array();
        if($results)
        {
            foreach ($results as $result)
            {
                $date_list[] = $result['delivery'];
            }
        }

        // if(isset($results[0]['total']))
        // {
        //     if($results[0]['total'] == 1)
        //         $has_order = true;
        // }

        echo json_encode(array('date_list' => $date_list));
        exit;
    }
    
    public function get_search_orders($user_id = false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \SentryAdmin::user_exists((int)$user_id))
        {
            $user = \SentryAdmin::user((int)$user_id);
        }
        
        if( \Input::get() )
        {
            if( \Input::get('date_from') != "" || \Input::get('date_to') != "" || \Input::get('status') != 'false' )
            {
                $items = \Order\Model_Order::find(function($query){
                    
                    if(isset($user))
                        $query->where('user_id', $user->id);
                    
                    $query->where('finished', '1');
                    $query->order_by('id', 'desc');
                    
                });
            }
        }
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        foreach($items as $number => $item)
                        {
                            $full_name = $item->first_name . ' ' . $item->last_name;
                            if(stripos($item->company, $value) === false && stripos($item->id, $value) === false )
                            {
                                if(stripos($full_name, $value) === false) unset($items[$number]);
                            }
                        }
                        break;
                    case 'email':
                        foreach($items as $number => $item)
                        {
                            if(stripos($item->email, $value) === false) unset($items[$number]);
                        }
                        break;
                 
                    case 'order_total_from':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] < $value) unset($items[$number]);
                        }
                        break;
                    case 'order_total_to':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] > $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', $item->created_at)) < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', $item->created_at)) > $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'status':
                        if(isset($items))
                        {
                            foreach($items as $number => $item)
                            {
                                if($value == 'false') break;
                                if(stripos($item->status, $value) === false) unset($items[$number]);
                            }
                        }
                        break;
                   
                    case 'tracking_no':
                        foreach($items as $number => $item)
                        {
                            if(!$value != '') break;
                            if(stripos($item->tracking_no, $value) === false) unset($items[$number]);
                        }
                        break;
                    
                    case 'payment_method':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(!empty($item->last_payment))
                            {
                                if($item->last_payment->method != $value) 
                                {
                                    unset($items[$number]);
                                }
                            }
                        }
                        break;
                        
                    case 'user_group':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if($item->user_id && \SentryAdmin::user_exists((int)$item->user_id))
                            {
                                $user = \SentryAdmin::user((int)$item->user_id);
                                if ($user->in_group($value)) unset($items[$number]);
                            }
                        }
                        break;
                       
                    case 'country':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'state':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'product_category':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            // Get order products
                            if(!empty($item->products))
                            {
                                $exists = array();
                                foreach ($item->products as $product)
                                {
                                    // Find category
                                    if(\Product\Model_Product_To_Categories::find(array('where' => array('product_id' => $product->product_id, 'category_id' => $value))))
                                    {
                                        $exists[] = $product->id;
                                    }
                                }
                                if(empty($exists)) unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(empty($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_orders()
    {
        $search = $this->get_search_orders();
        $items = $search['items'];
        $output = "Order Date,Client ID,Client Name,Order Amount, Order Status\n";
        $content = 'text/csv';
        $filename = 'Orders-Report-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $output .=  implode(",",[
                date('d/m/Y', $item->created_at).' '.date('H:i:s', $item->created_at),
                $item->user_id,
                $item->first_name . ' ' . $item->last_name,
                '$'.round($item->total_price(), 2),
                \Inflector::humanize($item->status),
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_orders_products()
    {
        \View::set_global('menu', 'admin/reports/orders_products');
        \View::set_global('title', 'Orders x Products Report');

        $search = $this->get_search_orders_by_delivery_date();
        
        $items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        // $items = array_slice($items, $pagination->offset, $pagination->per_page);

        $parent_categories = \Product\Model_Category::find(function($query){

            $query->where('parent_id', 0);
            $query->order_by('sort', 'asc');
            $query->order_by('id', 'asc');
            
        });

        \Theme::instance()->set_partial('content', $this->view_dir . 'orders_products')
            ->set('items', $items)
            ->set('parent_categories', $parent_categories)
            ->set('pagination', $pagination, false);
    }

    public function get_search_orders_by_delivery_date()
    {
        if( \Input::get() )
        {
            if( \Input::get('delivery_date') != "")
            {
                $items = \Order\Model_Order::find(function($query){

                    $query->where('finished', '1');
                    $query->order_by('delivery_datetime', 'asc');
                    
                });
            }
        }
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'delivery_date':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', strtotime($item->delivery_datetime))) != $date) unset($items[$number]);

                                //check if order has removed product orders and unset
                                if($item->products)
                                {
                                    $count = 0;
                                    foreach ($item->products as $product)
                                    {
                                        if(!\Product\Model_Product::find_one_by_id($product->product_id))
                                            $count++;
                                    }
                                    if(count($item->products) == $count) unset($items[$number]);
                                }
                            }
                        }
                        break;
                }
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(empty($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_orders_products()
    {
        $search = $this->get_search_orders_by_delivery_date();
        $items = $search['items'];
        $parent_categories = \Product\Model_Category::find(function($query){

            $query->where('parent_id', 0);
            $query->order_by('sort', 'asc');
            $query->order_by('id', 'asc');
            
        });
        $output = '';
        $content = 'text/csv';
        $filename = 'Orders-x-Products-Report-'.date('YmdHis').' Report.csv';

        if($items)
        {
            $output .= "Delivery Time,";
            foreach($items as $item)
            {
                $output .= $item->delivery_datetime?(date('h:i:s a', strtotime($item->delivery_datetime))):'';
                $output .= ",";
            }
            $output .= "Total\n";

            $output .= "Order No.,";
            foreach($items as $item)
            {
                $output .= $item->id;
                $output .= ",";
            }
            $output .= "\n";

            $output .= "Customer,";
            foreach($items as $item)
            {
                $output .= $item->first_name . ' ' . $item->last_name;
                $output .= ",";
            }
            $output .= "\n";

            foreach($parent_categories as $category)
            {
                if($all_ordered_products = $category->get_categories_ordered_products($items, $category))
                {
                    $hold_product_list_td = '';
                    foreach($all_ordered_products as $product): $line_total_quantity = 0;

                        // if product has more than one category only show in first category
                        if(count($product->categories) > 1)
                        {
                            $arrayKeys = array_keys($product->categories);
                            if($product->categories[$arrayKeys[0]]->id != $category->id)
                                continue;
                        }

                        $hold_product_list_td .= $product->title.',';

                        foreach($items as $item):
                            $quantity = $item->in_order_get_product_quantity_by_id($item->id, $product->id);
                            $line_total_quantity += $quantity;

                            if($quantity)
                                $hold_product_list_td .= $quantity;
                            $hold_product_list_td .= ',';
                        endforeach;

                        if($line_total_quantity)
                            $hold_product_list_td .= $line_total_quantity;

                        $hold_product_list_td .= "\n";
                    endforeach;

                    if($hold_product_list_td)
                    {
                        $output .= $category->title."\n";
                        $output .= $hold_product_list_td;
                    }
                }
            }

            $output .= "Total,";
            $over_all_total = 0;
            foreach($items as $item)
            {
                $total = $item->order_info_active_products($item->id)['quantity'];
                $over_all_total += $total;
                $output .= $total.",";
            }
            $output .= $over_all_total."\n";
        }
        else
        {
            $output .= "Delivery Time,Total\n";
            $output .= "Order No.\n";
            $output .= "Customer\n";
        }

        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers); 
    }

    public function action_export_orders_products_notes()
    {
        $search = $this->get_search_orders_by_delivery_date();
        $items = $search['items'];

        $output = "Customer,Company,Order No.,Notes\n";
        $content = 'text/csv';
        $filename = 'Orders-x-Products-Notes-Report-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $output .=  implode(",",[
                $item->first_name . ' ' . $item->last_name,
                $item->company,
                $item->id,
                $item->notes,
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_orders_products_print()
    {
        $delivery_date = \Input::get('delivery_date')?'- Delivery Date - '.str_replace('/', '-', \Input::get('delivery_date')):'';

        \Package::load('Pdf');
        $pdf = \Pdf\Pdf::forge('tcpdf')->init('L', 'mm', 'A4', true, 'UTF-8', false);
        $settings = \Config::load('autoresponder.db');
                
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($settings['company_name']);

        $pdf->SetTitle('Orders x Products Report '.$delivery_date);
        $pdf->SetSubject('Orders x Products Report '.$delivery_date);

        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(5, 10, 10);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
      
        // set font
        //$pdf->SetFont('times', 'BI', 20);

        // add a page
        $pdf->AddPage();

        $search = $this->get_search_orders_by_delivery_date();
        
        $items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();

        $parent_categories = \Product\Model_Category::find(function($query){

            $query->where('parent_id', 0);
            $query->order_by('sort', 'asc');
            $query->order_by('id', 'asc');
            
        });

        $content['content']['items'] = $items;
        $content['content']['parent_categories'] = $parent_categories;

        $html = \Theme::instance()->view($this->view_dir . 'print/orders_products', $content, false);

        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        // ---------------------------------------------------------

        ob_end_clean();

        //Close and output PDF document
        $pdf->Output('Orders x Products Report '.$delivery_date.'.pdf', 'I');
    }

    public function action_customer_orders()
    {

        \View::set_global('menu', 'admin/reports/customer_orders');
        \View::set_global('title', 'Customer x Orders Report');

        $search = $this->get_search_customer_orders();
        
        $items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        // $items = array_slice($items, $pagination->offset, $pagination->per_page);

        \Theme::instance()->set_partial('content', $this->view_dir . 'customer_orders')
            ->set('items', $items)
            ->set('pagination', $pagination, false);
    }
    
    public function get_search_customer_orders($user_id = false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \SentryAdmin::user_exists((int)$user_id))
        {
            $user = \SentryAdmin::user((int)$user_id);
        }

        if(\Input::get())
        {        
            if( ( \Input::get('date_from') != "" || \Input::get('date_to') != "" || \Input::get('status') != 'false' || \Input::get('title') != 'customer_order_select' ) )
            {
                $items = \Order\Model_Order::find(function($query){
                    
                    if(isset($user))
                        $query->where('user_id', $user->id);

                    //get customer only -- start
                    $customer_list = \SentryAdmin::user()->all('front');
                    $customer_id_list = array();
                    foreach ($customer_list as $customer) {
                        array_push($customer_id_list, $customer['id']);
                    }

                    if($customer_id_list)
                        $query->where('user_id', 'in', $customer_id_list);
                    //get customer only -- end
                    
                    $query->where('finished', '1');
                    $query->order_by('id', 'desc');
                    
                });
            }
        }
        
        if(isset($items))
        {
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title':
                            if($value != 'customer_order_select')
                            {
                                foreach($items as $number => $item)
                                {
                                    $full_name = $item->first_name . ' ' . $item->last_name;
                                    if(stripos($item->company, $value) === false && stripos($item->id, $value) === false )
                                    {
                                        if(stripos($full_name, $value) === false) unset($items[$number]);
                                    }
                                }
                            }
                            break;
                        case 'email':
                            foreach($items as $number => $item)
                            {
                                if(stripos($item->email, $value) === false) unset($items[$number]);
                            }
                            break;
                     
                        case 'order_total_from':
                            is_numeric($value) or $value == 0;
                            foreach($items as $number => $item)
                            {
                                $item_details = \Order\Model_Order::order_info($item->id);
                                if(isset($item_details['total_price']) && $item_details['total_price'] < $value) unset($items[$number]);
                            }
                            break;
                        case 'order_total_to':
                            is_numeric($value) or $value == 0;
                            foreach($items as $number => $item)
                            {
                                $item_details = \Order\Model_Order::order_info($item->id);
                                if(isset($item_details['total_price']) && $item_details['total_price'] > $value) unset($items[$number]);
                            }
                            break;
                        case 'date_from':
                            // convert format date to m/d/Y
                            $parts = explode('/', $value);
                            $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                            if($date = strtotime($value))
                            {
                                foreach($items as $number => $item)
                                {
                                    if(strtotime(date('m/d/Y', $item->created_at)) < $date) unset($items[$number]);
                                }
                            }
                            break;
                        case 'date_to':
                            // convert format date to m/d/Y
                            $parts = explode('/', $value);
                            $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                            if($date = strtotime($value))
                            {
                                foreach($items as $number => $item)
                                {
                                    if(strtotime(date('m/d/Y', $item->created_at)) > $date) unset($items[$number]);
                                }
                            }
                            break;
                        case 'status':
                            foreach($items as $number => $item)
                            {
                                if($value == 'false') break;
                                if(stripos($item->status, $value) === false) unset($items[$number]);
                            }
                            break;
                       
                        case 'tracking_no':
                            foreach($items as $number => $item)
                            {
                                if(!$value != '') break;
                                if(stripos($item->tracking_no, $value) === false) unset($items[$number]);
                            }
                            break;
                        
                        case 'payment_method':
                            foreach($items as $number => $item)
                            {
                                if($value == 'false') break;
                                if(!empty($item->last_payment))
                                {
                                    if($item->last_payment->method != $value) 
                                    {
                                        unset($items[$number]);
                                    }
                                }
                            }
                            break;
                            
                        case 'user_group':
                            foreach($items as $number => $item)
                            {
                                if($value == 'false') break;
                                if($item->user_id && \SentryAdmin::user_exists((int)$item->user_id))
                                {
                                    $user = \SentryAdmin::user((int)$item->user_id);
                                    if ($user->in_group($value)) unset($items[$number]);
                                }
                            }
                            break;
                           
                        case 'country':
                            foreach($items as $number => $item)
                            {
                                if($value == 'false') break;
                                if(stripos($item->country, $value) === false) unset($items[$number]);
                            }
                            break;
                            
                        case 'state':
                            foreach($items as $number => $item)
                            {
                                if($value == 'false') break;
                                if(stripos($item->country, $value) === false) unset($items[$number]);
                            }
                            break;
                            
                        case 'product_category':
                            foreach($items as $number => $item)
                            {
                                if($value == 'false') break;
                                // Get order products
                                if(!empty($item->products))
                                {
                                    $exists = array();
                                    foreach ($item->products as $product)
                                    {
                                        // Find category
                                        if(\Product\Model_Product_To_Categories::find(array('where' => array('product_id' => $product->product_id, 'category_id' => $value))))
                                        {
                                            $exists[] = $product->id;
                                        }
                                    }
                                    if(empty($exists)) unset($items[$number]);
                                }
                            }
                            break;
                    }
                }
            }
        }
		
		// Reset to empty array if there are no result found by query
		if(empty($items)) $items = array();
		
        return array('items' => $items);
    }

    public function action_export_customer_orders()
    {
        $search = $this->get_search_customer_orders();
        $items = $search['items'];

        $output = "Order Date,Order Amount,Order Status,Payment Status\n";
        $content = 'text/csv';
        $filename = 'Customer-x-Orders-Report-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $output .=  implode(",",[
                date('d/m/Y', $item->created_at).' '.date('H:i:s', $item->created_at),
                '$'.round($item->total_price(), 2),
                \Inflector::humanize($item->status),
                isset($item->last_payment->status_detail) ?  $item->last_payment->status_detail : '',
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

	public function action_products()
	{

	    \View::set_global('menu', 'admin/reports/products');
	    \View::set_global('title', 'Product Report');

        $search = $this->get_search_products();
        
        $items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        // $items = array_slice($items, $pagination->offset, $pagination->per_page);


		\Theme::instance()->set_partial('content', $this->view_dir . 'products')
            ->set('items', $items)
            ->set('pagination', $pagination, false);
	}
    
    public function get_search_products($category_id = false)
    {
        // Override category_id if its a search
        $category_id = \Input::get('category_id', $category_id);
        
        $product_ids = array();
        
        // If we are viewing category products
        // We need to find all products from that and child categories
        if(\Input::get())
        {
            if(\Input::get('status') != 'select')
            {
                if(is_numeric($category_id))
                {
                    $category = \Product\Model_Category::find_one_by_id($category_id);
                    if($category)
                    { 

                        \View::set_global('menu', 'dfdfdf');
                        \View::set_global('category', $category);
                        if($category->all_products)
                        {
                            $product_ids = array_keys($category->all_products);
                        }
                    }
                }
            }
        }
        
        // If we are filtering products by category and there is nothing found
        // We return empty array of products without even going to database
        if(empty($product_ids) && is_numeric($category_id)) 
        { 
            $items = array();
        }
        else
        {
            /************ Start generating query ***********/

            if(\Input::get())
            {
                if(\Input::get('status') != 'select')
                {
                    $items = \Product\Model_Product::find(function($query) use ($product_ids){

                        if(!empty($product_ids))
                        {
                            $query->where('id', 'in', $product_ids);
                        }

                        // Get search filters
                        foreach(\Input::get() as $key => $value)
                        {
                            if(!empty($value) || $value == '0')
                            {
                                switch($key)
                                {
                                    case 'title':
                                        $query->where($key, 'like', "%$value%")
                                            ->or_where('code', 'like', "%$value%");
                                        break;
                                    case 'status':
                                        if(is_numeric($value))
                                            $query->where($key, $value);
                                        break;
                                    case 'active_from':
                                        $date = strtotime($value);
                                        if($date)
                                            $query->where($key, '>=', $date);
                                        break;
                                    case 'active_to':
                                        $date = strtotime($value);
                                        if($date)
                                            $query->where($key, '<=', $date);
                                        break;
                                }
                            }
                        }

                        // Order query
                        $query->order_by('sort', 'asc');
                        $query->order_by('id', 'asc');
                    });
                }
            }
        }
        
        /************ End generating query ***********/
        
        // Reset to empty array if there are no result found by query
        if(empty($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_products()
    {
        $status = array(
            'false' => 'All',
            '1' => 'Active',
            '0' => 'Inactive',
        );

        $search = $this->get_search_products();
        $items = $search['items'];

        $output = "Product Code,Product Name,Attribute Group,Attribute,Status,Stock\n";
        $content = 'text/csv';
        $filename = 'Products-Report-'.date('YmdHis').' Report.csv';


        foreach ($items as $item) {
            $check_attribute = \DB::query('SELECT * FROM product_attributes a where a.product_id = '.$item->id)->execute();
            if(count($check_attribute)==1 && $check_attribute[0]['attribute_group_id']==0)
            {
                $hold_stock = \DB::query('SELECT b.id, b.stock_quantity FROM product a join product_attributes b on a.id = b.product_id where a.id = '.$item->id)->execute();

                // If page is active from certain date
                $get_status = '';
                if($item->status == 2)
                {
                    $dates = array();
                    !is_null($item->active_from) and array_push($dates, date('m/d/Y', $item->active_from));
                    !is_null($item->active_to) and array_push($dates, date('m/d/Y', $item->active_to));

                    if(true)
                    {
                        $get_status = 'Active '.implode(' - ', $dates);
                    }
                }
                else
                {
                    $get_status = $status[$item->status];
                }

                $output .=  implode(",",[
                    $item->code,
                    $item->title,
                    $item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A',
                    $item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A',
                    $get_status,
                    $hold_stock[0]['stock_quantity'],
                ]). "\n";
            }
            else
            {
                foreach ($check_attribute as $key => $value)
                {
                    $get_attribute = '';
                    $attribute_list = (json_decode($value['attributes'], TRUE)); 
                    $attribute_list_hold = array();

                    foreach ($attribute_list as $key => $value1) {
                        $attribute_list_hold[] = \DB::query('SELECT a.title FROM attribute_options a where a.attribute_id = '.$key.' and a.id = '.$value1)->execute();
                    }

                    $count_attr = count($attribute_list_hold);
                    $count_attr_count = 0;
                    foreach ($attribute_list_hold as $key => $value3) {
                        foreach ($value3[0] as $key => $value2) {
                            $get_attribute .= $value2;
                            if($count_attr-1 != $count_attr_count)
                                $get_attribute .= " | ";
                            $count_attr_count++;
                        }
                    }

                    $get_status = '';
                    // If page is active from certain date
                    if($item->status == 2)
                    {
                        $dates = array();
                        !is_null($item->active_from) and array_push($dates, date('m/d/Y', $item->active_from));
                        !is_null($item->active_to) and array_push($dates, date('m/d/Y', $item->active_to));

                        if(true)
                        {
                            $get_status = 'Active '.implode(' - ', $dates);
                        }
                    }
                    else
                    {
                        $get_status = $status[$item->status];
                    }

                    $output .=  implode(",",[
                        $value['product_code'],
                        $item->title,
                        ($item->active_attribute_group ? $item->active_attribute_group[0]->title : 'N/A'),
                        $get_attribute, 
                        $get_status,
                        $value['stock_quantity'],
                    ]). "\n";
                }
            }
        }

        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_customer_products()
    {

        \View::set_global('menu', 'admin/reports/customer_products');
        \View::set_global('title', 'Customer x Products Report');

        $search = $this->get_search_customer_products();
        
        $items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        // $items = array_slice($items, $pagination->offset, $pagination->per_page);


        \Theme::instance()->set_partial('content', $this->view_dir . 'customer_products')
            ->set('items', $items)
            ->set('pagination', $pagination, false);
    }
    
    public function get_search_customer_products($user_id = false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \SentryAdmin::user_exists((int)$user_id))
        {
            $user = \SentryAdmin::user((int)$user_id);
        }
        
        $items = \Order\Model_Order::find(function($query){
            
            if(isset($user))
                $query->where('user_id', $user->id);

            //get customer only -- start
            $customer_list = \SentryAdmin::user()->all('front');
            $customer_id_list = array();
            foreach ($customer_list as $customer) {
                array_push($customer_id_list, $customer['id']);
            }

            if($customer_id_list)
                $query->where('user_id', 'in', $customer_id_list);
            //get customer only -- end
            
            $query->where('finished', '1');
            $query->order_by('id', 'desc');
            
        });
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        if($value != 'customer_order_select')
                        {
                            foreach($items as $number => $item)
                            {
                                $full_name = $item->first_name . ' ' . $item->last_name;
                                if(stripos($item->company, $value) === false && stripos($item->id, $value) === false )
                                {
                                    if(stripos($full_name, $value) === false) unset($items[$number]);
                                }
                            }
                        }
                        break;
                    case 'email':
                        foreach($items as $number => $item)
                        {
                            if(stripos($item->email, $value) === false) unset($items[$number]);
                        }
                        break;
                 
                    case 'order_total_from':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] < $value) unset($items[$number]);
                        }
                        break;
                    case 'order_total_to':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_price']) && $item_details['total_price'] > $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if($item->created_at > $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->status, $value) === false) unset($items[$number]);
                        }
                        break;
                   
                    case 'tracking_no':
                        foreach($items as $number => $item)
                        {
                            if(!$value != '') break;
                            if(stripos($item->tracking_no, $value) === false) unset($items[$number]);
                        }
                        break;
                    
                    case 'payment_method':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(!empty($item->last_payment))
                            {
                                if($item->last_payment->method != $value) 
                                {
                                    unset($items[$number]);
                                }
                            }
                        }
                        break;
                        
                    case 'user_group':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if($item->user_id && \SentryAdmin::user_exists((int)$item->user_id))
                            {
                                $user = \SentryAdmin::user((int)$item->user_id);
                                if ($user->in_group($value)) unset($items[$number]);
                            }
                        }
                        break;
                       
                    case 'country':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'state':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'product_category':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            // Get order products
                            if(!empty($item->products))
                            {
                                $exists = array();
                                foreach ($item->products as $product)
                                {
                                    // Find category
                                    if(\Product\Model_Product_To_Categories::find(array('where' => array('product_id' => $product->product_id, 'category_id' => $value))))
                                    {
                                        $exists[] = $product->id;
                                    }
                                }
                                if(empty($exists)) unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(empty($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_customer_products()
    {
        $status = array(
            'false' => 'All',
            '1' => 'Active',
            '0' => 'Inactive',
        );
        $search = $this->get_search_customer_products();
        $items = $search['items'];

        $output = "Product Code,Product Name,Attribute Group,Status,Stock,Price Paid\n";
        $content = 'text/csv';
        $filename = 'Customer-x-Products-Report-'.date('YmdHis').' Report.csv';

        if( !is_null(\Input::get('title')) && \Input::get('title') != 'customer_order_select')
        {
            foreach ($items as $item) {
                foreach($item->products as $product)
                {
                    if($prdct = \Product\Model_Product::find_one_by_id($product->product_id))
                    {
                        $hold_stock = \DB::query('SELECT b.id, b.stock_quantity FROM product a join product_attributes b on a.id = b.product_id where a.id = '.$prdct->id)->execute();
                        $get_status = '';
                        // If page is active from certain date
                        if($prdct->status == 2)
                        {
                            $dates = array();
                            !is_null($prdct->active_from) and array_push($dates, date('m/d/Y', $prdct->active_from));
                            !is_null($prdct->active_to) and array_push($dates, date('m/d/Y', $prdct->active_to));

                            if(true)
                            {
                                $get_status = 'Active '.implode(' - ', $dates);
                            }
                        }
                        else
                        {
                            $get_status = $status[$prdct->status];
                        }

                        $output .=  implode(",",[
                            $product->code,
                            $product->title,
                            $prdct->active_attribute_group ? $prdct->active_attribute_group[0]->title : 'N/A',
                            $get_status,
                            $hold_stock[0]['stock_quantity'],
                            '$'.$product->price,
                        ]). "\n"; 
                    }
                    else
                    {
                        $output .=  implode(",",[
                            $product->code,
                            $product->title,
                            'N/A',
                            '',
                            '',
                            '$'.$product->price,
                        ]). "\n"; 
                    }  
                }
            }
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

	public function action_customers()
	{

	    \View::set_global('menu', 'admin/reports/customers');
	    \View::set_global('title', 'Customers Report');

        $search = $this->get_search_customers();
        
        $items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        // $items = array_slice($items, $pagination->offset, $pagination->per_page);

		\Theme::instance()->set_partial('content', $this->view_dir . 'customers')
            ->set('items', $items)
            ->set('pagination', $pagination, false);
	}
    
    public function get_search_customers($group_id = false)
    {
        // Override group_id if its a search
        $group_id = \Input::get('user_group', $group_id);

        $activated = \Input::get('activated', false);


        if(\Input::get())
        {
            if($group_id && \SentryAdmin::group_exists((int)$group_id))
            {
                // Get only group users

                \View::set_global('menu', 'dfdfdf');
                \View::set_global('group', \SentryAdmin::group((int)$group_id));
                $items = \SentryAdmin::group((int)$group_id)->users();
            }
            else
            {
                // Get all users and remove admin users from array
                $items = \SentryAdmin::user()->all('front');
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(empty($items)) $items = array();
        
        // Get user objects
        if(!empty($items))
        {
            foreach($items as $key=> $item)
            {
                $items[$key] = \SentryAdmin::user((int)$item['id']);
            }
            
            // Get search filters
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title': 
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                $full_name = $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name');
                                $customer_id = $item->get('metadata.user_id');

                                if(stripos($full_name, $value) === false && stripos($customer_id, $value) === false ) unset($items[$number]);
                            }
                            break;
                        case 'email':
                            foreach($items as $number => $item)
                            {
                                if(stripos($item->email, $value) === false) unset($items[$number]);
                            }
                            break;
                        case 'country':
                            if($value && $value !== 'false')
                            {
                                foreach($items as $number => $item)
                                {
                                    if(empty($item['metadata'])) 
                                    {
                                        unset($items[$number]);
                                        continue;
                                    }
                                    if(stripos($item->get('metadata.country'), $value) === false) unset($items[$number]);
                                }
                            }
                            break;
                        case 'postcode_from':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') < $value) unset($items[$number]);
                            }
                            break;
                        case 'postcode_to':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') > $value) unset($items[$number]);
                            }
                            break;
                        case 'activated':
                            if($value !== 'false')
                            {
                                foreach($items as $number => $item)
                                {
                                    if($item->activated != $value) unset($items[$number]);
                                }
                            }
                            break;
                    }
                }
            }
        }

        // Reset to empty array if there are no result found by query
        if(empty($items)) $items = array();
        
        return array('items' => $items);
    }

    public function action_export_customers()
    {
        $search = $this->get_search_customers();
        $items = $search['items'];

        $output = "Customer ID,Customer Name,Company Name,Customer Group,Email,Phone,State,Activated,Account Created\n";
        $content = 'text/csv';
        $filename = 'Customers-Report-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $user_groups = $item->groups();
            $user_group = current($user_groups);
            $output .=  implode(",",[
                ($item->metadata) ? $item->get('metadata.user_id') : '',
                ($item->metadata) ? $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name') : '',
                ($item->metadata) ? $item->get('metadata.business_name') : '',
                $user_group['name'],
                $item->get('email'),
                ($item->metadata) ? $item->get('metadata.phone') : '',
                ($item->metadata) ? $item->get('metadata.state') : '',
                $item->get('activated') == 1 ? 'Yes' : 'No',
                date('d/m/Y', $item->created_at),
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_order_notes()
    {
        \View::set_global('menu', 'admin/reports/order_notes');
        \View::set_global('title', 'Order Notes Report');

        $search = $this->get_search_order_notes();
        
        $items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        // $items = array_slice($items, $pagination->offset, $pagination->per_page);

        \Theme::instance()->set_partial('content', $this->view_dir . 'order_notes')
            ->set('items', $items)
            ->set('pagination', $pagination, false);
    }

    public function action_print(){
        \View::set_global('menu', 'admin/reports/print');
        \View::set_global('title', 'Print Report');

        $search = $this->get_search_order_notes();
        
        $items = $search['items'];

        if(is_null($items)) $items = array();

        // Reset to empty array if there are no result found by query
        \Theme::instance()->set_partial('content', $this->view_dir . 'print')
        ->set('items', $items);
    
        

        if($items){
            \Package::load('Pdf');    
            $pdf = \Pdf\Pdf::forge('tcpdf')->init('P', 'mm', 'A4', true, 'UTF-8', false);
            $settings = \Config::load('autoresponder.db');
                    
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor($settings['company_name']);

            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5, 10, 10);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
          
            // set font
            //$pdf->SetFont('times', 'BI', 20);

            // add a page
            $pdf->AddPage();

            $content['content']['content'] = $items;
            
            if( \Input::get('type') == 'delivery_notes'){
                 $pdf->SetTitle('Delivery Notes');
                 $html = \Theme::instance()->view('views/reports/print/delivery-note', $content, false);
            }
            else{
                $pdf->SetTitle('Order Notes');
                $html = \Theme::instance()->view('views/reports/print/order-note', $content, false);
            }
           

            $pdf->writeHTML($html, true, false, true, false, '');

            // ---------------------------------------------------------

            ob_end_clean();

            //Close and output PDF document
            $pdf->Output('Order_notes.pdf', 'I');
        }
    }

    public function get_search_order_notes()
    {
        if( \Input::get() )
        {
            if( \Input::get('user_id') != "" || \Input::get('date_from') != "" || \Input::get('date_to') != "")
            {
                $items = \Order\Model_Order::find(function($query){

                    $query->where('finished', '1');
                    $query->order_by('id', 'asc');
                    
                });
            }
        }
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'user_id': 
                        if($value == 'all') break;

                        foreach($items as $number => $item)
                        {
                            if($item->user_id != $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', $item->created_at)) < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', $item->created_at)) > $date) unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(empty($items)) $items = array();
        
        return array('items' => $items);
    }

    

    public function action_export_order_notes()
    {
        $search = $this->get_search_order_notes();
        $items = $search['items'];

        $output = "Customer,Company,Order No.,Notes\n";
        $content = 'text/csv';
        $filename = 'Order-Notes-Report-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $output .=  implode(",",[
                $item->first_name . ' ' . $item->last_name,
                $item->company,
                $item->id,
                $item->notes,
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

	public function action_sales_by_month()
	{

	    \View::set_global('menu', 'admin/reports/sales_by_month');
	    \View::set_global('title', 'Sales By Year Report');

        $search = $this->get_search_sales_by_month();

        $items      = $search['items'];
        $years      = $search['years'];


		\Theme::instance()->set_partial('content', $this->view_dir . 'sales_by_month')
            ->set('items', $items)
            ->set('years', $years);
	}

    public function get_search_sales_by_month()
    {
        $settings = \Config::load('autoresponder.db');
        $hold_gst = (isset($settings['gst']) ? 1 : 0);
        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);
        $years = \Order\Model_Order::find(array(
            'where' => array(
                array('total_price', 'is not', NULL),
            )
        ));

        $year_list = array();
        $month_list = array(); 
        if($years)
        {
            foreach ($years as $year) {
                $year = date('Y', $year->created_at);

                if (!in_array($year, $year_list))
                    $year_list += array($year => $year);
            } 
        }

        $get_first_year = $year_list;
        rsort($get_first_year);

        krsort($year_list);
        if(\Input::get())
        {
            if(\Input::get('year')!='select')
            {
                $choose_year = \Input::get('year') ? \Input::get('year') : $get_first_year[0];

                for ($i=1; $i<=12; $i++) 
                { 
                    $timestamp = mktime(0,0,1,$i,1,date("Y"));  
                    $month = date("F", $timestamp); 
                    $results = \DB::query('SELECT count(*) as num_orders, sum(total_price) as total_amount from orders where year(FROM_UNIXTIME(created_at)) = '.$choose_year.' and month(FROM_UNIXTIME(created_at)) = '.$i)->execute();
                    $gst = ($results[0]['total_amount']) ?\Helper::calculateGST($results[0]['total_amount'], $hold_gst_percentage) : 0;
                    $without_gst = ($results[0]['total_amount']) ? $results[0]['total_amount'] - $gst : 0;
                    array_push($month_list, array('month' => array('num_orders' => $results[0]['num_orders'], 'name' => $month, 'amount' => number_format($results[0]['total_amount'], 2),'without_gst' => $without_gst,'gst' => number_format($gst,2))));
                }
            }
        }

        return array('items' => $month_list, 'years' => $year_list);
    }

    public function action_export_sales_by_month()
    {
        $settings = \Config::load('autoresponder.db');
        $hold_gst = (isset($settings['gst']) ? 1 : 0);
        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

        $search = $this->get_search_sales_by_month();
        $items = $search['items'];

        $output = "Month,Number of Orders,Total Sales (without gst),Total Sales (with gst),GST ".$hold_gst_percentage."% (Included)\n";
        $content = 'text/csv';
        $filename = 'Sales-x-Year-Report-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $item['month']['amount'] = str_replace(',', '', $item['month']['amount']);

            $output .=  implode(",",[
                $item['month']['name'],
                $item['month']['num_orders'],
                '$'.str_replace(',', '', $item['month']['without_gst']),
                '$'.($item['month']['amount'] ? $item['month']['amount'] : '0'),
                '$'.str_replace(',', '', $item['month']['gst']),
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_sales_by_day()
    {

        \View::set_global('menu', 'admin/reports/sales_by_day');
        \View::set_global('title', 'Sales By Date Report');

        $search = $this->get_search_sales_by_date();

        $items      = $search['items'];

        \Theme::instance()->set_partial('content', $this->view_dir . 'sales_by_date')
            ->set('items', $items);
    }

    public function get_search_sales_by_date()
    {
        $items = array();
        if(\Input::get('date_from') != "" && \Input::get('date_to') != "")
        {
            // convert format date to m/d/Y
            $parts_from = explode('/', \Input::get('date_from'));
            $date_from = $parts_from[1].'-'.$parts_from[0].'-'.$parts_from[2];

            // convert format date to m/d/Y
            $parts_to = explode('/', \Input::get('date_to'));
            $date_to = $parts_to[1].'-'.$parts_to[0].'-'.$parts_to[2];

            $items = \DB::query("SELECT STR_TO_DATE(concat(month(delivery_datetime),'-',day(delivery_datetime),'-',year(delivery_datetime)), '%m-%d-%Y') as dt, finished FROM `orders` WHERE `finished` = '1' AND (STR_TO_DATE(concat(month(delivery_datetime),'-',day(delivery_datetime),'-',year(delivery_datetime)), '%m-%d-%Y') >= STR_TO_DATE('".$date_from."', '%m-%d-%Y') AND STR_TO_DATE(concat(month(delivery_datetime),'-',day(delivery_datetime),'-',year(delivery_datetime)), '%m-%d-%Y') <= STR_TO_DATE('".$date_to."', '%m-%d-%Y'))  GROUP BY dt ORDER BY dt ASC")->execute();
            
            // old process to show date from, date to and total amount results
            /*
            $result = \DB::query('SELECT sum(total_price) as total_price from orders where UNIX_TIMESTAMP(FROM_UNIXTIME(created_at, "%Y-%m-%d")) >= '.strtotime($date_from).' and UNIX_TIMESTAMP(FROM_UNIXTIME(created_at, "%Y-%m-%d")) <= '.strtotime($date_to))->execute();
            $date = array('amount'=>  $result[0]['total_price']);
            */
        }
        return array('items' => $items);
    }

    public function action_export_sales_by_day()
    {
        $settings = \Config::load('autoresponder.db');
        $hold_gst = (isset($settings['gst']) ? 1 : 0);
        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

        $search = $this->get_search_sales_by_date();
        $items = $search['items'];

        $output = "Date, Number of Orders,Total Sales (without gst),Total Sales (with gst),GST ".$hold_gst_percentage."% (Included)\n";
        $content = 'text/csv';
        $filename = 'Sales-x-Day-Report-'.date('YmdHis').' Report.csv';

        if($items)
        {
            foreach($items as $item)
            {
                $number_of_orders = 0;
                $total_sales = 0;
                $gst = 0;

                $orders = \Order\Model_Order::find(function($query){
                    $query->where('finished', '1');
                    $query->order_by('delivery_datetime', 'asc');
                });

                if($orders)
                {
                    // convert format date to m/d/Y
                    $parts = explode('/', date('d/m/Y', strtotime($item['dt'])));
                    $value = $parts[1].'/'.$parts[0].'/'.$parts[2];
                    if($date = strtotime($value))
                    {
                        foreach($orders as $number => $ord)
                        {
                            if(strtotime(date('m/d/Y', strtotime($ord->delivery_datetime))) == $date)
                            {
                                $number_of_orders += 1;
                                $total_sales += $ord->total_price();
                            }
                        }
                        $gst = \Helper::calculateGST($total_sales, $hold_gst_percentage);
                    }
                }
                $output .= date('d/m/Y', strtotime($item['dt'])).",".$number_of_orders.",$".str_replace(',', '', ($total_sales - $gst)).",$".str_replace(',', '', $total_sales).",$".str_replace(',', '', $gst)."\n";
            }
        }

        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_sales_by_customer()
    {

        \View::set_global('menu', 'admin/reports/sales_by_customer');
        \View::set_global('title', 'Sales By Customer');

        $search = $this->get_search_sales_by_customer();

        $sales_customer      = $search['items'];

        \Theme::instance()->set_partial('content', $this->view_dir . 'sales_by_customer')
            ->set('sales_customer', $sales_customer);
    }

    public function get_search_sales_by_customer()
    {
        $items = array();

        if(\Input::get('user_id') != "")
        {
            $total_amount_query = "(SELECT sum(total_price) as total_amount from orders where orders.user_id = users.id and orders.finished = 1) as total_amount";
            $total_order_query = "(SELECT count(orders.id) as total_order from orders where orders.user_id = users.id and orders.finished = 1) as total_order";
            if(\Input::get('user_id') == 'all')
                $query = "SELECT users.id, users_metadata.first_name, users_metadata.last_name, $total_order_query, $total_amount_query FROM `users` join users_groups on users_groups.user_id = users.id join groups on groups.id = users_groups.group_id join users_metadata on users.id = users_metadata.user_id WHERE groups.is_admin = 0";
            else
                $query = "SELECT users.id, users_metadata.first_name, users_metadata.last_name, $total_order_query, $total_amount_query FROM `users` join users_groups on users_groups.user_id = users.id join groups on groups.id = users_groups.group_id join users_metadata on users.id = users_metadata.user_id WHERE groups.is_admin = 0 AND users.id = ".\Input::get('user_id');

            $items = \DB::query($query)->execute();
        }
        
        return array('items' => $items);
    }

    public function action_export_sales_by_customer()
    {
        $search = $this->get_search_sales_by_customer();
        $items = $search['items'];

        $output = "Customer,Total Number of Order,Total Amount of Order\n";
        $content = 'text/csv';
        $filename = 'Sales-x-Customer-Report-'.date('YmdHis').' Report.csv';

        foreach ($items as $item) {
            $item['total_amount'] = str_replace(',', '', $item['total_amount']);

            $output .=  implode(",",[
                $item['first_name'].' '.$item['last_name'],
                $item['total_order'],
                '$'.($item['total_amount'] ? $item['total_amount'] : '0'),
            ]). "\n";
        }
        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }

    public function action_sales_by_item()
    {
        \View::set_global('menu', 'admin/reports/sales_by_item');
        \View::set_global('title', 'Sales By Item');

        $search = $this->get_search_sales_by_item();

        $items      = $search['items'];

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();

        $parent_categories = \Product\Model_Category::find(function($query){

            $query->where('parent_id', 0);
            $query->order_by('title', 'asc');
            
        });

        \Theme::instance()->set_partial('content', $this->view_dir . 'sales_by_item')
            ->set('items', $items)
            ->set('parent_categories', $parent_categories);
    }

    public function get_search_sales_by_item()
    {
        $items = array();
        if(\Input::get('date_from') != "" || \Input::get('date_to') != "")
        {
            $items = \Order\Model_Order::find(function($query){
                $query->where('finished', '1');
            });

            if(isset($items))
            {
                foreach(\Input::get() as $key => $value)
                {
                    if(!empty($value) || $value == '0')
                    {
                        switch($key)
                        {
                            case 'date_from':
                                // convert format date to m/d/Y
                                $parts = explode('/', $value);
                                $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                                if($date = strtotime($value))
                                {
                                    foreach($items as $number => $item)
                                    {
                                        if(strtotime(date('m/d/Y', strtotime($item->delivery_datetime))) < $date) unset($items[$number]);
                                    }
                                }
                                break;
                            case 'date_to':
                                // convert format date to m/d/Y
                                $parts = explode('/', $value);
                                $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                                if($date = strtotime($value))
                                {
                                    foreach($items as $number => $item)
                                    {
                                        if(strtotime(date('m/d/Y', strtotime($item->delivery_datetime))) > $date) unset($items[$number]);
                                    }
                                }
                                break;
                        }
                    }
                }
            }

            // get product ids belong to the range of date
        }
        return array('items' => $items);
    }

    public function action_export_sales_by_item()
    {
        $settings = \Config::load('autoresponder.db');
        $hold_gst = (isset($settings['gst']) ? 1 : 0);
        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

        $search = $this->get_search_sales_by_item();
        $items = $search['items'];
        $parent_categories = \Product\Model_Category::find(function($query){

            $query->where('parent_id', 0);
            $query->order_by('title', 'asc');
            
        });
        $output = '';
        $content = 'text/csv';
        $filename = 'Sales-x-Item-Report-'.date('YmdHis').' Report.csv';

        $output .= "Item,Number of Sold,Total Sales,GST ".$hold_gst_percentage."% (Included)\n";

        if($items)
        {
            foreach($parent_categories as $category)
            {
                if($all_ordered_products = $category->get_categories_ordered_products($items, $category))
                {
                    $hold_product_list_td = '';
                    foreach($all_ordered_products as $product): $line_total_quantity = 0; $line_total_sales = 0; $final_gst = 0;
                        // if product has more than one category only show in first category
                        if(count($product->categories) > 1)
                        {
                            $arrayKeys = array_keys($product->categories);
                            if(!in_array($category->id, $arrayKeys))
                                continue;
                        }

                        $hold_product_list_td .= $product->title.',';

                        foreach($items as $item):
                            $quantity = $item->in_order_get_product_quantity_by_id($item->id, $product->id);
                            $price = $item->in_order_get_product_price_by_id($item->id, $product->id);
                            $line_total_quantity += $quantity;
                            $line_total_sales += $price;
                        endforeach;

                        $hold_product_list_td .= $line_total_quantity.',';
                        $hold_product_list_td .= $line_total_sales.',';
                        
                        if($hold_gst && $hold_gst_percentage > 0 && $line_total_sales > 0)
                            $final_gst = \Helper::calculateGST($line_total_sales, $hold_gst_percentage);

                        $hold_product_list_td .= $final_gst;
                        $hold_product_list_td .= "\n";
                    endforeach;

                    if($hold_product_list_td)
                        $output .= $category->title."\n";
                    $output .= $hold_product_list_td;
                }
            }
        }

        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers); 
    }

    public function action_sales_by_month_report()
    {

        \View::set_global('menu', 'admin/reports/sales_by_month_report');
        \View::set_global('title', 'Sales By Month Report');

        $search = $this->get_search_sales_by_month_report();

        $items      = $search['items'];

        \Theme::instance()->set_partial('content', $this->view_dir . 'sales_by_month_report')
            ->set('items', $items);
    }

    public function action_export_sales_by_month_report()
    {
        $settings = \Config::load('autoresponder.db');
        $hold_gst = (isset($settings['gst']) ? 1 : 0);
        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

        $search = $this->get_search_sales_by_month_report();
        $items = $search['items'];

        $output = "Date,Number of Orders,Total Sales (without gst),Total Sales (with gst),GST ".$hold_gst_percentage."% (Included) \n";
        $content = 'text/csv';
        $filename = 'Sales-x-Month-Report-'.date('YmdHis').' Report.csv';


        if(count($items) > 0) {
            foreach ($items as $yr => $itm_val){
                $orders_num = 0;
                $without_gst = 0;
                $amount = 0;
                $gst = 0;
                $quarters['months'] = array(
                    1 => array(1=>'January',2=>'February',3=>'March'),
                    2 => array(1=>'April', 2=>'May', 3=>'June'),
                    3 => array(1=>'July', 2=>'August', 3=>'September',),
                    4 => array(1=>'October', 2=>'November', 3=>'December')

                );
                $month_count = 0;
                foreach ($itm_val as $item) {
                    $month_count++;

                    $orders_num += str_replace(',', '', $item['num_orders']);
                    $without_gst += str_replace(',', '', $item['month']['without_gst']);
                    $amount += str_replace(',', '', $item['month']['amount']);
                    $gst += str_replace(',', '', $item['month']['gst']);

                    $output .= $item['month']['name'].",".$item['num_orders'].",$".str_replace(',', '', $item['month']['without_gst']).",$".str_replace(',', '', $item['month']['amount']).",$".str_replace(',', '', $item['month']['gst'])."\n";


                    $quarter_key ='';

                    $q1 = array_column($quarters, 1);
                    $q2 = array_column($quarters, 2);
                    $q3 = array_column($quarters, 3);
                    $q4 = array_column($quarters, 4);
                    $quarter_key = (array_search($item['month']['name'], $q1[0]))?1:$quarter_key;
                    $quarter_key = (array_search($item['month']['name'], $q2[0]))?2:$quarter_key;
                    $quarter_key = (array_search($item['month']['name'], $q3[0]))?3:$quarter_key;
                    $quarter_key = (array_search($item['month']['name'], $q4[0]))?4:$quarter_key;

                    if (count($itm_val) == $month_count OR $item['month']['name'] == end($q1[0]) OR $item['month']['name'] == end($q2[0]) OR $item['month']['name'] == end($q3[0]) OR $item['month']['name'] == end($q4[0])) {
                        $output .= "Total for Q" . $quarter_key ." ".$yr."," . $orders_num .",$" . $without_gst . ",$" .$amount . ",$" . $gst . "\n";
                        $orders_num = 0;
                        $without_gst = 0;
                        $amount = 0;
                        $gst = 0;
                        $nn=0;
                    }
                }
            }
        }

        $output = rtrim($output, "\n");

        $headers = array(
            'Content-Type'          => $content,
            'Content-Disposition'   => 'attachment; filename="'.$filename.'"',
        );

        return \Response::forge($output, 200, $headers);
    }


    public function get_search_sales_by_month_report()
    {
        if (\Input::get('date_from') && \Input::get('date_to'))
        {

            $settings = \Config::load('autoresponder.db');
            $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

            $month_list = array();
            $date_from = \DateTime::createFromFormat('d/m/Y', \Input::get('date_from'));
            $date_to = \DateTime::createFromFormat('d/m/Y', \Input::get('date_to'));

            for ($y = $date_from->format('Y'); $y <= $date_to->format('Y'); $y++) {
                $month_list[$y] = array();
                for ($i = 1; $i <= 12; $i++) {
                    $timestamp = mktime(0, 0, 1, $i, 1, date("Y"));
                    $month = date("F", $timestamp);
                    $where = 'month(FROM_UNIXTIME(created_at)) = "' . $i . '" AND year(FROM_UNIXTIME(created_at)) = ' . $y;
                    $where .= ' AND FROM_UNIXTIME(created_at, "%Y-%m-%d") >= "'.$date_from->format('Y-m-d').'"';
                    $where .= ' AND FROM_UNIXTIME(created_at, "%Y-%m-%d") <= "'.$date_to->format('Y-m-d').'"';

                    if(\Input::get('user_id'))
                        $where .= ' AND user_id = '.\Input::get('user_id');

                    $results = \DB::query('SELECT count(*) as num_orders, sum(total_price) as total_amount from orders WHERE '.$where)->execute();

                    $gst = ($results[0]['total_amount']) ? \Helper::calculateGST($results[0]['total_amount'], $hold_gst_percentage) : 0;
                    $without_gst = ($results[0]['total_amount']) ? $results[0]['total_amount'] - $gst : 0;

                    if(($i >= $date_from->format('m') AND $y >= $date_from->format('Y') AND $y < $date_to->format('Y')) OR ($i <= $date_to->format('m') AND $y <= $date_to->format('Y') AND $y > $date_from->format('Y'))) {
                        array_push($month_list[$y], array('num_orders' => $results[0]['num_orders'], 'month' => array('name' => $month, 'amount' => number_format($results[0]['total_amount'], 2), 'without_gst' => $without_gst, 'gst' => number_format($gst, 2))));
                    }
                    else if($i >= $date_from->format('m') AND $i <= $date_to->format('m') AND $date_from->format('Y') == $date_to->format('Y')) {
                        array_push($month_list[$y], array('num_orders' => $results[0]['num_orders'], 'month' => array('name' => $month, 'amount' => number_format($results[0]['total_amount'], 2), 'without_gst' => $without_gst, 'gst' => number_format($gst, 2))));
                    }else{}

                }
            }

            return array('items' => $month_list);
        }

    }
}