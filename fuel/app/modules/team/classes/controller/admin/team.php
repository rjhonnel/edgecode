<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Team;

class Controller_Admin_Team extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/team/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('team::team', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/team/list');
	}
    
    public function get_search_items()
    {
        /************ Start generating query ***********/
		$items = Model_Team::find(function($query){
			
			// Get search filters
			foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
							$query->where('name', 'like', "%$value%");
							break;
						case 'featured':
							if(is_numeric($value))
								$query->where($key, $value);
							break;
						case 'status':
							if(is_numeric($value))
								$query->where($key, $value);
							break;
						case 'active_from':
                            $date = strtotime($value);
                            if($date)
                                $query->where($key, '>=', $date);
							break;
						case 'active_to':
                            $date = strtotime($value);
                            if($date)
                                $query->where($key, '<=', $date);
							break;
					}
				}
			}
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
        
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
        
        return array('items' => $items, 'pagination' => $pagination, 'status' => $status);
    }
	
	public function action_list()
	{
		\View::set_global('title', 'List Teams');
		
        $search = $this->get_search_items('root');
        
		$items      = $search['items'];
        $pagination = $search['pagination'];
        $status     = $search['status'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false)
			->set('status', $status);
	}
	
	public function action_create()
	{
		\View::set_global('title', 'Add New');
		
		if(\Input::post())
		{
			$val = Model_Team::validate('create');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->image))
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload member image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload member image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false)))
			{
				// Get POST values
				$insert = \Input::post();
				
				// Prepare some values
				$insert['published_at'] = !empty($insert['published_at']) ? strtotime($insert['published_at']) : \Date::forge()->get_timestamp();
				$insert['active_from'] 	= !empty($insert['active_from']) ? strtotime($insert['active_from']) : NULL;
				$insert['active_to'] 	= !empty($insert['active_to']) ? strtotime($insert['active_to']) : NULL;
				if($insert['status'] != 2)
				{
					unset($insert['active_from']);
					unset($insert['active_to']);
				}
				
				$item = Model_Team::forge($insert);
				
				try{
					$item->save();
					
					// Validate and insert team slug into SEO database table
					$val_seo = Model_Seo::validate('create_seo');
					$insert_seo = array(
						'content_id' 	=> $item->id,
						'slug'			=> \Inflector::friendly_title($item->name, '-', true),
					);
					
					while(!$val_seo->run($insert_seo))
					{
						$insert_seo['slug'] = \Str::increment($insert_seo['slug'], 1, '-');
					}
					
					$item_seo = Model_Seo::forge($insert_seo);
					$item_seo->save();
					// END OF: SEO
					
					// Insert team images
					if($this->_image_data)
					{
						$item_image = array(
							array(
								'id'	=> 0,
								'data'	=> array(
									'content_id'	=> $item->id,
									'image'			=> $this->_image_data[0]['saved_as'],
									'alt_text'		=> \Input::post('alt_text', ''),
									'cover'			=> 1,
									'sort'			=> 1,
								),
							),
						);
						
						Model_Team::bind_images($item_image);
					}
					
					\Messages::success('Member successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/team/update/' . $item->id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create member</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create member</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
			
			// Delete uploaded image if there is team saving error
			if(isset($this->_image_data))
				$this->delete_image($this->_image_data[0]['saved_as']);
		}
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'create');
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/team/list');
		
		// Get news item to edit
		if(!$item = Model_Team::find_one_by_id($id)) \Response::redirect('admin/team/list');
		
		\View::set_global('title', 'Edit Member');
		
		if(\Input::post())
		{
			$val = Model_Team::validate('update');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)) 
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload content image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload content image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)))
			{
				/** IMAGES **/
				// Get all alt texts to update if there is no image change
				foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
				{
					if(strpos($image_id, 'new_') === false)
					{
						$item_images[$image_id] = array(
							'id'	=> $image_id,
							'data'	=> array(
								'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
							),
						);
					}
				}
				
				// Save images if new files are submitted
				if(isset($this->_image_data))
				{
					foreach($this->_image_data as $image_data)
					{
						$cover_count = count($item->images);
						if(strpos($image_data['field'], 'new_') === false)
						{
							// Update existing image
							if(str_replace('image_', '', $image_data['field']) != 0)
							{
								$image_id = (int)str_replace('image_', '', $image_data['field']);
								$cover_count--;
								
								$item_images[$image_id] = array(
									'id'	=> $image_id,
									'data'	=> array(
										'content_id'	=> $item->id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
									),
								);
								
								$this->delete_image(\Input::post('image_db_' . $image_id, ''));
							}
						}
						else
						{
							// Save new image
							$image_tmp = str_replace('image_new_', '', $image_data['field']);
							
							$item_images[0] = array(
								'id'	=> 0,
								'data'	=> array(
									'content_id'	=> $item->id,
									'image'			=> $image_data['saved_as'],
									'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
									'cover'			=> $cover_count == 0 ? 1 : 0,
									'sort'			=> $cover_count + 1,
								),
							);
						}
					}
				}
				Model_Team::bind_images($item_images);
				/** END OF IMAGES **/
				
				// Get POST values
				$insert = \Input::post();
				
				// Prepare some values
				$insert['published_at'] = !empty($insert['published_at']) ? strtotime($insert['published_at']) : \Date::forge()->get_timestamp();
				$insert['active_from'] 	= !empty($insert['active_from']) ? strtotime($insert['active_from']) : NULL;
				$insert['active_to'] 	= !empty($insert['active_to']) ? strtotime($insert['active_to']) : NULL;
				
				if($insert['status'] != 2)
				{
					unset($insert['active_from']);
					unset($insert['active_to']);
				}
				
				$item->set($insert);
				
				try{
					$item->save();
					
					\Messages::success('Member successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/team/list/') : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update member</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				// Delete uploaded images if there is news saving error
				if(isset($this->_image_data))
					foreach($this->_image_data as $image_data)
						$this->delete_image($image_data['saved_as']);
						
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update member</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		$team = Model_Team::find_one_by_id($id);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('team', $team);
	}
	
	/****************************** CONTENT IMAGES ******************************/
	
	/**
	 * Upload all contet images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->image_upload_config = array(
		    'path' => \Config::get('details.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		);
		
		\Upload::process($this->image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save image
		    \Upload::save();
			$this->_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
				@unlink($folder . $name);
			}
		}
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Team::find_one_by_id($id))
			{
				// Delete other content data like images, files, etc.
				if(!empty($item->images))
				{
					foreach($item->images as $image)
					{
						$this->delete_image($image->image);
						$image->delete();
					}
				}
				
				// if(!empty($item->files))
				// {
				// 	foreach($item->files as $file)
				// 	{
				// 		$this->delete_file($file->file);
				// 		$file->delete();
				// 	}
				// }
				
				// if(!empty($item->videos))
				// {
				// 	foreach($item->videos as $video)
				// 	{
				// 		$this->delete_image($video->thumbnail, 'video');
				// 		$video->delete();
				// 	}
				// }
				
				// if(!empty($item->accordions))
				// {
				// 	foreach($item->accordions as $accordion)
				// 	{
				// 		\Request::forge('admin/team/accordion/delete/'.$accordion->id)->execute();
				// 	}
				// }
					
				try{
					
					$item->seo->delete();
					$item->delete();
					
					\Messages::success('Member successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete team</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
}