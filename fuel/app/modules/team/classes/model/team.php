<?php

	namespace Team;

	class Model_Team extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'team';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'type',
		    'name',
		    'position',
		    'description_full',
		    'status',
		    'facebook',
		    'twitter',
		    'linkedin',
		    'active_from',
		    'active_to',
		    'published_at',
		    'user_created',
		    'user_updated',
		    'sort',
		);
		
		protected static $_defaults = array(
		    'type' 		=> 'team',
		);
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    /**
	     * Insert or update page images
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	    	if(empty($images) || !is_array($images)) return false;
	    	
	    	foreach($images as $key => $image)
	    	{
	    		$item = Model_Image::forge($image['data']);
	    		
	    		if(is_numeric($image['id']) && $image['id'] > 0)
	    		{
	    			// Update existing image
	    			$item->set(array('id' => $image['id']));
	    			$item->is_new(false);
	    		}
	    		
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Insert or update page files
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     * 		'data'	=> Array of file data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     * 		'data'	=> Array of file data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $files
	     */
	    public static function bind_files($files = array())
	    {
	    	if(empty($files) || !is_array($files)) return false;
	    	
	    	foreach($files as $key => $file)
	    	{
	    		$item = Model_File::forge($file['data']);
	    		
	    		if(is_numeric($file['id']) && $file['id'] > 0)
	    		{
	    			// Update existing file
	    			$item->set(array('id' => $file['id']));
	    			$item->is_new(false);
	    		}
	    		
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Insert or update page videos
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     * 		'data'	=> Array of file data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     * 		'data'	=> Array of file data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $files
	     */
	    public static function bind_videos($videos = array())
	    {
	    	if(empty($videos) || !is_array($videos)) return false;
	    	
	    	foreach($videos as $key => $video)
	    	{
	    		$item = Model_Video::forge($video['data']);
	    		
	    		if(is_numeric($video['id']) && $video['id'] > 0)
	    		{
	    			// Update existing videos
	    			$item->set(array('id' => $video['id']));
	    			$item->is_new(false);
	    		}
	    		
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	    	$table_name = self::$_table_name;
            
	    	$query->where($table_name . '.type', self::$_defaults['type']);
	    	
            // On frontend search only active pages
            $active = \Theme::instance()->active();
	    	if($active['name'] == 'frontend') 
            {
                $query->where(function($query) use ($table_name){
                    $query->where($table_name . '.status', 1);
                    $query->or_where(function($query) use ($table_name){
                        $query->where($table_name . '.status', 2);
                        $query->and_where($table_name . '.active_from', '<=', time());
                        $query->and_where($table_name . '.active_to', '>=', time());
                    });
                });
            }
            
	    	// Enable caching for all SELECT queries
	    	//$query->cached(3600, 'admin.news.', false);
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
		    			// Get content images
						$item->get_images = static::lazy_load(function() use ($item){
		    				return Model_Image::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'images');
						
		    			// Get content files
						$item->get_files = static::lazy_load(function() use ($item){
		    				return Model_File::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'files');
						
		    			// Get content videos
						$item->get_videos = static::lazy_load(function() use ($item){
		    				return Model_Video::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'videos');
						
		    			// Get content children
						$item->get_children = static::lazy_load(function() use ($item){
		    				return Model_Team::find(array(
							    'where' => array(
							        'parent_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'children');
						
		    			// Get content accordions
						$item->get_accordions = static::lazy_load(function() use ($item){
		    				return Model_Accordion::find(array(
							    'where' => array(
							        'parent_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'accordions');
						
		    			// Get content children
						$item->get_seo = static::lazy_load(function() use ($item){
							return Model_Seo::find_one_by_content_id($item->id);
		    			}, $item->id, 'seo', 'object');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Team\Model_Team::$temp['lazy.'.$id.$type]))
		    	{
		    		\Team\Model_Team::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Team\Model_Team::$temp['lazy.'.$id.$type])) 
			    			\Team\Model_Team::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Team\Model_Team::$temp['lazy.'.$id.$type])) 
			    			\Team\Model_Team::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Team\Model_Team::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('name', 'Name')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);
			$val->add('description_full', 'Description')->add_rule('required');
			if(\Input::post('status', 1) == 2)
			{
				// Only set if status is "Active in period"
				$val->add('active_from', 'Active from')->add_rule('required')->add_rule('date');
				$val->add('active_to', 'Active to')->add_rule('required')->add_rule('date');
			}
			
			$val->add('published_at', 'Published at')->add_rule('date');
	
			return $val;
		}
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				$vars['user_created'] = \Sentry::user()->id;
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				$class = 'Team\Model_' . ucfirst($name);
				return new $class;
			}
			
			return static::forge();
		}
		
	}