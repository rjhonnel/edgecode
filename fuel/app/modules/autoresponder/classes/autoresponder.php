<?php

namespace Autoresponder;

class Autoresponder
{
    public $view_dir = 'views/_email/';
    
    public $view_admin;
    
    public $view_user;
    
    public $view_custom;
    
    public $autoresponders = array();
    
    public $user_id = null;
    
    /**
	 * @var	array	Pagination instances
	 */
	protected static $_instances = array();

	/**
	 * @var	array	Pagination default instance
	 */
	protected static $_instance = null;
    
    /**
	 * forge a new pagination instance
	 *
	 * @return	\Pagination	a new pagination instance
	 */
	public static function forge($name = 'default', $config = array())
	{
        \Config::load('auto_response_emails', true);
         
		if ($exists = static::instance($name))
		{
			\Error::notice('Autoresponder with this name exists already, cannot be overwritten.');
			return $exists;
		}

		static::$_instances[$name] = new static($config);

		if ($name == 'default')
		{
			static::$_instance = static::$_instances[$name];
		}

		return static::$_instances[$name];
	}

	/**
	 * retrieve an existing pagination instance
	 *
	 * @return	\Pagination	a existing pagination instance
	 */
	public static function instance($name = null)
	{
		if ($name !== null)
		{
			if ( ! array_key_exists($name, static::$_instances))
			{
				return false;
			}

			return static::$_instances[$name];
		}

		if (static::$_instance === null)
		{
			static::$_instance = static::forge();
		}

		return static::$_instance;
	}
           
    public function autoresponder_user($content = array(), $attachments = array())
    {
        if(!$user = $this->get_user())
        {
            return false;
        }
        
        $user_group = $user->groups();
        $user_group = $user_group[0];
        
        $email_data['user'] = $user;
        $email_data['user_metadata'] = isset($user['metadata'])?$user['metadata']:array();
        $email_data['content'] = $content;
        
        $email = \Email::forge();
        
        if(!empty($attachments))
        {
            foreach ($attachments as $attachment)
            {
                if(is_file($attachment)) $email->attach($attachment);
            }
        }
        
        $settings = \Config::load('autoresponder.db');

        $email->from($settings['sender_email_address'], $settings['company_name']);
        
        $email->to($user->get('email'));
        if(isset($content['cc_email']))
        {
            if($content['cc_email'])
                $email->cc($content['cc_email']);
        }
        $email->html_body(\Theme::instance()->view($this->view_dir . $this->view_user, $email_data, false));
        
        $subject = $settings['company_name'];
        if(isset($content['subject'])) $subject = $content['subject'];
        $email->subject($subject);
        
        $this->autoresponders[] = $email;
        
    }
    
    public function autoresponder_admin($content = array(), $emails = array(), $attachments = array())
    {

        if($user = $this->get_user())
        {
            $email_data['user'] = $user;
            $email_data['user_metadata'] = isset( $user['metadata'] ) ? $user['metadata'] : null;
        }
         
        $email_data['content'] = $content;
        
        $email = \Email::forge();
        

        


        if(!empty($attachments))
        {
            foreach ($attachments as $attachment)
            {
                if(is_file($attachment)) $email->attach($attachment);
            }
        }


       
        $bcc = \Config::get('auto_response_emails.bcc');
        
        if($bcc) $email->bcc($bcc);
        
        $settings = \Config::load('autoresponder.db');

        $email->from($settings['sender_email_address'], $settings['company_name']);

        $email->to($emails);
        if(isset($content['cc_email']))
        {
            if($content['cc_email'])
                $email->cc($content['cc_email']);
        }



        $email->html_body(\Theme::instance()->view($this->view_dir . $this->view_admin, $email_data, false));

        $subject = $settings['company_name'];
        if(isset($content['subject'])) $subject = $content['subject'];

        $email->subject($subject);

        $this->autoresponders[] = $email;
    }
    
    public function autoresponder_custom($content = array(), $emails = array(), $attachments = array())
    {
        $settings = \Config::load('autoresponder.db');

        $email = \Email::forge();
        
        $email->from($settings['sender_email_address'], $settings['company_name']);
        
        $email_data['content'] = $content;
        
        if(!empty($attachments))
        {
            foreach ($attachments as $attachment)
            {
                if(is_file($attachment)) $email->attach($attachment);
            }
        }

        if($emails == 'backup')
        {          
            $backup_settings = \Config::load('backup.db');  
            $emails = $backup_settings['email'];
        }

        $email->to($emails);

        $email->html_body(\Theme::instance()->view($this->view_dir . $this->view_custom, $email_data, false));

        $subject = $settings['company_name'];
        if(isset($content['subject'])) $subject = $content['subject'];
        $email->subject($subject);
        
        $this->autoresponders[] = $email;
        
    }
    
    public function autoresponder_custom_admin($content = array(), $emails = array())
    {
        $settings = \Config::load('autoresponder.db');

        $email = \Email::forge();
        
        $email->from($content['email'], $content['name']);
        
        $email_data['content'] = $content;

        $email->to($emails);
        $email->html_body(\Theme::instance()->view($this->view_dir . $this->view_custom . $this->view_user, $email_data, false));

        $subject = $settings['company_name'];
        if(isset($content['subject'])) $subject = $content['subject'];
        $email->subject($subject);
        
        $this->autoresponders[] = $email;        
    }
    
    protected function get_user()
    {
        if(!\Sentry::user_exists((int)$this->user_id))
        {
            return false;
        }
        return \Sentry::user((int)$this->user_id);
    }
    
    public function send()
    {
        if(!empty($this->autoresponders))
        {
            foreach ($this->autoresponders as $autoresponder)
            {
                try
                {
                    $autoresponder->send();
                    //\Messages::success('Email successfully sent.');
                }
                catch(\EmailValidationFailedException $e)
                {
                    //\Messages::error('<strong>' . 'There was an error while sending email.' . '</strong>');
                }
                catch(\EmailSendingFailedException $e)
                {
                    //\Messages::error('<strong>' . 'There was an error while sending email.' . '</strong>');
                }
            }
            
            return true;
        }
    }
    
}