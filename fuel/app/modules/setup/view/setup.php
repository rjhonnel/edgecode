<?php
use Setup\Setup_Installer;
?><!DOCTYPE html>
<html lang="{{App::getLocale()}}">
<head>
	<title>Edgecommerce | Setup</title>
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,300,700' rel='stylesheet' type='text/css'>
	<?php
	// styles for html elements
	// echo $theme->asset->css('jquery.bootstrap-touchspin.css');
	// echo $theme->asset->css('component.css');
	echo $theme->asset->css('main.css');
	// echo $theme->asset->css('select2.css');
	// EOF styles for html elements
	// required file to run jquery functions
	echo $theme->asset->js('plugins/jquery-2.2.4.min.js');
	?>

	<style type="text/css">
		body {
			background-color: #FEFEFE;
			padding-bottom: 150px;
		}
		.page-intro {
			margin-bottom: 40px;
		}
		p {
			margin-bottom: 6px;
		}

		.head-logo {
			padding-top: 20px;
			margin-bottom: 40px;
		}
		.head-logo img {
			float: right;
		}

		.panel-default {
			border: none;
		}
		.panel-default > .panel-heading {
			border-color: #72cd70;
		}
		.panel-default .panel-body {
			border-color: #dddddd;
			border: 1px solid #dddddd;
			border-top: none;
		}
		.alert h4 {
			margin-bottom: 12px;
		}
	</style>
	<script>
		var BASE_URL = "<?php echo Uri::create('/') ?>";
	</script>
</head>

<body>
	<form action="<?php echo Uri::create('/setup/index') ?>" method="POST">
		<div class="container">
			&nbsp;
			<div class="row">
				<div class="col-md-8 col-md-offset-2">

					<div class="page-intro">
						<div class="head-logo">
							<img src="/themes/frontend/images/brand.jpg" >
							<h2>Edge Commerce Setup</h2>
						</div>
						<!-- Display messages -->
						<?php echo \Messages::display(); ?>
						<!-- EOF Display messages -->
						<div id="general-message" class="alert alert-warning" style="display: none;">Error: <span></span></div>
						<?php if (version_compare(phpversion(), '5.5.9', '<')) : ?>
							<div class="alert alert-warning">Warning: The application requires PHP >= 5.5.9</div>
						<?php endif; ?>
						<?php if (!function_exists('proc_open')) : ?>
							<div class="alert alert-warning">Warning: <a href="http://php.net/manual/en/function.proc-open.php" target="_blank">proc_open</a> must be enabled.</div>
						<?php endif; ?>
						<?php if (!@fopen(APPPATH."cache.env", 'a')) : ?>
							<div class="alert alert-warning">Warning: Permission denied to write .env config file
								<pre>sudo chown www-data:www-data /path/to/edgecms/.env</pre>
							</div>
						<?php endif; ?>
						<p>
							If you need help you can either post to our <a href="http://www.edgecommerce.org/forums/forum/support/" target="_blank">support forum</a>
							or email us at <a href="mailto:info@edgecommerce.org" target="_blank">info@edgecommerce.org</a>.
						</p>
						<p>
	<pre>-- Commands to create a MySQL database and user
	CREATE SCHEMA `edgecms` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
	CREATE USER 'edgecms'@'localhost' IDENTIFIED BY 'edgecms';
	GRANT ALL PRIVILEGES ON `edgecms`.* TO 'edgecms'@'localhost';
	FLUSH PRIVILEGES;</pre>
						</p>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Application Settings</h3>
						</div>
						<div class="panel-body form-padding-right">
							<div class="form-group">
								<?php echo \Form::label('Application Url' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_APPLICATION_URL, \Input::post(Setup_Installer::KEY_APPLICATION_URL, \Fuel\Core\Uri::create('/')), array("class" => "required form-control", 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Site Title' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_APPLICATION_SITE_TITLE, \Input::post(Setup_Installer::KEY_APPLICATION_SITE_TITLE, 'Edge Commerce Demo Site'), array("class" => "required form-control", 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Admin Email' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_APPLICATION_SITE_EMAIL, \Input::post(Setup_Installer::KEY_APPLICATION_SITE_EMAIL, ''), array("class" => "required form-control", 'required'=>'required')); ?>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Database Connection</h3>
						</div>
						<div class="panel-body form-padding-right">
							<div class="form-group">
								<?php echo \Form::label('Database Host' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_DB_HOST, \Input::post(Setup_Installer::KEY_DB_HOST, 'localhost'), array("class" => "required form-control")); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Username' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_DB_USER, \Input::post(Setup_Installer::KEY_DB_USER, 'edgecms'), array("class" => "required form-control", 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Password' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_DB_PASS, \Input::post(Setup_Installer::KEY_DB_PASS, 'edgecms'), array("class" => "required form-control", 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Database Name' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_DB_NAME, \Input::post(Setup_Installer::KEY_DB_NAME, 'edgecms'), array("class" => "required form-control", 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Port' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_DB_PORT, \Input::post(Setup_Installer::KEY_DB_PORT, 3306), array("class" => "required form-control", 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('UNIX Socket'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_DB_UNIX_SOCKET, \Input::post(Setup_Installer::KEY_DB_UNIX_SOCKET, ''), array("class" => "form-control")); ?>
								<small>Example: /Applications/MAMP/tmp/mysql/mysql.sock</small>
							</div>
							<div class="alert alert-info" id="js-db-test-result" style="display:none;"></div>
							<div class="form-group">
								<button class="btn btn-info" type="button" onclick="testDatabase()" >Test connection</button>
							</div>
						</div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Email Settings</h3>
						</div>
						<div class="panel-body form-padding-right">

							<div class="form-group">
								<?php echo \Form::label('Mail Driver' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::select(Setup_Installer::KEY_MAIL_DRIVER, \Input::post(Setup_Installer::KEY_MAIL_DRIVER, 'mail'), ['smtp' => 'SMTP', 'mail' => 'Mail', 'sendmail' => 'Sendmail'], array('class' => 'select_init w_state_popup form-control required', 'tabindex' => '13', 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Mail Host'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_MAIL_HOST, \Input::post(Setup_Installer::KEY_MAIL_HOST), array("class" => " form-control")); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Mail Port' ); ?>
								<?php echo \Form::input(Setup_Installer::KEY_MAIL_PORT, \Input::post(Setup_Installer::KEY_MAIL_PORT, '465'), array("class" => " form-control")); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Mail Username' . ''); ?>
								<?php echo \Form::input(Setup_Installer::KEY_MAIL_USERNAME, \Input::post(Setup_Installer::KEY_MAIL_USERNAME), array("class" => " form-control")); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Mail Password' . ''); ?>
								<?php echo \Form::input(Setup_Installer::KEY_MAIL_PASSWORD, \Input::post(Setup_Installer::KEY_MAIL_PASSWORD), array("class" => " form-control")); ?>
							</div>
						</div>

						<script type="text/javascript">
							var completeText = 'Complete Setup';
							var db_valid = false;
							var mail_valid = false;

							var gm = $('#general-message');
							function displayMessage(msg)
							{
								gm.show().find('span').text(msg);

								$('html, body').animate({
									scrollTop: gm.offset().top - 60,
								}, 100, function(){
									gm.hide().slideDown();
								});
							}

							function testDatabase()
							{
								var data = $("form").serialize() + "&test=db";
								var $dbTest = $('#js-db-test-result');

								// Show Progress Text
								$dbTest.html('Working...').show();

								// Send / Test Information
								return $.post(BASE_URL+"setup/index", data, function( data ) {
									$dbTest.removeClass('alert-success')
										.removeClass('alert-warning');

									db_valid = false;
									var alertClass = 'alert-warning';

									if (data.status == 'success') {
										db_valid = true;
										alertClass = 'alert-success';
									}

									$dbTest.addClass(alertClass)
										.html(data.message);
								});
							}

							function testMail()
							{
								var data = $("form").serialize() + "&test=mail";

								// Send / Test Information
								return $.post(BASE_URL+"setup/index", data, function( data ) {
									mail_valid = data.status == 'success';
								});
							}

							$('form').submit(function(e) {
								var current = 0;
								var button = $("#complete-setup-button");

								setInterval(function() {
									var base = 'Processing';
									if (current == 0) {
										button.text(base+' .');
									} else if (current == 1) {
										button.text(base+' ..');
									} else if (current == 2) {
										button.text(base+' ...');
									}

									current = current == 2 ? 0 : current+1;
								}, 750);
							});

							$(function() {
								// Prevent the Enter Button from working
								$("#complete-setup-button").click(function (e) {
									var me = $(this);

									me.text('Processing ...');
									$.when(testDatabase()/*, testMail()*/).done(function() {
										if (!db_valid) {
											displayMessage('Unable to connect to the database.');
											me.text(completeText);
											return false;
										}

										/*if (!mail_valid) {
											displayMessage('Unable to send email.');
											me.text(completeText);
											return false;
										}*/

										gm.hide();
										$('form').submit();
									});
								});
							});
						</script>
					</div>


					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Admin Details</h3>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<?php echo \Form::label('Email' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_ADMIN_EMAIL, \Input::post(Setup_Installer::KEY_ADMIN_EMAIL), array("class" => " form-control", 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Username' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_ADMIN_USERNAME, \Input::post(Setup_Installer::KEY_ADMIN_USERNAME), array("class" => " form-control", 'required'=>'required')); ?>
							</div>
							<div class="form-group">
								<?php echo \Form::label('Password' . ' <span class="req">*</span>'); ?>
								<?php echo \Form::input(Setup_Installer::KEY_ADMIN_PASSWORD, \Input::post(Setup_Installer::KEY_ADMIN_PASSWORD), array("class" => " form-control", 'required'=>'required')); ?>
							</div>
						</div>
					</div>
					<button type="submit" style="display: none;"></button>
					<?php echo \Form::button(array('type' => 'button', 'value' => 'Complete Setup', 'class' => 'btn btn-warning', 'id'=>'complete-setup-button')); ?>
				</div>
			</div>
		</div>
	</form>
</body>
</html>