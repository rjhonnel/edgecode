<?php namespace Setup;

use Email\Email;
use Email\Email_Driver;
use Exception;
use Fuel\Core\Config;
use Fuel\Core\File;
use Fuel\Core\Validation;
use Sentry\Sentry;

class Setup_Installer
{
	const DEFAULT_DB_PORT = '3306';

	const KEY_APPLICATION_URL = 'application_url';
	const KEY_APPLICATION_SITE_TITLE = 'site_title';
	const KEY_APPLICATION_SITE_EMAIL = 'site_email';

	const KEY_DB_HOST = 'db_host';
	const KEY_DB_NAME = 'db_name';
	const KEY_DB_USER = 'db_user';
	const KEY_DB_PASS = 'db_pass';
	const KEY_DB_PORT = 'db_port';
	const KEY_DB_UNIX_SOCKET = 'db_unix_socket';

	const KEY_MAIL_DRIVER       = 'mail_driver';
	const KEY_MAIL_HOST         = 'mail_host';
	const KEY_MAIL_PORT         = 'mail_port';
	const KEY_MAIL_USERNAME    = 'mail_from_name';
	const KEY_MAIL_PASSWORD     = 'mail_password';

	const KEY_ADMIN_USERNAME       = 'admin_username';
	const KEY_ADMIN_EMAIL       = 'admin_email';
	const KEY_ADMIN_PASSWORD    = 'admin_password';

	protected $fieldTitleMaps = array(
		self::KEY_ADMIN_USERNAME              => 'Admin Username',
		self::KEY_ADMIN_EMAIL                 => 'Admin Email',
		self::KEY_ADMIN_PASSWORD              => 'Admin Password',
		self::KEY_APPLICATION_URL             => 'Application Url',
		self::KEY_APPLICATION_SITE_TITLE      => 'Site Title',
		self::KEY_APPLICATION_SITE_EMAIL      => 'Site Email',
	);

	/**
	 * @var array
	 */
	protected $data = [];

	/**
	 * @var bool
	 */
	protected $flag_admin_created = false;

	/**
	 * @var bool
	 */
	protected $flag_email_sent = false;

	/**
	 * Setup_Installer constructor.
	 *
	 * @param $data
	 */
	function __construct($data = [])
	{
		Config::load('setup::setup', 'setup');

		$this->data = $data;
	}

	/**
	 * Check if allow to setup
	 *
	 * @return bool
	 */
	public function isAllow()
	{
		return !File::exists($this->getSetupFlagPath()) // see setupCompleted method
			&& !File::exists($this->getConfig('paths.mail.target'))
			&& !File::exists($this->getConfig('paths.db.target'));
	}

	/**
	 * Remove all configuration file
	 */
	public function cancelConfiguration()
	{
		File::exists($this->getConfig('paths.db.target')) && File::delete($this->getConfig('paths.db.target'));
		File::exists($this->getConfig('paths.mail.target')) && File::delete($this->getConfig('paths.mail.target'));
		File::exists($this->getSetupFlagPath()) && File::delete($this->getSetupFlagPath());

		if ($this->flag_admin_created) {
			$user = Sentry::user($this->getData(self::KEY_ADMIN_USERNAME));
			$user->delete();
		}
	}

	/**
	 * Validate credentials before processing
	 *
	 * @return true
	 * @throws Setup_Exception
	 */
	public function validateCredentials()
	{
		/* $this->testMail(); */
		$this->testDB();

		// validate fields
		$val = Validation::forge();
		$val->add_field(self::KEY_ADMIN_EMAIL, $this->getData(self::KEY_ADMIN_EMAIL), 'required');
		$val->add_field(self::KEY_ADMIN_PASSWORD, $this->getData(self::KEY_ADMIN_PASSWORD), 'required|min_length[8]');
		$val->add_field(self::KEY_ADMIN_USERNAME, $this->getData(self::KEY_ADMIN_USERNAME), 'required');

		$val->add_field(self::KEY_APPLICATION_URL, $this->getData(self::KEY_APPLICATION_URL), 'required');
		$val->add_field(self::KEY_APPLICATION_SITE_EMAIL, $this->getData(self::KEY_APPLICATION_SITE_EMAIL), 'required');
		$val->add_field(self::KEY_APPLICATION_SITE_TITLE, $this->getData(self::KEY_APPLICATION_SITE_TITLE), 'required');

		if (!$val->run()) {
			foreach ($val->error() as $key => $value) {
				$title = isset($this->fieldTitleMaps[$key]) ? $this->fieldTitleMaps[$key] : null;
				throw new Setup_Exception($title . ': ' . $value);
			}
		}
	}

	/**
	 * Test DB Credentials
	 *
	 * @return \PDO
	 * @throws \Exception
	 */
	public function testDB() {
		return $this->getDbInstance();
	}

	/**
	 * Get DB Instance
	 *
	 * @return \PDO
	 * @throws \Exception
	 */
	public function getDbInstance()
	{
		try {
			$host = $this->getData(self::KEY_DB_HOST);
			$host = $host;

			$unix_socket = $this->getData(self::KEY_DB_UNIX_SOCKET);
			$unix_socket_value = '';
			if(!empty($unix_socket))
				$unix_socket_value = 'unix_socket='.$unix_socket.';';

			$dsn = sprintf('mysql:host=%s;port=%s;dbname=%s;%s',
				$host,
				$this->getData(self::KEY_DB_PORT),
				$this->getData(self::KEY_DB_NAME),
				$unix_socket_value
			);

			return new \PDO(
				$dsn,
				$this->getData(self::KEY_DB_USER),
				$this->getData(self::KEY_DB_PASS)
			);
		} catch (\Exception $e) {
			//throw $e;
			throw $e;
		}
	}

	/**
	 * Test email configuration
	 *
	 * @return  bool
	 * @throws  Exception
	 * @throws \EmailValidationFailedException
	 * @throws \FuelException
	 */
	public function testMail()
	{
		if ($this->flag_email_sent) {
			return $this->flag_email_sent;
		}

		$mail_config = [
			'useragent'	=> 'FuelPHP, PHP 5.3 Framework',
			'driver'		=> $this->getData(self::KEY_MAIL_DRIVER), //mail
			'is_html'		=> true,
			'charset'		=> 'utf-8',
			'encode_headers' => true,
			'encoding'		=> '8bit',
			'priority'		=> Email::P_NORMAL,
			'from'		=> array(
				'email'		=> 'setup@edgecommerce.org',
				'name'		=> 'Setup',
			),
			'return_path'   => false,
			'validate'	=> true,
			'auto_attach' => true,
			'generate_alt' => true,
			'force_mixed'   => false,
			'wordwrap'	=> 76,
			'sendmail_path' => '/usr/sbin/sendmail',
			'smtp'	=> array(
				'host'		=> $this->getData(self::KEY_MAIL_HOST),
				'port'		=> $this->getData(self::KEY_MAIL_PORT), //25
				'username'	=> $this->getData(self::KEY_MAIL_USERNAME),
				'password'	=> $this->getData(self::KEY_MAIL_PASSWORD),
				'timeout'	=> 5,
			),
			'newline' => "\r\n",
		];

		/**@var $instance Email_Driver */
		$instance = Email::forge($mail_config);
		$instance
			->from('rex.t@lightmedia.com.au')
			->to('rex.t@lightmedia.com.au')
			->cc('ren@lightmedia.com.au')
			->subject('New edge commerce Installation')
			->body('..');
		try
		{
			$instance->send();

			$this->flag_email_sent = true;
		} catch (Exception $e) {
			throw new Exception('Failed to send email.');
		}
	}

	/**
	 * Attempt to create all configuration.
	 *
	 * @throws Setup_Exception
	 */
	public function attempt()
	{
		ini_set('max_execution_time', 180);

		try {
			$this->validateCredentials();

			// create the configurations
			$this->createConfig();
			$this->createMailConfig();
			$this->createDBConfig();

			$this->processMysqlImport();

			// run fuelphp migration
			exec('php oil refine migrate -all');

			$this->setupCompleted();
		} catch (Exception $e) {
			$this->cancelConfiguration();
			throw $e;
		}
	}

	/**
	 * Complete the setup
	 *
	 * @return Void
	 * @throws \FileAccessException
	 * @throws \InvalidPathException
	 */
	private function setupCompleted()
	{
		$this->createAdminUser();

		$path = $this->getSetupFlagPath();
		$this->createFile($path, 'Setup Completed @ '.date('Y-m-d').'. Please see allow \Setup\Installer::isAllow');
	}

	/**
	 * Return create config
	 *
	 * @return Void
	 * @throws Setup_Exception
	 */
	protected function createConfig()
	{
		$path       = $this->getConfig('paths.config.template');
		$target     = $this->getConfig('paths.config.target');
		$content    = $this->getContent($path);
		$newContent = $this->contentReplace($content, array(
			'{{application_url}}' => $this->getData(self::KEY_APPLICATION_URL),
			'{{site_title}}' => $this->getData(self::KEY_APPLICATION_SITE_TITLE),
			'{{admin_email}}' => $this->getData(self::KEY_APPLICATION_SITE_EMAIL),
		));

		$this->createFile($target, $newContent);
	}

	/**
	 * Create mail config file
	 *
	 * @return Void
	 * @throws Setup_Exception
	 */
	protected function createMailConfig()
	{
		$path       = $this->getConfig('paths.mail.template');
		$target     = $this->getConfig('paths.mail.target');
		$content    = $this->getContent($path);

		$newContent = $this->contentReplace($content, array(
			'{{mail_driver}}' => $this->getData(self::KEY_MAIL_DRIVER),
			'{{mail_host}}' => $this->getData(self::KEY_MAIL_HOST),
			'{{mail_port}}' => $this->getData(self::KEY_MAIL_PORT, 465),
			'{{mail_from_username}}' => $this->getData(self::KEY_MAIL_USERNAME),
			'{{mail_from_password}}' => $this->getData(self::KEY_MAIL_PASSWORD),
		));


		$this->createFile($target, $newContent);
	}

	/**
	 * Create DB config file
	 *
	 * @return Void
	 * @throws Setup_Exception
	 */
	protected function createDBConfig()
	{
		$path       = $this->getConfig('paths.db.template');
		$target     = $this->getConfig('paths.db.target');
		$content    = $this->getContent($path);

		$unix_socket = $this->getData(self::KEY_DB_UNIX_SOCKET);
		$unix_socket_value = '';
		if(!empty($unix_socket))
			$unix_socket_value = 'unix_socket='.$unix_socket.';';

		$newContent = $this->contentReplace($content, array(
			'{{db_host}}' => $this->getData(self::KEY_DB_HOST),
			'{{db_name}}' => $this->getData(self::KEY_DB_NAME),
			'{{db_user}}' => $this->getData(self::KEY_DB_USER),
			'{{db_pass}}' => $this->getData(self::KEY_DB_PASS),
			'{{db_port}}' => $this->getData(self::KEY_DB_PORT, 3306),
			'{{db_unix_socket}}' => $unix_socket_value,
		));

		$this->createFile($target, $newContent);
	}

	/**
	 * Process mysql Import
	 *
	 * @return bool
	 * @throws Exception
	 */
	protected function processMysqlImport()
	{
		$pdo = $this->getDbInstance();
		$raw_sql = $this->getContent($this->getSqlPath());

		return $pdo->query($raw_sql);
	}

	/**
	 * Get sql path
	 *
	 * @return string
	 */
	protected function getSqlPath()
	{
		return $this->getConfig('paths.sql');
	}

	/**
	 * Create admin User
	 *
	 * @return mixed - The created user
	 */
	protected function createAdminUser()
	{
		$vars = array(
			'username' 	=> $this->getData(self::KEY_ADMIN_USERNAME),
			'email' 	=> $this->getData(self::KEY_ADMIN_EMAIL),
			'password' 	=> $this->getData(self::KEY_ADMIN_PASSWORD),
			'activated' => 1,
			'status' => 1,
		);

		$result = Sentry::user()->create($vars);

		if ($result) {
			$user = \Sentry::user($result);
			$user->add_to_group(1); // add as super admin
			$this->flag_admin_created = true;
		}

		return Sentry::user($result);
	}

	/**
	 * Set Data value
	 *
	 * @param  array $data
	 * @return $this
	 */
	public function setData($data = []) {
		$this->data = $data;
		return $this;
	}

	/**
	 * Get data value
	 *
	 * @param  string $key
	 * @param  mixed $default
	 * @return mixed
	 */
	public function getData($key, $default = null)
	{
		return array_key_exists($key, $this->data) ? $this->data[$key] : $default;
	}

	/**
	 * Get configuration data
	 *
	 * @param  string $key
	 * @param  mixed $default
	 * @return mixed
	 */
	public function getConfig($key, $default = null)
	{
		return Config::get('setup.'.$key, $default);
	}

	/**
	 * Replace all data instancs
	 *
	 * @param  $content
	 * @param  array $data
	 * @return mixed
	 */
	public function contentReplace($content, $data = array())
	{
		foreach ($data as $key=>$value) {
			$content = str_replace($key, $value, $content);
		}

		return $content;
	}

	/**
	 * Get content of a file
	 *
	 * @param  $path
	 * @return string
	 * @throws Setup_Exception
	 */
	public function getContent($path)
	{
		if (!File::exists($path)) {
			throw new Setup_Exception('Unable to find path: ' .$path);
		}

		return file_get_contents($path);
	}

	/**
	 * Create config file
	 *
	 * @param  $file_path
	 * @param  $content
	 * @return bool
	 * @throws \FileAccessException
	 * @throws \InvalidPathException
	 */
	public function createFile($file_path, $content)
	{
		$path = pathinfo($file_path);

		// delete existing file.
		File::exists($file_path) && File::delete($file_path);

		return File::create($path['dirname'], $path['basename'], $content);
	}

	/**
	 * Get the setup flag path
	 *
	 * @return string
	 */
	public function getSetupFlagPath()
	{
		return $this->getConfig('paths.flag');
	}

}