<?php namespace Setup;

use Fuel\Core\Input;
use Fuel\Core\Response;
use Fuel\Core\Session;
use Fuel\Core\View;
use Messages;
use Controller_Base_Public;
use Fuel\Core\Theme;
use Exception;

/**
 * Setup Controller
 *
 * @package  Setup
 * @author Lightmedia
 * @extends  Controller
 */
class Controller_Setup extends Controller_Base_Public
{
	const STATUS_ERROR = 'error';
	const STATUS_SUCCESS = 'success';

	/**
	 * Is this a setup controller ?
	 *
	 * @var bool
	 */
	var $is_setup_controller = true;

	/**
	 * The installer instance
	 *
	 * @var Setup_Installer
	 */
	protected $installer;


	/**
	 * Why we don't call parent::before or Class_Name::before ?
	 *
	 * Simple because at this time, there is no default configurations for databases, mails and etc. which sometimes
	 * the parent methods of this method might have executes crud to database.
	 *
	 * @return Void
	 */
	public function before()
	{
		Theme::instance()->active('frontend');
		Theme::instance()->set_template($this->template);

		View::set_global('theme', Theme::instance(), false);
	}

	/**
	 * Create and return installer instance
	 *
	 * @return Setup_Installer
	 */
	private function getInstaller()
	{
		if (!$this->installer) {
			$this->installer = new Setup_Installer();
		}

		return $this->installer;
	}

	/**
	 * Setup view
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// if not allowed. redirect to home page with error message
		if (!$this->getInstaller()->isAllow()) {
			Messages::error('Unable to run setups.');

			Response::redirect('/');
			return null;
		}

		if (Input::post()) {
			if ($this->process() === true) {
				Messages::success('Setup has been completed successfully.');
				Response::redirect('/');
			}
		}

		Theme::instance()->set_template('views/empty');

		echo Theme::instance()
			->view(APPPATH.'modules'.DS.'setup'.DS.'view'.DS.'setup.php')
			->set('installer', $this->getInstaller(), false);
	}

	/**
	 * Process the setup
	 * 
	 * @access  public
	 * @return  bool
	 */
	private function process()
	{
		$this->getInstaller()->setData(Input::post());

		if ($test = Input::post('test')) {
			$data = [];

			try {
				switch ($test) {
					case 'db':
							$this->getInstaller()->testDB();

							$data = [
								'status' => self::STATUS_SUCCESS,
								'message' => 'Database connection is successful.'
							];
						break;
					case 'mail':
							$this->getInstaller()->testMail();

							$data = [
								'status' => self::STATUS_SUCCESS,
								'message' => 'Sending Email is successful.'
							];

						break;
					default:
						$data['status'] = self::STATUS_ERROR;
						break;
				}
			} catch (Exception $e) {
				$data = [
					'status' => self::STATUS_ERROR,
					'message' => $e->getMessage()
				];
			}

			header('Content-Type: application/json');
			echo json_encode($data);
			exit;
		}

		try {
			$this->installer->attempt();

			return true;
		} catch (Exception $e) {
			Messages::warning('Error: '.$e->getMessage());
			Session::write();
		}

		return false;
	}

}
