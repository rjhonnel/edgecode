<?php

return array(
	'paths' => array(
		'config' => array(
			'target' => APPPATH . 'config' . DS . 'config.php',
			'template' => APPPATH.'modules'.DS.'setup'.DS.'resources'.DS.'template'.DS.'config.php.template',
		),
		'db' => array(
			'target' => APPPATH . 'config' . DS . 'development' . DS . 'db.php',
			'template' => APPPATH.'modules'.DS.'setup'.DS.'resources'.DS.'template'.DS.'db.php.template',
		),
		'mail' => array(
			'target' => APPPATH . 'config' . DS . 'email.php',
			'template' => APPPATH.'modules'.DS.'setup'.DS.'resources'.DS.'template'.DS.'email.php.template',
		),

		'sql' => APPPATH.'modules'.DS.'setup'.DS.'resources'.DS.'edgecms.sql',
		'flag' => APPPATH.'config'.DS.'setup.flag',
	),
);