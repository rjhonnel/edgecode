## Setup

## How to trigger Setup ?

These following file must not exists in config directory in order to entered setup:

- app/config/db.php
- app/config/email.php
- app/config/setup.flag

Please refer to ```Setup_Installer::isAllow``` method for more info.

## Resources

### Config Template
```./resources/template/*.template``` should get updated when its source gets updated.

### Edgecms.sql
- When gets updated, there should no user of edgecms.sql as it will try to create admin user and it might
throw an exception if user entered an existing user.

## Authors

- Lightmedia <info@lightmedia.com.au>
- Rex <rex.t@lightmedia.com.au>