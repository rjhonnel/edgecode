<?php

namespace Newsletter;

class Controller_Newsletter extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/newsletter/';
	
	public $theme_view_path;
    
     // Global theme variables
    public $page_theme;
    
	public function before()
	{
		parent::before();
		
		\Config::load('newsletter::newsletter', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
		
		$this->theme_view_path = DOCROOT . \Theme::instance()->asset_path($this->view_dir);
	}
	
	/**
	 * Subscribe
	 * 
	 * @access  public
     * @param   $slug
	 */
	public function action_subscribe()
	{
        if(\Input::post())
        {
            // check for a valid CSRF token
            if (!\Security::check_token())
            {
                \Messages::error('CSRF attack or expired CSRF token.');
                \Response::redirect(\Input::referrer(\Uri::create('/')));
            }
            
            $val = $this->validate();

            if($val->run())
            {
                try
                {
                    //Connect to Demo Edgeconnect

                    $api_key  = 'ecbee428f0ba4ea488781024fd4f95f6';
                    $group_id = 84;
                    $endpoint = 'http://demo.edgeconnect.net';

                    $a_url = parse_url( $endpoint );
                    $api = new \Edgeconnect( $api_key, array( 'protocol' => $a_url['scheme'], 'host' => $a_url['host'] ) );

                    $page = \Uri::create('/');
                    $hold_message = 'Subscribe Newsletter&#10;&#10;&#10;This message is from this page : Edge Commerce - ' . $page.'&#10;IP Address : '.$_SERVER['REMOTE_ADDR'];


                    $return = $api->createGroupUser($group_id, array(
                            'email' => \Input::post('email'),
                            'first_name' => '',
                            'last_name' => '',
                            'company' => '',
                            'title' => '',
                            'phone' => '',
                            'message' => $hold_message,
                    ));

                    // Send autoresponder
                    $autoresponder = \Autoresponder\Autoresponder::forge();
                    $autoresponder->view_custom = 'database_backup';

                    $settings = \Config::load('autoresponder.db');
                    $content['company_name'] = $settings['company_name'];
                    $content['address'] = $settings['address'];
                    $content['website'] = $settings['website'];
                    $content['message'] = 'Thank you for subscribing.';
                    $content['subject'] = 'Successfully subscribed to our Newsletter';

                    $autoresponder->autoresponder_custom($content, \Input::post('email'));

                    $autoresponder->send();

                    \Messages::success('Thank you for subscribing.');
                }
                catch(\FuelException $e)
                {
                    \Messages::error($e->getMessage());
                }
            }
            else
            {
                if($val->error() != array())
                {
                    // show validation errors
                    \Messages::error('There was an error while trying to subscribe to newsletter.');
                    foreach($val->error() as $e)
                    {
                        \Messages::error($e->get_message());
                    }
                }
            }
        }
        
        \Response::redirect(\Input::referrer(\Uri::create('/')));
        
    }
    
    public function action_report()
    {
        \Theme::instance()->set_partial('content', $this->view_dir . 'report');
    }
    
    /**
    * Validate fields
    * 
    * @param $factory = Validation name, if you want to have multiple validations
    */
    protected function validate($factory = 'subscribe')
    {
       $val = \Validation::forge($factory);
       $val->add('email', 'Email')->add_rule('required')->add_rule('valid_email');

       return $val;
    }

}
