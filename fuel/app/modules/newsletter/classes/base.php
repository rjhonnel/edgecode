<?php
namespace Newsletter;

class Base
{
     /**
	 * @var	array	Pagination instances
	 */
	protected static $_instances = array();

	/**
	 * @var	array	Pagination default instance
	 */
	protected static $_instance = null;
    
    /**
	 * forge a new yousendit instance
	 *
	 * @return  a new yousendit instance
	 */
	public static function forge($name = 'default', $config = array())
	{
        \Config::load('newsletter::newsletter', 'details');
         
		if ($exists = static::instance($name))
		{
			//\Error::notice('Yousendit instance with this name exists already, cannot be overwritten.');
			return $exists;
		}

		static::$_instances[$name] = new static($config);

		if ($name == 'default')
		{
			static::$_instance = static::$_instances[$name];
		}

		return static::$_instances[$name];
	}

	/**
	 * retrieve an existing yousendit instance
	 *
	 * @return	a existing yousendit instance
	 */
	public static function instance($name = null)
	{
		if ($name !== null)
		{
			if ( ! array_key_exists($name, static::$_instances))
			{
				return false;
			}

			return static::$_instances[$name];
		}

		if (static::$_instance === null)
		{
			static::$_instance = static::forge();
		}

		return static::$_instance;
	}
    
    public function get_list($data = array())
    {
        require_once APPPATH. 'vendor/campaignmonitor/csrest_lists.php';

        $auth = array('api_key' => \Config::get('details.api_key'));
        $client_id = \Config::get('details.client_id');
        $list_id = \Config::get('details.list_id');
        $list = new \CS_REST_Lists($list_id, $auth);
        
        return $list->get();
    }
    
    public function add_subscriber($data = array())
    {
        require_once APPPATH. 'vendor/campaignmonitor/csrest_subscribers.php';

        $auth = array('api_key' => \Config::get('details.api_key'));
        $client_id = \Config::get('details.client_id');
        $list_id = \Config::get('details.list_id');
        $subscriber = new \CS_REST_Subscribers($list_id, $auth);
        
        return $subscriber->add($data);
    }
}