<?php

namespace Backup;

class Controller_Admin_Backup extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/backup/';
	
	public function before()
	{
		parent::before();
		
        if(!\Sentry::user()->in_group('Super Admin') )
        {
            \Response::redirect('admin/order/list'); 
        }
	}
	
	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_index()
	{
		$settings = \Config::load('backup.db');

        if (\Input::post()) {
        	$input = \Input::post();

        	if(!\Input::is_ajax())
			{
				$val = Model_Backup::validate('create');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create settings</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
				}
				else {
		        	try {

		        		\Config::save('backup.db', array(
		        					'enable' => $input['enable'],
		        					'email' => $input['email'],
		        					'period' => $input['period']
		        				));

		        		//save cronjob
		        		$output = shell_exec('crontab -l');

		        		$db_backup_cron_file = "/tmp/db_backup_cron.txt";      		

		        		if($input['enable'])
		        		{
		        			if($input['period'] == 'daily')
		        			{
		        				$daily_backup_command = '0 0 * * * wget '.\Uri::create('backup/execute');
		        				file_put_contents($db_backup_cron_file, $daily_backup_command.PHP_EOL);
		        			}
		        			else if($input['period'] == 'weekly')
		        			{
		        				$weekly_backup_command = '0 0 * * 0 wget '.\Uri::create('backup/execute');
		        				file_put_contents($db_backup_cron_file, $weekly_backup_command.PHP_EOL);
		        			}
		        		}
		        		else
		        		{
		        			file_put_contents($db_backup_cron_file, "".PHP_EOL);
		        		}
	        			exec("crontab $db_backup_cron_file");

		        		\Messages::success('Settings successfully created.');
						\Response::redirect('admin/backup');
		        	} 
		        	catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create settings.</strong>');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
					}
				}
	        }
        }
        \View::set_global('menu', 'admin/backup');
		\View::set_global('title','Backup');
		\Theme::instance()->set_partial('content', $this->view_dir . 'index')
		->set('settings', $settings, false);
	}
}