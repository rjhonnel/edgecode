<?php

/**
 * CMS
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Backup
 * @extends \Controller_Base_Public
 */

namespace Backup;

class Controller_Backup extends \Controller_Base_Public
{
    public function action_execute()
    {
        $database_settings = \Config::get('db.'.\Config::get('db.active'));
        $dbname = explode('dbname=', $database_settings['connection']['dsn']);
        $username = $database_settings['connection']['username'];
        $password = $database_settings['connection']['password'];

        $backupfile = $dbname[1] . date("Y-m-d") . '.sql';
        $backupzip = $backupfile . '.tar.gz';
        system('mysqldump  -u' . $username . ' -p' . $password . ' ' . $dbname[1] . ' > ' . $backupfile);
        system("tar -czvf $backupzip $backupfile");


        // Send autoresponder
        $autoresponder = \Autoresponder\Autoresponder::forge();
        $autoresponder->view_custom = 'database_backup';

        $settings = \Config::load('autoresponder.db');
        $content['company_name'] = $settings['company_name'];
        $content['address'] = $settings['address'];
        $content['website'] = $settings['website'];
        $content['message'] = 'This is your database backup from '.\Uri::create('/');
        $content['subject'] = 'Database Backup';

        $autoresponder->autoresponder_custom($content, 'backup', array(DOCROOT.$backupzip));

        if($autoresponder->send())
        {
            echo 'The mail has been sent successfully.';
        }
        else
        {
            echo 'There was an error while trying to sent email.';
        }

        // Delete the file from your server
        unlink($backupfile);
        unlink($backupzip);

        exit();
    }
}