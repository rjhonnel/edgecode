<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Supplier;

class Controller_Admin_Supplier extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/supplier/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('supplier::supplier', 'details');
        \Config::load('sentry', true);
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/supplier/list');
	}
	
    public function get_search_items($group_id = false)
    {
        // Override group_id if its a search
        $group_id = \Input::get('user_group', $group_id);

        $activated = \Input::get('activated', false);


        
        if($group_id && \SentryAdmin::group_exists((int)$group_id))
        {
            // Get only group users
            \View::set_global('group', \SentryAdmin::group((int)$group_id));
            $items = \SentryAdmin::group((int)$group_id)->users();
        }
        else
        {
            // Get all supplier
            $items = \SentryAdmin::user()->all('supplier');
        }
        
        // Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
        
        // Get user objects
        if(!empty($items))
        {
            foreach($items as $key=> $item)
            {
                $items[$key] = \SentryAdmin::user((int)$item['id']);
            }
            
            // Get search filters
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title': 
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                $full_name = $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name');
                                $customer_id = $item->get('metadata.user_id');

                                if(stripos($full_name, $value) === false && stripos($customer_id, $value) === false ) unset($items[$number]);
                            }
                            break;
                        case 'email':
                            foreach($items as $number => $item)
                            {
                                if(stripos($item->email, $value) === false) unset($items[$number]);
                            }
                            break;
                        case 'country':
                            if($value && $value !== 'false')
                            {
                                foreach($items as $number => $item)
                                {
                                    if(empty($item['metadata'])) 
                                    {
                                        unset($items[$number]);
                                        continue;
                                    }
                                    if(stripos($item->get('metadata.country'), $value) === false) unset($items[$number]);
                                }
                            }
                            break;
                        case 'postcode_from':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') < $value) unset($items[$number]);
                            }
                            break;
                        case 'postcode_to':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') > $value) unset($items[$number]);
                            }
                            break;
                        case 'activated':
                        	if($value !== 'false')
                            {
	                        	foreach($items as $number => $item)
	                            {
	                                if($item->activated != $value) unset($items[$number]);
	                            }
                        	}
                        	break;
                    }
                }
            }
        }

        // Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
        return array('items' => $items, 'pagination' => $pagination);
    }
    
    /**
     * List all supplier users
     * 
     * @param $group_id    = Filter users by group ID
     */
	public function action_list($group_id = false)
	{
        \View::set_global('menu', 'admin/supplier/list');
		\View::set_global('title', 'List suppliers');
		
        $search = $this->get_search_items($group_id);
		
        $items      = $search['items'];
        $pagination = $search['pagination'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
	
	public function action_create()
	{
        \View::set_global('menu', 'admin/supplier/list');
		\View::set_global('title', 'Add New Supplier');
		
        // Get groups
        $groups = \SentryAdmin::group()->all('supplier');
        
		if(\Input::post())
		{
			$val = \Supplier\Controller_Admin_Validate::forge('create');
			
			if($val->run())
			{
                // Get POST values
				$insert = \Input::post();
				unset($insert['business_name2']);
				unset($insert['shipping_business_name2']);
				
				if(!\Input::post('on_account')){
					$insert['company'] = '';
					$insert['on_account'] = 0;
					$insert['business_name'] = \Input::post('business_name2');
				}
				else{
					$insert['on_account'] = 1;
				}

				array_walk($insert, create_function('&$val', '$val = trim($val);'));
				
			    try
			    {
                    // Generate random username
			    	//$username 	= 'user' . \Str::random('numeric', 16);
                    $username 	  = $insert['email'];
			    	$email 		  = $insert['email'];
			    	$password 	  = $insert['password']?$insert['password']:\Str::random('numeric', 16);
			    	$user_group	  = $insert['user_group'];
                    $activated	  = $insert['activated'];
                    $email_client = isset($insert['email_client'])?$insert['email_client']:0;
                    $insert['guest'] = $user_group == 3 ? 1: 0;
			    	unset($insert['email'], $insert['password'], 
                            $insert['confirm_password'], $insert['user_group'], 
                            $insert['details'], $insert['save'], 
                            $insert['update'], $insert['activated'],
                            $insert['email_client']);
			    	
                    $only_billing = array('business_name', 'purchase_limit_value' , 'purchase_limit_period', 'master', 'note', 'credit_account', 'guest');
                    // Set shipping data to be same as billing by default
                    /*foreach($insert as $key => $value)
                    {
                        if(!in_array($key, $only_billing) && strpos($key, 'shipping_') === false)
                        {
                            if(empty($insert['shipping_'.$key]))
                                $insert['shipping_'.$key] = $value;
                        }
                    }*/
                    
				    // create the user - no activation required
				    $vars = array(
					    'username' 	=> $username,
					    'email' 	=> $email,
					    'password' 	=> $password,
					    'metadata' 	=> $insert,
                        'activated' => $activated,
				    );
				    $user_id 	= \SentryAdmin::user()->create($vars);
				    $user 		= \SentryAdmin::user($user_id);
                    
                    // Send email to user with new password
                    if($email_client == 1 && !empty($vars['password']))
                    {
                        $email_data = array(
                            'site_title' 			=> \Config::get('site_title'),
                            'customer_identity'     => ucwords($user->get('metadata.first_name').' '.$user->get('metadata.last_name')),
                            'new_password'			=> $vars['password']
                        );
                        $this->autoresponder($user, $email_data);
                    }
				    
				    // Add user to 'customer' group (id = 3)
				    if($user_id and $user->add_to_group($user_group))
				    {
				    	\Messages::success('User successfully created.');
                        \Response::redirect(\Input::post('update', false) ? \Uri::create('admin/supplier/update/' . $user->id) : \Uri::admin('current'));
				    }
				    else
				    {
				    	// show validation errors
						\Messages::error('<strong>' . 'There was an error while trying to create user' . '</strong>');
				    }
			    }
			    catch (\Sentry\SentryUserException $e)
			    {
			    	// show validation errors
					\Messages::error('<strong>' . 'There was an error while trying to create user' . '</strong>');
			    	$errors = $e->getMessage();
			    	\Messages::error($errors);
			    }
            }
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>' . 'There was an error while trying to create supplier' . '</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
        
        \Theme::instance()->set_partial('content', $this->view_dir . 'create')
            ->set('groups', $groups);
	}
	
	public function action_update($id = false)
	{
        
		if(!is_numeric($id)) \Response::redirect('admin/supplier/list');
		
		// Get supplier to edit
		if(!\SentryAdmin::user_exists((int)$id)) \Response::redirect('admin/supplier/list');
        \View::set_global('menu', 'admin/supplier/list');
		\View::set_global('title', 'Edit Supplier');
		
        // Get groups
        $groups = \SentryAdmin::group()->all('supplier');
        
		// Update group details
		if(\Input::post('details', false))
		{
            $item = new \Sentry_User((int)$id);
			$val = \Supplier\Controller_Admin_Validate::forge('update', $item['id']);
			
			if($val->run())
			{
                // Get POST values
				$insert = \Input::post();
				unset($insert['business_name2']);
				unset($insert['shipping_business_name2']);
				
				if(!\Input::post('on_account')){
					$insert['company'] = '';
					$insert['on_account'] = 0;
					$insert['business_name'] = \Input::post('business_name2');
				}
				else{
					$insert['on_account'] = 1;
				}

				array_walk($insert, create_function('&$val', '$val = trim($val);'));
				
			    try
			    {
                    // Generate random username
			    	//$username 	= 'user' . \Str::random('numeric', 16);
                    $username 	  = $insert['email'];
			    	$email 		  = $insert['email'];
			    	$password 	  = $insert['password'];
			    	$user_group	  = $insert['user_group'];
                    $activated	  = $insert['activated'];
                    $email_client = $insert['email_client'];
                    $insert['guest'] = $user_group == 3 ? 1: 0;
			    	unset($insert['email'], $insert['password'], 
                            $insert['confirm_password'], $insert['user_group'], 
                            $insert['details'], $insert['save'],  
                            $insert['exit'], $insert['activated'],
                            $insert['email_client']);
			    	
                    $only_billing = array('business_name', 'purchase_limit_value' , 'purchase_limit_period', 'master', 'note', 'credit_account', 'guest');
                    
				    // create the user - no activation required
				    $vars = array(
					    'username' 	=> $username,
					    'email' 	=> $email,
					    'password' 	=> $password,
					    'metadata' 	=> $insert,
                        'activated' => $activated,
				    );
                    
                    // Send email to user with new password
                    if($email_client == 1 && !empty($vars['password']))
                    {
                        $email_data = array(
                            'site_title' 			=> \Config::get('site_title'),
                            'customer_identity'     => ucwords($item->get('metadata.first_name').' '.$item->get('metadata.last_name')),
                            'new_password'			=> $vars['password']
                        );
                        $this->autoresponder($item, $email_data);
                    }
                    
                    if(empty($vars['password'])) unset($vars['password']);
					
				    if($item->update($vars))
				    {
                        //Change user group if needed
                        $user_groups = $item->groups();
                        if(!empty($user_groups))
                        {
                            // Remove user from all other groups...
                            foreach($user_groups as $value)
                            {
                                $item->remove_from_group((int)$value['id']);
                            }
                        }
                        
                        $item = new \Sentry_User((int)$id);
                        // ...and add it to selected one
                        $item->add_to_group((int)$user_group);
                        
				    	\Messages::success('Supplier successfully updated.');
                        \Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/supplier/list/') : \Uri::admin('current'));
				    }
				    else
				    {
				    	// show validation errors
						\Messages::error('<strong>' . 'There was an error while trying to update supplier' . '</strong>');
				    }
			    }
			    catch (\Sentry\SentryUserException $e)
			    {
			    	// show validation errors
					\Messages::error('<strong>' . 'There was an error while trying to update supplier' . '</strong>');
			    	$errors = $e->getMessage();
			    	\Messages::error($errors);
			    }
            }
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>' . 'There was an error while trying to update supplier' . '</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		$user = new \Sentry_User((int)$id);
		
        // Get single user group
        $user_group = $user->groups();
        $user_group = current($user_group);
        
        $user->group = $user_group;
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')
                ->set('user', $user)
                ->set('groups', $groups);
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get user delete
			if($item = \SentryAdmin::user((int)$id))
			{
				// Delete item
				try{
					// Delete group
					$item->delete();
					
					\Messages::success('Supplier successfully deleted.');
				}
				catch (\Sentry\SentryUserException $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete supplier</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
    /**
     * Validate Model fields
     * 
     * @param $factory = Validation name, if you want to have multiple validations
     */
    public static function validate($factory)
    {
       $val = \Validation::forge($factory);
       $val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);

       return $val;
    }     
    
    protected function autoresponder($user, $data)
    {
        \Theme::instance()->active('frontend');
        \View::set_global('theme', \Theme::instance(), false);
        
        // Send autoresponder
        $autoresponder = \Autoresponder\Autoresponder::forge();

        $autoresponder->user_id =  $user->id;

        $autoresponder->view_user = 'new_password';

        $content['subject'] = 'New Password';
        $content['content'] = $data;
        $autoresponder->autoresponder_user($content);


        if($autoresponder->send())
        {
            \Messages::success('The mail has been sent successfully.');
        }
        else
        {
            \Messages::error('<strong>' . 'There was an error while trying to sent email.' . '</strong>');
        }
        
        \Theme::instance()->active('admin');
    }
}
                            
                            