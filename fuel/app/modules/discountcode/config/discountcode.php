<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
	'type' => array(
		'value'			=>  'Value',
		'percentage'	=>  'Percentage',
		'free shipping'	=>  'Free Shipping',
	),
	'status' => array(
		'active'			=>  'Active',
		'inactive'			=>  'Inactive',
	),
	'use_type' => array(
		'single use'		=>  'Single Use',
		'multi use'			=>  'Multi Use',
	),
 );