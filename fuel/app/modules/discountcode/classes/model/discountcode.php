<?php 

/**
 * CMS
 *
 * @package    
 * @version    
 * @author     
 *
 * @namespace Discountcode
 * @extends \Model_Base
 */

namespace Discountcode;

class Model_Discountcode extends \Model_Base
{
	// Temporary variable to store some query results
	public static $temp = array();

	// Global variable to force refreshing of cached lazy load results
	public static $refresh_lazy_load = false;

	// Set the table to use
	protected static $_table_name = 'discount_codes';

	// List of all columns that will be used on create/update
	protected static $_properties = array(
		'id',
		'code',
		'type',
		'is_applicable_to_samples',
		'status',
		'active_from',
		'active_to',
		'type_value',
		'min_order_value',
		'max_order_value',
		'use_type',
		'code_not_required',
		'category_list',
	);
		protected static $_defaults = array(
		    'is_applicable_to_samples' => 1,
		);
	
	/**
	 * Validate Model fields
	 * 
	 * @param $factory = Validation name, if you want to have multiple validations
	 */
	public static function validate($factory, $id = false)
	{
		$val = \Validation::forge($factory);

		if(\Input::post('code_not_required'))
		{
			$val->add('category_list', 'Category')->add_rule('required');
		}
		else
		{
			$val->add('code', 'Discount Code')
				->add_rule('required')
				->add_rule('unique', array(self::$_table_name, 'code', $id, 'id'))
				->add_rule('min_length', 2)
				->add_rule('max_length', 10);
		}

		$val->add('type', 'Discount Type')
			->add_rule('required')
			->add_rule('match_collection', array('value','percentage','free shipping'));
		$val->add('active_from', 'Active Date From')
			->add_rule('required_with', 'active_to')
			->add_rule('valid_date', 'd/m/Y');
		$val->add('active_to', 'Active Date To')
			->add_rule('required_with', 'active_from')
			->add_rule('valid_date', 'd/m/Y');
		$val->add('min_order_value', 'Minimum order')
			->add_rule('numeric_min',0);
		$val->add('max_order_value', 'Maximum order')
			->add_rule('numeric_min',0);

		return $val;
	}

	/*
	* Requires product id and product subtotal price in the order( subtotal_price = product_price * quantity )
	*/
	public static function getCategoryDiscount($product_id = false, $product_subtotal = 0)
	{
		$total_discount = 0;

        if($product_id && $product_subtotal)
        {
        	$discount_cnr = Model_Discountcode::find(function($query){
	            $query->where('code_not_required', 1);
	            $query->where('status', 'active');
	        });
	        $product = \Product\Model_Product::find_one_by_id($product_id);

        	if($discount_cnr && $product && $product->categories)
        	{
        		foreach ($discount_cnr as $dcnr)
        		{
        			$categorie_ids = json_decode($dcnr->category_list);
        			$dicount_product_ids = array();
        			foreach ($categorie_ids as $category_id)
        			{
        				$category = \Product\Model_Category::find_one_by_id($category_id);
    					$dicount_product_ids = array_merge($dicount_product_ids, array_keys($category->all_products));
        			}

    				if($dicount_product_ids)
    				{
    					if(in_array($product_id, $dicount_product_ids))
    					{
    						$valid = false;

    						// check for expiry date
							if ($dcnr->active_from != '0000-00-00' && $dcnr->active_to != '0000-00-00')
							{
								$current_date = strtotime(date('Y-m-d'));
								$from = strtotime($dcnr->active_from);
								$to = strtotime($dcnr->active_to);
								if ($current_date >= $from && $current_date <= $to)
								{
									$valid = true;
								}
							}
							else
							{
								$valid = true;
							}

							if ($valid)
							{
								$valid = false;
								// check for single/multiple use
								if ($dcnr->use_type == 'multi use')
								{
									$valid = true;
								}
								else
								{
									// check if discount code was already used.
									$order_exists = \Order\Model_Order::find_one_by_id_discount($dcnr->id);
									if (is_null($order_exists))
									{
										$valid = true;
									}
								}	
							}

							if ($valid)
							{
								//check order value min and max (use product_subtotal instead of overall order total)
			                    if ($dcnr->min_order_value != 0 || $dcnr->max_order_value != 0)
			                    {
			                        if($dcnr->max_order_value != 0)
			                        {
			                            if($product_subtotal >= $dcnr->min_order_value && $product_subtotal <= $dcnr->max_order_value)
			                                $valid = true;
			                            else
			                                $valid = false;
			                        }
			                        else
			                        {
			                            if($product_subtotal >= $dcnr->min_order_value)
			                                $valid = true;
			                            else
			                                $valid = false;
			                        }
			                    }
			                    else
			                    {
			                        $valid = true;
			                    }

			                    if ($valid)
			                    {
									$f_discount = 0;

			    					// compute value of discount
			    					switch($dcnr->type)
			    					{
			    						case 'free shipping':
			    								$order = \Order\Model_Order::forge();
			    								$shipping_price = $order->shipping_price(null, null, true);

			    								$f_discount = $shipping_price;
			    							break;
			    						case 'percentage': 		$f_discount = $product_subtotal * ($dcnr->type_value/100); break;
			    						case 'value': 			$f_discount = $dcnr->type_value; break;
			    					}

			    					$total_discount += round($f_discount, 2);
			                    }
							}
    					}
    				}
        		}
        	}
        }

        return $total_discount;
	}
}