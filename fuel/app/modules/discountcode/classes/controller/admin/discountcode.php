<?php

/**
 * The Discount Codes Admin Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Discountcode;

class Controller_Admin_Discountcode extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/discountcode/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('discountcode::discountcode', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
        if(\SentryAdmin::user()->is_production())
        {
            $arr = array('list');
            if(!in_array(\Uri::segment(4), $arr)){
                \Response::redirect('admin/order/list'); 
            }
        }
	}
	
	/**
	 * Index
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/discountcode/list');
	}
    
    public function action_create()
	{

        \View::set_global('menu', 'admin/discountcode');
		\View::set_global('title', 'Add Discount Codes');
		
		if(\Input::post())
		{
			$val = Model_Discountcode::validate('create');
			if (\Input::post('type') != 'free shipping')
			{
				$val->add('type_value', 'Amount')->add_rule('required')->add_rule('numeric_min', 1);
			}
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				if (isset($insert['active_from']) && $insert['active_from'])
				{
					$insert['active_from'] = date('Y-m-d', strtotime(str_replace('/', '-', $insert['active_from'])));
				}
				if (isset($insert['active_to']) && $insert['active_to'])
				{
					$insert['active_to'] = date('Y-m-d', strtotime(str_replace('/', '-', $insert['active_to'])));
				}

				if(\Input::post('category_list'))
				{
					$insert['category_list'] = json_encode(\Input::post('category_list'));
				}

				$item = Model_Discountcode::forge($insert); 
				
				try{
					$item->save();
					
					\Messages::success('Discount code successfully created.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/discountcode/list/') : \Uri::create('admin/discountcode/update/' . $item->id));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create discount</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create discount code</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}

		// Keep existing messages
		\Messages::instance()->shutdown();
		\Theme::instance()->set_partial('content', $this->view_dir . 'create');
	}
	
    /**
	 * Update
	 * 
	 * @access  public
	 * @return  Response
	 */
    public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/discountcode/list');
		
		// Get item to edit
		if(!$item = Model_Discountcode::find_one_by_id($id)) \Response::redirect('admin/discountcode/list');
        \View::set_global('menu', 'admin/discountcode');
		\View::set_global('title', 'Discount Codes Update');
		
		
		if(\Input::post())
		{
			$val = Model_Discountcode::validate('update', $item->id);
			
			if (\Input::post('type') != 'free shipping')
			{
				$val->add('type_value', 'Amount')->add_rule('required')->add_rule('numeric_min', 1);
			}
			if($val->run())
			{
				$insert = \Input::post();
				if (isset($insert['active_from']) && $insert['active_from'])
				{
					$insert['active_from'] = date('Y-m-d', strtotime(str_replace('/', '-', $insert['active_from'])));
				}
				if (isset($insert['active_to']) && $insert['active_to'])
				{
					$insert['active_to'] = date('Y-m-d', strtotime(str_replace('/', '-', $insert['active_to'])));
				}

				if(\Input::post('category_list'))
				{
					$insert['category_list'] = json_encode(\Input::post('category_list'));
				}
				
				$item->set($insert);
				try{
					$item->save();
					
					\Messages::success('Discount successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/discountcode/list/') : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update discount code</strong>');
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update discount code</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		$discountcode = Model_Discountcode::find_one_by_id($id);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('item', $discountcode);
	}
	
	/**
     * Delete Order
     * 
     * @param type $id  = Order ID
     */
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get discount code to delete
			if($item = Model_Discountcode::find_one_by_id($id))
			{
				try
				{
					$item->delete();
					\Messages::success('Discount successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					\Messages::error('<strong>' . 'There was an error while trying to delete discount' . '</strong>');
				}
			}
			else
			{
				\Messages::error('<strong>' . 'Discount code does not exists' . '</strong>');	
			}
		}
		
		\Response::redirect(\Input::referrer(\Uri::create('admin/discountcode/list')));
	}
    
    public function action_assign_customer_groups($user_id = false)
	{
        \View::set_global('title', 'Discount Codes Assign Customer Groups');
        
        $search = $this->get_search_items($user_id);
        
		$items      = $search['items'];
        $pagination = $search['pagination'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'assign_customer_groups')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
    
    public function action_assign_products()
	{
        \View::set_global('title', 'Discount Codes Assign Products');
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'assign_products');
	}
    
    public function action_assign_product_groups()
	{
        \View::set_global('title', 'Discount Codes Assign Product Groups');
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'assign_product_groups');
	}
    
    public function action_assign_product_categories()
	{
        \View::set_global('title', 'Discount Codes Assign Product Categories');
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'assign_product_categories');
	}
    
    
    public function get_search_items()
    {        
    	$filters = array(
    		'title' => \Input::get('title'),
    		'discount_status' => \Input::get('discount_status'),
    		'discount_type' => \Input::get('discount_type'),
    		'type' => \Input::get('type'),
    	);
        $items = Model_Discountcode::find(function($query) use ($filters){
            if($filters['title'])
                $query = $query->where('code', 'like', '%'.$filters['title'].'%');
            if($filters['discount_status'])
                $query = $query->where('status', $filters['discount_status']);
            if($filters['discount_type'])
                $query = $query->where('type', $filters['discount_type']);
            if($filters['type'])
                $query = $query->where('use_type', $filters['type']);

            $query->order_by('id', 'desc');
        });
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
        return array('items' => $items, 'pagination' => $pagination);
    }
    
	
	/**
	 * List pages
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_list()
	{
        \View::set_global('menu', 'admin/discountcode');
        \View::set_global('title', 'Discount Codes List');
        
        $search = $this->get_search_items();
        
		$items      = $search['items'];
        $pagination = $search['pagination'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
}
