<?php
/**
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    1.0
 * @author     CMS Development Team
 * 
 * @namespace User  
 * @extends \Admin\Controller_Base
 */

namespace Settings;

class Controller_Admin_User extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/settings/user/';
	
	public function before()
	{
		parent::before();
        if(!\Sentry::user()->in_group('Super Admin') )
        {
            $arr = array('update');
            if(!in_array(\Uri::segment(4), $arr)){
                \Response::redirect('admin/order/list'); 
            }
        }
	}
	
	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_index()
	{
		//Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/settings/user/list');
	}
	
    public function get_search_items($group_id = false)
    {
        // Override group_id if its a search
        $group_id = \Input::get('user_group', $group_id);
        
        if($group_id && \SentryAdmin::group_exists((int)$group_id))
        {
            // Get only group users
            \View::set_global('group', \SentryAdmin::group((int)$group_id));
            $items = \SentryAdmin::group((int)$group_id)->users();
        }
        else
        {
            $items = \SentryAdmin::user()->all('admin');
        }
        
        // Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
        
        // Get user objects
        if(!empty($items))
        {
            foreach($items as $key=> $item)
            {
                $items[$key] = \SentryAdmin::user((int)$item['id']);
            }
            
            // Get search filters
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title': 
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                $full_name = $item->get('metadata.first_name') . ' ' . $item->get('metadata.last_name');
                                if(stripos($full_name, $value) === false) unset($items[$number]);
                            }
                            break;
                        case 'email':
                            foreach($items as $number => $item)
                            {
                                if(stripos($item->email, $value) === false) unset($items[$number]);
                            }
                            break;
                        case 'country':
                            if($value && $value !== 'false')
                            {
                                foreach($items as $number => $item)
                                {
                                    if(empty($item['metadata'])) 
                                    {
                                        unset($items[$number]);
                                        continue;
                                    }
                                    if(stripos($item->get('metadata.country'), $value) === false) unset($items[$number]);
                                }
                            }
                            break;
                        case 'postcode_from':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') < $value) unset($items[$number]);
                            }
                            break;
                        case 'postcode_to':
                            foreach($items as $number => $item)
                            {
                                if(empty($item['metadata'])) 
                                {
                                    unset($items[$number]);
                                    continue;
                                }
                                if($item->get('metadata.postcode') > $value) unset($items[$number]);
                            }
                            break;
                    }
                }
            }
        }

        // Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
        return array('items' => $items, 'pagination' => $pagination);
    }

	public function action_list($group_id = false) {

        \View::set_global('menu', 'admin/settings/user/list');
		\View::set_global('title', 'Admin Users');
		
        $search = $this->get_search_items($group_id);
		
        $items      = $search['items'];
        $pagination = $search['pagination'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
	
	/**
	 * Update user profile
	 * 
	 * @param int 
	 * @access public
	 * @return void
	 */
	public function action_update($id = false)
	{
        if(!\Sentry::user()->in_group('Super Admin') )
        {
            if($id != \Sentry::user()->id)
            	\Response::redirect('admin/order/list');
        }

		if (! is_numeric($id)) \Response::redirect('admin/settings/user/list');
		// Cast user id param to int
		$id = (int)$id;
		// Redirect if user don't exists
		if (! \SentryAdmin::user_exists($id)) \Response::redirect('admin/settings/user/list');
		
		$user = \SentryAdmin::user($id);
		$user_group = new \Sentry_User((int)$id);
		$user_group1 = $user_group->groups();
		$user_group2 = current($user_group1);

		$user_data = array(
			'id'			=> $user->get('id'),
			'first_name' 	=> $user->get('metadata.first_name'),
			'last_name' 	=> $user->get('metadata.last_name'),
			'email' 		=> $user->get('email'),
			'username' 		=> $user->get('username'),
			'user_group' 		=> $user_group2['name']
		);

		if(\Input::post())
		{
			// Validate input parameters
			$val = \Validation::forge('admin_details_validation');
			$val->add('first_name', 'First Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
			$val->add('last_name', 'Last Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
			$val->add('email', 'Email')->add_rule('required')->add_rule('valid_email');
			$val->add('password', 'Password')->add_rule('min_length', 8);
			$val->add('confirm_password','Confirm Password')->add_rule('required_with','password')->add_rule('match_field','password');
			$val->add('username', 'Username')->add_rule('required')->add_rule('unique',array('users','username',$id));	
		
			if($val->run())
			{
				// Get Input parameters
				$post_data = \Input::post();
			
				try
				{
					$fields = array(
							'username' 	=> $post_data['username'],
							'email' 	=> $post_data['email'],
							'password'	=> $post_data['password'],
							'user_group'	=> $post_data['user_group'],
							'metadata'	=> array(
									'first_name' => $post_data['first_name'],
									'last_name'	 => $post_data['last_name']
							)
					); 
						
					if(empty($post_data['password'])) unset($fields['password']);
					
					$item = new \Sentry_User((int)$id);
					$update = $item->update($fields);
						
					if($update)
					{
                        $user_groups = $item->groups();
                        if(!empty($user_groups))
                        {
                            // Remove user from all other groups...
                            foreach($user_groups as $value)
                            {
                                $item->remove_from_group((int)$value['id']);
                            }
                        }
                        
                        $item = new \Sentry_User((int)$id);
                        // ...and add it to selected one
                        $item->add_to_group((int)$fields['user_group']);

						\Messages::success('User Details Successfully updated.');
						\Response::redirect(\Uri::admin('current'));
					}
					else
					{
						\Messages::error('There was an error while trying to update User details.');
					}
				}
				catch(\Sentry\SentryUserException $e)
				{
					\Messages::error($e->getMessage());
				}
			}
			else
			{
				if($val->error() != array())
				{
					// Show validation errors
					\Messages::error('<strong>There was an error while trying to update User details</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
			
		}

        \View::set_global('menu', 'admin/settings/user/list');
		\View::set_global('title','Update Admin User');
		\Theme::instance()->set_partial('content',$this->view_dir . 'update')->set('user_data',$user_data);
	}
	
	
	
	public function action_create()
	{
        \View::set_global('menu', 'admin/settings/user/list');
		\View::set_global('title', 'Add New Admin User');

        
		if(\Input::post())
		{
			// Validate input parameters
			$val = \Validation::forge('admin_details_validation');
			$val->add('first_name', 'First Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
			$val->add('last_name', 'Last Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
			$val->add('email', 'Email')->add_rule('required')->add_rule('valid_email');
			$val->add('password', 'Password')->add_rule('min_length', 8);
			$val->add('confirm_password','Confirm Password')->add_rule('required_with','password')->add_rule('match_field','password');
			$val->add('username', 'Username')->add_rule('required')->add_rule('unique',array('users','username'));	
		
			if($val->run())
			{
				// Get Input parameters
				$post_data = \Input::post();
			
				try
				{
					$fields = array(
							'username' 	=> $post_data['username'],
							'email' 	=> $post_data['email'],
							'password'	=> $post_data['password'],
							'metadata'	=> array(
									'first_name' => $post_data['first_name'],
									'last_name'	 => $post_data['last_name']
							)
					); 
							$user_group	= $post_data['user_group'];
						
					if(empty($post_data['password'])) unset($fields['password']);
					
					$item = new \Sentry_User();
					$create = $item->create($fields);
				    $user 		= \SentryAdmin::user($create);
						
					if($create and $user->add_to_group($user_group))
					{

						\Messages::success('User Details Successfully Created.');
						\Response::redirect(\Uri::admin('current'));
					}
					else
					{
						\Messages::error('There was an error while trying to update User details.');
					}
				}
				catch(\Sentry\SentryUserException $e)
				{
					\Messages::error($e->getMessage());
				}
			}
			else
			{
				if($val->error() != array())
				{
					// Show validation errors
					\Messages::error('<strong>There was an error while trying to update User details</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
        
        \Theme::instance()->set_partial('content', $this->view_dir . 'create');
	}
	
	
	
}