<?php
/**
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    1.0
 * @author     CMS Development Team
 * 
 * @namespace User  
 * @extends \Admin\Controller_Base
 */

namespace Settings;

class Controller_Admin_Settings extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/settings/option/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('settings::settings', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
        if(!\Sentry::user()->in_group('Super Admin') )
        {
            \Response::redirect('admin/order/list'); 
        }
	}

	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_index()
	{


		$settings = \Config::load('autoresponder.db');
		// $autoResponder = Model_Setting::find(array('where' => array(array('meta_key', '=', 'auto-responders'))));


        if (\Input::post()) {
        	$input = \Input::post();
        	
        	if(!\Input::is_ajax())
			{
				$val = Model_Setting::validate('create');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create settings</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
				}
				else {

		        	try {
		        		$logo_url = (isset($settings['logo_url']) ? $settings['logo_url'] : '');
		        		if(\Input::file())
		        		{
			        		$image = $this->upload_image();
			        		if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->image))
							{
								// No previous images and image is not selected and it is required
								\Messages::error('<strong>There was an error while trying to upload product image</strong>');
								\Messages::error('You have to select image');
								\Response::redirect('admin/settings');
							}
							elseif($image['errors'])
							{
								\Messages::error('<strong>There was an error while trying to upload product image</strong>');
								foreach($image['errors'] as $error) \Messages::error($error);
								\Response::redirect('admin/settings');
							}

							if($image['is_valid'] && !(!$image['exists']))
							{
								if($this->_image_data)
								{
									chmod($this->_image_data[0]['saved_to'].$this->_image_data[0]['saved_as'], 0755);
									chmod($this->_image_data[0]['saved_to'].'large/'.$this->_image_data[0]['saved_as'], 0755);
									chmod($this->_image_data[0]['saved_to'].'medium/'.$this->_image_data[0]['saved_as'], 0755);
									chmod($this->_image_data[0]['saved_to'].'thumbs/'.$this->_image_data[0]['saved_as'], 0755);
									
									// remove old image
									if($logo_url)
										$this->delete_image($logo_url);
									
									$logo_url = $this->_image_data[0]['saved_as'];
								}
							}
		        		}

		        		\Config::save('autoresponder.db', array(
		        					'logo_url' => $logo_url,
		        					'company_name' => $input['company_name'],
		        					'address' => $input['address'],
		        					'website' => $input['website'],
		        					'phone' => $input['phone'],
		        					'email_address' => $input['email_address'],
		        					'email_address' => $input['email_address'],
		        					'sender_email_address' => $input['sender_email_address'],
		        					'contact_us_email_address' => $input['contact_us_email_address'],
		        					'facebook_account_name' => $input['facebook_account_name'],
		        					'instagram_account_name' => $input['instagram_account_name'],
		        					'bank_details' => $input['bank_details'],
		        					'gst' => $input['gst'],
		        					'gst_percentage' => $input['gst_percentage'],
		        					'delay_sending_invoice' => $input['delay_sending_invoice']
		        				));

		        		
		        		// $setting->save();

		        		\Messages::success('Settings successfully updated.');
						\Response::redirect('admin/settings');
		        	} 
		        	catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create settings.</strong>');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
					}
				}
			}
        }

		\View::set_global('title','Settings');
        \View::set_global('menu', 'admin/settings');
		\Theme::instance()->set_partial('content', $this->view_dir . 'index')
		->set('settings', $settings, false);
	}

	public function upload_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->image_upload_config = array(
		    'path' => \Config::get('details.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
		    'max_size' => 500000,
		);
		
		\Upload::process($this->image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save image
		    \Upload::save();
			$this->_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_image_data as $image_data)
				{
					chmod($image_data['saved_to'].$image_data['saved_as'], 0755);

					$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
				@unlink($folder . $name);
			}
		}
	}

	public function action_delete_logo()
	{
		if (\Input::get('name'))
		{
			$this->delete_image(\Input::get('name'));

			$settings = \Config::load('autoresponder.db');
			if(isset($settings['logo_url']))
			{
				$settings['logo_url'] = '';
				\Config::save('autoresponder.db', $settings);
			}

			\Messages::success('Settings successfully deleted logo.');
			\Response::redirect('admin/settings');
		}
	}

	public function action_amazons3()
	{
		$settings = \Config::load('amazons3.db');


        if (\Input::post()) {
        	$input = \Input::post();
        	
        	if(!\Input::is_ajax())
			{
				$val = Model_AmazonS3::validate('create');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create amazon s3</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
				}
				else {

		        	try {

		        		\Config::save('amazons3.db', array(
		        					'amazon_enable' => $input['amazon_enable'],
		        					'amazon_key' => $input['amazon_key'],
		        					'amazon_secret' => $input['amazon_secret'],
		        					'amazon_site' => rtrim($input['amazon_site'], '/').'/', // make sure there is trailing slash in url
		        					'amazon_bucket' => $input['amazon_bucket'],
		        					'amazon_region' => $input['amazon_region'],
		        				));

		        		\Messages::success('Amazon S3 successfully updated.');
						\Response::redirect('admin/settings/amazons3');
		        	} 
		        	catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create amazon s3.</strong>');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
					}
				}
			}
        }

		\View::set_global('title','Amazon S3');
        \View::set_global('menu', 'admin/settings');
		\Theme::instance()->set_partial('content', $this->view_dir . 'amazon')
				->set('settings', $settings, false);
	}

	public function action_homepage_option()
	{
		$settings = \Config::load('homepage-option.db');

        if (\Input::post()) {
        	
        	$input = \Input::post();

        	try {

        		\Config::save('homepage-option.db', array(
        					'header_hide' => $input['header_hide'],
        					'categories_hide' => $input['categories_hide'],
        					'recommended_products_hide' => $input['recommended_products_hide']
        				));

        		\Messages::success('Homepage option was successfully updated');
				\Response::redirect('admin/settings/homepage_option');
        	} 
        	catch (\Database_Exception $e)
			{
				// show validation errors
				\Messages::error('<strong>There was an error while trying to create homepage option.</strong>');
			    
				// Uncomment lines below to show database errors
				$errors = $e->getMessage();
		    	\Messages::error($errors);
			}
        }
        \View::set_global('title','Homepage Option');
        \View::set_global('menu', 'admin/settings');
		\Theme::instance()->set_partial('content', $this->view_dir . 'homepage_option')
				->set('settings', $settings, false);
	}

	public function action_accounting_api()
	{
		$settings = \Config::load('accounting_api.db');

        if (\Input::post()) {
        	
        	$input = \Input::post();

        	try {

        		\Config::save('accounting_api.db', array(
        					'accounting_api' => $input['accounting_api'],
        				));

        		\Messages::success('Accounting API was successfully updated');
				\Response::redirect('admin/settings/accounting_api');
        	} 
        	catch (\Database_Exception $e)
			{
				// show validation errors
				\Messages::error('<strong>There was an error while trying to create accounting API.</strong>');
			    
				// Uncomment lines below to show database errors
				$errors = $e->getMessage();
		    	\Messages::error($errors);
			}
        }
        \View::set_global('title','Accounting API');
        \View::set_global('menu', 'admin/settings');
		\Theme::instance()->set_partial('content', $this->view_dir . 'accounting_api')
				->set('settings', $settings, false);
	}
}