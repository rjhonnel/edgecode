<?php

/**
 * CMS
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Settings
 * @extends \Controller_Base_Public
 */

namespace Settings;

class Controller_Settings extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/order/';
	
	public function before()
	{
		parent::before();
	}
	
	/*
	* Add cron job command in cpanel
	* *	*	*	*	*	wget --delete-after http://demo.edgecommerce.org/settings/delayed_actions >/dev/null 2>&1
	*/
    public function action_delayed_actions()
    {
    	$lock_file = 'delay-actions.lock';

    	if (file_exists($lock_file)) return;

    	file_put_contents($lock_file, 'Run delay actions.');
    	
    	$actions = \Settings\Model_Delayedactions::find();
    	if($actions)
    	{

    		$settings = \Config::load('autoresponder.db');
    		$hold_delay_sending_invoice = (isset($settings['delay_sending_invoice']) ? intval($settings['delay_sending_invoice']) : 0);

	    	foreach ($actions as $action)
	    	{
	    		switch ($action->action)
	    		{
	    			case 'send_invoice':
	    				// if invoice can be send
	    				$created_at = $action->created_at;
	    				$created_at_plus = date('Y-m-d H:i', strtotime($created_at . '+' . $hold_delay_sending_invoice . 'hours'));
	    				$current_date = date('Y-m-d H:i');

	    				if($created_at_plus == $current_date)
	    				{
		    				// check if order is existing
		    				if($order = \Order\Model_Order::find_one_by_id($action->item_id))
		    				{
		    					\Theme::instance()->active('admin');
			    				$admin_order = new \Order\Controller_Admin_Order($this->request);
			    				$admin_order->pdf_send_email($order->id, 'invoice');

			    				$action->delete();
		    				}
	    				}
	    			break;
	    		}
	    	}
    	}
    	
    	unlink($lock_file);

    	exit;
    }
}