<?php

	namespace Settings;

	class Model_AmazonS3 extends \Model_Base
	{
		// Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'config';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'identifier',
		    'config',
		    'hash',
		);



		 /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
	    public static function validate($factory)
	    {
	        $val = \Validation::forge($factory);
	        $val->add('amazon_key', 'Amazon Key')->add_rule('required');
	        $val->add('amazon_secret', 'Amazon Secret')->add_rule('required');
	        $val->add('amazon_site', 'Amazon Site')->add_rule('required');
	        $val->add('amazon_bucket', 'Amazon Bucket')->add_rule('required');
	        $val->add('amazon_region', 'Amazon Region')->add_rule('required');
	        
	        return $val;
	    }
	}