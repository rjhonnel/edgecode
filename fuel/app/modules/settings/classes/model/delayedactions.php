<?php

	namespace Settings;

	class Model_Delayedactions extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'delayed_actions';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'item_id',
		    'action',
		    'created_at'
		);
	}