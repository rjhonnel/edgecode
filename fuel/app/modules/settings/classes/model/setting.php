<?php

	namespace Settings;

	class Model_Setting extends \Model_Base
	{
		// Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'config';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'identifier',
		    'config',
		    'hash',
		);



		 /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
	    public static function validate($factory)
	    {
	        $val = \Validation::forge($factory);
	        $val->add('company_name', 'Company Name')->add_rule('required');
	        $val->add('address', 'Address')->add_rule('required');
	        $val->add('website', 'Website')->add_rule('required');//->add_rule('valid_url');
	        $val->add('phone', 'Phone')->add_rule('required');
	        $val->add('email_address', 'Email Address')->add_rule('required')->add_rule('valid_email');
	        $val->add('sender_email_address', 'Sender email address')->add_rule('required')->add_rule('valid_email');
	        $val->add('contact_us_email_address', 'Contact us email address')->add_rule('required')->add_rule('valid_email');

	        if(\Input::post('gst', 0) == 1)
	        {
	        	$val->add('gst_percentage', 'Gst Percentage')->add_rule('required')->add_rule('numeric_min', 1);
	        }


	        return $val;
	    }
	}