<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Application;

class Controller_Admin_Related extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/newsmanager/related/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('application::application', 'details');
		\Config::load('application::related', 'related');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('related.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/application/list');
	}
    
    public function get_search_items($item)
    {
        /************ Get non related infotabs ***********/
		$items = \Product\Model_Product::find(function($query) use ($item){
			
            $related_ids = array();
			foreach($item->related_products as $related)
				array_push($related_ids, $related->id);
			
			if(!empty($related_ids))
			{
				$related_ids = '(' . implode(',', $related_ids) . ')';
				$query->where('id', 'NOT IN', \DB::expr($related_ids));
			}
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		
		$item->not_related_products = $items ? $items : array();
        
		/************ End generating query ***********/
		
        foreach($item->not_related_products as $item_key => $item_value)
        {
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title':
                            if(stripos($item_value->title, $value) === false) unset($item->not_related_products[$item_key]);
                            break;
                        case 'status':
                            if(is_numeric($value))
                                if($item_value->status != $value) unset($item->not_related_products[$item_key]);
                            break;
                        case 'active_from':
                            $date = strtotime($value);
                            if($date)
                                if($item_value->active_from < $value) unset($item->not_related_products[$item_key]);
                            break;
                        case 'active_to':
                            $date = strtotime($value);
                            if($date)
                                if($item_value->active_to > $value) unset($item->not_related_products[$item_key]);
                            break;
                        case 'category_id':
                            if(is_numeric($value))
                                if(!array_key_exists($value, $item_value->categories)) unset($item->not_related_products[$item_key]);
                            break;
                    }
                }
            }
        }
        
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($item->not_related_products),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$item->not_related_products     = array_slice($item->not_related_products, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
        
        return array('item' => $item, 'pagination' => $pagination, 'status' => $status);
    }
	
	/**
	 * Manage related products
	 * 
	 * @param $id	= Product ID
	 */
	public function action_list($id)
	{
		if(!is_numeric($id)) \Response::redirect('admin/application/list');
		
		// Get news item to edit
		if(!$item = Model_Application::find_one_by_id($id)) \Response::redirect('admin/application/list');
		
		if(\Input::post())
		{
			$add 	= \Input::post('products.add', array());
			$remove = \Input::post('products.remove', array());
			
			if(\Input::post('add', false))
			{
				foreach($add as $value)
				{
					$related = Model_Application_To_Related::forge(array(
						'related_id' => $value,
						'application_id' => $item->id
					));	
					$related->save();
				}
				
				\Messages::success('Related products successfully added.');
			}
			else if(\Input::post('remove', false))
			{
				foreach($remove as $value)
				{
					$related = Model_Application_To_Related::find_one_by(array(
						array('related_id', '=', $value),
						array('application_id', '=', $item->id),
					));
					
					if(!is_null($related))
					{
						$related->delete();
					}	
				}
				
				\Messages::success('Related products successfully removed.');
			}
			
			if(\Input::is_ajax())
			{
				echo \Messages::display('left', false);
				exit;
			}
			else
			{
				\Response::redirect(\Input::referrer(\Uri::create('admin/application/list')));
			}
		}
		
		\View::set_global('title', 'List Related Products');
		
        $search = $this->get_search_items($item);
        
        $pagination = $search['pagination'];
        $status     = $search['status'];
        $item       = $search['item'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('application', $item)
			->set('pagination', $pagination, false)
			->set('status', $status);
	}
}
