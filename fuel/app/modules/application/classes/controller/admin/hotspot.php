<?php

namespace Application;

class Controller_Admin_Hotspot extends \Admin\Controller_Base
{
    /**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/newsmanager/hotspot/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('application::application', 'details');
		\Config::load('application::hotspot', 'hotspot');
        \Config::load('application::infotab', 'infotab');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('hotspot.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
    /**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/application/list');

	}
    
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_show($application_id = false, $hotspot_id = false)
	{
        // Check for application
		if(!is_numeric($application_id)) \Response::redirect('admin/application/list');
		// Get application item to edit
		if(!$application = Model_Application::find_one_by_id($application_id)) \Response::redirect('admin/application/list');
        \View::set_global('menu', 'admin/newscategory/list');
        // Get hotspot is exist
		if(is_numeric($hotspot_id))
        {
			if(!$hotspot = Model_Application_Image::find_by_pk($hotspot_id)) unset($hotspot);
        }
        
		$theme_partial = \Theme::instance()->set_partial('content', $this->view_dir . 'hotspot');
        $theme_partial->set('application', $application);
        
        if(isset($hotspot))
			$theme_partial->set('hotspot', $hotspot);
	}
    
    /**
	 * Edit product infotabs
	 * 
	 * @param $product_id	= Product ID
	 * @param $infotab_id	= Infotab ID
	 * @param $hotspot_id	= Hotspot ID
	 * 
	 */
	public function action_infotab_edit($produt_id = false, $infotab_id = false, $hotspot_id = false)
	{
		// Check for product
		if(!is_numeric($produt_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$product = Model_Product::find_one_by_id($produt_id)) \Response::redirect('admin/product/list');
		
		// Check for infotab
		if(!is_numeric($infotab_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$item = Model_Product_To_Infotabs::find_by_pk($infotab_id)) \Response::redirect('admin/product/list');
		
		// Get hotspot is exist
		if(is_numeric($hotspot_id))
			if(!$hotspot = Model_Infotab_Image::find_by_pk($hotspot_id)) 
				unset($hotspot);
		
		if(\Input::post())
		{
			$val = Model_Product_To_Infotabs::validate('update', $item->type);
			
			// Upload image and display errors if there are any
			$image = $this->upload_infotab_image();
			if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->images)) 
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->images)))
			{
				/** IMAGES **/
				// Get all alt texts to update if there is no image change
				foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
				{
					if(strpos($image_id, 'new_') === false)
					{
						$item_images[$image_id] = array(
							'id'	=> $image_id,
							'data'	=> array(
								'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
							),
						);
					}
				}
				
				// Save images if new files are submitted
				if(isset($this->_infotab_image_data))
				{
					$settings = \Config::load('amazons3.db');
					$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
					foreach($this->_infotab_image_data as $image_data)
					{
						$cover_count = count($item->images);
						if(strpos($image_data['field'], 'new_') === false)
						{
							// Update existing image
							if(str_replace('image_', '', $image_data['field']) != 0)
							{
								$image_id = (int)str_replace('image_', '', $image_data['field']);
								$cover_count--;
								
								$item_images[$image_id] = array(
									'id'	=> $image_id,
									'data'	=> array(
										'infotab_id'	=> $item->unique_id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
									),
								);
								
								$this->delete_infotab_image(\Input::post('image_db_' . $image_id, ''));
							}
						}
						else
						{
							// Save new image
							$image_tmp = str_replace('image_new_', '', $image_data['field']);
							
							$item_images[0] = array(
								'id'	=> 0,
								'data'	=> array(
									'infotab_id'	=> $item->unique_id,
									'image'			=> $image_data['saved_as'],
									'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
									'cover'			=> $cover_count == 0 ? 1 : 0,
									'sort'			=> $cover_count + 1,
								),
							);
						}

						//save to amazon
						if($hold_amazon_enable)
						{
							$folder_list = array('', 'thumbs/');
							foreach ($folder_list as $folder)
							{
		                        $file_info = array(
		                                'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
		                                'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
		                                'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
		                            );
								\Helper::uploadFileToAmazonS3($file_info, $settings);
								@unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
							}
						}
						// EOL - save to amazon
					}
				}
				
				Model_Infotab::bind_images($item_images);
				/** END OF IMAGES **/
				
				// Get POST values
				$insert = \Input::post();
				
				// Prep some values
				$insert['description'] = trim($insert['description']);
				$insert['active'] = !empty($insert['description']) ? 1 : 0;
				
				$item->set($insert);
				
				try{
					$item->save();
					
					\Messages::success('Infotab successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/infotab_list/'.$product->id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update infotab</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				// Delete uploaded images if there is product saving error
				if(isset($this->_infotab_image_data))
				{
					foreach($this->_infotab_image_data as $image_data)
					{
						$this->delete_infotab_image($image_data['saved_as']);
					}
				}
						
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update infotab</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		$item = Model_Product_To_Infotabs::find_by_pk($infotab_id);
		
		$theme_partial = \Theme::instance()->set_partial('content', $this->view_dir . 'infotabs_edit_' . strtolower($item->type))
			->set('product', $product)
			->set('infotab', $item);
		
		if(isset($hotspot))
			$theme_partial->set('hotspot', $hotspot);
	}
	
	/**
	 * Edit product hotspot infotab
	 * This infotab has realy custom form and needs to be separated of others
	 * 
	 * @param $content_id	= Content ID
	 * 
	 */
	public function action_edit($content_id = false)
	{
		// Check for product
		if(!is_numeric($content_id)) \Response::redirect('admin/application/list');
        \View::set_global('title', 'News Category Hotspot');
		// Get news item to edit
		if(!$content = Model_Application::find_one_by_id($content_id)) \Response::redirect('admin/application/list');
		
		if(\Input::post())
		{
			if(\Input::post('image', false))
			{
				// Upload image and display errors if there are any
				$image = $this->upload_infotab_image();
				if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($content->hotspot_image)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload image</strong>');
					\Messages::error('You have to select image');
				}
				elseif($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if(($image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($content->hotspot_image))) || \Input::post('use_cover_image', false))
				{
					// Clear previous messages if exists
					\Messages::reset();
					
					// Get POST values
					$insert = \Input::post();
					
					$item_image['hotspot_alt_text'] = \Input::post('alt_text', false) ? \Input::post('alt_text', false) : NULL;
					
					// Use cover image
					if(\Input::post('use_cover_image', false))
					{
						$cover_image = \Input::post('cover_image');
						$old_image = \Config::get('details.image.location.root') . $cover_image; 
						$new_image	 = \Config::get('infotab.image.location.root') . $cover_image; 
						
//						var_dump($old_image, $new_image); exit;
						
						while(file_exists($new_image))
						{
							$file_name 	= pathinfo($new_image, PATHINFO_FILENAME);
							$file_ext 	= pathinfo($new_image, PATHINFO_EXTENSION);
							$file_name = \Str::increment($file_name);
							
							$new_image = \Config::get('infotab.image.location.root') . $file_name . '.' . $file_ext;
						}
						
						if(\File::copy($old_image, $new_image))
						{
							$image = \Image::forge(array('presets' => \Config::get('infotab.image.resize', array())));
							$image->load($new_image);
							
							foreach(\Config::get('infotab.image.resize', array()) as $preset => $options)
							{
								$image->preset($preset);
							}
							
							if(isset($item_image))
							{
								$insert['hotspot_alt_text'] = isset($item_image['hotspot_alt_text']) ? $item_image['hotspot_alt_text'] : NULL;
								$insert['hotspot_image'] 	= basename($new_image);
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));
								
								//save to amazon
								$settings = \Config::load('amazons3.db');
								$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
								if($hold_amazon_enable)
								{
									$folder_list = array('', 'thumbs/');
									foreach ($folder_list as $folder)
									{
				                        $file_info = array(
				                                'savePath' => 'media/images/'.$folder.$file_name . '.' . $file_ext,
				                                'filePath' => $new_image,
				                                'contentType' => mime_content_type($new_image),
				                            );
										\Helper::uploadFileToAmazonS3($file_info, $settings);
										@unlink($new_image);
									}
								}
								// EOL - save to amazon
							}
						}
						else
						{
							\Messages::error('<strong>There was an error while trying to upload image</strong>');
							\Messages::error('Please try again');
						}
						
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					else
					{
						/** IMAGES **/
						// Save images if new files are submitted
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$item_image['hotspot_image'] = $image_data['saved_as'];
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));
							}
						}
						
						if(isset($item_image))
						{
							$insert['hotspot_alt_text'] = isset($item_image['hotspot_alt_text']) ? $item_image['hotspot_alt_text'] : NULL;
							$insert['hotspot_image'] 	= isset($item_image['hotspot_image']) ? $item_image['hotspot_image'] : $content->hotspot_image;
						}
						/** END OF IMAGES **/
					}
					
					// Make infotab active
					$insert['active'] = 1;
					
					$content->set($insert);
					
					try{
						$content->save();
						
						\Messages::success('Image successfully updated.');
						\Response::redirect(\Uri::create('admin/application/hotspot/show/' . $content->id));
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update image</strong>');
					    
						// Uncomment lines below to show database errors
						//$errors = $e->getMessage();
				    	//\Messages::error($errors);
					}
				}
				else
				{
					// Delete uploaded images if there is product saving error
					if(isset($this->_infotab_image_data))
					{
						foreach($this->_infotab_image_data as $image_data)
						{
							$this->delete_infotab_image($image_data['saved_as']);
						}
					}
				}
			}
			
			if(\Input::post('hotspot', false))
			{
				
			}
		}
		
		\Response::redirect(\Uri::create('admin/application/hotspot/show/' . $content->id));
	}
	
	/**
	 * Edit product hotspot position
	 * 
	 * @param $content_id	= Content ID
	 * @param $infotab_id	= Infotab ID
	 * 
	 */
	public function action_hotspot($content_id = false, $hotspot_id = false)
	{
		// Check for product
		if(!is_numeric($content_id)) \Response::redirect('admin/application/list');
		// Get news item to edit
		if(!$content = Model_Application::find_one_by_id($content_id)) \Response::redirect('admin/application/list');
		
		// Get hotspot is exist
		if(is_numeric($hotspot_id))
			if(!$hotspot = Model_Application_Image::find_by_pk($hotspot_id)) 
				unset($hotspot);
		
		if(\Input::post())
		{
			$insert = \Input::post();
			
			if(!\Input::is_ajax())
			{
				$val = Model_Application_Image::validate('create');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create hotspot</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
					\Response::redirect(\Uri::create('admin/application/hotspot/show/' . $content->id . (isset($hotspot) ? '/' . $hotspot->id : '')));
				}
				
				$insert['title'] 		= trim($insert['title']) != '' ? $insert['title'] : NULL;
				$insert['description'] 	= trim($insert['description']) != '' ? $insert['description'] : NULL;
			}
			
			$insert['application_id'] 	= $content->id;
			
			if(\Input::post('create', false))
			{
				$hotspot = Model_Application_Image::forge($insert);
				
				try{
					$hotspot->save();
					
					if(\Input::is_ajax())
					{
						$return['hotspot_id'] = $hotspot->id;
						echo json_encode($return); exit;
					}
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create hotspot</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
			    	
					if(\Input::is_ajax())
					{
						$return['message'] = \Messages::display();
						$return['hotspot_id'] = false;
						echo json_encode($return); exit;
					}
			    	
				}
			}
			
			if(\Input::post('update', false))
			{
				if(isset($hotspot))
				{
					/** IMAGES **/
					// Upload image and display errors if there are any
					$image = $this->upload_infotab_image();
					if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($hotspot->image)) 
					{
						// No previous images and image is not selected and it is required
						\Messages::error('<strong>There was an error while trying to upload hotspot image</strong>');
						\Messages::error('You have to select image');
					}
					elseif($image['errors'])
					{
						\Messages::error('<strong>There was an error while trying to upload hotspot image</strong>');
						foreach($image['errors'] as $error) \Messages::error($error);
					}
					
					if(($image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($hotspot->image))) || \Input::post('use_cover_image', false))
					{
						// Clear previous messages if exists
						\Messages::reset();
						
						$item_image['alt_text'] = \Input::post('alt_text', false) ? \Input::post('alt_text', false) : NULL;
						
						// Save images if new files are submitted
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$item_image['image'] = $image_data['saved_as'];
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));
							}
						}
						
						if(isset($item_image))
						{
							$insert['alt_text'] = isset($item_image['alt_text']) ? $item_image['alt_text'] : NULL;
							$insert['image'] 	= isset($item_image['image']) ? $item_image['image'] : $hotspot->image;
						}
					}
					else
					{
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					/** END OF IMAGES **/
					
					/** VIDEOS **/
					$item_video['video_title'] = \Input::post('video_title', false) ? \Input::post('video_title', false) : NULL;
					$item_video['video'] = \Input::post('video_url', false) ? \Input::post('video_url', false) : NULL;
					
					if(!is_null($item_video['video']))
					{
						// Check video
						$youtube = \App\Youtube::forge();
						$video = $youtube->parse($item_video['video'])->get();
						
						if(!$video)
						{
							\Messages::error('"' . $item_video['video'] . '" is invalid video URL. Video not updated.');
							
							// Revert to old values
							$item_video['video_title'] 	= $hotspot->video_title;
							$item_video['video'] 		= $hotspot->video;
						}
					}
					
					if(isset($item_video))
					{
						$insert['video'] 	= isset($item_video['video']) ? $item_video['video'] : NULL;
						$insert['video_title'] = isset($item_video['video_title']) ? $item_video['video_title'] : NULL;
						
						// Unset video title is there is no video
						if(is_null($insert['video'])) $insert['video_title'] = NULL;
					}
					/** END OF: VIDEOS **/
					
					$hotspot->set($insert);
					
					try{
						$hotspot->save();
						\Messages::success('Hotspot sucessfully updated.');
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('There was an error while trying to update hotspot.');
						\Messages::error('Please try again.');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
				    	
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					
					if(\Input::is_ajax())
					{
						echo \Messages::display(); exit;
					}
				}
			}
		}
		
		\Response::redirect(\Uri::create('admin/application/hotspot/show/' . $content->id . (isset($hotspot) ? '/' . $hotspot->id : '')));
	}
    
    /****************************** INFOTAB IMAGES ******************************/
	
	/**
	 * Upload all infotab images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_infotab_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->infotab_image_upload_config = array(
		    'path' => \Config::get('infotab.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		);
		
		\Upload::process($this->infotab_image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save image
		    \Upload::save();
			$this->_infotab_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_infotab_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('infotab.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('infotab.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 * @param $type			= Type of content (infotab or hotspot)
	 * @param $delete		= If type is "hotspot" we can either delete image or video
	 */
	public function action_delete_infotab_image($image_id = false, $content_id = false, $type = 'infotab', $delete = 'image')
	{
		if($image_id && $content_id)
		{
			$images = Model_Application_Image::find(array(
			    'where' => array(
			        'application_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
					
					if(strtolower($type) == 'hotspot')
					{
						// Delete only part of hotspot, either image or video
						if($delete == 'image')
						{
							// Delete only image but not whole hotspot
							$this->delete_infotab_image($image->image);
							
							$image->set(array(
								'image' => null,
								'alt_text' => null,
							));
						}
						else
						{
							// Delete only video but not whole hotspot
							$image->set(array(
								'video' => null,
								'video_title' => null,
							));
						}
						
						$image->save();
						
						\Messages::success(ucfirst($type) . ' ' . strtolower($delete) . ' was successfully deleted.');
					}
					else
					{
						// If there is only one image and image is required
						if(count($images) == 1)
						{
							if(\Config::get('infotab.image.required', false) && !\Request::is_hmvc())
							{
								\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
							}
							else
							{
								// Reset sort fields
								\DB::update(Model_Application_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
								// Delete image
								$this->delete_infotab_image($image->image);
								$image->delete();
								\Messages::success(ucfirst($type) . ' image was successfully deleted.');
							}
						}
						else
						{
							if($image->cover == 1 && !\Request::is_hmvc())
							{
								\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
							}
							else
							{
								// Reset sort fields
								\DB::update(Model_Application_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
								// Delete image
								$this->delete_infotab_image($image->image);
								$image->delete();
								\Messages::success(ucfirst($type) . ' image was successfully deleted.');
							}
						}
					}
				}
				else
				{
					\Messages::error(ucfirst($type) . ' image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error(ucfirst($type) . ' image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		if(\Input::is_ajax())
		{
			\Messages::reset();
			\Messages::success('Hotspot was successfully deleted.');
			echo \Messages::display();
		}
		else if(\Request::is_hmvc())
		{
			\Messages::reset();
		}
		else
		{
			\Response::redirect(\Input::referrer());
		}
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_infotab_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			foreach(\Config::get('infotab.' . $content_type . '.location.folders', array()) as $folder)
			{
				//delete from amazon
				if($hold_amazon_enable)
				{
					if($content_type == 'image')
					{
						\Helper::deleteFileFromAmazonS3('media/images/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/thumbs/' . $name, $settings);
					}
				}
				// EOL - delete from amazon
				@unlink($folder . $name);
			}
		}
	}
    
     /**
	 * Delete content image
	 * 
	 * @param $content_id		= Content ID
	 */
    public function action_delete_image($content_id = null)
    {
        try
        {
            if($application = Model_Application::find_one_by_id($content_id))
            {
                                               
                if(!empty($application->hotspot) && !empty($application->hotspot->images))
                {
                    foreach($application->hotspot->images as $image)
                    {
                        $this->delete_image($image->image);
                        $image->delete();
                    }
                }
                
                $this->delete_image($application->hotspot_image);
                
                $application->hotspot_alt_text = null;
                $application->hotspot_image = null;
                
                if($application->save())
                {
                    \Messages::success('Hotspot image was successfully deleted.');
                }
                else
                {
                    \Messages::error('There was an error while trying to delete hotspot image.');
                }
            }
        }
        catch (\Database_Exception $e)
        {
            // show validation errors
            \Messages::error('There was an error while trying to delete hotspot image.');

            // Uncomment lines below to show database errors
            $errors = $e->getMessage();
            \Messages::error($errors);
        }
        
        \Response::redirect(\Input::referrer());
    }
    
    /**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
				//delete from amazon
				if($hold_amazon_enable)
				{
					if($content_type == 'image')
					{
						\Helper::deleteFileFromAmazonS3('media/images/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/thumbs/' . $name, $settings);
					}
				}
				// EOL - delete from amazon
				@unlink($folder . $name);
			}
		}
	}
}