<?php

namespace Application;

class Controller_Application extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/application/';
	
	public $theme_view_path;
    
     // Global theme variables
    public $page_theme;
    
	public function before()
	{
		parent::before();
		
		\Config::load('application::application', 'details');
		\Config::load('application::casestudy', 'casestudy');
        \Config::load('application::infotab', 'infotab');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
		
		$this->theme_view_path = DOCROOT . \Theme::instance()->asset_path($this->view_dir);
	}
	
	/**
	 * Index application
	 * 
	 * @access  public
     * @param   $slug
	 */
	public function action_index($slug = false)
	{
        $item = false;
         
        // Find content by slug
        if($slug !== false)
        {
            $item = \Application\Model_Application::get_by_slug($slug);
        }
        elseif($slug === false)
        {
            $item = \Application\Model_Application::find(function($query){
                $query->where('status', 1);
                $query->order_by('sort', 'asc');
            });
        }
        
        if(!$item)
        {
            // Send to 404 application
            throw new \HttpNotFoundException;
        }

        $page = \Theme::instance()->set_partial('content', $this->view_dir . ($slug ? 'single' : 'list'));
        
        if(is_array($item))
        {
            // All applications page
            $page->set('applications', $item, false);
        }
        else
        {
            // Single application page
            $page->set('application', $item, false);
        }
	}
    
    public function action_hotspot($application = null, $hotspot = null)
    {
        // Check if we have valid application
        $item = \Application\Model_Application::find_one_by_id($application);
        
        // If no, redirect back to home page
        if(!$item) \Response::redirect(\Uri::front_create('/'));
        
        if(\Input::is_ajax())
        {
            // Check if we have valid hotspot
            $hotspot = \Application\Model_Application_Image::find_one_by_id($hotspot);
            
            // If not show error message
            if(!$hotspot)
            {
                echo 'Invalid link. Please try again!'; exit;
            }
            
            // Load hotspot view
            echo \Theme::instance()->view($this->view_dir . 'hotspot')->set('hotspot', $hotspot, false);
        }
        else
        {
            \Response::redirect(\Uri::front_create('application/' . $item->seo->slug));
        }
    }
    
    public function action_page()
    {
        \Theme::instance()->set_partial('content', $this->view_dir . 'html');
    }
    
    /**
	 * Case Studies
	 * 
	 * @access  public
     * @param   $slug
	 */
	public function action_casestudies()
	{
        $case_studies = \Application\Model_Casestudy::find(array(
            'where' => array('status' => 1),
            'order_by' => array('sort' => 'asc')
        ));
        
        \Theme::instance()->set_partial('content', $this->view_dir . 'case_studies')
                ->set('case_studies', $case_studies, false);
	}
    
                
}
