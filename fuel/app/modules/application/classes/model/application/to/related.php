<?php

	namespace Application;

	class Model_Application_To_Related extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'application_to_related';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'application_id',
		    'related_id',
		);
	}