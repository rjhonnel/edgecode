<?php

	namespace Application;

	class Model_Application_Image extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'application_images';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'application_id',
		    'title',
		    'description',
		    'image',
		    'alt_text',
		    'video',
		    'video_title',
		    'position_x',
		    'position_y',
		    'cover',
		    'sort',
		);
		
		/**
		 * Prep some query values
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
				
			return $vars;
		}
		
		/**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);
			$val->add('description', 'Description')->add_rule('required');
			
			return $val;
		}
	}