<?php

	namespace Application;

	class Model_Video extends \Model_Base
	{
        // Temporary variable to store some query results
		public static $temp = array();
        
	    // Set the table to use
	    protected static $_table_name = 'content_videos';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'content_id',
		    'content_type',
		    'url',
		    'title',
		    'thumbnail',
		    'sort',
		);
		
	    protected static $_defaults = array(
		    'content_type' => 'content',
		);
		
		 /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	    	$query->where('content_type', 'content');
	    }
	    
		/**
		 * Prep some query values
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
				
			return $vars;
		}
        
        /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
		    			// Get video parent
						$item->get_parent = static::lazy_load(function() use ($item){
		    				return Model_Application::find_one_by_id($item->content_id);
		    			}, $item->id, 'parent', 'object');
                        
		    			// Get video details
						$item->get_youtube = static::lazy_load(function() use ($item){
		    				
                            $object = new \stdClass();
                            
                            $youtube = \App\Youtube::forge();
                            $object->data = $youtube->parse($item->url)->get();
                            $object->embed = $youtube->embed($item->url);
                            
                            return $object;
                            
		    			}, $item->id, 'youtube', 'object');
						
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
        
        /**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Application\Model_Video::$temp['lazy.'.$id.$type]))
		    	{
		    		\Application\Model_Video::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Application\Model_Video::$temp['lazy.'.$id.$type])) 
			    			\Application\Model_Video::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Application\Model_Video::$temp['lazy.'.$id.$type])) 
			    			\Application\Model_Video::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Application\Model_Video::$temp['lazy.'.$id.$type];
	    	};
	    }
		
	}