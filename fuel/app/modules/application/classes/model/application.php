<?php

	namespace Application;

	class Model_Application extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'content';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'parent_id',
		    'type',
		    'title',
		    'description_intro',
		    'description_full',
		    'featured',
		    'status',
		    'active_from',
		    'active_to',
		    'published_at',
		    'user_created',
		    'user_updated',
		    'sort',
		    'hotspot_image',
		    'hotspot_alt_text',
		);
		
		protected static $_defaults = array(
		    'type' => 'application',
		);
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    /**
	     * Insert or update application images
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	    	if(empty($images) || !is_array($images)) return false;
	    	
	    	foreach($images as $key => $image)
	    	{
	    		$data = $image['data'];
				if(is_numeric($image['id']) && $image['id'] > 0)
				{
					// Update existing image
					$item = Model_Image::find_one_by_id($image['id']);
					$item->set($data);
				}
				else
				{
					$item = Model_Image::forge($data);
				}
	    		
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Insert or update application files
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     * 		'data'	=> Array of file data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     * 		'data'	=> Array of file data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $files
	     */
	    public static function bind_files($files = array())
	    {
	    	if(empty($files) || !is_array($files)) return false;
	    	
	    	foreach($files as $key => $file)
	    	{
	    		$data = $file['data'];
				if(is_numeric($file['id']) && $file['id'] > 0)
				{
					// Update existing file
					$item = Model_File::find_one_by_id($file['id']);
					$item->set($data);
				}
				else
				{
					$item = Model_File::forge($data);
				}
	    		
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Insert or update application videos
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     * 		'data'	=> Array of file data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     * 		'data'	=> Array of file data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $files
	     */
	    public static function bind_videos($videos = array())
	    {
	    	if(empty($videos) || !is_array($videos)) return false;
	    	
	    	foreach($videos as $key => $video)
	    	{
	    		$data = $video['data'];
				if(is_numeric($video['id']) && $video['id'] > 0)
				{
					// Update existing video
					$item = Model_Video::find_one_by_id($video['id']);
					$item->set($data);
				}
				else
				{
					$item = Model_Video::forge($data);
				}
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	    	$query->where('content.type', 'application');
	    	
	    	// Enable caching for all SELECT queries
	    	//$query->cached(3600, 'admin.application.', false);
	    	$table_name = self::$_table_name;

	    	$active = \Theme::instance()->active();
	        if($active['name'] == 'frontend')
	        {
	            $query->where(function($query) use ($table_name){
	                    $query->where($table_name . '.status', 1);
	                    $query->or_where(function($query) use ($table_name){
	                        $query->where($table_name . '.status', 2);
	                        $query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
	                        $query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
	                    });
	                });
	        }
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
		    			// Get content images
						$item->get_images = static::lazy_load(function() use ($item){
		    				return Model_Image::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'images');
						
		    			// Get content files
						$item->get_files = static::lazy_load(function() use ($item){
		    				return Model_File::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'files');
						
		    			// Get content videos
						$item->get_videos = static::lazy_load(function() use ($item){
		    				return Model_Video::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'videos');
						
		    			// Get content children
						$item->get_children = static::lazy_load(function() use ($item){
		    				return Model_Casestudy::find(array(
							    'where' => array(
							        'parent_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'children');
						
		    			// Get content accordions
						$item->get_accordions = static::lazy_load(function() use ($item){
		    				return Model_Accordion::find(array(
							    'where' => array(
							        'parent_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'accordions');
						
		    			// Get content children
						$item->get_seo = static::lazy_load(function() use ($item){
							return Model_Seo::find_one_by_content_id($item->id);
		    			}, $item->id, 'seo', 'object');
		    			
                        // Get related products
                        $item->get_related_products = static::lazy_load(function() use ($item){

                            $related = Model_Application_To_Related::find(function($query) use ($item)
                            {
                                return $query->where('application_id', $item->id);
                            }, 'related_id');

                            if(!empty($related))
                            {
                                $related = '(' . implode(',', array_keys($related)) . ')';

                                return \Product\Model_Product::find(function($query) use ($related, $item)
                                {
                                    $query->where('id', 'IN', \DB::expr($related));
                                    $query->where('id', '<>', $item->id);
                                    $query->order_by('sort', 'asc');

                                    return $query;
                                }, 'id');
                            }

                            return array();

                        }, $item->id, 'related_products');
                        
                        // Get hotspot images
						$item->get_hotspot = static::lazy_load(function() use ($item){
                            
                            $return = new \stdClass;
							$return->images = Model_Application_Image::find_by_application_id($item->id);
                            
                            return $return;
                            
		    			}, $item->id, 'hotspot', 'object');
                        
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Application\Model_Application::$temp['lazy.'.$id.$type]))
		    	{
		    		\Application\Model_Application::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Application\Model_Application::$temp['lazy.'.$id.$type])) 
			    			\Application\Model_Application::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Application\Model_Application::$temp['lazy.'.$id.$type])) 
			    			\Application\Model_Application::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Application\Model_Application::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 4)->add_rule('max_length', 255);
			if(\Input::post('status', 1) == 2)
			{
				// Only set if status is "Active in period"
				$val->add('active_from', 'Active from')->add_rule('required_with', 'active_to')->add_rule('valid_date', 'd/m/Y');
				$val->add('active_to', 'Active to')->add_rule('required_with', 'active_from')->add_rule('valid_date', 'd/m/Y');
			}
	
			return $val;
		}
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				$vars['user_created'] = \Sentry::user()->id;
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				$class = 'Application\Model_' . ucfirst($name);
				return new $class;
			}
			
			return static::forge();
		}
        
        public static function get_by_slug($slug = false)
        {
            if($seo = \Application\Model_Seo::find_one_by_slug($slug))
            {
                return Model_Application::find_one_by_id($seo->content_id);
            }
            
            return false;
        }
		
	}