<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Attribute;

class Controller_Admin_Group extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/attribute/group/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('attribute::attribute', 'attribute');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('attribute.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/attribute/group/list');
	}
	
	public function action_list()
	{
        \View::set_global('menu', 'admin/attribute/group/list');
		\View::set_global('title', 'List Attribute Groups');
		
		/************ Start generating query ***********/
		$items = Model_Attribute_Group::find(function($query){
			
			// Get search filters
			foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
							$query->where($key, 'like', "%$value%");
							break;
					}
				}
			}
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Get Attributes name string
		foreach($items as $item)
		{
			$display_num 	= 3;
			$counter 		= $display_num;
			$item->attributes_string = '';
			
			foreach($item->attributes as $attribute)
			{
				if($counter > 0)
				{
					$item->attributes_string .= $attribute->title . ', ';
					$counter--;
				}
			}
			
			$item->attributes_string = rtrim($item->attributes_string, ', ');
			$item->attributes_string .= count($item->attributes) > $display_num ? ' and ' . (count($item->attributes)-$display_num) . ' more' : '';
		}
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
            ->set('pagination', $pagination, false);
	}
	
	public function action_create()
	{
		\View::set_global('title', 'Add New Attribute Group');
		
		if(\Input::post())
		{
			$val = Model_Attribute_Group::validate('create');
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				
				$item = Model_Attribute_Group::forge($insert); 
				
				try{
					$item->save();
					
					\Messages::success('Attribute group successfully created.');
					
					\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/group/list')));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create attribute group</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create attribute group</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		// Keep existing messages
		\Messages::instance()->shutdown();
		
		\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/group/list')));
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/attribute/group/list');
		
		// Get news item to edit
		if(!$item = Model_Attribute_Group::find_one_by_id($id)) \Response::redirect('admin/attribute/group/list');

        \View::set_global('menu', 'admin/attribute/group/list');
		\View::set_global('title', 'Edit Attribute Group');
		
		if(\Input::post())
		{
			$add 	= \Input::post('attributes.add', array());
			$remove = \Input::post('attributes.remove', array());
			
			if(\Input::post('add', false))
			{
				foreach($add as $value)
				{
					$group = Model_Attribute_To_Groups::forge(array(
						'attribute_id' 	=> $value,
						'group_id' 		=> $item->id
					));	
					
					$group->save();
				}
				
				\Messages::success('Attributes successfully added.');
			}
			else if(\Input::post('remove', false))
			{
				foreach($remove as $value)
				{
					$group = Model_Attribute_To_Groups::find_one_by(array(
						array('attribute_id', '=', $value),
						array('group_id', '=', $item->id),
					));
					
					if(!is_null($group))
					{
						$group->delete();	
					}	
				}
				
				\Messages::success('Attributes successfully removed.');
			}
			else if(\Input::post('update', false))
			{
				$item->set(\Input::post());
				
				try{
					$item->save();
					
					\Messages::success('Attribute group successfully updated.');
					
					\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/group/update/'.$item->id)));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update attribute group</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			
			if(\Input::is_ajax())
			{
				echo \Messages::display('left', false);
				exit;
			}
			else
			{
				\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/group/list')));
			}
		}
		
		$group = Model_Attribute_Group::find_one_by_id($id);
		
		/************ Get non related infotabs ***********/
		$items = Model_Attribute::find(function($query) use ($group){
			
			$related_ids = array();
			foreach($group->attributes as $attribute)
				array_push($related_ids, $attribute->id);
			
			if(!empty($related_ids))
			{
				$related_ids = '(' . implode(',', $related_ids) . ')';
				$query->where('id', 'NOT IN', \DB::expr($related_ids));
			}
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		
		$group->not_related_attributes = $items ? $items : array();


		// Reset to empty array if there are no result found by query
		$not_related_attributes = !empty($group->not_related_attributes)?$group->not_related_attributes:array();
		
		// Initiate pagination
		$pagination_not_related_attributes = \Hybrid\Pagination::make(array(
			'total_items' => count($not_related_attributes),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$not_related_attributes = array_slice($not_related_attributes, $pagination_not_related_attributes->offset, $pagination_not_related_attributes->per_page);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')
					->set('not_related_attributes', $not_related_attributes)
					->set('pagination_not_related_attributes', $pagination_not_related_attributes, false)
					->set('group', $group);
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Attribute_Group::find_one_by_id($id))
			{
				// Delete item
				try{
					// Delete relation to attributes
					$attributes = Model_Attribute_To_Groups::find_by_group_id($item->id);
					if(!is_null($attributes))
					{
						foreach($attributes as $attribute) $attribute->delete();
					}
					
					$item->delete();
					
					\Messages::success('Attribute group successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete attribute group</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/group/list')));
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
			
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Attribute::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
    
    public function action_sort_attr_in_group($type = false)
	{
		//if(!$type) return false;
        
        var_dump($items);
		
		$items = \Input::post('sort');
        var_dump($items); exit;
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
			
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Attribute::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
    
    
}
