<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Attribute;

class Controller_Admin_Attribute extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/attribute/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('attribute::attribute', 'attribute');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('attribute.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/attribute/list');
	}
	
	public function action_list()
	{
		if(\Input::post('remove', false)) {
			$remove = \Input::post('attribute.remove', array());
			$is_remove = 0;
			
			foreach($remove as $value)
			{
				if(is_numeric($value))
				{
					// Get news item to edit
					if($item = Model_Attribute::find_one_by_id($value))
					{
						// Delete item
						try{
							// Delete attribute options
							if(!empty($item->options))
							{
								foreach($item->options as $option)
								{
									\Request::forge('admin/attribute/option/delete/' . $option->id)->execute()->response();
								}
							}
							
							$item->delete();
							
							$is_remove = 1;
						}
						catch (\Database_Exception $e)
						{
							\Messages::error('<strong>There was an error while trying to delete attribute</strong>');
						}
					}
				}
			}

			if($is_remove)
				\Messages::success('Attribute successfully deleted.');
		}
        \View::set_global('menu', 'admin/attribute/list');
		\View::set_global('title', 'List Attributes');
		
		/************ Start generating query ***********/
		$items = Model_Attribute::find(function($query){
			
			// Get search filters
			foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
							$query->where($key, 'like', "%$value%");
							break;
					}
				}
			}
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
            ->set('pagination', $pagination, false);
	}
	
	public function action_create()
	{
		\View::set_global('title', 'Add New Attribute');
		
		if(\Input::post())
		{
			$val = Model_Attribute::validate('create');
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				$item = Model_Attribute::forge($insert); 
				
				try{
					$item->save();
					
					\Messages::success('Attribute successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/attribute/update/' . $item->id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create attribute</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create attribute</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/attribute/list');
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/attribute/list');
		
		// Get news item to edit
		if(!$item = Model_Attribute::find_one_by_id($id)) \Response::redirect('admin/attribute/list');
        \View::set_global('menu', 'admin/attribute/list');
		\View::set_global('title', 'Edit Attribute');
		
		
		if(\Input::post())
		{
			
			if(\Input::post('option_title', false))
			{
				\Request::forge('admin/attribute/option/create')->set_method('POST')->execute()->response();
			}
			
			$val = Model_Attribute::validate('update');
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				
				$item->set($insert);
                
				try{
					$item->save();
					
					\Messages::success('Attribute successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/attribute/list/') : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update attribute</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update attribute</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		$attribute = Model_Attribute::find_one_by_id($id);
		
		$attribute_options = is_array($attribute->options) ? $attribute->options : array();
		
		// Initiate pagination
		$attribute_options_pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($attribute_options),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$attribute_options = array_slice($attribute_options, $attribute_options_pagination->offset, $attribute_options_pagination->per_page);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('attribute', $attribute)->set('attribute_options', $attribute_options)->set('attribute_options_pagination', $attribute_options_pagination, false);
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Attribute::find_one_by_id($id))
			{
				// Delete item
				try{
					// Delete attribute options
					if(!empty($item->options))
					{
						foreach($item->options as $option)
						{
							\Request::forge('admin/attribute/option/delete/' . $option->id)->execute()->response();
						}
					}
					
					$item->delete();
					
					\Messages::success('Attribute successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete attribute</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
			
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Attribute::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
}

                            