<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Attribute;

class Controller_Admin_Option extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/attribute/option/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('attribute::attribute', 'attribute');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('attribute.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/attribute/list');
	}
	
	public function action_create()
	{
		\View::set_global('title', 'Add New Attribute Option');
		
		if(\Input::post())
		{
			$val = Model_Attribute_Option::validate('create');
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				
				$insert['title'] = $insert['option_title'];
				
				$item = Model_Attribute_Option::forge($insert); 
				
				try{
					$item->save();
					
					\Messages::success('Attribute option successfully created.');
					if(!\Request::is_hmvc())
						\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/list')));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create attribute option</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create attribute option</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		// Keep existing messages
		\Messages::instance()->shutdown();
		if(!\Request::is_hmvc())
			\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/list')));
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/attribute/list');
		
		// Get news item to edit
		if(!$item = Model_Attribute_Option::find_one_by_id($id)) \Response::redirect('admin/attribute/list');
		
		\View::set_global('title', 'Edit Attribute Option');
		
		$return['error'] = true;
		
		if(\Input::post())
		{
			$val = Model_Attribute_Option::validate('update');
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				$insert['title'] = $insert['option_title'];
				
				$item->set($insert);
				
				try{
					$item->save();
					
					$return['error'] = false;
					
					\Messages::success('Attribute option successfully updated.');
					
					if(!\Input::is_ajax())
						\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/list')));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('There was an error while trying to update attribute option. Please try again.');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					//\Messages::error('<strong>There was an error while trying to update attribute option</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		if(\Input::is_ajax())
		{
			$return['message'] = \Messages::display();
			echo json_encode($return);
		}
		else
		{
			$option = Model_Attribute_Option::find_one_by_id($id);
			
			\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('option', $option);
		}
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Attribute_Option::find_one_by_id($id))
			{
				// Delete item
				try{
					$item->delete();

					// NRB-Gem: remove from product_attributes and product_attribute_price
					$a_attr = \Product\Model_Attribute::find_by(array(
                        array('attributes', 'like', '%"'.$item->attribute_id.'":"'.$id.'"%')
					));
					$a_attr_id = array();
					foreach($a_attr as $o_attr){
						$a_attr_id[] = $o_attr->id;
					}
					if (count($a_attr_id)) {
						$s_ids = '(' . implode(',', $a_attr_id) . ')';
						\DB::delete('product_attributes')
							->where('id', 'IN', \DB::expr($s_ids))
							->execute();
						\DB::delete('product_attribute_price')
							->where('product_attribute_id', 'IN', \DB::expr($s_ids))
							->execute();
					}
					
					\Messages::success('Attribute option successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete attribute option</strong>');
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		if(\Request::is_hmvc())
		{
			\Messages::reset();
		}
		else
		{
			\Response::redirect(\Input::referrer(\Uri::create('admin/attribute/list')));
		}
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
			
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Attribute::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
}
