<?php

	namespace Attribute;

	class Model_Attribute_Group extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
		
	    // Set the table to use
	    protected static $_table_name = 'attribute_groups';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'title',
		    'sort',
		);
		
		 /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
		    			// Get group attributes
						$item->get_attributes = static::lazy_load(function() use ($item){
							
							$attributes = Model_Attribute_To_Groups::find(function($query) use ($item)
							{
							    return $query->where('group_id', $item->id);
							}, 'attribute_id');
							
							if(!empty($attributes))
							{
								$attributes = '(' . implode(',', array_keys($attributes)) . ')';
							
								return Model_Attribute::find(function($query) use ($attributes)
								{
								    return $query->where('id', 'IN', \DB::expr($attributes))
                                        ->order_by('sort', 'asc');
								}, 'id');
							}
							
							return array();
							
		    			}, $item->id, 'attributes');
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result; 
	    }
		    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Attribute\Model_Attribute_Group::$temp['lazy.'.$id.$type]))
		    	{
		    		\Attribute\Model_Attribute_Group::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Attribute\Model_Attribute_Group::$temp['lazy.'.$id.$type])) 
			    			\Attribute\Model_Attribute_Group::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Attribute\Model_Attribute_Group::$temp['lazy.'.$id.$type])) 
			    			\Attribute\Model_Attribute_Group::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Attribute\Model_Attribute_Group::$temp['lazy.'.$id.$type];
	    	};
	    }
		
		/**
		 * Prep some query values
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
				
			return $vars;
		}
		
		/**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
			
			return $val;
		}
	}