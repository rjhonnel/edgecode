<?php

	namespace Attribute;

	class Model_Attribute_To_Groups extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'attribute_to_groups';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'attribute_id',
		    'group_id',
            'sort',
		);
		
	}