<?php

	namespace Attribute;

	class Model_Attribute_Option extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'attribute_options';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'attribute_id',
		    'title',
		    'sort',
		);
		
		/**
		 * Prep some query values
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				if(!isset($vars['attribute_id'])) $vars['attribute_id'] = 0;
				
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp WHERE tmp.attribute_id = ' . $vars['attribute_id'] . ')');
			}
				
			return $vars;
		}
		
		/**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			
			$val->add('option_title', 'Option Title')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
			
			return $val;
		}
	}