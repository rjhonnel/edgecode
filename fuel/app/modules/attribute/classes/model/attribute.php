<?php

	namespace Attribute;

	class Model_Attribute extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'attributes';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'type',
		    'title',
		    'sort',
            'name',
            'enable_image'
		);
		
		protected static $_defaults = array(
		    'type' => 'input',
		);
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
		    			// Get attribute options
						$item->get_options = static::lazy_load(function() use ($item){
		    				return Model_Attribute_Option::find(array(
							    'where' => array(
							        'attribute_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							), 'id');
		    			}, $item->id, 'options');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Attribute\Model_Attribute::$temp['lazy.'.$id.$type]))
		    	{
		    		\Attribute\Model_Attribute::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Attribute\Model_Attribute::$temp['lazy.'.$id.$type])) 
			    			\Attribute\Model_Attribute::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Attribute\Model_Attribute::$temp['lazy.'.$id.$type])) 
			    			\Attribute\Model_Attribute::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Attribute\Model_Attribute::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
	
			return $val;
		}
		
		/**
		 * Save sort options
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
				
			return $vars;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				if(ucfirst($name) == 'Attribute')
					$class = 'Attribute\Model_Attribute';
				else
					$class = 'Attribute\Model_Attribute_' . ucfirst($name);
					
				return new $class;
			}
			
			return static::forge();
		}
		
	}