<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
     
    'bulk_actions' => array(
        '0' => 'Select Action',
        'delete' => 'Delete',
     )
 
 );