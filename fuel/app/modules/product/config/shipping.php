<?php

return array(
    'default' => 10,
    'pickup' => 0,
    'price' => array(
        // NSW
        array(
            'state' => 'nsw',
            'from'  => '1000',
            'to'    => '1999',
            'price' => 40,
        ),
        array(
            'state' => 'nsw',
            'from'  => '2000',
            'to'    => '2599',
            'price' => 40,
        ),
        array(
            'state' => 'nsw',
            'from'  => '2619',
            'to'    => '2898',
            'price' => 40,
        ),
        array(
            'state' => 'nsw',
            'from'  => '2921',
            'to'    => '2999',
            'price' => 40,
        ),
        
        // ACT
        array(
            'state' => 'act',
            'from'  => '0200',
            'to'    => '0299',
            'price' => 40,
        ),
        array(
            'state' => 'act',
            'from'  => '2600',
            'to'    => '2618',
            'price' => 40,
        ),
        array(
            'state' => 'act',
            'from'  => '2900',
            'to'    => '2920',
            'price' => 40,
        ),
        
        // VIC
        array(
            'state' => 'vic',
            'from'  => '3000',
            'to'    => '3999',
            'price' => 40,
        ),
        array(
            'state' => 'vic',
            'from'  => '8000',
            'to'    => '8999',
            'price' => 40,
        ),
        
        // QLD
        array(
            'state' => 'qld',
            'from'  => '4000',
            'to'    => '4999',
            'price' => 50,
        ),
        array(
            'state' => 'qld',
            'from'  => '9000',
            'to'    => '9999',
            'price' => 50,
        ),
        
        // SA
        array(
            'state' => 'sa',
            'from'  => '5000',
            'to'    => '5799',
            'price' => 40,
        ),
        array(
            'state' => 'sa',
            'from'  => '5800',
            'to'    => '5999',
            'price' => 40,
        ),
        
        // WA
        array(
            'state' => 'wa',
            'from'  => '6000',
            'to'    => '6797',
            'price' => 70,
        ),
        array(
            'state' => 'wa',
            'from'  => '6800',
            'to'    => '6999',
            'price' => 70,
        ),
        
        // TAS
        array(
            'state' => 'tas',
            'from'  => '7000',
            'to'    => '7799',
            'price' => 40,
        ),
        array(
            'state' => 'tas',
            'from'  => '7800',
            'to'    => '7999',
            'price' => 40,
        ),
        
        // NT
        array(
            'state' => 'nt',
            'from'  => '0800',
            'to'    => '0899',
            'price' => 70,
        ),
        array(
            'state' => 'nt',
            'from'  => '0900',
            'to'    => '0999',
            'price' => 70,
        ),
    ),
);