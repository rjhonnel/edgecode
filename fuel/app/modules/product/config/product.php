<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
 
 	'image'	=> array(
 		'enabled'	=> true,
 		'required'	=> false,
 		'multiple'	=> true,
 		/**
	 	 * Define image required image sizes, locations and other options
	 	 * Array key is image location
	 	 * 
	 	 * First array member will be considered to be thumbnail image location
	 	 */
	 	'resize' => array(
		    'thumbs/' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            array('resize', 86, 86, true, true),
		            array('save_pa', 'thumbs/')
		        )
		    ),
            'medium/' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            array('resize', 220, 260, true, true),
		            array('save_pa', 'medium/')
		        )
		    ),
            'large/' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            array('resize', 460, 552, true, true),
		            array('save_pa', 'large/')
		        )
		    ),
		    '' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            //array('resize', 636, 636, true, true),
		            // array('save_pa', '')
		        )
		    ),
	 	),
	 	/**
	 	 * Used to display information about best image size in views
	 	 * Set this to size of largest image defined in array above
	 	 */
	 	'best_size' => '460x552px',
	 	/**
	 	 * Set location based items
	 	 */
	 	'location' => array(
	 		'folder'	=> 'images',
	 		'root' 		=> function(){ 
	 			return DOCROOT . 'media' . DIRECTORY_SEPARATOR . \Config::get('details.image.location.folder') . DIRECTORY_SEPARATOR; 
	 		},
	 		
	 		/*
		 	 * Get all image folders from config item above.
		 	 * No need to set this yourself, just call it
		 	 */
		 	'folders' => function(){
		 		$image_locations = \Config::get('details.image.resize');
		 		if(!empty($image_locations))
		 		{
		 			foreach($image_locations as $key => $value) 
		 				$return[] = \Config::get('details.image.location.root') . ($key != '' ? $key . DIRECTORY_SEPARATOR : $key);
		 		}
		 		
		 		return isset($return) ? $return : array();
		 	}
	 	),
	 	// END OF LOCATION
 	), 
 	// END OF IMAGES
 	
 	'file'	=> array(
 		'enabled'	=> true,
 		'required'	=> false,
 		'multiple'	=> true,
	 	/**
	 	 * Set location based items
	 	 */
	 	'location' => array(
	 		'folder'	=> 'files',
	 		'root' 		=> function(){ 
	 			return DOCROOT . 'media' . DIRECTORY_SEPARATOR . \Config::get('details.file.location.folder') . DIRECTORY_SEPARATOR; 
	 		},
	 	),
	 	// END OF LOCATION
 	), 
 	// END OF FILES
 	
 	'video'	=> array(
 		'enabled'	=> true,
 		'required'	=> false,
 		'multiple'	=> true,
 		/**
	 	 * Define image required image sizes, locations and other options
	 	 * Array key is image location
	 	 * 
	 	 * First array member will be considered to be thumbnail image location
	 	 */
	 	'resize' => array(
		    'thumbs/' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            array('resize', 200, 140, true, true),
		            array('save_pa', 'thumbs/')
		        )
		    ),
		    '' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            array('resize', 600, 400, true, true),
		            array('save_pa', '')
		        )
		    ),
	 	),
	 	/**
	 	 * Used to display information about best image size in views
	 	 * Set this to size of largest image defined in array above
	 	 */
	 	'best_size' => '600x400px',
	 	/**
	 	 * Set location based items
	 	 */
	 	'location' => array(
	 		'folder'	=> 'videos',
	 		'root' 		=> function(){ 
	 			return DOCROOT . 'media' . DIRECTORY_SEPARATOR . \Config::get('details.video.location.folder') . DIRECTORY_SEPARATOR; 
	 		},
	 		
	 		/*
		 	 * Get all image folders from config item above.
		 	 * No need to set this yourself, just call it
		 	 */
		 	'folders' => function(){
		 		$image_locations = \Config::get('details.video.resize');
		 		if(!empty($image_locations))
		 		{
		 			foreach($image_locations as $key => $value) 
		 				$return[] = \Config::get('details.video.location.root') . ($key != '' ? $key . DIRECTORY_SEPARATOR : $key);
		 		}
		 		
		 		return isset($return) ? $return : array();
		 	}
	 	),
	 	// END OF LOCATION
 	), 
 	// END OF VIDEOS
                
    'bulk_actions' => array(
        '0' => 'Select Action',
        //'export' => 'Export Products',
        'delete' => 'Delete',
        'active_enable' => 'Active',
        'active_disable' => 'Inactive',
        //'assign_to_product_category' => 'Assign to Product Category',
        //'move_to_product_group' => 'Move to Product Group',
        //'assign_product_label' => 'Assign Product Label',
     ),

    'product_type' => array(
    	'simple' => 'Simple',
    	'pack' => 'Pack',
    ),
    'pack' => array(
    	'type' =>  array(
	    	'included' => 'Included (no additional cost)',
	    	'extras' => 'Extras (selections charged at item price)',
	    ),
    ),
 );