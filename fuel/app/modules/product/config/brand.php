<?php
 return array(

 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,

 	'image'	=> array(
 		'enabled'	=> true,
 		'required'	=> false,
 		'multiple'	=> true,
 		/**
	 	 * Define image required image sizes, locations and other options
	 	 * Array key is image location
	 	 *
	 	 * First array member will be considered to be thumbnail image location
	 	 */
	 	'resize' => array(
		   'thumbs/' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            array('resize', 86, 86, true, true),
		            array('save_pa', 'thumbs/')
		        )
		    ),
            'medium/' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            array('resize', 230, 230, true, true),
		            array('save_pa', 'medium/')
		        )
		    ),
		    '' => array(
		        'bgcolor' 	=> '#ffffff',
		        'filetype' 	=> null,
		        'quality' 	=> 75,
		        'actions' 	=> array(
		            // array('resize', 419, 274, true, true),
		            // array('save_pa', '')
		        )
		    ),
	 	),
	 	/**
	 	 * Used to display information about best image size in views
	 	 * Set this to size of largest image defined in array above
	 	 */
	 	'best_size' => '400x350',
	 	/**
	 	 * Set location based items
	 	 */
	 	'location' => array(
	 		'folder'	=> 'images',
	 		'root' 		=> function(){
	 			return DOCROOT . 'media' . DIRECTORY_SEPARATOR . \Config::get('details.image.location.folder') . DIRECTORY_SEPARATOR;
	 		},

	 		/*
		 	 * Get all image folders from config item above.
		 	 * No need to set this yourself, just call it
		 	 */
		 	'folders' => function(){
		 		$image_locations = \Config::get('details.image.resize');
		 		if(!empty($image_locations))
		 		{
		 			foreach($image_locations as $key => $value)
		 				$return[] = \Config::get('details.image.location.root') . ($key != '' ? $key . DIRECTORY_SEPARATOR : $key);
		 		}

		 		return isset($return) ? $return : array();
		 	}
	 	),
	 	// END OF LOCATION
 	),
 	// END OF IMAGES
 );