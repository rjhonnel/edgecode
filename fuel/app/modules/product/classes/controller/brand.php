<?php

namespace Product;

class Controller_Brand extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/';
	
	public $theme_view_path;
    
	public function before()
	{
		parent::before();
		
		\Config::load('product::brand', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
		
		$this->theme_view_path = DOCROOT . \Theme::instance()->asset_path($this->view_dir);
	}
	
	/**
	 * Index page
	 * 
	 * @access  public
     * @param   $slug
	 */
	public function action_index($slug = false, $my_products = null)
	{
        if($my_products == 1) Model_Product::$filter_by_pricing_group = 'assigned';
        else Model_Product::$filter_by_pricing_group = 'not_assigned';
        $this->show($slug, $my_products);
    }
    
    protected function show($slug = false, $my_products = null)
    {
        if(isset($sorting)) Model_Brand::$sorting = $sorting;
        
        if($brand = Model_Brand::get_by_slug($slug))
        {
        	$view_file = 'brand_list';

        	$items = $brand->products;

            if($product_ids = array_keys($items))
            {
                $items = Model_Product::find(function($query) use ($product_ids){
                        $query->where('id', 'in', $product_ids);
                        
                        if(\Input::get('sort') == 'title_desc')
                            $query->order_by('title', 'desc');
                        else
                            $query->order_by('title', 'asc');
                });
            }
            
        	if(is_null($items)) $items = array();

            // Initiate pagination
            $pagination = \Hybrid\Pagination::make(array(
                'total_items' => count($items),
                'per_page' => \Input::get('per_page', 15),
                'uri_segment' => null,
            ));

            // Remove unwanted items, and show only required ones
            $items = array_slice($items, $pagination->offset, $pagination->per_page);

            \Theme::instance()->set_partial('content', $this->view_dir . $view_file)
                ->set('brand', $brand, false)
                ->set('items', $items, false)
                ->set('pagination', $pagination, false);

            \View::set_global('title', ($brand->seo->meta_title ?: $brand->title));
            \View::set_global('meta_description', ($brand->seo->meta_description ?: ''));
            \View::set_global('meta_keywords', ($brand->seo->meta_keywords ?: ''));
            $robots = array('meta_robots_index' => ( $brand->seo->meta_robots_index == 1 ? 'index' : 'noindex' ), 'meta_robots_follow' => ( $brand->seo->meta_robots_follow == 1 ? 'follow' : 'nofollow' ) );
            \View::set_global('robots', $robots);
            \View::set_global('canonical', $brand->seo->canonical_links);
            \View::set_global('h1_tag', $brand->seo->h1_tag);
            if($brand->seo->redirect_301){
                \Response::redirect($brand->seo->redirect_301);
            }
        }
        else
        {
            throw new \HttpNotFoundException;
        }
    }
}