<?php

namespace Product;

class Controller_Infotab extends \Controller_Base_Public
{
	
	/**
	 * Parse info tab content
	 * 
	 * @param $table = Table to parse
	 */
	public function action_parse_info_table($table = false)
	{
		if(\Input::is_ajax())
		{
			$table = \Input::post('table', '');
			echo \Helper::parse_info_table($table);
		}
		
		if(\Request::is_hmvc())
		{
			echo \Helper::parse_info_table($table);
		}
		
	}

	public function action_get_hotspot($hotspot_id = false)
	{
		if(is_numeric($hotspot_id))
		{
			if($hotspot = Model_Infotab_Image::find_by_pk($hotspot_id))
			{
				$image = '';
				if($hotspot->image)
				{
					$image = array(
						'url' => $hotspot->image? \Uri::create('media/images/' . $hotspot->image) :'',
						'alt_text' => $hotspot->alt_text,
					);
				}

				$video = '';
				if($hotspot->video)
				{
					$video = array(
						'url' => $hotspot->video,
						'video_image' => $hotspot->video_details['thumbnail']['large'],
						'video_title' => $hotspot->video_title,
					);
				}

				echo json_encode(array(
								'status' => 'success',
								'message' => 'Found hotspot.',
								'title' => $hotspot->title,
								'description' => $hotspot->description,
								'image' => $image,
								'video' => $video,
							));
				exit;
			}
		}

		echo json_encode(array('status' => 'error', 'message' => 'No hotspot found'));
		exit;
	}
}