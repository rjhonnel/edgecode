<?php

namespace Product;

class Controller_Product extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/';
	
	public $theme_view_path;
    
	public function before()
	{
		parent::before();
		
		\Config::load('product::product', 'details');
        \Config::load('order::order', 'order');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
		
		$this->theme_view_path = DOCROOT . \Theme::instance()->asset_path($this->view_dir);
	}
	
	/**
	 * Index page
	 * 
	 * @access  public
     * @param   $slug
	 */
	public function action_index($slug = false, $my_products = null)
	{
        if($my_products == 1) Model_Product::$filter_by_pricing_group = 'assigned';
        else Model_Product::$filter_by_pricing_group = 'not_assigned';
        $this->show($slug, $my_products);
    }

    protected function show($slug = false, $my_products = null)
    {
        $category_parents = false;
            
        if($product = Model_Product::get_by_slug($slug))
        {
            // Find main category
            
            $category = \Product\Model_Category::find_by('id',$product->data['category_id']);
            $c_category = \Product\Model_Category::find_by('id',$category[0]['parent_id']);
         
            if(!empty((array)json_decode($category[0]->user_group))){
                if(!in_array($group_id,(array)json_decode($category[0]->user_group))){
                  \Messages::error('Your not allowed to access this page.');
                  \Response::redirect('/');
                }
            }
            else if($c_category){
                if(!empty((array)json_decode($c_category[0]->user_group))){
                    if(!in_array($group_id,(array)json_decode($c_category[0]->user_group))){
                    \Messages::error('Your not allowed to access this page.');
                     \Response::redirect('/');
                    }
                } 
            } 

            if(!empty($product->categories))
            {
                $parents = array();
                if($category_parents_id = Model_Category::find_parents(key(($product->categories)), $parents, true))
                {
                   $category_parents = Model_Category::find(array('where' => array(array('id', 'in', $category_parents_id))));
                }
            }

            /** Get all packs under this product **/
            $product_id = $product->id;
            $pack_list = array();
            $get_packs = \Product\Model_Packs::find(function($query) use ($product_id) {
                $query->where('product_id', $product_id);
                $query->order_by('id', 'asc');
            });
            if($get_packs)
            {
                foreach ($get_packs as $pack)
                {
                    $get_image_id = '';
                    $get_image = '';
                    if($pack->image)
                    {
                        $get_image_id = $pack->image->id;
                        $get_image = $pack->image->image;
                    }

                    array_push($pack_list, array(
                        'id' => $pack->id,
                        'name' => $pack->name,
                        'description' => $pack->description,
                        'type' => $pack->type,
                        'selection_limit' => $pack->selection_limit,
                        'products' => explode(',', $pack->products),
                        'image_id' => $get_image_id,
                        'image' => $get_image,
                    ));
                }
            }
            /** END: Get all packs under this product **/
            
            \Theme::instance()->set_partial('content', $this->view_dir . 'product')
                ->set('product', $product, false)
                ->set('category_parents', $category_parents, false)
                ->set('pack_list', $pack_list)
                ->set('product_data', $product->data, false);

            \View::set_global('menu', 'admin/product/list');
            \View::set_global('title', ($product->seo->meta_title ?: $product->title));
            \View::set_global('meta_description', ($product->seo->meta_description ?: ''));
            \View::set_global('meta_keywords', ($product->seo->meta_keywords ?: ''));
            $robots = array('meta_robots_index' => ( $product->seo->meta_robots_index == 1 ? 'index' : 'noindex' ), 'meta_robots_follow' => ( $product->seo->meta_robots_follow == 1 ? 'follow' : 'nofollow' ) );
            \View::set_global('robots', $robots);
            \View::set_global('canonical', $product->seo->canonical_links);
            \View::set_global('h1_tag', $product->seo->h1_tag);
            if($product->seo->redirect_301){
                \Response::redirect($product->seo->redirect_301);
            }
        }
        else
        {
            throw new \HttpNotFoundException;
        }
    }
    
     /**
	 * Recently viewed products
	 * 
	 * @access  public
     * @param   $product_id
     * @return  $recently_viewed_items   array
	 */
    public function recently_viewed($product_id = false, $number_of_items = 30)
    {
        $recently_viewed_items = array();
        if(!is_numeric($product_id)) return $recently_viewed_items;
        
        $recently_viewed = \Session::get('recently_viewed', array());
        $recently_viewed = array_slice($recently_viewed, 0, $number_of_items, true);   
        $recently_viewed = array_reverse($recently_viewed, true);

        $k = array_keys($recently_viewed, $product_id);

        if(!empty($k)) foreach($k as $v) unset($recently_viewed[$v]);

        $recently_viewed[] = $product_id;
        $recently_viewed = array_reverse($recently_viewed, true);

        \Session::set('recently_viewed', $recently_viewed);

        // Get recently viewed products from DB
        $recently_viewed_items = Model_Product::find(function($query) use ($recently_viewed){
            $query->where('id', 'in', $recently_viewed);
            $query->order_by('FIELD', '(id, ' . implode(',', $recently_viewed) . ')');
        });
        
        return $recently_viewed_items;
    }
    
    public function action_product_data($id)
    {
        $selected_attributes_json = false;
        if(\Input::post() && $product = \Product\Model_Product::find_one_by_id($id))
        {
            $post = \Input::post();
            $selected_attributes = array();
            if(isset($post['select']) && !empty($post['select']))
            {
                ksort($post['select']);
                $selected_attributes_json = json_encode($post['select']);
            }
            
            echo json_encode(Model_Product::product_data($product, $selected_attributes_json, \Input::post('select'), \Input::post('attributeid')));
        }
    }
    
   /**
	 * Search page
	 * 
	 * @access  public
	 */
	public function action__search()
	{
        $keyword = \Input::get('keyword');
        $items = null;
        $products = array();
        
        //if(!$get = \Input::get()) \Response::redirect(\Input::referrer(\Uri::create('/')));
        
        //if(empty($get['searchTerm'])) \Response::redirect(\Input::referrer(\Uri::create('/')));
        
        if(strlen(trim($keyword)) < 3)
        {
            \Messages::error('Search word must be at least 3 characters long.');
        }
        else
        {
            $search_keywords = array_map('trim', explode(',', $keyword));
        
            $products = Model_Product::find(function($query) use ($search_keywords)
            {   
                $query->where('status', '=', 1);
                if(count($search_keywords) > 1)
                {
                    foreach ($search_keywords as $key => $keyword)
                    {
                        if($key == 0) 
                        {
                            $query->where('title', 'like', "%{$keyword}%");
                        }
                        else
                        {
                            $query->or_where('title', 'like', "%{$keyword}%");
                        }
                    }
                }
                else
                {
                    $query->where('title', 'like', "%{$search_keywords[0]}%");
                }
                
                $query->order_by('title');
                return $query;
            }, 'id');

            $items = $products;

            if($items)
            {
                // Price range
                if(\Input::get('range'))
                {
                    $range = explode('-', \Input::get('range'));
                    $price_from = isset($range[0]) && is_numeric($range[0]) ? $range[0] : null ;
                    $price_to = isset($range[1]) && is_numeric($range[1]) ? $range[1] : null ;
                    if(!is_null($price_from) && !is_null($price_to))
                    {
                        foreach ($items as $key => $item)
                        {
                            if($item->default_price[0] >= $price_from && $item->default_price[0] <= $price_to) continue;
                            unset($items[$key]);
                        }
                    }
                }
            }

            if($items)
            {
                if(\Input::get('sort') == 'price_asc')
                {
                    $items = \Product\Model_Product::get_sorted_products($items);
                }
                elseif(\Input::get('sort') == 'price_desc')
                {
                    $items = array_reverse(\Product\Model_Product::get_sorted_products($items));
                }
            }
        }
        
        \Helper::sorting($items);
        
         // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();

        // Initiate pagination
        $pagination = \Hybrid\Pagination::forge(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 30),
            'uri_segment' => null,
            'base_uri' => 'search',
        ));

        // Remove unwanted items, and show only required ones
        $items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        \View::set_global('title', 'Search');
        \Theme::instance()->set_partial('content', $this->view_dir . 'search')
                ->set('items', $items, false)
                ->set('pagination', $pagination, false)
                ->set('products', $products, false);
    }
    
     /**
	 * Special page
	 * 
	 * @access  public
     * @param   $slug
	 */
	public function action_special($type = 'best-sellers')
	{
        $types = array(
            'best-sellers' => 24,
            'new-release' => 25,
            'sale' => 26,
            'popular-products' => 1
        );

        if(!isset($types[$type]))
        {
            throw new \HttpNotFoundException;
        }
        
        if(!$group = Model_Group::find_one_by_id($types[$type]))
        {
            throw new \HttpNotFoundException;
        }
        
        if($type == 'sale')
        {
            $products = Model_Product::find();
        
            foreach ($products as $key => $product)
            {
                if(empty($product->sale_price) || !reset(($product->sale_price))->price > 0)
                {
                    unset($products[$key]);
                }
            }
        }
        else
        {
            $products = $group->products;
        }
        
        $items = $products;
        
        if($items)
        {
            // Price range
            if(\Input::get('range'))
            {
                $range = explode('-', \Input::get('range'));
                $price_from = isset($range[0]) && is_numeric($range[0]) ? $range[0] : null ;
                $price_to = isset($range[1]) && is_numeric($range[1]) ? $range[1] : null ;
                if(!is_null($price_from) && !is_null($price_to))
                {
                    foreach ($items as $key => $item)
                    {
                        if($item->default_price[0] >= $price_from && $item->default_price[0] <= $price_to) continue;
                        unset($items[$key]);
                    }
                }
            }
        }
        
        if($items)
        {
            if(\Input::get('sort') == 'price_asc')
            {
                $items = \Product\Model_Product::get_sorted_products($items);
            }
            elseif(\Input::get('sort') == 'price_desc')
            {
                $items = array_reverse(\Product\Model_Product::get_sorted_products($items));
            }
        }
        
         // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();

        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 12),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        $items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        \Theme::instance()->set_partial('content', $this->view_dir . 'special')
                ->set('items', $items, false)
                ->set('pagination', $pagination, false)
                ->set('products', $products, false)
                ->set('product_group', $group);
    }
    
    public function action_add_to_cart($return = 'return')
    {

        $this->add_to_cart($return);
    }
    
    public function add_to_cart($return = 'return')
    {
        if(!\Input::post()) return false;
         // check for a valid CSRF token
        // if (!\Security::check_token())
        // {
        //     \Messages::error('CSRF attack or expired CSRF token.');
        //     return false;
        // }
        
        $post = \Input::post();

        $product_id = $post['product_id'];
        
        if(!$product = Model_Product::find_one_by_id($product_id))
        {
            echo json_encode(array('status' =>'error', 'message' => 'Product not found.'));
            exit;
        }
        
        $selected_attributes = array();
        $selected_attributes_json = null;
        
        if(isset($post['select']) && !empty($post['select']))
        {
            ksort($post['select']);
            $selected_attributes_json = json_encode($post['select']);
        }

        $product_data = Model_Product::product_data($product, $selected_attributes_json, \Input::post('select'), \Input::post('attributeid'));
        $category_list = '';
        if(!empty($product->categories))
        {
            $categories = array();
            foreach ($product->categories as $category)
            {
                $categories[] = $category->title;
            }
            if($categories) $category_list = implode(',', $categories);
        }
        
        if(!empty($product_data))
        {
            $attr_obj = null;
            if(!empty($product_data['current_attributes'])) $attr_obj = $product_data['current_attributes'][0]->product_attribute;
            
            // check if product type is 'pack'
            $item_packs = array();
            $extra_items = array();
            if($product->product_type == 'pack')
            {
                if(\Input::post('packs'))
                {
                    $get_packs = \Product\Model_Packs::find(function($query) use ($product) {
                        $query->where('product_id', $product->id);
                        $query->order_by('id', 'asc');
                    });

                    if($get_packs)
                    {
                        foreach ($get_packs as $pack)
                        {
                            $pack_products = array();

                            if($pack->type == 'included')
                            {
                                $get_pack_selection = isset(\Input::post('packs')[$pack->id])?count(\Input::post('packs')[$pack->id]):0;
                                
                                if($get_pack_selection == 0)
                                {
                                    echo json_encode(array('status' =>'error', 'message' => 'Select '.intval($pack->selection_limit).' item(s) for '.$pack->name ));
                                    exit;
                                    break;
                                }
                                if($get_pack_selection > intval($pack->selection_limit))
                                {
                                    echo json_encode(array('status' =>'error', 'message' => 'Selection limit is '.intval($pack->selection_limit).' for '.$pack->name ));
                                    exit;
                                    break;
                                }

                                foreach (\Input::post('packs')[$pack->id] as $key => $get_product_id)
                                {
                                    $get_item = \Product\Model_Product::find_one_by_id($get_product_id);

                                    array_push($pack_products, array(
                                        'id' => $get_item->id,
                                        'name' => $get_item->title,
                                    ));
                                }

                                array_push($item_packs, array(
                                    'id' => $pack->id,
                                    'name' => $pack->name,
                                    'types' => $pack->type,
                                    'selection_limit' => $pack->selection_limit,
                                    'products' => $pack_products,
                                ));
                            }
                            else if($pack->type == 'extras')
                            {
                                if(isset(\Input::post('packs')[$pack->id]))
                                {
                                    $quantity_extra_hold = \Input::post('quantity_extra');
                                    foreach (\Input::post('packs')[$pack->id] as $key => $get_product_id)
                                    {
                                        $extra_qty = intval($quantity_extra_hold[$pack->id][$key]);
                                        $get_item = \Product\Model_Product::find_one_by_id($get_product_id);
                                        
                                        if(intval($extra_qty) < intval($get_item->minimum_order))
                                        {
                                            echo json_encode(array('status' =>'error', 'message' => 'Minimum order of '.$get_item->title.' is '.intval($get_item->minimum_order)));
                                            exit;
                                            break;
                                        }

                                        array_push($extra_items, array(
                                            'id' => $get_item->id,
                                            'name' => $get_item->title,
                                            'quantity' => $extra_qty,
                                        ));
                                    }
                                }
                            }
                        }   
                    }
                }
                else
                {
                    echo json_encode(array('status' =>'error', 'message' => 'Please select product pack(s)'));
                    exit;
                }

            }

            $item = array(
                'title' => $product->title,
                'id' => $product->id,
                'product_attribute_id' => $attr_obj ? $attr_obj->id : null,
                'quantity' => $post['quantity'],
                'attributes' =>  $attr_obj ? $attr_obj->attributes : null,
                'product_code' => $product_data['code'],
                'delivery_charge' => $product_data['delivery_charge'],
                'minimum_order' => $product->minimum_order,
                'product_category' => $category_list,
                'attributes_json'   => $attr_obj ? json_encode(\Product\Model_Attribute::get_combination($attr_obj->attributes)) : '',
                'unique_id' => uniqid(),
                'packs' => $item_packs,
            );

            if($product_data['sale'])
            {
               $item += array(
                   'price' => $product_data['sale'],
                   'price_type' => 'sale_price'
               );
            }
            else 
            {
                $item += array(
                   'price' => $product_data['price'],
                   'price_type' => 'retail_price'
               );
            }

            if ($this->do_not_allow_buy_out_of_stock && $product_data['stock_quantity'] < 1 ) {

                echo json_encode(array('status' =>'error', 'message' => 'Product is Out of Stock.'));
                exit;
            }

            $uid = \Cart::generateUID($item);
            
            if(\Cart::exists($uid)) {
                $cart_item = \Cart::item($uid);
                $quantity = $cart_item->get('quantity');

                if ($product_data['stock_quantity'] > 0 && $product_data['stock_quantity'] <= $quantity) {


                    echo json_encode(array('status' =>'error', 'message' => $product->title.' has not enough stock to fulfill your request.'));
                    exit;
                }

                if(intval($quantity) < intval($cart_item->get('minimum_order'))){
                    echo json_encode(array('status' =>'error', 'message' => 'Minimum order of this product is '.$post['minimum_order']));
                    exit;
                }
            }
            else
            {
                if ($product_data['stock_quantity'] > 0 && $product_data['stock_quantity'] < $post['quantity']) {


                    echo json_encode(array('status' =>'error', 'message' => $product->title.' has not enough stock to fulfill your request.'));
                    exit;
                }

                if(isset($post['minimum_order'])){
                    if($post['quantity'] < $post['minimum_order']){
                        echo json_encode(array('status' =>'error', 'message' => 'Minimum order of this product is '.$post['minimum_order']));
                        exit;
                    }
                }
            }

            // Add product that is added to cart to abandon cart
            $order_id = \Session::get('order.id', false);
            if(is_numeric($order_id))
            {
                $item_exists = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'product_id' => $product->id, 'order_id' => $order_id, 'attributes_id' => $item['attributes'] ) ) );
                if($item_exists)
                {
                    $qty = $item_exists[0]->quantity + $item['quantity'];
                    $item_exists[0]->set(array('quantity' => $qty, 'subtotal' => ($qty*$item_exists[0]->price) ));
                    $item_exists[0]->save();
                }
                else
                {
                    $abandon_cart_products = \Order\Model_Abandon_Cart_Products::forge(array(
                        'order_id'          => $order_id,
                        'title'             => $product->title,
                        'code'              => $product_data['code'],
                        'price'             => $product_data['price'],
                        'price_type'        => $item['price_type'],
                        'quantity'          => $item['quantity'],
                        'product_id'        => $product->id,
                        'subtotal'          => ($product_data['price']*$item['quantity']),
                        'attributes'        => $attr_obj ? json_encode(\Product\Model_Attribute::get_combination($item['attributes'])) : null,
                        'attributes_id'     => $item['attributes'],
                        'delivery_charge'   => isset($product_data['delivery_charge'])?$product_data['delivery_charge']:0,
                        'packs'             => $item['packs']?json_encode($item['packs']):null,
                    ));

                    if($category_list) $abandon_cart_products->product_category = $category_list;

                    $abandon_cart_products->save();
                }
            }

            if($return == 'return')
            {
                \Cart::add($item);

                $this->add_to_cart_extra_packs($order_id, $extra_items);

                 // Always return cart item id
                $uid = \Cart::generateUID($item);
                if(\Cart::exists($uid)) //return $uid;
                    echo json_encode(array('status' =>'success', 'message' => 'Product successfully added to cart.'));
                exit;
            }
            else
            { 
                $uid = \Cart::generateUID($item);
                if(\Cart::exists($uid)) // echo $uid;
                echo json_encode(array('status' =>'success', 'message' => 'Product successfully added to cart.'));
                exit;
            }
            
            \Messages::success('Product successfully added to cart.');
            echo json_encode(array('status' =>'success', 'message' => 'Product successfully added to cart.'));
            exit;
        }
        
        return false;
    }

    public function add_to_cart_extra_packs($order_id, $extra_items)
    {
        if($extra_items)
        {
            foreach ($extra_items as $extra_item)
            {
                if($product = Model_Product::find_one_by_id($extra_item['id']))
                {
                    $selected_attributes_json = null;
                    $product_data = Model_Product::product_data($product, $selected_attributes_json);
                    $category_list = '';
                    if(!empty($product_data))
                    {
                        $attr_obj = null;
                        if(!empty($product_data['current_attributes'])) $attr_obj = $product_data['current_attributes'][0]->product_attribute;

                        $item_packs = array();

                        $item = array(
                            'title' => $product->title,
                            'id' => $product->id,
                            'product_attribute_id' => $attr_obj ? $attr_obj->id : null,
                            'quantity' => $extra_item['quantity'],
                            'attributes' =>  $attr_obj ? $attr_obj->attributes : null,
                            'product_code' => $product_data['code'],
                            'delivery_charge' => $product_data['delivery_charge'],
                            'minimum_order' => $product->minimum_order,
                            'product_category' => $category_list,
                            'attributes_json'   => $attr_obj ? json_encode(\Product\Model_Attribute::get_combination($attr_obj->attributes)) : '',
                            'unique_id' => uniqid(),
                            'packs' => $item_packs,
                        );

                        if($product_data['sale'])
                        {
                           $item += array(
                               'price' => $product_data['sale'],
                               'price_type' => 'sale_price'
                           );
                        }
                        else 
                        {
                            $item += array(
                               'price' => $product_data['price'],
                               'price_type' => 'retail_price'
                           );
                        }

                        $uid = \Cart::generateUID($item);

                        // Add product that is added to cart to abandon cart
                        if(is_numeric($order_id))
                        {
                            $item_exists = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'product_id' => $product->id, 'order_id' => $order_id, 'attributes_id' => $item['attributes'] ) ) );
                            if($item_exists)
                            {
                                $qty = $item_exists[0]->quantity + $item['quantity'];
                                $item_exists[0]->set(array('quantity' => $qty, 'subtotal' => ($qty*$item_exists[0]->price) ));
                                $item_exists[0]->save();
                            }
                            else
                            {
                                $abandon_cart_products = \Order\Model_Abandon_Cart_Products::forge(array(
                                    'order_id'          => $order_id,
                                    'title'             => $product->title,
                                    'code'              => $product_data['code'],
                                    'price'             => $product_data['price'],
                                    'price_type'        => $item['price_type'],
                                    'quantity'          => $item['quantity'],
                                    'product_id'        => $product->id,
                                    'subtotal'          => ($product_data['price']*$item['quantity']),
                                    'attributes'        => $attr_obj ? json_encode(\Product\Model_Attribute::get_combination($item['attributes'])) : null,
                                    'attributes_id'     => $item['attributes'],
                                    'delivery_charge'   => isset($product_data['delivery_charge'])?$product_data['delivery_charge']:0,
                                    'packs'             => $item['packs']?json_encode($item['packs']):null,
                                ));

                                if($category_list) $abandon_cart_products->product_category = $category_list;

                                $abandon_cart_products->save();
                            }
                        }

                        \Cart::add($item);
                        $uid = \Cart::generateUID($item);
                    }
                }
            }
        }
    }
    
    public function action_upload_artwork($product_id = null)
    {
        // if(!\Input::is_ajax()) die('Access is denied.');
        
        if(!$product = \Product\Model_Product::find_one_by_id($product_id))
        {
            die('Internal server error, please try again.');
        }
        
        // Addd to cart
        if(!$cart_uid = $this->add_to_cart())
        {
            die('Internal server error, please try again.');
        }
        
        if($item_exists = \Cart::exists($cart_uid))
        {
            \Session::set('cart_uid', $cart_uid);
        }
        else
        {
            die('Internal server error, please try again.');
        }
        
        $cart_item = \Cart::item($cart_uid);
        $quantity = $cart_item->get('quantity');
        $config_max_types = \Config::get('order.artwork.max_types_required', 5);
        
        $max_types = $quantity > $config_max_types ? $config_max_types : $quantity ;
        
        // Login
        $this->ysi_login();
        
        // Init YSI object
        $ysi = \Yousendit\Base::forge();
        
        // Get info about Orders folder
        if(!$response = $ysi->folder_info())
        {
            die('Internal server error, please try again.');
        }
        
        echo \Theme::instance()->view('views/order/cart/upload', array(
            'product' => $product, 
            'cart_uid' => $cart_uid, 
            'quantity' => $quantity,
            'max_types' => $max_types), false); 
        //echo \Theme::instance()->view('views/base', array('content' => \Theme::instance()->view('views/order/cart/upload', array('product' => $product), false)), false);
        exit;
    }
    
    protected function ysi_login()
    {
        $ysi = \Yousendit\Base::forge();
        if($ysi->login())
        {
            return true;
        }
        else
        {
            return false;
            //var_dump($ysi->get_errors());
        }
    }
    
    public function action_ysi_logout()
    {
        $ysi = \Yousendit\Base::forge();
        $ysi->logout();
        $this->ysi = null;
    }
    
    public function action_init_upload()
    {
        $ysi = \Yousendit\Base::forge();
        $ysi->init_upload();
    }
    
    public function action_delete_artwork($file_id = null)
    {
        $ysi = \Yousendit\Base::forge();
        $ysi->delete_artwork($file_id);
        exit;
    }
    
    public function action_artwork_uploaded()
    {
        echo \Theme::instance()->view('views/order/cart/_artwork_uploaded', \Input::post(), false);
        exit;
    }
    
    public function action_commit_upload()
    {
       $ysi = \Yousendit\Base::forge();
       $ysi->commit_upload(\Session::get('ysi.folderId'));
    }
    
    public function action_add_artwork_type($product_id = null)
    {
        if(\Input::post())
        {
            if(!$product = \Product\Model_Product::find_one_by_id($product_id))
            {
                die('Internal server error, please try again.');
            }
            
            $quantity =\Input::post('quantity');
            
            $max_files = \Config::get('order.artwork.max_types_required', 5);
            $to = \Input::post('to', 1);
            $from = \Input::post('from', 1);
            
            if($to > $max_files) $to = $max_files;
            if($from < 1 || $from > $max_files) $from = 1;
            $content = '';
            $count = $to - $from;
            $max_quantity = $quantity - $count - 1;
            
            for ($from; $from<=$to; $from++)
            {
                $content .= \Theme::instance()->view('views/order/cart/_upload_artwork_type', array(
                    'num' => $from, 
                    'product' => $product, 
                    'max_quantity' => $max_quantity, 
                    'cart_uid' => \Input::post('cart_uid')));
            } 
            echo $content;
        }
        exit;
    }
    
    public function action_add_upload_item($product_id = null)
    {
        if(\Input::post())
        {
            if(!$product = \Product\Model_Product::find_one_by_id($product_id))
            {
                die('Internal server error, please try again.');
            }
            
            $max_files = \Config::get('order.artwork.max_number_of_files', 5);
            $to = \Input::post('to', 1);
            $from = \Input::post('from', 1);
            
            if($to > $max_files) $to = $max_files;
            if($from < 1 || $from > $max_files) $from = 1;
            $content = '';
            for ($from; $from<=$to; $from++)
            {
                $content .= \Theme::instance()->view('views/order/cart/_upload_item', array('num' => $from, 'product' => $product, 'cart_uid' => \Input::post('cart_uid')));
            } 
            echo $content;
        }
        exit;
    }
    
    public function action_create_order()
    {
        if(!\Input::is_ajax()) die();
        
         // We need order ID - save order if not exists
        $order_id = \Session::get('order.id', false);
        if(!is_numeric($order_id))
        {
            if(\Sentry::check())
            {
                $user = \Sentry::user();
                $order = \Order\Model_Order::forge(array(
                    'user_id' => $user->get('id'),
                    'finished' => 0
                ));
            }
            else
            {
                $order = \Order\Model_Order::forge(array(
                    'finished' => 0
                ));
            }
            $order->save();
            \Session::set('order.id', $order->id);
        }
        echo $order_id;
    }
    
    public function action_myproducts()
    {
       
        $products = \Product\Model_Product::filter_by_group();
        
                
        // Products
        $items = array();
        if($products)
        {
             $items = Model_Product::find(array(
                'where' => array(
                        array('id', 'in', array_keys($products['assigned'])),
                ),
            ), 'id');
        }
        
        $category = null;
        $parent_category = null;

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();

        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 15),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        $items = array_slice($items, $pagination->offset, $pagination->per_page);

        \Theme::instance()->set_partial('content', $this->view_dir . 'subcategory')
            ->set('page_title', 'My Products', false)
            ->set('category', $category, false)
            ->set('parent_category', $parent_category, false)
            ->set('items', $items, false)
            ->set('pagination', $pagination, false);
    }

    public function action_deals($slug = false, $deal_id =false)
    {
        $template = 'deals';
        if($slug)
        {
            if($product = Model_Product::get_by_slug($slug))
            {
                if($deal = Model_Deals::find_one_by_id($deal_id))
                {
                    $get_title = ($product->seo->meta_title ?: $product->title).': '.$deal->title;
                    \View::set_global('title', $get_title);
                    \View::set_global('meta_description', ($product->seo->meta_description ?: ''));
                    \View::set_global('meta_keywords', ($product->seo->meta_keywords ?: ''));
                    $robots = array('meta_robots_index' => ( $product->seo->meta_robots_index == 1 ? 'index' : 'noindex' ), 'meta_robots_follow' => ( $product->seo->meta_robots_follow == 1 ? 'follow' : 'nofollow' ) );
                    \View::set_global('robots', $robots);
                    \View::set_global('canonical', $product->seo->canonical_links);
                    \View::set_global('h1_tag', $product->seo->h1_tag);

                    $template = 'deals_single';
                    \Theme::instance()->set_partial('content', $this->view_dir . $template)
                        ->set('deal', $deal)
                        ->set('product', $product);
                }
                else
                {
                    throw new \HttpNotFoundException;
                }
            }
            else
            {
                throw new \HttpNotFoundException;
            }
        }
        else
        {
            \View::set_global('title', 'Deals');
            \View::set_global('meta_description', 'Deals');
            \View::set_global('meta_keywords', 'Deals');

            // get products with deals page enabled in admin

            $deals = Model_Deals::find(array(
                                            'order_by' => array('id' => 'desc'),
                                        ));

            \Theme::instance()->set_partial('content', $this->view_dir . $template)
                ->set('deals', $deals);
        }

    }
    
}