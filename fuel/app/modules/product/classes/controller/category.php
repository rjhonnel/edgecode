<?php

namespace Product;

class Controller_Category extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/';
	
	public $theme_view_path;
    
	public function before()
	{
		parent::before();
		
		\Config::load('product::category', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
		
		$this->theme_view_path = DOCROOT . \Theme::instance()->asset_path($this->view_dir);
	}
	
	/**
	 * Index page
	 * 
	 * @access  public
     * @param   $slug
	 */
	public function action_index($slug = false, $my_products = null)
	{
        if($my_products == 1) Model_Product::$filter_by_pricing_group = 'assigned';
        else Model_Product::$filter_by_pricing_group = 'not_assigned';
        $this->show($slug, $my_products);
    }
    
    protected function show($slug = false, $my_products = null)
    {
        $group_id = \Sentry::user()->group_id();

        if(isset($sorting)) Model_Category::$sorting = $sorting;
        
        if($category = Model_Category::get_by_slug($slug))
        {
            // TODO delete this
           
            srand($category->id);
            $c_category = \Product\Model_Category::find_by('id',$category->parent_id);
    
            if(!empty((array)json_decode($category->user_group))){
                
                if(!in_array($group_id,(array)json_decode($category->user_group))){
                    \Messages::error('Your not allowed to access this page.');
                     \Response::redirect('/');
                }
               
            } 
            else if($c_category){
                if(!empty((array)json_decode($c_category[0]->user_group))){
                    if(!in_array($group_id,(array)json_decode($c_category[0]->user_group))){
                    \Messages::error('Your not allowed to access this page.');
                     \Response::redirect('/');
                    }
                }
                
            } 
           
            $items = null;
            $parent_category = null;
            
            if($category->children)
            {
                $view_file = 'category';
                // Categories
                $items = Model_Category::find(array(
                    'where' => array(
                        'parent_id' => $category->id,
                        'status' => 1,
                        ),
                    'order_by' => array(
                        'sort' => 'asc'
                        )
                    ));

				if (!$items)
				{
					$view_file = 'subcategory';
					$items = $category->products;
				}
            }
            else
            {
                $view_file = 'subcategory';
                // Products
                $items = $category->products;
                

                // echo '<pre>';
                // print_r($items);
                // echo '</pre>';
                $parent_category = Model_Category::find_one_by_id($category->parent_id);
            }

            \Helper::sorting($items);
            
            // Reset to empty array if there are no result found by query
            if(is_null($items)) $items = array();

            // Initiate pagination
            $pagination = \Hybrid\Pagination::make(array(
                'total_items' => count($items),
                'per_page' => \Input::get('per_page', 15),
                'uri_segment' => null,
            ));

            // Remove unwanted items, and show only required ones
            $items = array_slice($items, $pagination->offset, $pagination->per_page);

            $category_parents = false;
            if($parent_category)
            {
                $parents = array();
                if($category_parents_id = Model_Category::find_parents($category->parent_id, $parents, true))
                {
                   $category_parents = Model_Category::find(array('where' => array(array('id', 'in', $category_parents_id))));
                }
            }

            \Theme::instance()->set_partial('content', $this->view_dir . $view_file)
                ->set('category', $category, false)
                ->set('parent_category', $parent_category, false)
                ->set('items', $items, false)
                ->set('category_parents', $category_parents, false)
                ->set('pagination', $pagination, false);

            \View::set_global('title', ($category->seo->meta_title ?: $category->title));
            \View::set_global('meta_description', ($category->seo->meta_description ?: ''));
            \View::set_global('meta_keywords', ($category->seo->meta_keywords ?: ''));
            $robots = array('meta_robots_index' => ( $category->seo->meta_robots_index == 1 ? 'index' : 'noindex' ), 'meta_robots_follow' => ( $category->seo->meta_robots_follow == 1 ? 'follow' : 'nofollow' ) );
            \View::set_global('robots', $robots);
            \View::set_global('canonical', $category->seo->canonical_links);
            \View::set_global('h1_tag', $category->seo->h1_tag);
            if($category->seo->redirect_301){
                \Response::redirect($category->seo->redirect_301);
            }
        }
        else
        {
            throw new \HttpNotFoundException;
        }
    }
    
}
