<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Product;

class Controller_Admin_Product extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('product::product', 'details');
		\Config::load('product::infotab', 'infotab');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/list');
	}
	
    public function get_search_items($category_id = false, $update_category = false)
    {
    	$has_cb = false;
        $category_product_ids = array();
        $brand_product_ids = array();

        $brand_id = \Input::get('brand_id', false);
        if($update_category == 'brand')
        	$brand_id = $category_id;

        if(is_numeric($brand_id))
        {
            $brand = \Product\Model_Brand::find_one_by_id($brand_id);
            if($brand)
            {
                \View::set_global('brand', $brand);
                if($brand->all_products)
                {
                    $brand_product_ids = array_keys($brand->all_products);
                }
            }
            $has_cb = true;
        }

        $category_id = \Input::get('category_id', false);
        if($update_category == 'category')
        	$category_id = $category_id;

        if(is_numeric($category_id))
        {
            $category = \Product\Model_Category::find_one_by_id($category_id);
            if($category)
            { 
                \View::set_global('category', $category);
                if($category->all_products)
                {
                    $category_product_ids = array_keys($category->all_products);
                }
            }
            $has_cb = true;
        }
        
        // If we are filtering products by category and there is nothing found
        // We return empty array of products without even going to database
        if($has_cb && (!$category_product_ids && !$brand_product_ids) )
        { 
            $items = array();
        }
        else
        {
	        /************ Start generating query ***********/
	        $items = Model_Product::find(function($query) use ($category_product_ids, $brand_product_ids){

	        	if(!empty($category_product_ids) && !empty($brand_product_ids))
	        	{
	                $query->and_where_open();
	                	$query->where('id', 'in', $category_product_ids);
	                	$query->and_where('id', 'in', $brand_product_ids);
	                $query->and_where_close();
	        	}
	        	else
	        	{
		            if(!empty($category_product_ids))
		            {
		                $query->where('id', 'in', $category_product_ids);
		            }

		            if(!empty($brand_product_ids))
		            {
		                $query->where('id', 'in', $brand_product_ids);
		            }
	        	}

	            // Get search filters
	            foreach(\Input::get() as $key => $value)
	            {
	                if(!empty($value) || $value == '0')
	                {
	                    switch($key)
	                    {
	                        case 'title':
	                            $query->where($key, 'like', "%$value%")
	                            	->or_where('code', 'like', "%$value%");
	                            break;
	                        case 'status':
	                            if(is_numeric($value))
	                                $query->where($key, $value);
	                            break;
	                        case 'active_from':
	                            $date = strtotime($value);
	                            if($date)
	                                $query->where($key, '>=', $date);
	                            break;
	                        case 'active_to':
	                            $date = strtotime($value);
	                            if($date)
	                                $query->where($key, '<=', $date);
	                            break;
	                    }
	                }
	            }

	            if(\Input::get('orderby') && \Input::get('order'))
	            {
		            $query->order_by(\Input::get('orderby'), \Input::get('order'));
	            }
	            else
	            {
		            // Order query
		            $query->order_by('sort', 'asc');
		            $query->order_by('id', 'asc');
	            }
	        });
		}
        
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
        
        return array('items' => $items, 'pagination' => $pagination, 'status' => $status);
    }
    
	public function action_list($category_id = false, $update_category = false)
	{

		if($action = \Input::get('bulk_actions')) {

			$products = \Input::get('product');

			if ($action == 'delete') {
				
				if ($products) {
					$updated = 0;
					foreach ($products as $product_id => $is_checked) {
						if ($is_checked == 1) {
							$this->action_delete($product_id, false);
							$updated += 1;
						}
					}

					if ( $updated != 0 ) {
						\Messages::success($updated .' item(s) successfully deleted.');	
					}
				}
			} else if ($action == 'active_enable') {

				if ($products) {
					$updated = 0;
					foreach ($products as $product_id => $is_checked) {
						if ($is_checked == 1) {
							$item = Model_Product::find_one_by_id($product_id);

							$item->set(array('status' => 1));
							if ($item->save()) {
								$updated += 1;	
							}
						}
					}

					if ( $updated != 0 ) {
						\Messages::success($updated .' item(s) successfully enabled.');	
					}
					
				}

				
			} else if ($action == 'active_disable') {

				if ($products) {
					$updated = 0;
					foreach ($products as $product_id => $is_checked) {
						if ($is_checked == 1) {
							$item = Model_Product::find_one_by_id($product_id);

							$item->set(array('status' => 0));
							if ($item->save()) {
								$updated += 1;
							}
						}
					}

					if ( $updated != 0 ) {
						\Messages::success($updated .' item(s) successfully disabled.');	
					}
				}
			}
			
		}

        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'List Products');
		\View::set_global('update_category', $update_category);
		
        $search = $this->get_search_items($category_id, $update_category);
        
        $items      = $search['items'];
        $pagination = $search['pagination'];
        $status     = $search['status'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false)
			->set('status', $status);
	}
	
	public function action_video()
	{
		$url = \Input::post('url', false);
		$return['response'] = false;
		
		if($url)
		{
			$youtube = \App\Youtube::forge();
			
			$video = $youtube->parse($url)->get();
			
			if(isset($video) && $video)
			{
				$return['response'] = $video;
				\Messages::success('Video found! Please check found data');
			}
			else
			{
				\Messages::error('Please check video URL and try again.');
			}
		}
		else
		{
			\Messages::info('Please insert video URL');
		}
		
		$return['message'] 	= \Messages::display('left', false);
		
		echo json_encode($return); exit;
	}
	
	public function action_create()
	{
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Add New');

		if(\Input::post())
		{
			$val = Model_Product::validate('create');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->image))
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload product image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload product image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}

			$is_valid_packs = true;
			$pack_errors = array();
			if(\Input::post('product_type') == 'pack')
			{
				$packs = \Input::post('packs');
				foreach ($packs as $key => $pack)
				{
					if($pack['name'] == '')
					{
						$is_valid_packs = false;
						array_push($pack_errors, 'Pack name is required.');
						break;
					}
				}
				foreach ($packs as $key => $pack)
				{
					if($pack['selection_limit'] == '' && $pack['type'] == 'included')
					{
						$is_valid_packs = false;
						array_push($pack_errors, 'Product selection limit is required.');
						break;
					}
				}
				foreach ($packs as $key => $pack)
				{
					if(!isset($pack['products']))
					{
						$is_valid_packs = false;
						array_push($pack_errors, 'Need to add products in pack options.');
						break;
					}
				}
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false)) && $is_valid_packs)
			{
				// Get POST values
				$insert = \Input::post();
				
				// Prepare some values
				// $insert['published_at'] = !empty($insert['published_at']) ? strtotime($insert['published_at']) : \Date::forge()->get_timestamp();
				$insert['active_from'] 	= !empty($insert['active_from']) ? \Helper::dmyToymd($insert['active_from'], '/') : NULL;
				$insert['active_to'] 	= !empty($insert['active_to']) ? \Helper::dmyToymd($insert['active_to'], '/') : NULL;
				if($insert['status'] != 2)
				{
					unset($insert['active_from']);
					unset($insert['active_to']);
				}
				
				$item = Model_Product::forge($insert);

				$hold_pack_ids = array();
				
				try{
					$item->save();
					
					// Validate and insert product slug into SEO database table
					$val_seo = Model_Product_Seo::validate('create_seo');
					$insert_seo = array(
						'content_id' 	=> $item->id,
						'slug'			=> \Inflector::friendly_title($item->title, '-', true),
					);
					
					while(!$val_seo->run($insert_seo))
					{
						$insert_seo['slug'] = \Str::increment($insert_seo['slug'], 1, '-');
					}
					
					$item_seo = Model_Product_Seo::forge($insert_seo);
					$item_seo->save();
					// END OF: SEO
					
					// Insert product categories
					foreach($insert['cat_ids'] as $parent_category)
					{
						$item_categories = Model_Product_To_Categories::forge(array(
								'product_id' 	=> $item->id,
								'category_id'	=> $parent_category,
						));
						$item_categories->save();
					}

					// Insert product brands
					foreach($insert['brand_ids'] as $parent_category)
					{
						$item_brands = Model_Product_To_Brands::forge(array(
								'product_id' 	=> $item->id,
								'brand_id'	=> $parent_category,
						));
						$item_brands->save();
					}

                    /**  PROPERTY GROUPS  **/
                    // First delete old groups
					$tmp_categories = \Product\Model_Product_To_Groups::find_by_product_id($item->id);
					if ($tmp_categories)
						foreach($tmp_categories as $tmp_cat) $tmp_cat->delete();
                    
                    // Insert product property groups
                    if (isset($insert['group_ids']))
                    {
						foreach($insert['group_ids'] as $parent_group)
						{
							$related = \Product\Model_Product_To_Groups::forge(array(
	                            'group_id' 		=> $parent_group,
	                            'product_id' 	=> $item->id,
	                        ));
	                        $related->save();
						}	
                    }
                    /**  END OF: PROPERTY GROUPS  **/
                    
                    /**  PRICING GROUPS  **/
                    // Insert product pricing group
                    if(\Input::post('group', false))
                    {
                        $pricing = \Product\Model_Product_To_Groups::forge(array(
                            'group_id' 		=> \Input::post('group'),
                            'product_id' 	=> $item->id,
                        ));
                        $pricing->save();
                    }
                    /**  END OF: PRICING GROUPS  **/

                    /**  PRODUCT PACKS  **/
                    // Save product packs
					$packs = \Input::post('packs');
					foreach ($packs as $key => $pack)
					{
                        $pack_data = \Product\Model_Packs::forge(array(
							'product_id' => $item->id,
							'name' => $pack['name'],
							'description' => $pack['description'],
							'type' => $pack['type'],
							'selection_limit' => $pack['selection_limit'],
							'products' => implode(',', $pack['products']),
						));
                        $pack_data->save();

                        $hold_pack_ids['packs:'.$key.':image'] = $pack_data->id;
					}
                    /**  END OF: PRODUCT PACKS  **/
                    
					// Insert product images
					if($this->_image_data)
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);

						foreach ($this->_image_data as $key => $_image_data)
						{
							chmod($_image_data['saved_to'].$_image_data['saved_as'], 0755);
							chmod($_image_data['saved_to'].'large/'.$_image_data['saved_as'], 0755);
							chmod($_image_data['saved_to'].'medium/'.$_image_data['saved_as'], 0755);
							chmod($_image_data['saved_to'].'thumbs/'.$_image_data['saved_as'], 0755);

							//save to amazon
							if($hold_amazon_enable)
							{
								$folder_list = array('', 'large/', 'medium/', 'thumbs/');
								foreach ($folder_list as $folder)
								{
			                        $file_info = array(
			                                'savePath' => 'media/images/'.$folder.$_image_data['saved_as'],
			                                'filePath' => $_image_data['saved_to'].$folder.$_image_data['saved_as'],
			                                'contentType' => mime_content_type($_image_data['saved_to'].$folder.$_image_data['saved_as']),
			                            );
									\Helper::uploadFileToAmazonS3($file_info, $settings);
									@unlink($_image_data['saved_to'].$folder.$_image_data['saved_as']);
								}
							}
							// EOL - save to amazon
							if(isset($_image_data['saved_to']) && (strpos($_image_data['field'], 'packs') !== false)) // check if save to is set and file is for pack
							{								
								$pack_image = array(
									array(
										'id'	=> 0,
										'data'	=> array(
											'content_id'	=> $hold_pack_ids[$_image_data['field']], // must be pack id
											'image'			=> $_image_data['saved_as'],
											'alt_text'		=> '',
											'cover'			=> 0,
											'sort'			=> 1,
										),
									),
								);
								Model_Packs::bind_images($pack_image);
							}
							else if(isset($_image_data['saved_to']))
							{							
								$item_image = array(
									array(
										'id'	=> 0,
										'data'	=> array(
											'content_id'	=> $item->id,
											'image'			=> $_image_data['saved_as'],
											'alt_text'		=> \Input::post('alt_text', ''),
											'cover'			=> 1,
											'sort'			=> 1,
										),
									),
								);
								
								Model_Product::bind_images($item_image);
							}
						}
					}
					\Messages::success('Product successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/product/update/' . $item->id) : \Uri::create('admin/product/create/' . $item_categories->category_id));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create product</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create product</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
				if($pack_errors)
				{
					foreach ($pack_errors as $errors)
					{
						\Messages::error($errors);
					}
				}
			}
			
			// Delete uploaded image if there is product saving error
			if(isset($this->_image_data))
			{
				$this->delete_image($this->_image_data[0]['saved_as']);
			}
		}
		
		$suppliers = \SentryAdmin::user()->all('supplier');

		$suppliers_list = array(0 => 'Select Supplier');
        foreach ($suppliers as $supplier) {
        	$meta_data = \SentryAdmin::user((int)$supplier['id'])['metadata'];
            $suppliers_list[$supplier['id']] = $meta_data['first_name'].' '.$meta_data['last_name'];
        }

		/** Get all products **/
		$products = \Product\Model_Product::find(function($query){
			$table_name = 'product';
			$query->where(function($query) use ($table_name){
	            $query->where($table_name . '.status', 1);
	            $query->or_where(function($query) use ($table_name){
	                $query->where($table_name . '.status', 2);
	                $query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
	                $query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
	            });
	        });
			$query->order_by('id', 'asc');
		});

		$product_list = array('' => 'Please select product');
		if($products)
		{
			foreach ($products as $product)
			{
				$product_list += array($product->id => $product->title);
			}
		}
		/** END: Get all products **/

		\Theme::instance()->set_partial('content', $this->view_dir . 'create')
                ->set('product_list', $product_list)
                ->set('suppliers', $suppliers_list);
	}

	private function check_code_exist($product_id, $code) {


		$flag = Model_Product::find(function($query) use($product_id, $code) {
			$query->where('id', '!=', $product_id);
			$query->and_where('code', '=', $code);
		});


		if ($flag) {
			return true;
		}

		return false;
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/list');
		
		// Get news item to edit
		if(!$item = Model_Product::find_one_by_id($id)) \Response::redirect('admin/product/list');
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Edit Product');
		
		if(\Input::post())
		{
			if(\Input::post('file_upload', false))
			{
				// Upload files and display errors if there are any
				$file = $this->upload_file();
				if(!$file['exists'] && \Config::get('details.image.required', false) && empty($item->images))
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload content file</strong>');
					\Messages::error('You have to select image');
				}
				elseif($file['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload content file</strong>');
					foreach($file['errors'] as $error) \Messages::error($error);
				}
				
				if($file['is_valid'] && !(!$file['exists'] && \Config::get('details.file.required', false) && empty($item->files)))
				{
					/** FILES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'title_') as $file_id => $title)
					{
						if(strpos($file_id, 'new_') === false)
						{
							$item_files[$file_id] = array(
								'id'	=> $file_id,
								'data'	=> array(
									'title'		=> \Input::post('title_' . $file_id, ''),
								),
							);
						}
					}
					
					// Save files if new ones are submitted
					if(isset($this->_file_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_file_data as $file_data)
						{
							$cover_count = count($item->files);
							if(strpos($file_data['field'], 'new_') === false)
							{
								// Update existing file
								if(str_replace('file_', '', $file_data['field']) != 0)
								{
									$file_id = (int)str_replace('file_', '', $file_data['field']);
									$cover_count--;
									
									$item_files[$file_id] = array(
										'id'	=> $file_id,
										'data'	=> array(
											'content_id'	=> $item->id,
											'file'			=> $file_data['saved_as'],
											'title'			=> \Input::post('title_' . $file_id, ''),
										),
									);
									
									$this->delete_file(\Input::post('file_db_' . $file_id, ''));
								}
							}
							else
							{
								// Save new file
								$file_tmp = str_replace('file_new_', '', $file_data['field']);
								
								$item_files[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'content_id'	=> $item->id,
										'file'			=> $file_data['saved_as'],
										'title'			=> \Input::post('title_new_' . $file_tmp, ''),
										'cover'			=> $cover_count == 0 ? 1 : 0,
										'sort'			=> $cover_count + 1,
									),
								);
							}

							//save to amazon
							if($hold_amazon_enable)
							{
			                    $file_info = array(
			                            'savePath' => 'media/files/'.$file_data['saved_as'],
			                            'filePath' => $file_data['saved_to'].$file_data['saved_as'],
			                            'contentType' => mime_content_type($file_data['saved_to'].$file_data['saved_as']),
			                        );
								\Helper::uploadFileToAmazonS3($file_info, $settings);
								@unlink($file_data['saved_to'].$file_data['saved_as']);
							}
							// EOL - save to amazon
						}
					}
					
					Model_Product::bind_files($item_files);
					
					\Messages::success('Product documents successfully updated.');
					\Response::redirect(\Uri::admin('current'));
					/** END OF FILES **/
				}
				else
				{
					// Delete uploaded files if there is product saving error
					if(isset($this->_file_data))
					{
						foreach($this->_file_data as $file_data)
						{
							$this->delete_file($file_data['saved_as']);
						}
					}
							
					\Messages::error('<strong>There was an error while trying to update product documents</strong>');
				}
			}
						
			else if(\Input::post('video_upload', false))
			{
				// Upload image and display errors if there are any
				$image = $this->upload_image('video');
				if($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload content image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if($image['is_valid'] && !(\Config::get('details.video.required', false) && empty($item->videos)))
				{
					/** VIDEOS **/
					// Get all video urls to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'video_url_') as $video_id => $url)
					{
						$not_updated[] = $video_id;
						
						if(strpos($video_id, 'new_') === false)
						{
							$video_url = \Input::post('video_url_' . $video_id, false);
							
							if($video_url)
							{
								// Check video
								$youtube = \App\Youtube::forge();
								$video = $youtube->parse(\Input::post('video_url_' . $video_id, ''))->get();
								
								if($video)
								{
									// Update existing videos
									$item_videos[$video_id] = array(
										'id'	=> $video_id,
										'data'	=> array(
											'content_id'	=> $item->id,
											'url'			=> \Input::post('video_url_' . $video_id, ''),
											'title'			=> \Input::post('video_title_' . $video_id, ''),
										),
									);
									
									// Remove video ID as it is updated correctly
									array_pop($not_updated);
									
									// Delete image and use default one
									if(\Input::post('video_delete_image_' . $video_id, false))
									{
										$item_videos[$video_id]['data']['thumbnail'] = '';
										$this->delete_image(\Input::post('video_file_db_' . $video_id, ''), 'video');
									}
								}
								else
								{
									\Messages::error('"' . \Input::post('video_url_' . $video_id, '') . '" is invalid video URL. Video not updated.');
								}
							}
							else
							{
								\Messages::error('Video URL can\'t be empty. Video not updated.');
							}
						}
						else
						{
							$video_url = \Input::post('video_url_' . $video_id, false);
							
							if($video_url)
							{
								// Check video
								$youtube = \App\Youtube::forge();
								$video = $youtube->parse(\Input::post('video_url_' . $video_id, ''))->get();
								
								if($video)
								{
									// Add new videos
									$item_videos[0] = array(
										'id'	=> 0,
										'data'	=> array(
											'content_id'	=> $item->id,
											'url'			=> \Input::post('video_url_' . $video_id, ''),
											'title'			=> \Input::post('video_title_' . $video_id, ''),
											'sort'			=> count($item->videos) + 1,
										),
									);
									
									// Remove video ID as it is updated correctly
									array_pop($not_updated);
								}
								else
								{
									\Messages::error('"' . \Input::post('video_url_' . $video_id, '') . '" is invalid video URL. Video not updated.');
								}
							}
						}
					}
					
					// Save images if new files are submitted
					if(isset($this->_image_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_image_data as $image_data)
						{
							if(!in_array(str_replace('video_file_', '', $image_data['field']), $not_updated))
							{
								if(strpos($image_data['field'], 'new_') === false)
								{
									// Update existing image
									if(str_replace('video_file_', '', $image_data['field']) != 0)
									{
										$video_id = (int)str_replace('video_file_', '', $image_data['field']);
										
										$item_videos[$video_id]['data']['thumbnail'] = $image_data['saved_as'];
										
										$this->delete_image(\Input::post('video_file_db_' . $video_id, ''), 'video');
									}
								}
								else
								{
									// Save new image
									$image_tmp = str_replace('video_file_new_', '', $image_data['field']);
									
									$item_videos[0]['data']['thumbnail'] = $image_data['saved_as'];
								}

								//save to amazon
								if($hold_amazon_enable)
								{
									$folder_list = array('', 'thumbs/');
									foreach ($folder_list as $folder)
									{
				                        $file_info = array(
				                                'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
				                                'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
				                                'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
				                            );
										\Helper::uploadFileToAmazonS3($file_info, $settings);
										@unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
									}
								}
								// EOL - save to amazon
							}
							else
							{
								// That video was not updated so we remove its image if there is one
								$this->delete_image($image_data['saved_as'], 'video');
							}
						}
					}
					Model_Product::bind_videos($item_videos);
					
					\Messages::success('Product videos successfully updated.');
					\Response::redirect(\Uri::admin('current'));
					/** END OF VIDEOS **/
				}
				else
				{
					// Delete uploaded images if there is product saving error
					if(isset($this->_image_data))
					{
						foreach($this->_image_data as $image_data)
						{
							$this->delete_image($image_data['saved_as'], 'video');
						}
					}
							
					\Messages::error('<strong>There was an error while trying to update product videos</strong>');
				}
			}
			else{
				$val = Model_Product::validate('update');
				$item_images = array();
				$hold_pack_ids = array();
				$hold_pack_image_ids = array();
				$hold_pack_image_names = array();
				
				// Upload image and display errors if there are any
				$image = $this->upload_image();
				if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload content image</strong>');
					\Messages::error('You have to select image');
				}
				elseif($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload content image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}

				$is_valid_packs = true;
				$pack_errors = array();
				if(\Input::post('product_type') == 'pack')
				{
					$packs = \Input::post('packs');
					foreach ($packs as $key => $pack)
					{
						if($pack['name'] == '')
						{
							$is_valid_packs = false;
							array_push($pack_errors, 'Pack name is required.');
							break;
						}
					}
					foreach ($packs as $key => $pack)
					{
						if($pack['selection_limit'] == '' && $pack['type'] == 'included')
						{
							$is_valid_packs = false;
							array_push($pack_errors, 'Product selection limit is required.');
							break;
						}
					}
					foreach ($packs as $key => $pack)
					{
						if(!isset($pack['products']))
						{
							$is_valid_packs = false;
							array_push($pack_errors, 'Need to add products in pack options.');
							break;
						}
					}
				}
				
				if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)) && $is_valid_packs)
				{
                    /**  PRODUCT PACKS  **/
                    // Save product packs
					$packs = \Input::post('packs');
					foreach ($packs as $key => $pack)
					{
						$hold_pack_id = isset($pack['pack_id'])?$pack['pack_id']:-1;
						$hold_pack_image_id = isset($pack['pack_image_id'])?$pack['pack_image_id']:0;
						$hold_pack_image_name = isset($pack['pack_image_name'])?$pack['pack_image_name']:'';
						if($pack_data = \Product\Model_Packs::find_one_by_id($hold_pack_id))
						{
							$pack_data->set(array(
								'product_id' => $item->id,
								'name' => $pack['name'],
								'description' => $pack['description'],
								'type' => $pack['type'],
								'selection_limit' => $pack['selection_limit'],
								'products' => implode(',', $pack['products']),
							));
						}
						else
						{
	                        $pack_data = \Product\Model_Packs::forge(array(
								'product_id' => $item->id,
								'name' => $pack['name'],
								'description' => $pack['description'],
								'type' => $pack['type'],
								'selection_limit' => $pack['selection_limit'],
								'products' => implode(',', $pack['products']),
							));
						}
                        $pack_data->save();

                        $hold_pack_ids['packs:'.$key.':image'] = $pack_data->id;
                        $hold_pack_image_ids['packs:'.$key.':image'] = $hold_pack_image_id;
                        $hold_pack_image_names['packs:'.$key.':image'] = $hold_pack_image_name;
					}
                    /**  END OF: PRODUCT PACKS  **/

					/** IMAGES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
					{
						if(strpos($image_id, 'new_') === false)
						{
							$cover_image = 0;
							if(\Input::post('cover_image'))
							{
								if(\Input::post('cover_image') == $image_id)
								$cover_image = 1;
							}
							$item_images[$image_id] = array(
								'id'	=> $image_id,
								'data'	=> array(
									'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
									'cover'			=> $cover_image,
								),
							);
						}
					}
					
					// Save images if new files are submitted
					if(isset($this->_image_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_image_data as $image_data)
						{
							chmod($image_data['saved_to'].$image_data['saved_as'], 0755);
							chmod($image_data['saved_to'].'large/'.$image_data['saved_as'], 0755);
							chmod($image_data['saved_to'].'medium/'.$image_data['saved_as'], 0755);
							chmod($image_data['saved_to'].'thumbs/'.$image_data['saved_as'], 0755);

							$cover_count = count($item->images);
							if(strpos($image_data['field'], 'new_') === false)
							{

								if(isset($image_data['saved_to']) && (strpos($image_data['field'], 'packs') !== false)) // check if save to is set and file is for pack
								{
									$pack_image = array(
										array(
											'id'	=> $hold_pack_image_ids[$image_data['field']],
											'data'	=> array(
												'content_id'	=> $hold_pack_ids[$image_data['field']], // must be pack id
												'image'			=> $image_data['saved_as'],
												'alt_text'		=> '',
												'cover'			=> 0,
												'sort'			=> 1,
											),
										),
									);
									Model_Packs::bind_images($pack_image);
									
									if($hold_pack_image_ids[$image_data['field']])
										$this->delete_image($hold_pack_image_names[$image_data['field']]);
								}
								else
								{
									// Update existing image
									if(str_replace('image_', '', $image_data['field']) != 0)
									{
										$image_id = (int)str_replace('image_', '', $image_data['field']);
										$cover_count--;
										$cover_image = 0;
										if(\Input::post('cover_image'))
										{
											if(\Input::post('cover_image') == $image_id)
											$cover_image = 1;
										}
										
										$item_images[$image_id] = array(
											'id'	=> $image_id,
											'data'	=> array(
												'content_id'	=> $item->id,
												'image'			=> $image_data['saved_as'],
												'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
												'cover'			=> $cover_image,
											),
										);
										
										$this->delete_image(\Input::post('image_db_' . $image_id, ''));
									}
								}
							}
							else
							{
								// Save new image
								$image_tmp = str_replace('image_new_', '', $image_data['field']);
								$cover_image = 0;
								$item_images[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'content_id'	=> $item->id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
										'cover'			=> $cover_image,
										'sort'			=> $cover_count + 1,
									),
								);
							}

							//save to amazon
							if($hold_amazon_enable)
							{
								$folder_list = array('', 'large/', 'medium/', 'thumbs/');
								foreach ($folder_list as $folder)
								{
			                        $file_info = array(
			                                'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
			                                'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
			                                'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
			                            );
									\Helper::uploadFileToAmazonS3($file_info, $settings);
									@unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
								}
							}
							// EOL - save to amazon
						}
					}
					Model_Product::bind_images($item_images);
					/** END OF IMAGES **/
					
					// Get POST values
					$insert = \Input::post();



					// if featured is not set, set to 0
					if(!isset($insert['featured']))
						$insert['featured'] = 0;
					
					// Prepare some values
					//	$insert['published_at'] = !empty($insert['published_at']) ? strtotime($insert['published_at']) : $news->created_at;
					$insert['active_from'] 	= !empty($insert['active_from']) ? \Helper::dmyToymd($insert['active_from'], '/') : NULL;
					$insert['active_to'] 	= !empty($insert['active_to']) ? \Helper::dmyToymd($insert['active_to'], '/') : NULL;
					if($insert['status'] != 2)
					{
						unset($insert['active_from']);
						unset($insert['active_to']);
					}
					

					// Check if product code is not exists
					if (! $this->check_code_exist($item->id, $insert['code'])) {

						$item->set($insert);
					
						try{
							$item->save();
							
							// First delete old categories
							$tmp_categories = Model_Product_To_Categories::find_by_product_id($item->id);
							foreach($tmp_categories as $tmp_cat) $tmp_cat->delete();
						
							foreach($insert['cat_ids'] as $parent_category)
							{
								$item_categories = Model_Product_To_Categories::forge(array(
										'product_id' 	=> $item->id,
										'category_id'	=> $parent_category,
								));
								$item_categories->save();
							}

							// First delete old brands
							$tmp_brands = Model_Product_To_Brands::find_by_product_id($item->id);
							foreach($tmp_brands as $tmp_cat) $tmp_cat->delete();

							foreach($insert['brand_ids'] as $parent_category)
							{
								$item_categories = Model_Product_To_Brands::forge(array(
										'product_id' 	=> $item->id,
										'brand_id'	=> $parent_category,
								));
								$item_categories->save();
							}

		                    /**  PROPERTY GROUPS  **/
		                    // First delete old groups
							$tmp_categories = \Product\Model_Product_To_Groups::find_by_product_id($item->id);
							if ($tmp_categories)
								foreach($tmp_categories as $tmp_cat) $tmp_cat->delete();
		                    
		                    // Insert product property groups
		                    $a_group_ids = isset($insert['group_ids']) ? $insert['group_ids'] : array();
							foreach($a_group_ids as $parent_group)
							{
								$related = \Product\Model_Product_To_Groups::forge(array(
		                            'group_id' 		=> $parent_group,
		                            'product_id' 	=> $item->id,
		                        ));
		                        $related->save();
							}
		                    /**  END OF: PROPERTY GROUPS  **/
		                    
		                    /**  PRICING GROUPS  **/
		                    // Insert product pricing group
		                    if(\Input::post('group', false))
		                    {
		                        $pricing = \Product\Model_Product_To_Groups::forge(array(
		                            'group_id' 		=> \Input::post('group'),
		                            'product_id' 	=> $item->id,
		                        ));
		                        $pricing->save();
		                    }
		                    /**  END OF: PRICING GROUPS  **/
							
							\Messages::success('Product successfully updated.');
							\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/list/') : \Uri::admin('current'));
						}
						catch (\Database_Exception $e)
						{
							// show validation errors
							\Messages::error('<strong>There was an error while trying to update product</strong>');
						    
							// Uncomment lines below to show database errors
							//$errors = $e->getMessage();
					    	//\Messages::error($errors);
						}
					} else {
						\Messages::error('<strong>Product code already exists</strong>');
					
					}
				}
				else
				{
					// Delete uploaded images if there is product saving error
					if(isset($this->_image_data))
					{
						foreach($this->_image_data as $image_data)
						{
							$this->delete_image($image_data['saved_as']);
						}
					}
							
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update product</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
					if($pack_errors)
					{
						foreach ($pack_errors as $errors)
						{
							\Messages::error($errors);
						}
					}
				}
			}
			
		}
		$product = Model_Product::find_one_by_id($id);
		
		
		$suppliers = \SentryAdmin::user()->all('supplier');

		$suppliers_list = array(0 => 'Select Supplier');
        foreach ($suppliers as $supplier) {
        	$meta_data = \SentryAdmin::user((int)$supplier['id'])['metadata'];
            $suppliers_list[$supplier['id']] = $meta_data['first_name'].' '.$meta_data['last_name'];
        }

		/** Get all products expcept itself **/
		$products = \Product\Model_Product::find(function($query) use ($id){
			$table_name = 'product';
			$query->where(function($query) use ($table_name, $id){
	            $query->where($table_name . '.id','<>', $id);
	            $query->where($table_name . '.status', 1);
	            $query->or_where(function($query) use ($table_name){
	                $query->where($table_name . '.status', 2);
	                $query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
	                $query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
	            });
	        });
			$query->order_by('id', 'asc');
		});

		$product_list = array('' => 'Please select product');
		if($products)
		{
			foreach ($products as $hproduct)
			{
				$product_list += array($hproduct->id => $hproduct->title);
			}
		}
		/** END: Get all products expcept itself **/

		/** Get all packs under this product **/
		$pack_list = array();
		$get_packs = \Product\Model_Packs::find(function($query) use ($id) {
			$query->where('product_id', $id);
			$query->order_by('id', 'asc');
		});
		if($get_packs)
		{
			foreach ($get_packs as $pack)
			{
				$get_image_id = '';
				$get_image = '';
				if($pack->image)
				{
					$get_image_id = $pack->image->id;
					$get_image = $pack->image->image;
				}

				array_push($pack_list, array(
					'id' => $pack->id,
					'name' => $pack->name,
					'description' => $pack->description,
					'type' => $pack->type,
					'selection_limit' => $pack->selection_limit,
					'products' => explode(',', $pack->products),
					'image_id' => $get_image_id,
					'image' => $get_image,
				));
			}
		}
		/** END: Get all packs under this product **/
	
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('product', $product)
                ->set('product_list', $product_list)
                ->set('pack_list', $pack_list)
                ->set('suppliers', $suppliers_list);
	}
	
	/**
	 * Update product SEO options
	 * 
	 * @param $id	= Product ID
	 */
	public function action_update_seo($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/list');
		
		// Get news item to edit
		if(!$item = Model_Product::find_one_by_id($id)) \Response::redirect('admin/product/list');
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Edit Meta Content');
		
		if(\Input::post())
		{
			$val = Model_Product_Seo::validate('update', $item->id);
			
			// Get POST values
			$insert = \Input::post();
			
			if($val->run($insert))
			{
				// Update SEO database table
				$item_seo = Model_Product_Seo::find_one_by_content_id($item->id);
				
				if ($item_seo)
				{
					$item_seo->set($insert);
				}
				else
				{
					$item_seo = Model_Category_Seo::forge($insert);
				}
				
				try{
					$item_seo->save();
					
					\Messages::success('Meta content successfully updated.');
					\Response::redirect(\Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update meta content</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update meta content</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update_seo')
			->set('product', Model_Product::find_one_by_id($id));
	}
	
	public function action_delete($id = false, $redirect = true)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Product::find_one_by_id($id))
			{
				$product_id = $item->id;

				// Delete deals
				if(!empty($item->deals))
				{
					foreach ($item->deals as $deal)
					{
						$deal->delete();
					}
				}
                
				// Delete other content data like images, files, etc.
				if(!empty($item->infotabs))
				{
					foreach($item->infotabs as $infotab)
					{
						// Delete product infotab with all its data
						$this->action_infotab_delete($infotab->unique_id);
					}
				}
				
				if(!empty($item->images))
				{
					foreach($item->images as $image)
					{
						$this->delete_image($image->image);
						$image->delete();
					}
				}
				
				$categories = Model_Product_To_Categories::find_by_product_id($item->id);
				if(!empty($categories))
				{
					foreach($categories as $category)
					{
						$category->delete();
					}
				}
				
				
				if(!empty($item->files))
				{
					foreach($item->files as $file)
					{
						$this->delete_file($file->file);
						$file->delete();
					}
				}
				
				if(!empty($item->videos))
				{
					foreach($item->videos as $video)
					{
						$this->delete_image($video->thumbnail, 'video');
						$video->delete();
					}
				}

				$brands = Model_Product_To_Brands::find_by_product_id($item->id);
				if(!empty($brands))
				{
					foreach($brands as $brand)
					{
						$brand->delete();
					}
				}

                $this->delete_product_attribute($id);
					
				if (!empty($item->seo))
				{
					$item->seo->delete();	
				}

				// delete packs
				$packs = \Product\Model_Packs::find_by_product_id($item->id);
				if($packs)
				{
					foreach($packs as $pack)
					{
						$pack->delete();
					}
				}
				
				// Delete item
				try{
					$item->delete();
					
					\Messages::success('Product successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete product</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		if ($redirect) {
			\Response::redirect(\Input::referrer());
		}
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
			
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Product::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						// 'cover'	=> ($key == 0 ? 1 : 0),
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
	
	/****************************** CONTENT VIDEOS ******************************/
	
	/**
	 * Delete content file
	 * 
	 * @param $video_id		= Video ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_video($video_id = false, $content_id = false)
	{
		if($video_id && $content_id)
		{
			$videos = Model_Product_Video::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($videos)
			{
				if(isset($videos[$video_id]))
				{
					$video = $videos[$video_id];
					
					// If there is only one video and video is required
					if(count($videos) == 1)
					{
						if(\Config::get('details.video.required', false))
						{
							\Messages::error('You can\'t delete all videos. Please add new video in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Product_Video::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $video->sort)->execute();
							// Delete video
							$this->delete_image($video->thumbnail, 'video');
							$video->delete();
							\Messages::success('Video was successfully deleted.');
						}
					}
					else
					{
						// Reset sort fields
						\DB::update(Model_Product_Video::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $video->sort)->execute();
						// Delete video
						$this->delete_image($video->thumbnail, 'video');
						$video->delete();
						
						\Messages::success('Video was successfully deleted.');
					}
				}
				else
				{
					\Messages::error('Video you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Video you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/****************************** INFOTAB IMAGES ******************************/
	
	/**
	 * Upload all infotab images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_infotab_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->infotab_image_upload_config = array(
		    'path' => \Config::get('infotab.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		    'max_size' => 500000,
		);
		
		\Upload::process($this->infotab_image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save image
		    \Upload::save();
			$this->_infotab_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_infotab_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('infotab.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('infotab.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 * @param $type			= Type of content (infotab or hotspot)
	 * @param $delete		= If type is "hotspot" we can either delete image or video
	 */
	public function action_delete_infotab_image($image_id = false, $content_id = false, $type = 'infotab', $delete = 'image')
	{
		if($image_id && $content_id)
		{
			$images = Model_Infotab_Image::find(array(
			    'where' => array(
			        'infotab_id' => $content_id,
			        'item_type' => 'product'
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
					
					if(strtolower($type) == 'hotspot')
					{
						// Delete only part of hotspot, either image or video
						if($delete == 'image')
						{
							// Delete only image but not whole hotspot
							$this->delete_infotab_image($image->image);
							
            				$image->set(array(
            					'image' => '',
            					'alt_text' => '',
            				));
						}
						else
						{
							// Delete only video but not whole hotspot
            				$image->set(array(
            					'video' => '',
            					'video_title' => '',
            				));
						}
						
						$image->save();
						
						\Messages::success(ucfirst($type) . ' ' . strtolower($delete) . ' was successfully deleted.');
					}
					else
					{
						// If there is only one image and image is required
						if(count($images) == 1)
						{
							if(\Config::get('infotab.image.required', false) && !\Request::is_hmvc())
							{
								\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
							}
							else
							{
								// Reset sort fields
								\DB::update(Model_Infotab_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
								// Delete image
								$this->delete_infotab_image($image->image);
								$image->delete();
								\Messages::success(ucfirst($type) . ' image was successfully deleted.');
							}
						}
						else
						{
							if($image->cover == 1 && !\Request::is_hmvc())
							{
								\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
							}
							else
							{
								// Reset sort fields
								\DB::update(Model_Infotab_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
								// Delete image
								$this->delete_infotab_image($image->image);
								$image->delete();
								\Messages::success(ucfirst($type) . ' image was successfully deleted.');
							}
						}
					}
				}
				else
				{
					\Messages::error(ucfirst($type) . ' image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error(ucfirst($type) . ' image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		if(\Input::is_ajax())
		{
			\Messages::reset();
			\Messages::success('Hotspot was successfully deleted.');
			echo \Messages::display();
		}
		else if(\Request::is_hmvc())
		{
			\Messages::reset();
		}
		else
		{
			\Response::redirect(\Input::referrer());
		}
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_infotab_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			foreach(\Config::get('infotab.' . $content_type . '.location.folders', array()) as $folder)
			{
				//delete from amazon
				if($hold_amazon_enable)
				{
					if($content_type == 'image')
					{
						\Helper::deleteFileFromAmazonS3('media/images/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/thumbs/' . $name, $settings);
					}
				}
				// EOL - delete from amazon

				@unlink($folder . $name);
			}
		}
	}
	
	/****************************** INFOTAB FILES ******************************/
	
	/**
	 * Upload all contet files to local directory defined in $this->file_upload_config
	 * 
	 */
	public function upload_infotab_file()
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// File upload configuration
		$this->file_upload_config = array(
		    'path' => \Config::get('details.file.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('pdf', 'jpg'),
		);
		
		\Upload::process($this->file_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save file
		    \Upload::save();
			$this->_infotab_file_data = \Upload::get_files();
			
			return $return;
		}
		else
		{
			// FILE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, file is not uploaded
		return $return;
	}
	
	/**
	 * Delete content file
	 * 
	 * @param $file_id		= File ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_infotab_file($file_id = false, $content_id = false)
	{
		if($file_id && $content_id)
		{
			$files = Model_Infotab_File::find(array(
			    'where' => array(
			        'infotab_id' => $content_id,
			        'item_type' => 'product'
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($files)
			{
				if(isset($files[$file_id]))
				{
					$file = $files[$file_id];
					
					// If there is only one image and image is required
					if(count($files) == 1)
					{
						if(\Config::get('details.file.required', false))
						{
							\Messages::error('You can\'t delete all files. Please add new file in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Infotab_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							\Messages::success('File was successfully deleted.');
						}
					}
					else
					{
						// Dont use cover option for files
						if(FALSE && $file->cover == 1)
						{
							\Messages::error('You can\'t delete cover file. Set different file as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Infotab_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							
							// Set another file as cover if cover is deleted
							if($file->cover == 1)
							{
								$files = Model_Infotab_File::find(array(
								    'where' => array(
								        'infotab_id' => $content_id,
								        'item_type' => 'product'
								    ),
								    'order_by' => array('sort' => 'asc'),
								));
								
								$files[0]->cover = 1;
								$files[0]->save();
							}
							
							
							\Messages::success('File was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete file from file system
	 * 
	 * @param $name = File name
	 */
	public function delete_infotab_file($name = false)
	{
		if($name && \Config::get('details.file.location.root', false))
		{
			//delete from amazon
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			if($hold_amazon_enable)
			{
				\Helper::deleteFileFromAmazonS3('media/files/' . $name, $settings);
			}
			// EOL - delete from amazon

			@unlink(\Config::get('details.file.location.root') . $name);
		}
	}
	
	/****************************** CONTENT IMAGES ******************************/
	
	/**
	 * Upload all contet images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '')
			{
				if(is_array($file['name']))
				{
					foreach ($file['name'] as $get_file)
					{
						if($get_file['image'] != '')
						{
							$return['exists'] = true;
							break;
						}
					}
				}
				else
					$return['exists'] = true;
			}
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->image_upload_config = array(
		    'path' => \Config::get('details.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
		    'max_size' => 10000000
		);

		\Upload::process($this->image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			\Upload::save();

			$this->_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try
			{
				foreach($this->_image_data as $image_data)
				{
					if(isset($image_data['saved_to']))
					{
						chmod($image_data['saved_to'].$image_data['saved_as'], 0755);

						$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
						$image->load($image_data['saved_to'].$image_data['saved_as']);
						
						foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
						{
							$image->preset($preset);
						}
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_image($image_id = false, $content_id = false)
	{
		if($image_id && $content_id)
		{
			$images = Model_Product_Image::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
					
					// If there is only one image and image is required
					if(count($images) == 1)
					{
						if(\Config::get('details.image.required', false))
						{
							\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Product_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
					else
					{
						if($image->cover == 1)
						{
							\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Product_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('Image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Content Image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_pack_image($image_id = false, $content_id = false)
	{
		if($image_id && $content_id)
		{
			$images = Model_Packs_Image::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
					
					// If there is only one image and image is required
					if(count($images) == 1)
					{
						if(\Config::get('details.image.required', false))
						{
							\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Packs_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
					else
					{
						if($image->cover == 1)
						{
							\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Packs_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('Image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Content Image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
				//delete from amazon
				if($hold_amazon_enable)
				{
					if($content_type == 'image')
					{
						\Helper::deleteFileFromAmazonS3('media/images/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/thumbs/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/medium/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/large' . $name, $settings);
					}
				}
				// EOL - delete from amazon

				@unlink($folder . $name);
			}
		}
	}
	
	/****************************** CONTENT FILES ******************************/
	
	/**
	 * Upload all contet files to local directory defined in $this->file_upload_config
	 * 
	 */
	public function upload_file()
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// File upload configuration
		$this->file_upload_config = array(
		    'path' => \Config::get('details.file.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('pdf', 'xls', 'xlsx', 'doc', 'docx', 'txt'),
		);
		
		\Upload::process($this->file_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save file
		    \Upload::save();
			$this->_file_data = \Upload::get_files();
			
			return $return;
		}
		else
		{
			// FILE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, file is not uploaded
		return $return;
	}
	
	/**
	 * Delete content file
	 * 
	 * @param $file_id		= File ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_file($file_id = false, $content_id = false)
	{
		if($file_id && $content_id)
		{
			$files = Model_Product_File::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($files)
			{
				if(isset($files[$file_id]))
				{
					$file = $files[$file_id];
					
					// If there is only one image and image is required
					if(count($files) == 1)
					{
						if(\Config::get('details.file.required', false))
						{
							\Messages::error('You can\'t delete all files. Please add new file in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Product_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							\Messages::success('File was successfully deleted.');
						}
					}
					else
					{
						// Dont use cover option for files
						if(FALSE && $file->cover == 1)
						{
							\Messages::error('You can\'t delete cover file. Set different file as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Product_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							
							// Set another file as cover if cover is deleted
							if($file->cover == 1)
							{
								$files = Model_Product_File::find(array(
								    'where' => array(
								        'content_id' => $content_id,
								    ),
								    'order_by' => array('sort' => 'asc'),
								));
								
								$files[0]->cover = 1;
								$files[0]->save();
							}
							
							
							\Messages::success('File was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete file from file system
	 * 
	 * @param $name = File name
	 */
	public function delete_file($name = false)
	{
		if($name && \Config::get('details.file.location.root', false))
		{
			//delete from amazon
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			if($hold_amazon_enable)
			{
				\Helper::deleteFileFromAmazonS3('media/files/' . $name, $settings);
			}
			// EOL - delete from amazon

			@unlink(\Config::get('details.file.location.root') . $name);
		}
	}
	
	/**********************************************************************************
	 ********************************** IFNOTAB RELATED METHODS ***********************
	 **********************************************************************************/
	
	/**
	 * Add infotabs to product
	 * 
	 * @param $product_id	= Product ID
	 * 
	 */
	public function action_infotab_list($produt_id = false)
	{
		if(!is_numeric($produt_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$item = Model_Product::find_one_by_id($produt_id)) \Response::redirect('admin/product/list');
		
		if(\Input::post())
		{
			$add 	= \Input::post('infotabs.add', array());
			$remove = \Input::post('infotabs.remove', array());
			
			if(\Input::post('add', false))
			{
				foreach($add as $value)
				{
					$infotab = Model_Product_To_Infotabs::forge(array(
						'infotab_id' => $value,
						'product_id' => $item->id
					));	
					$infotab->save();
				}
				
				\Messages::success('Info Tabs successfully added.');
			}
			else if(\Input::post('remove', false))
			{
				foreach($remove as $value)
				{
					$this->action_infotab_delete($value);
				}
				
				\Messages::success('Info Tabs successfully removed.');
			}
			
			if(\Input::is_ajax())
			{
				echo \Messages::display('left', false);
				exit;
			}
			else
			{
				\Response::redirect(\Input::referrer(\Uri::create('admin/product/list')));
			}
			
		}
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Product Infotabs');
		
		/************ Get non related infotabs ***********/
		$items = Model_Infotab::find(function($query) use ($item){
			
			$related_ids = array();
			foreach($item->infotabs as $infotab)
				array_push($related_ids, $infotab->infotab_id);
			
			if(!empty($related_ids))
			{
				$related_ids = '(' . implode(',', $related_ids) . ')';
				$query->where('id', 'NOT IN', \DB::expr($related_ids));
			}
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		
		$item->not_related_infotabs = $items ? $items : array();
		
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'infotabs_list')
			->set('product', $item);
	}
	
	/**
	 * Delete infotab from products_to_infotabs table
	 * 
	 * @param $infotab_id = Unique Infotab ID
	 */
	public function action_infotab_delete($infotab_id)
	{
		$infotab = Model_Product_To_Infotabs::find_by_pk($infotab_id);
		
		// Delete infotab image
		if($infotab->image)
		{
			\Request::forge('admin/product/infotab/delete_image/' . $infotab_id)->execute()->response();
		}
		
		// Delete hotspot images
		if(!empty($infotab->images))
		{
			foreach($infotab->images as $image)
			{
				\Request::forge('admin/product/delete_infotab_image/' . $image->id . '/' . $image->infotab_id)->execute()->response();
			}
		}
		
		$infotab->delete();
	}
	
	/**
	 * Edit product infotabs
	 * 
	 * @param $product_id	= Product ID
	 * @param $infotab_id	= Infotab ID
	 * @param $hotspot_id	= Hotspot ID
	 * 
	 */
	public function action_infotab_edit($produt_id = false, $infotab_id = false, $hotspot_id = false)
	{
		// Check for product
		if(!is_numeric($produt_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$product = Model_Product::find_one_by_id($produt_id)) \Response::redirect('admin/product/list');
		
		// Check for infotab
		if(!is_numeric($infotab_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$item = Model_Product_To_Infotabs::find_by_pk($infotab_id)) \Response::redirect('admin/product/list');
		
		// Get hotspot is exist
		if(is_numeric($hotspot_id))
			if(!$hotspot = Model_Infotab_Image::find_by_pk($hotspot_id)) 
				unset($hotspot);
		
		if(\Input::post())
		{
			$val = Model_Product_To_Infotabs::validate('update', $item->type);
			if(\Input::post('to_upload') == 'files')
			{
				$item_files = array();
				// Upload file and display errors if there are any
				$file = $this->upload_infotab_file();
				if(!$file['exists'] && \Config::get('infotab.image.required', false) && empty($item->images)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload infotab file</strong>');
					\Messages::error('You have to select pdf');
				}
				elseif($file['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload infotab file</strong>');
					foreach($file['errors'] as $error) \Messages::error($error);
				}
				
				if($val->run() && $file['is_valid'] && !(!$file['exists'] && \Config::get('infotab.file.required', false) && empty($item->files)))
				{
					/** FILES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'title_') as $file_id => $title)
					{
						if(strpos($file_id, 'new_') === false)
						{
							$item_files[$file_id] = array(
								'id'	=> $file_id,
								'data'	=> array(
									'title'		=> \Input::post('title_' . $file_id, ''),
								),
							);
						}
					}

					// Save files if new ones are submitted
					if(isset($this->_infotab_file_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_infotab_file_data as $file_data)
						{
							$cover_count = count($item->files);
							if(strpos($file_data['field'], 'new_') === false)
							{
								// Update existing file
								if(str_replace('file_', '', $file_data['field']) != 0)
								{
									$file_id = (int)str_replace('file_', '', $file_data['field']);
									$cover_count--;
									
									$item_files[$file_id] = array(
										'id'	=> $file_id,
										'data'	=> array(
											'infotab_id'	=> $item->id,
											'item_id'	=> $product->id,
											'file'			=> $file_data['saved_as'],
											'title'			=> \Input::post('title_' . $file_id, ''),
											'item_type'		=> 'product'
										),
									);
									
									$this->delete_file(\Input::post('file_db_' . $file_id, ''));
								}
							}
							else
							{
								// Save new file
								$file_tmp = str_replace('file_new_', '', $file_data['field']);
								
								$item_files[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'infotab_id'	=> $item->id,
										'item_id'	=> $product->id,
										'file'			=> $file_data['saved_as'],
										'title'			=> \Input::post('title_new_' . $file_tmp, ''),
										'cover'			=> $cover_count == 0 ? 1 : 0,
										'sort'			=> $cover_count + 1,
										'item_type'		=> 'product'
									),
								);
							}

							//save to amazon
							if($hold_amazon_enable)
							{
			                    $file_info = array(
			                            'savePath' => 'media/files/'.$file_data['saved_as'],
			                            'filePath' => $file_data['saved_to'].$file_data['saved_as'],
			                            'contentType' => mime_content_type($file_data['saved_to'].$file_data['saved_as']),
			                        );
								\Helper::uploadFileToAmazonS3($file_info, $settings);
								@unlink($file_data['saved_to'].$file_data['saved_as']);
							}
							// EOL - save to amazon
						}
					}
					
					Model_Infotab::bind_files($item_files);
					/** END OF IMAGES **/
					
					// Get POST values
					$insert = \Input::post();
					
					// Prep some values
					$insert['description'] = trim($insert['description']);
					$insert['active'] = !empty($insert['description']) ? 1 : 0;
					
					$item->set($insert);
					
					try{
						$item->save();
						
						\Messages::success('Infotab successfully updated.');
						\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/infotab_list/'.$product->id) : \Uri::admin('current'));
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab</strong>');
					    
						// Uncomment lines below to show database errors
						//$errors = $e->getMessage();
				    	//\Messages::error($errors);
					}
				}
				else
				{
					// Delete uploaded images if there is product saving error
					if(isset($this->_infotab_file_data))
					{
						foreach($this->_infotab_file_data as $file_data)
						{
							$this->delete_infotab_file($file_data['saved_as']);
						}
					}
							
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}
			else
			{
				$item_images = array();
				// Upload image and display errors if there are any
				$image = $this->upload_infotab_image();
				if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->images)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
					\Messages::error('You have to select image');
				}
				elseif($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->images)))
				{
					/** IMAGES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
					{
						if(strpos($image_id, 'new_') === false)
						{
							$item_images[$image_id] = array(
								'id'	=> $image_id,
								'data'	=> array(
									'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
								),
							);
						}
					}
					
					// Save images if new files are submitted
					if(isset($this->_infotab_image_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_infotab_image_data as $image_data)
						{
							$cover_count = count($item->images);
							if(strpos($image_data['field'], 'new_') === false)
							{
								// Update existing image
								if(str_replace('image_', '', $image_data['field']) != 0)
								{
									$image_id = (int)str_replace('image_', '', $image_data['field']);
									$cover_count--;
									
									$item_images[$image_id] = array(
										'id'	=> $image_id,
										'data'	=> array(
											'infotab_id'	=> $item->unique_id,
											'image'			=> $image_data['saved_as'],
											'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
											'item_type'		=> 'product'
										),
									);
									
									$this->delete_infotab_image(\Input::post('image_db_' . $image_id, ''));
								}
							}
							else
							{
								// Save new image
								$image_tmp = str_replace('image_new_', '', $image_data['field']);
								
								$item_images[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'infotab_id'	=> $item->unique_id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
										'cover'			=> $cover_count == 0 ? 1 : 0,
										'sort'			=> $cover_count + 1,
										'item_type'		=> 'product'
									),
								);
							}

							//save to amazon
							if($hold_amazon_enable)
							{
								$folder_list = array('', 'thumbs/');
								foreach ($folder_list as $folder)
								{
			                        $file_info = array(
			                                'savePath' => 'media/images/'.$folder.$image_data[0]['saved_as'],
			                                'filePath' => $image_data[0]['saved_to'].$folder.$image_data[0]['saved_as'],
			                                'contentType' => mime_content_type($image_data[0]['saved_to'].$folder.$image_data[0]['saved_as']),
			                            );
									\Helper::uploadFileToAmazonS3($file_info, $settings);
									@unlink($image_data[0]['saved_to'].$folder.$image_data[0]['saved_as']);
								}
							}
							// EOL - save to amazon
						}
					}
					
					Model_Infotab::bind_images($item_images);
					/** END OF IMAGES **/
					
					// Get POST values
					$insert = \Input::post();
					
					// Prep some values
					$insert['description'] = trim($insert['description']);
					$insert['active'] = !empty($insert['description']) ? 1 : 0;
					
					$item->set($insert);
					
					try{
						$item->save();
						
						\Messages::success('Infotab successfully updated.');
						\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/infotab_list/'.$product->id) : \Uri::admin('current'));
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab</strong>');
					    
						// Uncomment lines below to show database errors
						//$errors = $e->getMessage();
				    	//\Messages::error($errors);
					}
				}
				else
				{
					// Delete uploaded images if there is product saving error
					if(isset($this->_infotab_image_data))
					{
						foreach($this->_infotab_image_data as $image_data)
						{
							$this->delete_infotab_image($image_data['saved_as']);
						}
					}
							
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}
		}
		
		$item = Model_Product_To_Infotabs::find_by_pk($infotab_id);

        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Edit Product Info Tab');
		
		$theme_partial = \Theme::instance()->set_partial('content', $this->view_dir . 'infotabs_edit_' . strtolower($item->type))
			->set('product', $product)
			->set('infotab', $item);
		
		if(isset($hotspot))
			$theme_partial->set('hotspot', $hotspot);
	}
	
	/**
	 * Edit product hotspot infotab
	 * This infotab has realy custom form and needs to be separated of others
	 * 
	 * @param $product_id	= Product ID
	 * @param $infotab_id	= Infotab ID
	 * 
	 */
	public function action_infotab_edit_hotspot($produt_id = false, $infotab_id = false)
	{
		// Check for product
		if(!is_numeric($produt_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$product = Model_Product::find_one_by_id($produt_id)) \Response::redirect('admin/product/list');
		
		// Check for infotab
		if(!is_numeric($infotab_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$item = Model_Product_To_Infotabs::find_by_pk($infotab_id)) \Response::redirect('admin/product/list');
		
		if(\Input::post())
		{
			if(\Input::post('image', false))
			{
				// Upload image and display errors if there are any
				$image = $this->upload_infotab_image();
				if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->image)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
					\Messages::error('You have to select image');
				}
				elseif($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if(($image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->image))) || \Input::post('use_cover_image', false))
				{
					// Clear previous messages if exists
					\Messages::reset();
					
					// Get POST values
					$insert = \Input::post();
					
					$item_image['alt_text'] = \Input::post('alt_text', false) ? \Input::post('alt_text', false) : NULL;
					
					// Use cover image
					if(\Input::post('use_cover_image', false))
					{
						$cover_image = \Input::post('cover_image');
						$old_image = \Config::get('details.image.location.root') . $cover_image; 
						$new_image	 = \Config::get('infotab.image.location.root') . $cover_image; 
						
						var_dump($old_image, $new_image);
						
						while(file_exists($new_image))
						{
							$file_name 	= pathinfo($new_image, PATHINFO_FILENAME);
							$file_ext 	= pathinfo($new_image, PATHINFO_EXTENSION);
							$file_name = \Str::increment($file_name);
							
							$new_image = \Config::get('infotab.image.location.root') . $file_name . '.' . $file_ext;
						}
						
						if(\File::copy($old_image, $new_image))
						{
							$image = \Image::forge(array('presets' => \Config::get('infotab.image.resize', array())));
							$image->load($new_image);
							
							foreach(\Config::get('infotab.image.resize', array()) as $preset => $options)
							{
								$image->preset($preset);
							}
							
							if(isset($item_image))
							{
								$insert['alt_text'] = isset($item_image['alt_text']) ? $item_image['alt_text'] : NULL;
								$insert['image'] 	= basename($new_image);
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));

								//save to amazon
								$settings = \Config::load('amazons3.db');
								$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
								if($hold_amazon_enable)
								{
									$folder_list = array('', 'thumbs/');
									foreach ($folder_list as $folder)
									{
				                        $file_info = array(
				                                'savePath' => 'media/images/'.$folder.$file_name . '.' . $file_ext,
				                                'filePath' => $new_image,
				                                'contentType' => mime_content_type($new_image),
				                            );
										\Helper::uploadFileToAmazonS3($file_info, $settings);
										@unlink($new_image);
									}
								}
								// EOL - save to amazon
							}
						}
						else
						{
							\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
							\Messages::error('Please try again');
						}
						
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					else
					{
						/** IMAGES **/
						// Save images if new files are submitted
						if(isset($this->_infotab_image_data))
						{
							$settings = \Config::load('amazons3.db');
							$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
							foreach($this->_infotab_image_data as $image_data)
							{
								$item_image['image'] = $image_data['saved_as'];
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));

								//save to amazon
								if($hold_amazon_enable)
								{
									$folder_list = array('', 'thumbs/');
									foreach ($folder_list as $folder)
									{
				                        $file_info = array(
				                                'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
				                                'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
				                                'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
				                            );
										\Helper::uploadFileToAmazonS3($file_info, $settings);
										@unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
									}
								}
								// EOL - save to amazon
							}
						}
						
						if(isset($item_image))
						{
							$insert['alt_text'] = isset($item_image['alt_text']) ? $item_image['alt_text'] : NULL;
							$insert['image'] 	= isset($item_image['image']) ? $item_image['image'] : $item->image;
						}
						/** END OF IMAGES **/
					}
					
					// Make infotab active
					$insert['active'] = 1;
					
					$item->set($insert);
					
					try{
						$item->save();
						
						\Messages::success('Infotab image successfully updated.');
						\Response::redirect(\Uri::create('admin/product/infotab_edit/' . $product->id . '/' . $item->unique_id));
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab image</strong>');
					    
						// Uncomment lines below to show database errors
						//$errors = $e->getMessage();
				    	//\Messages::error($errors);
					}
				}
				else
				{
					// Delete uploaded images if there is product saving error
					if(isset($this->_infotab_image_data))
					{
						foreach($this->_infotab_image_data as $image_data)
						{
							$this->delete_infotab_image($image_data['saved_as']);
						}
					}
				}
			}
			
			if(\Input::post('hotspot', false))
			{
				
			}
		}
		
		\Response::redirect(\Uri::create('admin/product/infotab_edit/' . $product->id . '/' . $item->unique_id));
	}
	
	/**
	 * Edit product hotspot position
	 * 
	 * @param $product_id	= Product ID
	 * @param $infotab_id	= Infotab ID
	 * 
	 */
	public function action_infotab_hotspot($produt_id = false, $infotab_id = false, $hotspot_id = false)
	{
		// Check for product
		if(!is_numeric($produt_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$product = Model_Product::find_one_by_id($produt_id)) \Response::redirect('admin/product/list');
		
		// Check for infotab
		if(!is_numeric($infotab_id)) \Response::redirect('admin/product/list');
		// Get news item to edit
		if(!$item = Model_Product_To_Infotabs::find_by_pk($infotab_id)) \Response::redirect('admin/product/list');
		
		// Get hotspot is exist
		if(is_numeric($hotspot_id))
			if(!$hotspot = Model_Infotab_Image::find_by_pk($hotspot_id)) 
				unset($hotspot);
		
		if(\Input::post())
		{
			$insert = \Input::post();
			
			if(!\Input::is_ajax())
			{
				$val = Model_Infotab_Image::validate('create');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create hotspot</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
					\Response::redirect(\Uri::create('admin/product/infotab_edit/' . $product->id . '/' . $item->unique_id . (isset($hotspot) ? '/'.$hotspot->id : '')));
				}
				
				$insert['title'] 		= trim($insert['title']) != '' ? $insert['title'] : NULL;
				$insert['description'] 	= trim($insert['description']) != '' ? $insert['description'] : NULL;
			}
			
			$insert['infotab_id'] 	= $infotab_id;
			$insert['item_type'] 	= 'product';
			
			if(\Input::post('create', false))
			{
				$hotspot = Model_Infotab_Image::forge($insert);
				
				try{
					$hotspot->save();
					
					if(\Input::is_ajax())
					{
						$return['hotspot_id'] = $hotspot->id;
						echo json_encode($return); exit;
					}
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create hotspot</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
			    	
					if(\Input::is_ajax())
					{
						$return['message'] = \Messages::display();
						$return['hotspot_id'] = false;
						echo json_encode($return); exit;
					}
			    	
				}
			}
			
			if(\Input::post('update', false))
			{
				if(isset($hotspot))
				{
					/** IMAGES **/
					// Upload image and display errors if there are any
					$image = $this->upload_infotab_image();
					if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($hotspot->image)) 
					{
						// No previous images and image is not selected and it is required
						\Messages::error('<strong>There was an error while trying to upload hotspot image</strong>');
						\Messages::error('You have to select image');
					}
					elseif($image['errors'])
					{
						\Messages::error('<strong>There was an error while trying to upload hotspot image</strong>');
						foreach($image['errors'] as $error) \Messages::error($error);
					}
					
					if(($image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($hotspot->image))) || \Input::post('use_cover_image', false))
					{
						// Clear previous messages if exists
						\Messages::reset();
						
						$item_image['alt_text'] = \Input::post('alt_text', false) ? \Input::post('alt_text', false) : NULL;
						
						// Save images if new files are submitted
						if(isset($this->_infotab_image_data))
						{
							$settings = \Config::load('amazons3.db');
							$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
							foreach($this->_infotab_image_data as $image_data)
							{
								$item_image['image'] = $image_data['saved_as'];
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));

								//save to amazon
								if($hold_amazon_enable)
								{
				                    $file_info = array(
				                            'savePath' => 'media/files/'.$image_data['saved_as'],
				                            'filePath' => $image_data['saved_to'].$image_data['saved_as'],
				                            'contentType' => mime_content_type($image_data['saved_to'].$image_data['saved_as']),
				                        );
									\Helper::uploadFileToAmazonS3($file_info, $settings);
									@unlink($image_data['saved_to'].$image_data['saved_as']);
								}
								// EOL - save to amazon
							}
						}
						
						if(isset($item_image))
						{
							$insert['alt_text'] = isset($item_image['alt_text']) ? $item_image['alt_text'] : NULL;
							$insert['image'] 	= isset($item_image['image']) ? $item_image['image'] : $hotspot->image;
						}
					}
					else
					{
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					/** END OF IMAGES **/
					
					/** VIDEOS **/
					$item_video['video_title'] = \Input::post('video_title', false) ? \Input::post('video_title', false) : NULL;
					$item_video['video'] = \Input::post('video_url', false) ? \Input::post('video_url', false) : NULL;
					
					if(!is_null($item_video['video']))
					{
						// Check video
						$youtube = \App\Youtube::forge();
						$video = $youtube->parse($item_video['video'])->get();
						
						if(!$video)
						{
							\Messages::error('"' . $item_video['video'] . '" is invalid video URL. Video not updated.');
							
							// Revert to old values
							$item_video['video_title'] 	= $hotspot->video_title;
							$item_video['video'] 		= $hotspot->video;
						}
					}
					
					if(isset($item_video))
					{
						$insert['video'] 	= isset($item_video['video']) ? $item_video['video'] : NULL;
						$insert['video_title'] = isset($item_video['video_title']) ? $item_video['video_title'] : NULL;
						
						// Unset video title is there is no video
						if(is_null($insert['video'])) $insert['video_title'] = NULL;
					}
					/** END OF: VIDEOS **/
					
					$hotspot->set($insert);
					
					try{
						$hotspot->save();
						\Messages::success('Hotspot sucessfully updated.');
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('There was an error while trying to update hotspot.');
						\Messages::error('Please try again.');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
				    	
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					
					if(\Input::is_ajax())
					{
						echo \Messages::display(); exit;
					}
				}
			}
		}
		
		\Response::redirect(\Uri::create('admin/product/infotab_edit/' . $product->id . '/' . $item->unique_id . (isset($hotspot) ? '/'.$hotspot->id : '')));
	}
    
    protected function delete_product_attribute($product_id = null)
    {
        if($item = Model_Product::find_one_by_id($product_id))
        {
            if(!empty($item->attributes))
            {
                foreach($item->attributes as $attribute)
                {
                    
                    if(!empty($attribute->prices))
                    {
                        foreach ($attribute->prices as $price) $price->delete();
                    }
                    
                    if(!empty($attribute->images))
                    {
                        foreach ($attribute->images as $image)
                        {
                            $this->delete_image($image->image);
                            $image->delete();
                        }
                    }
                    
                    $attribute->delete();
                }
            }
        }
    }
    	
    public function action_import(){
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Bulk Add Products');
    	
		if(\Input::file())
		{
			if(\Input::file('file')['tmp_name'])
			{
	    		$handle = fopen(\Input::file('file')['tmp_name'], 'r');
	    		while( ( $data = fgetcsv($handle, 1000, ",") ) !== FALSE  )
				{
					if($data[0] == 'category(header)')
						continue;

					if($data[2])
					{
						$insert['title'] = $data[2];
						$insert['code'] = $data[3];
						$insert['description_full'] = $data[6];

						$inserts['category'] = $data[0];
						$inserts['subcategory'] = $data[1];
						$inserts['brand'] = $data[5];

						$insert['active_from'] = NULL;
						$insert['active_to'] = NULL;
						$insert['status'] = 1;
						$item = \Product\Model_Product::forge($insert);
						try{
							$item->save();

							$product_slug = \Inflector::friendly_title($item->title, '-', true);
							$product_slug_result = \Product\Model_Product_Seo::find(function($query) use ($product_slug){
								$query->where('slug', $product_slug);
							});
							if(isset($product_slug_result[0]))
							{
								while(isset($product_slug_result[0]))
								{
									$product_slug = \Str::increment($product_slug, 1, '-');
									$product_slug_result = \Product\Model_Product_Seo::find(function($query) use ($product_slug){
										$query->where('slug', $product_slug);
									});
								}
							}
							$props = array( 
								'content_id' => $item->id, 
								'content_type' => 'product', 
								'slug' => $product_slug,
								'meta_robots_index' => 1,
								'meta_robots_follow' => 1
							);
							\DB::insert('content_seo')->set($props)->execute();
							// END OF: SEO


							//Image 
							if(!empty($data[7]))
							{
								$file = @file_get_contents($data[7]);
								if($file)
								{
									$parts_url = explode('/', $data[7]);
									$url = end($parts_url);
									$filename = explode('?', basename($url));
									$extension = pathinfo($filename[0] , PATHINFO_EXTENSION);
									if ( $extension != "" )
						            {
						                $extension = "." . $extension;
						            }

						            $file_info = new \finfo(FILEINFO_MIME_TYPE);
									$mime_type = $file_info->buffer($file);
									if($mime_type)
									{
										$mt = explode('/', $mime_type);
										$extension = "." . $mt[1];
									}
									$generated_filename = md5(date('Y-m-d H:i:s:u')) . $extension;

									file_put_contents(\Config::get('details.' . 'image' . '.location.root').$generated_filename, $file);

									$image = \Image::forge(array('presets' => \Config::get('details.' . 'image' . '.resize', array())));
									$image->load(\Config::get('details.' . 'image' . '.location.root').$generated_filename);
									
									foreach(\Config::get('details.' . 'image' . '.resize', array()) as $preset => $options)
									{
										$image->preset($preset);
									}
									
									$item_image = array(
										array(
											'id'	=> 0,
											'data'	=> array(
												'content_id'	=> $item->id,
												'image'			=> $generated_filename,
												'alt_text'		=> '',
												'cover'			=> 1,
												'sort'			=> 1,
											),
										),
									);
									
									\Product\Model_Product::bind_images($item_image);
								}
							}
							// End of Image

							//price		
							$insert_attribute = \Product\Model_Attribute::forge(array(
	                        	'product_id' 			=> $item->id,
	                        	'attribute_group_id' 	=> 0,
	                        	'attributes' 			=> NULL,
	                        	'product_code' 			=> NULL,
	                        	'retail_price' 			=> $data[4],
	                        	'active' 				=> 1,
	                        	'sale_price' 			=> 0,
	                            'stock_quantity'        => 0,
	                        ));
	                        $insert_attribute->save();

							$category_id = 0;
							$subcategory_id = 0;
							$category_items = \Product\Model_Category::find(function($query) use ($inserts){
								$query->where('title', $inserts['category']);
							});
							if(isset($category_items[0]))
							{
								$category_id = $category_items[0]->id;
								if(!empty($inserts['subcategory']))
								{
									$subcategory_items = \Product\Model_Category::find(function($query) use ($inserts, $category_id){
										$query->where('title', $inserts['subcategory']);
										$query->where('parent_id', $category_id);
									});
									if(isset($subcategory_items[0]))
									{
										$category_id = $subcategory_items[0]->id;
										$subcategory_id = $subcategory_items[0]->id;
									}
								}
							}

							if($category_id == 0)
							{
								if(!empty($inserts['category']))
								{
									$category_insert_arr['title'] = $inserts['category'];
									$category_insert_arr['active_from'] = NULL;
									$category_insert_arr['active_to'] = NULL;
									$category_insert_arr['status'] = 1;
									$category_insert_arr['parent_id'] = 0;
									$category_insert = \Product\Model_Category::forge($category_insert_arr);
									$category_insert->save();
									$category_id = $category_insert->id;
									// Validate and insert category slug into SEO database table								
									$product_slug = \Inflector::friendly_title($category_insert->title, '-', true);
									$product_slug_result = \Product\Model_Category_Seo::find(function($query) use ($product_slug){
										$query->where('slug', $product_slug);
									});
									if(isset($product_slug_result[0]))
									{
										while(isset($product_slug_result[0]))
										{
											$product_slug = \Str::increment($product_slug, 1, '-');
											$product_slug_result = \Product\Model_Category_Seo::find(function($query) use ($product_slug){
												$query->where('slug', $product_slug);
											});
										}
									}
									$props = array( 
										'content_id' => $category_insert->id, 
										'content_type' => 'product_category', 
										'slug' => $product_slug,
										'meta_robots_index' => 1,
										'meta_robots_follow' => 1
									);
									\DB::insert('content_seo')->set($props)->execute();
									// END OF: SEO
								}
							}
							if($subcategory_id == 0)
							{
								if(!empty($inserts['subcategory']))
								{
									$subcategory_insert_arr['title'] = $inserts['subcategory'];
									$subcategory_insert_arr['active_from'] = NULL;
									$subcategory_insert_arr['active_to'] = NULL;
									$subcategory_insert_arr['status'] = 1;
									$subcategory_insert_arr['parent_id'] = $category_id;
									$subcategory_insert = \Product\Model_Category::forge($subcategory_insert_arr);
									$subcategory_insert->save();
									$category_id = $subcategory_insert->id;
									// Validate and insert category slug into SEO database table	
									$product_slug = \Inflector::friendly_title($subcategory_insert->title, '-', true);
									$product_slug_result = \Product\Model_Category_Seo::find(function($query) use ($product_slug){
										$query->where('slug', $product_slug);
									});
									if(isset($product_slug_result[0]))
									{
										while(isset($product_slug_result[0]))
										{
											$product_slug = \Str::increment($product_slug, 1, '-');
											$product_slug_result = \Product\Model_Category_Seo::find(function($query) use ($product_slug){
												$query->where('slug', $product_slug);
											});
										}
									}
									$props = array( 
										'content_id' => $subcategory_insert->id, 
										'content_type' => 'product_category', 
										'slug' => $product_slug,
										'meta_robots_index' => 1,
										'meta_robots_follow' => 1
									);
									\DB::insert('content_seo')->set($props)->execute();
									// END OF: SEO
								}
							}
							
							// Insert product categories
							$item_categories = \Product\Model_Product_To_Categories::forge(array(
									'product_id' 	=> $item->id,
									'category_id'	=> $category_id,
							));
							$item_categories->save();

						}
						catch (\Database_Exception $e){ continue; }
						catch (\Exception $e){ continue; }
					}
		
				}
	    		\Messages::success('Product was successfully imported.');
    		}
    		else
    		{
	    		\Messages::error('Select file to upload.');
    		}
    	}
    	\Theme::instance()->set_partial('content', $this->view_dir . 'import');
    }
	
}

                            