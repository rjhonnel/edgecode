<?php

/**
 * Controller Admin Price.
 *
 * @extends  \Admin\Controller_Base
 */
namespace Product;

class Controller_Admin_Price extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/price/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('product::price', 'details');
		\Config::load('product::infotab', 'infotab');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
    /**
	 * Index
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/list');
	}


    private function check_attr_code_exists($product_id, $attr_id = 0, $attr_code) {

        $flag = Model_Attribute::find(function($query) use($product_id, $attr_code, $attr_id) { 
            $query->where('id', '!=', $attr_id);
            $query->and_where('product_id', '=', $product_id);
            $query->and_where('product_code', '=', $attr_code);
        });

        if ($flag) {
            return true;
        }

        return false;
    }
    
	/**
	 * Update product price
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_update($id = false)
	{

        $flag = true;

		if(!is_numeric($id)) \Response::redirect('admin/product/list');
		
		// Get product item to edit
		if(!$item = Model_Product::find_one_by_id($id)) \Response::redirect('admin/product/list');
        
        // Redirect to attribute group
        if(\Input::get('attribute_group', false) === false && !empty($item->attributes))
        {
            foreach($item->attributes as $attr_obj)
            {
                if($attr_obj->attribute_group_id > 0)
                {
                    \Response::redirect(\Uri::create(\Uri::admin(), array(), array(
                        'attribute_group' => $attr_obj->attribute_group_id
                    )));
                }
            }
        }

        // NRB-Gem: Comment out; Logic does not apply anymore
        // if(\Input::get('user_group', false) === false)
        // {
        //     \Response::redirect(\Uri::create(\Uri::admin(), array(), array('user_group' => 3) + \Input::get()));
        // }
        \View::set_global('menu', 'admin/product/list');
        \View::set_global('title', 'Edit Product Price');
        
        \View::set_global('quick_menu_save', 'form.main_form');
        
        // Update
        if(\Input::post())  
        {

            $post = \Input::post();
            
            $return_to = is_numeric(\Input::post('return_to')) ? '#' . \Input::post('return_to') : '' ;
            if(\Input::post('apply_all_save'))
            {     
                // Find attributes for current product
                $items_db = Model_Attribute::find(function($query) use($id, $post){
                    
                    $query->select('product_attributes.*');
                    $query->join('attribute_groups','LEFT')->on('attribute_groups.id', '=', 'product_attributes.attribute_group_id');
                    $query->where('product_id', $id);
                    
                    if(isset($post['attribute_group_id']) && is_numeric($post['attribute_group_id']))
                    {
                        $query->and_where('product_attributes.attribute_group_id', $post['attribute_group_id']);
                    }
                    
                    $query->order_by('attribute_groups.sort', 'asc');
                    
                }, 'id');
                
                // Decode attribute json
                $combinations_db_arr = $this->decode_attribute_json($items_db);
                
                // Find current attribute group
                $attribute_group = array();
                // Set vars
                $combinations      = array();
                $combinations_data = array();
                $combinations_tmp  = array();
                if($post['attribute_group_id'])
                {
                    $attribute_group = \Attribute\Model_Attribute_Group::find_one_by_id($post['attribute_group_id']);
                    
                    // Create attribute combinations
                    if($attribute_group && $attribute_group->attributes)
                    {
                        $i = 0;
                        foreach($attribute_group->attributes as $attribute)
                        {
                                    
                            if($attribute->options)
                            {
                                foreach ($attribute->options as $option)
                                {
                                    $combinations[$i][] =  $attribute->title . ': ' . $option->title;
                                    $combinations_tmp[$i][] = array($attribute->id => $option->id);
                                }
                            }
                            $i++;
                        }
                        
                        // Create combinations from current attributes
                        $combinations = $this->combos($combinations);
                        $combinations_tmp = $this->combos($combinations_tmp);
                    }
        
                    // Sort array asc
                    $combinations_sorted = $this->sort_array_asc($combinations_tmp);
                    
                    foreach ($combinations_sorted as $key => $value)
                    {
                        $combinations_data[$key] = json_encode($value);
                    }
                }

                if($combinations)
                {
                    foreach ($combinations as $key => $combination)
                    {
                        $hold_code = $item->code ? $item->code.'-'.($key+1) : null;
                        $attributes_data = $combinations_data[$key];

                        $update = Model_Attribute::find(function($query) use($post, $hold_code, $attributes_data){
                            $query->where('product_id', $post['product_id']);
                            $query->where('attributes', $attributes_data);
                            $query->where('product_code', $hold_code);
                        });

                        if(isset($update[0]))
                        {
                            $update = $update[0];
                            $data = array(
                                'retail_price'          => \Input::post('price_apply_all'),
                                'active'                => \Input::post('enable_all_price_attr'),
                            );

                            $update->set($data);
                            $update->save();
                        }
                        else
                        {
                            $insert = Model_Attribute::forge(array(
                                'product_id'            => $post['product_id'],
                                'attribute_group_id'    => $post['attribute_group_id'],
                                'attributes'            => $attributes_data,
                                'product_code'          => $hold_code,
                                'retail_price'          => \Input::post('price_apply_all'),
                                'active'                => \Input::post('enable_all_price_attr'),
                                'sale_price'            => 0,
                                'stock_quantity'        => 0,
                                'delivery_charge'        => 0,
                            ));
                            $insert->save();
                        }
                    }
                }

                \Messages::success('Attributes successfully updated.');
                \Response::redirect(\Uri::create(\Uri::admin('current'), array(), \Input::get()) . $return_to);
            }
            
            //$val = Model_Attribute::validate();
            
            try
            {
                if(isset($post[$post['price_type']]))
                {
                    foreach($post[$post['price_type']] as $key => $price)
                    {
                         if($update_price = Model_Attribute_Price::find_one_by_id($key))
                         {
                             if(isset($post['active'][$key])) $update_price->active = $post['active'][$key];
                             if(isset($post['product_group_discount_id'][$key])) $update_price->product_group_discount_id = $post['product_group_discount_id'][$key];
                             $update_price->save();
                         }
                    }
                }
            
                $multiple_images = array();
                $image_new = false;
                foreach($post['attributes'] as $key => $attribute)
                {

                    if(!empty($post['delete_items']))
                    {
                        $delete_items = explode(',', $post['delete_items']);
                        $result1 = \DB::delete('product_attributes')->where('id', 'in', $delete_items)->execute();
                        $result2 = \DB::delete('product_attribute_price')->where('product_attribute_id', 'in', $delete_items)->execute();
                    }

                    // Check existing product attribute group
                    $existing_attribute_group = Model_Attribute::find(function($query) use($post)
                    {
                        $query->where('product_id', $post['product_id']);
                        $query->and_where_open();
                        $query->where('attribute_group_id', null, \DB::expr('IS NOT NULL'));
                       // $query->and_where('attribute_group_id', '!=' , 0);
                        $query->and_where('attribute_group_id', '!=' , $post['attribute_group_id']);
                        $query->and_where_close();
                    });

                    //if($existing_attribute_group && $post['attribute_group_id'] != 0)
                    if($existing_attribute_group)
                    {
                        foreach($existing_attribute_group as $item)
                        {
                            $delete_attribute = $item->id;
                            $item->delete();
                            $attribute_option = Model_Attribute_Price::find_one_by_product_attribute_id($delete_attribute);
                            if($attribute_option) $attribute_option->delete();
                        }
                    }

                    // Update
                    if(isset($post['update_items'][$key]))
                    {


                        // Lightmedia - michael: check if product attribute code is exists on the product
                        if ($this->check_attr_code_exists($id, $post['update_items'][$key], $post['product_code'][$key])) {

                            $flag = false;
                            \Messages::error($post['product_code'][$key]. ' code already used');
                            continue;
                        }

                        $update = Model_Attribute::find_one_by_id($post['update_items'][$key]);
                        if($update)
                        {                        	
                            $item_images = array();
                            $data = array(
                            	'product_id' 			=> $post['product_id'],
                            	'attribute_group_id' 	=> $post['attribute_group_id'],
                            	'attributes' 			=> $attribute,
                            	'product_code' 			=> $post['product_code'][$key],
                            	'retail_price' 			=> $post['retail_price'][$key],
                            	'sale_price' 			=> $post['sale_price'][$key],
                                'stock_quantity'        => $post['stock_quantity'][$key],
                                'delivery_charge'        => $post['delivery_charge'][$key],
                            	'active'				=> isset($post['active']) && $post['active'][$key] ? $post['active'][$key] : $post['active_new'][$key],
                            );
                            // Default radio
                            $data['default'] = 0;
                            if(isset($post['default']) && $post['default'] == $key)
                            {
                               $this->reset_default($item->id);
                               $data['default'] = 1;
                            }
                            $update->set($data);
                            $update->save();
                            $attr_id = $update->id;
                            
                            // Get combinations for multiple images
                            if(isset($post['apply_image']) && isset($post['action']))
                            {
                                if(in_array($attr_id, $post['action'])) $multiple_images['action'][$attr_id] = $attr_id;
                            }
                            
                            if(isset($_FILES['image_new_' . $attr_id]))
                            {
                                // Upload image and display errors if there are any
                                $image = $this->upload_image('image');
                                
                                
                                if($image['errors'] && $image['exists'])
                                {
                                    \Messages::error('<strong>There was an error while trying to upload product attribute image</strong>');
                                    foreach($image['errors'] as $error) \Messages::error($error);
                                }

                                // if($image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)))
                                if($image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false)))
                                {
                                    /** IMAGES **/
                                    // Get all alt texts to update if there is no image change
                                    foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
                                    {
                                        $cover_image = 0;
                                        if(\Input::post('cover_image'))
                                        {
                                            if(\Input::post('cover_image') == $image_id)
                                            $cover_image = 1;
                                        }
                                        if(strpos($image_id, 'new_') === false)
                                        {
                                            $item_images[$image_id] = array(
                                                'id'	=> $image_id,
                                                'data'	=> array(
                                                    'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
                                                    'cover'         => $cover_image,
                                                ),
                                            );

                                            if(!empty($item_images)) Model_Attribute::bind_images($item_images);
                                        }
                                    }

                                    // Save images if new files are submitted
                                    if(isset($this->_image_data) && $image['exists'] !== false)
                                    {
                                        $settings = \Config::load('amazons3.db');
                                        $hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
                                        foreach($this->_image_data as $image_data)
                                        {
                                            $cover_count = count($update->images);
                                            if(strpos($image_data['field'], 'new_') === false)
                                            {
                                                // Update existing image
                                                if(str_replace('image_', '', $image_data['field']) != 0)
                                                {
                                                    $image_id = (int)str_replace('image_', '', $image_data['field']);
                                                    $cover_count--;
                                                    $cover_image = 0;
                                                    if(\Input::post('cover_image'))
                                                    {
                                                        if(\Input::post('cover_image') == $image_id)
                                                        $cover_image = 1;
                                                    }

                                                    $item_images[$image_id] = array(
                                                        'id'	=> $image_id,
                                                        'data'	=> array(
                                                            'content_id'	=> $attr_id,
                                                            'image'			=> $image_data['saved_as'],
                                                            'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
                                                            'cover'         => $cover_image,
                                                        ),
                                                    );

                                                    //$this->delete_image(\Input::post('image_db_' . $image_id, ''));
                                                }

                                                if(!empty($item_images)) Model_Attribute::bind_images($item_images);

                                            }
                                            else
                                            {
                                                // Save new image
                                                $image_tmp = str_replace('image_new_', '', $image_data['field']);
                                                $cover_image = 0;
                                                $image_new = $item_images[0] = array(
                                                    'id'	=> 0,
                                                    'data'	=> array(
                                                        'content_id'	=> $attr_id,
                                                        'image'			=> $image_data['saved_as'],
                                                        'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
                                                        'cover'			=> $cover_image,
                                                        'sort'			=> $cover_count + 1,
                                                    ),
                                                );

                                                if(!empty($item_images)) Model_Attribute::bind_images($item_images);

                                                // Multiple images
                                                if(isset($post['apply_image']) && isset($post['action']) && false)
                                                {
                                                    foreach($post['action'] as $action_value)
                                                    {
                                                        if($action_value == $attr_id) continue;
                                                        $item_images = array();
                                                        if($action_value > 0)
                                                        {
                                                           $image_new[0] = array(
                                                                'id'	=> 0,
                                                                'data'	=> array(
                                                                    'content_id'	=> $action_value,
                                                                    'image'			=> $image_data['saved_as'],
                                                                    'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
                                                                    'cover'			=> $cover_count == 0 ? 1 : 0,
                                                                    'sort'			=> $cover_count + 1,
                                                                ),
                                                            ); 
                                                        }
                                                        if(!empty($item_images)) Model_Attribute::bind_images($item_images);
                                                    }
                                                }
                                            }

                                            //save to amazon
                                            if($hold_amazon_enable)
                                            {
                                                $folder_list = array('', 'large/', 'medium/', 'thumbs/');
                                                foreach ($folder_list as $folder)
                                                {
                                                    $file_info = array(
                                                            'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
                                                            'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
                                                            'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
                                                        );
                                                    \Helper::uploadFileToAmazonS3($file_info, $settings);
                                                    @unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
                                                }
                                            }
                                            // EOL - save to amazon
                                        }
                                    }
                                    /** END OF IMAGES **/
                                }
                            }
                            
                            if(isset($post['active_new'][$key]))
                            {
                                //NRB-Gem: insert attributes to existing Products added to Pricing Groups
                                $this->add_attribute_price($post, array(
                                    'attr_id' => $attr_id,
                                    'key' => $key
                                ));
                            }
                        }
                    }
                    // Insert
                    else 
                    {

                        if ($this->check_attr_code_exists($post['product_id'], false, $post['product_code_new'][$key])) {
                            $flag = false;
                            \Messages::error($post['product_code_new'][$key]. ' code already used');
                            continue;
                        }


                        $item_images = array();
                        $insert = Model_Attribute::forge(array(
                        	'product_id' 			=> $post['product_id'],
                        	'attribute_group_id' 	=> $post['attribute_group_id'],
                        	'attributes' 			=> $attribute,
                        	'product_code' 			=> $post['product_code_new'][$key],
                        	'retail_price' 			=> $post['retail_price_new'][$key],
                        	'active' 				=> $post['active_new'][$key],
                        	'sale_price' 			=> $post['sale_price_new'][$key],
                            'stock_quantity'        => $post['stock_quantity_new'][$key],
                            'delivery_charge'        => $post['delivery_charge_new'][$key],
                        ));
                        // Default radio
                        $insert->default = 0;
                        if(isset($post['default']) && $post['default'] == $key)
                        {
                           $this->reset_default($item->id);
                           $insert->default = 1;
                        }
                        $insert->save();
                        $attr_id = $insert->id;
                        
                        // Get combinations for multiple images
                        if(isset($post['apply_image']) && isset($post['action_new'][$key]) && $post['action_new'][$key] == 1)
                        {
                            $multiple_images['action_new'][$attr_id] = $attr_id;
                        }
                        
                        if(isset($_FILES['_image_new_' . $key]))
                        {
                            // Upload image and display errors if there are any
                            $image = $this->upload_image('image');

                            if($image['errors'] && $image['exists'])
                            {
                                \Messages::error('<strong>There was an error while trying to upload product attribute image</strong>');
                                foreach($image['errors'] as $error) \Messages::error($error);
                            }

                            // if($image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)))
                            if($image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false)))
                            {
                                /** IMAGES **/
                                // Save images if new files are submitted
                                if(isset($this->_image_data) && $image['exists'] !== false)
                                {
                                    $settings = \Config::load('amazons3.db');
                                    $hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
                                    foreach($this->_image_data as $image_data)
                                    {
                                        $cover_count = 0;
                                        // Save new image
                                        $image_tmp = str_replace('_image_new_', '', $image_data['field']);

                                        $image_new = $item_images[0] = array(
                                            'id'	=> 0,
                                            'data'	=> array(
                                                'content_id'	=> $attr_id,
                                                'image'			=> $image_data['saved_as'],
                                                'alt_text'		=> \Input::post('_alt_text_new_' . $image_tmp, ''),
                                                'cover'			=> $cover_count == 0 ? 1 : 0,
                                                'sort'			=> $cover_count + 1,
                                            ),
                                        );

                                        if(!empty($item_images)) Model_Attribute::bind_images($item_images);

                                        // Multiple images
                                        if(isset($post['apply_image']) && isset($post['action_new']) && false)
                                        {
                                            foreach($post['action_new'] as $action_key => $action_value)
                                            {
                                                if($action_value == $attr_id) continue;
                                                $item_images = array();
                                                if($action_value > 0)
                                                {
                                                   $image_new[0] = array(
                                                        'id'	=> 0,
                                                        'data'	=> array(
                                                            'content_id'	=> $action_value,
                                                            'image'			=> $image_data['saved_as'],
                                                            'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
                                                            'cover'			=> $cover_count == 0 ? 1 : 0,
                                                            'sort'			=> $cover_count + 1,
                                                        ),
                                                    ); 
                                                }
                                                if(!empty($item_images)) Model_Attribute::bind_images($item_images);
                                            }
                                        }

                                        //save to amazon
                                        if($hold_amazon_enable)
                                        {
                                            $folder_list = array('', 'large/', 'medium/', 'thumbs/');
                                            foreach ($folder_list as $folder)
                                            {
                                                $file_info = array(
                                                        'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
                                                        'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
                                                        'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
                                                    );
                                                \Helper::uploadFileToAmazonS3($file_info, $settings);
                                                @unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
                                            }
                                        }
                                        // EOL - save to amazon
                                    }
                                }
                                /** END OF IMAGES **/
                            }
                        }
                        
                        
                        if(isset($post[$post['price_type'] . '_new'][$key]))
                        {
                            //NRB-Gem: insert attributes to existing Products added to Pricing Groups
                            $this->add_attribute_price($post, array(
                                'attr_id' => $attr_id,
                                'key' => $key
                            ));
                        }
                    }
                }
                
                // Update/insert multiple images
                if(!empty($multiple_images) && !empty($image_new) && isset($image_new['data']))
                {
                    $content_id = $image_new['data']['content_id'];
                    if(isset($multiple_images['action']) && !empty($multiple_images['action']))
                    {
                        foreach($multiple_images['action'] as $value)
                        {
                            $item_images = array();
                            if($content_id == $value) continue;
                            $image_new['data']['content_id'] = $value;
                            $item_images[0] = $image_new;
                            Model_Attribute::bind_images($item_images);
                        }
                    }
                    
                    if(isset($multiple_images['action_new']) && !empty($multiple_images['action_new']))
                    {
                        foreach($multiple_images['action_new'] as $value)
                        {
                            $item_images = array();
                            if($content_id == $value) continue;
                            $image_new['data']['content_id'] = $value;
                            $item_images[0] = $image_new;
                            Model_Attribute::bind_images($item_images);
                        }
                    }
                    
                }
                
                if ($flag) {
                    \Messages::success('Product successfully updated.');
                }
            }
            catch (\Database_Exception $e)
            {
                // show validation errors
                \Messages::error('<strong>There was an error while trying to update product attributes</strong>');

                // Uncomment lines below to show database errors
                // $errors = $e->getMessage();
                // \Messages::error($errors);

                \Response::redirect(\Uri::create(\Uri::admin('current'), array(), \Input::get()) . $return_to);
            }
            
            \Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/list/') : \Uri::create(\Uri::admin('current'), array(), \Input::get()) . $return_to);
            
        }
		
        // Get current product
        $product = Model_Product::find_one_by_id($id);
        
        // Create array for attribute group select
        $attribute_groups_select = \Attribute\Model_Attribute_Group::fetch_pair('id', 'title', array(), array('0' => '- No Attributes -'));
        
        // Set user group for lazy load
        if(\Input::get('user_group'))
        {
            Model_Attribute::set_user_group();
        }
        
        // Set tier price group for lazy load
        if(\Input::get('tier_price'))
        {
            Model_Attribute::set_tier_price();
        }
        
        $param = \Input::get();
     
        // Find attributes for current product
        $items_db = Model_Attribute::find(function($query) use($id, $param){
			
            $query->select('product_attributes.*');
            $query->join('attribute_groups','LEFT')->on('attribute_groups.id', '=', 'product_attributes.attribute_group_id');
            $query->where('product_id', $id);
            
            if(isset($param['attribute_group']) && is_numeric($param['attribute_group']))
            {
                $query->and_where('product_attributes.attribute_group_id', $param['attribute_group']);
            }
            
			$query->order_by('attribute_groups.sort', 'asc');
            
		}, 'id');
        
        // Decode attribute json
        $combinations_db_arr = $this->decode_attribute_json($items_db);
        
        // Find current attribute group
        $attribute_group = array();
        if(\Input::get('attribute_group'))
        {
            $attribute_group = \Attribute\Model_Attribute_Group::find_one_by_id(\Input::get('attribute_group'));
        }
        
        // Set vars
        $combinations      = array();
        $combinations_data = array();
        $combinations_tmp  = array();
        $attributes_order  = array();
        $compared          = array();
        $update_items      = array();
        $delete_items      = '';
        
        // Create attribute combinations
        if($attribute_group && $attribute_group->attributes)
        {
            $i = 0;
            foreach($attribute_group->attributes as $attribute)
            {
                $attributes_order[] = $attribute->id;
                        
                if($attribute->options)
                {
                    foreach ($attribute->options as $option)
                    {
                        $combinations[$i][] =  $attribute->title . ': ' . $option->title;
                        //$combinations[$i][] =  array($attribute->title => $option->title);
                        $combinations_tmp[$i][] = array($attribute->id => $option->id);
                    }
                }
                $i++;
            }
            
            // Create combinations from current attributes
            $combinations = $this->combos($combinations);
            $combinations_tmp = $this->combos($combinations_tmp);
            
            // Sort product atributes from database 
            if($combinations_db_arr)
            {
                $sorted_db_arr = $this->sort_array(array_keys($attribute_group->attributes), $combinations_db_arr);
                
                if($sorted_db_arr) 
                {
                    $compared = $this->compare_array($combinations_tmp, $sorted_db_arr, $combinations_db_arr);
                }
            }
        }
        
        // Something crazy
        if(!empty($compared) && !empty($items_db))
        {
            if(!empty($compared['not_exist_in1'])) 
            {
                $delete_items = array_keys($compared['not_exist_in1']);
                $delete_items = implode(',', $delete_items);
            }
            if(!empty($compared['exist_id'])) $update_items = $compared['exist_id'];
        }
        
        // Sort array asc
        $combinations_sorted = $this->sort_array_asc($combinations_tmp);
        
        foreach ($combinations_sorted as $key => $value)
        {
            $combinations_data[$key] = json_encode($value);
        }
        
        // Select user groups
        $user_groups = \SentryAdmin::group()->all('front');
        
        if(!empty($user_groups)) $user_groups= \Model_Base::fetch_pair('id', 'name', array(), false, $user_groups);
        else $user_groups = array();
        
        $price_select = array(
            //0 => '- Select Price Type -',
            //1 => 'Unit Price',
            //2 => 'Tier Price',
            3 => 'Sale Price',
        );
        
        $price_select_fields = array(
            //0 => '- Select Price Type -',
            //1 => 'unit_price',
            //2 => 'tier_price',
            3 => 'sale_price',
        );
        
        // Set default user price type or use selected
        $price_field = 'sale_price';
        if(isset($price_select_fields[\Input::get('special')])) $price_field = $price_select_fields[\Input::get('special')];
        
        $listing = true;
        if(\Input::get('special') == 2 && !\Input::get('tier_price')) $listing = false;
        
        // Product pricing group
        if($item->pricing_group) $pricing_group = $item->pricing_group->id;
        else $pricing_group = false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
        
        // NRB-Gem: removed logic
        // Find and select tier price
        // $tier_price = array();
        // if(\Input::get('user_group') && is_numeric(\Input::get('user_group')) && $pricing_group)
        // {
        //     $tier_price = \Product\Model_Group_Discounts::find(array(
        //         'where' => array(
        //             'user_group_id' => \Input::get('user_group'),
        //             'product_group_id' => $pricing_group,
        //         ),
        //         'order_by' => array(
        //             'discount' => 'asc'
        //         )
        //     ));
        // }
        
        // Reset to empty array if there are no result found by query
		if(is_null($combinations)) $combinations = array();
        if(is_null($combinations_data)) $combinations_data = array();
        $number_of_combinations = count($combinations);
		
        // Pagination per page default
        $per_page = \Input::get('per_page', 'all') == 'all' ? 10 : \Input::get('per_page');
        
        $show_all = $per_page > $number_of_combinations ? true : false ;
        
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($combinations),
			'per_page' => $per_page,
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$combinations = array_slice($combinations, $pagination->offset, $pagination->per_page, true);
        $combinations_data = array_slice($combinations_data, $pagination->offset, $pagination->per_page, true);
         
		\Theme::instance()->set_partial('content', $this->view_dir . 'attributes')
                ->set('product', $product)
                // NRB-Gem: removed logic
                // ->set('user_groups', $user_groups, false)
                ->set('price_select', $price_select, false)
                // NRB-Gem: removed logic
                // ->set('tier_select', $this->tier_price($tier_price), false)
                ->set('combinations', $combinations, false)
                ->set('combinations_data', $combinations_data, false)
                ->set('attribute_groups_select', $attribute_groups_select, false)
                ->set('delete_items', $delete_items, false)
                ->set('update_items', $update_items, false)
                ->set('items_db', $items_db, false)
                ->set('price_field', $price_field)
                ->set('listing', $listing)
                ->set('pagination', $pagination, false)
                ->set('number_of_combinations', $number_of_combinations)
                ->set('show_all', $show_all);
	}
    
    /**
	 * Combos
	 * 
	 * @access  protected
	 * @return  array
	 */
    protected function combos($data, &$all = array(), $group = array(), $val = null, $i = 0)
    {
        if (isset($val))
        {
            if(is_array($val))
            {
                $k = current(array_keys($val));
                $group[$k] = implode(' ', $val);
            }
            else
            {
                array_push($group, $val);
            }
        }

        if ($i >= count($data))
        {
            array_push($all, $group);
        }
        else
        {
            if(isset($data[$i]))
            {
                foreach ($data[$i] as $v)
                {
                    $this->combos($data, $all, $group, $v, $i + 1);
                }
            }
        }

        return $all;
    }
    
     /**
	 * Sort array by array
	 * 
	 * @access  protected
	 * @return  array
	 */
    protected function sort_array($sort_array = array(), $array = array())
    {
        $out = array();
        foreach ($sort_array as $sort_value)
        {
            foreach($array as $key => $arr)
            {
                if(isset($arr[$sort_value])) 
                {
                    $out[$key][$sort_value] = $arr[$sort_value];
                }
            }
        }
        return $out;
    }
    
     /**
	 * Sort array asc
	 * 
	 * @access  protected
	 * @return  array
	 */
    protected function sort_array_asc($array = array())
    {
        foreach ($array as &$value)
        {
            ksort($value);
        }
        return $array;
    }
    
     /**
	 * Compare two arrays
	 * 
	 * @access  protected
	 * @return  array
	 */
    protected function compare_array($array1 = array(), $array2 = array(), $array2_not_sorted = array(), $key_from_array = 1)
    {
        $out = $exist = $exist_id = $not_exist = $not_exist_id = array();
        foreach($array1 as $key1 => $arr1)
        {
            foreach($array2 as $key2 => $arr2)
            {
                if($arr1 == $arr2)
                {
                    $exist[${'key' . $key_from_array}] = $arr2;
                    $exist_id[${'key' . $key_from_array}] = $key2;
                    continue 2;
                }
            }
            $not_exist[$key1] = $arr1;
        }
        
        $out['exist'] = $exist;
        $out['exist_id'] = $exist_id;
        $out['not_exist_in2'] = $not_exist;
        $out['not_exist_in1'] = array();
                    
        if(!empty($exist_id) && !empty($array2_not_sorted))
        {
            $fliped = array_flip($exist_id);
            $out['not_exist_in1'] = array_diff_key($array2_not_sorted, $fliped);
        }
        
        return $out;
    }
    
     /**
	 * Decode attribute json
	 * 
	 * @access  protected
	 * @return  array
	 */
    protected function decode_attribute_json($items_db = null)
    {
        $combinations = array();
        if(!empty($items_db))
        {
            foreach($items_db as $key => $item_db)
            {
                if($item_db->attributes != '')
                {
                    $combinations[$item_db->id] = json_decode($item_db->attributes, true); 
                }
            }
        }
        return $combinations;
    }
    
     /**
	 * Create tier price select
	 * 
	 * @access  protected
	 * @return  array
	 */
    protected function tier_price($tier_price = null)
    {
        $tier_select = array();
        if($tier_price)
        {
            $from = 0;
            $count_tier_price = count($tier_price);
            foreach ($tier_price as $key => $tier)
            {
                if($tier->qty == 0) continue;
                if(($key + 1) >= $count_tier_price) $to = '+';
                else $to = '-' . $tier_price[$key + 1]->qty;
                $tier_select[$tier->id] = $tier->qty . $to;
            }
        }
        return $tier_select;
    }
    
    /****************************** CONTENT IMAGES ******************************/
	
    /**
	 * Upload all contet images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_image($content_type = 'image', $name = false)
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
        if($name != false)
        {
            if($file = \Input::file($name))
            {
                if($file['name'] != '') $return['exists'] = true;
            } 
        }
        else
        {
            foreach(\Input::file() as $file)
            {
                if($file['name'] != '') $return['exists'] = true;
            }
        }
        
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		 
		// Image upload configuration
		$this->image_upload_config = array(
		    'path' => \Config::get('details.' . $content_type . '.location.root'),
		    'normalize' => true,
            'randomize' => true,
            'change_case' => 'lower',
		    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		);
		
		\Upload::process($this->image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
            
			// Save image
		    \Upload::save();
            if($name != false)
            {
                $this->_image_data[0] = \Upload::get_files($name);
            }
            else
            {
                $this->_image_data = \Upload::get_files();
            }
            
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						//$return['is_valid']	= false;
						//$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
    
    
    
    
	/**
	 * Upload all contet images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_image_old($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
       
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		 
		// Image upload configuration
		$this->image_upload_config = array(
		    'path' => \Config::get('details.' . $content_type . '.location.root'),
		    'normalize' => true,
            'randomize' => true,
		    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		);
		
		\Upload::process($this->image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
            
			// Save image
		    \Upload::save();
			$this->_image_data = \Upload::get_files();
            
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						//$return['is_valid']	= false;
						//$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_image($image_id = false, $content_id = false)
	{
		if($image_id && $content_id)
		{
			$images = Model_Attribute_Image::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
            
			if($images)
			{
                
				if(isset($images[$image_id]))
				{
                    
					$image = $images[$image_id];
                    
                    // Check if image used for more than one combination       
                    $image2 = Model_Attribute_Image::find(array(
                        'where' => array(
                            'image' => $image->image,
                        ),
                    ), 'id');
                    
                    if($image2 && count($image2) > 1)
                    {
                        // Reset sort fields
                        \DB::update(Model_Attribute_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
                        $image->delete();
                        \Messages::success('Image was successfully deleted.');
                        \Response::redirect(\Input::referrer());
                    }

					// If there is only one image and image is required
					if(count($images) == 1)
					{
						if(\Config::get('details.image.required', false))
						{
							\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Attribute_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
					else
					{
						if($image->cover == 1)
						{
							\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Attribute_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('Image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Content Image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
            $settings = \Config::load('amazons3.db');
            $hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
                //delete from amazon
                if($hold_amazon_enable)
                {
                    if($content_type == 'image')
                    {
                        \Helper::deleteFileFromAmazonS3('media/images/' . $name, $settings);
                        \Helper::deleteFileFromAmazonS3('media/images/thumbs/' . $name, $settings);
                        \Helper::deleteFileFromAmazonS3('media/images/medium/' . $name, $settings);
                        \Helper::deleteFileFromAmazonS3('media/images/large' . $name, $settings);
                    }
                }
                // EOL - delete from amazon

				@unlink($folder . $name);
			}
		}
	}
    
    public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
			
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Attribute::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
    
    public function action_get_image_box($id = null, $key = null, $attribute_group = 0)
    {
        $item = null;
        if(is_numeric($id) && $id > 0) $item = \Product\Model_Attribute::find_one_by_id($id);
        
        echo \Theme::instance()->view($this->view_dir . '_image_box')
                ->set('item', $item)
                ->set('key', $key)
                ->set('attribute_group', $attribute_group);
        exit;
    }
    
    public function reset_default($id = false)
    {
        // Reset default
        $default = Model_Attribute::find(array(
            'where' => array(
                'product_id' => $id,
                'default' => 1,
            ),
        ));
        if($default)
        {
            foreach ($default as $item)
            {
                $item->default = 0;
                $item->save();
            }
        }
    }
    
    // Common function: Adding attributes to product_attribute_price
    private function add_attribute_price($post, $data)
    {
        $a_products = Model_Product_To_Groups::find_by(array(
            'product_id' => $post['product_id']
        ));
        if ($a_products) {
            foreach($a_products as $o_product) {
                // Insert or update option
                $a_attr = Model_Attribute_Price::find_by(array(
                    'product_attribute_id' => $data['attr_id'], 
                    'pricing_group_id' => $o_product->group_id
                ));
                if($a_attr)
                {
                    $insert_price = $a_attr[0];
                }
                else
                {
                    $insert_price = Model_Attribute_Price::forge();
                }
                //$insert_price->price = $post[$post['price_type'] . '_new'][$data['key']];
                //$insert_price->active = $post['active_new'][$key];
                // NRB-Gem: Set customer groups for this product
                // product_group_options.user_group_id => product_group_options.product_group_id = $o_product->group_id
                $o_group = Model_Group_Options::find_by(array(
                    'product_group_id' => $o_product->group_id,
                    'active' => 1,
                ), null, 1);

                $data = array(
                	'product_attribute_id' 		=> $data['attr_id'],
                	'type' 						=> $post['price_type'],
                	'product_group_discount_id' => $post['product_group_discount_id'],
                	'pricing_group_id' 			=> $o_product->group_id,
                	'user_group_id'				=> ($o_group ? $o_group[0]->user_group_id : null)
                );
                $insert_price->set($data);
                $insert_price->save();
            }
        }
    }
    
    
}
