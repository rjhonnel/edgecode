<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Product;

class Controller_Admin_Group extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/group/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('product::group', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/group/list/property');
	}
	
    public function get_search_items($parent_id = 0, $group_type)
    {
        $group_type = strtolower($group_type);
		$group_type = $group_type && in_array($group_type, array('property', 'pricing')) ? $group_type : 'property';
		
        \View::set_global('group_type', $group_type);
        
         // Override category_id if its a search
        $parent_id = \Input::get('parent_id', $parent_id);
        
        // If we are viewing category products
        // We need to find all products from that and child categories
        if($parent_id)
        {
            $group = \Product\Model_Group::find_one_by_id($parent_id);
            $group and \View::set_global('group', $group);
        }
        
        // Reset $parent_id if its invalid value
        is_numeric($parent_id) or $parent_id = 0;
        
        /************ Start generating query ***********/
		$items = Model_Group::find(function($query) use ($group_type, $parent_id){
			
			$query->where('type', '=', $group_type);
			$query->where('parent_id', '=', $parent_id);

			// Get search filters
			foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
							$query->where($key, 'like', "%$value%");
							break;
						case 'status':
							if(is_numeric($value))
								$query->where($key, $value);
							break;
					}
				}
			}
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        return array('items' => $items, 'pagination' => $pagination);
    }
    
	public function action_list($group_type = false, $parent_id = false)
	{
        \View::set_global('menu', 'admin/product/group/list/pricing');
		\View::set_global('title', 'Pricing groups');
		
        $search = $this->get_search_items($parent_id, $group_type);
		
        $items      = $search['items'];
        $pagination = $search['pagination'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
	
    public function action_subgroups($id = false)
	{
        if(!is_numeric($id)) \Response::redirect('admin/product/group/list');
		
		// Get group to edit
		if(!$item = Model_Group::find_one_by_id($id)) \Response::redirect('admin/product/group/list');
        
		\View::set_global('title', 'List sub groups');
		
        $search = $this->get_search_items($item->id, $item->type);
		
        $items      = $search['items'];
        $pagination = $search['pagination'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list_subgroups')
			->set('group', $item)
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
    
	public function action_create($group_type = false)
	{
		\View::set_global('title', 'Add New Group');
		
		if(\Input::post())
		{
			$val = Model_Group::validate('create');
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				$item = Model_Group::forge($insert);
				
				try{
					$item->save();
					
					\Messages::success('Group successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/product/group/update/' . $item->id) : \Input::referrer(\Uri::admin('current')));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create group</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create group</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/group/list' . ($group_type ? '/'.$group_type : ''));
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/group/list');
		
		// Get group to edit
		if(!$item = Model_Group::find_one_by_id($id)) \Response::redirect('admin/product/group/list');

        \View::set_global('menu', 'admin/product/group/list/pricing');
		\View::set_global('title', 'Edit Pricing Group');
		
		$i_selected_customer_group = 0;
		// Update group details
		if(\Input::post('details', false))
		{
			$i_selected_customer_group = \Input::post('rdo_group');
			foreach(\Input::post('action') as $user_group_id => $value)
            {
                /**
                 * OPTIONS
                 */
                $options[$id]['user_group_id'] = $user_group_id;
                $options[$id]['product_group_id'] = $id;
                $options[$id]['apply_tier_to_sale'] = $_POST['apply_tier_to_sale'][$user_group_id];
                $options[$id]['sale_discount'] = $_POST['sale_discount'][$user_group_id];
                $options[$id]['able_to_view'] = $_POST['able_to_view'][$user_group_id];
                if ($user_group_id == $i_selected_customer_group) 
                {
	                $options[$id]['able_to_buy'] = $_POST['able_to_buy'][$user_group_id];
	                $options[$id]['active'] = 1;

                  // NRB-Gem: Update user_group_id of products under this Pricing Group
                  // used to display correct sale price in admin/product/price/update/{product_id}
                  \DB::update('product_attribute_price')
                    ->value('user_group_id', $user_group_id)
                    ->where('pricing_group_id', '=', $id)
                    ->execute();
                } else 
                {
	                $options[$id]['able_to_buy'] = 0;
                	$options[$id]['active'] = 0;
                }

                // Insert or update option
                $option = Model_Group_Options::find_by(array('user_group_id' => $user_group_id, 'product_group_id' => $id), null, null, 1);
                if($option)
                {
                    $option = $option[0];
                    $option->set($options[$id]);
                }
                else
                {
                    $option = Model_Group_Options::forge($options[$id]);
                }
                
                $option->save();
                
                /**
                 * DISCOUNTS
                 */
                
                // Delete old discounts
                $all_discounts_id = array();
                $all_discounts = Model_Group_Discounts::find_by(array('user_group_id' => $user_group_id, 'product_group_id' => $id), null, null, null);
                //if($all_discounts) foreach($all_discounts as $discount) $discount->delete();
                if($all_discounts) 
                {
                    foreach($all_discounts as $discount)
                    {
                        $all_discounts_id[$discount->id] = $discount;
                    }
                }
                
                // Update
                $discounts = array();
                foreach($_POST['qty'][$user_group_id] as $key => $value)
                {
                    // Ignore discounts with same qty. Only one discount per QTY number is allowed
                    // We will insert first QTY in list. All other will be ignired
                    if(!isset($discounts[$user_group_id][$value]))
                    {
                        if(isset($all_discounts_id[$key])) unset($all_discounts_id[$key]);
                        $discounts[$user_group_id][$value]['id'] = $key;
                        $discounts[$user_group_id][$value]['user_group_id'] = $user_group_id;
                        $discounts[$user_group_id][$value]['product_group_id'] = $id;
                        $discounts[$user_group_id][$value]['qty'] = $value;
                        $discounts[$user_group_id][$value]['discount'] = $_POST['discount'][$user_group_id][$key];
                    }
                }
                
                // Delete
                if($all_discounts_id) foreach ($all_discounts_id as $discount) $discount->delete();
                
                if(!empty($discounts))
                {
                    foreach($discounts[$user_group_id] as $key => $value)
                    {
                        $id = $discounts[$user_group_id][$key]['id'];
                        $discount = Model_Group_Discounts::find_one_by_id($id);
                        if($discount) 
                        {
                            $discount->set($discounts[$user_group_id][$key]);
                            $discount->save();
                        }
                    } 
                }
                
                // Insert
                $new_discounts = array();
                foreach($_POST['new_qty'][$user_group_id] as $key => $value)
                {
                    // Ignore discounts with same qty. Only one discount per QTY number is allowed
                    // We will insert first QTY in list. All other will be ignired
                    if(!isset($discounts[$user_group_id][$value]))
                    {
                        $new_discounts[$user_group_id][$value]['user_group_id'] = $user_group_id;
                        $new_discounts[$user_group_id][$value]['product_group_id'] = $id;
                        $new_discounts[$user_group_id][$value]['qty'] = $value;
                        $new_discounts[$user_group_id][$value]['discount'] = $_POST['new_discount'][$user_group_id][$key];
                    }
                }
                if(!empty($new_discounts))
                {
                    foreach($new_discounts[$user_group_id] as $key => $value)
                    {
                        // Insert discount
                        $discount = Model_Group_Discounts::forge($new_discounts[$user_group_id][$key]);
                        $discount->save();
                    } 
                }
            }//end 

			$val = Model_Group::validate('update');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images))
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload group image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload group image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)))
			{
				/** IMAGES **/
				// Get all alt texts to update if there is no image change
				foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
				{
					if(strpos($image_id, 'new_') === false)
					{
						$item_images[$image_id] = array(
							'id'	=> $image_id,
							'data'	=> array(
								'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
							),
						);
					}
				}
				
				// Save images if new files are submitted
				if(isset($this->_image_data))
				{
					foreach($this->_image_data as $image_data)
					{
						$cover_count = count($item->images);
						if(strpos($image_data['field'], 'new_') === false)
						{
							// Update existing image
							if(str_replace('image_', '', $image_data['field']) != 0)
							{
								$image_id = (int)str_replace('image_', '', $image_data['field']);
								$cover_count--;
				
								$item_images[$image_id] = array(
									'id'	=> $image_id,
									'data'	=> array(
										'content_id'	=> $item->id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
									),
								);
				
								$this->delete_image(\Input::post('image_db_' . $image_id, ''));
							}
						}
						else
						{
							// Save new image
							$image_tmp = str_replace('image_new_', '', $image_data['field']);
								
							$item_images[0] = array(
								'id'	=> 0,
								'data'	=> array(
									'content_id'	=> $item->id,
									'image'			=> $image_data['saved_as'],
									'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
									'cover'			=> $cover_count == 0 ? 1 : 0,
									'sort'			=> $cover_count + 1,
								),
							);
						}
					}
				}
				
				$item_images = isset($item_images) ? $item_images : array();
				
				Model_Group::bind_images($item_images);
				/** END OF IMAGES **/
				
				// Get POST values
				$insert = \Input::post();
				$insert['type'] = 'pricing';
				$item->set($insert);
				
				try{
					$item->save();
					
					\Messages::success('Group successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/group/list/pricing') : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update group</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				// Delete uploaded images if there is product saving error
				if(isset($this->_image_data))
				{
					foreach($this->_image_data as $image_data)
					{
						$this->delete_image($image_data['saved_as']);
					}
				}
				
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update group</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		$group = Model_Group::find_one_by_id($id);

		$products = Model_Group_Options::find(array(
                        'where' => array(
                        	array('product_group_id', '=', $item->id)
                        ),
                        'order_by' => array(
					        'id' => 'asc'
					    )
					));

		$rrp_discounts = Model_Group_Discounts::find_by(array(
                        array('product_group_id', '=', $item->id)
					));

		$cust_groups = \DB::select()->from('groups')->where('is_admin', 0)->order_by('id')->as_object()->execute();

		$a_cust = array();
		$a_rrp_discounts = array();
		foreach($cust_groups as $cust){
			$a_cust[$cust->id] = $cust->name;
			if ($rrp_discounts)
			{
				foreach($rrp_discounts as $disc){
					if($disc->user_group_id == $cust->id){
						$a_rrp_discounts[$cust->id] = $disc->discount;
					}
				}
			}
		}//outer loop

		$total_user_groups = count($a_cust);
		$a_groups = array();
		$a_has = array();
		$i_ctr = 0;
		if($products){
			foreach($products as $product){
				if(isset($a_cust[$product->user_group_id])){
					$a_groups[$i_ctr]['user_group_id'] 		= $product->user_group_id;
					$a_groups[$i_ctr]['user_group'] 		= isset($a_cust[$product->user_group_id]) ? $a_cust[$product->user_group_id] : '';
					$a_groups[$i_ctr]['id'] 				= $product->product_group_id;
					$a_groups[$i_ctr]['active'] 			= $product->active;
					$a_groups[$i_ctr]['able_to_buy'] 		= $product->able_to_buy;
					$a_groups[$i_ctr]['able_to_view'] 		= $product->able_to_view;
					$a_groups[$i_ctr]['sale_discount'] 		= $product->sale_discount;
					$a_groups[$i_ctr]['rrp_discount'] 		= isset($a_rrp_discounts[$product->user_group_id]) ? $a_rrp_discounts[$product->user_group_id] : 0;
					$a_groupd[$i_ctr]['discounts']			= $rrp_discounts;
					$a_groups[$i_ctr]['apply_tier_to_sale']	= $product->apply_tier_to_sale;
					$a_has[] = $product->user_group_id;
					$i_ctr++;
				}
			}//loop
		}//if

		if($total_user_groups > count($a_has)){
			foreach($a_cust as $s_key => $c_group){
				if(!in_array($s_key, $a_has)){
					$a_groups[$i_ctr]['user_group_id'] 		= $s_key;
					$a_groups[$i_ctr]['user_group'] 		= $c_group;
					$a_groups[$i_ctr]['id'] 				= 0;
					$a_groups[$i_ctr]['active'] 			= 0;
					$a_groups[$i_ctr]['able_to_buy'] 		= 0;
					$a_groups[$i_ctr]['able_to_view'] 		= 0;
					$a_groups[$i_ctr]['sale_discount'] 		= 0;
					$a_groups[$i_ctr]['rrp_discount'] 		= 0;
					$a_groupd[$i_ctr]['discounts']			= 0;
					$a_groups[$i_ctr]['apply_tier_to_sale']	= 0;
					$i_ctr++;
				}
			}//loop
		}
		
		// disable customer groups which already belongs to another pricing group
		if($i_selected_customer_group == 0) 
		{
			$assigned_groups = Model_Group_Options::find_by(array(
				'active' => 1,
				array('product_group_id', '!=', $id)
			));

			foreach($assigned_groups as $result) {
				foreach($a_groups as $key => $value){
					if ($a_groups[$key]['user_group_id'] == $result->user_group_id) {
						$a_groups[$key]['disabled'] = true;	
					}
				}
			}
		}

		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('group', $group)->set('a_groups', $a_groups);
	}
	
	public function action_products($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/group/list');
	
		// Get group to edit
		if(!$item = Model_Group::find_one_by_id($id)) \Response::redirect('admin/product/group/list');

        \View::set_global('menu', 'admin/product/group/list/pricing');
		\View::set_global('title', 'Edit Pricing Group Product Assigned');
	
		// Update group products
		if(\Input::post())
		{ 
			$add 	= \Input::post('products.add', array());
			$remove = \Input::post('products.remove', array());
			$update = \Input::post('products.update', array());
       	
			if(\Input::post('add', false))
			{
				foreach($add as $value)
				{
					$related = Model_Product_To_Groups::forge(array(
                        'group_id' 		=> $item->id,
                        'product_id' 	=> $value,
					));
					$related->save();

          // NRB-Gem: Get current Customer Group for this Pricing Group
          $product_group_option = \Product\Model_Group_Options::find_one_by(array(
            'active' => 1,
            'product_group_id' => $id
          ));
          $user_group_id = null;
          if ($product_group_option) {
            $user_group_id = $product_group_option->user_group_id;
          }
          // NRB-Gem: add attributes
          $a_attributes = \Product\Model_Attribute::find(function($query) use ($value){
            $query->where('product_id', '=', $value);
            $query->where('active', '=', 1);
          });
          $a_attribute_ids = array();
          if (count($a_attributes)) {
            foreach($a_attributes as $o_attr) {
              $o_attr_price = new Model_Attribute_Price(array(
                'product_attribute_id' => $o_attr->id,
                'pricing_group_id' => $item->id,
                'user_group_id' => $user_group_id,
                'type' => 'sale_price'
              ));
              $o_attr_price->save();
            }
          }
				}
	
				\Messages::success('Products successfully added to group.');
			}
			else if(\Input::post('remove', false))
			{
				foreach($remove as $value)
				{
					$related = Model_Product_To_Groups::find_one_by(array(
                        array('group_id', '=', $item->id),
                        array('product_id', '=', $value),
					));
	
					if(!is_null($related))
					{
            $related->delete();

            // NRB-Gem: remove attributes
            $a_attributes = \Product\Model_Attribute::find(function($query) use ($value){
              $query->where('product_id', '=', $value);
            });
            $a_attribute_ids = array();
            if (count($a_attributes)) {
              foreach($a_attributes as $o_attr) {
                $a_attribute_ids[] = $o_attr->id;
              }
            }
            if (count($a_attribute_ids)) {
              \DB::delete('product_attribute_price')
                ->where('product_attribute_id', 'IN', \DB::expr('(' . implode(',', $a_attribute_ids) . ')'))
                ->where('pricing_group_id', '=', $item->id)
                ->execute();
            }
					}
				}
	
				\Messages::success('Products successfully removed from group.');
			}

			if(!empty($update))
			{
				foreach($update['discount'] as $key => $value)
				{	
					if(isset($update['attr_id'][$key])){
						foreach($update['attr_id'][$key] as $attribute_id){
              $update_sale_price = Model_Attribute_Price::find_one_by(array(
                array('product_attribute_id', '=', $attribute_id),
                array('pricing_group_id', '=', $item->id)
              ));
							if($update_sale_price)
              {	
                $update_sale_price->set(array(
                	'discount' 			=> $update['discount'][$key][$attribute_id],
                	'able_discount' => $update['able_discount'][$key][$attribute_id],
                	'price' 				=> $update['price'][$key][$attribute_id]
                ));
                $update_sale_price->save();
              }
						}
					} else {
						$prod_attributes = \Product\Model_Attribute::find_by_product_id($key);
         		$attribute_id = $prod_attributes[0]->id;

	              $o_attr_price_list = Model_Attribute_Price::find_one_by(array(
	                array('product_attribute_id' , '=', $attribute_id),
              		array('pricing_group_id', '<>', $item->id)
	              ));
	              if($o_attr_price_list)
	              	$o_attr_price_list->delete();

            $update_sale_price = Model_Attribute_Price::find_one_by(array(
              array('product_attribute_id', '=', $attribute_id),
              array('pricing_group_id', '=', $item->id)
            ));
						if($update_sale_price)
            {
              $update_sale_price->set(array(
              	'discount' 			=> $update['discount'][$key],
              	'able_discount' => $update['able_discount'][$key],
              	'price' 				=> $update['price'][$key]
              ));
              $update_sale_price->save();
            }
					}
				}

				\Messages::success('Products successfully updated.');
			}
	
			// if(\Input::is_ajax())
			// {
			// 	echo \Messages::display('left', false);
			// 	exit;
			// }
			// else
			// {
				\Response::redirect(\Input::referrer(\Uri::admin('current')));
			// }
		}
	
		$group = Model_Group::find_one_by_id($id);
        
	
		/************ Get non related infotabs ***********/
		$items = Model_Product::find(function($query) use ($item){
				
			$product_ids = array();
			
      		// NRB-Gem: Comment out, to enable multiple pricing groups for a product
			// Products can be in only ONE discounted group
			// But can exist in MULTIPLE standard groups
			// if($item->type == 'pricing')
			// {
			// 	$discounted_products = Model_Product::in_group_type('pricing');
			// 	foreach($discounted_products as $product)
			// 		array_push($product_ids, $product->id);
			// }
				
			foreach($item->products as $product)
				array_push($product_ids, $product->id);
	
			if(!empty($product_ids))
			{
				$product_ids = '(' . implode(',', $product_ids) . ')';
				$query->where('id', 'NOT IN', \DB::expr($product_ids));
			}
            
            // Filters
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title':
                        	$query->where_open()
                            	->where($key, 'like', "%$value%")
                                ->or_where('code', 'like', "%$value%")
                                ->where_close();
                            break;
	                        case 'status':
	                            if(is_numeric($value))
								$query->where($key, $value);
							break;
                        case 'category_id':
                            if(is_numeric($value))
                            {
                                $products = Model_Product_To_Categories::find(function($query) use ($value)
                                {
                                    return $query->where('category_id', $value);
                                }, 'product_id');
                                
                                if(empty($products)) $products = array(0);
                                    
                                $query->where('id', 'IN', array_keys($products));
                            }
                            break;
                    }
                }
            }
				
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
	
		$group->not_related_products = null;
        $not_related_products = $items ? $items : array();
    // product_group_options
    $customer_groups = Model_Group_Options::find_by(array('active' => 1, 'product_group_id' => $id));
    $customer_group = array();
    if ($customer_groups) {
      $customer_group = Model_Customer_Group::find_one_by_id($customer_groups[0]->user_group_id);
    }

        // Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
	
		\Theme::instance()->set_partial('content', $this->view_dir . 'products')
                ->set('group', $group, false)
                ->set('items', $items, false)
                ->set('pagination', $pagination, false)
                ->set('status', $status, false)
                ->set('customer_group', $customer_group, false);
	}
	
	public function action_products_assigned($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/group/list');
	
		// Get group to edit
		if(!$item = Model_Group::find_one_by_id($id)) \Response::redirect('admin/product/group/list');
        \View::set_global('menu', 'admin/product/group/list/pricing');
		\View::set_global('title', 'Edit Group Products');

		// Update group products
		if(\Input::post())
		{ 
			$add 	= \Input::post('products.add', array());
			$remove = \Input::post('products.remove', array());
			$update = \Input::post('products.update', array());

			if(\Input::post('add', false))
			{
				foreach($add as $value)
				{
					$related = Model_Product_To_Groups::forge(array(
                        'group_id' 		=> $item->id,
                        'product_id' 	=> $value,
					));
					$related->save();

			          // NRB-Gem: Get current Customer Group for this Pricing Group
			          $product_group_option = \Product\Model_Group_Options::find_one_by(array(
			            'active' => 1,
			            'product_group_id' => $id
			          ));
			          $user_group_id = null;
			          if ($product_group_option) {
			            $user_group_id = $product_group_option->user_group_id;
			          }
			          // NRB-Gem: add attributes
			          $a_attributes = \Product\Model_Attribute::find(function($query) use ($value){
			            $query->where('product_id', '=', $value);
			            $query->where('active', '=', 1);
			          });
			          $a_attribute_ids = array();
			          if (count($a_attributes)) {
			            foreach($a_attributes as $o_attr) {
			              $o_attr_price = new Model_Attribute_Price(array(
			                'product_attribute_id' => $o_attr->id,
			                'pricing_group_id' => $item->id,
			                'user_group_id' => $user_group_id,
			                'type' => 'sale_price'
			              ));
			              $o_attr_price->save();
			            }
			          }
				}
	
				\Messages::success('Products successfully added to group.');
			}
			else if(\Input::post('remove', false))
			{
				foreach($remove as $value)
				{
					$related = Model_Product_To_Groups::find_one_by(array(
                        array('group_id', '=', $item->id),
                        array('product_id', '=', $value),
					));
	
					if(!is_null($related))
					{
            			// $related->delete();
            			// replace to bulk delete so that it will remove if there are duplicate values
            			\DB::delete('product_to_groups')
	            			->where('group_id', '=', $item->id)
	            			->where('product_id', '=', $value)
	            			->execute();

			            // NRB-Gem: remove attributes
			            $a_attributes = \Product\Model_Attribute::find(function($query) use ($value){
			              $query->where('product_id', '=', $value);
			            });
			            $a_attribute_ids = array();
			            if (count($a_attributes)) {
			              foreach($a_attributes as $o_attr) {
			                $a_attribute_ids[] = $o_attr->id;
			              }
			            }
			            if (count($a_attribute_ids)) {
			              \DB::delete('product_attribute_price')
			                ->where('product_attribute_id', 'IN', \DB::expr('(' . implode(',', $a_attribute_ids) . ')'))
			                ->where('pricing_group_id', '=', $item->id)
			                ->execute();
			            }
					}
				}
	
				\Messages::success('Products successfully removed from group.');
			}

			if(!empty($update))
			{
				foreach($update['discount'] as $key => $value)
				{	
					if(isset($update['attr_id'][$key])){
						foreach($update['attr_id'][$key] as $attribute_id){
			              $update_sale_price = Model_Attribute_Price::find_one_by(array(
			                array('product_attribute_id', '=', $attribute_id),
			                array('pricing_group_id', '=', $item->id)
			              ));
										if($update_sale_price)
			              {	
			                $update_sale_price->set(array(
			                	'discount' 			=> $update['discount'][$key][$attribute_id],
			                	'able_discount' => $update['able_discount'][$key][$attribute_id],
			                	'price' 				=> $update['price'][$key][$attribute_id]
			                ));
			                $update_sale_price->save();
			              }
						}
					} else {
						$prod_attributes = \Product\Model_Attribute::find_by_product_id($key);
		         		$attribute_id = $prod_attributes[0]->id;

			              // $o_attr_price_list = Model_Attribute_Price::find_one_by(array(
			              //   array('product_attribute_id' , '=', $attribute_id),
		              	// 	array('pricing_group_id', '<>', $item->id)
			              // ));
			              // if($o_attr_price_list)
			              // 	$o_attr_price_list->delete();

			            $update_sale_price = Model_Attribute_Price::find_one_by(array(
			              array('product_attribute_id', '=', $attribute_id),
			              array('pricing_group_id', '=', $item->id)
			            ));
						if($update_sale_price)
			            {
			              $update_sale_price->set(array(
			              	'discount' 			=> $update['discount'][$key],
			              	'able_discount' => $update['able_discount'][$key],
			              	'price' 				=> $update['price'][$key]
			              ));
			              $update_sale_price->save();
			            }
					}
				}

				\Messages::success('Products successfully updated.');
			}
	
			// if(\Input::is_ajax())
			// {
			// 	echo \Messages::display('left', false);
			// 	exit;
			// }
			// else
			// {
				\Response::redirect(\Input::referrer(\Uri::admin('current')));
			// }
		}
	
		$group = Model_Group::find_one_by_id($id);
        
	
		/************ Get non related infotabs ***********/
		$group_products  = Model_Product::find(function($query) use ($group){
				
			$product_ids = array();
			
      		// NRB-Gem: Comment out, to enable multiple pricing groups for a product
			// Products can be in only ONE discounted group
			// But can exist in MULTIPLE standard groups
			// if($item->type == 'pricing')
			// {
			// 	$discounted_products = Model_Product::in_group_type('pricing');
			// 	foreach($discounted_products as $product)
			// 		array_push($product_ids, $product->id);
			// }
			if(isset($group->products))
			{
				foreach($group->products as $product)
					array_push($product_ids, $product->id);
			}
	
			if(!empty($product_ids))
			{
				$product_ids = '(' . implode(',', $product_ids) . ')';
				$query->where('id', 'IN', \DB::expr($product_ids));
			}
            
            // Filters
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title':
                        	$query->where_open()
	                            ->where($key, 'like', "%$value%")
	                            ->or_where('code', 'like', "%$value%")
	                            ->where_close();
                            break;
                        case 'status':
                            if(is_numeric($value))
								$query->where($key, $value);
							break;
                        case 'category_id':
                            if(is_numeric($value))
                            {
                                $products = Model_Product_To_Categories::find(function($query) use ($value)
                                {
                                    return $query->where('category_id', $value);
                                }, 'product_id');
                                
                                if(empty($products)) $products = array(0);
                                    
                                $query->where('id', 'IN', array_keys($products));
                            }
                            break;
                    }
                }
            }
				
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
	
		$group->not_related_products = null;
        // $not_related_products = $items ? $items : array();
    // product_group_options
    $customer_groups = Model_Group_Options::find_by(array('active' => 1, 'product_group_id' => $id));
    $customer_group = array();
    if ($customer_groups) {
      $customer_group = Model_Customer_Group::find_one_by_id($customer_groups[0]->user_group_id);
    }

        // Reset to empty array if there are no result found by query
		// if(is_null($items)) $items = array();
		
		// Initiate pagination
		// $pagination = \Hybrid\Pagination::make(array(
		// 	'total_items' => count($items),
		// 	'per_page' => \Input::get('per_page', 10),
		// 	'uri_segment' => null,
		// ));

		// Remove unwanted items, and show only required ones
		// $items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
	
		// Initiate pagination
		$group_pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($group_products),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));
		// Remove unwanted items, and show only required ones
		$group_list = $group_products?array_slice($group_products, $group_pagination->offset, $group_pagination->per_page):false;
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'products_assigned')
                ->set('group', $group, false)
                ->set('group_list', $group_list, false)
                ->set('group_pagination', $group_pagination, false)
                // ->set('items', $items, false)
                // ->set('pagination', $pagination, false)
                ->set('status', $status, false)
                ->set('customer_group', $customer_group, false);
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
	
		$items = \Input::post('sort');
	
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
				
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
	
				$model = Model_Group::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);
	
					$item->set(array(
						'cover'	=> ($key == 0 ? 1 : 0),
						'sort'	=> ++$start,
					));
						
					$item->save();
				}
	
				\Messages::success('Items successfully reordered.');
	
				echo \Messages::display('left', false);
			}
		}
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Group::find_one_by_id($id))
			{
				// Delete item
				try{
					// Delete all relations of this group with products
					$groups = Model_Product_To_Groups::find_by_group_id($item->id);
					if(!empty($groups))
					{
						foreach($groups as $group)
						{
							$group->delete();
						}
					}
					
					$groups = Model_Group_Options::find_by_product_group_id($item->id);
					if(!empty($groups))
					{
						foreach($groups as $group)
						{
							$group->delete();
						}
					}
					
					if(!empty($item->images))
					{
						foreach($item->images as $image)
						{
							$this->delete_image($image->image);
							$image->delete();
						}
					}
					
					// Delete group
					$item->delete();
					
					\Messages::success('Group successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete group</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/****************************** CONTENT IMAGES ******************************/
	
	/**
	 * Upload all contet images to local directory defined in $this->image_upload_config
	 *
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 *
	 */
	public function upload_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
	
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
	
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
	
		// Image upload configuration
		$this->image_upload_config = array(
			'path' => \Config::get('details.' . $content_type . '.location.root'),
			'normalize' => true,
			'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		);
	
		\Upload::process($this->image_upload_config);
	
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save image
			\Upload::save();
			$this->_image_data = \Upload::get_files();
				
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
						
					foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
	
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
	
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 *
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_image($image_id = false, $content_id = false)
	{
		if($image_id && $content_id)
		{
			$images = Model_Group_Image::find(array(
				'where' => array(
					'content_id' => $content_id,
				),
				'order_by' => array('sort' => 'asc'),
			), 'id');
				
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
						
					// If there is only one image and image is required
					if(count($images) == 1)
					{
						if(\Config::get('details.image.required', false))
						{
							\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Group_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
					else
					{
						if($image->cover == 1)
						{
							\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Group_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('Image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Group Image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
	
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete image from file system
	 *
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 *
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
				@unlink($folder . $name);
			}
		}
	}
}
