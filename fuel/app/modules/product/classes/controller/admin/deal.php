<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Product;

class Controller_Admin_Deal extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/deals/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('product::product', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		throw new \HttpNotFoundException;
	}

	public function action_list($product_id = false)
	{
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Deals');

		if($product = Model_Product::find_one_by_id($product_id))
		{
			/************ Start generating query ***********/
			$items = Model_Deals::find(function($query) use ($product_id) {
				$query->where('product_id', $product_id);
				// Order query
				$query->order_by('sort', 'asc');
				$query->order_by('id', 'asc');
			});
			/************ End generating query ***********/
		
			// Reset to empty array if there are no result found by query
			if(is_null($items)) $items = array();
			
			// Initiate pagination
			$pagination = \Hybrid\Pagination::make(array(
				'total_items' => count($items),
				'per_page' => \Input::get('per_page', 10),
				'uri_segment' => null,
			));

			// Remove unwanted items, and show only required ones
			$items = array_slice($items, $pagination->offset, $pagination->per_page);

			\Theme::instance()->set_partial('content', $this->view_dir . 'list')
						->set('product', $product)
						->set('items', $items)
						->set('pagination', $pagination, false);
		}
		else
			\Response::redirect('admin/product/list');
	}


	public function action_create($product_id = false)
	{
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Add New Deal');
		
		if($product = Model_Product::find_one_by_id($product_id))
		{
			if(\Input::post())
			{
				$val = Model_Deals::validate('create');
				
				// Get POST values
				$insert = \Input::post();
				
				$insert['product_id'] = $product_id;
				
				if($val->run($insert))
				{
					$insert['active_from'] 	= !empty($insert['active_from']) ? \Helper::dmyToymd($insert['active_from'], '/') : NULL;
					$insert['active_to'] 	= !empty($insert['active_to']) ? \Helper::dmyToymd($insert['active_to'], '/') : NULL;
					if($insert['status'] != 2)
					{
						unset($insert['active_from']);
						unset($insert['active_to']);
					}

					$item_deals = Model_Deals::forge($insert);
					
					try{
						$item_deals->save();
						
						\Messages::success('Deal successfully added.');
						\Response::redirect('admin/product/deal/list/'.$product_id);
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create deal</strong>');
					}
				}
				else
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create deal</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}

			\Theme::instance()->set_partial('content', $this->view_dir . 'create')
						->set('product', $product);
		}
		else
		{
			\Messages::error('Product not found');
			\Response::redirect('admin/product/list');
		}
	}

	public function action_update($product_id = false, $deal_id = false)
	{
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Edit Deal');
		
		if($product = Model_Product::find_one_by_id($product_id))
		{
			if($deal = Model_Deals::find_one_by_id($deal_id))
			{
				if(\Input::post())
				{
					$val = Model_Deals::validate('create');
					
					// Get POST values
					$insert = \Input::post();
					
					$insert['product_id'] = $product_id;
					
					if($val->run($insert))
					{
						$insert['active_from'] 	= !empty($insert['active_from']) ? \Helper::dmyToymd($insert['active_from'], '/') : NULL;
						$insert['active_to'] 	= !empty($insert['active_to']) ? \Helper::dmyToymd($insert['active_to'], '/') : NULL;
						if($insert['status'] != 2)
						{
							unset($insert['active_from']);
							unset($insert['active_to']);
						}

						$deal->set($insert);
						
						try{
							$deal->save();
							
							\Messages::success('Deal successfully added.');
							\Response::redirect('admin/product/deal/list/'.$product_id.'/'.$deal_id);
						}
						catch (\Database_Exception $e)
						{
							// show validation errors
							\Messages::error('<strong>There was an error while trying to create deal</strong>');
						}
					}
					else
					{
						if($val->error() != array())
						{
							// show validation errors
							\Messages::error('<strong>There was an error while trying to create deal</strong>');
							foreach($val->error() as $e)
							{
								\Messages::error($e->get_message());
							}
						}
					}
				}

				\Theme::instance()->set_partial('content', $this->view_dir . 'update')
							->set('product', $product)
							->set('deal', $deal);
			}
			else
			{
				\Messages::error('Deal not found');
				\Response::redirect('admin/product/deal/list/'.$product_id);
			}
		}
		else
		{
			\Messages::error('Product not found');
			\Response::redirect('admin/product/list');
		}
	}

	public function action_delete($product_id = false, $deal_id = false)
	{
        \View::set_global('menu', 'admin/product/list');
		\View::set_global('title', 'Edit Deal');
		
		if($product = Model_Product::find_one_by_id($product_id))
		{
			if($deal = Model_Deals::find_one_by_id($deal_id))
			{
				try{
					$deal->delete();
					
					\Messages::success('Deal successfully deleted.');
					\Response::redirect('admin/product/deal/list/'.$product_id);
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete accordion</strong>');
				}
			}
			else
			{
				\Messages::error('Deal not found');
				\Response::redirect('admin/product/deal/list/'.$product_id);
			}
		}
		else
		{
			\Messages::error('Product not found');
			\Response::redirect('admin/product/list');
		}
	}
}