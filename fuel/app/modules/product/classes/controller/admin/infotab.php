<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Product;

class Controller_Admin_Infotab extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/infotab/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('product::infotab', 'infotab');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('infotab.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * Parse info tab content
	 * 
	 * @param $table = Table to parse
	 */
	public function action_parse_info_table($table = false)
	{
		if(\Input::is_ajax())
		{
			$table = \Input::post('table', '');
			echo \Helper::parse_info_table($table);
		}
		
		if(\Request::is_hmvc())
		{
			echo \Helper::parse_info_table($table);
		}
		
	}
	
	/**
	 * Parse ifno table content
	 * 
	 * @param $table = Table to parse
	 */
	public function parse_info_table($table)
	{
		$table = trim($table, "\n\r ");
		$items = array();
		
		if(!empty($table))
		{
			$items = preg_split ('/$\R?^/m', $table);
			
			$table_counter 	= 0;
			$row_counter 	= 0;
			$tmp_tables 	= array();
			$maximum 		= array();
			
			// Parse table line by line and fill tables and regular content
			foreach($items as $key => $item)
			{
				$item = trim($item);
				
				if(trim($item, '=') == 'table')
				{
					if($table_counter%2 == 0)
					{
						// Table open
						$items[$key] = 'table_'.$table_counter;
						$table_counter++;
					}
					else
					{
						// Table close
						$table_counter++;
						unset($items[$key]);
					}
				}
				else
				{
					if($table_counter%2 == 1)
					{
						// Table fill
						if(isset($item[0]) && $item[0] == '#') 
						{
							// Fill table headers
							if(!isset($tmp_tables[$table_counter-1]['tbody']))
								$tmp_tables[$table_counter-1]['thead'][$row_counter][] = substr($item, 1);
							else	
								$tmp_tables[$table_counter-1]['tbody'][$row_counter][] = substr($item, 1);
						}
						elseif($item != '')
						{
							// Fill table rows
							$tmp_tables[$table_counter-1]['tbody'][$row_counter][] = $item;
						}
						else
						{
							// Empty row means we have new table row
							$row_counter++;
						}
						
						unset($items[$key]);
						
					}
					else
					{
						// Content fill
						// Content is already copied as it is, so we dont need to do anything here
					}
				}
				
			}
			
			// If we have any tables in our content we need to generate their HTML
			if(!empty($tmp_tables))
			{
				// Get maximum number of rows in one column (as table is simetric)
				foreach($tmp_tables as $key => $tmp_table)
				{
					$maximum[$key] = 0;
					
					foreach($tmp_table as $tmp_part)
						foreach($tmp_part as $tmp_row)
							if(count($tmp_row) > $maximum[$key]) $maximum[$key] = count($tmp_row);
				}
				
				// Set all other columns to have same number of rows as maximum
				foreach($tmp_tables as $key => $tmp_table)
				{
					foreach($tmp_table as $key1 => $tmp_part)
						foreach($tmp_part as $key2 => $tmp_row)
							if(count($tmp_row) < $maximum[$key])
							{
								for($i = 0; $i < $maximum[$key] - count($tmp_row); $i++)
								{
									$tmp_tables[$key][$key1][$key2][] = '';
								}
							}
				}
				
				// Generate table HTML
				foreach($tmp_tables as $key => $tmp_table)
				{
					$tables[$key] = '<table class="table table-bordered table-striped table-condensed">';
						
						if(isset($tmp_table['thead']))
						{
							$tables[$key] .= '<thead>';
							
							foreach($tmp_table['thead'] as $tmp_thead)
							{
								$tables[$key] .= '<tr>';
								foreach($tmp_thead as $tmp_column)
								{
									$tables[$key] .= '<th>' . $tmp_column . '</th>';
								}
								$tables[$key] .= '</tr>';
							}
							
							$tables[$key] .= '</thead>';
						}
						
						if(isset($tmp_table['tbody']))
						{
							$tables[$key] .= '<tbody>';
							
							foreach($tmp_table['tbody'] as $tmp_tbody)
							{
								$tables[$key] .= '<tr>';
								foreach($tmp_tbody as $tmp_column)
								{
									$tables[$key] .= '<td>' . $tmp_column . '</td>';
								}
								$tables[$key] .= '</tr>';
							}
							
							$tables[$key] .= '</tbody>';
						}
						
					$tables[$key] .= '</table>';
				}
				
				// Return table HTMLs inside original array
				foreach($tables as $key => $table)
				{
					foreach($items as $key1 => $item)
					{
						if($item == 'table_'.$key)
							$items[$key1] = $table;
					}
				}
			}
			
			// Recalculate array keys
			$items = array_values($items);
		}
		
		return !empty($items) ? implode('<br />', $items) : '';
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/infotab/list');
	}
	
	public function action_list()
	{
        \View::set_global('menu', 'admin/product/infotab/list');
		\View::set_global('title', 'List Infotabs');
		
		/************ Start generating query ***********/
		$items = Model_Infotab::find(function($query){
			
			// Get search filters
			foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
							$query->where($key, 'like', "%$value%");
							break;
						case 'status':
							if(is_numeric($value))
								$query->where($key, $value);
							break;
					}
				}
			}
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false);
	}
	
	public function action_video()
	{
		$url = \Input::post('url', false);
		$return['response'] = false;
		
		if($url)
		{
			$youtube = \App\Youtube::forge();
			
			$video = $youtube->parse($url)->get();
			
			if(isset($video) && $video)
			{
				$return['response'] = $video;
				\Messages::success('Video found! Please check found data');
			}
			else
			{
				\Messages::error('Please check video URL and try again.');
			}
		}
		else
		{
			\Messages::info('Please insert video URL');
		}
		
		$return['message'] 	= \Messages::display('left', false);
		
		echo json_encode($return); exit;
	}
	
	public function action_create()
	{
		\View::set_global('title', 'Add New Info Tab');
		
		if(\Input::post())
		{
			$val = Model_Infotab::validate('create');
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				$item = Model_Infotab::forge($insert);
				
				try{
					$item->save();
					
					\Messages::success('Info Tab successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/product/infotab/update/' . $item->id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create info tab</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create info tab</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/infotab/list');
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/infotab/list');

        \View::set_global('menu', 'admin/product/infotab/list');
		// Get news item to edit
		if(!$item = Model_Infotab::find_one_by_id($id)) \Response::redirect('admin/product/infotab/list');
		
		\View::set_global('title', 'Edit Info Tab');
		
		if(\Input::post())
		{
			$val = Model_Infotab::validate('update');
			
			if($val->run())
			{
				// Get POST values
				$insert = \Input::post();
				
				$item->set($insert);
				
				try{
					$item->save();
					
					\Messages::success('Info Tab successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/infotab/list/') : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update info tab</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update info tab</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		$infotab = Model_Infotab::find_one_by_id($id);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('infotab', $infotab);
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Infotab::find_one_by_id($id))
			{
				// Delete item
				try{
					// Delete all relations of this infotab with products
					$infotabs = Model_Product_To_Infotabs::find_by_infotab_id($item->id);
					if(!empty($infotabs))
					{
						foreach($infotabs as $infotab)
						{
							\Request::forge('admin/product/infotab_delete/' . $infotab->unique_id)->execute()->response();
						}
					}
					
					// Delete infotab
					$item->delete();
					
					\Messages::success('Info Tab successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete info tab</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
			
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Infotab::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'cover'	=> ($key == 0 ? 1 : 0),
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
	
	/****************************** CONTENT IMAGES ******************************/
	
	/**
	 * Delete content image
	 * 
	 * @param $content_id	= Content ID
	 */
	public function action_delete_image($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Product_To_Infotabs::find_by_pk($id))
			{
				// Delete hotspot images
				if(!empty($item->images))
				{
					foreach($item->images as $image)
					{
						\Request::forge('admin/product/delete_infotab_image/' . $image->id . '/' . $image->infotab_id)->execute()->response();
					}
				}
					
				// Delete item
				try{
					$this->delete_image($item->image);
				
    				$item->set(array(
    					'image' => '',
    					'alt_text' => '',
    					'active' => 0,
    				));
				
					$item->save();
					
					\Messages::success('Info Tab image successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete info tab image</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		if(\Request::is_hmvc())
		{
			\Messages::reset();
		}
		else
		{
			\Response::redirect(\Input::referrer(\Uri::create('admin/product/infotab/list')));
		}
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			foreach(\Config::get('infotab.' . $content_type . '.location.folders', array()) as $folder)
			{
				@unlink($folder . $name);
			}
		}
	}
	
}
