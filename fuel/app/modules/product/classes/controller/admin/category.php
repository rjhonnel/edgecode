<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Product;

class Controller_Admin_Category extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/category/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('product::category', 'details');
		\Config::load('product::infotab', 'infotab');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/category/list');
	}
	
    public function get_search_items($category_id = 0)
    {
        // Override category_id if its a search
        $category_id = \Input::get('category_id', $category_id);
        
        // Reset $parent_id if its invalid value
        is_numeric($category_id) or $category_id = 0;
        
        if($category_id)
        {
            $category = \Product\Model_Category::find_one_by_id($category_id);
            if($category)
            {
                \View::set_global('category', $category);
            }
        }
        
        /************ Start generating query ***********/
		$items = Model_Category::find(function($query) use ($category_id){
			
			// Select only root pages
			$query->where('parent_id', $category_id);
			
			// Get search filters
			foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
							$query->where($key, 'like', "%$value%");
							break;
						case 'status':
							if(is_numeric($value))
								$query->where($key, $value);
							break;
						case 'active_from':
                            $date = strtotime($value);
                            if($date)
                                $query->where($key, '>=', $date);
							break;
						case 'active_to':
                            $date = strtotime($value);
                            if($date)
                                $query->where($key, '<=', $date);
							break;
					}
				}
			}
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
        
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
        
        return array('items' => $items, 'pagination' => $pagination, 'status' => $status);
    }
    
	public function action_list()
	{
        \View::set_global('menu', 'admin/product/category/list');
		\View::set_global('title', 'List Categories');
		
		$search = $this->get_search_items('root');
		
        $items      = $search['items'];
        $pagination = $search['pagination'];
        $status     = $search['status'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false)
			->set('status', $status);
	}
	
	public function action_video()
	{
		$url = \Input::post('url', false);
		$return['response'] = false;
		
		if($url)
		{
			$youtube = \App\Youtube::forge();
			
			$video = $youtube->parse($url)->get();
			
			if(isset($video) && $video)
			{
				$return['response'] = $video;
				\Messages::success('Video found! Please check found data');
			}
			else
			{
				\Messages::error('Please check video URL and try again.');
			}
		}
		else
		{
			\Messages::info('Please insert video URL');
		}
		
		$return['message'] 	= \Messages::display('left', false);
		
		echo json_encode($return); exit;
	}
	
	public function action_create()
	{
		$groups = \SentryAdmin::group()->all('front');

        \View::set_global('menu', 'admin/product/category/list');
		\View::set_global('title', 'Add New Category');
		
		if(\Input::post())
		{
			$val = Model_Category::validate('create');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->image))
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload category image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload category image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false)))
			{
				// Get POST values
				$insert = \Input::post();
				
				// Prepare some values
				$insert['active_from'] 	= !empty($insert['active_from']) ? \Helper::dmyToymd($insert['active_from'], '/') : NULL;
				$insert['active_to'] 	= !empty($insert['active_to']) ? \Helper::dmyToymd($insert['active_to'], '/') : NULL;
				if($insert['status'] != 2)
				{
					unset($insert['active_from']);
					unset($insert['active_to']);
				}

				if(isset($insert['user_group'])){
					$insert['user_group'] = json_encode((object)$insert['user_group']);
				}

				$item = Model_Category::forge($insert);
				
				try{
					$item->save();
					
					// Validate and insert category slug into SEO database table
					$val_seo = Model_Category_Seo::validate('create_seo');
					$insert_seo = array(
						'content_id' 	=> $item->id,
						'slug'			=> \Inflector::friendly_title($item->title, '-', true),
					);
					
					while(!$val_seo->run($insert_seo))
					{
						$insert_seo['slug'] = \Str::increment($insert_seo['slug'], 1, '-');
					}
					
					$item_seo = Model_Category_Seo::forge($insert_seo);
					$item_seo->save();
					// END OF: SEO
					
					// Insert category images
					if($this->_image_data)
					{
						$item_image = array(
							array(
								'id'	=> 0,
								'data'	=> array(
									'content_id'	=> $item->id,
									'image'			=> $this->_image_data[0]['saved_as'],
									'alt_text'		=> \Input::post('alt_text', ''),
									'cover'			=> 1,
									'sort'			=> 1,
								),
							),
						);
						
						Model_Category::bind_images($item_image);

						//save to amazon
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						if($hold_amazon_enable)
						{
							$folder_list = array('', 'large/', 'medium/', 'thumbs/');
							foreach ($folder_list as $folder)
							{
		                        $file_info = array(
		                                'savePath' => 'media/images/'.$folder.$this->_image_data[0]['saved_as'],
		                                'filePath' => $this->_image_data[0]['saved_to'].$folder.$this->_image_data[0]['saved_as'],
		                                'contentType' => mime_content_type($this->_image_data[0]['saved_to'].$folder.$this->_image_data[0]['saved_as']),
		                            );
								\Helper::uploadFileToAmazonS3($file_info, $settings);
								@unlink($this->_image_data[0]['saved_to'].$folder.$this->_image_data[0]['saved_as']);
							}
						}
						// EOL - save to amazon
					}
					
					\Messages::success('Category successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/product/category/update/' . $item->id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create category</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create category</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
			
			// Delete uploaded image if there is category saving error
			if(isset($this->_image_data))
				$this->delete_image($this->_image_data[0]['saved_as']);
		}
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'create')->set('groups', $groups);
	}
	
	public function action_update($id = false)
	{
		$groups = \SentryAdmin::group()->all('front');

        \View::set_global('menu', 'admin/product/category/list');

		if(!is_numeric($id)) \Response::redirect('admin/product/category/list');
		
		// Get news item to edit
		if(!$item = Model_Category::find_one_by_id($id)) \Response::redirect('admin/product/category/list');
		
		\View::set_global('title', 'Edit Category');
		
		if(\Input::post())
		{
			if(\Input::post('file_upload', false))
			{
				// Upload files and display errors if there are any
				$file = $this->upload_file();
				if(!$file['exists'] && \Config::get('details.image.required', false) && empty($item->images))
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload content file</strong>');
					\Messages::error('You have to select image');
				}
				elseif($file['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload content file</strong>');
					foreach($file['errors'] as $error) \Messages::error($error);
				}
				
				if($file['is_valid'] && !(!$file['exists'] && \Config::get('details.file.required', false) && empty($item->files)))
				{
					/** FILES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'title_') as $file_id => $title)
					{
						if(strpos($file_id, 'new_') === false)
						{
							$item_files[$file_id] = array(
								'id'	=> $file_id,
								'data'	=> array(
									'title'		=> \Input::post('title_' . $file_id, ''),
								),
							);
						}
					}
					
					// Save files if new ones are submitted
					if(isset($this->_file_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_file_data as $file_data)
						{
							$cover_count = count($item->files);
							if(strpos($file_data['field'], 'new_') === false)
							{
								// Update existing file
								if(str_replace('file_', '', $file_data['field']) != 0)
								{
									$file_id = (int)str_replace('file_', '', $file_data['field']);
									$cover_count--;
									
									$item_files[$file_id] = array(
										'id'	=> $file_id,
										'data'	=> array(
											'content_id'	=> $item->id,
											'file'			=> $file_data['saved_as'],
											'title'			=> \Input::post('title_' . $file_id, ''),
										),
									);
									
									$this->delete_file(\Input::post('file_db_' . $file_id, ''));
								}
							}
							else
							{
								// Save new file
								$file_tmp = str_replace('file_new_', '', $file_data['field']);
								
								$item_files[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'content_id'	=> $item->id,
										'file'			=> $file_data['saved_as'],
										'title'			=> \Input::post('title_new_' . $file_tmp, ''),
										'cover'			=> $cover_count == 0 ? 1 : 0,
										'sort'			=> $cover_count + 1,
									),
								);
							}

							//save to amazon
							if($hold_amazon_enable)
							{
			                    $file_info = array(
			                            'savePath' => 'media/files/'.$file_data['saved_as'],
			                            'filePath' => $file_data['saved_to'].$file_data['saved_as'],
			                            'contentType' => mime_content_type($file_data['saved_to'].$file_data['saved_as']),
			                        );
								\Helper::uploadFileToAmazonS3($file_info, $settings);
								@unlink($file_data['saved_to'].$file_data['saved_as']);
							}
							// EOL - save to amazon
						}
					}
					
					Model_Category::bind_files($item_files);
					
					\Messages::success('Category documents successfully updated.');
					\Response::redirect(\Uri::admin('current'));
					/** END OF FILES **/
				}
				else
				{
					// Delete uploaded files if there is product saving error
					if(isset($this->_file_data))
					{
						foreach($this->_file_data as $file_data)
						{
							$this->delete_file($file_data['saved_as']);
						}
					}
							
					\Messages::error('<strong>There was an error while trying to update category documents</strong>');
				}
			}
						
			else if(\Input::post('video_upload', false))
			{
				// Upload image and display errors if there are any
				$image = $this->upload_image('video');
				if($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload content image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if($image['is_valid'] && !(\Config::get('details.video.required', false) && empty($item->videos)))
				{
					/** VIDEOS **/
					// Get all video urls to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'video_url_') as $video_id => $url)
					{
						$not_updated[] = $video_id;
						
						if(strpos($video_id, 'new_') === false)
						{
							$video_url = \Input::post('video_url_' . $video_id, false);
							
							if($video_url)
							{
								// Check video
								$youtube = \App\Youtube::forge();
								$video = $youtube->parse(\Input::post('video_url_' . $video_id, ''))->get();
								
								if($video)
								{
									// Update existing videos
									$item_videos[$video_id] = array(
										'id'	=> $video_id,
										'data'	=> array(
											'content_id'	=> $item->id,
											'url'			=> \Input::post('video_url_' . $video_id, ''),
											'title'			=> \Input::post('video_title_' . $video_id, ''),
										),
									);
									
									// Remove video ID as it is updated correctly
									array_pop($not_updated);
									
									// Delete image and use default one
									if(\Input::post('video_delete_image_' . $video_id, false))
									{
										$item_videos[$video_id]['data']['thumbnail'] = '';
										$this->delete_image(\Input::post('video_file_db_' . $video_id, ''), 'video');
									}
								}
								else
								{
									\Messages::error('"' . \Input::post('video_url_' . $video_id, '') . '" is invalid video URL. Video not updated.');
								}
							}
							else
							{
								\Messages::error('Video URL can\'t be empty. Video not updated.');
							}
						}
						else
						{
							$video_url = \Input::post('video_url_' . $video_id, false);
							
							if($video_url)
							{
								// Check video
								$youtube = \App\Youtube::forge();
								$video = $youtube->parse(\Input::post('video_url_' . $video_id, ''))->get();
								
								if($video)
								{
									// Add new videos
									$item_videos[0] = array(
										'id'	=> 0,
										'data'	=> array(
											'content_id'	=> $item->id,
											'url'			=> \Input::post('video_url_' . $video_id, ''),
											'title'			=> \Input::post('video_title_' . $video_id, ''),
											'sort'			=> count($item->videos) + 1,
										),
									);
									
									// Remove video ID as it is updated correctly
									array_pop($not_updated);
								}
								else
								{
									\Messages::error('"' . \Input::post('video_url_' . $video_id, '') . '" is invalid video URL. Video not updated.');
								}
							}
						}
					}
					
					// Save images if new files are submitted
					if(isset($this->_image_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_image_data as $image_data)
						{
							if(!in_array(str_replace('video_file_', '', $image_data['field']), $not_updated))
							{
								if(strpos($image_data['field'], 'new_') === false)
								{
									// Update existing image
									if(str_replace('video_file_', '', $image_data['field']) != 0)
									{
										$video_id = (int)str_replace('video_file_', '', $image_data['field']);
										
										$item_videos[$video_id]['data']['thumbnail'] = $image_data['saved_as'];
										
										$this->delete_image(\Input::post('video_file_db_' . $video_id, ''), 'video');
									}
								}
								else
								{
									// Save new image
									$image_tmp = str_replace('video_file_new_', '', $image_data['field']);
									
									$item_videos[0]['data']['thumbnail'] = $image_data['saved_as'];
								}

								//save to amazon
								if($hold_amazon_enable)
								{
									$folder_list = array('', 'thumbs/');
									foreach ($folder_list as $folder)
									{
				                        $file_info = array(
				                                'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
				                                'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
				                                'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
				                            );
										\Helper::uploadFileToAmazonS3($file_info, $settings);
										@unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
									}
								}
								// EOL - save to amazon
							}
							else
							{
								// That video was not updated so we remove its image if there is one
								$this->delete_image($image_data['saved_as'], 'video');
							}
						}
					}
					Model_Category::bind_videos($item_videos);
					
					\Messages::success('Category videos successfully updated.');
					\Response::redirect(\Uri::admin('current'));
					/** END OF VIDEOS **/
				}
				else
				{
					// Delete uploaded images if there is product saving error
					if(isset($this->_image_data))
					{
						foreach($this->_image_data as $image_data)
						{
							$this->delete_image($image_data['saved_as'], 'video');
						}
					}
							
					\Messages::error('<strong>There was an error while trying to update category videos</strong>');
				}
			}

			else
			{
				$val = Model_Category::validate('update');
				
				// Upload image and display errors if there are any
				$image = $this->upload_image();
				if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload content image</strong>');
					\Messages::error('You have to select image');
				}
				elseif($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload content image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)))
				{
					/** IMAGES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
					{
						if(strpos($image_id, 'new_') === false)
						{
							$cover_image = 0;
							if(\Input::post('cover_image'))
							{
								if(\Input::post('cover_image') == $image_id)
								$cover_image = 1;
							}
							$item_images[$image_id] = array(
								'id'	=> $image_id,
								'data'	=> array(
									'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
									'cover'			=> $cover_image,
								),
							);
						}
					}
					
					// Save images if new files are submitted
					if(isset($this->_image_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_image_data as $image_data)
						{
							$cover_count = count($item->images);
							if(strpos($image_data['field'], 'new_') === false)
							{
								// Update existing image
								if(str_replace('image_', '', $image_data['field']) != 0)
								{
									$image_id = (int)str_replace('image_', '', $image_data['field']);
									$cover_count--;
									$cover_image = 0;
									if(\Input::post('cover_image'))
									{
										if(\Input::post('cover_image') == $image_id)
										$cover_image = 1;
									}
									
									$item_images[$image_id] = array(
										'id'	=> $image_id,
										'data'	=> array(
											'content_id'	=> $item->id,
											'image'			=> $image_data['saved_as'],
											'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
											'cover'			=> $cover_image,
										),
									);
									
									$this->delete_image(\Input::post('image_db_' . $image_id, ''));
								}
							}
							else
							{
								// Save new image
								$image_tmp = str_replace('image_new_', '', $image_data['field']);
								$cover_image = 0;
								$item_images[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'content_id'	=> $item->id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
										'cover'			=> $cover_image,
										'sort'			=> $cover_count + 1,
									),
								);
							}

							//save to amazon
							if($hold_amazon_enable)
							{
								$folder_list = array('', 'large/', 'medium/', 'thumbs/');
								foreach ($folder_list as $folder)
								{
			                        $file_info = array(
			                                'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
			                                'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
			                                'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
			                            );
									\Helper::uploadFileToAmazonS3($file_info, $settings);
									@unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
								}
							}
							// EOL - save to amazon
						}
					}
					Model_Category::bind_images($item_images);
					/** END OF IMAGES **/
					
					// Get POST values
					$insert = \Input::post();
					
					// Prepare some values
					// $insert['published_at'] = !empty($insert['published_at']) ? strtotime($insert['published_at']) : $news->created_at;
					$insert['active_from'] 	= !empty($insert['active_from']) ? \Helper::dmyToymd($insert['active_from'], '/') : NULL;
					$insert['active_to'] 	= !empty($insert['active_to']) ? \Helper::dmyToymd($insert['active_to'], '/') : NULL;
					if($insert['status'] != 2)
					{
						unset($insert['active_from']);
						unset($insert['active_to']);
					}

					if(isset($insert['user_group'])){
						$insert['user_group'] = json_encode((object)$insert['user_group']);
					} else {
						$insert['user_group'] = '';
					}
					
					$item->set($insert);
					try{
						$item->save();
						\Messages::success('Category successfully updated.');
						\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/category/list/') : \Uri::admin('current'));
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update category</strong>');
					    
						// Uncomment lines below to show database errors
						//$errors = $e->getMessage();
				    	//\Messages::error($errors);
					}
				}
				else
				{
					// Delete uploaded images if there is category saving error
					if(isset($this->_image_data))
						foreach($this->_image_data as $image_data)
							$this->delete_image($image_data['saved_as']);
							
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update category</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}
		}
		$category = Model_Category::find_one_by_id($id);



		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('category', $category)->set('groups',$groups);
	}
	
	/**
	 * Update category SEO options
	 * 
	 * @param $id	= Category ID
	 */
	public function action_update_seo($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/category/list');
		
		// Get news item to edit
		if(!$item = Model_Category::find_one_by_id($id)) \Response::redirect('admin/product/category/list');

        \View::set_global('menu', 'admin/product/category/list');
		\View::set_global('title', 'Edit Category Meta Content');
		
		if(\Input::post())
		{
			$val = Model_Category_Seo::validate('update', $item->id);
			
			// Get POST values
			$insert = \Input::post();
			
			if($val->run($insert))
			{
				// Update SEO database table
				$item_seo = Model_Category_Seo::find_one_by_content_id($item->id);
				if ($item_seo)
				{
					$item_seo->set($insert);
				}
				else
				{
					$item_seo = Model_Category_Seo::forge($insert);
				}

				
				try{
					$item_seo->save();
					
					\Messages::success('Meta content successfully updated.');
					\Response::redirect(\Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update meta content</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update meta content</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update_seo')
			->set('category', Model_Category::find_one_by_id($id));
	}
	
	/**
	 * List category subcategories if they exist
	 * 
	 * @param $id	= Category ID
	 */
	public function action_subcategories($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/category/list');
		
		// Get news item to edit
		if(!$item = Model_Category::find_one_by_id($id)) \Response::redirect('admin/product/category/list');

        \View::set_global('menu', 'admin/product/category/list');
		\View::set_global('title', 'List Sub Categories');
		
		$search = $this->get_search_items($item->id);
		
        $items      = $search['items'];
        $pagination = $search['pagination'];
        $status     = $search['status'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list_subcategories')
			->set('items', $items)
			->set('category', $item)
			->set('pagination', $pagination, false)
			->set('status', $status);
			
	}

	public function action_related($id = false)
	{
		// disable related categories
		\Response::redirect('admin/product/category/list');

		if(!is_numeric($id)) \Response::redirect('admin/product/category/list');
		
		// Get news item to edit
		if(!$item = Model_Category::find_one_by_id($id)) \Response::redirect('admin/product/category/list');

		if(\Input::post())
		{
			$add 	= \Input::post('categories.add', array());
			$remove = \Input::post('categories.remove', array());
			
			if(\Input::post('add', false))
			{
				foreach($add as $value)
				{
					$related = Model_Category_To_Related::forge(array(
						'related_id' => $value,
						'category_id' => $item->id
					));	
					$related->save();
				}
				
				\Messages::success('Related categories successfully added.');
			}
			else if(\Input::post('remove', false))
			{
				foreach($remove as $value)
				{
					$related = Model_Category_To_Related::find_one_by(array(
						array('related_id', '=', $value),
						array('category_id', '=', $item->id),
					));
					
					if(!is_null($related))
					{
						$related->delete();
					}	
				}
				
				\Messages::success('Related categories successfully removed.');
			}
			
			if(\Input::is_ajax())
			{
				echo \Messages::display('left', false);
				exit;
			}
			else
			{
				\Response::redirect(\Input::referrer(\Uri::create('admin/product/category/list')));
			}
		}

        \View::set_global('menu', 'admin/product/category/list');
		\View::set_global('title', 'List Related Categories');

		$search = $this->get_search_category_items($item);
        
        $pagination = $search['pagination'];
        $status     = $search['status'];
        $item       = $search['item'];

		\Theme::instance()->set_partial('content', $this->view_dir . 'related')
			->set('category', $item)
			->set('pagination', $pagination, false)
			->set('status', $status);
	}

	public function get_search_category_items($item)
    {
        /************ Get non related infotabs ***********/
		$items = Model_Category::find(function($query) use ($item){
			
			$related_ids = array($item->id);
			foreach($item->related_categories as $related)
				array_push($related_ids, $related->id);
			
			if(!empty($related_ids))
			{
				$related_ids = '(' . implode(',', $related_ids) . ')';
				$query->where('id', 'NOT IN', \DB::expr($related_ids));
			}
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		
		$item->not_related_categories = $items ? $items : array();
        
		/************ End generating query ***********/
		
        foreach($item->not_related_categories as $item_key => $item_value)
        {
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title':
                            if(stripos($item_value->title, $value) === false) unset($item->not_related_categories[$item_key]);
                            break;
                        case 'status':
                            if(is_numeric($value))
                                if($item_value->status != $value) unset($item->not_related_categories[$item_key]);
                            break;
                        case 'active_from':
                            $date = strtotime($value);
                            if($date)
                                if($item_value->active_from < $value) unset($item->not_related_categories[$item_key]);
                            break;
                        case 'active_to':
                            $date = strtotime($value);
                            if($date)
                                if($item_value->active_to > $value) unset($item->not_related_categories[$item_key]);
                            break;
                    }
                }
            }
        }
        
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($item->not_related_categories),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$item->not_related_categories     = array_slice($item->not_related_categories, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
        
        return array('item' => $item, 'pagination' => $pagination, 'status' => $status);
    }
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_Category::find_one_by_id($id))
			{
				// Only delete categories without products
				if(count($item->all_products) > 0){
					\Messages::error('This category has products. In order to delete this category first delete all products.');
				}
				else{
					// Only delete categories without subcategories
					if(empty($item->children))
					{	
						// Delete other content data like images, files, etc.
						if(!empty($item->images))
						{
							foreach($item->images as $image)
							{
								$this->delete_image($image->image);
								$image->delete();
							}
						}

						$tmp_categories = Model_Product_To_Categories::find_by_category_id($id);
						if($tmp_categories)
							foreach($tmp_categories as $tmp_cat) $tmp_cat->delete();


						try{
							if (method_exists($item->seo, 'delete'))
							{
								$item->seo->delete();	
							}
							$item->delete();
							
							\Messages::success('Category successfully deleted.');
						}
						catch (\Database_Exception $e)
						{
							// show validation errors
							\Messages::error('<strong>There was an error while trying to delete category</strong>');
						    
							// Uncomment lines below to show database errors
							//$errors = $e->getMessage();
					    	//\Messages::error($errors);
						}
					}
					else
					{
						\Messages::error('This category has subcategories. In order to delete this category first delete all subcategories.');
					}
				}
				
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
				
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_Category::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
	
	/****************************** CONTENT VIDEOS ******************************/
	
	/**
	 * Delete content file
	 * 
	 * @param $video_id		= Video ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_video($video_id = false, $content_id = false)
	{
		if($video_id && $content_id)
		{
			$videos = Model_Category_Video::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($videos)
			{
				if(isset($videos[$video_id]))
				{
					$video = $videos[$video_id];
					
					// If there is only one video and video is required
					if(count($videos) == 1)
					{
						if(\Config::get('details.video.required', false))
						{
							\Messages::error('You can\'t delete all videos. Please add new video in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Category_Video::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $video->sort)->execute();
							// Delete video
							$this->delete_image($video->thumbnail, 'video');
							$video->delete();
							\Messages::success('Video was successfully deleted.');
						}
					}
					else
					{
						// Reset sort fields
						\DB::update(Model_Category_Video::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $video->sort)->execute();
						// Delete video
						$this->delete_image($video->thumbnail, 'video');
						$video->delete();
						
						\Messages::success('Video was successfully deleted.');
					}
				}
				else
				{
					\Messages::error('Video you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Video you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/****************************** CONTENT IMAGES ******************************/
	
	/**
	 * Upload all contet images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '')
			{
				if(is_array($file['name']))
				{
					foreach ($file['name'] as $get_file)
					{
						if($get_file['image'] != '')
						{
							$return['exists'] = true;
							break;
						}
					}
				}
				else
					$return['exists'] = true;
			}
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->image_upload_config = array(
		    'path' => \Config::get('details.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('jpg', 'jpeg', 'gif', 'png'),
		    'max_size' => 10000000
		);

		\Upload::process($this->image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			\Upload::save();

			$this->_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try
			{
				foreach($this->_image_data as $image_data)
				{
					if(isset($image_data['saved_to']))
					{
						chmod($image_data['saved_to'].$image_data['saved_as'], 0755);

						$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
						$image->load($image_data['saved_to'].$image_data['saved_as']);
						
						foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
						{
							$image->preset($preset);
						}
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_image($image_id = false, $content_id = false)
	{
		if($image_id && $content_id)
		{
			$images = Model_Category_Image::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
					
					// If there is only one image and image is required
					if(count($images) == 1)
					{
						if(\Config::get('details.image.required', false))
						{
							\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Category_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
					else
					{
						if($image->cover == 1)
						{
							\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Category_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('Image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Content Image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
				//delete from amazon
				if($hold_amazon_enable)
				{
					if($content_type == 'image')
					{
						\Helper::deleteFileFromAmazonS3('media/images/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/thumbs/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/medium/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/large' . $name, $settings);
					}
				}
				// EOL - delete from amazon
				
				@unlink($folder . $name);
			}
		}
	}
	
	/****************************** CONTENT FILES ******************************/
	
	/**
	 * Upload all contet files to local directory defined in $this->file_upload_config
	 * 
	 */
	public function upload_file()
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// File upload configuration
		$this->file_upload_config = array(
		    'path' => \Config::get('details.file.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('pdf', 'xls', 'xlsx', 'doc', 'docx', 'txt'),
		);
		
		\Upload::process($this->file_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save file
		    \Upload::save();
			$this->_file_data = \Upload::get_files();
			
			return $return;
		}
		else
		{
			// FILE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, file is not uploaded
		return $return;
	}
	
	/**
	 * Delete content file
	 * 
	 * @param $file_id		= File ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_file($file_id = false, $content_id = false)
	{
		if($file_id && $content_id)
		{
			$files = Model_Category_File::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($files)
			{
				if(isset($files[$file_id]))
				{
					$file = $files[$file_id];
					
					// If there is only one image and image is required
					if(count($files) == 1)
					{
						if(\Config::get('details.file.required', false))
						{
							\Messages::error('You can\'t delete all files. Please add new file in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Category_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							\Messages::success('File was successfully deleted.');
						}
					}
					else
					{
						// Dont use cover option for files
						if(FALSE && $file->cover == 1)
						{
							\Messages::error('You can\'t delete cover file. Set different file as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Category_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							
							// Set another file as cover if cover is deleted
							if($file->cover == 1)
							{
								$files = Model_Category_File::find(array(
								    'where' => array(
								        'content_id' => $content_id,
								    ),
								    'order_by' => array('sort' => 'asc'),
								));
								
								$files[0]->cover = 1;
								$files[0]->save();
							}
							
							
							\Messages::success('File was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete file from file system
	 * 
	 * @param $name = File name
	 */
	public function delete_file($name = false)
	{
		if($name && \Config::get('details.file.location.root', false))
		{
			//delete from amazon
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			if($hold_amazon_enable)
			{
				\Helper::deleteFileFromAmazonS3('media/files/' . $name, $settings);
			}
			// EOL - delete from amazon

			@unlink(\Config::get('details.file.location.root') . $name);
		}
	}
	
	/****************************** INFOTAB IMAGES ******************************/
	
	/**
	 * Upload all infotab images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_infotab_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->infotab_image_upload_config = array(
		    'path' => \Config::get('infotab.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		    'max_size' => 500000,
		);
		
		\Upload::process($this->infotab_image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save image
		    \Upload::save();
			$this->_infotab_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_infotab_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('infotab.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('infotab.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 * @param $type			= Type of content (infotab or hotspot)
	 * @param $delete		= If type is "hotspot" we can either delete image or video
	 */
	public function action_delete_infotab_image($image_id = false, $content_id = false, $type = 'infotab', $delete = 'image')
	{
		if($image_id && $content_id)
		{
			$images = Model_Infotab_Image::find(array(
			    'where' => array(
			        'infotab_id' => $content_id,
			        'item_type' => 'product_category'
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
					
					if(strtolower($type) == 'hotspot')
					{
						// Delete only part of hotspot, either image or video
						if($delete == 'image')
						{
							// Delete only image but not whole hotspot
							$this->delete_infotab_image($image->image);
							
            				$image->set(array(
            					'image' => '',
            					'alt_text' => '',
            				));
						}
						else
						{
							// Delete only video but not whole hotspot
            				$image->set(array(
            					'video' => '',
            					'video_title' => '',
            				));
						}
						
						$image->save();
						
						\Messages::success(ucfirst($type) . ' ' . strtolower($delete) . ' was successfully deleted.');
					}
					else
					{
						// If there is only one image and image is required
						if(count($images) == 1)
						{
							if(\Config::get('infotab.image.required', false) && !\Request::is_hmvc())
							{
								\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
							}
							else
							{
								// Reset sort fields
								\DB::update(Model_Infotab_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
								// Delete image
								$this->delete_infotab_image($image->image);
								$image->delete();
								\Messages::success(ucfirst($type) . ' image was successfully deleted.');
							}
						}
						else
						{
							if($image->cover == 1 && !\Request::is_hmvc())
							{
								\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
							}
							else
							{
								// Reset sort fields
								\DB::update(Model_Infotab_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
								// Delete image
								$this->delete_infotab_image($image->image);
								$image->delete();
								\Messages::success(ucfirst($type) . ' image was successfully deleted.');
							}
						}
					}
				}
				else
				{
					\Messages::error(ucfirst($type) . ' image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error(ucfirst($type) . ' image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		if(\Input::is_ajax())
		{
			\Messages::reset();
			\Messages::success('Hotspot was successfully deleted.');
			echo \Messages::display();
		}
		else if(\Request::is_hmvc())
		{
			\Messages::reset();
		}
		else
		{
			\Response::redirect(\Input::referrer());
		}
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_infotab_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			foreach(\Config::get('infotab.' . $content_type . '.location.folders', array()) as $folder)
			{
				//delete from amazon
				if($hold_amazon_enable)
				{
					if($content_type == 'image')
					{
						\Helper::deleteFileFromAmazonS3('media/images/' . $name, $settings);
						\Helper::deleteFileFromAmazonS3('media/images/thumbs/' . $name, $settings);
					}
				}
				// EOL - delete from amazon

				@unlink($folder . $name);
			}
		}
	}
	
	/****************************** INFOTAB FILES ******************************/
	
	/**
	 * Upload all contet files to local directory defined in $this->file_upload_config
	 * 
	 */
	public function upload_infotab_file()
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// File upload configuration
		$this->file_upload_config = array(
		    'path' => \Config::get('details.file.location.root'),
		    'normalize' => true,
		    'randomize' => true,
		    'ext_whitelist' => array('pdf', 'jpg'),
		);
		
		\Upload::process($this->file_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save file
		    \Upload::save();
			$this->_infotab_file_data = \Upload::get_files();
			
			return $return;
		}
		else
		{
			// FILE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, file is not uploaded
		return $return;
	}
	
	/**
	 * Delete content file
	 * 
	 * @param $file_id		= File ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_infotab_file($file_id = false, $content_id = false)
	{
		if($file_id && $content_id)
		{
			$files = Model_Infotab_File::find(array(
			    'where' => array(
			        'infotab_id' => $content_id,
			        'item_type' => 'product_category'
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($files)
			{
				if(isset($files[$file_id]))
				{
					$file = $files[$file_id];
					
					// If there is only one image and image is required
					if(count($files) == 1)
					{
						if(\Config::get('details.file.required', false))
						{
							\Messages::error('You can\'t delete all files. Please add new file in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Infotab_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							\Messages::success('File was successfully deleted.');
						}
					}
					else
					{
						// Dont use cover option for files
						if(FALSE && $file->cover == 1)
						{
							\Messages::error('You can\'t delete cover file. Set different file as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Infotab_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							
							// Set another file as cover if cover is deleted
							if($file->cover == 1)
							{
								$files = Model_Infotab_File::find(array(
								    'where' => array(
								        'infotab_id' => $content_id,
								        'item_type' => 'product_category'
								    ),
								    'order_by' => array('sort' => 'asc'),
								));
								
								$files[0]->cover = 1;
								$files[0]->save();
							}
							
							
							\Messages::success('File was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete file from file system
	 * 
	 * @param $name = File name
	 */
	public function delete_infotab_file($name = false)
	{
		if($name && \Config::get('details.file.location.root', false))
		{
			//delete from amazon
			$settings = \Config::load('amazons3.db');
			$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
			if($hold_amazon_enable)
			{
				\Helper::deleteFileFromAmazonS3('media/files/' . $name, $settings);
			}
			// EOL - delete from amazon

			@unlink(\Config::get('details.file.location.root') . $name);
		}
	}
	
	/**********************************************************************************
	 ********************************** IFNOTAB RELATED METHODS ***********************
	 **********************************************************************************/
	
	/**
	 * Add infotabs to category
	 * 
	 * @param $category_id	= category ID
	 * 
	 */
	public function action_infotab_list($category_id = false)
	{
		if(!is_numeric($category_id)) \Response::redirect('admin/product/category/list');
		// Get news item to edit
		if(!$item = Model_Category::find_one_by_id($category_id)) \Response::redirect('admin/product/category/list');
		
		if(\Input::post())
		{
			$add 	= \Input::post('infotabs.add', array());
			$remove = \Input::post('infotabs.remove', array());
			
			if(\Input::post('add', false))
			{
				foreach($add as $value)
				{
					$infotab = Model_Category_To_Infotabs::forge(array(
						'infotab_id' => $value,
						'category_id' => $item->id
					));	
					$infotab->save();
				}
				
				\Messages::success('Info Tabs successfully added.');
			}
			else if(\Input::post('remove', false))
			{
				foreach($remove as $value)
				{
					$this->action_infotab_delete($value);
				}
				
				\Messages::success('Info Tabs successfully removed.');
			}
			
			if(\Input::is_ajax())
			{
				echo \Messages::display('left', false);
				exit;
			}
			else
			{
				\Response::redirect(\Input::referrer(\Uri::create('admin/product/category/list')));
			}
			
		}
        \View::set_global('menu', 'admin/product/category/list');
		\View::set_global('title', 'Category Infotabs');
		
		/************ Get non related infotabs ***********/
		$items = Model_Infotab::find(function($query) use ($item){
			
			$related_ids = array();
			foreach($item->infotabs as $infotab)
				array_push($related_ids, $infotab->infotab_id);
			
			if(!empty($related_ids))
			{
				$related_ids = '(' . implode(',', $related_ids) . ')';
				$query->where('id', 'NOT IN', \DB::expr($related_ids));
			}
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		
		$item->not_related_infotabs = $items ? $items : array();
		
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'infotabs_list')
			->set('category', $item);
	}
	
	/**
	 * Delete infotab from products_to_infotabs table
	 * 
	 * @param $infotab_id = Unique Infotab ID
	 */
	public function action_infotab_delete($infotab_id)
	{
		$infotab = Model_Category_To_Infotabs::find_by_pk($infotab_id);
		
		// Delete infotab image
		if($infotab->image)
		{
			\Request::forge('admin/product/category/infotab/delete_image/' . $infotab_id)->execute()->response();
		}
		
		// Delete hotspot images
		if(!empty($infotab->images))
		{
			foreach($infotab->images as $image)
			{
				\Request::forge('admin/product/category/delete_infotab_image/' . $image->id . '/' . $image->infotab_id)->execute()->response();
			}
		}
		
		$infotab->delete();
	}
	
	/**
	 * Edit product infotabs
	 * 
	 * @param $product_id	= Product ID
	 * @param $infotab_id	= Infotab ID
	 * @param $hotspot_id	= Hotspot ID
	 * 
	 */
	public function action_infotab_edit($category_id = false, $infotab_id = false, $hotspot_id = false)
	{
		// Check for product
		if(!is_numeric($category_id)) \Response::redirect('admin/product/category/list');
		// Get news item to edit
		if(!$category = Model_Category::find_one_by_id($category_id)) \Response::redirect('admin/product/category/list');
		
		// Check for infotab
		if(!is_numeric($infotab_id)) \Response::redirect('admin/product/category/list');
		// Get news item to edit
		if(!$item = Model_Category_To_Infotabs::find_by_pk($infotab_id)) \Response::redirect('admin/product/category/list');
		
		// Get hotspot is exist
		if(is_numeric($hotspot_id))
			if(!$hotspot = Model_Infotab_Image::find_by_pk($hotspot_id)) 
				unset($hotspot);
		
		if(\Input::post())
		{
			$val = Model_Category_To_Infotabs::validate('update', $item->type);
			if(\Input::post('to_upload') == 'files')
			{
				$item_files = array();
				// Upload file and display errors if there are any
				$file = $this->upload_infotab_file();
				if(!$file['exists'] && \Config::get('infotab.image.required', false) && empty($item->images)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload infotab file</strong>');
					\Messages::error('You have to select pdf');
				}
				elseif($file['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload infotab file</strong>');
					foreach($file['errors'] as $error) \Messages::error($error);
				}
				
				if($val->run() && $file['is_valid'] && !(!$file['exists'] && \Config::get('infotab.file.required', false) && empty($item->files)))
				{
					/** FILES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'title_') as $file_id => $title)
					{
						if(strpos($file_id, 'new_') === false)
						{
							$item_files[$file_id] = array(
								'id'	=> $file_id,
								'data'	=> array(
									'title'		=> \Input::post('title_' . $file_id, ''),
								),
							);
						}
					}

					// Save files if new ones are submitted
					if(isset($this->_infotab_file_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_infotab_file_data as $file_data)
						{
							$cover_count = count($item->files);
							if(strpos($file_data['field'], 'new_') === false)
							{
								// Update existing file
								if(str_replace('file_', '', $file_data['field']) != 0)
								{
									$file_id = (int)str_replace('file_', '', $file_data['field']);
									$cover_count--;
									
									$item_files[$file_id] = array(
										'id'	=> $file_id,
										'data'	=> array(
											'infotab_id'	=> $item->id,
											'item_id'	=> $category->id,
											'file'			=> $file_data['saved_as'],
											'title'			=> \Input::post('title_' . $file_id, ''),
											'item_type'		=> 'product_category'
										),
									);
									
									$this->delete_file(\Input::post('file_db_' . $file_id, ''));
								}
							}
							else
							{
								// Save new file
								$file_tmp = str_replace('file_new_', '', $file_data['field']);
								
								$item_files[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'infotab_id'	=> $item->id,
										'item_id'	=> $category->id,
										'file'			=> $file_data['saved_as'],
										'title'			=> \Input::post('title_new_' . $file_tmp, ''),
										'cover'			=> $cover_count == 0 ? 1 : 0,
										'sort'			=> $cover_count + 1,
										'item_type'		=> 'product_category'
									),
								);
							}

							//save to amazon
							if($hold_amazon_enable)
							{
			                    $file_info = array(
			                            'savePath' => 'media/files/'.$file_data['saved_as'],
			                            'filePath' => $file_data['saved_to'].$file_data['saved_as'],
			                            'contentType' => mime_content_type($file_data['saved_to'].$file_data['saved_as']),
			                        );
								\Helper::uploadFileToAmazonS3($file_info, $settings);
								@unlink($file_data['saved_to'].$file_data['saved_as']);
							}
							// EOL - save to amazon
						}
					}
					
					Model_Infotab::bind_files($item_files);
					/** END OF IMAGES **/
					
					// Get POST values
					$insert = \Input::post();
					
					// Prep some values
					$insert['description'] = trim($insert['description']);
					$insert['active'] = !empty($insert['description']) ? 1 : 0;
					
					$item->set($insert);
					
					try{
						$item->save();
						
						\Messages::success('Infotab successfully updated.');
						\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/category/infotab_list/'.$category->id) : \Uri::admin('current'));
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab</strong>');
					    
						// Uncomment lines below to show database errors
						//$errors = $e->getMessage();
				    	//\Messages::error($errors);
					}
				}
				else
				{
					// Delete uploaded images if there is category saving error
					if(isset($this->_infotab_file_data))
					{
						foreach($this->_infotab_file_data as $file_data)
						{
							$this->delete_infotab_file($file_data['saved_as']);
						}
					}
							
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}
			else
			{
				$item_images = array();
				// Upload image and display errors if there are any
				$image = $this->upload_infotab_image();
				if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->images)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
					\Messages::error('You have to select image');
				}
				elseif($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->images)))
				{
					/** IMAGES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
					{
						if(strpos($image_id, 'new_') === false)
						{
							$item_images[$image_id] = array(
								'id'	=> $image_id,
								'data'	=> array(
									'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
								),
							);
						}
					}
					
					// Save images if new files are submitted
					if(isset($this->_infotab_image_data))
					{
						$settings = \Config::load('amazons3.db');
						$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
						foreach($this->_infotab_image_data as $image_data)
						{
							$cover_count = count($item->images);
							if(strpos($image_data['field'], 'new_') === false)
							{
								// Update existing image
								if(str_replace('image_', '', $image_data['field']) != 0)
								{
									$image_id = (int)str_replace('image_', '', $image_data['field']);
									$cover_count--;
									
									$item_images[$image_id] = array(
										'id'	=> $image_id,
										'data'	=> array(
											'infotab_id'	=> $item->unique_id,
											'image'			=> $image_data['saved_as'],
											'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
											'item_type'		=> 'product_category'
										),
									);
									
									$this->delete_infotab_image(\Input::post('image_db_' . $image_id, ''));
								}
							}
							else
							{
								// Save new image
								$image_tmp = str_replace('image_new_', '', $image_data['field']);
								
								$item_images[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'infotab_id'	=> $item->unique_id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
										'cover'			=> $cover_count == 0 ? 1 : 0,
										'sort'			=> $cover_count + 1,
										'item_type'		=> 'product_category'
									),
								);
							}

							//save to amazon
							if($hold_amazon_enable)
							{
								$folder_list = array('', 'thumbs/');
								foreach ($folder_list as $folder)
								{
			                        $file_info = array(
			                                'savePath' => 'media/images/'.$folder.$image_data[0]['saved_as'],
			                                'filePath' => $image_data[0]['saved_to'].$folder.$image_data[0]['saved_as'],
			                                'contentType' => mime_content_type($image_data[0]['saved_to'].$folder.$image_data[0]['saved_as']),
			                            );
									\Helper::uploadFileToAmazonS3($file_info, $settings);
									@unlink($image_data[0]['saved_to'].$folder.$image_data[0]['saved_as']);
								}
							}
							// EOL - save to amazon
						}
					}
					
					Model_Infotab::bind_images($item_images);
					/** END OF IMAGES **/
					
					// Get POST values
					$insert = \Input::post();
					
					// Prep some values
					$insert['description'] = trim($insert['description']);
					$insert['active'] = !empty($insert['description']) ? 1 : 0;
					
					$item->set($insert);
					
					try{
						$item->save();
						
						\Messages::success('Infotab successfully updated.');
						\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/product/category/infotab_list/'.$category->id) : \Uri::admin('current'));
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab</strong>');
					    
						// Uncomment lines below to show database errors
						//$errors = $e->getMessage();
				    	//\Messages::error($errors);
					}
				}
				else
				{
					// Delete uploaded images if there is category saving error
					if(isset($this->_infotab_image_data))
					{
						foreach($this->_infotab_image_data as $image_data)
						{
							$this->delete_infotab_image($image_data['saved_as']);
						}
					}
							
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}
		}
		
		$item = Model_Category_To_Infotabs::find_by_pk($infotab_id);

        \View::set_global('menu', 'admin/product/category/list');
		\View::set_global('title', 'Edit Category Info Tab');
		
		$theme_partial = \Theme::instance()->set_partial('content', $this->view_dir . 'infotabs_edit_' . strtolower($item->type))
			->set('category', $category)
			->set('infotab', $item);
		
		if(isset($hotspot))
			$theme_partial->set('hotspot', $hotspot);
	}
	
	/**
	 * Edit product hotspot infotab
	 * This infotab has realy custom form and needs to be separated of others
	 * 
	 * @param $product_id	= Product ID
	 * @param $infotab_id	= Infotab ID
	 * 
	 */
	public function action_infotab_edit_hotspot($category_id = false, $infotab_id = false)
	{
		// Check for product
		if(!is_numeric($category_id)) \Response::redirect('admin/product/category/list');
		// Get news item to edit
		if(!$category = Model_Category::find_one_by_id($category_id)) \Response::redirect('admin/product/category/list');
		
		// Check for infotab
		if(!is_numeric($infotab_id)) \Response::redirect('admin/product/category/list');
		// Get news item to edit
		if(!$item = Model_Category_To_Infotabs::find_by_pk($infotab_id)) \Response::redirect('admin/product/category/list');
		
		if(\Input::post())
		{
			if(\Input::post('image', false))
			{
				// Upload image and display errors if there are any
				$image = $this->upload_infotab_image();
				if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->image)) 
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
					\Messages::error('You have to select image');
				}
				elseif($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if(($image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($item->image))) || \Input::post('use_cover_image', false))
				{
					// Clear previous messages if exists
					\Messages::reset();
					
					// Get POST values
					$insert = \Input::post();
					
					$item_image['alt_text'] = \Input::post('alt_text', false) ? \Input::post('alt_text', false) : NULL;
					
					// Use cover image
					if(\Input::post('use_cover_image', false))
					{
						$cover_image = \Input::post('cover_image');
						$old_image = \Config::get('details.image.location.root') . $cover_image; 
						$new_image	 = \Config::get('infotab.image.location.root') . $cover_image; 
						
						var_dump($old_image, $new_image);
						
						while(file_exists($new_image))
						{
							$file_name 	= pathinfo($new_image, PATHINFO_FILENAME);
							$file_ext 	= pathinfo($new_image, PATHINFO_EXTENSION);
							$file_name = \Str::increment($file_name);
							
							$new_image = \Config::get('infotab.image.location.root') . $file_name . '.' . $file_ext;
						}
						
						if(\File::copy($old_image, $new_image))
						{
							$image = \Image::forge(array('presets' => \Config::get('infotab.image.resize', array())));
							$image->load($new_image);
							
							foreach(\Config::get('infotab.image.resize', array()) as $preset => $options)
							{
								$image->preset($preset);
							}
							
							if(isset($item_image))
							{
								$insert['alt_text'] = isset($item_image['alt_text']) ? $item_image['alt_text'] : NULL;
								$insert['image'] 	= basename($new_image);
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));

								//save to amazon
								$settings = \Config::load('amazons3.db');
								$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
								if($hold_amazon_enable)
								{
									$folder_list = array('', 'thumbs/');
									foreach ($folder_list as $folder)
									{
				                        $file_info = array(
				                                'savePath' => 'media/images/'.$folder.$file_name . '.' . $file_ext,
				                                'filePath' => $new_image,
				                                'contentType' => mime_content_type($new_image),
				                            );
										\Helper::uploadFileToAmazonS3($file_info, $settings);
										@unlink($new_image);
									}
								}
								// EOL - save to amazon
							}
						}
						else
						{
							\Messages::error('<strong>There was an error while trying to upload infotab image</strong>');
							\Messages::error('Please try again');
						}
						
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					else
					{
						/** IMAGES **/
						// Save images if new files are submitted
						if(isset($this->_infotab_image_data))
						{
							$settings = \Config::load('amazons3.db');
							$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
							foreach($this->_infotab_image_data as $image_data)
							{
								$item_image['image'] = $image_data['saved_as'];
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));

								//save to amazon
								if($hold_amazon_enable)
								{
									$folder_list = array('', 'thumbs/');
									foreach ($folder_list as $folder)
									{
				                        $file_info = array(
				                                'savePath' => 'media/images/'.$folder.$image_data['saved_as'],
				                                'filePath' => $image_data['saved_to'].$folder.$image_data['saved_as'],
				                                'contentType' => mime_content_type($image_data['saved_to'].$folder.$image_data['saved_as']),
				                            );
										\Helper::uploadFileToAmazonS3($file_info, $settings);
										@unlink($image_data['saved_to'].$folder.$image_data['saved_as']);
									}
								}
								// EOL - save to amazon
							}
						}
						
						if(isset($item_image))
						{
							$insert['alt_text'] = isset($item_image['alt_text']) ? $item_image['alt_text'] : NULL;
							$insert['image'] 	= isset($item_image['image']) ? $item_image['image'] : $item->image;
						}
						/** END OF IMAGES **/
					}
					
					// Make infotab active
					$insert['active'] = 1;
					
					$item->set($insert);
					
					try{
						$item->save();
						
						\Messages::success('Infotab image successfully updated.');
						\Response::redirect(\Uri::create('admin/product/category/infotab_edit/' . $category->id . '/' . $item->unique_id));
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update infotab image</strong>');
					    
						// Uncomment lines below to show database errors
						//$errors = $e->getMessage();
				    	//\Messages::error($errors);
					}
				}
				else
				{
					// Delete uploaded images if there is product saving error
					if(isset($this->_infotab_image_data))
					{
						foreach($this->_infotab_image_data as $image_data)
						{
							$this->delete_infotab_image($image_data['saved_as']);
						}
					}
				}
			}
			
			if(\Input::post('hotspot', false))
			{
				
			}
		}
		
		\Response::redirect(\Uri::create('admin/product/category/infotab_edit/' . $category->id . '/' . $item->unique_id));
	}
	
	/**
	 * Edit product hotspot position
	 * 
	 * @param $product_id	= Product ID
	 * @param $infotab_id	= Infotab ID
	 * 
	 */
	public function action_infotab_hotspot($category_id = false, $infotab_id = false, $hotspot_id = false)
	{
		// Check for product
		if(!is_numeric($category_id)) \Response::redirect('admin/product/category/list');
		// Get news item to edit
		if(!$category = Model_Category::find_one_by_id($category_id)) \Response::redirect('admin/product/category/list');
		
		// Check for infotab
		if(!is_numeric($infotab_id)) \Response::redirect('admin/product/category/list');
		// Get news item to edit
		if(!$item = Model_Category_To_Infotabs::find_by_pk($infotab_id)) \Response::redirect('admin/product/category/list');
		
		// Get hotspot is exist
		if(is_numeric($hotspot_id))
			if(!$hotspot = Model_Infotab_Image::find_by_pk($hotspot_id)) 
				unset($hotspot);
		
		if(\Input::post())
		{
			$insert = \Input::post();
			
			if(!\Input::is_ajax())
			{
				$val = Model_Infotab_Image::validate('create');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to create hotspot</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
					\Response::redirect(\Uri::create('admin/product/category/infotab_edit/' . $category->id . '/' . $item->unique_id . (isset($hotspot) ? '/'.$hotspot->id : '')));
				}
				
				$insert['title'] 		= trim($insert['title']) != '' ? $insert['title'] : NULL;
				$insert['description'] 	= trim($insert['description']) != '' ? $insert['description'] : NULL;
			}
			
			$insert['infotab_id'] 	= $infotab_id;
			$insert['item_type'] 	= 'product_category';
			
			if(\Input::post('create', false))
			{
				$hotspot = Model_Infotab_Image::forge($insert);
				
				try{
					$hotspot->save();
					
					if(\Input::is_ajax())
					{
						$return['hotspot_id'] = $hotspot->id;
						echo json_encode($return); exit;
					}
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create hotspot</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
			    	
					if(\Input::is_ajax())
					{
						$return['message'] = \Messages::display();
						$return['hotspot_id'] = false;
						echo json_encode($return); exit;
					}
			    	
				}
			}
			
			if(\Input::post('update', false))
			{
				if(isset($hotspot))
				{
					/** IMAGES **/
					// Upload image and display errors if there are any
					$image = $this->upload_infotab_image();
					if(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($hotspot->image)) 
					{
						// No previous images and image is not selected and it is required
						\Messages::error('<strong>There was an error while trying to upload hotspot image</strong>');
						\Messages::error('You have to select image');
					}
					elseif($image['errors'])
					{
						\Messages::error('<strong>There was an error while trying to upload hotspot image</strong>');
						foreach($image['errors'] as $error) \Messages::error($error);
					}
					
					if(($image['is_valid'] && !(!$image['exists'] && \Config::get('infotab.image.required', false) && empty($hotspot->image))) || \Input::post('use_cover_image', false))
					{
						// Clear previous messages if exists
						\Messages::reset();
						
						$item_image['alt_text'] = \Input::post('alt_text', false) ? \Input::post('alt_text', false) : NULL;
						
						// Save images if new files are submitted
						if(isset($this->_infotab_image_data))
						{
							$settings = \Config::load('amazons3.db');
							$hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
							foreach($this->_infotab_image_data as $image_data)
							{
								$item_image['image'] = $image_data['saved_as'];
								
								// Delete old infotab image
								if(\Input::post('image_db', false))
									$this->delete_infotab_image(\Input::post('image_db', ''));

								//save to amazon
								if($hold_amazon_enable)
								{
				                    $file_info = array(
				                            'savePath' => 'media/files/'.$image_data['saved_as'],
				                            'filePath' => $image_data['saved_to'].$image_data['saved_as'],
				                            'contentType' => mime_content_type($image_data['saved_to'].$image_data['saved_as']),
				                        );
									\Helper::uploadFileToAmazonS3($file_info, $settings);
									@unlink($image_data['saved_to'].$image_data['saved_as']);
								}
								// EOL - save to amazon
							}
						}
						
						if(isset($item_image))
						{
							$insert['alt_text'] = isset($item_image['alt_text']) ? $item_image['alt_text'] : NULL;
							$insert['image'] 	= isset($item_image['image']) ? $item_image['image'] : $hotspot->image;
						}
					}
					else
					{
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					/** END OF IMAGES **/
					
					/** VIDEOS **/
					$item_video['video_title'] = \Input::post('video_title', false) ? \Input::post('video_title', false) : NULL;
					$item_video['video'] = \Input::post('video_url', false) ? \Input::post('video_url', false) : NULL;
					
					if(!is_null($item_video['video']))
					{
						// Check video
						$youtube = \App\Youtube::forge();
						$video = $youtube->parse($item_video['video'])->get();
						
						if(!$video)
						{
							\Messages::error('"' . $item_video['video'] . '" is invalid video URL. Video not updated.');
							
							// Revert to old values
							$item_video['video_title'] 	= $hotspot->video_title;
							$item_video['video'] 		= $hotspot->video;
						}
					}
					
					if(isset($item_video))
					{
						$insert['video'] 	= isset($item_video['video']) ? $item_video['video'] : NULL;
						$insert['video_title'] = isset($item_video['video_title']) ? $item_video['video_title'] : NULL;
						
						// Unset video title is there is no video
						if(is_null($insert['video'])) $insert['video_title'] = NULL;
					}
					/** END OF: VIDEOS **/
					
					$hotspot->set($insert);
					
					try{
						$hotspot->save();
						\Messages::success('Hotspot sucessfully updated.');
					}
					catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('There was an error while trying to update hotspot.');
						\Messages::error('Please try again.');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
				    	
						// Delete uploaded images if there is product saving error
						if(isset($this->_infotab_image_data))
						{
							foreach($this->_infotab_image_data as $image_data)
							{
								$this->delete_infotab_image($image_data['saved_as']);
							}
						}
					}
					
					if(\Input::is_ajax())
					{
						echo \Messages::display(); exit;
					}
				}
			}
		}
		
		\Response::redirect(\Uri::create('admin/product/category/infotab_edit/' . $category->id . '/' . $item->unique_id . (isset($hotspot) ? '/'.$hotspot->id : '')));
	}
}
