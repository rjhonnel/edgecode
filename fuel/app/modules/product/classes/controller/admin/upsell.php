<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Product;

class Controller_Admin_Upsell extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/upsell/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('product::product', 'details');
		\Config::load('product::upsell', 'upsell');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('upsell.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/list');
	}
	
    public function get_search_items($item)
    {
        /************ Get non upsell products ***********/
        $items = Model_Product::find(function($query) use ($item){
			
			$upsell_ids = array($item->id);
			foreach($item->upsell_products as $upsell)
				array_push($upsell_ids, $upsell->id);
			
			if(!empty($upsell_ids))
			{
				$upsell_ids = '(' . implode(',', $upsell_ids) . ')';
				$query->where('id', 'NOT IN', \DB::expr($upsell_ids));
			}
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
		
		$item->not_upsell_products = $items ? $items : array();
        
		/************ End generating query ***********/
		
        foreach($item->not_upsell_products as $item_key => $item_value)
        {
            foreach(\Input::get() as $key => $value)
            {
                if(!empty($value) || $value == '0')
                {
                    switch($key)
                    {
                        case 'title':
                            if(stripos($item_value->title, $value) === false) unset($item->not_upsell_products[$item_key]);
                            break;
                        case 'status':
                            if(is_numeric($value))
                                if($item_value->status != $value) unset($item->not_upsell_products[$item_key]);
                            break;
                        case 'active_from':
                            $date = strtotime($value);
                            if($date)
                                if($item_value->active_from < $value) unset($item->not_upsell_products[$item_key]);
                            break;
                        case 'active_to':
                            $date = strtotime($value);
                            if($date)
                                if($item_value->active_to > $value) unset($item->not_upsell_products[$item_key]);
                            break;
                        case 'category_id':
                            if(is_numeric($value))
                                if(!array_key_exists($value, $item_value->categories)) unset($item->not_upsell_products[$item_key]);
                            break;
                    }
                }
            }
        }
        
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($item->not_upsell_products),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$item->not_upsell_products     = array_slice($item->not_upsell_products, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
        
        return array('item' => $item, 'pagination' => $pagination, 'status' => $status);
    }
    
	/**
	 * Manage upsell products
	 * 
	 * @param $id	= Product ID
	 */
	public function action_list($id)
	{
		if(!is_numeric($id)) \Response::redirect('admin/product/list');
		
		// Get news item to edit
		if(!$item = Model_Product::find_one_by_id($id)) \Response::redirect('admin/product/list');
		
		if(\Input::post())
		{
			$add 		= \Input::post('products.add', array());
			$remove 	= \Input::post('products.remove', array());
			$discounts 	= \Input::post('discount', array());
			
			if(\Input::post('add', false))
			{
				foreach($add as $value)
				{
					$upsell = Model_Product_To_Upsell::forge(array(
						'upsell_id' => $value,
						'product_id' => $item->id
					));	
					$upsell->save();
				}
				
				\Messages::success('Upsell products successfully added.');
			}
			else if(\Input::post('remove', false))
			{
				foreach($remove as $value)
				{
					$upsell = Model_Product_To_Upsell::find_one_by(array(
						array('upsell_id', '=', $value),
						array('product_id', '=', $item->id),
					));
					
					if(!is_null($upsell))
					{
						$upsell->delete();
					}	
				}
				
				\Messages::success('Upsell products successfully removed.');
			}
			else if(\Input::post('save', false))
			{
				foreach($discounts as $key => $value)
				{
					$upsell = Model_Product_To_Upsell::find_one_by(array(
						array('upsell_id', '=', $key),
						array('product_id', '=', $item->id),
					));
					
					if(!is_null($upsell))
					{
						$upsell->discount = round($value, 0);
						$upsell->save();
					}	
				}
				
				\Messages::success('Upsell discounts successfully saved.');
			}
			
			if(\Input::is_ajax())
			{
				echo \Messages::display('left', false);
				exit;
			}
			else
			{
				\Response::redirect(\Input::referrer(\Uri::create('admin/product/list')));
			}
		}
		
		\View::set_global('title', 'List Upsell Products');
		
        $search = $this->get_search_items($item);
        
        $pagination = $search['pagination'];
        $status     = $search['status'];
        $item       = $search['item'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('product', $item)
			->set('pagination', $pagination, false)
			->set('status', $status);
	}
}
