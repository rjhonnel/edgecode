<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace Product;


class Controller_Admin_Stock extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/product/stock/';
	
	public function before()
	{
		parent::before();
	}

	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/product/stock/list');
	}
	
    public function get_search_items($category_id = false)
    {
        // Override category_id if its a search
        $category_id = \Input::get('category_id', $category_id);
        
        $product_ids = array();
        
        // If we are viewing category products
        // We need to find all products from that and child categories
        if(is_numeric($category_id))
        {
            $category = \Product\Model_Category::find_one_by_id($category_id);
            if($category)
            { 
                \View::set_global('category', $category);
                if($category->all_products)
                {
                    $product_ids = array_keys($category->all_products);
                }
            }
        }

        // If we are filtering products by category and there is nothing found
        // We return empty array of products without even going to database
        if(empty($product_ids) && is_numeric($category_id)) 
        { 
            $items = array();
        }
        else
        {            
            /************ Start generating query ***********/
            $items = Model_Product::find(function($query) use ($product_ids){

                if(!empty($product_ids))
                {
                    $query->where('product.id', 'in', $product_ids);
                }

                // Get search filters
                foreach(\Input::get() as $key => $value)
                {
                    if(!empty($value) || $value == '0')
                    {
                        switch($key)
                        {
                            case 'title':
                                $query->select('product.*');
                                $query->distinct();
                                $query->join('product_attributes');
                                $query->on('product.id', '=', 'product_attributes.product_id');
                                $query->where('product.'.$key, 'like', "%$value%")
                                	->or_where('product.code', 'like', "%$value%")
                                    ->or_where('product_attributes.product_code', 'like', "%$value%");
                                break;
                            case 'status':
                                if(is_numeric($value))
                                    $query->where($key, $value);
                                break;
                            case 'active_from':
                                $date = strtotime($value);
                                if($date)
                                    $query->where($key, '>=', $date);
                                break;
                            case 'active_to':
                                $date = strtotime($value);
                                if($date)
                                    $query->where($key, '<=', $date);
                                break;
                        }
                    }
                }

                // Order query
                $query->order_by('product.sort', 'asc');
                $query->order_by('product.id', 'asc');
            });
        }
        
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
        
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );

        $group = Model_Group::find();
        
        return array('items' => $items, 'group' => $group, 'pagination' => $pagination, 'status' => $status);
    }
    
	public function action_list($category_id = false, $update_category = false)
	{

        $stock_options = \Config::load('stock-option.db');
        \View::set_global('menu', 'admin/product/stock/list');
		\View::set_global('title', 'Stock Control');
		\View::set_global('update_category', $update_category);

        // Update
        if(\Input::post() && isset($_POST['update_stock']))  
        {            
            $post = \Input::post();            
            $return_to = is_numeric(\Input::post('return_to')) ? '#' . \Input::post('return_to') : '' ;

            try
            {
                foreach($post['stock_quantity'] as $key => $value)
                {
                	$attr = Model_Attribute::find_one_by_id($key);
					$attr->set(array(
						'stock_quantity' => $value
						));
					$attr->save();
                }
                
                
                \Messages::success('Stock quantity successfully updated.');
            }
            catch (\Database_Exception $e)
            {
                // show validation errors
                \Messages::error('<strong>There was an error while trying to update stock quantity.</strong>');

                // Uncomment lines below to show database errors
                // $errors = $e->getMessage();
                // \Messages::error($errors);

                \Response::redirect(\Uri::create(\Uri::admin('current'), array(), \Input::get()) . $return_to);
            }
            
            \Response::redirect(\Uri::create(\Uri::admin('current'), array(), \Input::get()) . $return_to);
            
        }
		
        $search = $this->get_search_items($category_id);
        
        $items      = $search['items'];
        $group      = $search['group'];
        $pagination = $search['pagination'];
        $status     = $search['status'];
        
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('group', $group)
			->set('pagination', $pagination, false)
			->set('status', $status)
            ->set('options', $stock_options);
	}

    public function action_option() {




        $stock_options = \Config::load('stock-option.db');

        #\Config::save('visio.db', array('my_param' => 'my_value'));



       // $stock_options = Model_Setting::find(array('where' => array(array('meta_key', '=', 'stock-option'))));


         // \Config::set('stock-option.manage_stock', 1);


        // echo 'test '. \Config::get('stock-option.manage_stock');

        if ($input = \Input::post()) {
            // $insert = array(
            //         'meta_key' => 'stock-option',
            //         'meta_value' => json_encode(array(
            //                 'manage_stock' => $input['manage_stock'],
            //                 'hide_out_of_stock' => $input['hide_out_of_stock'],
            //                 'allow_buy_out_of_stock' => $input['allow_buy_out_of_stock'],
            //             ))
            //     );

           
            



            // if (!$setting = Model_Setting::find_one_by_id($stock_options[0]->id)) {
            //     $setting = Model_Setting::forge($insert);
            // } else {
            //     $setting->set($insert);
            // }
                

            try {
                // $setting->save();

                \Config::save('stock-option.db', array(
                            'manage_stock' => $input['manage_stock'],
                            'hide_out_of_stock' => $input['hide_out_of_stock'],
                            'do_not_allow_buy_out_of_stock' => $input['do_not_allow_buy_out_of_stock'],
                        ));



                \Messages::success('Option successfully created.');
                \Response::redirect('admin/product/stock/option');
            } 
            catch (\Database_Exception $e)
            {
                // show validation errors
                \Messages::error('<strong>There was an error while trying to create option</strong>');
                
                // Uncomment lines below to show database errors
                $errors = $e->getMessage();
                \Messages::error($errors);
            }    
        }
        \View::set_global('menu', 'admin/product/stock/option');
	    \View::set_global('title','Stock Options');
        \Theme::instance()->set_partial('content', $this->view_dir . 'option')->set('option', $stock_options, false);
    }
}