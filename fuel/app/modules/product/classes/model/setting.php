<?php

	namespace Product;

	class Model_Setting extends \Model_Base
	{
		// Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'options';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'meta_key',
		    'meta_value',
		);



		 /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
	    public static function validate($factory)
	    {
	        $val = \Validation::forge($factory);
	        $val->add('company_name', 'Company Name')->add_rule('required');
	        $val->add('address', 'Address')->add_rule('required');
	        $val->add('website', 'Website')->add_rule('required')->add_rule('valid_url');
	        $val->add('phone', 'Phone')->add_rule('required');
	        $val->add('email_address', 'Email Address')->add_rule('required')->add_rule('valid_email');
	        $val->add('sender_email_address', 'Sender email address')->add_rule('required')->add_rule('valid_email');
	        $val->add('contact_us_email_address', 'Contact us email address')->add_rule('required')->add_rule('valid_email');


	        return $val;
	    }
	}