<?php

	namespace Product;

	class Model_Product_To_Categories extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'product_to_categories';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'product_id',
		    'category_id',
		);
	}