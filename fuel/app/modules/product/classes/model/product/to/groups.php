<?php

	namespace Product;

	class Model_Product_To_Groups extends \Model_Base
	{
		// Set the table to use
		protected static $_table_name = 'product_to_groups';
		 
		// List of all columns that will be used on create/update
		protected static $_properties = array(
			'id',
			'group_id',
			'product_id',
		);
	}