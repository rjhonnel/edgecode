<?php

	namespace Product;

	class Model_Product_To_Brands extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'product_to_brands';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'product_id',
		    'brand_id',
		);
	}