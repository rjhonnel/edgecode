<?php

	namespace Product;

	class Model_Category_To_Infotabs extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'product_category_to_infotabs';
	    
	    protected static $_primary_key = 'unique_id';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'unique_id',
		    'infotab_id',
		    'category_id',
		    'description',
		    'image',
		    'alt_text',
		    'active',
		);
		
		protected static $_defaults = array(
		    'active' => 0,
		);
		
		protected static function pre_find(&$query)
		{
			$query->select(
				static::$_table_name.'.*', 
				Model_Infotab::get_protected('_table_name').'.*'
			);
			$query->join(Model_Infotab::get_protected('_table_name'));
			$query->on(Model_Infotab::get_protected('_table_name').'.id', '=', static::$_table_name.'.infotab_id');
		}
		
		/**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
		    			// Get content images
						$item->get_images = static::lazy_load(function() use ($item){
		    				return Model_Infotab_Image::find(array(
							    'where' => array(
							        'infotab_id' => $item->unique_id,
							        'item_type' => 'product_category'
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->unique_id, 'images');

	                    // Get content files
	                    $item->get_files = static::lazy_load(function() use ($item){
	                        return Model_Infotab_File::find(array(
	                            'where' => array(
	                                'infotab_id' => $item->id,
	                                'item_id' => $item->category_id,
							        'item_type' => 'product_category'
	                            ),
	                            'order_by' => array('sort' => 'asc'),
	                        ));
	                    }, $item->id, 'files');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Product\Model_Category_To_Infotabs::$temp['lazy.'.$id.$type]))
		    	{
		    		\Product\Model_Category_To_Infotabs::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Product\Model_Category_To_Infotabs::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Category_To_Infotabs::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Product\Model_Category_To_Infotabs::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Category_To_Infotabs::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Product\Model_Category_To_Infotabs::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
		/**
	     * Validate Model fields
	     * 
	     * @param $factory 	= Validation name, if you want to have multiple validations
	     * @param $type 	= Type of Infotab (normal, hotspot, table)
	     */
		public static function validate($factory, $type = 'normal')
		{
			$type = strtolower($type);
			$type = in_array($type, array('normal', 'hotspot', 'table')) ? $type : 'normal';
			
			$val = \Validation::forge($factory);
			
			switch($type)
			{
				case 'normal':
                    //$val->add('description', 'Description')->add_rule('required');
                    break;
				case 'table':
					$val->add('description', 'Description')->add_rule('required');
					break;
				case 'hotspot':
					break;
			}
			
			return $val;
		}
	}