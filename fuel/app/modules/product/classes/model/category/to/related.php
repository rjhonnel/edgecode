<?php

	namespace Product;

	class Model_Category_To_Related extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'product_categories_to_related';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'category_id',
		    'related_id',
		);
	}