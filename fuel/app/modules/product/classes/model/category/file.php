<?php

	namespace Product;

	class Model_Category_File extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'content_files';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'content_id',
		    'content_type',
		    'file',
		    'title',
		    'cover',
		    'sort',
		);
		
	    protected static $_defaults = array(
		    'content_type' => 'product_category',
		);
		
		 /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	    	$query->where('content_type', 'product_category');
	    }
	    
		/**
		 * Prep some query values
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
				
			return $vars;
		}
	}