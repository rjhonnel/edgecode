<?php

	namespace Product;

	class Model_Infotab extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'infotab';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'type',
		    'title',
		    'global',
		    'user_created',
		    'user_updated',
		    'sort',
		);
		
		protected static $_defaults = array(
		    'type' => 'normal',
		);
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    /**
	     * Insert or update product images
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	    	if(empty($images) || !is_array($images)) return false;
	    	
	    	foreach($images as $key => $image)
	    	{
	    		$data = $image['data'];
	    		if(is_numeric($image['id']) && $image['id'] > 0)
	    		{
	    			// Update existing image
	    			$item = Model_Infotab_Image::find_one_by_id($image['id']);
	    			$item->set($data);
	    		}
	    		else
	    		{
	    			$item = Model_Infotab_Image::forge($data);
	    		}

	    		$item->save();
	    	}
	    }

	    /**
	     * Insert or update product files
	     * Certain array structure need to be passed
	     * 
	     * array(
	     *  0 => array(
	     *      'id'    => If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     *      'data'  => Array of file data to insert
	     *  ),
	     *  1 => array(
	     *      'id'    => If numeric and larger than 0, file will be updated. Otherwise file is considered new
	     *      'data'  => Array of file data to insert
	     *  ),
	     *  etc.
	     * )
	     * 
	     * @param $files
	     */
	    public static function bind_files($files = array())
	    {
	        if(empty($files) || !is_array($files)) return false;

	        foreach($files as $key => $file)
	        {
	            $data = $file['data'];
	            if(is_numeric($file['id']) && $file['id'] > 0)
	            {
	                // Update existing file
	                $item = Model_Infotab_File::find_one_by_id($file['id']);
	                $item->set($data);
	            }
	            else
	            {
	                $item = Model_Infotab_File::forge($data);
	            }

	            $item->save();
	        }
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
		    			// Get content images
						$item->get_images = static::lazy_load(function() use ($item){
		    				return Model_Infotab_Image::find(array(
							    'where' => array(
							        'infotab_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'images');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Product\Model_Infotab::$temp['lazy.'.$id.$type]))
		    	{
		    		\Product\Model_Infotab::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Product\Model_Infotab::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Infotab::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Product\Model_Infotab::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Infotab::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Product\Model_Infotab::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);
	
			return $val;
		}
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				$vars['user_created']	= \Sentry::user()->id;
				// Auto increment sort column
				$vars['sort']			= \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				if(ucfirst($name) == 'Infotab')
					$class = 'Product\Model_Infotab';
				else
					$class = 'Product\Model_Infotab_' . ucfirst($name);
					
				return new $class;
			}
			
			return static::forge();
		}
		
	}