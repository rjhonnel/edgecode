<?php

	namespace Product;

	class Model_Infotab_File extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'infotab_files';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'infotab_id',
		    'item_id', // before it was "product_id"
		    'infotab_type',
		    'file',
		    'title',
		    'cover',
		    'sort',
		    'item_type', // product, product_category
		);
		
	    protected static $_defaults = array(
		    'infotab_type' => 'infotab',
		);
		
		 /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	    	$query->where('infotab_type', 'infotab');
	    }
	    
		/**
		 * Prep some query values
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
				
			return $vars;
		}
	}