<?php

namespace Product;

class Model_Product extends \Model_Base
{
    // Temporary variable to store some query results
    public static $temp = array();
    
    // Possible values: assigned, not_assigned, null
    public static $filter_by_pricing_group = null;
    
    public static $frontend_only_active = true;

    // Set the table to use
    protected static $_table_name = 'product';

    // List of all columns that will be used on create/update
    protected static $_properties = array(
        'id',
        'type',
        'title',
        'description_intro',
        'description_full',
        'featured',
        'status',
        'active_from',
        'active_to',
        'published_at',
        'user_created',
        'user_updated',
        'sort',
        'code',
        'bar_code',
        'artwork_required',
        'artwork_free_over',
        'supplier',
        'minimum_order',
        'product_type',
    );

    protected static $_defaults = array(
        'type' 	=> 'product',
        'product_type'  => 'simple',
    );

    protected static $_created_at = 'created_at';
    protected static $_updated_at = 'updated_at';

    /**
     * Insert or update product images
     * Certain array structure need to be passed
     * 
     * array(
     * 	0 => array(
     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
     * 		'data'	=> Array of image data to insert
     * 	),
     * 	1 => array(
     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
     * 		'data'	=> Array of image data to insert
     * 	),
     * 	etc.
     * )
     * 
     * @param $images
     */
    public static function bind_images($images = array())
    {
        if(empty($images) || !is_array($images)) return false;

        foreach($images as $key => $image)
        {
            $data = $image['data'];
    		if(is_numeric($image['id']) && $image['id'] > 0)
    		{
    			// Update existing image
    			$item = Model_Product_Image::find_one_by_id($image['id']);
    			$item->set($data);
    		}
    		else
    		{
    			$item = Model_Product_Image::forge($data);
    		}

            $item->save();
        }
    }

    /**
     * Insert or update product files
     * Certain array structure need to be passed
     * 
     * array(
     * 	0 => array(
     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
     * 		'data'	=> Array of file data to insert
     * 	),
     * 	1 => array(
     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
     * 		'data'	=> Array of file data to insert
     * 	),
     * 	etc.
     * )
     * 
     * @param $files
     */
    public static function bind_files($files = array())
    {
        if(empty($files) || !is_array($files)) return false;

        foreach($files as $key => $file)
        {
        	$data = $file['data'];
    		if(is_numeric($file['id']) && $file['id'] > 0)
    		{
    			// Update existing file
    			$item = Model_Product_File::find_one_by_id($file['id']);
    			$item->set($data);
    		}
    		else
    		{
    			$item = Model_Product_File::forge($data);
    		}

            $item->save();
        }
    }

    /**
     * Insert or update product videos
     * Certain array structure need to be passed
     * 
     * array(
     * 	0 => array(
     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
     * 		'data'	=> Array of file data to insert
     * 	),
     * 	1 => array(
     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
     * 		'data'	=> Array of file data to insert
     * 	),
     * 	etc.
     * )
     * 
     * @param $files
     */
    public static function bind_videos($videos = array())
    {
        if(empty($videos) || !is_array($videos)) return false;

        foreach($videos as $key => $video)
        {
        	$data = $video['data'];
    		if(is_numeric($video['id']) && $video['id'] > 0)
    		{
    			// Update existing video
    			$item = Model_Product_Video::find_one_by_id($video['id']);
    			$item->set($data);
    		}
    		else
    		{
    			$item = Model_Product_Video::forge($data);
    		}
            $item->save();
        }
    }

    /**
     * Get Produts that are in discounted groups
     * 
     * @param $group_type	= Group type (standard, discount, etc.)
     * 
     */
    public static function in_group_type($group_type = 'standard')
    {
        $groups = Model_Group::find(function($query) use ($group_type){
            return $query->where('type', $group_type);
        }, 'id');

        if(!empty($groups))
        {
            $groups = '(' . implode(',', array_keys($groups)) . ')';

            $products = Model_Product_To_Groups::find(function($query) use ($groups){
                return $query->where('group_id', 'IN', \DB::expr($groups));
            }, 'product_id');

            if(!empty($products))
            {
                $products = '(' . implode(',', array_keys($products)) . ')';

                return Model_Product::find(function($query) use ($products)
                {
                    $query->where('id', 'IN', \DB::expr($products));
                    $query->order_by('sort', 'asc');

                    return $query;
                }, 'id');
            }
        }

        return array();
    }

    /**
     * Join content tables before any data select
     * 
     * @param $query
     */
    public static function pre_find(&$query)
    {
        $query->where('product.type', 'product');

        // Enable caching for all SELECT queries
        // $query->cached(3600, 'admin.product.', false);
        
        if(static::$filter_by_pricing_group == 'assigned')
        { 
           //$products = static::filter_by_group();
           if(!empty($products['assigned']))
           {
                $query->where('product.id', 'in', array_keys($products['assigned']));
           }
        }
         $table_name = self::$_table_name;
        
        // Select only active products
        $active = \Theme::instance()->active();
        if($active['name'] == 'frontend')
        {
            $query->where(function($query) use ($table_name){
                    $query->where($table_name . '.status', 1);
                    $query->or_where(function($query) use ($table_name){
                        $query->where($table_name . '.status', 2);
                        $query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
                        $query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
                    });
                });
        }
        
        if(static::$filter_by_pricing_group == 'not_assigned')
        {
            
           // $products = static::filter_by_group();
            if(!empty($products['not_assigned']))
            {
                $query->where('product.id', 'not in', array_keys($products['not_assigned']));
            }
        }
    }

    /**
     * Get additional content data selected
     * 
     * @param $result = Query result
     */
    public static function post_find($result)
    {
        $active = \Theme::instance()->active();
        $stock_options = \Config::load('stock-option.db');
        $hold_manage_stock = (isset($stock_options['manage_stock']) ? 1 : 0);
        $hold_hide_out_of_stock = 0;
        if($hold_manage_stock)
        {
            $hold_hide_out_of_stock = (isset($stock_options['hide_out_of_stock']) ? 1 : 0);
        }

        if($result !== null)
        {
            if(is_array($result))
            {
                foreach($result as $key => $item)
                {
                   
                    // It will first check if we already have result in temporary result, 
                    // and only execute query if we dont. That way we dont have duplicate queries

                    // Get product categories
                    $item->get_categories = static::lazy_load(function() use ($item){

                        $categories = Model_Product_To_Categories::find(function($query) use ($item)
                        {
                            return $query->where('product_id', $item->id);
                        }, 'category_id');

                        if(!empty($categories))
                        {
                            $categories = '(' . implode(',', array_keys($categories)) . ')';

                            return Model_Category::find(function($query) use ($categories)
                            {
                                return $query->where('id', 'IN', \DB::expr($categories));
                            }, 'id');
                        }

                        return array();

                    }, $item->id, 'categories');

                    // Get product brands
                    $item->get_brands = static::lazy_load(function() use ($item){

                        $brands = Model_Product_To_Brands::find(function($query) use ($item)
                        {
                            return $query->where('product_id', $item->id);
                        }, 'brand_id');

                        if(!empty($brands))
                        {
                            $brands = '(' . implode(',', array_keys($brands)) . ')';

                            return Model_Brand::find(function($query) use ($brands)
                            {
                                return $query->where('id', 'IN', \DB::expr($brands));
                            }, 'id');
                        }

                        return array();

                    }, $item->id, 'brands');

                    // Get product infotabs
                    $item->get_infotabs = static::lazy_load(function() use ($item){

                        return Model_Product_To_Infotabs::find(function($query) use ($item)
                        {
                            $query->order_by('sort', 'asc');
                            return $query->where('product_id', $item->id);
                        });

                    }, $item->id, 'infotabs');

                    // Get product groups
                    $item->get_groups = static::lazy_load(function() use ($item){

                        $groups = Model_Product_To_Groups::find(function($query) use ($item)
                        {
                            return $query->where('product_id', $item->id);
                        }, 'group_id');

                        if(!empty($groups))
                        {
                            $groups = '(' . implode(',', array_keys($groups)) . ')';

                            return Model_Group::find(function($query) use ($groups, $item)
                            {
                                $query->where('id', 'IN', \DB::expr($groups));
                                $query->order_by('sort', 'asc');

                                return $query;
                            }, 'id');
                        }

                        return array();

                    }, $item->id, 'groups');

                    // Get product groups
                    $item->get_pricing_group = static::lazy_load(function() use ($item){

                        $groups = Model_Product_To_Groups::find(function($query) use ($item)
                        {
                            return $query->where('product_id', $item->id);
                        }, 'group_id');

                        if(!empty($groups))
                        {
                            $groups = '(' . implode(',', array_keys($groups)) . ')';

                            $groups =  Model_Group::find(function($query) use ($groups, $item)
                            {
                                $query->where('type', 'pricing');
                                $query->where('id', 'IN', \DB::expr($groups));
                                $query->order_by('sort', 'asc');

                                return $query;
                            });

                            return !empty($groups) ? $groups[0] : false;
                        }

                        return false;

                    }, $item->id, 'pricing_group', 'object');

                    // Get related products
                    $item->get_related_products = static::lazy_load(function() use ($item){

                        $related = Model_Product_To_Related::find(function($query) use ($item)
                        {
                            return $query->where('product_id', $item->id);
                        }, 'related_id');

                        if(!empty($related))
                        {
                            $related = '(' . implode(',', array_keys($related)) . ')';

                            return Model_Product::find(function($query) use ($related, $item)
                            {
                                $query->where('id', 'IN', \DB::expr($related));
                                $query->where('id', '<>', $item->id);
                                $query->order_by('sort', 'asc');

                                return $query;
                            }, 'id');
                        }

                        return array();

                    }, $item->id, 'related_products');

                    // Get upsell products
                    $item->get_upsell_products = static::lazy_load(function() use ($item){

                        $upsell = Model_Product_To_Upsell::find(function($query) use ($item)
                        {
                            return $query->where('product_id', $item->id);
                        }, 'upsell_id');

                        if(!empty($upsell))
                        {
                            $upsell = '(' . implode(',', array_keys($upsell)) . ')';

                            return Model_Product::find(function($query) use ($upsell, $item)
                            {
                                $query->select(Model_Product_To_Upsell::get_protected('_table_name').'.discount', Model_Product::get_protected('_table_name').'.*');
                                $query->join(Model_Product_To_Upsell::get_protected('_table_name'));
                                $query->on(Model_Product_To_Upsell::get_protected('_table_name').'.upsell_id', '=', Model_Product::get_protected('_table_name').'.id');
                                $query->where(Model_Product::get_protected('_table_name').'.id', 'IN', \DB::expr($upsell));
                                $query->where(Model_Product::get_protected('_table_name').'.id', '<>', $item->id);
                                $query->order_by(Model_Product::get_protected('_table_name').'.sort', 'asc');

                                return $query;
                            }, 'id');
                        }

                        return array();

                    }, $item->id, 'upsell_products');

                    // Get content images
                    $item->get_images = static::lazy_load(function() use ($item){
                        return Model_Product_Image::find(array(
                            'where' => array(
                                'content_id' => $item->id,
                            ),
                            'order_by' => array('sort' => 'asc'),
                        ));
                    }, $item->id, 'images');

                    // Get content files
                    $item->get_files = static::lazy_load(function() use ($item){
                        return Model_Product_File::find(array(
                            'where' => array(
                                'content_id' => $item->id,
                            ),
                            'order_by' => array('sort' => 'asc'),
                        ));
                    }, $item->id, 'files');

                    // Get content videos
                    $item->get_videos = static::lazy_load(function() use ($item){
                        return Model_Product_Video::find(array(
                            'where' => array(
                                'content_id' => $item->id,
                            ),
                            'order_by' => array('sort' => 'asc'),
                        ));
                    }, $item->id, 'videos');

                    // Get content children
                    $item->get_seo = static::lazy_load(function() use ($item){
                        return Model_Product_Seo::find_one_by_content_id($item->id);
                    }, $item->id, 'seo', 'object');

                    // Get attributes
                    $item->get_attributes = static::lazy_load(function() use ($item){
                        return Model_Attribute::find(array(
                            'where' => array(
                                'product_id' => $item->id,
                            ),
                        ));
                    }, $item->id, 'attributes');

                     // Get default attribute
                    $item->get_default_attributes = static::lazy_load(function() use ($item){
                        return Model_Attribute::find(array(
                            'where' => array(
                                'product_id' => $item->id,
                                'default' => 1,
                                'active' => 1,
                            ),
                        ));
                    }, $item->id, 'default_attributes');

                    // Get all active prices
                    $item->get_all_prices = static::lazy_load(function() use ($item){

                        $product_attributes = Model_Attribute::find(function($query) use ($item)
                        {
                            $query->join(Model_Attribute_Price::get_protected('_table_name'));
                            $query->on(Model_Attribute_Price::get_protected('_table_name').'.product_attribute_id', '=', Model_Attribute::get_protected('_table_name').'.id');
                            $query->where('product_id', $item->id);
                            $query->and_where('attributes', '!=', '');
                            $query->and_where('active', 1);
                            return $query;
                        }, 'id');
                        return $product_attributes;

                    }, $item->id, 'unit_price');
                    
                    // Get prices for product
                    $item->get_prices = static::lazy_load(function() use ($item){

                        $product_attributes = Model_Attribute::find(function($query) use ($item)
                        {
                            $query->join(Model_Attribute_Price::get_protected('_table_name'));
                            $query->on(Model_Attribute_Price::get_protected('_table_name').'.product_attribute_id', '=', Model_Attribute::get_protected('_table_name').'.id');
                            $query->where('product_id', $item->id);
                            return $query;
                        }, 'id');
                        return $product_attributes;

                    }, $item->id, 'unit_price');

                    // Get unit price
                    $item->get_unit_price = static::lazy_load(function() use ($item){

                        $product_attributes = Model_Attribute::find(function($query) use ($item)
                        {
                            $query->join(Model_Attribute_Price::get_protected('_table_name'));
                            $query->on(Model_Attribute_Price::get_protected('_table_name').'.product_attribute_id', '=', Model_Attribute::get_protected('_table_name').'.id');
                            $query->where('product_id', $item->id);
                            $query->and_where('type', 'unit_price');
                            $query->and_where('attributes', '!=', '');
                            $query->and_where('active', 1);
                            $query->limit(1);
                            return $query;
                        }, 'id');
                        return $product_attributes;

                    }, $item->id, 'unit_price');

                     // Get sale price
                    $item->get_sale_price = static::lazy_load(function() use ($item){

                        $product_attributes = Model_Attribute::find(function($query) use ($item)
                        {
                            $query->join(Model_Attribute_Price::get_protected('_table_name'));
                            $query->on(Model_Attribute_Price::get_protected('_table_name').'.product_attribute_id', '=', Model_Attribute::get_protected('_table_name').'.id');
                            $query->where('product_id', $item->id);
                            $query->and_where('type', 'sale_price');
                            $query->and_where('attributes', '!=', '');
                            $query->and_where('active', 1);
                            $query->limit(1);
                            return $query;
                        }, 'id');
                        return $product_attributes;

                    }, $item->id, 'sale_price');

                     // Get default price (RRP or Sale price)
                    $item->get_default_price = static::lazy_load(function() use ($item){

                        $out = array(0);
                        $product_attributes = Model_Attribute::find(function($query) use ($item)
                        {
                            $query->join(Model_Attribute_Price::get_protected('_table_name'));
                            $query->on(Model_Attribute_Price::get_protected('_table_name').'.product_attribute_id', '=', Model_Attribute::get_protected('_table_name').'.id');
                            $query->where('product_id', $item->id);
                            $query->and_where('type', 'sale_price');
                            $query->and_where('attributes', '!=', '');
                            $query->and_where('active', 1);
                            $query->and_where('default', 1);
                            $query->limit(1);
                            return $query;
                        }, 'id');

                        if($product_attributes)
                        {
                            $obj = reset($product_attributes);
                            if($obj->price > 0)
                            {
                                $out[0] = $obj->price;
                                $out[1] = 'sale_price';
                            }
                            else
                            {
                                $out[0] = $obj->retail_price;
                                $out[1] = 'retail_price'; 
                            }
                        }
                        return $out;

                    }, $item->id, 'default_price');
                    
                    // Get active attribute group
                    $item->get_active_attribute_group = static::lazy_load(function() use ($item){
                        if($attribute = Model_Attribute::find_one_by_product_id($item->id))
                        {
                            return $attribute->attribute_group;
                        }
                        return false;
                    }, $item->id, 'active_attribute_group');

                    // Get deals
                    $item->get_deals = static::lazy_load(function() use ($item){
                        return Model_Deals::find(array(
                            'where' => array(
                                'product_id' => $item->id,
                            ),
                        ));
                    }, $item->id, 'deals');

                    // Get attributes
                    $item->get_attributes = static::lazy_load(function() use ($item){
                        return Model_Attribute::find(array(
                            'where' => array(
                                'product_id' => $item->id,
                            ),
                        ));
                    }, $item->id, 'attributes');
                    
                     // Get product data
                    $item->get_data = static::lazy_load(function() use ($item){
                        return \Product\Model_Product::product_data($item);
                    }, $item->id, 'data');

                    // only apply stock option function in frontend
                    if($active['name'] == 'frontend')
                    {
                        $hold_attribute = isset($item->default_attributes[0]) ? $item->default_attributes[0] : ( isset($item->attributes[0]) ? $item->attributes[0] : false);

                        $get_stock_quantity = isset($hold_attribute->stock_quantity)?$hold_attribute->stock_quantity :0;
                        //unset product if hide_out_of_stock is enable and product's stock is less than 1 
                        if($get_stock_quantity < 1 && $hold_hide_out_of_stock == 1)
                        {
                            unset($result[$key]);
                        }
                    }
                }
            }
        }

        // return the result
        return $result; 
    }

    /**
     * Load function result only once and than remember 
     * it in $temp variable for later use
     * 
     * @param $closure		= Function for returning data
     * @param $id			= Item ID
     * @param $type			= Name for additional data
     * @param $return		= Return type, either array or object (empty result will be casted in that type)
     */
    private static function lazy_load($closure, $id, $type, $return = 'array')
    {
        return function() use ($closure, $id, $type, $return){
            if(!isset(\Product\Model_Product::$temp['lazy.'.$id.$type]))
            {
                \Product\Model_Product::$temp['lazy.'.$id.$type] = $closure();

                // Make sure we always return array
                if($return == 'array')
                {
                    if(!is_array(\Product\Model_Product::$temp['lazy.'.$id.$type])) 
                        \Product\Model_Product::$temp['lazy.'.$id.$type] = array();
                }
                else
                {
                    if(!is_object(\Product\Model_Product::$temp['lazy.'.$id.$type]) && !is_bool(\Product\Model_Product::$temp['lazy.'.$id.$type])) 
                        \Product\Model_Product::$temp['lazy.'.$id.$type] = new \stdClass();
                }
            }

            return \Product\Model_Product::$temp['lazy.'.$id.$type];
        };
    }

    /**
     * Validate Model fields
     * 
     * @param $factory = Validation name, if you want to have multiple validations
     */
    public static function validate($factory)
    {
        $val = \Validation::forge($factory);
        $val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);
        $val->add('cat_ids', 'Product Category')->add_rule('required');
        if(\Input::post('status', 1) == 2)
        {
            // Only set if status is "Active in period"
            $val->add('active_from', 'Active from')->add_rule('required_with', 'active_to')->add_rule('valid_date', 'd/m/Y');
            $val->add('active_to', 'Active to')->add_rule('required_with', 'active_from')->add_rule('valid_date', 'd/m/Y');
        }

        return $val;
    }

    /**
     * Save users that commited insert/update operations
     * 
     * @param $vars
     */
    protected function prep_values($vars)
    {
        // Set user who is created/updated item
        if($this->is_new())
        {
            $vars['user_created']	= \Sentry::user()->id;
            // Auto increment sort column
            $vars['sort']			= \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
        }
        else
            $vars['user_updated'] = \Sentry::user()->id;

        return $vars;
    }

    /**
     * Return other models instances, like Images, Files, etc.
     *
     * @param $name = Model Name
     */
    public static function factory($name = false)
    {
        if(is_string($name))
        {
            if(ucfirst($name) == 'Product')
                $class = 'Product\Model_Product';
            else
                $class = 'Product\Model_Product_' . ucfirst($name);

            return new $class;
        }

        return static::forge();
    }

    /**
     * Get product by slug
     * 
     * @param $slug
     */
    public static function get_by_slug($slug = false)
    {
        if($seo = \Product\Model_Product_Seo::find_one_by_slug($slug))
        {
            return Model_Product::find_one_by_id($seo->content_id);
        }

        return false;
    }

    /**
     * Get product attributes
     * 
     * @param $product  obj Model_Product
     * @param $active   true, false, all
     * @return $out     array(array($obj))
     */
    public static function get_product_attributes($product = false, $active = true)
    {
        $out = array();
        if(!empty($product->attributes))
        {
            // Decode all product atributes
            $product_attributes = array();
            foreach ($product->attributes as $k => $attribute)
            {
                $decoded = json_decode($attribute->attributes, true);
                if($decoded) 
                {
                    $product_attributes[$k] = $decoded;
                    $product_attributes_id[$k] = $attribute->id;
                }
            }

            // Create array with attributes id
            $attributes = array();
            if(!empty($product_attributes))
            {
                foreach($product_attributes as $v)
                {
                    if(is_array($v))
                    {
                        foreach ($v as $kk => $vv)
                        {
                            $attributes[] = $kk;
                        }
                    }
                }
            }

            // Select used attributes for this product
            $used_attributes = array();
            if(!empty($attributes))
            {
                // Find used attributes
                $used_attributes = \Attribute\Model_Attribute::find(array(
                    'where' => array(
                        array('id', 'in', array_unique($attributes))
                    ),
                    'order_by' => array(
                        'sort' => 'asc'
                    )
                ), 'id');
            }

            if(!empty($used_attributes) && !empty($product_attributes))
            {
                foreach ($product_attributes as $k => $v)
                {
                    if(is_array($v))
                    {
                        foreach($v as $kk => $vv)
                        {
                            if(isset($used_attributes[$kk]))
                            {
                                $options = $used_attributes[$kk]->options;
                                if(!empty($options) && isset($options[$vv]))
                                {
                                    if($active !== 'all')
                                    {
                                        if($active)
                                        {
                                            if($product->attributes[$k]->active != 1) continue;
                                        } 
                                        else
                                        {
                                            if($product->attributes[$k]->active != 1) continue;
                                        }
                                    }

                                    $tmp_obj = new \stdClass();
                                    $tmp_obj->sort = $used_attributes[$kk]->sort;
                                    $tmp_obj->attribute = $used_attributes[$kk];
                                    $tmp_obj->option = $options[$vv];
                                    $tmp_obj->product_attribute = $product->attributes[$k];
                                    $out[$product_attributes_id[$k]][] = $tmp_obj;
                                }
                            }
                        }
                        if(isset($out[$product_attributes_id[$k]]))
                        {
                            usort($out[$product_attributes_id[$k]], array('\Product\Model_Product', 'sort_product_attributes'));
                        }
                    }
                }
            }
        }
        return $out;
    }

    /**
     * Sort product attributes
     * use with usort
     * usort($array, array("Class", "sort_product_attributes"));
     * 
     */
    public static function sort_product_attributes( $a, $b) {
        return $a->attribute->sort == $b->attribute->sort ? 0 : ( $a->attribute->sort > $b->attribute->sort ) ? 1 : -1;
    }
    
    /**
     * Sort product by price
     * use with usort
     * usort($array, array("Class", "sort_product_attributes"));
     * 
     */
    public static function sort_products( $a, $b) {
        return $a->default_price[0] == $b->default_price[0] ? 0 : ( $a->default_price[0] > $b->default_price[0] ) ? 1 : -1;
    }
    
    public static function get_sorted_products($products)
    {
        usort($products, array('\Product\Model_Product', 'sort_products'));
        return $products;
    }


    /**
    * Get product data (code, price, attributes, images)
    * 
    * @access  public
    * @param   $product   object
    * @return  
    */
   public static function product_data($product = false, $attributes_json = null, $post_select = false, $attributeid = false)
   {
       if(!$product) return;

       $out['current_attributes'] = null;
       $out['code']               = null;
       $out['images']             = null;
       $out['select']             = null;
       $out['select_name']        = null;
       $out['sx']                 = null; // select (attribute) order
       $out['retail_price']       = null;
       $out['sale']               = null;
       $out['price']              = 0;
       $out['price_type']         = null;

       $current_attributes = array();
       $select             = array();
       $select_name        = array();
       $sx                 = array();
       $options            = array();

       if(json_decode($attributes_json) != null)
       {
           $current_attributes = Model_Attribute::find(array(
               'where' => array(
                   'attributes' => $attributes_json,
                   'product_id' => $product->id,
                   'active' => 1
               ),
               'limit' => 1,
           ));
       }

       //get attributes per product
       $prod_attributes = \Product\Model_Attribute::find_by_product_id( $product->id );

       $a_attribute_ids = array();
       if ($prod_attributes) {
           foreach($prod_attributes as $attr_id){
                array_push($a_attribute_ids, $attr_id->id);
           }
       }

       // NRB-Gem: if current URL is Pricing Group -> Products
       // Get pricing_attribute_price from current Pricing Group
       // Products can belong to multiple Pricing Groups
       $a_groups = \Sentry::user()->groups();
       if ($a_attribute_ids) {
            if (\Sentry::check()) {
                $a_cond = array(
                    array('product_attribute_id', 'in', $a_attribute_ids)
                );
                if (\Uri::segment(2) != 'admin' && \Sentry::user()->groups() && $a_groups[0]) {
                    $a_cond['user_group_id'] = $a_groups[0]['id'];
                }
                if (\Uri::segment(3) == 'group' && \Uri::segment(4) == 'products_assigned') {
                    $a_cond['pricing_group_id'] = \Uri::segment(5);
                }
                $product_sale_price = \Product\Model_Attribute_Price::find(array(
                    'where' => $a_cond
                ));
            }
        }
        // NRB-Gem: check if logged in user can buy product for discounted rate
        if (\Uri::segment(2) != 'admin' && \Sentry::user()->groups() && !\Sentry::user()->is_admin() && $a_groups[0]) {
            $o_product_group_options = \DB::select_array(array('able_to_buy'))
                            ->from('product_group_options')
                            ->join('product_to_groups')
                            ->on('product_group_options.product_group_id', '=', 'product_to_groups.group_id')
                            ->where('product_group_options.user_group_id', $a_groups[0]['id'])
                            ->where('able_to_buy', 1)
                            ->where('product_to_groups.product_id', $product->id)
                            ->execute();
            $a_result = $o_product_group_options->as_array();
            $product_sale_price = $a_result && $a_result[0]['able_to_buy'] ? $product_sale_price : false;
        }

        // Get all product attributes
       $product_attributes = Model_Product::get_product_attributes($product);

       // Product with attributes
       if($product_attributes)
       {
           // Set current attribute
           if($current_attributes && isset($product_attributes[$current_attributes[0]->id]))
           {
               $current_attributes = $product_attributes[$current_attributes[0]->id];
           }

           elseif($post_select && (count($post_select) > 1))
           {
               foreach($post_select as $sk => $sv)
               {
                   if(!isset($post_select_pop)) 
                   {
                       $post_select_pop = $post_select;
                       array_pop($post_select_pop);
                   }

                   $count = count($post_select_pop);
                   foreach($product_attributes as $pk => $pv)
                   {
                       $i = 0;
                       $not_exists = array();
                       foreach($pv as $pkk => $pvv)
                       {
                           if($i >= $count) continue;
                           if(!isset($post_select_pop[$pvv->attribute->id]) || $post_select_pop[$pvv->attribute->id] != $pvv->option->id)
                           {
                               $not_exists[] = true;
                           }
                           $i++;
                       }
                       if(empty($not_exists)) 
                       {
                           $current_attributes = $product_attributes[$pk];
                           break 2;
                       }
                   }
                   if(empty($current_attributes) && $count > 1)
                   {
                       array_pop($post_select_pop);
                   }
               }
               if(empty($current_attributes)) $current_attributes = reset($product_attributes);
           }

           elseif($product->default_attributes)
           { 
               $current_attributes = $product_attributes[$product->default_attributes[0]->id];
           }
           else
           { 
               $current_attributes = reset($product_attributes);
           }

           foreach($current_attributes as $k => $v)
           {
               $options[] = $v->option->id;
           }

           foreach ($product_attributes as $k => $v)
           {
               if(is_array($v))
               {
                   foreach ($v as $kk => $vv)
                   {
                       $select_name[$vv->attribute->id] = $vv->attribute->name != '' ? $vv->attribute->name : $vv->attribute->title ;
                       $select_tmp[$k][$vv->attribute->id][$vv->option->id] = $vv->option->title;

                       foreach($options as $option_key => $option_value)
                       {
                           if($kk == $option_key)
                           {
                               $select[$vv->attribute->id][$vv->option->id] = $vv->option->title;
                               if($vv->option->id != $option_value) continue 3;
                           }
                       }
                   }
               }
           }

           if($select)
           {
               $sx = array_keys($select_name);
           }
           
           $b_use_original_price = false;
           if(!empty($current_attributes))
           {
               $attr_obj                  = $current_attributes[0]->product_attribute; 
               $out['current_attributes'] = $current_attributes;
               $out['code']               = $attr_obj->product_code;
               if(!empty($attr_obj->images)) $out['images'] = $attr_obj->images;
               $out['select']             = $select;
               $out['select_name']        = $select_name;
               $out['sx']                 = $sx;
               $out['stock_quantity']       = $attr_obj->stock_quantity;
               $out['delivery_charge']       = $attr_obj->delivery_charge;
               $out['retail_price']       = $attr_obj->retail_price;
                
               // NRB-Gem: changed condition from: (!empty($product_sale_price) && $product_sale_price[0]->price != 0) 
               // fix for attributes with sale price are not displayed
               if(!empty($product_sale_price))
               {
               		$out['with_attr'] = array('attribute' => array());
                    foreach($product_sale_price as $with_attr){
                        $out['with_attr']['able_discount'][$with_attr->product_attribute_id]   = $with_attr->able_discount;
                        if ($out['with_attr']['able_discount'][$with_attr->product_attribute_id]) {
                            $out['with_attr']['discount'][$with_attr->product_attribute_id]   = $with_attr->discount;
                            $out['with_attr']['price'][$with_attr->product_attribute_id]      = $with_attr->price;
                        }
                        $out['with_attr']['attribute'][$with_attr->product_attribute_id] = '';
                        foreach($product_attributes as $o_prod_attr){
                            $a_attr = array();
                            foreach($o_prod_attr as $o_attr) {
                                if($with_attr->product_attribute_id == $o_attr->product_attribute->id){
                                    $a_attr[] = $o_attr->option->title;
                                }
                            }
                            $out['with_attr']['attribute'][$with_attr->product_attribute_id] .= implode(' | ', $a_attr);
                        }
                        // NRB-Gem: do not display inactive attribute
                        if (empty($out['with_attr']['attribute'][$with_attr->product_attribute_id])) {
                            unset($out['with_attr']['attribute'][$with_attr->product_attribute_id]);
                            unset($out['with_attr']['able_discount'][$with_attr->product_attribute_id]);
                            unset($out['with_attr']['discount'][$with_attr->product_attribute_id]);
                            unset($out['with_attr']['price'][$with_attr->product_attribute_id]);
                        }
                    }
                    // NRB-Gem: get default prices
                    foreach($prod_attributes as $attr){
                        if (isset($out['with_attr']['discount'][$attr->id]) && $out['with_attr']['discount'][$attr->id] > 0) {
                            $out['with_attr']['price'][$attr->id] = $attr->retail_price - ($attr->retail_price * ($out['with_attr']['discount'][$attr->id] / 100));
                        } else {
                            if (isset($out['with_attr']['price'][$attr->id]) && !$out['with_attr']['price'][$attr->id]) {
                                $out['with_attr']['price'][$attr->id] = $attr->sale_price ? $attr->sale_price : $attr->retail_price;      
                            }
                        }
                    }

                   $out['sale'] = \Product\Model_Product::calculate_discount( $product, $product_sale_price[0]->discount );
                   $out['price'] = \Product\Model_Product::calculate_discount( $product, $product_sale_price[0]->price );
                   $out['price_type'] = 'sale_price';

                    // NRB-Gem: Set discounted rate from product_attribute_price Product attributes
                    if ($attributes_json) {
                        foreach($current_attributes as $o_attribute) {
                            if($o_attribute->product_attribute->attributes == $attributes_json) {
                                if($out['with_attr']['able_discount'][$o_attribute->product_attribute->id]){
                                    if ($out['with_attr']['discount'][$o_attribute->product_attribute->id] > 0) {
                                        $out['price'] = $out['retail_price'] - ($out['retail_price'] * ($out['with_attr']['discount'][$o_attribute->product_attribute->id] / 100));
                                    } else {
                                        $out['price'] = $out['with_attr']['price'][$o_attribute->product_attribute->id];   
                                    }
                                } else {
                                    $b_use_original_price = true;
                                }
                            }
                        }
                    }
               }
               else
               {
                   $out['price'] = \Product\Model_Product::calculate_discount( $product, $attr_obj->retail_price );
                   $out['price_type'] = 'retail_price';
                   $b_use_original_price = true;
               }
                // NRB-Gem: Display original price if Discount price is not enabled (used by non-ajax request)
                if ((\Uri::segment(1) == 'product' || \Uri::segment(1) == 'page' || \Uri::segment(1) == 'search') && !$attributes_json) {
                    $a_prod_attr_keys = array_keys($product_attributes);
                    $attr_id = $a_prod_attr_keys[0];
                    if (isset($out['with_attr']['able_discount'][$attr_id])) {
                        if (isset($out['with_attr']['discount']) && $out['with_attr']['discount'][$attr_id] > 0) {
                            $out['price'] = $out['retail_price'] - ($out['retail_price'] * ($out['with_attr']['discount'][$attr_id] / 100));
                        } else {
                            $out['price'] = isset($out['with_attr']['price']) ? $out['with_attr']['price'][$attr_id] : '';
                        }
                    } else {
                        $b_use_original_price = true;
                        $attributes_json = true;
                    }
                }
                // NRB-Gem: Set original discounted rate for Product attributes
                if ($b_use_original_price && $attributes_json){
                    if ($attr_obj->sale_price > 0) {
                        $out['price'] = $attr_obj->sale_price;
                        $out['price_type'] = 'sale_price';
                    } else {
                        $out['price'] = $attr_obj->retail_price;
                        unset($out['price_type']);
                    }
                }
           }
       }
       // Product with no-attributes selected
       // NRB-Gem: removed checking
       else//if(isset($product->attributes[0]) && $product->attributes[0]->attribute_group_id == 0)
       {
            $p = isset($product->attributes[0])?$product->attributes[0]:false;
            $out['current_attributes'] = null;
            if($p)
            {
                $out['code']               = $p->product_code;
                $out['images']             = $p->images;
                $out['retail_price']       = $p->retail_price;
                $out['stock_quantity']       = $p->stock_quantity;
                $out['delivery_charge']     = $p->delivery_charge;
            }
            else
            {
                $out['code']               = false;
                $out['images']             = false;
                $out['retail_price']       = false;
                $out['stock_quantity']       = false;
                $out['delivery_charge']       = 0;
            }

            $out['able_discount']      = isset($product_sale_price[0]) ? $product_sale_price[0]->able_discount : 0;

            if(!empty($product_sale_price) && $product_sale_price[0]->price != 0)  
            {
                if ($out['able_discount']) {
                    $out['sale'] = \Product\Model_Product::calculate_discount( $p, $product_sale_price[0]->discount );
                    $out['price'] = \Product\Model_Product::calculate_discount( $p, $product_sale_price[0]->price);
                }
                $out['price_type'] = 'sale_price';
            }
            else
            {
                $out['sale'] = isset($product_sale_price[0]) ? $product_sale_price[0]->discount : 0;
                $out['price'] = \Product\Model_Product::calculate_discount( $product, ($p?$p->retail_price:false) );
                $out['price_type'] = 'retail_price';
            }
            // NRB-Gem: Display original price if Discount price is not enabled
            if ($out['able_discount']) {
                if ($product_sale_price[0]->discount > 0) {
                    $out['price'] = $p->retail_price - ($p->retail_price * ($product_sale_price[0]->discount / 100));
                }
            } else {
                if($p)
                {
                    if ($p->sale_price > 0) {
                        $out['price'] = $p->sale_price;
                        $out['price_type'] = 'sale_price';
                    } else {
                        unset($out['price_type']);
                        $out['price'] = $p?$p->retail_price:false;
                    }
                } else {
                    unset($out['price_type']);
                    $out['price'] = $p?$p->retail_price:false;
                }
            }
       }

       $prod_category = Model_Product_To_Categories::find_by_product_id($product->id);
       $category = \Product\Model_Category::find_one_by_id($prod_category[0]->category_id);

       $out['category_id'] = isset($prod_category[0]->category_id) ? $prod_category[0]->category_id : null;
       $out['children'] = \Product\Model_Category::find_by('parent_id',$prod_category[0]->category_id);
       $out['category'] = isset($category->title) ? $category->title : null;
       $out['parent_id'] = isset($category->parent_id) ? $category->parent_id : null;

       // Product without attributes
       if(empty($out['current_attributes']))
       {
           $out['code'] = $product->code;
       }

       // Set images from product if there is no images in product attribute
       if(empty($out['images']) && !empty($product->images))  $out['images'] = $product->images;
        
       return $out;

   }
   

   public static function calculate_discount( $item, $total_price = 0 ) {
  
        $user = false;

        if (\Sentry::check())
        {
            $user = \Sentry::user();
        }

        if( $user ) {

            $user_group_id = $user['groups'][0]['id'];

            $product_groups = \Product\Model_Product_To_Groups::find_by_product_id( $item->id );

                    
            $_discount = 0;

            if ($product_groups) 
            {
	            foreach( $product_groups as $group ) {

	                $retail__discounts = \Product\Model_Group_Discounts::find_by(array('user_group_id' => $user_group_id, 'product_group_id' => $group->group_id ), null, null, null);

	                $sale__discount = \Product\Model_Group_Options::find_by(array('user_group_id' => $user_group_id, 'product_group_id' => $group->group_id ), null, null, null);
	                

	                if( $item->price_type == 'sale_price' ) {
	                    
	                    $the_discount = $sale__discount[0]->sale_discount;
	                } else {
	                    $the_discount = $retail__discounts ? $retail__discounts[0]->discount : 0;
	                }


	                $_discount = (int) $_discount + $the_discount;
	            }
            }



            $total_price = $total_price ? $total_price - ( ( (int)$_discount / $total_price ) * 100 ) : 0;
        }

        return $total_price;

    }


   // My Products
   public static function filter_by_group()
   {
        $out['assigned'] = array();
        $out['not_assigned'] = array();
        $products_assigned = array();
        $products_not_assigned = array();
        $in_groups = array();
        $not_in_groups = array();
        
        if(parent::check_logged()) 
        {
            $groups = Model_Group_Options::find(array(), 'product_group_id');
            
            $user = \Sentry::user();
            $user_group = $user->groups();
            $user_group = $user_group[0];

            if(isset($user_group['id']))
            {
                $in_groups = Model_Group_Options::find(array('where' => array(
                    'user_group_id' => $user_group['id'],
                    'able_to_buy' => 1
                )), 'product_group_id');

                $not_in_groups = Model_Group_Options::find(array('where' => array(
                    array('user_group_id', '!=', $user_group['id'])
                )), 'product_group_id');
                
                if(!is_array($in_groups)) $in_groups = array();
                if(!is_array($not_in_groups)) $not_in_groups = array();
                
                $not_in_groups = array_diff_key ($not_in_groups, $in_groups);
            }
        }
        else
        {
            $not_in_groups = Model_Group_Options::find(array(), 'product_group_id');
        }
        
        if(!empty($in_groups))
        {
            $groups = '(' . implode(',', array_keys($in_groups)) . ')';

            $products_assigned = Model_Product_To_Groups::find(function($query) use ($groups)
            {
                $query->where('group_id', 'IN', \DB::expr($groups));
                return $query;
            }, 'product_id');
        }
                
        if(!empty($not_in_groups))
        {
            $groups = '(' . implode(',', array_keys($not_in_groups)) . ')';

            $products_not_assigned = Model_Product_To_Groups::find(function($query) use ($groups)
            {
                $query->where('group_id', 'IN', \DB::expr($groups));
                return $query;
            }, 'product_id');
        }
        
        $out['assigned'] = $products_assigned;
        $out['not_assigned'] = $products_not_assigned;
        
        return $out;
   }
   
   public static function my_categories()
   { 
        $products = static::filter_by_group();
        $categories = array();
        if(!empty($products['assigned']))
        {  
            $categories = Model_Product_To_Categories::find(array(
                 'where' => array(
                     array('product_id', 'in', array_keys($products['assigned'])),
                 ),
            ), 'category_id');
            
             
            if($categories)
            {
                $categories = Model_Category::find(array(
                    'where' => array(
                        array('id', 'in', array_keys($categories)),
                    ),
               ), 'id');
            }
        }
        return $categories;
   }

   public static function get_cover_image($item)
   {
        // var_dump($item->default_attributes[0]->retail_price);

        // Get content cover image
        $cover_image = Model_Product_Image::find(array(
            'where' => array(
            'content_id' => $item->id,
            'cover' => 1,
        ),
            'order_by' => array('sort' => 'asc'),
        ));
        if(isset($cover_image[0]))
        {
            return $cover_image[0];
        }
        else
        {
            $images_list = Model_Product_Image::find(array(
                    'where' => array(
                    'content_id' => $item->id,
                ),
                'order_by' => array('sort' => 'asc'),
            ));
            if(isset($images_list[0]))
            {
                return $images_list[0];
            }
        }
        return false;
   }
		
}