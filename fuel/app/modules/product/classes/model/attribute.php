<?php

	namespace Product;

	class Model_Attribute extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
        
        public static $user_group = null;
        public static $tier_price = null;
	
	    // Set the table to use
	    protected static $_table_name = 'product_attributes';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'product_id',
		    'attribute_group_id',
		    'attributes',
            'product_code',
            'retail_price',
            'active',
            'default',
            'sale_price',
            'stock_quantity',
            'delivery_charge'
		);
		
		protected static $_defaults = array(
            'active' => 1,
            'default' => 0,
        );
	    
	    /**
	     * Insert or update page images
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	    	if(empty($images) || !is_array($images)) return false;
	    	
	    	foreach($images as $key => $image)
	    	{
	    		$item = Model_Attribute_Image::forge($image['data']);
	    		
	    		if(is_numeric($image['id']) && $image['id'] > 0)
	    		{
	    			// Update existing image
	    			$item->set(array('id' => $image['id']));
	    			$item->is_new(false);
	    		}
	    		
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
                    $tier_price = static::$tier_price;
                    $user_group = static::$user_group;
                    
		    		foreach($result as $item)
		    		{
                        // Get all prices
						$item->get_prices = static::lazy_load(function() use ($item){
                            return \Product\Model_Attribute_Price::find(function($query) use($item){
                                return $query->where('product_attribute_id', $item->id);
                            });
		    			}, $item->id, 'prices');
                        
                        // Get unit price
						$item->get_unit_price = static::lazy_load(function() use ($item, $user_group){
                            return \Product\Model_Attribute_Price::find(function($query) use($item, $user_group){
                                $query->where('product_attribute_id', $item->id);
                                $query->and_where('type', 'unit_price');
                                return $query->and_where('user_group_id', $user_group);
                            });
		    			}, $item->id, 'unit_price');
                        
                         // Get sale price
						$item->get_sale_price = static::lazy_load(function() use ($item, $user_group){
                            return \Product\Model_Attribute_Price::find(function($query) use($item, $user_group){
                                $query->where('product_attribute_id', $item->id);
                                $query->and_where('type', 'sale_price');
                                return $query->and_where('user_group_id', $user_group);
                            });
		    			}, $item->id, 'sale_price');
                        
                        // Get tier price
						$item->get_tier_price = static::lazy_load(function() use ($item, $user_group, $tier_price){
                            return \Product\Model_Attribute_Price::find(function($query) use($item, $user_group, $tier_price){
                                $query->where('product_attribute_id', $item->id);
                                $query->and_where('type', 'tier_price');
                                $query->and_where('user_group_id', $user_group);
                                return $query->and_where('product_group_discount_id', $tier_price);
                            });
		    			}, $item->id, 'tier_price');
                        
                        // Get content images
						$item->get_images = static::lazy_load(function() use ($item){
		    				return \Product\Model_Attribute_Image::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'images');
                        
                        // Get content images
						$item->get_cover_image = static::lazy_load(function() use ($item){
		    				return \Product\Model_Attribute_Image::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							        'cover' => 1,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'cover_image');
                        
                        // Get attribute group
						$item->get_attribute_group = static::lazy_load(function() use ($item){
                            // Return 0 for no attribute group
                            if($item->attribute_group_id == 0) 
                            {
                                $tmp = new \stdClass();
                                $tmp->title = 'No Attributes';
                                return array(0 => $tmp);
                            }
                            // Return null for not yet selected group
                            return \Attribute\Model_Attribute_Group::find(function($query) use($item){
                                $query->where('id', $item->attribute_group_id);
                            });
		    			}, $item->id, 'attribute_group');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Product\Model_Attribute::$temp['lazy.'.$id.$type]))
		    	{
		    		\Product\Model_Attribute::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Product\Model_Attribute::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Attribute::$temp['lazy.'.$id.$type] = array();
		    		}
                    else
		    		{
		    			if(!is_object(\Product\Model_Attribute::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Attribute::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Product\Model_Attribute::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('product_id', 'Product ID')->add_rule('required');
			$val->add('attribute_group_id', 'Attribute Group ID')->add_rule('required');
            $val->add('attributes', 'Attributes')->add_rule('required');
	
			return $val;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				if(ucfirst($name) == 'Category')
					$class = 'Product\Model_Attribute';
				else
					$class = 'Product\Model_Attribute_' . ucfirst($name);
				
				return new $class;
			}
			
			return static::forge();
		}
        
        public static function set_user_group($id = null)
        {
            if($id) static::$user_group = $id;
            
            if(\Input::get('user_group')) static::$user_group = \Input::get('user_group');
        }
        
        public static function set_tier_price($id = null)
        {
            if($id) static::$tier_price = $id;
            
            if(\Input::get('tier_price')) static::$tier_price = \Input::get('tier_price');
        }
        
        /**
		 * Get attribute combination
		 *
		 * @param $combination  json | array
         * @return              $out
		 */
        public static function get_combination($combination, $return = 'array')
        {
            $out = array();
            if(!is_array($combination)) 
            {
                $combination = json_decode($combination, true);
                if($combination == null) return null;
            }
            
            $ids = array_keys($combination);
            
            $attributes = \Attribute\Model_Attribute::find(array(
                'where' => array(
                    array('id', 'in', $ids),
                ),
                'order_by' => array(
                    'sort' => 'asc',
                ),
            ));
            
            if($attributes)
            {
                foreach ($attributes as $attribute)
                {
                    if($attribute->options)
                    {
                        foreach($attribute->options as $option)
                        {
                            if(isset($combination[$attribute->id]) && $combination[$attribute->id] == $option->id)
                            {
                                //$out[] = $attribute->title . ': ' . $option->title;
                                $out[$attribute->title] = $option->title;
                            }
                        }
                    }
                }
            }
            
            if($return == 'string')
            {
                $array = array();
                foreach ($out as $key => $value)
                {
                    $array[] = $key . ': ' . $value;
                }
                return implode(' | ', $array);
            }
            else
            {
               return $out;
            }
        }


        /*
		* Update stock quantity after saving order
        */
        public static function update_stock_quantity($combination, $quantity)
        {
            $ids = $combination?array_keys($combination):null;
            $ids  = $ids == null ? array(null) : array_keys($ids);
            
            $attributes = \Attribute\Model_Attribute::find(array(
                'where' => array(
                    array('id', 'in', $ids),
                ),
                'order_by' => array(
                    'sort' => 'asc',
                ),
            ));
            
            if($attributes)
            {
                foreach ($attributes as $attribute)
                {
                    if($attribute->options)
                    {
                        foreach($attribute->options as $option)
                        {
                            if(isset($combination[$attribute->id]) && $combination[$attribute->id] == $option->id)
                            {
                                //$out[] = $attribute->title . ': ' . $option->title;
                                // $out[$attribute->title] = $option->title;
                                \Product\Model_Attribute::delete($option->id);
                            }
                        }
                    }
                }
            }
        }

        public static function get_product_code($id) {
        	$product_attribute = \Product\Model_Attribute::find(array(
                'where' => array(
                    array('id', '=', $id),
                ),
            ));
        	return $product_attribute[0]['product_code'];
        }
                
		
	}