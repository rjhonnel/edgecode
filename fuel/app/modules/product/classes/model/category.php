<?php

	namespace Product;

	class Model_Category extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
        
        public static $sort = 'asc';
        
        public static $sorting = array();
	
	    // Set the table to use
	    protected static $_table_name = 'product_categories';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'parent_id',
		    'title',
		    'description_intro',
		    'description_full',
		    'status',
		    'active_from',
		    'active_to',
		    'published_at',
		    'user_created',
		    'user_updated',
		    'sort',
		    'user_group',
		);
		
		protected static $_defaults = array();
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    /**
	     * Insert or update page images
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	    	if(empty($images) || !is_array($images)) return false;
	    	
	    	foreach($images as $key => $image)
	    	{
	    		$data = $image['data'];
	    		if(is_numeric($image['id']) && $image['id'] > 0)
	    		{
	    			// Update existing image
	    			$item = Model_Category_Image::find_one_by_id($image['id']);
	    			$item->set($data);
	    		}
	    		else
	    		{
	    			$item = Model_Category_Image::forge($data);
	    		}

	    		$item->save();
	    	}
	    }

    /**
     * Insert or update Category files
     * Certain array structure need to be passed
     * 
     * array(
     * 	0 => array(
     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
     * 		'data'	=> Array of file data to insert
     * 	),
     * 	1 => array(
     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
     * 		'data'	=> Array of file data to insert
     * 	),
     * 	etc.
     * )
     * 
     * @param $files
     */
    public static function bind_files($files = array())
    {
        if(empty($files) || !is_array($files)) return false;

        foreach($files as $key => $file)
        {
        	$data = $file['data'];
    		if(is_numeric($file['id']) && $file['id'] > 0)
    		{
    			// Update existing file
    			$item = Model_Category_File::find_one_by_id($file['id']);
    			$item->set($data);
    		}
    		else
    		{
    			$item = Model_Category_File::forge($data);
    		}

            $item->save();
        }
    }

    /**
     * Insert or update Category videos
     * Certain array structure need to be passed
     * 
     * array(
     * 	0 => array(
     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
     * 		'data'	=> Array of file data to insert
     * 	),
     * 	1 => array(
     * 		'id' 	=> If numeric and larger than 0, file will be updated. Otherwise file is considered new
     * 		'data'	=> Array of file data to insert
     * 	),
     * 	etc.
     * )
     * 
     * @param $files
     */
    public static function bind_videos($videos = array())
    {
        if(empty($videos) || !is_array($videos)) return false;

        foreach($videos as $key => $video)
        {
        	$data = $video['data'];
    		if(is_numeric($video['id']) && $video['id'] > 0)
    		{
    			// Update existing video
    			$item = Model_Category_Video::find_one_by_id($video['id']);
    			$item->set($data);
    		}
    		else
    		{
    			$item = Model_Category_Video::forge($data);
    		}
            $item->save();
        }
    }
        
        /**
         * Helper function for getting all child categories
         * 
         * @param object $category          = Parent category object
         * @param object $categories_array  = Starting array of category id-s
         * @return array         
         */
        public static function get_all_children($category, &$categories_array = array())
        {
            array_push($categories_array, $category->id);
            array_push($categories_array, $category->id);
            
            if(!empty($category->children))
            {
                foreach($category->children as $child_category)
                {
                    \Product\Model_Category::get_all_children($child_category, $categories_array);
                }
            }
            
            $categories_array = array_unique($categories_array);
            asort($categories_array);
            
            return array_merge($categories_array);
        }

	    /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	        $table_name = self::$_table_name;
	        
	        // Select only active products
	        $active = \Theme::instance()->active();
	        if($active['name'] == 'frontend')
	        {
	            $query->where(function($query) use ($table_name){
	                    $query->where($table_name . '.status', 1);
	                    $query->or_where(function($query) use ($table_name){
	                        $query->where($table_name . '.status', 2);
	                        $query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
	                        $query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
	                    });
	                });
	        }
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
                        // Get category products
                        $item->get_products = static::lazy_load(function() use ($item){
		    					
		    				$products = Model_Product_To_Categories::find(function($query) use ($item)
		    				{
		    					return $query->where('category_id', $item->id);
		    				}, 'product_id');
		    					
		    				if(!empty($products))
		    				{
		    					$products = '(' . implode(',', array_keys($products)) . ')';
		    						
		    					return Model_Product::find(function($query) use ($products, $item)
		    					{	 

		    						$query->select('product.*');


		    						$query->and_where('product.id', 'IN', \DB::expr($products));

		    						
		    						$query->group_by('product.id');

		    						$query->order_by('product.sort', 'asc');


		    						// print_r((string)$query);
		    						// die;
		    			
		    						return $query;
		    					}, 'id');
		    				}
		    					
		    				return array();
		    					
		    			}, $item->id, 'products');
                        
                        // Get all category products (including subcategory products)
                        $item->get_all_products = static::lazy_load(function() use ($item){
		    				
                            $categories_ids = \Product\Model_Category::get_all_children($item);
                            
		    				$products = Model_Product_To_Categories::find(function($query) use ($item, $categories_ids)
		    				{
		    					return $query->where('category_id', 'in', $categories_ids);
		    				}, 'product_id');
                            
		    				if(!empty($products))
		    				{
		    					$products = '(' . implode(',', array_keys($products)) . ')';
		    						
		    					return Model_Product::find(function($query) use ($products, $item)
		    					{
		    						$query->where('id', 'IN', \DB::expr($products));
		    						$query->order_by('sort', 'asc');
		    			
		    						return $query;
		    					}, 'id');
		    				}
		    					
		    				return array();
		    					
		    			}, $item->id, 'all_products');
                        
		    			// Get content images
						$item->get_images = static::lazy_load(function() use ($item){
		    				return Model_Category_Image::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'images');

	                    // Get content files
	                    $item->get_files = static::lazy_load(function() use ($item){
	                        return Model_Category_File::find(array(
	                            'where' => array(
	                                'content_id' => $item->id,
	                            ),
	                            'order_by' => array('sort' => 'asc'),
	                        ));
	                    }, $item->id, 'files');

	                    // Get content videos
	                    $item->get_videos = static::lazy_load(function() use ($item){
	                        return Model_Category_Video::find(array(
	                            'where' => array(
	                                'content_id' => $item->id,
	                            ),
	                            'order_by' => array('sort' => 'asc'),
	                        ));
	                    }, $item->id, 'videos');

	                    // Get product infotabs
	                    $item->get_infotabs = static::lazy_load(function() use ($item){

	                        return Model_Category_To_Infotabs::find(function($query) use ($item)
	                        {
	                            $query->order_by('sort', 'asc');
	                            return $query->where('category_id', $item->id);
	                        });

	                    }, $item->id, 'infotabs');
						
		    			// Get content children
						$item->get_children = static::lazy_load(function() use ($item){
		    				return \Product\Model_Category::find(array(
							    'where' => array(
							        'parent_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'children');
						
		    			// Get content children
						$item->get_seo = static::lazy_load(function() use ($item){
							return Model_Category_Seo::find_one_by_content_id($item->id);
		    			}, $item->id, 'seo', 'object');

	                    // Get related categories
	                    $item->get_related_categories = static::lazy_load(function() use ($item){

	                        $related = Model_Category_To_Related::find(function($query) use ($item)
	                        {
	                            return $query->where('category_id', $item->id);
	                        }, 'related_id');

	                        if(!empty($related))
	                        {
	                            $related = '(' . implode(',', array_keys($related)) . ')';

	                            return \Product\Model_Category::find(function($query) use ($related, $item)
	                            {
	                                $query->where('id', 'IN', \DB::expr($related));
	                                $query->where('id', '<>', $item->id);
	                                $query->order_by('sort', 'asc');

	                                return $query;
	                            }, 'id');
	                        }

	                        return array();

	                    }, $item->id, 'related_categories');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Product\Model_Category::$temp['lazy.'.$id.$type]))
		    	{
		    		\Product\Model_Category::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Product\Model_Category::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Category::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Product\Model_Category::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Category::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Product\Model_Category::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 4)->add_rule('max_length', 255);
			if(\Input::post('status', 1) == 2)
			{
				// Only set if status is "Active in period"
				$val->add('active_from', 'Active from')->add_rule('required_with', 'active_to')->add_rule('valid_date', 'd/m/Y');
				$val->add('active_to', 'Active to')->add_rule('required_with', 'active_from')->add_rule('valid_date', 'd/m/Y');
			}
	
			return $val;
		}
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				$vars['user_created']	= \Sentry::user()->id;
				// Auto increment sort column
				$vars['sort']			= \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				if(ucfirst($name) == 'Category')
					$class = 'Product\Model_Category';
				else
					$class = 'Product\Model_Category_' . ucfirst($name);
				
				return new $class;
			}
			
			return static::forge();
		}
        
        public static function get_by_slug($slug = false)
        {
            if($seo = \Product\Model_Category_Seo::find_one_by_slug($slug))
            {
                return Model_Category::find_one_by_id($seo->content_id);
            }
            
            return false;
        }
		
        public static function find_parents($id = false, &$parents = array(), $return_requested = false)
        {
            if(is_numeric($id))
            {   
                if($category = Model_Category::find_one_by_id($id))
                {
                    if($category->parent_id > 0)
                    {
                        $parents[$id] = $category->parent_id;
                        Model_Category::find_parents($category->parent_id, $parents, $return_requested);
                    }
                }
            }
            if($return_requested) $parents[$id] = $id;
            return array_reverse($parents);
        }
        
        /**
        * Sort product by price
        * use with usort
        * usort($array, array("Class", "sort_product_attributes"));
        *  usort($out[$product_attributes_id[$k]], array('\Product\Model_Product', 'sort_product_attributes'));
        */
       public static function sort_product_by_price( $a, $b) {
           return $a->default_price[0] == $b->default_price[0] ? 0 : ( static::$sort == 'asc' ? $a->default_price[0] > $b->default_price[0] :  $a->default_price[0] < $b->default_price[0]) ? 1 : -1;
       }
       
       public static function get_products($category_id = false, $sort = 'asc')
       {
           static::$sort = $sort;
           
           $out = array();
           $products = Model_Product_To_Categories::find(function($query) use ($category_id)
           {
                return $query->where('category_id', $category_id);
           }, 'product_id');

           if(!empty($products))
           {
                $products = '(' . implode(',', array_keys($products)) . ')';

                $out = Model_Product::find(function($query) use ($products)
                {
                     $query->where('id', 'IN', \DB::expr($products));
                     return $query;
                }, 'id');
                
           }

           if(!empty($out))
           {
               usort($out, array('\Product\Model_Category', 'sort_product_by_price'));  
           }

           return $out;
       }


       	public static function get_categories_ordered_products($orders, $category)
       	{
       		$get_order_ids = array();
       		foreach ($orders as $order)
       			array_push($get_order_ids, $order->id);

			$result = \DB::select(\DB::expr("group_concat(order_products.product_id separator ',') as product_ids"))
						->from('orders')
						->join('order_products')
						->on('orders.id', '=', 'order_products.order_id')
						->where('orders.finished', '=', 1)
						->where(\DB::expr("orders.id in (".implode(',', $get_order_ids).")"))
						->execute();
			$products_in_order = $result[0]["product_ids"];

       		$categories_ids = \Product\Model_Category::get_all_children($category);

       		$products = Model_Product_To_Categories::find(function($query) use ($category, $categories_ids)
			{
				return $query->where('category_id', 'in', $categories_ids);
			}, 'product_id');
            
			if(!empty($products) && $products_in_order)
			{
				$products = implode(',', array_intersect(array_keys($products), explode(',', $products_in_order)));
				if($products)
				{
					return Model_Product::find(function($query) use ($products, $category)
					{
						$query->where('id', 'IN', \DB::expr('('.$products.')'));
						$query->order_by('sort', 'asc');
			
						return $query;
					}, 'id');

				}
			}
       	}

	   	public static function get_cover_image($item)
	   	{
	        // Get content cover image
	        $cover_image = Model_Category_Image::find(array(
	            'where' => array(
	            'content_id' => $item->id,
	            'cover' => 1,
	        ),
	            'order_by' => array('sort' => 'asc'),
	        ));
	        if(isset($cover_image[0]))
	        {
	            return $cover_image[0];
	        }
	        else
	        {
	            $images_list = Model_Category_Image::find(array(
	                    'where' => array(
	                    'content_id' => $item->id,
	                ),
	                'order_by' => array('sort' => 'asc'),
	            ));
	            if(isset($images_list[0]))
	            {
	                return $images_list[0];
	            }
	        }
	        return false;
	   	}
	}