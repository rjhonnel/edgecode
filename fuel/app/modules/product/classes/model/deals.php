<?php

	namespace Product;

	class Model_Deals extends \Model_Base
	{
	    // Temporary variable to store some query results
	    public static $temp = array();

	    // Set the table to use
	    protected static $_table_name = 'product_deals';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'product_id',
		    'title',
		    'short_description',
		    'full_description',
		    'status',
		    'active_from',
		    'active_to',
		    'sort',
		);
		
		protected static $_defaults = array(
		    'status' => 1,
		);

	    /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
         	$table_name = self::$_table_name;

	        // Select only active product or existing
	        $active = \Theme::instance()->active();
	        if($active['name'] == 'frontend')
	        {
	            $query->where(function($query) use ($table_name){
	                    $query->where($table_name . '.status', 1);
	                    $query->or_where(function($query) use ($table_name){
	                        $query->where($table_name . '.status', 2);
	                        $query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
	                        $query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
	                    });
	                });
	        }
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
                        // Get product
	                    $item->get_product = static::lazy_load(function() use ($item){
	                        if($product = Model_Product::find_one_by_id($item->product_id))
	                        {
	                            return $product;
	                        }
	                        return false;
	                    }, $item->id, 'product', 'object');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }

	    /**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	        return function() use ($closure, $id, $type, $return){
	            if(!isset(\Product\Model_Deals::$temp['lazy.'.$id.$type]))
	            {
	                \Product\Model_Deals::$temp['lazy.'.$id.$type] = $closure();

	                // Make sure we always return array
	                if($return == 'array')
	                {
	                    if(!is_array(\Product\Model_Deals::$temp['lazy.'.$id.$type])) 
	                        \Product\Model_Deals::$temp['lazy.'.$id.$type] = array();
	                }
	                else
	                {
	                    if(!is_object(\Product\Model_Deals::$temp['lazy.'.$id.$type]) && !is_bool(\Product\Model_Deals::$temp['lazy.'.$id.$type])) 
	                        \Product\Model_Deals::$temp['lazy.'.$id.$type] = new \stdClass();
	                }
	            }

	            return \Product\Model_Deals::$temp['lazy.'.$id.$type];
	        };
	    }

		/**
	     * Validate Model fields
	     * 
	     * @param $factory 	= Validation name, if you want to have multiple validations
	     * @param $id		= ID if item that is being validated if we need to define some custom rules (like unique)
	     */
		public static function validate($factory, $id = false)
		{
			$val = \Validation::forge($factory);

			$val->add('title', 'Title')->add_rule('required');
			$val->add('full_description', 'Full Description')->add_rule('required');
			$val->add('status', 'Status')->add_rule('required');
			
	        if(\Input::post('status', 1) == 2)
	        {
	            // Only set if status is "Active in period"
	            $val->add('active_from', 'Active from')->add_rule('required_with', 'active_to')->add_rule('valid_date', 'd/m/Y');
	            $val->add('active_to', 'Active to')->add_rule('required_with', 'active_from')->add_rule('valid_date', 'd/m/Y');
	        }
			
			return $val;
		}
	}