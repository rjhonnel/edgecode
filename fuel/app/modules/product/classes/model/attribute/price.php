<?php

	namespace Product;

	class Model_Attribute_Price extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'product_attribute_price';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
            'product_attribute_id',
		    'user_group_id',
		    'able_discount',
		    'discount',
            'price',
            'type',
		    'product_group_discount_id',
		    'pricing_group_id' // NRB-Gem: support for multiple pricing group per product
		);

	    
	    /**
	     * Insert or update page images
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	    	if(empty($images) || !is_array($images)) return false;
	    	
	    	foreach($images as $key => $image)
	    	{
	    		$item = Model_Attribute_Image::forge($image['data']);
	    		
	    		if(is_numeric($image['id']) && $image['id'] > 0)
	    		{
	    			// Update existing image
	    			$item->set(array('id' => $image['id']));
	    			$item->is_new(false);
	    		}
	    		
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// Get content children
						$item->get_attribute = static::lazy_load(function() use ($item){
		    				return \Product\Model_Attribute::find(array(
							    'where' => array(
							        'id' => $item->product_attribute_id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'attribute');
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Product\Model_Attribute_Price::$temp['lazy.'.$id.$type]))
		    	{
		    		\Product\Model_Attribute_Price::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Product\Model_Attribute_Price::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Attribute_Price::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Product\Model_Attribute_Price::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Attribute_Price::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Product\Model_Attribute_Price::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);
			$val->add('description_full', 'Description')->add_rule('required');
	
			return $val;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				if(ucfirst($name) == 'Category')
					$class = 'Product\Model_Attribute_Price';
				else
					$class = 'Product\Model_Attribute_' . ucfirst($name);
				
				return new $class;
			}
			
			return static::forge();
		}
		
	}