<?php

	namespace Product;

	class Model_Packs extends \Model_Base
	{
	    // Temporary variable to store some query results
	    public static $temp = array();

	    // Set the table to use
	    protected static $_table_name = 'product_packs';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'product_id',
		    'name',
		    'description',
		    'type',
		    'selection_limit',
		    'products',
		);

	    /**
	     * Insert or update pack images
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	        if(empty($images) || !is_array($images)) return false;

	        foreach($images as $key => $image)
	        {
	            $data = $image['data'];
	    		if(is_numeric($image['id']) && $image['id'] > 0)
	    		{
	    			// Update existing image
	    			$item = Model_Packs_Image::find_one_by_id($image['id']);
	    			$item->set($data);
	    		}
	    		else
	    		{
	    			$item = Model_Packs_Image::forge($data);
	    		}

	            $item->save();
	        }
	    }

	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	        if($result !== null)
	        {
	            if(is_array($result))
	            {
	                foreach($result as $key => $item)
	                {
	                   
	                    // It will first check if we already have result in temporary result, 
	                    // and only execute query if we dont. That way we dont have duplicate queries

	                    // Get content get_image
	                    $item->get_image = static::lazy_load(function() use ($item){
	                        if($pack = Model_Packs_Image::find_one_by_content_id($item->id))
	                        {
	                        	return $pack;
	                        }
	                        return false;
	                    }, $item->id, 'image', 'object');
	                }
	            }
	        }

	        // return the result
	        return $result; 
	    }

	    /**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	        return function() use ($closure, $id, $type, $return){
	            if(!isset(\Product\Model_Packs::$temp['lazy.'.$id.$type]))
	            {
	                \Product\Model_Packs::$temp['lazy.'.$id.$type] = $closure();

	                // Make sure we always return array
	                if($return == 'array')
	                {
	                    if(!is_array(\Product\Model_Packs::$temp['lazy.'.$id.$type])) 
	                        \Product\Model_Packs::$temp['lazy.'.$id.$type] = array();
	                }
	                else
	                {
	                    if(!is_object(\Product\Model_Packs::$temp['lazy.'.$id.$type]) && !is_bool(\Product\Model_Packs::$temp['lazy.'.$id.$type])) 
	                        \Product\Model_Packs::$temp['lazy.'.$id.$type] = new \stdClass();
	                }
	            }

	            return \Product\Model_Packs::$temp['lazy.'.$id.$type];
	        };
	    }
	}