<?php

	namespace Product;

	class Model_Brand extends \Model_Base
	{
	    // Temporary variable to store some query results
		public static $temp = array();
        
        public static $sort = 'asc';
        
        public static $sorting = array();
	
	    // Set the table to use
	    protected static $_table_name = 'product_brands';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'title',
		    'description_intro',
		    'description_full',
		    'status',
		    'active_from',
		    'active_to',
		    'published_at',
		    'user_created',
		    'user_updated',
		    'sort',
		);
		
		protected static $_defaults = array();
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    /**
	     * Insert or update page images
	     * Certain array structure need to be passed
	     * 
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     * 
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	    	if(empty($images) || !is_array($images)) return false;
	    	
	    	foreach($images as $key => $image)
	    	{
	    		$data = $image['data'];
	    		if(is_numeric($image['id']) && $image['id'] > 0)
	    		{
	    			// Update existing image
	    			$item = Model_Brand_Image::find_one_by_id($image['id']);
	    			$item->set($data);
	    		}
	    		else
	    		{
	    			$item = Model_Brand_Image::forge($data);
	    		}

	    		$item->save();
	    	}
	    }
        
        /**
         * Helper function for getting all child brands
         * 
         * @param object $brand          = Parent brand object
         * @param object $brands_array  = Starting array of brand id-s
         * @return array         
         */
        public static function get_all_children($brand, &$brands_array = array())
        {
            array_push($brands_array, $brand->id);
            array_push($brands_array, $brand->id);
            
            if(!empty($brand->children))
            {
                foreach($brand->children as $child_brand)
                {
                    \Product\Model_Brand::get_all_children($child_brand, $brands_array);
                }
            }
            
            $brands_array = array_unique($brands_array);
            asort($brands_array);
            
            return array_merge($brands_array);
        }

	    /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	        $table_name = self::$_table_name;
	        
	        // Select only active products
	        $active = \Theme::instance()->active();
	        if($active['name'] == 'frontend')
	        {
	            $query->where(function($query) use ($table_name){
	                    $query->where($table_name . '.status', 1);
	                    $query->or_where(function($query) use ($table_name){
	                        $query->where($table_name . '.status', 2);
	                        $query->and_where($table_name . '.active_from', '<=', strtotime(date('Y-m-d')));
	                        $query->and_where($table_name . '.active_to', '>=', strtotime(date('Y-m-d')));
	                    });
	                });
	        }
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
                        // Get brand products
                        $item->get_products = static::lazy_load(function() use ($item){
		    					
		    				$products = Model_Product_To_Brands::find(function($query) use ($item)
		    				{
		    					return $query->where('brand_id', $item->id);
		    				}, 'product_id');
		    					
		    				if(!empty($products))
		    				{
		    					$products = '(' . implode(',', array_keys($products)) . ')';
		    						
		    					return Model_Product::find(function($query) use ($products, $item)
		    					{	 

		    						$query->select('product.*');


		    						$query->and_where('product.id', 'IN', \DB::expr($products));

		    						
		    						$query->group_by('product.id');

		    						$query->order_by('product.sort', 'asc');
		    			
		    						return $query;
		    					}, 'id');
		    				}
		    					
		    				return array();
		    					
		    			}, $item->id, 'products');
                        
                        // Get all brand products (including subcategory products)
                        $item->get_all_products = static::lazy_load(function() use ($item){
		    				
                            $brands_ids = \Product\Model_Brand::get_all_children($item);
                            
		    				$products = Model_Product_To_Brands::find(function($query) use ($item, $brands_ids)
		    				{
		    					return $query->where('brand_id', 'in', $brands_ids);
		    				}, 'product_id');
                            
		    				if(!empty($products))
		    				{
		    					$products = '(' . implode(',', array_keys($products)) . ')';
		    						
		    					return Model_Product::find(function($query) use ($products, $item)
		    					{
		    						$query->where('id', 'IN', \DB::expr($products));
		    						$query->order_by('sort', 'asc');
		    			
		    						return $query;
		    					}, 'id');
		    				}
		    					
		    				return array();
		    					
		    			}, $item->id, 'all_products');
                        
		    			// Get content images
						$item->get_images = static::lazy_load(function() use ($item){
		    				return Model_Brand_Image::find(array(
							    'where' => array(
							        'content_id' => $item->id,
							    ),
							    'order_by' => array('sort' => 'asc'),
							));
		    			}, $item->id, 'images');
						
		    			// Get content children
						$item->get_seo = static::lazy_load(function() use ($item){
							return Model_Brand_Seo::find_one_by_content_id($item->id);
		    			}, $item->id, 'seo', 'object');
		    			
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
	    
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Product\Model_Brand::$temp['lazy.'.$id.$type]))
		    	{
		    		\Product\Model_Brand::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Product\Model_Brand::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Brand::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Product\Model_Brand::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Brand::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Product\Model_Brand::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 4)->add_rule('max_length', 255);
			if(\Input::post('status', 1) == 2)
			{
				// Only set if status is "Active in period"
				$val->add('active_from', 'Active from')->add_rule('required_with', 'active_to')->add_rule('valid_date', 'd/m/Y');
				$val->add('active_to', 'Active to')->add_rule('required_with', 'active_from')->add_rule('valid_date', 'd/m/Y');
			}
	
			return $val;
		}
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				$vars['user_created']	= \Sentry::user()->id;
				// Auto increment sort column
				$vars['sort']			= \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				if(ucfirst($name) == 'Brand')
					$class = 'Product\Model_Brand';
				else
					$class = 'Product\Model_Brand_' . ucfirst($name);
				
				return new $class;
			}
			
			return static::forge();
		}
        
        public static function get_by_slug($slug = false)
        {
            if($seo = \Product\Model_Brand_Seo::find_one_by_slug($slug))
            {
                return Model_Brand::find_one_by_id($seo->content_id);
            }
            
            return false;
        }
        
        /**
        * Sort product by price
        * use with usort
        * usort($array, array("Class", "sort_product_attributes"));
        *  usort($out[$product_attributes_id[$k]], array('\Product\Model_Product', 'sort_product_attributes'));
        */
       public static function sort_product_by_price( $a, $b) {
           return $a->default_price[0] == $b->default_price[0] ? 0 : ( static::$sort == 'asc' ? $a->default_price[0] > $b->default_price[0] :  $a->default_price[0] < $b->default_price[0]) ? 1 : -1;
       }
       
       public static function get_products($brand_id = false, $sort = 'asc')
       {
           static::$sort = $sort;
           
           $out = array();
           $products = Model_Product_To_Brand::find(function($query) use ($brand_id)
           {
                return $query->where('brand_id', $brand_id);
           }, 'product_id');

           if(!empty($products))
           {
                $products = '(' . implode(',', array_keys($products)) . ')';

                $out = Model_Product::find(function($query) use ($products)
                {
                     $query->where('id', 'IN', \DB::expr($products));
                     return $query;
                }, 'id');
                
           }

           if(!empty($out))
           {
               usort($out, array('\Product\Model_Brand', 'sort_product_by_price'));  
           }

           return $out;
       }
	}