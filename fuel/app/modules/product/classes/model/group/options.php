<?php

namespace Product;

class Model_Group_Options extends \Model_Base
{
    // Temporary variable to store some query results
    public static $temp = array();
    
    // Set the table to use
    protected static $_table_name = 'product_group_options';

    // List of all columns that will be used on create/update
    protected static $_properties = array(
        'user_group_id',
        'product_group_id',
        'active',
        'able_to_buy',
        'able_to_view',
        'sale_discount',
        'apply_tier_to_sale',
    );
    
    /**
    * Get additional content data selected
    * 
    * @param $result = Query result
    */
    public static function post_find($result)
    {
        if($result !== null)
        {
            if(is_array($result))
            {
                foreach($result as $item)
                {
                    // It will first check if we already have result in temporary result, 
                    // and only execute query if we dont. That way we dont have duplicate queries

                    // Get group
                    $item->get_group = static::lazy_load(function() use ($item){
                        return Model_Group::find_one_by_id($item->product_group_id);
                    }, $item->id, 'group', 'object');

                }
            }
        }

        // return the result
        return $result;
    }
    
     /**
     * Load function result only once and than remember 
     * it in $temp variable for later use
     * 
     * @param $closure		= Function for returning data
     * @pa ram $id			= Item ID
     * @param $type			= Name for additional data
     * @param $return		= Return type, either array or object (empty result will be casted in that type)
     */
    private static function lazy_load($closure, $id, $type, $return = 'array')
    {
        return function() use ($closure, $id, $type, $return){
            if(!isset(\Product\Model_Group_Options::$temp['lazy.'.$id.$type]))
            {
                \Product\Model_Group_Options::$temp['lazy.'.$id.$type] = $closure();

                // Make sure we always return array
                if($return == 'array')
                {
                    if(!is_array(\Product\Model_Group_Options::$temp['lazy.'.$id.$type])) 
                        \Product\Model_Group_Options::$temp['lazy.'.$id.$type] = array();
                }
                else
                {
                    if(!is_object(\Product\Model_Group_Options::$temp['lazy.'.$id.$type]) && !is_bool(\Product\Model_Group_Options::$temp['lazy.'.$id.$type])) 
                        \Product\Model_Group_Options::$temp['lazy.'.$id.$type] = new \stdClass();
                }
            }

            return \Product\Model_Group_Options::$temp['lazy.'.$id.$type];
        };
    }

}