<?php

	namespace Product;

	class Model_Group_Discounts extends \Model_Base
	{
	// Set the table to use
	    protected static $_table_name = 'product_group_discounts';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'user_group_id',
		    'product_group_id',
		    'qty',
		    'discount',
            
		);
	}