<?php

	namespace Product;
	
	// NRB-Gem: Added Pricing Groups model
	class Model_Customer_Group extends \Model_Base
	{
	  // Set the table to use
	  protected static $_table_name = 'groups';

	  // List of all columns that will be used on create/update
	  protected static $_properties = array(
	    'id',
	    'name',
	    'level',
	    'is_admin',
	    'parent',
	    'deletable'
	  );
	}
	// End Pricing Groups model

	class Model_Group extends \Model_Base
	{
        // User group ID (used for getting some additional group data)
        public static $user_group_id = NULL;
        
	    // Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'product_groups';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'parent_id',
		    'type',
		    'description_intro',
		    'description_full',
		    'title',
		    'user_created',
		    'user_updated',
		    'sort',
		);
		
		protected static $_defaults = array(
		    'parent_id' 		=> 0,
		    'type' 				=> 'property',
			'description_intro' => NULL,
			'description_full'	=> NULL,
		);
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    /**
	     * Insert or update group images
	     * Certain array structure need to be passed
	     *
	     * array(
	     * 	0 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	1 => array(
	     * 		'id' 	=> If numeric and larger than 0, image will be updated. Otherwise image is considered new
	     * 		'data'	=> Array of image data to insert
	     * 	),
	     * 	etc.
	     * )
	     *
	     * @param $images
	     */
	    public static function bind_images($images = array())
	    {
	    	if(empty($images) || !is_array($images)) return false;
	    
	    	foreach($images as $key => $image)
	    	{
	    		$item = Model_Group_Image::forge($image['data']);
	    		 
	    		if(is_numeric($image['id']) && $image['id'] > 0)
	    		{
	    			// Update existing image
	    			$item->set(array('id' => $image['id']));
	    			$item->is_new(false);
	    		}
	    		 
	    		$item->save();
	    	}
	    }
	    
	    /**
	     * Get additional content data selected
	     * 
	     * @param $result = Query result
	     */
	    public static function post_find($result)
	    {
	    	if($result !== null)
		    {
		    	if(is_array($result))
		    	{
		    		foreach($result as $item)
		    		{
		    			// It will first check if we already have result in temporary result, 
		    			// and only execute query if we dont. That way we dont have duplicate queries
		    			
		    			// Get related products
		    			$item->get_products = static::lazy_load(function() use ($item){
		    					
		    				$products = Model_Product_To_Groups::find(function($query) use ($item)
		    				{
		    					return $query->where('group_id', $item->id);
		    				}, 'product_id');
		    					
		    				if(!empty($products))
		    				{
		    					$products = '(' . implode(',', array_keys($products)) . ')';
		    						
		    					return Model_Product::find(function($query) use ($products, $item)
		    					{
		    						$query->where('id', 'IN', \DB::expr($products));
		    						$query->order_by('sort', 'asc');
		    			
		    						return $query;
		    					}, 'id');
		    				}
		    					
		    				return array();
		    					
		    			}, $item->id, 'products');
		    			
		    			// Get content children
		    			$item->get_children = static::lazy_load(function() use ($item){
		    				return Model_Group::find(array(
	    						'where' => array(
    								'parent_id' => $item->id,
	    						),
	    						'order_by' => array('sort' => 'asc'),
		    				));
		    			}, $item->id, 'children');
		    			
		    			// Get group images
		    			$item->get_images = static::lazy_load(function() use ($item){
		    				return Model_Group_Image::find(array(
	    						'where' => array(
    								'content_id' => $item->id,
	    						),
	    						'order_by' => array('sort' => 'asc'),
		    				));
		    			}, $item->id, 'images');
                        
                        // Get group discount options
                        // Use one more item property (user_group_id)
                        // It can be set using methods (set_user_group_id and get_user_group_id)
		    			$item->get_discount_options = static::lazy_load(function() use ($item){
                            $options = Model_Group_Options::find(array(
	    						'where' => array(
    								'user_group_id'     => $item->get_user_group_id(),
    								'product_group_id'  => $item->id,
	    						),
		    				));
                            return isset($options[0]) ? $options[0] : array();
		    			}, $item->id, 'discount_options', 'object');
                        
                        // Get group discount options
                        // Use one more item property (user_group_id)
                        // It can be set using methods (set_user_group_id and get_user_group_id)
		    			$item->get_discounts = static::lazy_load(function() use ($item){
                            return Model_Group_Discounts::find(array(
	    						'where' => array(
    								'user_group_id'     => $item->get_user_group_id(),
    								'product_group_id'  => $item->id,
	    						),
                                'order_by' => array('qty' => 'asc'),
		    				), 'qty');
		    			}, $item->id, 'discounts');
                        
                        // DO NOT CHANGE THIS TWO FUNCTIONS AS THEY ARE NEEDED
                        $item->set_user_group_id = function($user_group_id = NULL){
                            \Product\Model_Group::$user_group_id = $user_group_id;
                        };
                        
                        $item->get_user_group_id = function(){
                            return \Product\Model_Group::$user_group_id;
                        };
		    		}
		    	}
		    }
		    
		    // return the result
		    return $result;
	    }
        
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    public static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Product\Model_Group::$temp['lazy.'.$id.$type]))
		    	{
		    		\Product\Model_Group::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Product\Model_Group::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Group::$temp['lazy.'.$id.$type] = array();
		    		}
                    // custom return, so we will accept anything
		    		else if($return == 'custom')
		    		{
			    		// Do nothing with result
		    		}
                    // In every other case we return object
		    		else
		    		{
		    			if(!is_object(\Product\Model_Group::$temp['lazy.'.$id.$type])) 
			    			\Product\Model_Group::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Product\Model_Group::$temp['lazy.'.$id.$type];
	    	};
	    }
	    
	    /**
	     * Validate Model fields
	     * 
	     * @param $factory = Validation name, if you want to have multiple validations
	     */
		public static function validate($factory)
		{
			$val = \Validation::forge($factory);
			$val->add('title', 'Title')->add_rule('required')->add_rule('min_length', 5)->add_rule('max_length', 255);
	
			return $val;
		}
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				$vars['user_created']	= \Sentry::user()->id;
				// Auto increment sort column
				$vars['sort']			= \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}
		
		/**
		 * Return other models instances, like Images, Files, etc.
		 *
		 * @param $name = Model Name
		 */
		public static function factory($name = false)
		{
			if(is_string($name))
			{
				if(ucfirst($name) == 'Group')
					$class = 'Product\Model_Group';
				else
					$class = 'Product\Model_Group_' . ucfirst($name);
					
				return new $class;
			}
			
			return static::forge();
		}
		
	}