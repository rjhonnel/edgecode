<?php
 return array(
 	
 	/**
 	 * Enable or disable this payment
 	 */
 	'enabled' => true,  // Required true or false

 	'code'	  => 'paypal',  // Required
 		
 	'name'	  => 'PayPal',  // Required

 	'config' => array(
        
        /**
         * New config
         */
        'active' => 'test',
        
        'default' => array(
            'param'         => 'cmd=_express-checkout&token=',
            'mode'			=> 'live',
            'version'		=> '104.0',
            'currency'		=> 'AUD',
            'user_name' 	=> 'lily_api1.lightmedia.com.au',
            'password' 		=> 'H89D9Y43C2ZP8M5N',
            'signature' 	=> 'AFcWxV21C7fd0v3bYYYRCpSSRl31AiFAHJEoCuTsZo0hhbpPJh63wibP',
            'notify_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/finalise_order') : \Uri::create('order/checkout/finalise_order'),
            'cancel_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/cancel_order') : \Uri::create('order/checkout/cancel_order'),
            'return_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/paypal') : \Uri::create('order/checkout/paypal')
        ),
        'test' => array(
            'param'         => 'cmd=_express-checkout&token=',
            'mode'			=> 'sandbox',
            'version'		=> '104.0',
            'currency'		=> 'AUD',
            'user_name' 	=> 'lily-facilitator_api1.lightmedia.com.au',
            'password' 		=> 'NTJKZ8HRLLY97HJP',
            'signature' 	=> 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AKQF.nmocUCy1pgPJt0zyq9esZQK',
            'notify_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/finalise_order') : \Uri::create('order/checkout/finalise_order'),
            'cancel_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/cancel_order') : \Uri::create('order/checkout/cancel_order'),
            'return_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/paypal') : \Uri::create('order/checkout/paypal')
        ),
        
    ),
 	
 );