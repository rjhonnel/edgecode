<?php
 return array(
 	
 	/**
 	 * Enable or disable this payment
 	 */
 	'enabled' => true,  // Required true or false

 	'code'	  => 'eway',  // Required
 		
 	'name'	  => 'eWay',  // Required

 	'config' => array(
        
        /**
         * New config
         */
        'active' => 'test',
        
        'default' => array(
            'mode'          => 'live',
            'customerId'    => '91711049',
            'apiKey'        => '60CF3CjMmx0rdSJTKM056jIWLMKUVCwnYDZHPYHkPHcw0xmd8ysAuUrRhYEO4oMqqcawGV',
            'password'      => 'dRw1eL2y',
            'return_update_url' => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/update') : \Uri::create('order/checkout/cost'),
            'return_edit_url'   => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/edit') : \Uri::create('order/checkout/cost'),
        ),
        'test' => array(
            'mode'          => 'sandbox',
            'customerId'    => '91711049',
            'apiKey'        => '60CF3CjMmx0rdSJTKM056jIWLMKUVCwnYDZHPYHkPHcw0xmd8ysAuUrRhYEO4oMqqcawGV',
            'password'      => 'dRw1eL2y',
            'return_update_url' => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/update') : \Uri::create('order/checkout/cost'),
            'return_edit_url'   => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/edit') : \Uri::create('order/checkout/cost'),
        ),
        
    ),
 	
 );