<?php
 return array(
 	
 	/**
 	 * Enable or disable this payment
 	 */
 	'enabled' => true,  // Required true or false

 	'code'	  => 'securepay',  // Required
 		
 	'name'	  => 'SecurePay',  // Required

 	'config' => array(
        
        /**
         * New config
         */
        'active' => 'test',
        
        'default' => array(
            'mode'          => 'live',
            'merchantId'    => 'ABC0001',
            'password'      => 'abc123',
            'return_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/return_securepay') : \Uri::create('order/checkout/return_securepay'),
            'cancel_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/cancel_order') : \Uri::create('order/checkout/cancel_order'),
            'return_update_url' => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/update') : \Uri::create('order/checkout/cost'),
            'return_edit_url'   => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/edit') : \Uri::create('order/checkout/cost')
        ),
        'test' => array(
            'mode'          => 'sandbox',
            'merchantId'    => 'ABC0001',
            'password'      => 'abc123',
            'return_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/return_securepay') : \Uri::create('order/checkout/return_securepay'),
            'cancel_url'    => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/cancel_order') : \Uri::create('order/checkout/cancel_order'),
            'return_update_url' => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/update') : \Uri::create('order/checkout/cost'),
            'return_edit_url'   => (\SentryAdmin::user()->is_admin())? \Uri::create('admin/order/edit') : \Uri::create('order/checkout/cost')
        ),
        
    ),
 	
 );