<?php

/**
 * Part of Eximius Solutions CMS.
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Page
 * @extends Controller_Base
 */

namespace Payment;

use Response;
use Messages;

class Controller_PayPal_Public extends Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/payment/';
	
	public function before()
	{
		parent::before();
	
		\Config::load('payment::paypal', 'details', true, true);
	
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * Index action
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_index()
	{
//        echo 'test'; 
//        
//        $int_value = (int) 123;
//        
//        $test = $int_value + '123.2';
//        
//        var_dump($test);
//        
//        var_dump($this->order_info(12783));
//        
//        exit;
        
		die(__('Access is denied.'));	
	}
	
	/**
	 * Catch IPN response from PayPal
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_ipn()
	{
		\Package::load('paypalipn');
		
		$post = \Input::post();
        
        $admin_email = 'aleksandar.ilic@eximius-solutions.com';
        
		if(isset($post['invoice']) || is_numeric($post['invoice']))
		{
			if($order = \Order\Model_Order::find_one_by_id($post['invoice']))
			{
				$order_info = $this->order_info($post['invoice']);
				
				//$validate_order['mc_currency'] =  strtoupper($order['currency']);
				$validate_order['mc_currency']      =  "AUD" ;
				$validate_order['mc_gross']         =  $order_info['grand_total_price'];
				
				$response = \PaypalIpn::forge()->validate($validate_order);	
                
                $response_check = $response !== false ;
                
//                @mail($admin_email, \Config::get('site_title', 'Giant Invitation Australia') . ' Pre response ' . $response['invoice'], 
//                    'Order id : ' . $response['invoice'] . "\n" .  
//                    'Response:' . print_r($response, true) . "\n" .  
//                    'Check:' . $response_check);
                
				$payment = \Payment\Model_Payment::forge();
				
				/*
				[mc_gross] => 527.00
				[invoice] => 9
				[protection_eligibility] => Eligible
				[address_status] => confirmed
				[item_number1] =>
				[tax] => 0.00
				[item_number2] =>
				[payer_id] => 2AY578XVRY42G
				[address_street] => 1 Main St
				[payment_date] => 05:19:51 Dec 20, 2012 PST
				[payment_status] => Completed
				[charset] => windows-1252
				[address_zip] => 95131
				[mc_shipping] => 0.00
				[mc_handling] => 0.00
				[first_name] => Djordje
				[mc_fee] => 20.90
				[address_country_code] => US
				[address_name] => Djordje Dimitrijev
				[notify_version] => 3.7
				[custom] =>
				[payer_status] => verified
				[business] => s01_1354717870_biz@eximius-solutions.com
				[address_country] => United States
				[num_cart_items] => 2
				[mc_handling1] => 0.00
				[mc_handling2] => 0.00
				[address_city] => San Jose
				[verify_sign] => Amjz0My6wXvXmjP5pfTStQO3QZ2QA4.Ti6ln42PKcmRKuS-ZegoVx6nF
				[payer_email] => b01_1354717574_per@eximius-solutions.com
				[mc_shipping1] => 0.00
				[mc_shipping2] => 0.00
				[tax1] => 0.00
				[tax2] => 0.00
				[txn_id] => 3AR12649JA9996934
				[payment_type] => instant
				[last_name] => Dimitrijev
				[address_state] => CA
				[item_name1] => Fujiyama
				[receiver_email] => s01_1354717870_biz@eximius-solutions.com
				[item_name2] => Musala
				[payment_fee] =>
				[quantity1] => 2
				[quantity2] => 1
				[receiver_id] => W4MYKU8N4SVHS
				[txn_type] => cart
				[mc_gross_1] => 398.00
				[mc_currency] => EUR
				[mc_gross_2] => 129.00
				[residence_country] => US
				[test_ipn] => 1
				[transaction_subject] => Shopping CartFujiyamaMusala
				[payment_gross] =>
				[ipn_track_id] => 72c4d71c8638a
				*/
				
				if ($response !== false)
				{					
					$json_response = json_encode($response);
					$payment->order_id = $response['invoice'];
					$payment->total_price = $response['mc_gross'];
					$payment->method = 'paypal';
					$payment->status = $response['payment_status'];
					$payment->response = $json_response;
                    
					if(isset($response['pending_reason'])) $payment->status_detail = $response['pending_reason'];
					
                    
                    if(strtolower($response['payment_status']) == 'completed')
                    {
                        // Send email
                        /*$emailer = new \PaymentProccess\PaymentEmailer($order);
                        $emailer->send();*/
                        $emailer = new \Autorespondertrigger\Trigger();
                        $emailer->sendOrderPaymentReceivedPaypalCredit($order);
                        
                        $order->total_price = $order_info['total_price'];

                        $order->paymentmethod = 'N/A';
                        if(isset($order->payments[0]->method)) $order->paymentmethod = $order->payments[0]->method;
                        
                        // Create redemption code ( if order is sample order - this will be checked in RedemptionCodeSaverClass)
                        $redemptionCodeSaver = new \DiscountCodeApplier\RedemptionCodeSaver($order);
                        $redemptionCodeSaver->save();
                        

                    }
                    else
                    {
                        logger(\Fuel::L_INFO, 'PayPal IPN - Pending order. Order ID: ' . $response['invoice'] . '. - ' . $json_response);
                        @mail($admin_email, \Config::get('site_title', 'Giant Invitation Australia') . ' | PayPal IPN - Pending order. Order ID: ' . $response['invoice'], 
                            'PayPal IPN - Pending order. Order ID: ' . $response['invoice'] . "\n" .  print_r($response, true));
                    }
                    
				}
				else
				{
					$payment->order_id      = $post['invoice'];
					$payment->total_price   = $post['mc_gross'];
					$payment->method        = 'paypal';
					$payment->status        = 'Pending';
					$payment->response      = json_encode($post);
                    
                    // Log order as failed
                    logger(\Fuel::L_INFO, 'PayPal IPN - Failed order. Order ID: ' . $response['invoice'] . '. - ' . $json_response);
                    @mail($admin_email, \Config::get('site_title', 'Giant Invitation Australia') . ' | PayPal IPN - Failed order. Order ID: ' . $response['invoice'], 
                        'PayPal IPN - Failed order. Order ID: ' . $response['invoice'] . "\n" .  print_r($response, true));
				}
                
                // Save payment to database
                try
                {
                    $payment->save();
                }
                catch (\Database_Exception $e)
                {
                    logger(\Fuel::L_INFO, 'PayPal IPN - error during inserting data into the database. Order ID: ' . $response['invoice'] . '. - ' . $json_response);
                    @mail($admin_email, \Config::get('site_title', 'My Shortlist') . ' | PayPal IPN - DB error. Order ID: ' . $response['invoice'], 
                        'PayPal IPN - error during inserting data into the database. Order ID: ' . $response['invoice'] . "\n" .  print_r($response, true));
                }
				
			}
			else
			{
				logger(\Fuel::L_INFO, 'PayPal IPN - error. There is no order with ID: ' . $post['invoice'] . '. - ' . json_encode($post));
				@mail($admin_email, \Config::get('site_title', 'My Shortlist') . ' | PayPal IPN - error. There is no order with ID: ' . $post['invoice'],
                    'PayPal IPN - there is no order with ID: ' . $post['invoice'] . "\n" .  print_r($post, true));
			}
		}
		else 
		{
			logger(\Fuel::L_INFO, 'PayPal IPN - error. Missing Order ID. - ' . json_encode($post));
			@mail($admin_email, \Config::get('site_title', 'My Shortlist') . ' | PayPal IPN - error. Missing Order ID.',
                'PayPal IPN - Missing Order ID.' . "\n" . print_r($post, true));
		}
	
	}	
	
	/**
	 * Form to submit to PayPal
	 *
	 * @access  public
	 * @param   object	          $order = Order object
	 * @param   array of objects  $products = Products from order
	 * @return  string			  $page
	 */
	public function action_form($order = false, $products = false)
	{
		
		if( ! \Request::is_hmvc())
		{
			die('Access is denied.');
		}
		
		if(empty($order) || empty($products)) return;
		
		//send email to user
		//$this->send_email($order, $products);
        
		// Active config
        $active = \Config::get('details.config.active', 'test');
		$config = \Config::get('details.config.' . $active, false);

        if($config === false)
        {
            throw new \Exception('Invalid payment config.');
        }

		// Seller account
		$business = $config['business'];
	
		// The URL to which PayPal posts information about the payment
		$notify_url = $config['notify_url'];
	
		// Return url
		$return = $config['return'];
	
		$fields = array();
		$form = '';
	
		// Form to submit to PayPal 
		// https://www.x.com/developers/paypal/documentation-tools/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables
		$form .= '<form action="' . $config['url'] . '" method="post" id="payment_form" target="_parent">';
	
		// Technical HTML Variables
		$form .= \Form::hidden('cmd', '_cart');
		$form .= \Form::hidden('notify_url', $notify_url);
	
		$fields['cmd'] = '_cart';
		$fields['notify_url'] = $notify_url;
		
		$fields['custom'] = 'paypal';
		
		$fields['customvar'] = 'paypal';
	
		// HTML Variables for Payment Transactions
		$form .= \Form::hidden('invoice', $order->id);
		$form .= \Form::hidden('currency_code', strtoupper($order->currency));
		//$form .= \Form::hidden('address_override', '1');
	
		$fields['invoice'] = $order->id;
		$fields['currency_code'] = strtoupper($order->currency);
		//$fields['address_override'] = '1';
	
		// HTML Variables for Shopping Carts
		$form .= \Form::hidden('business', $business);
		$form .= \Form::hidden('upload', '1');
	
		$fields['business'] = $business;
		$fields['upload'] = '1';
	
		foreach ($products as $key => $product)
		{
			$form .= \Form::hidden('item_name_' . ($key+1), $product->title);
			//$form .= \Form::hidden('item_number_' . $key, $product->id);
			$form .= \Form::hidden('amount_' . ($key+1), $product->price);
			$form .= \Form::hidden('quantity_' . ($key+1), $product->quantity);
				
			$fields['item_name_' . ($key+1)] = $product->title;
			$fields['item_number_' . ($key+1)] = $product->id;
			$fields['amount_' . ($key+1)] = $product->price;
			$fields['quantity_' . ($key+1)] = $product->quantity;
		}
	
		// HTML Variables for Filling Out PayPal Checkout Pages Automatically for Buyers
		// 		$form .= \Form::hidden('address1', $order->address);
		// 		$form .= \Form::hidden('city', $order->suburb);
		//   	$form .= \Form::hidden('country', strtoupper($order->country));
		// 		$form .= \Form::hidden('email', $order->email);
		$form .= \Form::hidden('first_name', $order->first_name);
		$form .= \Form::hidden('last_name', $order->last_name);
		//   	$form .= \Form::hidden('state', strtoupper($order->state));
		// 		$form .= \Form::hidden('zip', $order->postcode);
	
		// 		$fields['address1'] = $order->address;
		// 		$fields['city'] = $order->suburb;
		// 		$fields['country'] = $order->country;
		// 		$fields['email'] = $order->email;
		// 		$fields['first_name'] = $order->first_name;
		// 		$fields['last_name'] = $order->last_name;
		// 		$fields['state'] = $order->state;
		// 		$fields['zip'] = $order->postcode;
	
		// HTML Variables for Displaying PayPal Checkout Pages
		$form .= \Form::hidden('lc', 'AU');
		$form .= \Form::hidden('return', $return);
		
		$form .= \Form::hidden('rm', 2);
	
		$fields['lc'] = 'AU';
		$fields['return'] = $return;
	
		$form .= '</form>';
	
		// 		// Send form with curl
		// 		$ch = curl_init();
		// 		curl_setopt($ch, CURLOPT_URL, "https://www.' . $sandbox_str . 'paypal.com/cgi-bin/webscr");
		// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		// 		curl_setopt($ch, CURLOPT_POST, true);
	
		// 		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		// 		$output = curl_exec($ch);
		// 		//$info = curl_getinfo($ch);
		// 		curl_close($ch);
	
        return $form;
        
		//return \Theme::instance()->view($this->view_dir . 'payment_form', array('title' => 'Pluris shoes' ,'msg' => 'Please wait, you will be redirected to the PayPal payment.', 'form' => $form), false);
	
	}
	
	public function action_final_response($param = false)
	{
		$post = \Input::post();

		if(isset($post['payment_status']) && $post['payment_status'] != '')
		{
			\Messages::info(__('Thank you for your order. The current status of your payment is :status. You can check your order and payment status at My Pluris page.', array('status' => $post['payment_status'])));
			\Response::redirect(\Uri::front_create('order/checkout/final'));
		}
			
	}
	
	/**
	 * Payment methods select list
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_second_option($language = false, $method = false)
	{
		if(!\Input::is_ajax())
		{
			die('Access is denied.');
		}
	
		$out = array();
		
		$out['data'] = '';
	
		$out['data'] = '<h3>Paypal</h3>';

		$out['data'] .= '<p class="marg_top_b">' .  __('Sie werden am Ende Ihres Bestellvorgangs für die Zahlungsabwicklung direkt zu PayPal weiter geleitet. Erst danach ist Ihre Bestellung erfolgreich abgeschlossen.') . '</p>';

		echo json_encode($out);
		
	}
	
}
