<?php

/**
 * Part of Eximius Solutions CMS.
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Page
 * @extends \Controller_Base_Public
 */

namespace Payment;

class Controller_Base_Public extends \Controller_Base_Public
{
	public $language;
	public $languages;
	
	/**
	 * Get enabled payments
	 *
	 * @access  public
	 * @param   string   $get = Get payments that are: enabled | disabled | all
	 * @param   string   $load_config = Load payments config
	 * @param   bool     $allowed
	 * @return  array
	 *
	 */
	public function action_get_payments($get = 'enabled', $load_config = true, $allowed = true)
	{
		if( ! \Request::is_hmvc())
		{
			die('Access is denied.');
		}
		
		$user = \Sentry::user();
		$out = array();
		$path =  APPPATH . "modules" . DS . "payment" . DS . "config" . DS ;
		$config_files = \File::read_dir($path);
		if(!empty($config_files))
		{
			foreach ($config_files as $file)
			{
				$file_parts = pathinfo($file); 
				$payment_config = include_once $path . $file;
				
				// Check allowed
				if($allowed && isset($payment_config['allowed']) && is_array($payment_config['allowed']))
				{
					foreach ($payment_config['allowed'] as $key => $value)
					{
						if(!isset($user['metadata'][$key]) || !in_array($user['metadata'][$key], $value)) continue 2;
					}
				}
				
				switch ($get)
				{
					case 'enabled':
						if($payment_config['enabled'])
						{
							$out[$payment_config['code']] = $payment_config['name'];
							if($load_config) \Config::load('payment::' . $file_parts['filename'], $file_parts['filename']);
						}
						
						break;
						
					case 'disabled':
						if(!$payment_config['enabled'])
						{
							$out[$payment_config['code']] = $payment_config['name'];
							if($load_config) \Config::load('payment::' . $file_parts['filename'], $file_parts['filename']);
						}
						
						break;

					default:
					case 'all':
						$out[$payment_config['code']] = $payment_config['name'];
						if($load_config) \Config::load('payment::' . $file_parts['filename'], $file_parts['filename']);

				}

			}			
			
		}
	
		return $out;
	}
	
	/**
	 * Order info: total price, quantity, num of items, items returned
	 *
	 * @access public
	 * @param  int    $id = Order id
	 * @return array  $out
	 */
	public function order_info1($id = false)
	{
		$out = array();
	
		if($item = \Order\Model_Order::find_one_by_id($id))
		{
			if(!empty($item->products))
			{
				$sum_products = count($item->products);
				$sum_products_quantity = 0;
				$total_price = 0;
					
				foreach ($item->products as $key => $product)
				{
					$sum_products_quantity += $product->quantity;
					$total_price += $product->price * $product->quantity;
				}
	
				$out['total_price'] =  $total_price;
				$out['quantity'] =  $sum_products_quantity;
				$out['num_items'] =  $sum_products;
				$out['currency'] =  'AUD';
	
			}
		}
	
		return $out;
	}
	
    /**
     * Order info: total price, quantity, num of items, currency,
     * shipping price, grand total price
     * 
     * @param type $id
     * @return type
     */
    public function order_info($id = false)
    {
        $out = array();

        if($item = \Order\Model_Order::find_one_by_id($id) )
        {
            if(!empty($item->products))
            {
                $currency               = $item->currency;
                $sum_products           = count($item->products);
                $sum_products_quantity  = 0;
                $total_price            = 0;

                foreach ($item->products as $key => $product)
                {
                    $total_price += $product->sub_total_with_tax;
                }

                $out = array(
                    'total_price'       => $total_price ,
                    'quantity'          => $sum_products_quantity,
                    'num_items'         => $sum_products,
                    'currency'          => $currency,
                    'shipping_price'    => $item->shipping_price,
                    'grand_total_price' => $total_price + $item->shipping_price
                );

                if( $item->is_sample_order == 1)
                {
                     $out = array(
                    'total_price'       => $item->total_price ,
                    'quantity'          => $sum_products_quantity,
                    'num_items'         => $sum_products,
                    'currency'          => $currency,
                    'shipping_price'    => $item->shipping_price,
                    'grand_total_price' => $item->total_price + $item->shipping_price
                );
                }

            }
        }
        return $out;
    }
    
	/**
	 * Second option in form - rewrite in child payment class if necessary
	 *
	 * @access public
	 * @return void
	 */
	public function action_second_option()
	{
		return;
	}
	
	/**
	 * Validation - rewrite in child payment class if necessary
	 *
	 * @access public
	 * @return void
	 */
	public static function form_validation()
	{
		return;	
	}	
	
	/**
	 * Final response
	 *
	 * @access public
	 * @return void
	 */
	public function action_final_response()
	{
		return;
	}
	
	/**
	 * Send email
	 *
	 * @access public
	 * @param  object               $order = Order object
	 * @param  array of objects     $products = Products from order
	 * @param  string               $type = Type of email to send
	 * @return void
	 */
	public function send_email($order = false, $products = false, $type = 'job')
	{
		// Send email to user
		\Package::load('email');
        
		// Load email addresses from config (these will be bcc receivers)
		\Config::load('auto_response_emails', 'autoresponders');
			
		$bcc = \Config::get('autoresponders.order_emails', false);
		if(!$bcc) $bcc = \Config::get('autoresponders.default_emails', false);
		
		$email_data = array(
            'order'         => $order,
            'products'      => $products,
            'site_title'    => \Config::get('site_title'),
		);
			
		$email = \Email::forge();
		$email->to($order['email'], ucwords($order['first_name'] . ' ' . $order['last_name']));
		if($bcc) $email->bcc($bcc);
		$email->subject($email_data['site_title']. ' - Your Order');
		
        // Set correct email view
        $email_view = ($type == 'credits' ? 'order_credits' : 'order');
        
		$email_html = \Theme::instance()->view('views/_email/' . $email_view)->set('email_data', $email_data, false);
		
		$email->html_body($email_html);

		try
		{
			$email->send();
			\Messages::success('A copy of your request has been sent to ' . $order['email'] . ' for your own reference.');
		}
		catch(\EmailValidationFailedException $e)
		{
			\Messages::error('Error while sending email.');
		}
		catch(\EmailSendingFailedException $e)
		{
			\Messages::error('Error while sending email.');
		}
	}
    
    /**
	 * Add user credits depending of order
	 *
	 * @access public
	 * @param  object	          $order = Order object
	 * @param  array of objects   $products = Products from order
	 * @return void
	 */
	public function add_credits($order = false, $products = false)
	{
		if(!is_numeric($order->user_id)) return false;
		
		// Get user to edit
		if(!\Sentry::user_exists((int)$order->user_id)) return false;
        
        $user = new \Sentry_User((int)$order->user_id);
        
        // Add user credits
        $update = array(
            'metadata' => array(
                'seek_credits'      => $user->get('metadata.seek_credits') + $order->seek,
                'careerone_credits' => $user->get('metadata.careerone_credits') + $order->careerone,
            ),
        );
        
        // Update user credits
        if($user->update($update))
        {
            return true;
        }
        
        return false;
        
	}
	
    /**
	 * Remove user credits depending of order
	 *
	 * @access public
	 * @param  object	          $order = Order object
	 * @param  array of objects   $products = Products from order
	 * @return void
	 */
	public function remove_credits($order = false, $products = false)
	{
		return \Payment\Model_Payment::remove_credits($order, $products);
	}
    
    /**
	 * Publish order job
	 *
	 * @access public
	 * @param  object	          $order = Order object
	 * @param  array of objects   $products = Products from order
	 * @return void
	 */
	public function publish_order_job($order = false, $products = false)
	{
		return \Payment\Model_Payment::publish_order_job($order, $products);
	}
	
}