<?php

namespace Payment;

class Controller_Admin_Payment extends \Admin\Controller_Base
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/payment/';
	
	public function before()
	{
		parent::before();
		
        if(!\Sentry::user()->in_group('Super Admin') )
        {
            \Response::redirect('admin/order/list'); 
        }
	}
	
	/**
	 * The index action
	 * 
	 * @access public
	 * @return void
	 */
	public function action_paypal()
	{
		$settings = \Config::load('paypal.db');

        if (\Input::post()) {
        	$input = \Input::post();

        	if(!\Input::is_ajax())
			{
				$val = Model_Paypal::validate('create');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update settings</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
				}
				else {
		        	try {

		        		\Config::save('paypal.db', array(
		        					'activate' => $input['activate'],
		        					'name' => $input['name'],
		        					'code' => $input['code'],
		        					'mode' => $input['mode'],
		        					'username_test' => $input['username_test'],
		        					'password_test' => $input['password_test'],
		        					'signature_test' => $input['signature_test'],
		        					'username_live' => $input['username_live'],
		        					'password_live' => $input['password_live'],
		        					'signature_live' => $input['signature_live']
		        				));

		        		
		        		// $setting->save();

		        		\Messages::success('Paypal successfully updated.');
						\Response::redirect('admin/payment/paypal');
		        	} 
		        	catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update settings.</strong>');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
					}
				}
	        }
        }

        \View::set_global('menu', 'admin/payment/paypal');
		\View::set_global('title','Paypal');
		\Theme::instance()->set_partial('content', $this->view_dir . 'paypal')
		->set('settings', $settings, false);
	}
	public function action_securePay()
	{
		$settings = \Config::load('securePay.db');

        if (\Input::post()) {
        	$input = \Input::post();

        	if(!\Input::is_ajax())
			{
				$val = Model_Securepay::validate('update');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update settings</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
				}
				else {
		        	try {

		        		\Config::save('securePay.db', array(
		        					'activate' => $input['activate'],
		        					'name' => $input['name'],
		        					'code' => $input['code'],
		        					'mode' => $input['mode'],
		        					'merchantId_test' => $input['merchantId_test'],
		        					'password_test' => $input['password_test'],
		        					'merchantId_live' => $input['merchantId_live'],
		        					'password_live' => $input['password_live']
		        				));

		        		
		        		// $setting->save();

		        		\Messages::success('SecurePay successfully updated.');
						\Response::redirect('admin/payment/securePay');
		        	} 
		        	catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update settings.</strong>');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
					}
				}
	        }
        }
        \View::set_global('menu', 'admin/payment/paypal');
		\View::set_global('title','SecurePay');
		\Theme::instance()->set_partial('content', $this->view_dir . 'securePay')
		->set('settings', $settings, false);
	}
	public function action_eWay()
	{
		$settings = \Config::load('eWay.db');

        if (\Input::post()) {
        	$input = \Input::post();

        	if(!\Input::is_ajax())
			{
				$val = Model_Eway::validate('update');
				
				if(!$val->run())
				{
					if($val->error() != array())
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update settings</strong>');
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
					
				}
				else {
		        	try {

		        		\Config::save('eWay.db', array(
		        					'activate' => $input['activate'],
		        					'name' => $input['name'],
		        					'code' => $input['code'],
		        					'mode' => $input['mode'],
		        					'customerId_test' => $input['customerId_test'],
		        					'username_test' => $input['username_test'],
		        					'password_test' => $input['password_test'],
		        					'customerId_live' => $input['customerId_live'],
		        					'username_live' => $input['username_live'],
		        					'password_live' => $input['password_live']
		        				));

		        		
		        		// $setting->save();

		        		\Messages::success('eWay successfully updated.');
						\Response::redirect('admin/payment/eWay');
		        	} 
		        	catch (\Database_Exception $e)
					{
						// show validation errors
						\Messages::error('<strong>There was an error while trying to update settings.</strong>');
					    
						// Uncomment lines below to show database errors
						$errors = $e->getMessage();
				    	\Messages::error($errors);
					}
				}
	        }
        }
        \View::set_global('menu', 'admin/payment/paypal');
		\View::set_global('title','eWay');
		\Theme::instance()->set_partial('content', $this->view_dir . 'eWay')
		->set('settings', $settings, false);
	}
	public function action_settings()
	{
		$settings = \Config::load('payment_settings.db');

        if (\Input::post()) {
        	$input = \Input::post();

        	if(!\Input::is_ajax())
			{
	        	try {

	        		\Config::save('payment_settings.db', array(
	        					'enable_partial_payment' => $input['enable_partial_payment']?:0,
	        				));

	        		
	        		// $setting->save();

	        		\Messages::success('Settings successfully updated.');
					\Response::redirect('admin/payment/settings');
	        	} 
	        	catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update settings.</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
	        }
        }
        \View::set_global('menu', 'admin/payment/paypal');
		\View::set_global('title','Settings');
		\Theme::instance()->set_partial('content', $this->view_dir . 'settings')
		->set('settings', $settings, false);
	}
}