<?php 

/**
 * Part of Eximius Solutions CMS.
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 *
 * @namespace Page
 * @extends \Admin\Controller_Base
 */

namespace Payment;

class Model_Payment extends \Model_Base
{
			
    // Set the table to use
    protected static $_table_name = 'payments';

    // List of all columns that will be used on create/update
    protected static $_properties = array(
        'id',
        'order_id',
        'total_price',
        'method',
        'status',
        'status_detail',
        'response',
        'custom_data',
        'partial_payment',
    );

    protected static $_created_at = 'created_at';
    protected static $_updated_at = 'updated_at';
    
    
    /**
	 * Publish order job
	 *
	 * @access public
	 * @param  object	          $order = Order object
	 * @param  array of objects   $products = Products from order
	 * @return void
	 */
	public static function publish_order_job($order = false, $products = false)
	{        
        // Include all jobs, even innactive one
        \Job\Model_Job::filter_active(false);
        
		if($job = \Job\Model_Job::find_one_by('id', $order->job_id))
		{
            
            $query = \DB::update(\Job\Model_Job::get_protected('_table_name'));
            $query->where('id', $order->job_id);
            $query->set(array(
                'status' => 2,
                'active_from' => strtotime("now"),
                'active_to' => strtotime("+30 days"),
            ));
            $result = $query->execute();           
            
            if($result)
            {
                return $job;
            }
		}
        
        return false;
	}
    
    /**
	 * Remove user credits depending of order
	 *
	 * @access public
	 * @param  object	          $order = Order object
	 * @param  array of objects   $products = Products from order
	 * @return void
	 */
	public static function remove_credits($order = false, $products = false)
	{
		if(!is_numeric($order->user_id)) return false;
		
		// Get user to edit
		if(!\Sentry::user_exists((int)$order->user_id)) return false;
        
        $user = new \Sentry_User((int)$order->user_id);
        
        // Remove user credits
        $seek = $user->get('metadata.seek_credits') - ($order->seek > 0 ? 1 : 0);
        $seek >= 0 or $seek = 0;
        
        $careerone = $user->get('metadata.careerone_credits') - ($order->careerone > 0 ? 1 : 0);
        $careerone >= 0 or $careerone = 0;
        
        $update = array(
            'metadata' => array(
                'seek_credits'      => $seek,
                'careerone_credits' => $careerone,
            ),
        );
        
        // Update user credits
        if($user->update($update))
        {
            return true;
        }
        
        return false;
	}

}