<?php

namespace News;

class Controller_News extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/news/';
	
	public $theme_view_path;
    
     // Global theme variables
    public $page_theme;
    
	public function before()
	{
		parent::before();
		
        \Config::load('news::news', 'details');

		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
		
		$this->theme_view_path = DOCROOT . \Theme::instance()->asset_path($this->view_dir);
	}
	
	/**
	 * Index application
	 * 
	 * @access  public
     * @param   $slug
	 */
	
    public function action_index($slug = false)
	{
        $item = false;
         
        // Find content by slug
        if($slug !== false)
        {
           $category_parents = false;
           
           if($item = \Application\Model_Application::get_by_slug($slug))
            {
            	$children = $item->children;
               
                $page = \Theme::instance()->set_partial('content', $this->view_dir . 'category');
                $page->set('item', $item, false);
                $page->set('children', $children, false);       
               
            }
            else if($item = \Application\Model_Casestudy::get_by_slug($slug)){
            	
        		$category_parents = $item->parent;
                $page = \Theme::instance()->set_partial('content', $this->view_dir . 'single');
                $page->set('item', $item, false);
                $page->set('category_parents', $category_parents, false);
            }
            else{
            	throw new \HttpNotFoundException;
            }
        }
        else
        {
           $categories = \Application\Model_Application::find();
           $page = \Theme::instance()->set_partial('content', $this->view_dir . 'categories');
           $page->set('categories', $categories, false);
  
        }
	}

}