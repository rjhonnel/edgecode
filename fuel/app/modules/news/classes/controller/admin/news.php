<?php

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 * 
 * @package  app
 * @extends  Controller
 */
namespace News;

class Controller_Admin_News extends \Admin\Controller_Base 
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/news/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('news::news', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * The basic welcome message
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		\Messages::instance()->shutdown();
		\Response::redirect('admin/news/list');
	}
    
    public function get_search_items()
    {
        /************ Start generating query ***********/
		$items = Model_News::find(function($query){
			
			// Get search filters
			foreach(\Input::get() as $key => $value)
			{
				if(!empty($value) || $value == '0')
				{
					switch($key)
					{
						case 'title':
							$query->where($key, 'like', "%$value%");
							break;
						case 'featured':
							if(is_numeric($value))
								$query->where($key, $value);
							break;
						case 'status':
							if(is_numeric($value))
								$query->where($key, $value);
							break;
						case 'active_from':
                            $date = strtotime($value);
                            if($date)
                                $query->where($key, '>=', $date);
							break;
						case 'active_to':
                            $date = strtotime($value);
                            if($date)
                                $query->where($key, '<=', $date);
							break;
					}
				}
			}
			
			// Order query
			$query->order_by('sort', 'asc');
			$query->order_by('id', 'asc');
		});
        
		/************ End generating query ***********/
		
		// Reset to empty array if there are no result found by query
		if(is_null($items)) $items = array();
		
		// Initiate pagination
		$pagination = \Hybrid\Pagination::make(array(
			'total_items' => count($items),
			'per_page' => \Input::get('per_page', 10),
			'uri_segment' => null,
		));

		// Remove unwanted items, and show only required ones
		$items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        $status = array(
            'false' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive',
            '2' => 'Active in period',
        );
        
        return array('items' => $items, 'pagination' => $pagination, 'status' => $status);
    }
	
	public function action_list()
	{
		\View::set_global('title', 'List News');

		
        $search = $this->get_search_items('root');
        
		$items      = $search['items'];
        $pagination = $search['pagination'];
        $status     = $search['status'];
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'list')
			->set('items', $items)
			->set('pagination', $pagination, false)
			->set('status', $status);
	}
	
	public function action_video()
	{
		$url = \Input::post('url', false);
		$return['response'] = false;
		
		if($url)
		{
			$youtube = \App\Youtube::forge();
			
			$video = $youtube->parse($url)->get();
			
			if(isset($video) && $video)
			{
				$return['response'] = $video;
				\Messages::success('Video found! Please check found data');
			}
			else
			{
				\Messages::error('Please check video URL and try again.');
			}
		}
		else
		{
			\Messages::info('Please insert video URL');
		}
		
		$return['message'] 	= \Messages::display('left', false);
		
		echo json_encode($return); exit;
	}
	
	public function action_create()
	{
		\View::set_global('title', 'Add New');
		
		if(\Input::post())
		{
			$val = Model_News::validate('create');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->image))
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload news image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload news image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false)))
			{
				// Get POST values
				$insert = \Input::post();
				
				// Prepare some values
				$insert['published_at'] = !empty($insert['published_at']) ? strtotime($insert['published_at']) : \Date::forge()->get_timestamp();
				$insert['active_from'] 	= !empty($insert['active_from']) ? strtotime($insert['active_from']) : NULL;
				$insert['active_to'] 	= !empty($insert['active_to']) ? strtotime($insert['active_to']) : NULL;
				if($insert['status'] != 2)
				{
					unset($insert['active_from']);
					unset($insert['active_to']);
				}
				
				$item = Model_News::forge($insert);
				
				try{
					$item->save();
					
					// Validate and insert news slug into SEO database table
					$val_seo = Model_Seo::validate('create_seo');
					$insert_seo = array(
						'content_id' 	=> $item->id,
						'slug'			=> \Inflector::friendly_title($item->title, '-', true),
					);
					
					while(!$val_seo->run($insert_seo))
					{
						$insert_seo['slug'] = \Str::increment($insert_seo['slug'], 1, '-');
					}
					
					$item_seo = Model_Seo::forge($insert_seo);
					$item_seo->save();
					// END OF: SEO
					
					// Insert news images
					if($this->_image_data)
					{
						$item_image = array(
							array(
								'id'	=> 0,
								'data'	=> array(
									'content_id'	=> $item->id,
									'image'			=> $this->_image_data[0]['saved_as'],
									'alt_text'		=> \Input::post('alt_text', ''),
									'cover'			=> 1,
									'sort'			=> 1,
								),
							),
						);
						
						Model_News::bind_images($item_image);
					}
					
					\Messages::success('News successfully created.');
					\Response::redirect(\Input::post('update', false) ? \Uri::create('admin/news/update/' . $item->id) : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create news</strong>');
				    
					// Uncomment lines below to show database errors
					$errors = $e->getMessage();
			    	\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to create news</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
			
			// Delete uploaded image if there is news saving error
			if(isset($this->_image_data))
				$this->delete_image($this->_image_data[0]['saved_as']);
		}
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'create');
	}
	
	public function action_update($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/news/list');
		
		// Get news item to edit
		if(!$item = Model_News::find_one_by_id($id)) \Response::redirect('admin/news/list');


        \View::set_global('menu', 'admin/newslist');
		\View::set_global('title', 'Edit News');
		
		if(\Input::post())
		{
			$val = Model_News::validate('update');
			
			// Upload image and display errors if there are any
			$image = $this->upload_image();
			if(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)) 
			{
				// No previous images and image is not selected and it is required
				\Messages::error('<strong>There was an error while trying to upload content image</strong>');
				\Messages::error('You have to select image');
			}
			elseif($image['errors'])
			{
				\Messages::error('<strong>There was an error while trying to upload content image</strong>');
				foreach($image['errors'] as $error) \Messages::error($error);
			}
			
			if($val->run() && $image['is_valid'] && !(!$image['exists'] && \Config::get('details.image.required', false) && empty($item->images)))
			{
				/** IMAGES **/
				// Get all alt texts to update if there is no image change
				foreach(\Arr::filter_prefixed(\Input::post(), 'alt_text_') as $image_id => $alt_text)
				{
					if(strpos($image_id, 'new_') === false)
					{
						$cover_image = 0;
						if(\Input::post('cover_image'))
						{
							if(\Input::post('cover_image') == $image_id)
							$cover_image = 1;
						}
						$item_images[$image_id] = array(
							'id'	=> $image_id,
							'data'	=> array(
								'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
								'cover'			=> $cover_image,
							),
						);
					}
				}
				
				// Save images if new files are submitted
				if(isset($this->_image_data))
				{
					foreach($this->_image_data as $image_data)
					{
						$cover_count = count($item->images);
						if(strpos($image_data['field'], 'new_') === false)
						{
							// Update existing image
							if(str_replace('image_', '', $image_data['field']) != 0)
							{
								$image_id = (int)str_replace('image_', '', $image_data['field']);
								$cover_count--;
								$cover_image = 0;
								if(\Input::post('cover_image'))
								{
									if(\Input::post('cover_image') == $image_id)
									$cover_image = 1;
								}
								
								
								$item_images[$image_id] = array(
									'id'	=> $image_id,
									'data'	=> array(
										'content_id'	=> $item->id,
										'image'			=> $image_data['saved_as'],
										'alt_text'		=> \Input::post('alt_text_' . $image_id, ''),
										'cover'			=> $cover_image,
									),
								);
								
								$this->delete_image(\Input::post('image_db_' . $image_id, ''));
							}
						}
						else
						{
							// Save new image
							$image_tmp = str_replace('image_new_', '', $image_data['field']);
							$cover_image = 0;
							$item_images[0] = array(
								'id'	=> 0,
								'data'	=> array(
									'content_id'	=> $item->id,
									'image'			=> $image_data['saved_as'],
									'alt_text'		=> \Input::post('alt_text_new_' . $image_tmp, ''),
									'cover'			=> $cover_image,
									'sort'			=> $cover_count + 1,
								),
							);
						}
					}
				}
				Model_News::bind_images($item_images);
				/** END OF IMAGES **/
				
				// Get POST values
				$insert = \Input::post();
				
				// Prepare some values
				$insert['published_at'] = !empty($insert['published_at']) ? strtotime($insert['published_at']) : \Date::forge()->get_timestamp();
				$insert['active_from'] 	= !empty($insert['active_from']) ? strtotime($insert['active_from']) : NULL;
				$insert['active_to'] 	= !empty($insert['active_to']) ? strtotime($insert['active_to']) : NULL;
				
				if($insert['status'] != 2)
				{
					unset($insert['active_from']);
					unset($insert['active_to']);
				}
				
				$item->set($insert);
				
				try{
					$item->save();
					
					\Messages::success('News successfully updated.');
					\Response::redirect(\Input::post('exit', false) ? \Uri::create('admin/news/list/') : \Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update news</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				// Delete uploaded images if there is news saving error
				if(isset($this->_image_data))
					foreach($this->_image_data as $image_data)
						$this->delete_image($image_data['saved_as']);
						
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update news</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		$news = Model_News::find_one_by_id($id);
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update')->set('news', $news);
	}
	
	public function action_update_files($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/news/list');
		
		// Get news item to edit
		if(!$item = Model_News::find_one_by_id($id)) \Response::redirect('admin/news/list');
		
		\View::set_global('title', 'Edit News Files');
		
		if(\Input::post())
		{
			if(\Input::post('file_upload', false))
			{
				// Upload files and display errors if there are any
				$file = $this->upload_file();
				if(!$file['exists'] && \Config::get('details.image.required', false) && empty($item->images))
				{
					// No previous images and image is not selected and it is required
					\Messages::error('<strong>There was an error while trying to upload content file</strong>');
					\Messages::error('You have to select image');
				}
				elseif($file['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload content file</strong>');
					foreach($file['errors'] as $error) \Messages::error($error);
				}
				
				if($file['is_valid'] && !(!$file['exists'] && \Config::get('details.file.required', false) && empty($item->files)))
				{
					/** FILES **/
					// Get all alt texts to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'title_') as $file_id => $title)
					{
						if(strpos($file_id, 'new_') === false)
						{
							$item_files[$file_id] = array(
								'id'	=> $file_id,
								'data'	=> array(
									'title'		=> \Input::post('title_' . $file_id, ''),
								),
							);
						}
					}
					
					// Save files if new ones are submitted
					if(isset($this->_file_data))
					{
						foreach($this->_file_data as $file_data)
						{
							$cover_count = count($item->files);
							if(strpos($file_data['field'], 'new_') === false)
							{
								// Update existing file
								if(str_replace('file_', '', $file_data['field']) != 0)
								{
									$file_id = (int)str_replace('file_', '', $file_data['field']);
									$cover_count--;
									
									$item_files[$file_id] = array(
										'id'	=> $file_id,
										'data'	=> array(
											'content_id'	=> $item->id,
											'file'			=> $file_data['saved_as'],
											'title'			=> \Input::post('title_' . $file_id, ''),
										),
									);
									
									$this->delete_file(\Input::post('file_db_' . $file_id, ''));
								}
							}
							else
							{
								// Save new file
								$file_tmp = str_replace('file_new_', '', $file_data['field']);
								
								$item_files[0] = array(
									'id'	=> 0,
									'data'	=> array(
										'content_id'	=> $item->id,
										'file'			=> $file_data['saved_as'],
										'title'			=> \Input::post('title_new_' . $file_tmp, ''),
										'cover'			=> $cover_count == 0 ? 1 : 0,
										'sort'			=> $cover_count + 1,
									),
								);
							}
						}
					}
					
					Model_News::bind_files($item_files);
					
					\Messages::success('News documents successfully updated.');
					\Response::redirect(\Uri::admin('current'));
					/** END OF FILES **/
				}
				else
				{
					// Delete uploaded files if there is news saving error
					if(isset($this->_file_data))
						foreach($this->_file_data as $file_data)
							$this->delete_file($file_data['saved_as']);
							
					\Messages::error('<strong>There was an error while trying to update news documents</strong>');
				}
			}
			
			if(\Input::post('video_upload', false))
			{
				// Upload image and display errors if there are any
				$image = $this->upload_image('video');
				if($image['errors'])
				{
					\Messages::error('<strong>There was an error while trying to upload content image</strong>');
					foreach($image['errors'] as $error) \Messages::error($error);
				}
				
				if($image['is_valid'] && !(!$image['exists'] && \Config::get('details.video.required', false) && empty($item->videos)))
				{
					/** VIDEOS **/
					// Get all video urls to update if there is no image change
					foreach(\Arr::filter_prefixed(\Input::post(), 'video_url_') as $video_id => $url)
					{
						$not_updated[] = $video_id;
						
						if(strpos($video_id, 'new_') === false)
						{
							$video_url = \Input::post('video_url_' . $video_id, false);
							
							if($video_url)
							{
								// Check video
								$youtube = \App\Youtube::forge();
								$video = $youtube->parse(\Input::post('video_url_' . $video_id, ''))->get();
								
								if($video)
								{
									// Update existing videos
									$item_videos[$video_id] = array(
										'id'	=> $video_id,
										'data'	=> array(
											'content_id'	=> $item->id,
											'url'			=> \Input::post('video_url_' . $video_id, $video['link']) ?: $video['link'],
											'title'			=> \Input::post('video_title_' . $video_id, $video['title']) ?: $video['title'],
										),
									);
									
									// Remove video ID as it is updated correctly
									array_pop($not_updated);
									
									// Delete image and use default one
									if(\Input::post('video_delete_image_' . $video_id, false))
									{
										$item_videos[$video_id]['data']['thumbnail'] = '';
										$this->delete_image(\Input::post('video_file_db_' . $video_id, ''), 'video');
									}
								}
								else
								{
									\Messages::error('"' . \Input::post('video_url_' . $video_id, '') . '" is invalid video URL. Video not updated.');
								}
							}
							else
							{
								\Messages::error('Video URL can\'t be empty. Video not updated.');
							}
						}
						else
						{
							$video_url = \Input::post('video_url_' . $video_id, false);
							
							if($video_url)
							{
								// Check video
								$youtube = \App\Youtube::forge();
								$video = $youtube->parse(\Input::post('video_url_' . $video_id, ''))->get();
								
								if($video)
								{
									// Add new videos
									$item_videos[0] = array(
										'id'	=> 0,
										'data'	=> array(
											'content_id'	=> $item->id,
											'url'			=> \Input::post('video_url_' . $video_id, $video['link']) ?: $video['link'],
											'title'			=> \Input::post('video_title_' . $video_id, $video['title']) ?: $video['title'],
											'sort'			=> count($item->videos) + 1,
										),
									);
									
									// Remove video ID as it is updated correctly
									array_pop($not_updated);
								}
								else
								{
									\Messages::error('"' . \Input::post('video_url_' . $video_id, '') . '" is invalid video URL. Video not updated.');
								}
							}
						}
					}
					
					// Save images if new files are submitted
					if(isset($this->_image_data))
					{
						foreach($this->_image_data as $image_data)
						{
							if(!in_array(str_replace('video_file_', '', $image_data['field']), $not_updated))
							{
								if(strpos($image_data['field'], 'new_') === false)
								{
									// Update existing image
									if(str_replace('video_file_', '', $image_data['field']) != 0)
									{
										$video_id = (int)str_replace('video_file_', '', $image_data['field']);
										
										$item_videos[$video_id]['data']['thumbnail'] = $image_data['saved_as'];
										
										$this->delete_image(\Input::post('video_file_db_' . $video_id, ''), 'video');
									}
								}
								else
								{
									// Save new image
									$image_tmp = str_replace('video_file_new_', '', $image_data['field']);
									
									$item_videos[0]['data']['thumbnail'] = $image_data['saved_as'];
								}
							}
							else
							{
								// That video was not updated so we remove its image if there is one
								$this->delete_image($image_data['saved_as'], 'video');
							}
						}
					}
					Model_News::bind_videos($item_videos);
					
					\Messages::success('News videos successfully updated.');
					\Response::redirect(\Uri::admin('current'));
					/** END OF VIDEOS **/
				}
				else
				{
					// Delete uploaded images if there is news saving error
					if(isset($this->_image_data))
						foreach($this->_image_data as $image_data)
							$this->delete_image($image_data['saved_as'], 'video');
							
					\Messages::error('<strong>There was an error while trying to update news videos</strong>');
				}
			}
			
		}
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update_files')
			->set('news', Model_News::find_one_by_id($id));
	}
	
	/**
	 * Update news SEO options
	 * 
	 * @param $id	= News ID
	 */
	public function action_update_seo($id = false)
	{
		if(!is_numeric($id)) \Response::redirect('admin/news/list');
		
		// Get news item to edit
		if(!$item = Model_News::find_one_by_id($id)) \Response::redirect('admin/news/list');
		
		\View::set_global('title', 'Edit Meta Content');
		
		if(\Input::post())
		{
			$val = Model_Seo::validate('update', $item->id);
			
			// Get POST values
			$insert = \Input::post();
			
			if($val->run($insert))
			{
				// Update SEO database table
				$item_seo = Model_Seo::find_one_by_content_id($item->id);
				
				$item_seo->set($insert);
				
				try{
					$item_seo->save();
					
					\Messages::success('Meta content successfully updated.');
					\Response::redirect(\Uri::admin('current'));
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update meta content</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to update meta content</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
		
		\Theme::instance()->set_partial('content', $this->view_dir . 'update_seo')
			->set('news', Model_News::find_one_by_id($id));
	}
	
	public function action_delete($id = false)
	{
		if(is_numeric($id))
		{
			// Get news item to edit
			if($item = Model_News::find_one_by_id($id))
			{
				// Delete other content data like images, files, etc.
				if(!empty($item->images))
				{
					foreach($item->images as $image)
					{
						$this->delete_image($image->image);
						$image->delete();
					}
				}
				
				if(!empty($item->files))
				{
					foreach($item->files as $file)
					{
						$this->delete_file($file->file);
						$file->delete();
					}
				}
				
				if(!empty($item->videos))
				{
					foreach($item->videos as $video)
					{
						$this->delete_image($video->thumbnail, 'video');
						$video->delete();
					}
				}
				
				if(!empty($item->accordions))
				{
					foreach($item->accordions as $accordion)
					{
						\Request::forge('admin/news/accordion/delete/'.$accordion->id)->execute();
					}
				}
					
				try{
					
					$item->seo->delete();
					$item->delete();
					
					\Messages::success('News successfully deleted.');
				}
				catch (\Database_Exception $e)
				{
					// show validation errors
					\Messages::error('<strong>There was an error while trying to delete news</strong>');
				    
					// Uncomment lines below to show database errors
					//$errors = $e->getMessage();
			    	//\Messages::error($errors);
				}
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	public function action_sort($type = false)
	{
		if(!$type) return false;
		
		$items = \Input::post('sort');
		
		if(is_array($items))
		{
			foreach($items as $item)
			{
				list($item, $old_item) = explode('_', $item);
				if(is_numeric($item)) $sort[] = $item;
				if(is_numeric($old_item)) $old_sort[] = $old_item;
			}
				
			if(is_array($sort))
			{
				// Get starting point for sort
				$start = min($old_sort);
				$start = $start > 0 ? --$start : $start;
				
				$model = Model_News::factory(ucfirst($type));
				foreach($sort as $key => $id)
				{
					$item = $model::find_one_by_id($id);

					$item->set(array(
						'sort'	=> ++$start,
					));
					
					$item->save();
				}
				
				\Messages::success('Items successfully reordered.');
				
				echo \Messages::display('left', false);
			}
		}
	}
	
	/****************************** CONTENT VIDEOS ******************************/
	
	/**
	 * Delete content file
	 * 
	 * @param $video_id		= Video ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_video($video_id = false, $content_id = false)
	{
		if($video_id && $content_id)
		{
			$videos = Model_Video::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($videos)
			{
				if(isset($videos[$video_id]))
				{
					$video = $videos[$video_id];
					
					// If there is only one video and video is required
					if(count($videos) == 1)
					{
						if(\Config::get('details.video.required', false))
						{
							\Messages::error('You can\'t delete all videos. Please add new video in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Video::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $video->sort)->execute();
							// Delete video
							$this->delete_image($video->thumbnail, 'video');
							$video->delete();
							\Messages::success('Video was successfully deleted.');
						}
					}
					else
					{
						// Reset sort fields
						\DB::update(Model_Video::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $video->sort)->execute();
						// Delete video
						$this->delete_image($video->thumbnail, 'video');
						$video->delete();
						
						\Messages::success('Video was successfully deleted.');
					}
				}
				else
				{
					\Messages::error('Video you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Video you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/****************************** CONTENT IMAGES ******************************/
	
	/**
	 * Upload all contet images to local directory defined in $this->image_upload_config
	 * 
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function upload_image($content_type = 'image')
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// Image upload configuration
		$this->image_upload_config = array(
		    'path' => \Config::get('details.' . $content_type . '.location.root'),
		    'normalize' => true,
		    'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
		);
		
		\Upload::process($this->image_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save image
		    \Upload::save();
			$this->_image_data = \Upload::get_files();
			
			// Resize images to desired dimensions defined in config file
			try{
				foreach($this->_image_data as $image_data)
				{
					$image = \Image::forge(array('presets' => \Config::get('details.' . $content_type . '.resize', array())));
					$image->load($image_data['saved_to'].$image_data['saved_as']);
					
					foreach(\Config::get('details.' . $content_type . '.resize', array()) as $preset => $options)
					{
						$image->preset($preset);
					}
				}
				
				return $return;
			}
			catch(\Exception $e){
				$return['is_valid']	= false;
				$return['errors'][] = $e->getMessage();
			}
		}
		else
		{
			// IMAGE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, image is not uploaded
		return $return;
	}
	
	/**
	 * Delete content image
	 * 
	 * @param $image_id		= Image ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_image($image_id = false, $content_id = false)
	{
		if($image_id && $content_id)
		{
			$images = Model_Image::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($images)
			{
				if(isset($images[$image_id]))
				{
					$image = $images[$image_id];
					
					// If there is only one image and image is required
					if(count($images) == 1)
					{
						if(\Config::get('details.image.required', false))
						{
							\Messages::error('You can\'t delete all images. Please add new image in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
					else
					{
						if($image->cover == 1)
						{
							\Messages::error('You can\'t delete cover image. Set different image as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_Image::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $image->sort)->execute();
							// Delete image
							$this->delete_image($image->image);
							$image->delete();
							\Messages::success('Image was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('Image you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('Content Image you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete image from file system
	 * 
	 * @param $name 			= Image name
	 * @param $content_type 	= Content type to pull config from (Image, Video)
	 * 
	 */
	public function delete_image($name = false, $content_type = 'image')
	{
		if($name)
		{
			foreach(\Config::get('details.' . $content_type . '.location.folders', array()) as $folder)
			{
				@unlink($folder . $name);
			}
		}
	}
	
	/****************************** CONTENT FILES ******************************/
	
	/**
	 * Upload all contet files to local directory defined in $this->file_upload_config
	 * 
	 */
	public function upload_file()
	{
		$return['is_valid'] = true;
		$return['exists'] 	= false;
		$return['errors'] 	= false;
		
		// Check if there are selected files
		foreach(\Input::file() as $file)
		{
			if($file['name'] != '') $return['exists'] = true;
		}
		
		// No files selected, so no errors too
		if(!$return['exists']) return $return;
		
		// File upload configuration
		$this->file_upload_config = array(
		    'path' => \Config::get('details.file.location.root'),
		    'normalize' => true,
		    'ext_whitelist' => array('pdf', 'xls', 'xlsx', 'doc', 'docx', 'txt'),
		);
		
		\Upload::process($this->file_upload_config);
		
		// if there are any valid files
		if (\Upload::is_valid())
		{
			// Save file
		    \Upload::save();
			$this->_file_data = \Upload::get_files();
			
			return $return;
		}
		else
		{
			// FILE ERRORS
			if(\Upload::get_errors() !== array())
			{
				foreach (\Upload::get_errors() as $file)
				{
					foreach($file['errors'] as $key => $value)
					{
						$return['is_valid']	= false;
						$return['errors'][] = $value['message'];
					}
				}
			}
		}
		
		// If we got up to here, file is not uploaded
		return $return;
	}
	
	/**
	 * Delete content file
	 * 
	 * @param $file_id		= File ID
	 * @param $content_id	= Content ID
	 */
	public function action_delete_file($file_id = false, $content_id = false)
	{
		if($file_id && $content_id)
		{
			$files = Model_File::find(array(
			    'where' => array(
			        'content_id' => $content_id,
			    ),
			    'order_by' => array('sort' => 'asc'),
			), 'id');
			
			if($files)
			{
				if(isset($files[$file_id]))
				{
					$file = $files[$file_id];
					
					// If there is only one image and image is required
					if(count($files) == 1)
					{
						if(\Config::get('details.file.required', false))
						{
							\Messages::error('You can\'t delete all files. Please add new file in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							\Messages::success('File was successfully deleted.');
						}
					}
					else
					{
						// Dont use cover option for files
						if(FALSE && $file->cover == 1)
						{
							\Messages::error('You can\'t delete cover file. Set different file as cover in order to delete this one.');
						}
						else
						{
							// Reset sort fields
							\DB::update(Model_File::get_protected('_table_name'))->value('sort', \DB::expr('sort - 1'))->where('sort', '>', $file->sort)->execute();
							// Delete file
							$this->delete_file($file->file);
							$file->delete();
							
							// Set another file as cover if cover is deleted
							if($file->cover == 1)
							{
								$files = Model_File::find(array(
								    'where' => array(
								        'content_id' => $content_id,
								    ),
								    'order_by' => array('sort' => 'asc'),
								));
								
								$files[0]->cover = 1;
								$files[0]->save();
							}
							
							
							\Messages::success('File was successfully deleted.');
						}
					}
				}
				else
				{
					\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
				}
			}
			else
			{
				\Messages::error('File you are trying to delete don\'t exists. Check your url and try again.');
			}
		}
		
		\Response::redirect(\Input::referrer());
	}
	
	/**
	 * Delete file from file system
	 * 
	 * @param $name = File name
	 */
	public function delete_file($name = false)
	{
		if($name && \Config::get('details.file.location.root', false))
		{
			@unlink(\Config::get('details.file.location.root') . $name);
		}
	}
	
}

                            