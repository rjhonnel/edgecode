<?php

	namespace News;

	class Model_Video extends \Model_Base
	{
	    // Set the table to use
	    protected static $_table_name = 'content_videos';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
		    'content_id',
		    'content_type',
		    'url',
		    'title',
		    'thumbnail',
		    'sort',
		);
		
	    protected static $_defaults = array(
		    'content_type' => 'content',
		);
		
		 /**
	     * Join content tables before any data select
	     * 
	     * @param $query
	     */
	    public static function pre_find(&$query)
	    {
	    	$query->where('content_type', 'content');
	    }
	    
		/**
		 * Prep some query values
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
			{
				// Auto increment sort column
				$vars['sort'] = \DB::expr('(SELECT COALESCE(MAX(tmp.sort)+1, 1) FROM ' . static::$_table_name . ' tmp)');
			}
				
			return $vars;
		}
		
	}