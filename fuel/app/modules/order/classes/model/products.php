<?php 

/**
 * CMS
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 *
 * @namespace Order
 * @extends \Model_Base
 */

namespace Order;

class Model_Products extends \Model_Base
{
		// Temporary variable to store some query results
		public static $temp = array();
        
        // Global variable to force refreshing of cached lazy load results
		public static $refresh_lazy_load = false;
	
	    // Set the table to use
	    protected static $_table_name = 'order_products';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
			'order_id',
	    	'title',
	    	'description',
	    	'price',
            'price_type',
	    	'credit',
	    	'quantity',
            'product_id',
            'subtotal',
            'attributes',
            'product_category',
            'code',
            'artwork_required',
            'artwork_free_over',
            'attributes_id',
            'delivery_charge',
            'delivery_time',
            'packs',
            
		);
	    
	    protected static $_defaults = array(
	    	'credit'    => 0,
	    	'quantity'  => 1,
	    );	
        
        /**
		 * Get additional content data selected
		 *
		 * @param $result = Query result
		 */
		public static function post_find($result)
		{
			if($result !== null)
			{
				if(is_array($result))
				{
					foreach($result as $item)
					{
						// It will first check if we already have result in temporary result,
						// and only execute query if we dont. That way we dont have duplicate queries
				   
						// Get products
						$item->get_artwork = static::lazy_load(function() use ($item){
		    				return Model_Artwork::find(array(
								'where' => array(
									'order_product_id' => $item->id,
								),
                                'order_by' => array(
                                    'type' => 'asc'
                                )
							));
		    			}, $item->id, 'artwork');	
					}
				}
			}
		
			// return the result
			return $result;
		}
        
        private static function lazy_load($closure, $id, $type, $return = 'array')
        {
            return function() use ($closure, $id, $type, $return){
                if(!isset(\Order\Model_Products::$temp['lazy.'.$id.$type]))
                {
                    \Order\Model_Products::$temp['lazy.'.$id.$type] = $closure();

                    // Make sure we always return array
                    if($return == 'array')
                    {
                        if(!is_array(\Order\Model_Products::$temp['lazy.'.$id.$type])) 
                            \Order\Model_Products::$temp['lazy.'.$id.$type] = array();
                    }
                    else
                    {
                        if(!is_object(\Order\Model_Products::$temp['lazy.'.$id.$type]) && !is_bool(\Order\Model_Products::$temp['lazy.'.$id.$type])) 
                            \Order\Model_Products::$temp['lazy.'.$id.$type] = new \stdClass();
                    }
                }

                return \Order\Model_Products::$temp['lazy.'.$id.$type];
            };
        }

		
	}