<?php 

/**
 * CMS
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 *
 * @namespace Order
 * @extends \Model_Base
 */

namespace Order;

class Model_Artwork extends \Model_Base
{
		// Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'order_artworks';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
		    'id',
			'order_id',
	    	'order_product_id',
	    	'file_id',
            'quantity',
            'size',
	    	'parent_id',
            'name',
            'owned_by_storage',
            'created_on',
            'revision',
            'type',
            'cart_uid',
            'unique_id',
            'deleted_at',
            
		);
	    
	    protected static $_defaults = array();	    
		
        protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	}