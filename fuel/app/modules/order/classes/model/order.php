<?php 

/**
 * CMS
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 *
 * @namespace Order
 * @extends \Model_Base
 */

namespace Order;

class Model_Order extends \Model_Base
{
		// Temporary variable to store some query results
		public static $temp = array();
        
        // Global variable to force refreshing of cached lazy load results
		public static $refresh_lazy_load = false;
	
	    // Set the table to use
	    protected static $_table_name = 'orders';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
            'id',
            'user_id',
            'title',
            'first_name',
            'last_name',
            'company',
            'address',
            'address2',
            'suburb',
            'postcode',
            'country',
            'state',
            'phone',
            'email',
            'shipping_title',
            'shipping_first_name',
            'shipping_last_name',
            'shipping_company',
            'shipping_address',
            'shipping_address2',
            'shipping_suburb',
            'shipping_postcode',
            'shipping_country',
            'shipping_state',
            'shipping_phone',
            'shipping_email',
            'birthday',
            'guest',
            'status',
            'tracking_no',
            'date',
            'total_price',
            'notes',
            'delivery_notes',
            'id_discount',
            'discount_amount',
            'shipping_method',
            'shipping_price',
            'shipping_status',
            'special_order',
            'gst_price',
            'user_created',
            'user_updated',
            'finished',
            'accepted',
            
            'delivery_docket_file',
            'delivery_docket_title',
            'credit_account',
            'total_delivery_charge',
            'delivery_datetime',
            'delivery_datetime_list',
            'client_po',
            'cater_for',
            'date_viewed',
            
		);
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    protected static $_defaults = array(
            
	    );
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		
        protected function prep_values($vars)
		{
            if(\Sentry::check())
            {
               // Set user who is created/updated item
                if($this->is_new())
                    $vars['user_created'] = \Sentry::user()->id;
                else
                    $vars['user_updated'] = \Sentry::user()->id;
            }
				
			return $vars;
		}
         
        
		/**
		 * Get additional content data selected
		 *
		 * @param $result = Query result
		 */
		public static function post_find($result)
		{
			if($result !== null)
			{
				if(is_array($result))
				{
					foreach($result as $item)
					{
                        
						// It will first check if we already have result in temporary result,
						// and only execute query if we dont. That way we dont have duplicate queries
				   
						// Get products
						$item->get_products = static::lazy_load(function() use ($item){
		    				return Model_Products::find(array(
								'where' => array(
									'order_id' => $item->id,
								),
							));
		    			}, $item->id, 'products');	

                        // Get client notes
                        $item->get_client_notes = static::lazy_load(function() use ($item){
                            return \User\Model_Notes::find(array(
                                'where' => array(
                                    'user_id' => $item->user_id,
                                ),
                                'order_by' => array(
                                    'id' => 'desc',
                                ),
                                'limit' => 5
                            ));
                        }, $item->user_id, 'client_notes');  

		    			// Get order payments
		    			$item->get_payments = static::lazy_load(function() use ($item){
		    				return \Payment\Model_Payment::find(array(
                                'where' => array(
                                    'order_id' => $item->id,
                                ),
                                'order_by' => array(
                                    'id' => 'desc',
                                ),
		    				));
		    			}, $item->id, 'payments');

		    			// Get last payment status
		    			$item->get_last_payment = static::lazy_load(function() use ($item){
		    				$payments = \Payment\Model_Payment::find(array(
                                'where' => array(
                                    'order_id' => $item->id,
                                ),
                                'order_by' => array(
                                    'id' => 'desc',
                                ),
		    				));
                            
                            return isset($payments[0]) ? $payments[0] : array();
                            
		    			}, $item->id, 'last_payment', 'object');
                        
                        // Get history
						$item->get_history = static::lazy_load(function() use ($item){
		    				return Model_History::find(array(
								'where' => array(
									'order_id' => $item->id,
								),
                                'order_by' => array(
                                    'created_at' => 'desc',
                                ),
							));
		    			}, $item->id, 'history');	
                        
                        // Get artworks
						$item->get_artwork = static::lazy_load(function() use ($item){
		    				return Model_Artwork::find(array(
								'where' => array(
									'order_id' => $item->id,
								),
							));
		    			}, $item->id, 'artwork');
                        
                        // Get artworks status
						$item->get_artwork_status = static::lazy_load(function() use ($item){
                            $out = array(0,0);
                            $products = Model_Products::find(array(
								'where' => array(
									'order_id' => $item->id,
                                    'artwork_required' => 1,
								),
							));

                            if(!$products) return $out;
                            
                            $out[0] = count($products);
                            
                            foreach ($products as $product)
                            {
                                if($product->artwork) $out[1]++;
                            }
                            return $out;
                            
		    			}, $item->id, 'artwork_status', 'array');
                        
                        // Get order user object
						$item->get_user = static::lazy_load(function() use ($item){
                            return \Sentry::user((int)$item->user_id);
		    			}, $item->id, 'user', 'object');

					}
				}
			}
		
			// return the result
			return $result;
		}
		
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	     private static function lazy_load($closure, $id, $type, $return = 'array')
         {
            return function() use ($closure, $id, $type, $return){
                if(!isset(\Order\Model_Order::$temp['lazy.'.$id.$type]))
                {
                    \Order\Model_Order::$temp['lazy.'.$id.$type] = $closure();

                    // Make sure we always return array
                    if($return == 'array')
                    {
                        if(!is_array(\Order\Model_Order::$temp['lazy.'.$id.$type])) 
                            \Order\Model_Order::$temp['lazy.'.$id.$type] = array();
                    }
                    else
                    {
                        if(!is_object(\Order\Model_Order::$temp['lazy.'.$id.$type]) && !is_bool(\Order\Model_Order::$temp['lazy.'.$id.$type])) 
                            \Order\Model_Order::$temp['lazy.'.$id.$type] = new \stdClass();
                    }
                }

                return \Order\Model_Order::$temp['lazy.'.$id.$type];
            };
        }

	    
	    public static function order_info($id = false)
	    {
	    	$out = array();
	    
	    	if($item = \Order\Model_Order::find_one_by_id($id))
	    	{
	    		if(!empty($item->products))
	    		{
	    			$sum_products           = count($item->products);
	    			$sum_products_quantity  = 0;
	    			$total_price            = 0;
                    $total_order            = 0;
	    			$total_credits          = 0;
	    				
	    			foreach ($item->products as $key => $product)
	    			{
	    				$sum_products_quantity += $product->quantity;
	    				$total_price += $product->price * $product->quantity;
	    				$total_credits += $product->credit;
	    			}
	    
                    $out = array(
                        'total_price'   => $total_price,
                        'total_order'   => $item->total_price(),
                        'quantity'      => $sum_products_quantity,
                        'credits'       => $total_credits,
                        'num_items'     => $sum_products,
                    );
	    		}
	    	}
	    
	    	return $out;
	    }

        
        public static function order_info_active_products($id = false, $category = false)
        {
            $out = array();
        
            if($item = \Order\Model_Order::find_one_by_id($id))
            {
                if(!empty($item->products))
                {
                    $sum_products_quantity  = 0;
                    $total_price            = 0;
                    $total_order            = 0;
                    $total_credits          = 0;
                    
                    $count = 0;
                    foreach ($item->products as $key => $product)
                    {
                        if(!\Product\Model_Product::find_one_by_id($product->product_id))
                            continue;

                        if($category)
                        {
                            // check if product has category_id
                            $hold_product = \Product\Model_Product::find_one_by_id($product->product_id);

                            if(count($hold_product->categories) > 1)
                            {
                                $arrayKeys = array_keys($hold_product->categories);
                                if(!in_array($category->id, $arrayKeys))
                                    continue;
                            }
                            $categories_ids = \Product\Model_Category::get_all_children($category);
                            $hold_products = \Product\Model_Product_To_Categories::find(function($query) use ($category, $categories_ids)
                            {
                                return $query->where('category_id', 'in', $categories_ids);
                            }, 'product_id');
                            if(!in_array($product->product_id, array_keys($hold_products)))
                                continue;
                        }
                        
                        $sum_products_quantity += $product->quantity;
                        $total_price += $product->price * $product->quantity;
                        $total_credits += $product->credit;
                        $count++;
                    }
                    $sum_products           = $count;
        
                    $out = array(
                        'total_price'   => $total_price,
                        'total_order'   => $item->total_price(),
                        'quantity'      => $sum_products_quantity,
                        'credits'       => $total_credits,
                        'num_items'     => $sum_products,
                    );
                }
            }
        
            return $out;
        }

        public static function in_order_get_product_quantity_by_id($id = false, $product_id = false)
        {
            $quantity = 0;
            if($item = \Order\Model_Order::find_one_by_id($id))
            {
                if(!empty($item->products))
                {
                    foreach ($item->products as $key => $product)
                    {
                        if($product->product_id == $product_id)
                        {
                            $quantity += $product->quantity;
                        }
                    }
                }
            }
            return $quantity;
        }

        public static function in_order_get_product_price_by_id($id = false, $product_id = false)
        {
            $price = 0;
            if($item = \Order\Model_Order::find_one_by_id($id))
            {
                if(!empty($item->products))
                {
                    foreach ($item->products as $key => $product)
                    {
                        if($product->product_id == $product_id)
                        {
                            $price += ($product->price * $product->quantity);
                        }
                    }
                }
            }
            return $price;
        }
        
        public static function recalculate_order($id)
        {
            $total = 0;
            static::$refresh_lazy_load = true;
            
            if($item = \Order\Model_Order::find_one_by_id($id))
            {
                if(!empty($item->products))
                {
                    foreach ($item->products as $product)
                    {
                       $total += $product->price * $product->quantity;
                    }
                }
                
                $item->total_price = $total;
                $item->save();
            }
        }
        
        /**
         * Calculate order shipping price
         * 
         * @param type $postcode    = Postcode to calculate price
         * @param type $default     = Default return value if postcode dont exist or not set
         * @param type $default     = 
         * @return type
         */
        public function shipping_price($postcode = null, $default = null, $force_current_user = false, $is_ship = true)
        {
            // Default postcode to users postcode
            if(is_null($postcode))
            {
                if($force_current_user)
                {
                   if(\Model_Base::check_logged()) 
                   {
                        $user = \Sentry::user();
                        $postcode = $user->get('metadata.postcode'); 
                   }
                }
                else
                {
                    $postcode = $this->user->get('metadata.postcode');
                }
            }
            
            // Load shipping configuration
            \Config::load('product::shipping', 'shipping', true);

            $prices = \Arr::sort(\Config::get('shipping.price', array()), 'from');
            
            if(!is_numeric($default)) $default = \Config::get('shipping.default', 0);
            $target_price = $default;

            // If we dont have postcode we return zero shipping price
            if(!$postcode || !is_numeric($postcode))
            {
                return $target_price;
            }

            // Find coresponding price for this postcode
            foreach($prices as $price)
            {
                if($price['from'] <= $postcode && $price['to'] >= $postcode)
                {
                    $target_price = $price['price']; break;
                }
            }

            if($is_ship)
                $target_price = \Config::get('shipping.default');
            else
                $target_price = \Config::get('shipping.pickup');
            return $target_price;
        }
        
        /**
         * Calculate order total price
         * We will return sum of all kind of different 
         * prices that affect total price
         * 
         * @return type
         */
        public function total_price()
        {
            $total_price[]['price'] = $this->shipping_price;
            $total_price[]['price'] = $this->total_price;
            $total_price[]['price'] = $this->total_delivery_charge;
            
            return \Arr::sum($total_price, 'price') - $this->discount_amount;
        }
        
        public static function credit_account($user_id = null, $new_amount = 0)
        {
            if(is_numeric($user_id) && \Sentry::user_exists((int)$user_id)) 
            {
                 $user = \Sentry::user((int)$user_id);
            }
            else
            {
                $user = \Sentry::user();
            }
 
            $out['credit'] = false;
            
            if($user->get('metadata.credit_account') != 1) return $out;
            
            $out['credit'] = true;
        
            if($month = $user->get('metadata.purchase_limit_period'))
            {
                $start = strtotime(date('m-01-Y', strtotime("-{$month} month")));
                $orders_total = 0;

                $orders = \Order\Model_Order::find(array(
                    'where' => array(
                        array('created_at', '>', $start),
                        'user_id' => $user->get('id'),
                        'finished' => 1,
                    )
                ));
                
                if($orders)
                {
                    foreach ($orders as $order)
                    {
                        $orders_total += $order->total_price + $order->shipping_price;
                    }
                }
                if(($orders_total + $new_amount) > $user->get('metadata.purchase_limit_value'))
                {
                    $out['over_limit'] = true;
                    $out['over_limit_amount'] = ($orders_total + $new_amount) - $user->get('metadata.purchase_limit_value');
                }
                else 
                {
                    $out['over_limit'] = false;
                    $out['over_limit_amount'] = 0;
                }
                
                $out['orders_total'] = $orders_total;
                $out['limit'] = $user->get('metadata.purchase_limit_value');
                $out['period'] = $user->get('metadata.purchase_limit_period');
            }
            
            if(is_numeric(\Session::get('order.id')))
            {
            	$order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
            	if (isset($order))
            	{
            		$new_amount = ($order->total_price + $order->shipping_price) - $order->discount_amount;
            	}	
            }
            
            $out['current_total'] = $new_amount;
            return $out;
        }
        
        /**
         * Validate Model fields
         * 
         * @param $factory = Validation name, if you want to have multiple validations
         */
        public static function validate($factory)
        {
            $val = \Validation::forge($factory);
            $val->add('first_name', 'First Name')->add_rule('required');
            $val->add('last_name', 'Last Name')->add_rule('required');
            $val->add('address', 'Address')->add_rule('required');
            $val->add('suburb', 'Suburb')->add_rule('required');
            $val->add('country', 'Country')->add_rule('required');
            $val->add('postcode', 'Postcode')->add_rule('required');
            $val->add('phone', 'Phone')->add_rule('required');
            /*$val->add('shipping_first_name', 'Shipping First Name')->add_rule('required');
            $val->add('shipping_last_name', 'Shipping Last Name')->add_rule('required');
            $val->add('shipping_address', 'Shipping Address')->add_rule('required');
            $val->add('shipping_suburb', 'Shipping Suburb')->add_rule('required');
            $val->add('shipping_country', 'Shipping Country')->add_rule('required');
            $val->add('shipping_postcode', 'Shipping Postcode')->add_rule('required');
            $val->add('shipping_phone', 'Shipping Phone')->add_rule('required');*/
    
            return $val;
        }
	}
                            