<?php

namespace Order;

class Model_Xero extends \Model_Base
{
    // Temporary variable to store some query results
    public static $temp = array();

    // Set the table to use
    protected static $_table_name = 'config';

    // List of all columns that will be used on create/update
    protected static $_properties = array(
        'identifier',
        'config',
        'hash',
    );



    /**
     * Validate Model fields
     *
     * @param $factory = Validation name, if you want to have multiple validations
     */
    public static function validate($factory)
    {
        $val = \Validation::forge($factory);
        $val->add('name', 'Name')->add_rule('required');
        $val->add('code', 'Code')->add_rule('required');

        if(\Input::post('mode') == 0)
        {
            $val->add('auth_token', 'Authentication Token')->add_rule('required');
            $val->add('auth_secret', 'Authentication Secret')->add_rule('required');
        }
        else
        {
            $val->add('auth_token', 'Authentication Token')->add_rule('required');
            $val->add('auth_secret', 'Authentication Secret')->add_rule('required');
        }


        return $val;
    }
}