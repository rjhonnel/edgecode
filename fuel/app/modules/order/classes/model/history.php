<?php 

/**
 * CMS
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 *
 * @namespace Order
 * @extends \Model_Base
 */

namespace Order;

class Model_History extends \Model_Base
{
		// Temporary variable to store some query results
		public static $temp = array();
	
	    // Set the table to use
	    protected static $_table_name = 'order_history';
	    
	    // List of all columns that will be used on create/update
	    protected static $_properties = array(
            'id',
            'order_id',
            'event',
            'user_created',
            'user_updated',
		);
	    
	    protected static $_created_at = 'created_at';
	    protected static $_updated_at = 'updated_at';
	    
	    protected static $_defaults = array(

	    );
	    
		
		/**
		 * Save users that commited insert/update operations
		 * 
		 * @param $vars
		 */
		protected function prep_values($vars)
		{
			// Set user who is created/updated item
			if($this->is_new())
				$vars['user_created'] = \Sentry::user()->id;
			else
				$vars['user_updated'] = \Sentry::user()->id;
				
			return $vars;
		}
		
		/**
		 * Get additional content data selected
		 *
		 * @param $result = Query result
		 */
		public static function post_find($result)
		{
			if($result !== null)
			{
				if(is_array($result))
				{
					foreach($result as $item)
					{
						// It will first check if we already have result in temporary result,
						// and only execute query if we dont. That way we dont have duplicate queries
				   
						// Get products
						$item->get_products = static::lazy_load(function() use ($item){
		    				return Model_Products::find(array(
								'where' => array(
									'order_id' => $item->id,
								),
							));
		    			}, $item->id, 'products');	

		    			// Get order payments
		    			$item->get_payments = static::lazy_load(function() use ($item){
		    				return \Payment\Model_Payment::find(array(
                                'where' => array(
                                    'order_id' => $item->id,
                                ),
                                'order_by' => array(
                                    'id' => 'desc',
                                ),
		    				));
		    			}, $item->id, 'payments');

		    			// Get last payment status
		    			$item->get_last_payment = static::lazy_load(function() use ($item){
		    				$payments = \Payment\Model_Payment::find(array(
                                'where' => array(
                                    'order_id' => $item->id,
                                ),
                                'order_by' => array(
                                    'id' => 'desc',
                                ),
		    				));
                            
                            return isset($payments[0]) ? $payments[0] : array();
                            
		    			}, $item->id, 'last_payment', 'object');
					}
				}
			}
		
			// return the result
			return $result;
		}
		
		/**
	     * Load function result only once and than remember 
	     * it in $temp variable for later use
	     * 
	     * @param $closure		= Function for returning data
	     * @param $id			= Item ID
	     * @param $type			= Name for additional data
	     * @param $return		= Return type, either array or object (empty result will be casted in that type)
	     */
	    private static function lazy_load($closure, $id, $type, $return = 'array')
	    {
	    	return function() use ($closure, $id, $type, $return){
	    		if(!isset(\Order\Model_History::$temp['lazy.'.$id.$type]))
		    	{
		    		\Order\Model_History::$temp['lazy.'.$id.$type] = $closure();
		    		
		    		// Make sure we always return array
		    		if($return == 'array')
		    		{
			    		if(!is_array(\Order\Model_History::$temp['lazy.'.$id.$type])) 
			    			\Order\Model_History::$temp['lazy.'.$id.$type] = array();
		    		}
		    		else
		    		{
		    			if(!is_object(\Order\Model_History::$temp['lazy.'.$id.$type])) 
			    			\Order\Model_History::$temp['lazy.'.$id.$type] = new \stdClass();
		    		}
		    	}
		    	
		    	return \Order\Model_History::$temp['lazy.'.$id.$type];
	    	};
	    }

	}