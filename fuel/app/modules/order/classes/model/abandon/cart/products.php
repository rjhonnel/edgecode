<?php 

/**
 * CMS
 *
 * @package    EXCMS
 * @version    2.0
 * @author     CMS Development Team
 *
 * @namespace Order
 * @extends \Model_Base
 */

namespace Order;

class Model_Abandon_Cart_Products extends \Model_Base
{
	// Temporary variable to store some query results
	public static $temp = array();
    
    // Global variable to force refreshing of cached lazy load results
	public static $refresh_lazy_load = false;

    // Set the table to use
    protected static $_table_name = 'order_abandon_cart_products';
    
    // List of all columns that will be used on create/update
    protected static $_properties = array(
        'id',
        'order_id',
        'title',
        'description',
        'price',
        'price_type',
        'quantity',
        'product_id',
        'subtotal',
        'attributes',
        'attributes_id',
        'product_category',
        'code',
        'delivery_charge',
        'packs',
        
    );
    
    protected static $_defaults = array(
        'quantity'  => 1,
    );
		
}
                            