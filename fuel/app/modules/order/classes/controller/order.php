<?php

/**
 * CMS
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Order
 * @extends \Controller_Base_Public
 */

namespace Order;

class Controller_Order extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/order/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('order::cart', 'cart');
        \Config::load('order::order', true);
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
    public function action_cart_listing($type = 'cart')
    {
        if(!\Input::is_ajax()) die('Access Is Not Allowed!');
        
        $out = array();
        $out['cart_empty'] = 0;
        
        $items = \Cart::items();
        // Cart is empty?
        if(!$items) $out['cart_empty'] = 1;
        
        // Product table
        $out['product_table'] = \Theme::instance()->view('views/order/_partials/product_listing_table', array('items' => $items), false)->render();
        
        // Price table
        $out['price_table'] = \Theme::instance()->view('views/order/_partials/product_price_table', array('type' => $type), false)->render();
        
        echo json_encode($out);
        
        exit;
    }

}