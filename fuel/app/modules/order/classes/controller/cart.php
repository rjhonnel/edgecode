<?php

/**
 * CMS
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Order
 * @extends \Controller_Base_Public
 */

namespace Order;

class Controller_Cart extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/order/cart/';
	
	public function before()
	{
		parent::before();
		
		\Config::load('order::cart', 'details');
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
	/**
	 * Index page
	 * 
	 * @access  public
	 * @return  Response
	 */
	public function action_index()
	{
		// Keep existing messages
		//\Messages::instance()->shutdown();
		
		// Redirect to default method
        \Response::redirect(\Uri::create('order/cart/edit'));
	}
    
    /**
	 * Update cart content
	 *
	 * @access  public
	 * @return  Response
	 */
	protected function update_cart()
    {
        $items = \Cart::items();
        $product_subtotal = 0;
        $grand_total = 0;
        $product_stock = 0;
        $reach_stock_limit = 0;
        
        if(\Input::post() && $items)
        {
            try
            {
                $post = \Input::post();
                foreach ($items as $item)
                {
                
                    if(isset($post['minimum_order'][$item->get('minimum_order')])) {
                        if(intval($post['quantity'][$item->uid()]) < intval($post['minimum_order'][$item->get('minimum_order')]))
                        {
                           echo json_encode(array('error' => 1,'minimum_order' => $item->get('minimum_order')));
                           exit;  
                        }    
                    }
                   
                    if(isset($post['quantity'][$item->uid()]))
                    {
                        $quantity = $post['quantity'][$item->uid()];
                        // Update quantity of products in abandon cart
                        $order_id = \Session::get('order.id', false);
                        if(is_numeric($order_id))
                        {
                            $item_exists = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'product_id' => $item->get('id'), 'order_id' => $order_id, 'attributes_id' => $item->get('attributes') ) ) );

                            $product = \Product\Model_Product::find_one_by_id($item->get('id'));
                            $product_data = \Product\Model_Product::product_data($product, $item->get('attributes'));

                            if($product_data['stock_quantity'] > 0 || ($product_data['stock_quantity'] == 0 && $this->do_not_allow_buy_out_of_stock == 0)) //(if "Allow customer to buy if Out of Stock" is enabled user can update quantity of products without limit for no stock )
                            {
                                if($item_exists)
                                {
                                    $allow_quantity = $quantity;
                                    if(!($quantity <= $product_data['stock_quantity']) && $product_data['stock_quantity'] > 0) // if user input quantity greater than stock, use the remaining quantity as quantity
                                    {
                                        $allow_quantity = $product_data['stock_quantity'];
                                        $reach_stock_limit = true;
                                    }

                                    $product_subtotal = ($allow_quantity*$item->get('price'));
                                    $item_exists[0]->set(array('quantity' => $allow_quantity, 'subtotal' => $product_subtotal ));
                                    $item_exists[0]->save();
                                    $item->setQuantity($allow_quantity);
                                }

                            }

                            if($product_data['stock_quantity'] > 0)
                                $product_stock = $product_data['stock_quantity'];
                        }
                    }
                    $total_price = $item->totalDiscountedPrice(true);
                    $grand_total += $total_price;
                }

                // if(isset($post['artwork_quantity']))
                // {
                //     foreach ($post['artwork_quantity'] as $key => $value)
                //     {
                //         if($artwork = \Order\Model_Artwork::find_one_by_id($key))
                //         {
                //             $artwork->set(array('quantity' => $value));
                //             $artwork->save();
                //         }
                //     }
                // }
                // \Messages::success('Cart successfully updated.');
                echo json_encode(array(
                        'product_subtotal' => $product_subtotal,
                        'grand_total' => $grand_total,
                        'product_stock' => $product_stock,
                        'reach_stock_limit' => $reach_stock_limit
                    ));
            }
            catch (\Database_Exception $e)
            {
                // show errors
                // \Messages::error('There was an error while trying to update cart.');
            }
        }
    }
    
    public function action_update_cart()
    {
        
//        echo json_encode(\Input::post());
//        exit;
        
        $this->update_cart();
        
        if(!\Input::is_ajax()) 
        {
            \Response::redirect(\Input::referrer(\Uri::create('/')));
        }
        else 
        {
            echo \Messages::display();
        }
        
    }
    
    /**
	 * Delete item from cart
	 *
	 * @access  public
	 * @return  Response
	 */
    public function action_delete($uid = false)
    {

        $item = \Cart::item($uid);
        
        if($item) 
        {
            \Cart::remove($uid);
            \Messages::success('Product successfully deleted from cart.');
            
            $items = \Cart::items();
            if(empty($items)) \Messages::info('Your cart is empty.');
        }
        else
        {
            \Messages::error('There was an error while trying to delete product from cart.');
        }
        
        \Response::redirect(\Input::referrer(\Uri::create('order/cart/edit')));
    }
	
    public function action_open_cart()
    {
        $items = \Cart::items();
        
        $this->page_theme = \Theme::instance()->set_partial('content', $this->view_dir . 'cart');
        $this->page_theme->set('items', $items, false);
    }
    
    public function action_edit_cart()
    {
        $items = \Cart::items();
        $max_types_required = array();
        $max_files = \Config::get('order.artwork.max_types_required', 5);
        
        \View::set_global('title', 'Cart');
        $this->page_theme = \Theme::instance()->set_partial('content', $this->view_dir . 'edit_cart');
        $this->page_theme->set('items', $items, false);
    }
    
    public function action_remove_cart_item($uid = false)
    {
        $item = \Cart::item($uid);
        if($item) 
        {
            if($unique_id = $item->get('unique_id'))
            {
                // Set artwork for delete
                if($artworks = \Order\Model_Artwork::find(array('where' => array('unique_id' => $unique_id))))
                {
                    foreach ($artworks as $artwork)
                    {
                    	$artwork->set(array('deleted_at' => time()));
                        $artwork->save();
                    }
                }
            }
            
            \Cart::remove($uid);

            // remove product in abandon cart
            $order_id = \Session::get('order.id', false);
            if(is_numeric($order_id))
            {
                $item_exists = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'product_id' => $item->get('id'), 'order_id' => $order_id, 'attributes_id' => $item->get('attributes') ) ) );
                if($item_exists)
                {
                    $item_exists[0]->delete();
                }
            }
                
            \Messages::success('Product successfully deleted from cart.');
        }
        else
        {
            \Messages::error('There was an error while trying to delete product from cart.');
        }
        echo \Messages::display();
    }
    
    public function action_clear_cart()
    {
        $items = \Cart::items();
        
        $order = null;
        if(is_numeric(\Session::get('order.id'))) $order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
        
        if($order)
        {
            $order->delete();
        }
         
        if($items)
        {
            foreach ($items as $item)
            {
                // Find artworks
               if($unique_id = $item->get('unique_id'))
               {
                   if($artworks = \Order\Model_Artwork::find(array('where' => array('unique_id' => $unique_id, 'order_id' => \Session::get('order.id')))))
                   {
                       $ysi = \Yousendit\Base::forge();

                       // Artworks (update, delete)
                       foreach ($artworks as $artwork)
                       {
                           // Remove deleted artwork
                           if($artwork->file_id) 
                           {
                               $ysi->delete_artwork($artwork->file_id);
                           }
                       }
                   }
               }
            }
        }

        // clear products in abandon cart
        $order_id = \Session::get('order.id', false);
        if(is_numeric($order_id))
        {
            $item_exists = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'order_id' => $order_id ) ) );
            if($item_exists)
            {
                foreach ($item_exists as $product)
                {
                    $product->delete();
                }
            }
        }
         
        // Delete order & cart session
        \Session::delete('order.id');
        \Cart::clear();
        \Messages::info('Your cart is empty.');
        
        if(\Input::is_ajax())
        {
           echo \Messages::display();
           exit;
        }
        else
        {
            \Response::redirect(\Input::referrer(\Uri::create('order/checkout/address')));
        }
        
    }
    
    public function action_cart_info()
    {
        $out = array();
        $out['total'] = \Cart::total();
        $out['item_count'] = \Cart::itemCount();
        $out['items'] = count(\Cart::items());
        echo json_encode($out);
        exit;
    }
    
    public function action_add_artwork_type()
    {
        $artwork_table = '';
        $count_artworks = 0;
        
        if(\Input::post())
        {
            $post = \Input::post();
            $item = \Cart::item($post['uid']);
            $items = $post['items'];
            $artwork_type = $post['artwork_type'];
            
            if($item)
            {
                if($artworks = \Order\Model_Artwork::find(array('where' => array('unique_id' => $item->get('unique_id'), 'order_id' => \Session::get('order.id'), 'deleted_at' => 0))))
                {
                    $count_artworks = count($artworks);
                }
                   
                if($artwork_type > $items)
                {
                    for ($i = $items; $i < $artwork_type; $i++) 
                    {
                        $artwork_table .= '<tr>';
                        $artwork_table .= '<td></td>';
                        
                        $artwork_table .= '<td>';
                        
                        $artwork_table .= \Form::open(array('action' => '', 'method' => 'post', 'enctype' => 'multipart/form-data', 'class' => 'edit_artwork_file_upload'));
                        $artwork_table .= \Form::file('fname');
                        $artwork_table .= \Form::hidden('bid');
                        $artwork_table .= \Form::close();
                        
                        $artwork_table .= \Form::open(array('action' => \Uri::create('order/cart/cart_update'), 'method' => 'post', 'class' => 'edit_artwork_init_upload'));
                        $artwork_table .= \Form::hidden('itemid', '');
                        $artwork_table .= \Form::hidden('URLs', '');
                        $artwork_table .= \Form::hidden('token',  \Session::get('ysi.sToken'));
                        $artwork_table .= \Form::hidden('unique_id',  $item->get('unique_id'));
                        $artwork_table .= \Form::hidden('uid',  $item->uid());
                        $artwork_table .= \Form::hidden('type',  $i + 1);
                        $artwork_table .= \Form::hidden('product_id',  $item->get('id'));
                        $artwork_table .= '<a href="#" class="ec_submit">Upload</a>';
                        $artwork_table .= \Form::close();
                        
                        $artwork_table .= '</td>';
                        
                        $artwork_table .= '<td>';
                        
                        $artwork_table .= \Form::open(array('action' => \Uri::create('order/cart/cart_update'), 'method' => 'post', 'class' => 'edit_artwork_upload'));
                        $artwork_table .= \Form::select("artwork_quantity_new", 1, \Helper::numbers_array(1, $item->get('quantity')), array('class' => 'select_init w_60'));
                        $artwork_table .= \Form::close();
                        
                        $artwork_table .= '</td>';
                        
                        $artwork_table .= '<td>';
                        $artwork_table .= '</td>';
                        $artwork_table .= '</tr>';
                    }
                }
            }
        }
        echo $artwork_table;
    }
    
    public function action_delete_artwork($file_id = null)
    {
        if(\Input::is_ajax())
        {
            $ysi = \Yousendit\Base::forge();
            $out = $ysi->delete_artwork($file_id, true);
            
            if(isset($out['errormessage'])) \Messages::error($out['errormessage']);
            else \Messages::success('Product artwork successfully deleted.');
            
            echo \Messages::display();
        }
    }
    
    function action_init_upload()
    {
        $ysi = \Yousendit\Base::forge();
        $ysi->init_upload();
        
        exit;
    }
    
    function action_commit_upload()
    {
        $ysi = \Yousendit\Base::forge();
        $ysi->commit_upload(\Session::get('ysi.folderId'));
        
        exit;
    }

    public function action_artwork_uploaded()
    {
        $artwork_table = '';
        
        if(\Input::post())
        {
            $item = \Cart::item(\Input::post('cart_uid'));
            $artwork =  \Order\Model_Artwork::find_one_by_file_id(\Input::post('file_id'));
            if($item && $artwork)
            {
                $artwork_table .= '<tr>';
                $artwork_table .= '<td>';
                $artwork_table .= $artwork->type;
                $artwork_table .= '</td>';
                $artwork_table .= '<td>';
                $artwork_table .= $artwork->name;
                $artwork_table .= '</td>';
                $artwork_table .= '<td>';

                $artwork_table .= \Form::open(array('action' => \Uri::create('order/cart/cart_update'), 'method' => 'post', 'class' => 'edit_cart_form'));

                $artwork_table .= \Form::select("artwork_quantity[{$artwork->id}]", $artwork->quantity, \Helper::numbers_array(1,$item->get('quantity')), array('class' => 'select_init w_60'));

                $artwork_table .= \Form::close();

                $artwork_table .= '</td>';
                $artwork_table .= '<td>';
                $artwork_table .= '<a href="' . \Uri::create('order/cart/delete_artwork/' . $artwork->file_id) . '" class="remove_item"></a>';
                $artwork_table .= '</td>';
                $artwork_table .= '</tr>';
                
            }
        }
        
        echo $artwork_table;
        
    }
	
	
}
