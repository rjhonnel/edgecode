<?php
/**
 * CMS
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Order
 * @extends \Controller_Base_Public
 */

namespace Order;

class Controller_Checkout extends \Controller_Base_Public
{
	/**
	 * Base view directory for this module
	 * You can change it to whatever you want and views will be loaded from that directory
	 */
	public $view_dir = 'views/order/checkout/';
    
    public function before()
	{
		parent::before();
		
		\Config::load('order::cart', 'details');
        \Config::load('user::user', 'user');
        \Config::load('auto_response_emails', true);
		
		// Check if module is disabled and forbid access to it
		if(\Config::get('details.enabled') == FALSE)
		{
			throw new \HttpNotFoundException;
		}
	}
	
    /**
	 * Default method
     * Just redirects to home page
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_index()
	{
        // Redirect to default method
        \Response::redirect(\Uri::create('order/checkout/address'));
    }
    
    public function action_address()
    {
        if($this->check_logged_type() == 'guest') \Sentry::logout();
        
        if($this->check_logged()) \Response::redirect(\Uri::create('order/checkout/cost'));
        
        if(\Input::post('details'))
        {
            // check for a valid CSRF token
            if (!\Security::check_token())
            {
                \Messages::error('CSRF attack or expired CSRF token.');
                \Response::redirect(\Input::referrer(\Uri::create('/')));
            } 
            
            $insert = \Input::post();
            $has_create_account = isset($insert['create_account'])?$insert['create_account']:0;
            if($has_create_account == 0)
            {
                $val = \User\Controller_Validate::forge('guest', false, (\Input::post('same_address', 1) ? false : 'shipping'));
            }
            else
            {
                $val = \User\Controller_Validate::forge('create', false, (\Input::post('same_address', 1) ? false : 'shipping'));
            }

            if($val->run())
            {
                // Get POST values
                array_walk($insert, create_function('&$val', '$val = trim($val);'));

                try
                {
                    // Mark user as guest if its that case
                    if($has_create_account == 0)
                    {
                        $insert['guest'] = 1;
                        $guest = 1;
                        $username = uniqid(time(), true);
                    }
                    else
                    {
                        $username 	= $insert['email'];
                        $insert['guest'] = 0;
                    }
                     
                    $email = $insert['email'];
                    $password 	= $insert['password'] ? $insert['password'] : \Str::random('alnum', 10);
                    $user_group	= $insert['guest'] ? 3 : 4;
                    unset($insert['details'], $insert['signup'], $insert['user_group'], $insert['username'], 
                            $insert['email'], $insert['password'], $insert['confirm_password'], 
                            $insert['confirm_email'], $insert['terms'], $insert['create_account'], 
                            $insert['same_address'], $insert['fuel_csrf_token']);
                    
                    // create the user - no activation required
                    $vars = array(
                        'username' 	=> $username,
                        'email' 	=> $email,
                        'password' 	=> $password,
                        'metadata' 	=> $insert,
                    );

                    $user_registration = \Sentry::user()->register($vars);

                    $user = \Sentry::user((int)$user_registration['id']);
                    
                    // Update abandon cart order user_id if user login and cart has products and order id session
                    $order_id = \Session::get('order.id', false);
                    if(is_numeric($order_id))
                    {
                        $items = \Cart::items();
                        if($items)
                        {
                            if($get_order = \Order\Model_Order::find_one_by_id($order_id))
                            {
                                $get_order->set(array('user_id' => $user->get('id')));
                                $get_order->save();
                            }
                        }
                    }

                    // Add user to 'customer' group (id = 3)
                    if($user_registration and $user->add_to_group($user_group))
                    {
                        // Activate account
                        $activation = explode('/', $user_registration['hash']);
                        
                        \Sentry::activate_user($activation[0], $activation[1]);
                        // Login user and redirect to next page
                        //\Sentry::login($email, $password, true);
                        \Sentry::login($username, $password, true);
                        \Response::redirect(\Uri::front_create('order/checkout/cost'));
                    }
                }
                catch(\Sentry\SentryUserException $e)
                {
                    // show validation errors
                    //\Messages::error('<h4>There was an error while trying to create user</h4>');
                    $errors = $e->getMessage();
                    \Messages::error($errors);
                }
            }
            else
            {
                if($val->error() != array())
                {
                    // show validation errors
                    //\Messages::error('<h4>There was an error while trying to create user</h4>');
                    foreach($val->error() as $e)
                    {
                        \Messages::error($e->get_message());
                    }
                }
            }
        }
        
        
        \View::set_global('title', 'One Step Checkout');
        \Theme::instance()->set_partial('content', $this->view_dir . 'address');
    }
    
    public function action_edit_address($type = 'billing')
    {
        if(!\Input::is_ajax())
        {
            throw new \HttpNotFoundException;
        }
        
        if(!$this->check_logged())
        {
            \Messages::error('You must be logged in if you want to edit your address.');
            echo \Messages::display();
            exit;
        }
        
        $this->update_address($type);
        
        $user = \Sentry::user();

        switch ($type)
        {
            case 'shipping':
                $subtitle = 'Edit Shipping Address';
                $view = '_shipping_address';
                $type = 'shipping';
                break;
            default:
                $subtitle = 'Edit Billing Address';
                $view = '_billing_address';
                $type = 'billing';
            
        }
        
        $metadata = $user['metadata'];
        $content = \Theme::instance()->view('views/order/checkout/' . $view, array('edit' => true, 'user' => $user, 'metadata' => $metadata), false)->render();
        echo \Theme::instance()->view('views/order/checkout/address_edit', array('content' => $content, 'subtitle' => $subtitle, 'type' => $type), false);
        exit;
    }
    
    protected  function update_address($type = 'billing')
    {
        
        $user = \Sentry::user();
        
        if(\Input::post())
        { 
            $insert = \Input::post();
            if($this->check_logged_type() == 'guest')
            {
                $val = $this->validate($type, $user->id, ($type == 'billing' ? false : 'shipping'));
                $insert['username'] = uniqid(time(), true);
            }
            else
            {
                $val = $this->validate($type, $user->id, ($type == 'billing' ? false : 'shipping'));
                $insert['username'] =  $insert['email'];
            }
            // $val = \User\Controller_Validate::forge($type, $user->id, ($type == 'billing' ? false : 'shipping'));
             
            if($val->run())
            { 
                // Get POST values
                
                array_walk($insert, create_function('&$val', '$val = trim($val);'));

                try
                {
                   
                    $vars = array();
                    if($type == 'billing')
                    {
                        $vars['username'] 	= $insert['username'];
                        $vars['email'] 		= $insert['email'];
                    }
                   
                    //$password 	= $insert['password'] ? $insert['password'] : \Str::random('alnum', 10);
                    unset($insert['details'], $insert['signup'], $insert['user_group'], $insert['username'], $insert['email'], $insert['password'], $insert['confirm_password'], $insert['confirm_email'], $insert['terms'], $insert['create_account'], $insert['same_address']);

                    $vars += array(
                        //'password' 	=> $password,
                        'metadata' 	=> $insert,
                    );
                    
                    if($user->update($vars))
                    {
                        \Messages::success('Details successfully updated!');
                    }
                    else
                    {
                        // This should never happen, but just in case
                        \Messages::error('There was an error updating. Please try again.');
                    }

                }
                catch(\Sentry\SentryUserException $e)
                {
                    // show validation errors
                    //\Messages::error('<h4>There was an error while trying to create user</h4>');
                    $errors = $e->getMessage();
                    \Messages::error($errors);
                }
                
                
            }
            else
            {
                if($val->error() != array())
                {
                    // show validation errors
                    //\Messages::error('<h4>There was an error while trying to create user</h4>');
                    foreach($val->error() as $e)
                    {
                        \Messages::error($e->get_message());
                    }
                }
            }
            
            //echo  \Messages::display(); exit;
        }
    }
    
    public function action_cost()
    {
        $settings = \Config::load('autoresponder.db');

        if(!$this->check_logged())
        {
            \Messages::error('You must be logged in if you want to continue with your order.');
            \Response::redirect(\Uri::create('order/checkout/address'));
        }
        
        $items = \Cart::items();
        
        if(empty($items))
        {
            \Messages::error('Your cart is empty. Please add some products.');
            if(!$this->check_logged())
                \Response::redirect(\Uri::create('order/checkout/address'));
        }

        if(\Input::post() && $items)
        {
            // Save order
            $user = false;
            $order = false;

            if (\Sentry::check())
            {
                $user = \Sentry::user();
            }

            $order = $this->save_order();
            $order = \Order\Model_Order::find_one_by_id($order->id);

            if(\Input::post('payment_type'))
            {
                $payment = \Payment\Model_Payment::forge(); 
                $payment->set(array(
                    'order_id' => $order->id,
                    'total_price' => \Input::post('grand_total'),
                    'method' => \Input::post('payment_type'),
                    'status' => 'pending',
                    'status_detail' => 'Pending',
                ));
                $payment->save();

                try
                {
                    // Save and proccess payment
                    $paymentProccessStrategy = new \PaymentProccess\PaymentProccessTypeStrategy(\Input::post(), $order);

                    $form = $paymentProccessStrategy->proccessPayment();

                    if(isset($form)) $redirect = 'paypal_form';
                    else
                    {
                        // Determine redirect page by looking what proccess type we used
                        if($paymentProccessStrategy->fetchTypeOfProccess() == 'PaymentProccess\PaymentProccessTypeBank')
                            $redirect = 'success_bank';
                        else $redirect = 'success';
                    }

                }
                // If you want to catch exceptions for all payment proccesses make some \Exception\SomePayment
                // that will be applicable for all payment proccesses and throw them from everywhere :)
                catch ( \PaymentProccess\Exception\SecurePay\SecurePayServerDownException $e )
                {
                    \Messages::error($e->getMessage());
                    $redirect = 'fail';
                }
                catch ( \PaymentProccess\Exception\SecurePay\SecurePayTransactionFailedException $e )
                {
                     \Messages::error($e->getMessage());
                     $redirect = 'fail';
                }
                catch ( \PaymentProccess\Exception\SecurePay\SecurePayCustomerDataInvalidException $e )
                {
                     \Messages::error($e->getMessage());
                     $redirect = 'fail';
                }
                catch ( \Exception $e )
                {
                     \Messages::error($e->getMessage());
                     $redirect = 'fail';
                }

                try
                {
                    if($redirect != 'fail')
                    {
                        $order->set(array('accepted' => 1));
                        $order->save();
                        $this->autoresponder($user, $order);
                    }
 
                }
                catch (\Database_Exception $e)
                {
                    // show validation errors
                    \Messages::error('There was an error while trying to save order payment.');

                    // Uncomment lines below to show database errors
                    $errors = $e->getMessage();
                    \Messages::error($errors);

                    \Response::redirect(\Uri::create('order/checkout/cost'));
                }

                if($redirect == 'fail')
                    \Response::redirect(\Uri::create('order/checkout/cost'));
            }
            else
            {
                $order->set(array('accepted' => 1));
                $order->save();
            }

            // clear products in abandon cart when successful checkout
            if(is_numeric($order->id))
            {
                $item_exists = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'order_id' => $order->id ) ) );
                if($item_exists)
                {
                    foreach ($item_exists as $product)
                    {
                        $product->delete();
                    }
                }
            }

            $this->autoresponder($user, $order);
        }
        
        $order = \Order\Model_Order::forge();
        $shipping_price = $order->shipping_price(null, null, true);
        
        \View::set_global('title', 'Checkout');
        \Theme::instance()->set_partial('content', $this->view_dir . 'cost')
            ->set('items', $items, false)
            ->set('credit_account', \Order\Model_Order::credit_account(null, \Cart::getTotal('price')), false)
            ->set('user', \Sentry::user(), false)
            ->set('shipping_price', $shipping_price)
            ->set('settings', $settings);
    }

    public function action_shipment_price()
    {
        $settings = \Config::load('autoresponder.db');

        if(\Input::post('method') == 'pickup'){
            $method = false;
            $method_label = 'pickup';
        } else {
            $method = true;
            $method_label = 'delivery';
        }
        $f_discount = \Input::post('discount');
        $check_discount_shipping = false;
        $return['check_discount_shipping'] = false;
        if (\Input::post('action') == 'remove')
        {
            $discount = \Discountcode\Model_Discountcode::find_one_by_code(\Input::post('discount_code'));
            if ($discount && $discount->status == 'active')
            {
                // check for expiry date
                if ($discount->active_from != '0000-00-00' && $discount->active_to != '0000-00-00')
                {
                    $current_date = strtotime(date('Y-m-d'));
                    $from = strtotime($discount->active_from);
                    $to = strtotime($discount->active_to);
                    if ($current_date >= $from && $current_date <= $to)
                    {
                        $valid = true;
                    }
                }
                else
                {
                    $valid = true;
                }

                if ($valid)
                {
                    $valid = false;
                    // check for single/multiple use
                    if ($discount->use_type == 'multi use')
                    {
                        $valid = true;
                    }
                    else
                    {
                        // check if discount code was already used.
                        $order_exists = \Order\Model_Order::find_one_by_id_discount($discount->id);
                        if (is_null($order_exists))
                        {
                            $valid = true;
                        }
                    }   
                    if($discount->type == 'free shipping')
                        $valid = true;
                }
                if ($valid)
                {
                    if(is_numeric(\Session::get('order.id'))) 
                    {
                        $order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
                    }
                    if(!$order) $order = \Order\Model_Order::forge();

                    $method = (\Input::post('method') == 'pickup' ? false : true);
                    $shipping_price = $order->shipping_price(null, null, true, $method);

                    $sub_total = \Input::post('total_price') + $shipping_price + \Input::post('total_delivery_charge');

                    //check order value min and max
                    if ($discount->min_order_value != 0 || $discount->max_order_value != 0)
                    {
                        if($discount->max_order_value != 0)
                        {
                            if($sub_total >= $discount->min_order_value && $sub_total <= $discount->max_order_value)
                                $valid = true;
                            else
                                $valid = false;
                        }
                        else
                        {
                            if($sub_total >= $discount->min_order_value)
                                $valid = true;
                            else
                                $valid = false;
                        }
                    }
                    else
                    {
                        $valid = true;
                    }
                    //check order value min and max
                    
                    if ($valid)
                    {
                        // compute value of discount
                        switch($discount->type)
                        {
                            case 'free shipping':   
                                $f_discount = \Input::post('method') == 'pickup' ? 0 : $shipping_price; 
                                
                                if($f_discount == 0)
                                {
                                    $check_discount_shipping = true;
                                }
                                $return['check_discount_shipping'] = true;
                                $return['discount'] = $f_discount;
                            break;
                        }
                        $order->set(array(
                            'finished'          => 0,
                            'id_discount'       => $discount->id,
                            'discount_amount'   => $f_discount,
                            'shipping_price'    => $shipping_price,
                            'total_price'       => \Input::post('total_price'),
                            'total_delivery_charge'       => \Input::post('method') == 'pickup' ? 0 :\Input::post('total_delivery_charge'),
                        ));
                        $order->save();
                    }
                }   
            }
        }

        if(is_numeric(\Session::get('order.id'))) 
        {
            $order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
        }
        if(!$order) $order = \Order\Model_Order::forge(); 

        $return['shipping_price'] = $order->shipping_price(null, null, true, $method); 

        $items = \Cart::items();
        $cart_total = 0;
        $total_delivery_charge = 0;
        foreach ($items as $item){
            $total_price = $item->totalDiscountedPrice(true);
            $cart_total += $total_price;

            $product_data = null;
            if($product = \Product\Model_Product::find_one_by_id($item->get('id')))
            {
                $product_data = \Product\Model_Product::product_data($product, $item->get('attributes'));
            };
            if(isset($product_data["delivery_charge"]))
                $total_delivery_charge += $product_data["delivery_charge"];
        }
        $total_delivery_charge = \Input::post('method') == 'pickup' ? 0 : $total_delivery_charge;

        $total = \Helper::total_price(array($cart_total, $return['shipping_price']));

        $hold_gst = (isset($settings['gst']) ? 1 : 0);
        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

        $return['delivery_charge'] = number_format($total_delivery_charge,2);
        $return['grand_total'] = number_format($cart_total + $return['shipping_price'] - ( $check_discount_shipping ? 0 : $f_discount ) + $total_delivery_charge, 2);

        $return['gst_price'] = 0;
        if($return['check_discount_shipping']){
            $sub_total = $cart_total + $return['shipping_price'] - $return['discount']; 
        }else{
            $sub_total = $cart_total + $return['shipping_price'];
        }
        
        if($hold_gst && $hold_gst_percentage > 0)
        {
            $return['gst_price'] = \Helper::calculateGST($sub_total, $settings['gst_percentage']);
        }

        
        $order->set(array(
            'finished'          => 0,
            'shipping_method'   => $method_label,
            'shipping_price'    => $return['shipping_price'],
            'total_price'       => $return['grand_total'],
            'gst_price'         => $return['gst_price'],
            'delivery_charge'       => $return['delivery_charge'],
        ));
        $order->save();
        \Session::set('order.id', $order->id);

        echo json_encode($return);
    }
    
    public function action_payment()
    {

        $items = \Cart::items();
        
        if(empty($items))
        {
            \Messages::error('Your cart is empty. Please add some products.');
            \Response::redirect(\Uri::create('order/checkout/cost'));
        }
        
        if(!$this->check_logged())
        {
            \Messages::error('You must be logged in if you want to continue with your order.');
            \Response::redirect(\Uri::create('order/checkout/address'));
        }
        
        // Save order
        $user = false;
        $order = false;

        if (\Sentry::check())
        {
            $user = \Sentry::user();
        }
        
        if(\Input::post())
        {   
        	$b_valid = true;
        	if (\Input::post('payment_type') == 'creditCard')
        	{
	        	$val = \Validation::forge($factory);
				$val->add('name', 'Card Holder Name')->add_rule('required');
				$val->add('ccnumber', 'Credit Card Number')->add_rule('required');
				$val->add('cvv', 'Card Verification Number')->add_rule('required');
				$val->run();
				if($val->error())
				{
					$b_valid = false;
					// show validation errors
					\Messages::error('<strong>There was an error while trying to submit your Credit Card details</strong>');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
        	}
        	
            if($b_valid && $order = $this->save_order())
            {
                $order = \Order\Model_Order::find_one_by_id($order->id);

                // Save and proccess payment
                $paymentProccessStrategy = new \PaymentProccess\PaymentProccessTypeStrategy(\Input::post(), $order);

                //$log = new \Autorespondertrigger\SaverLog();
                //$log->saveLog($order->id);

                $form = false;
                $redirect = 'fail';

                try
                {

                    $form = $paymentProccessStrategy->proccessPayment();

                    if(isset($form)) $redirect = 'paypal_form';
                    else
                    {
                        // Determine redirect page by looking what proccess type we used
                        if($paymentProccessStrategy->fetchTypeOfProccess() == 'PaymentProccess\PaymentProccessTypeBank')
                            $redirect = 'success_bank';
                        else $redirect = 'success';
                    }

                }
                // If you want to catch exceptions for all payment proccesses make some \Exception\SomePayment
                // that will be applicable for all payment proccesses and throw them from everywhere :)
                catch ( \PaymentProccess\Exception\SecurePay\SecurePayServerDownException $e )
                {
                    \Messages::error($e->getMessage());
                    $redirect = 'fail';
                }
                catch ( \PaymentProccess\Exception\SecurePay\SecurePayTransactionFailedException $e )
                {
                     \Messages::error($e->getMessage());
                     $redirect = 'fail';
                }
                catch ( \PaymentProccess\Exception\SecurePay\SecurePayCustomerDataInvalidException $e )
                {
                     \Messages::error($e->getMessage());
                     $redirect = 'fail';
                }

                try
                {
                    if($redirect != 'fail')
                    {
                        $order->set(array('accepted' => 1));
                        $order->save();
                        $this->autoresponder($user, $order);


                    }
 
                }
                catch (\Database_Exception $e)
                {
                    // show validation errors
                    \Messages::error('There was an error while trying to save order.');

                    // Uncomment lines below to show database errors
                    $errors = $e->getMessage();
                    \Messages::error($errors);

                    \Response::redirect(\Uri::create('order/checkout/cost'));
                }
            }
        }
        
       \Theme::instance()->set_partial('content', $this->view_dir . 'payment')
                ->set('credit_account', \Order\Model_Order::credit_account(null, \Cart::getTotal('price')), false)
                ->set('user', $user, false);
    }
    
    public function action_complete($order_id = null)
    {
        if(!$this->check_logged())
        {
            \Messages::error('You must be logged in if you want to continue with your order.');
            \Response::redirect(\Uri::create('order/checkout/address'));
        }
        
        $user = \Sentry::user();
        $order = \Order\Model_Order::find(array('where' => array(
            'id' => $order_id,
            'user_id' => $user->get('id'),
        )));
        
        \View::set_global('title', 'Order Complete');
        \Theme::instance()->set_partial('content', $this->view_dir . 'complete')
                ->set('user', $user, false)
                ->set('order', $order, false);
    }
    
    public function action_remove_cart_item($uid = false)
    {
        $item = \Cart::item($uid);
        if($item) 
        {
            if($unique_id = $item->get('unique_id'))
            {
                // Set artwork for delete
                if($artworks = \Order\Model_Artwork::find(array('where' => array('unique_id' => $unique_id))))
                {
                    foreach ($artworks as $artwork)
                    {
                        $artwork->set(array('deleted_at' => time()));
                        $artwork->save();
                    }
                }
            }
            
            \Cart::remove($uid);
                
            \Messages::success('Product successfully deleted from cart.');
        }
        else
        {
            \Messages::error('There was an error while trying to delete product from cart.');
           
        }
        
        \Response::redirect(\Uri::create('order/checkout/cost'));
    }
    
    /**
	 * Buy credits checkout
	 *
	 * @access  public
	 * @return  void
	 */
	public function action_credits()
	{
        \View::set_global('title', 'Purchase Credits');
        
        if(\Input::post('buy'))
        {
            $products   = array();
            $total      = array(
                'price' => 0,
                'save' => 0,
            );
            
            $bundles = \Input::post('bundles');
            
            $prices = \Config::get('job.bundles.multiple', array());
            
            // Rename array keys
            foreach($bundles as $key => $value)
            {
                $new_key = str_replace('::', '.', $key);
                
                $bundles[$new_key] = $value;
                \Arr::delete($bundles, $key);
                
                if(\Arr::get($prices, $new_key, false))
                {
                    list($quantity, $option) = explode('.', $new_key);
                    
                    $product = \Arr::get($prices, $new_key);
                    $product['title']   = $quantity . ' Pack Job Bundle / ' . ucfirst($option);
                    $products[$key] = $product;
                    
                    $total['price'] += $product['price'];
                    $total['save'] += $product['save'];
                }
            }
            

            // Save result in session
            \Session::set('checkout', array(
                'products' => $products,
                'total' => $total,
            ));
        }
        elseif(!\Session::get('checkout') || !\Session::get('checkout.products', array()))
        {
            // Session must exist to access this page. Return to purchase credits page
            $purchase_page = \Page\Model_Page::get_locked_pages('purchase_page', true);
            
            \Response::redirect(\Uri::front_create('page/' . $purchase_page->seo->slug));
        }
        
        // Set session key just to meke sure user can not retur to payment page from any other page but this
        \Session::set('payment_key', \Str::random('unique'));
        
        $this->page_theme = \Theme::instance()->set_partial('content', $this->view_dir . 'credits');
    }
    
    /**
     * Remove order product from session
     * and return form html
     * 
     * @access  public
     */
    public function action_remove()
    {
        if(!\Input::is_ajax())
        {
            throw new \HttpNotFoundException;
        }
        
        $return['success'] = false;
        
        if(\Input::post('remove'))
        {
            $key = str_replace('.', '::', \Input::post('key', ''));
            
            \Session::set('checkout.total.price', \Session::get('checkout.total.price') - \Session::get('checkout.products.' . $key . '.price'));
            \Session::set('checkout.total.save', \Session::get('checkout.total.save') - \Session::get('checkout.products.' . $key . '.save'));
            
            \Session::delete('checkout.products.' . $key);
            
            // If cart is left empty we redirect user to previous page
            if(!\Session::get('checkout.products'))
            {
                $purchase_page = \Page\Model_Page::get_locked_pages('purchase_page', true);
                
                $return['redirect'] = \Uri::front_create('page/' . $purchase_page->seo->slug);
            }
            
            $return['success'] = true;
            $return['price'] = number_format(\Session::get('checkout.total.price'), 2);
            $return['save'] = number_format(\Session::get('checkout.total.save'), 2);
        }
        
        echo json_encode($return);
    }
    
    
    /**
	 * Checkout step - Payment
	 * 
	 * @access  public
	 */
	public function action_paypal($id = false)
	{
        if(!$id)
            $id = \Session::get('order.id');

        if((int)$id)
        {
            $processPayment = new \PaymentProccess\PaymentProccessTypePaypal(array('payment_type' => 'paypal'), \Order\Model_Order::find_one_by_id($id));
            $failed = $processPayment->notify();
            if ($failed)
            {
            	\Messages::error('There was an error in processing your payment.');
            	\Response::redirect(\Uri::create('order/checkout/cost'));
            }
        }
        else
        {
            \Messages::error('Order not found');
            \Response::redirect(\Uri::create('order/checkout/cost'));
        }
	}
	
	public function action_finalise_order($id = false)
	{
        if(!$id)
            $id = \Session::get('order.id');

        if((int)$id)
        {
            $order = \Order\Model_Order::find_one_by_id($id);
            if ($order)
            {
                try
                {
                    $order->set(array('accepted' => 1));
                    $order->save();

                    if($user = \Sentry::user((int)$order->user_id))
                        $this->autoresponder($user, $order);
                }
                catch (\Database_Exception $e)
                {
                    // show validation errors
                    \Messages::error('There was an error while trying to save order.');

                    // Uncomment lines below to show database errors
                    $errors = $e->getMessage();
                    \Messages::error($errors);

                    \Response::redirect(\Uri::create('order/checkout/cost'));
                }   
            }
        }
        \Response::redirect(\Uri::create('order/checkout/payment'));
	}
    
    public function action_cancel_order($id = false)
    {
        if(!$id)
            $id = \Session::get('order.id');

        if((int)$id)
        {
            $order = \Order\Model_Order::find_one_by_id($id);
            if ($order)
            {
                \Messages::error('Payment cancelled.');
                \Response::redirect(\Uri::create('order/checkout/cost'));
            }
            else
            {
                \Messages::error('Order not found');
                \Response::redirect(\Uri::create('order/checkout/cost'));
            }
        }
        else
        {
            \Messages::error('Order not found');
            \Response::redirect(\Uri::create('order/checkout/cost'));
        }
    }

    public function action_return_securepay($id = false)
    {
        $settings = \Config::load('securePay.db');
        $hold_code = (isset($settings['code']) ? $settings['code'] : 'securepay');

        $this->payment_return($id, $hold_code);
    }

    public function payment_return($id, $code)
    {
        if(!$id)
            $id = \Session::get('order.id');

        if((int)$id)
        {
            $order = \Order\Model_Order::find_one_by_id($id);
            if ($order)
            {
                if(isset(\Input::get()['restext']))
                {
                    if(\Input::get()['restext'] == 'Approved')
                    {
                        try
                        {
                            $paymentSaver = new \PaymentProccess\PaymentSaver();
                            $paymentSaver->savePaymentDetails($code, $order, 'Completed', 'Transaction completed', \Input::get()['restext']);
                            
                            $order->set(array('accepted' => 1));
                            $order->save();
                            
                            if($user = \Sentry::user((int)$order->user_id))
                                $this->autoresponder($user, $order);
                        }
                        catch (\Database_Exception $e)
                        {
                            // show validation errors
                            \Messages::error('There was an error while trying to save order.');

                            // Uncomment lines below to show database errors
                            $errors = $e->getMessage();
                            \Messages::error($errors);

                            \Response::redirect(\Uri::create('order/checkout/cost'));
                        }
                    }
                    else
                    {
                        $paymentSaver = new \PaymentProccess\PaymentSaver();
                        $paymentSaver->savePaymentDetails($code, $order, 'Failed', 'Transaction failed', \Input::get()['restext']);

                        \Messages::error(\Input::get()['restext']);
                        \Response::redirect(\Uri::create('order/checkout/cost'));
                    }
                }
            }
            else
            {
                \Messages::error('Order not found');
                \Response::redirect(\Uri::create('admin/order/list'));
            }
        }
        else
        {
            \Messages::error('Order not found');
            \Response::redirect(\Uri::create('order/checkout/cost'));
        }
    }
    
    public function action_login()
    {
        if (!(\Sentry::check() && !\Sentry::user()->is_admin()))
		{
			\View::set_global('title', 'Login');
            
			if(\Input::post('login'))
			{
				$val = \User\Controller_Validate::forge('login');
				
				if($val->run())
				{
					try
				    {
				   	 	if(\Sentry::user_exists(\Input::param('identity')) && !\Sentry::user(\Input::param('identity'))->is_admin())
						{
							// check the credentials.
							$valid_login = \Sentry::login(\Input::param('identity'), \Input::param('password'), true);
							
							if($valid_login)
							{
								\Messages::success('You have logged in successfully');
//								\Response::redirect(\Input::referrer(\Uri::front_create('user/account/dashboard')));
								\Response::redirect(\Uri::front_create('order/checkout/cost'));
							}
							else
							{
								\Messages::error('Email and/or password is incorrect');
							}
						}
						else
						{
							\Messages::error('Email and/or password is incorrect');
						}
					    
				    }
					catch (\Sentry\SentryAuthException $e)
				    {
					    // show validation errors
						//\Messages::error('<h4>There was an error while trying to login</h4>');
				    	$errors = $e->getMessage();
				    	\Messages::error($errors);
				    }
				    catch (\Sentry\SentryUserException $e)
				    {
					    // show validation errors
						//\Messages::error('<h4>There was an error while trying to login</h4>');
				    	$errors = $e->getMessage();
				    	\Messages::error($errors);
				    }
				}
				else
				{
					if($val->error() != array())
					{
						// show validation errors
						foreach($val->error() as $e)
						{
							\Messages::error($e->get_message());
						}
					}
				}
			}
		}
        
        //Keep existing messages
        \Messages::instance()->shutdown();
        \Response::redirect(\Uri::front_create('order/checkout/address'));
    }
    
    
    public function create_user()
	{
        // Get groups
        $groups = \Sentry::group()->all('front');
        
		if(\Input::post())
		{
            // Get POST values
            $insert = \Input::post();
            
            $register_type = 'register';
            if(\Input::post('register'))  $register_type = $insert['register'];
                
            $ship_to = 'billing';
            if($insert['ship'] == 'other') $ship_to = 'shipping';
                
			$val = \User\Controller_Validate::forge($register_type == 'register' ? 'create' : 'guest', false, $ship_to == 'shipping' ? 'shipping' : false);
			
			if($val->run())
			{
                
				array_walk($insert, create_function('&$val', '$val = trim($val);'));
				
			    try
			    {
                    // Generate random username
			    	$email 		= $insert['email'];
                    $user_group	= 3; 
                    
                    if($register_type == 'guest')
                    {
                        $username 	= 'user' . \Str::random('numeric', 16);
                        $insert['guest'] = 1;
                        $random_password = 'random' . \Str::random(unique);
                        $password 	= $random_password;
                    }
                    else
                    {
                        $username 	= $email;
                        $insert['guest'] = 0;
                        $password 	= $insert['password'];
                    }
			    	
			    	unset($insert['email'], $insert['password'], $insert['confirm_password'], $insert['user_group'], $insert['details'], $insert['save'], $insert['update']);
			    	
                    $only_billing = array('email');
                    
                    $billing_data = \Arr::filter_prefixed($insert, "billing_");
                        
                    // Set shipping data to be same as billing by default
                    if($ship_to_billing)
                    {
                        foreach($billing_data as $key => $value)
                        {
                            if(!in_array($key, $only_billing))
                            {
                                $insert['shipping_'.$key] = $value;
                            }
                        }
                    }
                    
                    $metadata = \Arr::remove_prefixed($insert, "billing_") + $billing_data;
                    $table = \DB::table_prefix('users_metadata');
                    $columns = \DB::list_columns($table);
                    $insert = array_intersect_key ($metadata, $columns);
                    
				    // create the user - no activation required
				    $vars = array(
					    'username' 	=> $username,
					    'email' 	=> $email,
					    'password' 	=> $password,
					    'metadata' 	=> $insert,
				    );
                    
				    $user_id 	= \Sentry::user()->create($vars);
				    $user 		= \Sentry::user($user_id);
				    
				    // Add user to 'customer' group (id = 3)
				    if($user_id and $user->add_to_group($user_group))
				    {
                        if($register_type == 'account') \Messages::success('User successfully created.');
                        if($register_type == 'guest') \Messages::success('You register as a guest.');
                        
                        $login_column = \Config::get('sentry.login_column', 'email');

                        if (\Sentry::login($$login_column, $password, true))
                        {
                             \Response::redirect(\Uri::create('order/checkout/cost'));
                        }
                        else
                        {
                            if($register_type == 'account') \Messages::error('There was an error while trying to create account. Please try to create new account.');
                            if($register_type == 'guest') \Messages::error('There was an error. Please try to login with your account details.');
                        }
				    }
				    else
				    {
				    	// show validation errors
						\Messages::error('There was an error while trying to create account.');
				    }
			    }
			    catch (\Sentry\SentryUserException $e)
			    {
			    	// show validation errors
					\Messages::error('There was an error while trying to create user.');
			    	$errors = $e->getMessage();
			    	\Messages::error($errors);
			    }
            }
			else
			{
				if($val->error() != array())
				{
					// show validation errors
					\Messages::error('There was an error while trying to create user.');
					foreach($val->error() as $e)
					{
						\Messages::error($e->get_message());
					}
				}
			}
		}
	}
    
    public function action_check_login()
    {
        $user_not_exists = true;
        
        if (\Sentry::user_exists(\Input::get('fieldValue')))
        {
             $user_not_exists = false;
        }
        
        $out = array(
            \Input::get('fieldId'),
            $user_not_exists
        );
        
        echo json_encode($out);
        
    }
    
    public function action_delete_artwork($file_id = null)
    {
        $ysi = \Yousendit\Base::forge();
        $ysi->delete_artwork($file_id);
        \Response::redirect(\Uri::create('order/checkout/cost'));
    }
    
    public function action_order_type()
    {
        if(!$this->check_logged())
        {
            \Messages::error('You must be logged in if you want to continue with your order.');
            \Response::redirect(\Uri::create('order/checkout/address'));
        }
        
        $items = \Cart::items();
        
        if(empty($items))
        {
            \Messages::error('Your cart is empty. Please add some products.');
            \Response::redirect(\Uri::create('order/checkout/cost'));
        }
        
        $credit_account = \Order\Model_Order::credit_account(null, \Cart::getTotal('price'));
        
        if(!$credit_account['credit']) \Response::redirect('order/checkout/payment');
        
        \Theme::instance()->set_partial('content', $this->view_dir . 'order_type')
                ->set('credit_account', $credit_account, false)
                ->set('user', \Sentry::user(), false);
    }
    
    public function action_credit()
    {
        if(!$this->check_logged())
        {
            \Messages::error('You must be logged in if you want to continue with your order.');
            \Response::redirect(\Uri::create('order/checkout/address'));
        }
        
        if(!\Input::post())
        {
            throw new \HttpNotFoundException;
        }
        
        if(\Input::post('order_type') == 'payment') \Response::redirect(\Uri::create('order/checkout/payment'));
            
        $credit_account = \Order\Model_Order::credit_account(null, \Cart::getTotal('price'));
        
        if(\Input::post('order_type') != 'credit')
        {
            \Messages::error('There was an error while trying to save your order.');
            \Response::redirect(\Input::referrer(\Uri::create('order/checkout/cost')));
        }
        
        if(!$credit_account['credit'] || $credit_account['over_limit'])
        {
            \Messages::error("You don't have permission for this action.");
            \Response::redirect(\Input::referrer(\Uri::create('order/checkout/cost')));
        }
        $items = \Cart::items();
        
        $user = \Sentry::user();
       
        if( $order = $this->save_order() )
        {
        	$payment = \Payment\Model_Payment::find_one_by_order_id($order->id);
        	if (!isset($payment))
        	{
        		$payment = \Payment\Model_Payment::forge();	
        	}
        	$total_price = $order->total_price ? 
        		(($order->total_price + $order->shipping_price) - $order->discount_amount) : 
        		(($order['total_price'] + $order['shipping_price']) - $order['discount_amount']);
        	$payment->set(array(
            	'order_id' => $order->id,
            	'total_price' => $total_price,
            	'method' => 'credit',
            	'status' => 'ordered',
            	'status_detail' => 'Credit Account'
        	));
            $payment->save();
             $this->autoresponder($user, $order);
             \Response::redirect(\Input::referrer(\Uri::create('order/checkout/complete/' . $order->id)));
        }
        
        \Messages::error('There was an error while trying to save your order.');
        \Response::redirect(\Input::referrer(\Uri::create('order/checkout/cost')));
       
    }
    
    protected function save_order()
    {
        if(!$this->check_logged())
        {
            \Messages::error('You must be logged in if you want to continue with your order.');
            \Response::redirect(\Uri::create('order/checkout/address'));
        }
        
        // Save order
        $user = false;
        $order = false;
        $items = \Cart::items();

        if (\Sentry::check())
        {
            $user = \Sentry::user();
        }


       

        if(\Input::post() && $items && $user)
        {


             // check for a valid CSRF token
            if (!\Security::check_token())
            {
                \Messages::error('CSRF attack or expired CSRF token.');
                \Response::redirect(\Input::referrer(\Uri::create('order/checkout/cost')));
            }
        
            try
            {
                // Update or create order
                if(is_numeric(\Session::get('order.id'))) $order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
                
                if(!$order) $order = \Order\Model_Order::forge();

                $cart_total = 0;
                $total_delivery_charge = 0;
                foreach ($items as $item){
                    $product_data = null;
                    if($product = \Product\Model_Product::find_one_by_id($item->get('id')))
                    {
                        $product_data = \Product\Model_Product::product_data($product, $item->get('attributes'));
                    };

                    $total_price = $item->totalDiscountedPrice(true);
                    if(isset($product_data["price"]) && $product_data["price"] != 0){
                        $total_price = $product_data["price"]*$item->get('quantity');
                    }
                    $cart_total += $total_price;
                    $total_delivery_charge += $product_data["delivery_charge"];
                }

                $method = (\Input::post('delivery') == 'pickup' ? false : true);
                $shipping_price = $order->shipping_price(null, null, true, $method);

                $delivery_datetime = NULL;
                $hold_time_list_arr_sort = \Input::post('delivery_datetime_list');
                if(\Input::post('delivery_datetime'))
                {
                    $hold_date = explode('/', \Input::post('delivery_datetime'));
                    $rearrange = $hold_date[2].'-'.$hold_date[1].'-'.$hold_date[0];

                    // rearrange time list
                    $hold_time_list_arr = explode(';', \Input::post('delivery_datetime_list'));
                    sort($hold_time_list_arr);
                    $hold_time_list_arr_sort = implode(';', $hold_time_list_arr);

                    $delivery_datetime = date('Y-m-d H:i', strtotime($rearrange.' '.$hold_time_list_arr[0]));
                }

                $metadata = $user->get('metadata');
                $data = array(
                    'email' => $user->get('email'),
                    'user_id' => $user->get('id'),
                    'status' => 'new',
                    'total_price' => $cart_total,
                    'finished' => 1,
                    'discount_amount' => \Input::post('discount_amount'),
                    'guest' => ($metadata['guest'] ? 1 : 0),
                    'accepted' => ($metadata['master'] == 1 ? 1 : 0),
                    'credit_account' => ($metadata['credit_account'] == 1 ? 1 : 0),
                    'shipping_method'=> \Input::post('delivery'),
                    'shipping_price' => $shipping_price,
                    'gst_price' => \Input::post('gst_amount'),
                    'total_delivery_charge' => \Input::post('delivery') == "pickup" ? 0: $total_delivery_charge,
                    'delivery_datetime' => $delivery_datetime,
                    'delivery_datetime_list' => \Input::post('delivery_datetime_list')?$hold_time_list_arr_sort:NULL
                );

                // $order->discount_amount = $item_with_discount['total_discount'];//\Cart::getTotal('price');
                //\Cart::getDiscountedTotal('price');//\Cart::getTotal('price');
                
                if($billing = \Arr::filter_prefixed($metadata, 'shipping_'))
                {
                    foreach ($billing as $key => $value)
                    {
                        if($key == 'company')
                            $data[$key] = isset($metadata['business_name'])?$metadata['business_name']:'';
                        else
                            $data[$key] = $metadata[$key];
                        unset($metadata[$key]);
                    }
                } 
                foreach ($metadata as $key => $value)
                {
                   $data[$key] = $value;
                }
                $order->set($data);
                // Save order, add products to order products
                if($order->save())
                {
                    // remove to update if there is new changes in cart items
                    if($item_exists = \Order\Model_Products::find( array('where' => array( 'order_id' => $order->id ) ) ))
                    {
                        foreach ($item_exists as $ie)
                            $ie->delete();
                    }

                    foreach ($items as $item)
                    {
                        $product_data = null;
                        if($product = \Product\Model_Product::find_one_by_id($item->get('id')))
                        {
                            $product_data = \Product\Model_Product::product_data($product, $item->get('attributes'));
                        };


                        $item_exists = \Order\Model_Products::find( array('where' => array( 'product_id' => $product->id, 'order_id' => $order->id, 'attributes_id' => $item->get('attributes') ) ) );

                        if($product_data && !$item_exists )
                        {
                            $pack_list = null;
                            if($item->get('packs'))
                            {
                                $pack_list = $item->get('packs');
                                foreach($pack_list as $key => $pack)
                                {
                                    $pack_list[$key]['time'] = isset(\Input::post('product_time')[$product->id.'-'.$pack['id']])?\Input::post('product_time')[$product->id.'-'.$pack['id']]:'N/A';
                                }
                                $pack_list = json_encode($pack_list);
                            }

                            $order_products = \Order\Model_Products::forge(array(
                                'order_id'          => $order->id,
                                'title'             => $product->title,
                                'code'              => $product_data['code'],
                                'price'             => $product_data['price'],
                                'price_type'        => isset($product_data['price_type'])?$product_data['price_type']:'',
                                'quantity'          => $item->get('quantity'),
                                'product_id'        => $product->id,
                                'artwork_required'  => $product->artwork_required,
                                'artwork_free_over' => $product->artwork_free_over,
                                'subtotal'          => ($product_data['price']*$item->get('quantity')),
                                'attributes'        => json_encode(\Product\Model_Attribute::get_combination($item->get('attributes'))),
                                'attributes_id'     => $item->get('attributes'),
                                'delivery_charge'   => $product_data['delivery_charge']?:0,
                                'delivery_time'     => isset(\Input::post('product_time')[$product->id])?\Input::post('product_time')[$product->id]:'N/A',
                                'packs'             => $pack_list,
                            ));
                            //$item->singleDiscountedPrice(true);//$item->singlePrice(true);
                            //$item->totalDiscountedPrice(true);//$item->totalPrice(true);
                            if(!empty($product->categories))
                            {
                                $categories = array();
                                foreach ($product->categories as $category)
                                {
                                    $categories[] = $category->title;
                                }
                                if($categories) $order_products->product_category = implode(',', $categories);
                            }

                            // update stock quantity
                            \Product\Model_Attribute::update_stock_quantity($item->get('attributes'), $item->get('quantity'));

                            $order_products->save();


                            
                            // Find artworks
                            if($unique_id = $item->get('unique_id'))
                            {
                                if($artworks = \Order\Model_Artwork::find(array('where' => array('unique_id' => $unique_id, 'order_id' => $order->id))))
                                {
                                    $ysi = \Yousendit\Base::forge();
                                    
                                    // Artworks (update, delete)
                                    foreach ($artworks as $artwork)
                                    {
                                        // Remove deleted artwork
                                        if($artwork->deleted_at > 0) 
                                        {
                                            $ysi->delete_artwork($artwork->file_id);
                                            $artwork->delete();
                                        }
                                        else
                                        {
                                            $artwork->set(array('order_product_id' => $order_products->id));
                                            $artwork->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                if($order)
                {

                    return $order;
                }
                else
                {
                    return false;
                }
                
            }
            catch (\Database_Exception $e)
            {
                // show validation errors
                \Messages::error('There was an error while trying to save your order.');

                // Uncomment lines below to show database errors
                $errors = $e->getMessage();
                \Messages::error($errors);

                \Response::redirect(\Uri::create('order/checkout/cost'));
            }
            
            return false;
        }
    }
    
    protected function autoresponder($user, $order)
    {

        $settings = \Config::load('autoresponder.db');

        // Send autoresponder
        $autoresponder = \Autoresponder\Autoresponder::forge();

        $autoresponder->user_id =  $user->id;

        $autoresponder->view_user = 'order';
        $autoresponder->view_admin = 'order';

        $content['content'] = $order;

        $content['subject'] = 'Order confirmation from '.$settings['website'];
        
        // Add cc email
        $content['cc_email'] = array();
        // get user cc email
        if($user->get('metadata.cc_email'))
            $content['cc_email'][] = $user->get('metadata.cc_email');
        // get user company email
        if($company_id = (int)$user->get('metadata.company'))
        {
            $user_company = \Company\Model_Company::find_one_by_id($company_id);
            $content['cc_email'][] = $user_company->company_email;
        }
        // End - Add cc email

        $autoresponder->autoresponder_user($content);

        $content['subject'] = 'Order confirmation from '.$settings['website'].' for Admin';
        $autoresponder->autoresponder_admin($content, $settings['email_address']);//\Config::get('auto_response_emails.order_emails'));

        if($autoresponder->send())
        {
            \Messages::success('Thank You. Your order has been submitted and copy of this order has been sent to your email address.');
        }
        else
        {
            \Messages::success('Thank You. Your order has been submitted.');
        }
        
        // Delete order & cart session
        \Session::delete('order.id');
        \Session::delete('paypal.token');
        \Cart::clear();
                        
        \Response::redirect(\Uri::create('order/checkout/complete/' . $order->id));
    }
    
    /**
	 * Validate user forms
	 * 
	 * @param string $type 		= Validation type
	 * @param string $update_id = Updated item ID, used for some custom rules
	 * @return Validation object
	 */
	protected function validate($type, $update_id = false, $input_prefix = false)
	{
		$val = \Validation::forge($type);
		
        // Create new user
		if($type == 'create' || $type == 'update' || $type == 'guest' || $type == 'shipping' || $type == 'billing')
		{
			if($type != 'shipping')
            {
                // $val->add('business_name', 'Entity Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                if($this->check_logged_type() == 'guest')
                {
                    $val->add('email', 'Email')->add_rule('required')->add_rule('valid_email');
                }
                else
                {
                    //$val->add('email', 'Email')->add_rule('required')->add_rule('valid_email')->add_rule('unique', array('users', 'email', $update_id));
                    $val->add('email', 'Email')->add_rule('required')->add_rule('valid_email')->add_rule('unique', array('users', 'username', $update_id));
                }
            }
           
            if($type != 'shipping' && ($type == 'create' || ($type == 'update' && \Input::post('password', false))))
            {
                $val->add('password', 'Password')->add_rule('required')->add_rule('min_length', 6);
                $val->add('confirm_password', 'Confirm Password')->add_rule('required')->add_rule('match_field', 'password');
            }
			
            if($type != 'shipping')
            {
                if($input_prefix) $input_prefixes = array('', $input_prefix);
                else $input_prefixes = array('');
            }
            else
            {
               $input_prefixes = array($type); 
            }
            
            foreach($input_prefixes as $input_prefix)
            {
                // Set to different input prefix, so we are sure we covered both fieldset
                $name_prefix    = $input_prefix ? ucfirst($input_prefix) . ' ' : '';
                $input_prefix   = $input_prefix ? $input_prefix . '_' : '';
                
//                $val->add($input_prefix . 'title', $name_prefix . 'Title')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                $val->add($input_prefix . 'first_name', $name_prefix . 'First Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                $val->add($input_prefix . 'last_name', $name_prefix . 'Last Name')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                // $val->add($input_prefix . 'job_title', $name_prefix . 'Job Title')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
//                $val->add($input_prefix . 'address', $name_prefix . 'Address')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
               $val->add($input_prefix . 'suburb', $name_prefix . 'Suburb')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
//                $val->add($input_prefix . 'country', $name_prefix . 'Country')->add_rule('required_not_zero');
//                $val->add($input_prefix . 'state', $name_prefix . 'State')->add_rule('required_not_zero');
               $val->add($input_prefix . 'postcode', $name_prefix . 'Postcode')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
                $val->add($input_prefix . 'phone', $name_prefix . 'Phone')->add_rule('required')->add_rule('min_length', 2)->add_rule('max_length', 255);
            }
		}
		
		return $val;
	}
	
	public function action_get_discount()
	{
        $settings = \Config::load('autoresponder.db');
		$success = false;
		$valid = false;
		$lbl = 'apply';
		$f_discount = 0.00;
        $grand_total = 0.00;
		$gst_price = \Input::post('gst_price');

		// remove discount
		if (\Input::post('action') == 'remove')
		{
            $method = (\Input::post('method') == 'pickup' ? false : true);

            $order = \Order\Model_Order::forge();

            $items = \Cart::items();
            $cart_total = 0;
            $total_delivery_charge = 0;
            foreach ($items as $item){
                $total_price = $item->totalDiscountedPrice(true);
                $cart_total += $total_price;

                $product_data = null;
                if($product = \Product\Model_Product::find_one_by_id($item->get('id')))
                {
                    $product_data = \Product\Model_Product::product_data($product, $item->get('attributes'));
                };
                $total_delivery_charge += $product_data["delivery_charge"];
            }
            $total = \Helper::total_price(array($cart_total, 0));
            $shipping_price = $order->shipping_price(null, null, true, $method);
            $gst_price = 0;
            $sub_total = $cart_total + $shipping_price + $total_delivery_charge;

            $hold_gst = (isset($settings['gst']) ? 1 : 0);
            $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

            if($hold_gst && $hold_gst_percentage > 0)
            {
                $gst_price = \Helper::calculateGST($sub_total, $hold_gst_percentage);
            }

            //$gst_price = number_format(($cart_total + $shipping_price) * .10, 2, '.', '');
            $grand_total = $sub_total;

			$success = true;
			if(is_numeric(\Session::get('order.id'))) 
			{
				$order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
				$order->set(array(
					'id_discount' => NULL,
					'discount_amount' => 0,
                    'total_price'       => $grand_total,
                    'gst_price'         => $gst_price,
                    'total_delivery_charge'       => \Input::post('total_delivery_charge'),
				));
				$order->save();
			}
		}
		else
		{
            if(\Input::post('discount_code'))
            {
    			$discount = \Discountcode\Model_Discountcode::find_one_by_code(\Input::post('discount_code'));
    			if ($discount && $discount->status == 'active')
    			{
    				// check for expiry date
    				if ($discount->active_from != '0000-00-00' && $discount->active_to != '0000-00-00')
    				{
    					$current_date = strtotime(date('Y-m-d'));
    					$from = strtotime($discount->active_from);
    					$to = strtotime($discount->active_to);
    					if ($current_date >= $from && $current_date <= $to)
    					{
    						$valid = true;
    					}
    				}
    				else
    				{
    					$valid = true;
    				}

    				if ($valid)
    				{
    					$valid = false;
    					// check for single/multiple use
    					if ($discount->use_type == 'multi use')
    					{
    						$valid = true;
    					}
    					else
    					{
    						// check if discount code was already used.
    						$order_exists = \Order\Model_Order::find_one_by_id_discount($discount->id);
    						if (is_null($order_exists))
    						{
    							$valid = true;
    						}
    					}	
    				}

    				if ($valid)
    				{
    					if(is_numeric(\Session::get('order.id'))) 
    					{
    						$order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
    					}
    					if(!$order) $order = \Order\Model_Order::forge();

                        $method = (\Input::post('method') == 'pickup' ? false : true);
    					$shipping_price = $order->shipping_price(null, null, true, $method);

                        $gst_price = 0;
                        $sub_total = \Input::post('total_price') + $shipping_price + \Input::post('total_delivery_charge');

                        //check order value min and max
                        if ($discount->min_order_value != 0 || $discount->max_order_value != 0)
                        {
                            if($discount->max_order_value != 0)
                            {
                                if($sub_total >= $discount->min_order_value && $sub_total <= $discount->max_order_value)
                                    $valid = true;
                                else
                                    $valid = false;
                            }
                            else
                            {
                                if($sub_total >= $discount->min_order_value)
                                    $valid = true;
                                else
                                    $valid = false;
                            }
                        }
                        else
                        {
                            $valid = true;
                        }
                        //check order value min and max
    					
                        if ($valid)
                        {
        					// compute value of discount
        					switch($discount->type)
        					{
        						case 'free shipping':	$f_discount = \Input::post('method') == 'pickup' ? 0 : $shipping_price; break;
        						case 'percentage': 		$f_discount = \Input::post('total_price') * ($discount->type_value/100); break;
        						case 'value': 			$f_discount = $discount->type_value; break;
        					}

                            $hold_gst = (isset($settings['gst']) ? 1 : 0);
                            $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

                            if($hold_gst && $hold_gst_percentage > 0)
                            {
                                $gst_price = \Helper::calculateGST($sub_total - $f_discount, $hold_gst_percentage);
                            }

                            //$gst_price = number_format(($cart_total + $shipping_price) * .10, 2, '.', '');
                            $grand_total = $sub_total;
                            
        					$order->set(array(
        						'finished' 			=> 0,
        						'id_discount' 		=> $discount->id,
        						'discount_amount' 	=> $f_discount,
        						'shipping_price' 	=> $shipping_price,
                                'gst_price'         => $gst_price,
        						'total_price' 		=> $grand_total,
                                'total_delivery_charge'       => \Input::post('total_delivery_charge'),
        					));
        					$order->save();
        					\Session::set('order.id', $order->id);
        					
        					$lbl = 'remove';
        					$success = true;
                        }
    				}			
    			}
            }
		}
		echo json_encode(array('success' => $success, 'btn_label' => $lbl, 'discount_amount' => $f_discount, 'grand_total' => $grand_total, 'gst_price' => $gst_price));
	}
    
    public function action_check_discount()
    {
        $f_discount = 0.00;
        $discount = \Discountcode\Model_Discountcode::find_one_by_code(\Input::post('discount_code'));
        if ($discount && $discount->status == 'active')
        {
            // check for expiry date
            if ($discount->active_from != '0000-00-00' && $discount->active_to != '0000-00-00')
            {
                $current_date = strtotime(date('Y-m-d'));
                $from = strtotime($discount->active_from);
                $to = strtotime($discount->active_to);
                if ($current_date >= $from && $current_date <= $to)
                {
                    $valid = true;
                }
            }
            else
            {
                $valid = true;
            }

            if ($valid)
            {
                $valid = false;
                // check for single/multiple use
                if ($discount->use_type == 'multi use')
                {
                    $valid = true;
                }
                else
                {
                    // check if discount code was already used.
                    $order_exists = \Order\Model_Order::find_one_by_id_discount($discount->id);
                    if (is_null($order_exists))
                    {
                        $valid = true;
                    }
                }   
            }
            if ($valid)
            {
                if(is_numeric(\Session::get('order.id'))) 
                {
                    $order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
                }
                if(!$order) $order = \Order\Model_Order::forge();

                $method = (\Input::post('method') == 'pickup' ? false : true);
                $shipping_price = $order->shipping_price(null, null, true, $method);

                $sub_total = \Input::post('total_price') + $shipping_price + \Input::post('total_delivery_charge');

                //check order value min and max
                if ($discount->min_order_value != 0 || $discount->max_order_value != 0)
                {
                    if($discount->max_order_value != 0)
                    {
                        if($sub_total >= $discount->min_order_value && $sub_total <= $discount->max_order_value)
                            $valid = true;
                        else
                            $valid = false;
                    }
                    else
                    {
                        if($sub_total >= $discount->min_order_value)
                            $valid = true;
                        else
                            $valid = false;
                    }
                }
                else
                {
                    $valid = true;
                }
                //check order value min and max
                
                if($valid)
                {
                    // compute value of discount
                    switch($discount->type)
                    {
                        case 'free shipping':   $f_discount = \Input::post('method') == 'pickup' ? 0 : $shipping_price; break;
                        case 'percentage':      $f_discount = \Input::post('total_price') * ($discount->type_value/100); break;
                        case 'value':           $f_discount = $discount->type_value; break;
                    }
                }
            }
        }
        echo json_encode(array('discount' => $f_discount));
    }

    public function action_refresh_address()
    {
        $user = \Sentry::user();
        echo json_encode(array('user' => $user->get('metadata')));
    }

    public function action_clear_discount()
    {
        $order = \Order\Model_Order::forge();

        if(is_numeric(\Session::get('order.id'))) 
        {
            $order = \Order\Model_Order::find_one_by_id(\Session::get('order.id'));
            $order->set(array(
                'id_discount' => NULL,
                'discount_amount' => 0
            ));
            $order->save();
        }
    }
}