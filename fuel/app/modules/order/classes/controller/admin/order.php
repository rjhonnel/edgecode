<?php

/**
 * CMS
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 * 
 * @namespace Order
 * @extends \Admin\Controller_Base
 */

namespace Order;

use Product\Model_Product;

class Controller_Admin_Order extends \Admin\Controller_Base
{
    /**
     * Base view directory for this module
     * You can change it to whatever you want and views will be loaded from that directory
     */
    public $view_dir = 'views/order/';
    
    public function before()
    {
        parent::before();
        
        \Config::load('order::order', 'details');
        \Config::load('user::user', 'user');
        \Config::load('auto_response_emails', true);
        \Config::load('user::user', 'user');
        \Config::load('order::myob', 'myob');
        
        // Check if module is disabled and forbid access to it
        if(\Config::get('details.enabled') == FALSE)
        {
            throw new \HttpNotFoundException;
        }
    }
    
    /**
     * The basic welcome message
     * 
     * @access  public
     * @return  Response
     */
    public function action_index()
    {
        // Keep existing messages
        \Messages::instance()->shutdown();
        \Response::redirect('admin/order/list');
    }
    
    public function get_search_items($user_id = false, $no_limit=false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \SentryAdmin::user_exists((int)$user_id))
        {
            $user = \SentryAdmin::user((int)$user_id);
        }
        
        $items = \Order\Model_Order::find(function($query){
            
            if(isset($user))
                $query->where('user_id', $user->id);
            
            $query->where('finished', '1');
           /* if(\Input::get('d_order-sort'))
                $query->order_by('delivery_datetime', \Input::get('d_order-sort'));
            else
                $query->order_by('id', 'desc');*/

            if(\Input::get('order-sort'))
            {
                $query->order_by('date_viewed', \Input::get('order-sort'));
                $query->limit(10);
            }   
            else{
                $query->order_by('id', 'desc');
            }
            
        });
        
        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        foreach($items as $number => $item)
                        {
                            $full_name = $item->first_name . ' ' . $item->last_name;
                            if(stripos($item->company, $value) === false && stripos($item->id, $value) === false )
                            {
                                if(stripos($full_name, $value) === false) unset($items[$number]);
                            }
                        }
                        break;
                    case 'email':
                        foreach($items as $number => $item)
                        {
                            if(stripos($item->email, $value) === false) unset($items[$number]);
                        }
                        break;
                 
                    case 'order_total_from':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_order']) && $item_details['total_order'] < $value) unset($items[$number]);
                        }
                        break;
                    case 'order_total_to':
                        is_numeric($value) or $value == 0;
                        foreach($items as $number => $item)
                        {
                            $item_details = \Order\Model_Order::order_info($item->id);
                            if(isset($item_details['total_order']) && $item_details['total_order'] > $value) unset($items[$number]);
                        }
                        break;
                    case 'date_from':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', $item->created_at)) < $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_to':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', $item->created_at)) > $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'delivery_datetime':
                        // convert format date to m/d/Y
                        $parts = explode('/', $value);
                        $value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                        if($date = strtotime($value))
                        {
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', strtotime($item->delivery_datetime))) != $date) unset($items[$number]);
                            }
                        }
                        break;
                    case 'date_filter':
                        if($value == 'today')
                        {
                            $today = strtotime(date('m/d/Y'));
                            foreach($items as $number => $item)
                            {
                                if(strtotime(date('m/d/Y', $item->created_at)) != $today) unset($items[$number]);
                            }
                        }
                        break;
                    case 'status':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->status, $value) === false) unset($items[$number]);
                        }
                        break;
                   
                    case 'tracking_no':
                        foreach($items as $number => $item)
                        {
                            if(!$value != '') break;
                            if(stripos($item->tracking_no, $value) === false) unset($items[$number]);
                        }
                        break;
                    
                    case 'payment_method':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(!empty($item->last_payment))
                            {
                                if($item->last_payment->method != $value) 
                                {
                                    unset($items[$number]);
                                }
                            }
                        }
                        break;
                        
                    case 'user_group':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if($item->user_id && \SentryAdmin::user_exists((int)$item->user_id))
                            {
                                $user = \SentryAdmin::user((int)$item->user_id);
                                if (!$user->in_group($value)) unset($items[$number]);
                            }
                            else
                                unset($items[$number]);
                        }
                        break;
                       
                    case 'country':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'state':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            if(stripos($item->country, $value) === false) unset($items[$number]);
                        }
                        break;
                        
                    case 'product_category':
                        foreach($items as $number => $item)
                        {
                            if($value == 'false') break;
                            // Get order products
                            if(!empty($item->products))
                            {
                                $exists = array();
                                foreach ($item->products as $product)
                                {
                                    // Find category
                                    if(\Product\Model_Product_To_Categories::find(array('where' => array('product_id' => $product->product_id, 'category_id' => $value))))
                                    {
                                        $exists[] = $product->id;
                                    }
                                }
                                if(empty($exists)) unset($items[$number]);
                            }
                        }
                        break;
                }
            }
        }

        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();

        if(!$no_limit) {
            // Initiate pagination
            $pagination = \Hybrid\Pagination::make(array(
                'total_items' => count($items),
                'per_page' => \Input::get('per_page', 10),
                'uri_segment' => null,
            ));

            // Remove unwanted items, and show only required ones
            $items = array_slice($items, $pagination->offset, $pagination->per_page);
            return array('items' => $items, 'pagination' => $pagination);
        } else {
            return array('items' => $items);
        }
    }
    
    
    /**
     * List pages
     * 
     * @access  public
     * @return  Response
     */
    public function action_list($user_id = false)
    {
        \View::set_global('menu', 'admin/order/list');
        \View::set_global('title', 'List Orders');
        
        $search = $this->get_search_items($user_id);
        
        $items      = $search['items'];
        $pagination = $search['pagination'];
        $this_class = $this;
     
        $get_attributes = function($attributes) use ($this_class)
        {
            return $this_class->attributes_to_string($attributes);
        };
        \Theme::instance()->set_partial('content', $this->view_dir . 'list')
            ->set('items', $items)
            ->set('get_attributes', $get_attributes, false)
            ->set('pagination', $pagination, false);
    }
    
    /**
     * Create new order
     * 
     * @access  public
     * @return  Response
     */

    public function action_create()
    {
        $settings = \Config::load('autoresponder.db');

        if(\Input::post('create_form'))
        {
            $user = \Sentry::user((int)\Input::post('customer_id'));
            if($user)
            {
                // save the order created
                $order = $this->save_order($user);
                if(!$order)
                {
                    \Messages::error('There was an error while trying to save your order.');
                    \Response::redirect(\Uri::create('admin/order/create'));
                }

                if(\Input::post('payment_form') && !\Input::post('save_order_only'))
                {
                    $order = \Order\Model_Order::find_one_by_id($order->id);

                    $form = false;
                    $redirect = 'fail';

                    try
                    {
                        // Save and proccess payment
                        $paymentProccessStrategy = new \PaymentProccess\PaymentProccessTypeStrategy(\Input::post(), $order);

                        $form = $paymentProccessStrategy->proccessPayment();

                        if(isset($form)) $redirect = 'paypal_form';
                        else
                        {
                            // Determine redirect page by looking what proccess type we used
                            if($paymentProccessStrategy->fetchTypeOfProccess() == 'PaymentProccess\PaymentProccessTypeBank')
                                $redirect = 'success_bank';
                            else $redirect = 'success';
                        }

                    }
                    // If you want to catch exceptions for all payment proccesses make some \Exception\SomePayment
                    // that will be applicable for all payment proccesses and throw them from everywhere :)
                    catch ( \PaymentProccess\Exception\SecurePay\SecurePayServerDownException $e )
                    {
                        \Messages::error($e->getMessage());
                        $redirect = 'fail';
                    }
                    catch ( \PaymentProccess\Exception\SecurePay\SecurePayTransactionFailedException $e )
                    {
                         \Messages::error($e->getMessage());
                         $redirect = 'fail';
                    }
                    catch ( \PaymentProccess\Exception\SecurePay\SecurePayCustomerDataInvalidException $e )
                    {
                         \Messages::error($e->getMessage());
                         $redirect = 'fail';
                    }
                    catch ( \Exception $e )
                    {
                         \Messages::error($e->getMessage());
                         $redirect = 'fail';
                    }

                    try
                    {
                        if($redirect != 'fail')
                        {
                            $order->set(array('accepted' => 1));
                            $order->save();
                            $this->autoresponder($user, $order);
                            \Messages::success('Payment completed.');

                            if(\Input::post('partial_payment', 0))
                            {
                                /*\Response::redirect(\Uri::create('admin/order/edit/'.$order->id.'?step=3'));*/
                                \Response::redirect(\Uri::create('admin/order/list/'));
                            }
                            else
                            {
                                /*\Response::redirect(\Uri::create('admin/order/update/'.$order->id));*/
                                \Response::redirect(\Uri::create('admin/order/list/'));
                            }

                        }
     
                    }
                    catch (\Database_Exception $e)
                    {
                        // show validation errors
                        \Messages::error('There was an error while trying to save order payment.');

                        // Uncomment lines below to show database errors
                        $errors = $e->getMessage();
                        \Messages::error($errors);

                        //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                         \Response::redirect(\Uri::create('admin/order/list/'));
                    }

                    if($redirect == 'fail')
                        //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                        \Response::redirect(\Uri::create('admin/order/list/'));
                }
                else
                {
                    $order = \Order\Model_Order::find_one_by_id($order->id);
                    $order->set(array('accepted' => 1));
                    $order->save();
                    $this->autoresponder($user, $order);
                    \Messages::success('Payment completed.');

                    if(\Input::post('partial_payment', 0) && !\Input::post('save_order_only'))
                    {
                        //\Response::redirect(\Uri::create('admin/order/edit/'.$order->id.'?step=3'));
                        \Response::redirect(\Uri::create('admin/order/list/'));
                    }
                    else
                    {
                        //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                        \Response::redirect(\Uri::create('admin/order/list/'));
                    }
                }
            }
        }
        else
        {
            $order_id = \Session::get('admin_order.id', false);
            if(!is_numeric($order_id))
            {
                $order = \Order\Model_Order::forge();
                $order->finished = 0;
                $order->save();
                \Session::set('admin_order.id', $order->id);
            }
        }
        \View::set_global('menu', 'admin/order/list');
        \View::set_global('title', 'Add New');
        \Theme::instance()->set_partial('content', $this->view_dir . 'create')->set('settings', $settings, false);
    }

    protected function save_order($user)
    {
        $order = false;

        try
        {
            // Update or create order
            if(is_numeric(\Session::get('admin_order.id'))) $order = \Order\Model_Order::find_one_by_id(\Session::get('admin_order.id'));

            if(!$order) $order = \Order\Model_Order::forge();

            $hold_delivery_datetime = null;
            $hold_delivery_datetime_list = NULL;
            $date = date('Y-m-d H:i:s');
            if(\Input::post('delivery_date'))
            {
                $delivery_date = \Input::post('delivery_date');

                $parts = explode('/', $delivery_date);
                $delivery_date_value = $parts[1].'/'.$parts[0].'/'.$parts[2];


                // rearrange time list
                $hold_time_list_arr = explode(';', \Input::post('delivery_datetime_list'));
                sort($hold_time_list_arr);
                $hold_time_list_arr_sort = implode(';', $hold_time_list_arr);

                $hold_delivery_datetime_list = \Input::post('delivery_datetime_list')?$hold_time_list_arr_sort:NULL;

                $hold_delivery_datetime = date('Y-m-d H:i', strtotime($delivery_date_value.' '.$hold_time_list_arr[0]));
            }

            $data = array(
                'user_id' => $user->id,
                'status' => \Input::post('status'),
                'title' => $user->get('metadata.title'),
                'first_name' => $user->get('metadata.first_name'),
                'last_name' => $user->get('metadata.last_name'),
                'company' => $user->get('metadata.business_name'),
                'address' => $user->get('metadata.address'),
                'address2' => $user->get('metadata.address2'),
                'suburb' => $user->get('metadata.suburb'),
                'postcode' => $user->get('metadata.postcode'),
                'country' => $user->get('metadata.country'),
                'state' => $user->get('metadata.state'),
                'phone' => $user->get('metadata.phone'),
                'email' => $user->get('email'),
                'guest' => 1,
                'total_price' => \Input::post('products_total_hidden'),
                'discount_amount' => \Input::post('discount_hidden'),
                'shipping_method'=> null,
                'shipping_price' => \Input::post('shipping_hidden'),
                'gst_price' => \Input::post('gst_amount_hidden'),
                'finished' => 1,
                'accepted' => 0,
                'credit_account' => 0,
                'shipping_title' => \Input::post('shipping_title', 'Mr.'),
                'shipping_first_name' => \Input::post('shipping_first_name', ''),
                'shipping_last_name' => \Input::post('shipping_last_name', ''),
                'shipping_company' => \Input::post('shipping_company', ''),
                'shipping_address' => \Input::post('shipping_address', ''),
                'shipping_address2' => \Input::post('shipping_address2', ''),
                'shipping_suburb' => \Input::post('shipping_suburb'),
                'shipping_postcode' => \Input::post('shipping_postcode', ''),
                'shipping_country' => \Input::post('shipping_country'),
                'shipping_state' => \Input::post('shipping_state', ''),
                'shipping_phone' => \Input::post('shipping_phone', ''),
                'notes' => \Input::post('notes'),
                'delivery_notes' => \Input::post('delivery_notes'),
                'total_delivery_charge' => \Input::post('delivery_charge_hidden'),
                'delivery_datetime' => $hold_delivery_datetime,
                'delivery_datetime_list' => \Input::post('delivery_datetime_list'),
                'client_po' => \Input::post('client_po'),
                'cater_for' => \Input::post('cater_for'),
                'date_viewed' => $date,
            );
            $order->set($data);

            $product_id = \Input::post('product_id');
            $product_quantity = \Input::post('product_quantity');
            $product_price = \Input::post('product_price');
            $product_delivery_charge = \Input::post('product_delivery_charge');
            $delivery_time = \Input::post('delivery_time');
            $packs = \Input::post('packs');
            $pack_delivery_time = \Input::post('pack_delivery_time');

            // Save order, add products to order products
            if($order->save())
            {            
                foreach ($product_id as $key => $item)
                {
                    $product_data = null;
                    $selected_attributes_json = null;
                    if($product = \Product\Model_Product::find_one_by_id($item))
                    {
                        $product_data = \Product\Model_Product::product_data($product, null);

                        $orig_attributes = array();
                        $select_attributes = \Input::post('select_attributes')[$key];
                        if(isset($select_attributes) && !empty($select_attributes))
                        {
                            $orig_attributes = $select_attributes;
                            ksort($select_attributes);
                            $selected_attributes_json = json_encode($select_attributes);
                        }
                        if($selected_attributes_json)
                            $product_data = \Product\Model_Product::product_data($product, $selected_attributes_json);
                    };


                    $item_exists = \Order\Model_Products::find( array('where' => array( 'product_id' => $product->id, 'order_id' => $order->id, 'attributes_id' => $selected_attributes_json ) ) );

                    if($product_data && !$item_exists )
                    { 
                        $pack_list = null;
                        if(isset($packs[$product->id]))
                        {
                            $pack_list = array();
                            foreach($packs[$product->id] as $pack_key => $pack)
                            {
                                $pack_products = array();
                                if(isset($pack['products']))
                                {
                                    foreach ($pack['products'] as $ppkey => $pp)
                                    {
                                        array_push($pack_products, array(
                                            'id' => $ppkey,
                                            'name' => $pp,
                                        ));
                                    }
                                }

                                array_push($pack_list, array(
                                    'id' => $pack_key,
                                    'name' => $pack['name'],
                                    'types' => $pack['types'],
                                    'selection_limit' => $pack['selection_limit'],
                                    'time' => $pack_delivery_time[$product->id][$pack_key],
                                    'products' => $pack_products,
                                ));
                            }
                            $pack_list = json_encode($pack_list);
                        }

                        $attributes_hold = isset($product_data['current_attributes'][0]->product_attribute->attributes)?$product_data['current_attributes'][0]->product_attribute->attributes:0;
                        $order_products = \Order\Model_Products::forge(array(
                            'order_id'          => $order->id,
                            'title'             => $product->title,
                            'code'              => $product_data['code'],
                            'price'             => $product_price[$key],
                            'price_type'        => isset($product_data['price_type'])?$product_data['price_type']:'',
                            'quantity'          => $product_quantity[$key],
                            'product_id'        => $product->id,
                            'subtotal'          => ( $product_price[$key] * $product_quantity[$key] ),
                            'attributes'        => json_encode(\Product\Model_Attribute::get_combination($attributes_hold)),
                            'attributes_id'     => $attributes_hold,
                            'delivery_charge'   => $product_delivery_charge[$key]?:0,
                            'delivery_time'     => $delivery_time[$key],
                            'packs'             => $pack_list,
                        ));

                        if(!empty($product->categories))
                        {
                            $categories = array();
                            foreach ($product->categories as $category)
                            {
                                $categories[] = $category->title;
                            }
                            if($categories) $order_products->product_category = implode(',', $categories);
                        }

                        // update stock quantity
                        \Product\Model_Attribute::update_stock_quantity(0, $product_quantity[$key]);

                        $order_products->save();
                    }
                }

                if(!\Input::post('save_order_only'))
                {
                    $payment = \Payment\Model_Payment::forge(); 
                    $payment->set(array(
                        'order_id' => $order->id,
                        'total_price' => \Input::post('partial_payment', 0)?\Input::post('ppamount', 0):$order->total_price(),
                        'method' => \Input::post('payment_method'),
                        'status' => \Input::post('payment_status'),
                        'status_detail' => \Inflector::humanize(\Input::post('payment_status')),
                        'partial_payment' => \Input::post('partial_payment', 0),
                    ));
                    $payment->save();
                }
            }
            
            if($order)
            {

                return $order;
            }
            else
            {
                return false;
            }
            
        }
        catch (\Database_Exception $e)
        {
            // show validation errors
            \Messages::error('There was an error while trying to save your order.');

            // Uncomment lines below to show database errors
            $errors = $e->getMessage();
            \Messages::error($errors);

            \Response::redirect(\Uri::create('admin/order/create'));
        }
        
        return false;
    }
    
    protected function autoresponder($user, $order)
    {
        $settings = \Config::load('autoresponder.db');

        // Send autoresponder
        $autoresponder = \Autoresponder\Autoresponder::forge();

        $autoresponder->user_id =  (int)$user->id;

        $autoresponder->view_user = 'new_order';
        $autoresponder->view_admin = 'new_order';

        $content['content'] = $order;

        $content['subject'] = 'Order confirmation from '.$settings['website'];
        
        // Add cc email
        $content['cc_email'] = array();
        // get user cc email
        if($user->get('metadata.cc_email'))
            $content['cc_email'][] = $user->get('metadata.cc_email');
        // get user company email
        if($company_id = (int)$user->get('metadata.company'))
        {
            $user_company = \Company\Model_Company::find_one_by_id($company_id);
            $content['cc_email'][] = $user_company->company_email;
        }
        // End - Add cc email

        $autoresponder->autoresponder_user($content);

        $content['subject'] = 'Order confirmation from '.$settings['website'].' for Admin';
        $autoresponder->autoresponder_admin($content, $settings['email_address']);//\Config::get('auto_response_emails.order_emails'));

        if($autoresponder->send())
        {
            \Messages::success('Thank You. Your order has been submitted and copy of this order has been sent to your email address.');
        }
        else
        {
            \Messages::success('Thank You. Your order has been submitted.');
        }

        // Delete order & cart session
        \Session::delete('admin_order.id');
    }

    public function action_get_cart_data()
    {
        $items = \Input::post('items');
        $return_items = array();
        $products_total = 0;
        $discount_value = 0;
        $total_category_discount = 0;
        $delivery_charge = 0;
        $hold = floatval(\Input::post('discount_value'));

        $settings = \Config::load('autoresponder.db');

        if($hold > 0)
            $discount_value = $hold;

        if($items)
        {
            foreach ($items as $key => $item) 
            {
           
                $product = \Product\Model_Product::find_one_by_id($item['product_id']);
                $product_data = $product->data;

                $selected_attributes_json = null;
                $orig_attributes = array();
                if(isset($item['select']) && !empty($item['select']))
                {
                    $orig_attributes = $item['select'];
                    ksort($item['select']);
                    $selected_attributes_json = json_encode($item['select']);
                }
                if($selected_attributes_json)
                    $product_data = \Product\Model_Product::product_data($product, $selected_attributes_json);

                $price = $item['product_price'];
                $get_category_discount = \Discountcode\Model_Discountcode::getCategoryDiscount($item['product_id'], ( $item['product_quantity'] * $price ));
                $total_category_discount += $get_category_discount;
                array_push($return_items, array('price'=> $price,'title' => $product['title'], 'select' => $orig_attributes, 'code' => $product_data['code'], 'product_id' => $item['product_id'], 'product_quantity' => $item['product_quantity'], 'category_discount' => $get_category_discount,'delivery_time' => (isset($item['delivery_time'])?$item['delivery_time']:''),'packs' => (isset($item['packs'])?$item['packs']:array()), "delivery_charge" => isset($product_data['delivery_charge'])?$product_data['delivery_charge']:0 ));

                $products_total += ( $item['product_quantity'] * $price );
                $delivery_charge += isset($product_data['delivery_charge'])?$product_data['delivery_charge']:0;
            }

            //discount
            $discount_value += $total_category_discount;
            
            $order = \Order\Model_Order::forge();
            $shipping_price = 0;
            if(\Input::post('shipping_value') > 0)
                $shipping_price = \Input::post('shipping_value');
            $final_gst = 0;
            $grand_total = $products_total + $shipping_price - $discount_value + $delivery_charge;

            $hold_gst = (isset($settings['gst']) ? 1 : 0);
            $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

            if($hold_gst && $hold_gst_percentage > 0) {
                $final_gst = \Helper::calculateGST($grand_total, $hold_gst_percentage);
            }
            echo json_encode( array( 'items' => $return_items, 'products_total' => $products_total, 'discount' => $discount_value, 'category_discount' => $total_category_discount, 'shipping' => $shipping_price, 'delivery_charge' => $delivery_charge, 'gst_amount' => $final_gst,'grand_total' => $grand_total ) );
        }
        else
        {
            $order = \Order\Model_Order::forge();

            $final_gst = 0;
            $sub_total = 0;
            $shipping_price = 0;
            $discount_value = 0;
            $delivery_charge = 0;
            $grand_total = $products_total + $shipping_price - $discount_value + $delivery_charge;
        
            if(\Input::post('shipping_value') > 0)
                $shipping_price = \Input::post('shipping_value');
            $products_total = 0;

            echo json_encode(array( 'items' => false, 'products_total' => 0, 'discount' => $discount_value, 'category_discount' => $total_category_discount, 'shipping' => $shipping_price, 'gst_amount' => $final_gst, 'delivery_charge' => $delivery_charge,'grand_total' => $sub_total ));
        }
    }

    public function action_get_product_data()
    {
        $product_id = \Input::post('product_id');

        if($product_id)
        {
            if($product = \Product\Model_Product::find_one_by_id($product_id))
            {
                $product_data = $product->data;

                $has_attributes = false;
                if(isset($product_data['current_attributes']))
                    $has_attributes = true;

                $price = $product_data['price']?$product_data['price']:$product_data['retail_price'];
                echo json_encode( array( "product" => array( 
                                            "id" => $product['id'],
                                            "name" => $product['title'],
                                            "price" => $price?:0,
                                            "delivery_charge" => isset($product_data['delivery_charge'])?$product_data['delivery_charge']:0,
                                            "has_attributes" => $has_attributes,
                                ) ) );
            }
            else
                echo json_encode( array('product' => false) );
        }
        else
        {
            echo json_encode( array('product' => false) );
        }
    }

    public function action_get_discount_value()
    {
        $discount_action = 'Apply';
        $valid = false;
        $f_discount = 0.00;
        $success = false;
        $grand_total = 0.00;
        $gst_price = \Input::post('gst_price');

        $settings = \Config::load('autoresponder.db');

        if (\Input::post('discount_action') == 'Remove')
        {
            $method = (\Input::post('method') == 'pickup' ? false : true);

            $order = \Order\Model_Order::forge();

            $shipping_price = \Input::post('shipping');

            $grand_total = \Input::post('total_price') + $shipping_price + \Input::post('delivery_charge');

            $gst_price = 0;

            $hold_gst = (isset($settings['gst']) ? 1 : 0);
            $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

            if($hold_gst && $hold_gst_percentage > 0)
            {
                $gst_price = \Helper::calculateGST($grand_total, $hold_gst_percentage);
            }

            $success = true;
            if(is_numeric(\Session::get('admin_order.id'))) 
            {
                $order = \Order\Model_Order::find_one_by_id(\Session::get('admin_order.id'));
                $order->set(array(
                    'id_discount' => NULL,
                    'discount_amount' => 0,
                    'total_price'       => $grand_total,
                    'gst_price'         => $gst_price
                ));
                $order->save();
            }
        }
        else
        {
            if(\Input::post('discount_code'))
            {
                $discount = \Discountcode\Model_Discountcode::find_one_by_code(\Input::post('discount_code'));
                if ($discount && $discount->status == 'active')
                {
                    // check for expiry date
                    if ($discount->active_from != '0000-00-00' && $discount->active_to != '0000-00-00')
                    {
                        $current_date = strtotime(date('Y-m-d'));
                        $from = strtotime($discount->active_from);
                        $to = strtotime($discount->active_to);
                        if ($current_date >= $from && $current_date <= $to)
                        {   
                            $valid = true;
                        }
                    }
                    else
                    {
                        $valid = true;
                    }

                    if ($valid)
                    {
                        $valid = false;
                        // check for single/multiple use
                        if ($discount->use_type == 'multi use')
                        {
                            $valid = true;
                        }
                        else
                        {
                            // check if discount code was already used.
                            $order_exists = \Order\Model_Order::find_one_by_id_discount($discount->id);
                            if (is_null($order_exists))
                            {
                                $valid = true;
                            }
                        }   
                    }
                    if ($valid)
                    {
                        if(is_numeric(\Session::get('admin_order.id'))) 
                        {
                            $order = \Order\Model_Order::find_one_by_id(\Session::get('admin_order.id'));
                        }
                        if(!$order) $order = \Order\Model_Order::forge();

                        $method = (\Input::post('method') == 'pickup' ? false : true);
                        $shipping_price = \Input::post('shipping');
                        
                        // compute value of discount
                        switch($discount->type)
                        {
                            case 'free shipping':   $f_discount = \Input::post('method') == 'pickup' ? 0 : $shipping_price; break;
                            case 'percentage':      $f_discount = \Input::post('total_price') * ($discount->type_value/100); break;
                            case 'value':           $f_discount = $discount->type_value; break;
                        }
                        // $gst_price = number_format((\Input::post('total_price') + $shipping_price) * .10, 2, '.', '');
                        // $grand_total = number_format(\Input::post('total_price') + $shipping_price + ((\Input::post('total_price') + $shipping_price) * .10), 2, '.', '');

                        $gst_price = 0;
                        $sub_total = \Input::post('total_price') + $shipping_price + \Input::post('delivery_charge');
                        
                        $hold_gst = (isset($settings['gst']) ? 1 : 0);
                        $hold_gst_percentage = (isset($settings['gst_percentage']) ? $settings['gst_percentage'] : 0);

                        if($hold_gst && $hold_gst_percentage > 0)
                        {
                            $gst_price = \Helper::calculateGST($sub_total - $f_discount, $hold_gst_percentage);
                        }

                        $grand_total = $sub_total;

                        $order->set(array(
                            'finished'          => 0,
                            'id_discount'       => $discount->id,
                            'discount_amount'   => $f_discount,
                            'shipping_price'    => $shipping_price,
                            'gst_price'         => $gst_price,
                            'total_price'       => $grand_total,
                        ));
                        $order->save();
                        \Session::set('admin_order.id', $order->id);
                        
                        $discount_action = 'Remove';
                        $success = true;
                    }                   
                }
            }
        }
        echo json_encode(array('success' => $success, 'discount_action' => $discount_action, 'discount_amount' => $f_discount, 'gst_price' => $gst_price));
    }

    public function action_get_customer_details()
    {
        $user_id = \Input::post('customer_id');

        if($user_id && \SentryAdmin::user_exists((int)$user_id))
        {

            $user = \SentryAdmin::user((int)$user_id);
            if($user->get('metadata.company')){
                $company = \Company\Model_Company::find_one_by_id((int)$user->get('metadata.company'));

                $company_name = $company->company_name;
            }
            else{
                $company_name = $user->get('metadata.business_name');   
            }
            echo json_encode( array( "user" => array( 
                                        "email" => $user->get('email'),
                                        "title" => $user->get('metadata.title') , 
                                        "first_name" => $user->get('metadata.first_name') , 
                                        "last_name" => $user->get('metadata.last_name') , 
                                        "company" => $company_name, 
                                        "address" => $user->get('metadata.address') , 
                                        "address2" => $user->get('metadata.address2') , 
                                        "suburb" => $user->get('metadata.suburb') , 
                                        "country" => $user->get('metadata.country') , 
                                        "state" => $user->get('metadata.state') , 
                                        "postcode" => $user->get('metadata.postcode') , 
                                        "phone" => $user->get('metadata.phone')
                            ) ) );
        }
        else
        {
            echo json_encode( array('user' => false) );
        }

    }

    public function action_edit($id = false)
    {
    
        if(!is_numeric($id)) \Response::redirect('admin/order/list');
        
        // Get order to edit
        if(!$order = \Order\Model_Order::find_one_by_id($id)) \Response::redirect('admin/order/list');
        // check if order is finished
        if($order->finished == 0) \Response::redirect('admin/order/list');

        \View::set_global('menu', 'admin/order/list');
        \View::set_global('title', 'Edit Products Ordered');

        $settings = \Config::load('autoresponder.db');

        $user = $this->get_user($order->user_id);

        if(\Input::post('edit_form'))
        {
            if($order){
            $date = date('Y-m-d H:i:s');
                $order->set(array('date_viewed' => $date));
                $order->save();
            }
            $id_discount = null;
            $discount = \Discountcode\Model_Discountcode::find_one_by_code(\Input::post('discount_code'));
            if($discount)
                $id_discount = $discount->id;
            
            $hold_delivery_datetime = null;
            $hold_delivery_datetime_list = NULL;
            $date = date('Y-m-d H:i:s');
            if(\Input::post('delivery_date'))
            {
                $delivery_date = \Input::post('delivery_date');

                $parts = explode('/', $delivery_date);
                $delivery_date_value = $parts[1].'/'.$parts[0].'/'.$parts[2];


                // rearrange time list
                $hold_time_list_arr = explode(';', \Input::post('delivery_datetime_list'));
                sort($hold_time_list_arr);
                $hold_time_list_arr_sort = implode(';', $hold_time_list_arr);

                $hold_delivery_datetime_list = \Input::post('delivery_datetime_list')?$hold_time_list_arr_sort:NULL;

                $hold_delivery_datetime = date('Y-m-d H:i', strtotime($delivery_date_value.' '.$hold_time_list_arr[0]));
            }

            $data = array(
                'shipping_title' => \Input::post('shipping_title'),
                'shipping_first_name' => \Input::post('shipping_first_name'),
                'shipping_last_name' => \Input::post('shipping_last_name'),
                'shipping_company' => \Input::post('shipping_company'),
                'shipping_address' => \Input::post('shipping_address'),
                'shipping_address2' => \Input::post('shipping_address2'),
                'shipping_suburb' => \Input::post('shipping_suburb'),
                'shipping_country' => \Input::post('shipping_country'),
                'shipping_state' => \Input::post('shipping_state'),
                'shipping_postcode' => \Input::post('shipping_postcode'),
                'shipping_phone' => \Input::post('shipping_phone'),
                'discount_amount' => \Input::post('discount_hidden'),
                'total_price' => \Input::post('products_total_hidden'),
                'shipping_price' => \Input::post('shipping_hidden'),
                'gst_price' => \Input::post('gst_amount_hidden'),
                'status' => \Input::post('status'),
                'notes' => \Input::post('notes'),
                'delivery_notes' => \Input::post('delivery_notes'),
                'delivery_datetime' => $hold_delivery_datetime,
                'delivery_datetime_list' => \Input::post('delivery_datetime_list'),
                'id_discount' => $id_discount,
                'total_delivery_charge' => \Input::post('delivery_charge_hidden'),
                'client_po' => \Input::post('client_po'),
                'cater_for' => \Input::post('cater_for'),
            );

            $order->set($data);
            if($order->save())
            {
                $product_id = \Input::post('product_id');
                $product_quantity = \Input::post('product_quantity');
                $product_price = \Input::post('product_price');
                $product_delivery_charge = \Input::post('product_delivery_charge');
                $delivery_time = \Input::post('delivery_time');
                $packs = \Input::post('packs');
                $pack_delivery_time = \Input::post('pack_delivery_time');
                if($item_exists = \Order\Model_Products::find( array('where' => array( 'order_id' => $order->id ) ) ))
                {
                    foreach ($item_exists as $ie)
                        $ie->delete();
                }

                foreach ($product_id as $key => $item)
                {

                    $product_data = null;
                    $selected_attributes_json = null;

                    if($product = \Product\Model_Product::find_one_by_id($item))
                    {
                        $product_data = \Product\Model_Product::product_data($product, null);

                        $orig_attributes = array();
                        $select_attributes = isset(\Input::post('select_attributes')[$key])?\Input::post('select_attributes')[$key]:null;
                        if(isset($select_attributes) && !empty($select_attributes))
                        {
                            $orig_attributes = $select_attributes;
                            ksort($select_attributes);
                            $selected_attributes_json = json_encode($select_attributes);
                        }
                        if($selected_attributes_json)
                            $product_data = \Product\Model_Product::product_data($product, $selected_attributes_json);
                    }

                    if($product_data)
                    {
                        $pack_list = null;
                        if(isset($packs[$product->id]))
                        {
                            $pack_list = array();
                            foreach($packs[$product->id] as $pack_key => $pack)
                            {
                                $pack_products = array();
                                if(isset($pack['products']))
                                {
                                    foreach ($pack['products'] as $ppkey => $pp)
                                    {
                                        array_push($pack_products, array(
                                            'id' => $ppkey,
                                            'name' => $pp,
                                        ));
                                    }
                                }

                                array_push($pack_list, array(
                                    'id' => $pack_key,
                                    'name' => $pack['name'],
                                    'types' => $pack['types'],
                                    'selection_limit' => $pack['selection_limit'],
                                    'time' => $pack_delivery_time[$product->id][$pack_key],
                                    'products' => $pack_products,
                                ));
                            }
                            $pack_list = json_encode($pack_list);
                        }

                        $attributes_hold = isset($product_data['current_attributes'][0]->product_attribute->attributes)?$product_data['current_attributes'][0]->product_attribute->attributes:0;
                        $order_products = \Order\Model_Products::forge(array(
                            'order_id'          => $order->id,
                            'title'             => $product->title,
                            'code'              => $product_data['code'], 
                            'price'             => $product_price[$key],
                            'price_type'        => isset($product_data['price_type'])?$product_data['price_type']:'',
                            'quantity'          => $product_quantity[$key],
                            'product_id'        => $product->id,
                            'subtotal'          => ( $product_price[$key] * $product_quantity[$key] ),
                            'attributes'        => json_encode(\Product\Model_Attribute::get_combination($attributes_hold)),
                            'attributes_id'     => $attributes_hold,
                            'delivery_charge'   => $product_delivery_charge[$key]?:0,
                            'delivery_time'     => $delivery_time[$key],
                            'packs'             => $pack_list,
                        ));

                        if(!empty($product->categories))
                        {
                            $categories = array();
                            foreach ($product->categories as $category)
                            {
                                $categories[] = $category->title;
                            }
                            if($categories) $order_products->product_category = implode(',', $categories);
                        }

                        // update stock quantity
                        \Product\Model_Attribute::update_stock_quantity(0, $product_quantity[$key]);

                        $order_products->save();

                    }
                }

                if(!\Input::post('save_order_only'))
                {
                    $payment = \Payment\Model_Payment::forge(); 
                    $payment->set(array(
                        'order_id' => $order->id,
                        'total_price' => \Input::post('partial_payment', 0)?\Input::post('ppamount', 0):$order->total_price(),
                        'method' => \Input::post('payment_method'),
                        'status' => \Input::post('payment_status'),
                        'status_detail' => \Inflector::humanize(\Input::post('payment_status')),
                        'partial_payment' => \Input::post('partial_payment', 0),
                    ));
                    $payment->save();
                }


                $order_history = Model_History::forge(array(
                    'order_id' => $order->id,
                    'event' => 'Products ordered edited.',
                ));
                $order_history->save();

                if(\Input::post('payment_form') && !\Input::post('save_order_only')){
                    $order = \Order\Model_Order::find_one_by_id($id);

                    $form = false;
                    $redirect = 'fail';

                    try
                    {
                        // Save and proccess payment
                        $paymentProccessStrategy = new \PaymentProccess\PaymentProccessTypeStrategy(\Input::post(), $order);

                        $form = $paymentProccessStrategy->proccessPayment();

                        if(isset($form)) $redirect = 'paypal_form';
                        else
                        {
                            // Determine redirect page by looking what proccess type we used
                            if($paymentProccessStrategy->fetchTypeOfProccess() == 'PaymentProccess\PaymentProccessTypeBank')
                                $redirect = 'success_bank';
                            else $redirect = 'success';
                        }

                    }
                    // If you want to catch exceptions for all payment proccesses make some \Exception\SomePayment
                    // that will be applicable for all payment proccesses and throw them from everywhere :)
                    catch ( \PaymentProccess\Exception\SecurePay\SecurePayServerDownException $e )
                    {
                        \Messages::error($e->getMessage());
                        $redirect = 'fail';
                    }
                    catch ( \PaymentProccess\Exception\SecurePay\SecurePayTransactionFailedException $e )
                    {
                         \Messages::error($e->getMessage());
                         $redirect = 'fail';
                    }
                    catch ( \PaymentProccess\Exception\SecurePay\SecurePayCustomerDataInvalidException $e )
                    {
                         \Messages::error($e->getMessage());
                         $redirect = 'fail';
                    }
                    catch ( \Exception $e )
                    {
                         \Messages::error($e->getMessage());
                         $redirect = 'fail';
                    }

                    try
                    {
                        if($redirect != 'fail')
                        {
                            $order->set(array('accepted' => 1));
                            $order->save();
                            \Messages::success('Payment completed.');

                            if(\Input::post('partial_payment', 0))
                            {
                                //\Response::redirect(\Uri::create('admin/order/edit/'.$order->id.'?step=3'));
                                \Response::redirect(\Uri::create('admin/order/list/'));
                            }
                            else
                            {
                                //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                                \Response::redirect(\Uri::create('admin/order/list/'));
                            }
                        }
     
                    }
                    catch (\Database_Exception $e)
                    {
                        // show validation errors
                        \Messages::error('There was an error while trying to save order.');

                        // Uncomment lines below to show database errors
                        $errors = $e->getMessage();
                        \Messages::error($errors);

                        //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                        \Response::redirect(\Uri::create('admin/order/list/'));
                    }

                    if($redirect == 'fail')
                        //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                        \Response::redirect(\Uri::create('admin/order/list/'));
                }
                else
                {
                    \Messages::success('Payment completed.');
                    if(\Input::post('partial_payment', 0) && !\Input::post('save_order_only'))
                    {
                        //\Response::redirect(\Uri::create('admin/order/edit/'.$order->id.'?step=3'));
                        \Response::redirect(\Uri::create('admin/order/list/'));
                    }
                    else
                    {
                        //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                        \Response::redirect(\Uri::create('admin/order/list/'));
                    }
                }
            }
            \Messages::success('Products ordered updated.');
            //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
            \Response::redirect(\Uri::create('admin/order/list/'));

        }

        \Theme::instance()->set_partial('content', $this->view_dir . 'edit')
                ->set(compact('user', 'order'), null, false)
                ->set('settings', $settings, false);
              
    }
    
    /**
     * Update Order
     * 
     * @param type $id  = Order ID
     */
    public function action_update($id = false)
    {
        if(!is_numeric($id)) \Response::redirect('admin/order/list');
        
        // Get order to edit
        if(!$order = \Order\Model_Order::find_one_by_id($id)) \Response::redirect('admin/order/list');
        // check if order is finished
        if($order->finished == 0) \Response::redirect('admin/order/list');
        
        $notes = \User\Model_Notes::find_by('user_id',$order->user_id);

        \View::set_global('menu', 'admin/order/list');
        \View::set_global('title', 'Edit Order');

        $settings = \Config::load('autoresponder.db');
        
        if(\Input::post('order_details', false) && \Input::post('customer_details', false))
        {
            //validate delivery date and time
            /*if(!\Input::post('delivery_date'))
            {
                \Messages::error('Delivery date required.');
                \Response::redirect(\Uri::admin('current'));
            }
            elseif(!\Input::post('delivery_datetime_list'))
            {
                \Messages::error('Delivery time is required.');
                \Response::redirect(\Uri::admin('current'));
            }*/

            try
            {
                if($order){
                    $date = date('Y-m-d H:i:s');
                    $order->set(array('date_viewed' => $date));
                    $order->save();
                }
                $fields = array('status', 'shipping_method', 'tracking_no','client_po','cater_for');
                $events = array();
                
                foreach ($fields as $field)
                {
                    $config = \Config::get('details.' . $field, array());
                    
                    if(isset($config[\Input::post($field)]) && $order->$field) $value = $config[\Input::post($field)];
                    else $value = \Inflector::humanize(\Input::post($field));
                    
                    if(isset($config[$order->$field])) $old_value = $config[$order->$field];
                    else $old_value = \Inflector::humanize($order->$field);
                    
                    if($order->$field != \Input::post($field)) 
                        $events[] = \Inflector::humanize($field) . ' changed. ' . $old_value . ' => ' . $value;

                    $data[$field] = \Input::post($field);
                }
                
                // Send to customer
                if(\Input::post('send_to_customer_payment'))
                {
                    //echo 'send_to_customer_payment'; exit;
                }
                 
                if(\Input::post('send_to_customer_shipping'))
                {
                    //echo 'send_to_customer_shipping'; exit;
                }
                $order->set($data);
                if($order->save() && !empty($events))
                {
                    foreach ($events as $event)
                    {
                        $order_history = Model_History::forge(array(
                            'order_id' => $order->id,
                            'event' => $event,
                        ));
                        $order_history->save();
                    }
                }

                \Messages::success('Order successfully updated.');
                
                if(\Input::post('status') == 'shipped')
                    $this->sendNoticeShipped(\Order\Model_Order::find_one_by_id($id));
                else if(\Input::post('status') == 'received')
                    $this->sendNoticeReceived(\Order\Model_Order::find_one_by_id($id));
                else if(\Input::post('status') == 'invoiced')
                    $this->sendNoticeInvoiced(\Order\Model_Order::find_one_by_id($id));
                
                // if(\Input::post('exit')) \Response::redirect(\Uri::create('admin/order/list'));
                // \Response::redirect(\Uri::admin('current'));
                \Response::redirect(\Uri::create('admin/order/list'));
            }
            catch (\Database_Exception $e)
            {
                \Messages::error('<strong>' . 'There was an error while trying to update order.' . '</strong>');
            }
            // }
            
            // if(\Input::post('customer_details', false))
            // {
            $val = Model_Order::validate('update');
            if($val->run()){
            
            try
            {
                $post_data = \Input::post();
                $hold_delivery_datetime = NULL;
                if(\Input::post('delivery_date'))
                {
                    $delivery_date = \Input::post('delivery_date');

                    $parts = explode('/', $delivery_date);
                    $delivery_date_value = $parts[1].'/'.$parts[0].'/'.$parts[2];

                    // rearrange time list
                    $hold_time_list_arr = explode(';', \Input::post('delivery_datetime_list'));
                    sort($hold_time_list_arr);
                    $hold_time_list_arr_sort = implode(';', $hold_time_list_arr);

                    $post_data['delivery_datetime_list'] = \Input::post('delivery_datetime_list')?$hold_time_list_arr_sort:NULL;

                    $hold_delivery_datetime = date('Y-m-d H:i', strtotime($delivery_date_value.' '.$hold_time_list_arr[0]));
                    $post_data = $post_data + array('delivery_datetime' => $hold_delivery_datetime);
                }

                $order->set($post_data);
                
                if($order->save())
                {
                    $order_history = Model_History::forge(array(
                        'order_id' => $order->id,
                        'event' => 'Order edited.',
                    ));
                    $order_history->save();
                        
                    // \Messages::success('Order successfully updated.');
                
                    //if(\Input::post('exit')) \Response::redirect(\Uri::create('admin/order/list'));
                    //\Response::redirect(\Uri::admin('current'));
                     \Response::redirect(\Uri::create('admin/order/list'));
                }
            }
            catch (\Database_Exception $e)
            {
                \Messages::error('<strong>' . 'There was an error while trying to update order.' . '</strong>');
            }
            }
            else
            {
                // Delete uploaded images if there is news saving error
                if(isset($this->_image_data))
                    foreach($this->_image_data as $image_data)
                        $this->delete_image($image_data['saved_as']);
                        
                if($val->error() != array())
                {
                    // show validation errors
                    \Messages::error('<strong>There was an error while trying to update member</strong>');
                    foreach($val->error() as $e)
                    {
                        \Messages::error($e->get_message());
                    }
                }
            }
        }
        
        if(\Input::post('upload_type', false) && \Input::post('upload', false))
        {
            $this->uploads($id);
        }
        
        if(\Input::post('order_edit', false))
        {
            $post = \Input::post();
            
            \Messages::success('Order successfully updated.');
            //if(\Input::post('exit')) \Response::redirect(\Uri::create('admin/order/list'));
            //\Response::redirect(\Uri::admin('current'));
            \Response::redirect(\Uri::create('admin/order/list'));

        }
        
        $user = $this->get_user($order->user_id);
        
        $this_class = $this;
        $get_user = function($user_id) use ($this_class)
        {
            return $this_class->get_user($user_id);
        };
        $get_attributes = function($attributes) use ($this_class)
        {
            return $this_class->attributes_to_string($attributes);
        };

        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($notes),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));
        
        \Theme::instance()->set_partial('content', $this->view_dir . 'update')
                ->set(compact('user', 'order'), null, false)
                ->set('get_user', $get_user, false)
                ->set('notes', $notes, false)
                ->set('get_attributes', $get_attributes, false)
                ->set('pagination', $pagination, false)
                ->set('settings', $settings, false);
    }
    
    protected function sendNoticeShipped($order)
    {        
        $settings = \Config::load('autoresponder.db');

        // Add cc email
        $cc_email = array();
        if($order->user_id)
        {
            $user = \Sentry::user((int)$order->user_id);
            // get user cc email
            if($user->get('metadata.cc_email'))
                $cc_email[] = $user->get('metadata.cc_email');
            // get user company email
            if($company_id = (int)$user->get('metadata.company'))
            {
                $user_company = \Company\Model_Company::find_one_by_id($company_id);
                $cc_email[] = $user_company->company_email;
            }
        }
        // End - Add cc email

        $email = \Email::forge();
        $email->to($order->email, $order->first_name.' '.$order->last_name);
        $email->subject('Order Shipped from '.$settings['company_name']);
        $email->from($settings['email_address'], $settings['company_name']);
        if($cc_email)
            $email->cc($cc_email);
        
        $emailView = \Theme::instance()->view('views/_email/order')->set('content', $order, false);
        $email->html_body($emailView);
        
        try
        {
            $email->send();
        }
        catch(\Exception $e)
        {
            if(\Fuel::$env == 'development')
                throw new \Exception($e->getMessage());
            else 
                return false;
        }
        return true;
    }
    protected function sendNoticeReceived($order)
    {
        $autoresponder = \Autoresponder\Autoresponder::forge();
        $autoresponder->view_admin = 'received';
        $content['subject'] = 'Received Order';
        $content['order_id'] = $order->id;
        // Add cc email
        $content['cc_email'] = array();
        if($order->user_id)
        {
            $user = \Sentry::user((int)$order->user_id);
            // get user cc email
            if($user->get('metadata.cc_email'))
                $content['cc_email'][] = $user->get('metadata.cc_email');
            // get user company email
            if($company_id = (int)$user->get('metadata.company'))
            {
                $user_company = \Company\Model_Company::find_one_by_id($company_id);
                $content['cc_email'][] = $user_company->company_email;
            }
        }
        // End - Add cc email
        $autoresponder->autoresponder_admin($content, array($order->email));
        
        try
        {
            $autoresponder->send();
        }
        catch(\Exception $e)
        {
            if(\Fuel::$env == 'development')
                throw new \Exception($e->getMessage());
            else 
                return false;
        }
        return true;
    }
    protected function sendNoticeInvoiced($order)
    {
        $settings = \Config::load('autoresponder.db');
        // check if Delay sending invoice has value
        $hold_delay_sending_invoice = (isset($settings['delay_sending_invoice']) ? intval($settings['delay_sending_invoice']) : 0);
        if($hold_delay_sending_invoice)
        {
            // delay sending of invoice. save to "delayed_actions" table
            // id
            // item_id
            // action = send_invoice
            $delayed_actions = \Settings\Model_Delayedactions::forge();
            $delayed_actions->item_id = $order->id;
            $delayed_actions->action = 'send_invoice';
            $delayed_actions->created_at = date('Y-m-d H:i:s');
            $delayed_actions->save();
        }
        else
        {
            // directly send invoice if no delay set
            $this->pdf_send_email($order->id, 'invoice');
        }
    }
    
    public function action_delete_file($type = false, $order_id = false)
    {
        if($order = \Order\Model_Order::find_one_by_id($order_id))
        {
            $name = $order->delivery_docket_file;
            $this->delete_file($name);
            
            $order->set(array(
                'delivery_docket_file' => null,
                'delivery_docket_title' => null,
            ));
            $order->save();
            
            \Messages::success('File successfully deleted.');
            \Response::redirect(\Input::referrer(\Uri::admin('current')));
        }
    }
    
     public function action_delete_history($id = false)
    {
        if($order_history = Model_History::find_one_by_id($id))
        {
            try 
            {
               if($order_history->delete())
               {
                    \Messages::success('Order history event successfully deleted.');
               }
               else
               {
                    \Messages::error('<strong>' . 'There was an error while trying to delete order history event.' . '</strong>');
               }
            }
            catch (\Database_Exception $e)
            {
                \Messages::error('<strong>' . 'There was an error while trying to delete order history event.' . '</strong>');
            }
        }
        else
        {
             \Messages::error('<strong>' . 'There is no order history event with this ID.' . '</strong>');
        }
        
        \Response::redirect(\Input::referrer(\Uri::admin('current')));
    }
    
    protected function delete_file($name = false)
    {
        if($name && \Config::get('details.file.location.root', false))
        {
            @unlink(\Config::get('details.file.location.root') . $name);
        }
    }
    
    protected function uploads($order_id = false)
    {
        if(!in_array(\Input::post('upload_type'), array('invoice', 'delivery_docket')))
        {
            return;
        }
        else
        {
            $type = \Input::post('upload_type');
        }
        $post = \Input::post();
        
        if($type && $order = \Order\Model_Order::find_one_by_id($order_id))
        {
            
            $old_file = $order->delivery_docket_file;
            if($this->file_upload($type))
            {
                if(isset($this->uploaded_files))
                {
                    $first_file = reset($this->uploaded_files);
                    try
                    {
                        $order->set(array(
                            'delivery_docket_file' => $first_file['saved_as'],
                            'delivery_docket_title' => $post[$type . '_title'],
                        ));
                        if($order->save())
                        {
                            $this->delete_file($old_file);
                            
                            \Response::redirect(\Uri::admin('current'));
                        }
                    }
                    catch (\Database_Exception $e)
                    {
                        $this->delete_file($first_file['saved_as']);
                        \Messages::error('<strong>' . 'There was an error while trying to upload file.' . '</strong>');
                        \Response::redirect(\Uri::admin('current'));
                    }
                }
            }
        }
        
    }
    
    protected function file_upload()
    {        
        // File upload configuration
        $this->file_upload_config = array(
            'path' => \Config::get('details.file.location.root'),
            'normalize' => true,
            'ext_whitelist' => array('pdf', 'xls', 'xlsx', 'doc', 'docx', 'txt'),
        );
    
        // process the uploaded files in $_FILES
        \Upload::process($this->file_upload_config);

        // if there are any valid files
        if (\Upload::is_valid())
        {
            // save them according to the config
            \Upload::save();
            \Messages::success('File successfully uploaded.');

            $this->uploaded_files = \Upload::get_files();

            return true;
        }
        else
        {
            // FILE ERRORS
            if(\Upload::get_errors() !== array())
            {
                foreach (\Upload::get_errors() as $file)
                {
                    foreach($file['errors'] as $key => $value)
                    {
                        \Messages::error($value['message']);
                    }
                }
                
                \Response::redirect(\Uri::admin('current'));
            }
        } 
        
        return false;
        
    }
    
    public function action_send_email($user_id = false, $type = false,  $order_id = false)
    {
        if($order = \Order\Model_Order::find_one_by_id($order_id))
        {
            $this->send_email($user_id, $type, $order, false);
            
            $master_user_id = $this->master_user($user_id);
            
            if($master_user_id && $master_user_id != $user_id) $this->send_email($master_user_id, $type, $order, $user_id);
        }
        else
        {
           \Messages::error('<strong>' . 'There was an error while sending email.' . '</strong>');
        }
        
        \Response::redirect(\Input::referrer(\Uri::create('admin/order/list'))); 
        
    }
    
    public function get_user($user_id = false)
    {
        if (\SentryAdmin::user_exists((int)$user_id))
        {
            return \SentryAdmin::user((int)$user_id);
        }
        return false;
    }
    
    public function attributes_to_string($attributes = false)
    {
        $out = false;
        if(json_decode($attributes, true) != null)
        {
            $attributes = json_decode($attributes, true);
        }
        if(is_array($attributes))
        {
            foreach($attributes as $key => $value)
            {
                $out[] = $key . ': ' . $value;
            }
        }
        if($out) $out = implode(' | ', $out); 
        return $out;
    }
    
    protected function master_user($user_id = false)
    {
        if(!$user = \SentryAdmin::user((int)$user_id))
        {
            return false;
        }
        
        if($user->get('metadata.master') == 1) return $user_id;
        
        $user_group = $user->groups();
        $user_group = $user_group[0];
        
        $users = \SentryAdmin::group($user_group['id'])->users();
        
        foreach ($users as $user_item)
        {
            if($user_tmp = \SentryAdmin::user((int)$user_item['id']))
            {
                if($user_tmp->get('metadata.master') == 1) return $user_tmp->get('id');
            }
        }
        
        return false;
        
    }
    
    protected  function send_email($user_id = false, $type = false, $order = false, $master = false)
    {
        $auto_response_emails = \Config::load('autoresponder.db');
        
        if(!$user = \SentryAdmin::user((int)$user_id))
        {
            return false;
        }
        
        $master_user = false;
        if(is_numeric($master))
        {
            $master_user = \SentryAdmin::user((int)$master);
        }
        
        $email_data['site_title'] = \Config::get('site_title');
        $email_data['order'] = $order;
        $email_data['type'] = $type;
        $email_data['master_user'] = $master_user;
        
        // Set a subject
        // order_status  ppi_delivery_date  invoice  delivery_docket
        
        switch ($type)
        {
            case 'order_status':
                if($order->status == 'in_progress')
                {
                    $subject = 'Order ' . $order->id  . ' status is in progress';
                }
                elseif($order->status == 'closed')
                {
                    $subject = 'Order ' . $order->id  . ' has been closed';
                }
                else
                {
                    $subject = 'Order ' . $order->id  . ' status is in progress';
                }
                
                break;
            
            case 'ppi_delivery_date':
                $subject = 'Order ' . $order->id  . ' delivery date';
                break;
            
            case 'invoice':
                $subject = 'Invoice for order ' . $order->id;
                break;
            
            case 'delivery_docket':
                $subject = 'Delivery Docket for order ' . $order->id;
                break;
        }
        
        $email_data['subject'] = $subject;
        $email = \Email::forge();
        $email->from($auto_response_emails['email_address'], $auto_response_emails['company_name']);

        // Send to
        $email->to($user['email'], $user['metadata']['entity']);

        // Set a subject
        $email->subject($subject);

        //$email->bcc(\Config::get('orders_email'));

        // Set a html body message
        $email->html_body(\Theme::instance()->view('views/_email/' . $type, $email_data, false));

        if($order->delivery_docket_file && is_file(\Config::get('details.file.location.root') . $order->delivery_docket_file))
            $email->attach(DOCROOT.'media/documents/' .$order->delivery_docket_file);
        
        $this->email_procces($email);

    }
    
    /**
     * Delete Order
     * 
     * @param type $id  = Order ID
     */
    public function action_delete($id = false)
    {
        if(is_numeric($id))
        {
            // Get news item to edit
            if($item = \Order\Model_Order::find_one_by_id($id))
            {
                if($item->finished == 0)
                {
                    try{
                        $item_exists = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'order_id' => $id ) ) );
                        if($item_exists)
                        {
                            foreach ($item_exists as $product)
                            {
                                $product->delete();
                            }
                        }
                        $item->delete();
                        
                        \Messages::success('Order successfully deleted.');
                    }
                    catch (\Database_Exception $e)
                    {
                        \Messages::error('<strong>' . 'There was an error while trying to delete order' . '</strong>');
                    }
                }
                else
                {
                    // Delete other content data like images, files, etc.
                    if(!empty($item->payments))
                    {
                        foreach($item->payments as $payment)
                        {
                            $payment->delete();
                        }
                    }
                    
                    if(!empty($item->products))
                    {
                        foreach($item->products as $product)
                        {
                            $product->delete();
                        }
                    }
                    
                    if(!empty($item->history))
                    {
                        foreach($item->history as $event)
                        {
                            $event->delete();
                        }
                    }
                    
                    try{
                        $item->delete();
                        
                        \Messages::success('Order successfully deleted.');
                    }
                    catch (\Database_Exception $e)
                    {
                        \Messages::error('<strong>' . 'There was an error while trying to delete order' . '</strong>');
                    }
                }
            }
        }
        
        \Response::redirect(\Input::referrer(\Uri::create('admin/order/list')));
    }
    
    /**
     * Delete Order Product
     * 
     * @param type $id  = Product ID
     */
    public function action_delete_product($order_id = false, $product_id = false)
    {
        if(is_numeric($order_id) && is_numeric($product_id))
        {
            if($order = \Order\Model_Order::find_one_by_id($order_id))
            {
                if(count($order->products) > 1)
                {
                    // Get product item to delete
                    if($product = \Order\Model_Products::find_one_by_id($product_id))
                    {
                        try{
                            
                            if($artworks = $product->artwork)
                            {
                                $ysi = \Yousendit\Base::forge();
                                $ysi->login();
                        
                                foreach ($artworks as $artwork)
                                {
                                    $ysi->delete_artwork($artwork->file_id);
                                }
                            }
                            
                            $product->delete();

                            \Messages::success('Order product successfully deleted.');
                        }
                        catch (\Database_Exception $e)
                        {
                            \Messages::error('There was an error while trying to delete order product');
                        }
                    }
                }
                else
                {
                    \Messages::error('You can\'t delete all order products. Please delete order instead.');
                }
                
                \Order\Model_Order::recalculate_order($order_id);
            }
        }
        
        if(is_numeric($order_id))
            \Response::redirect(\Input::referrer(\Uri::create('admin/order/update' . $order_id)));
        else
            \Response::redirect(\Input::referrer(\Uri::create('admin/order/list')));
    }
    
    /**
     * Get enabled payments
     *
     * @access  public
     * @param   string   $get = Get payments that are: enabled | disabled | all
     * @param   string   $load_config = Load payments config
     * @return  array
     *
     */
    public function get_payments($get = 'enabled', $load_config = false)
    {
    
        $out = array();
        $path =  APPPATH . "modules" . DS . "payment" . DS . "config" . DS ;
        $config_files = \File::read_dir($path);
        if(!empty($config_files))
        {
            foreach ($config_files as $file)
            {
                $file_parts = pathinfo($file);
                $payment_config = include_once $path . $file;
                switch ($get)
                {
                    case 'enabled':
                        if($payment_config['enabled'])
                        {
                            $out[$payment_config['code']] = $payment_config['name'];
                            if($load_config) \Config::load($file_parts['filename'], true);
                        }
    
                        break;
    
                    case 'disabled':
                        if(!$payment_config['enabled'])
                        {
                            $out[$payment_config['code']] = $payment_config['name'];
                            if($load_config) \Config::load($file_parts['filename'], true);
                        }
    
                        break;
    
                    default:
                    case 'all':
                        $out[$payment_config['code']] = $payment_config['name'];
                        if($load_config) \Config::load($file_parts['filename'], true);
    
                }
            }
        }
    
        return $out;
    }
    
    protected function email_procces($email)
    {
        try
        {
            $email->send();
            \Messages::success('Email successfully sent.');
        }
        catch(\EmailValidationFailedException $e)
        {
            \Messages::error('<strong>' . 'There was an error while sending email.' . '</strong>');
        }
        catch(\EmailSendingFailedException $e)
        {
            \Messages::error('<strong>' . 'There was an error while sending email.' . '</strong>');
        }
    }
    
    public function action_pdf($order_id = false, $type = 'order')
    {
        if(!$order = \Order\Model_Order::find_one_by_id($order_id))
        {
            \Messages::error('There was an error while trying to create PDF for order ID: ' . $order_id);
            \Response::redirect(\Input::referrer(\Uri::create('admin/order/list')));
        }
       
        $notes = \User\Model_Notes::find_by('user_id',$order->user_id);
        
        \Package::load('Pdf');
        $pdf = \Pdf\Pdf::forge('tcpdf')->init('P', 'mm', 'A4', true, 'UTF-8', false);
        $settings = \Config::load('autoresponder.db');
                
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($settings['company_name']);

        switch ($type) {
            case 'invoice':
                $pdf->SetTitle('Tax Invoice '. $order_id);
                $pdf->SetSubject('Tax Invoice '. $order_id);
                break;
            case 'delivery-note':
                $pdf->SetTitle('Delivery Note '. $order_id);
                $pdf->SetSubject('Delivery Note '. $order_id);
                break;
            case 'packing-slip':
                $pdf->SetTitle('Packing Slip '. $order_id);
                $pdf->SetSubject('Packing Slip '. $order_id);
                break;
            default:
                $pdf->SetTitle('Order '. $order_id);
                $pdf->SetSubject('Order '. $order_id);
                break;
        }
        //$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetMargins(5, 10, 10);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
      
        // set font
        //$pdf->SetFont('times', 'BI', 20);

        // add a page
        $pdf->AddPage();
        
        // ---------------------------------------------------------
        
        $content['content']['content'] = $order;
        $content['content']['notes'] = $notes;
        
        switch ($type) {
            case 'invoice':
                $html = \Theme::instance()->view($this->view_dir . 'invoice/invoice', $content, false) ; break;
            case 'delivery-note':
                $html = \Theme::instance()->view($this->view_dir . 'invoice/delivery-note', $content, false) ; break;
            case 'packing-slip':
                $html = \Theme::instance()->view($this->view_dir . 'invoice/packing-slip', $content, false) ; break;
            default:
                $html = \Theme::instance()->view($this->view_dir . 'invoice/order', $content, false) ; break;
        }
        
        
        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        // ---------------------------------------------------------

        ob_end_clean();

        //Close and output PDF document
        $pdf->Output('Order '. $order_id .'.pdf', 'I');
    }

    public function action_pdf_send_email($order_id = false, $type = 'order')
    {
        if(!$order = \Order\Model_Order::find_one_by_id($order_id))
        {
            \Messages::error('There was an error while trying to create PDF for order ID: ' . $order_id);
            \Response::redirect(\Input::referrer(\Uri::create('admin/order/list')));
        }

        try
        {
            $this->pdf_send_email($order_id, $type);

            \Messages::success('Email successfully sent.');

            if(\Input::get('redirect') == 'order_list')
                \Response::redirect('admin/order/list');
            else
                \Response::redirect('admin/order/update/'.$order->id);
        }
        catch(\EmailValidationFailedException $e)
        {
            \Messages::error('<strong>' . 'There was an error while sending email.' . '</strong>');
        }
        catch(\EmailSendingFailedException $e)
        {
            \Messages::error('<strong>' . 'There was an error while sending email.' . '</strong>');
        }
    }

    public function pdf_send_email($order_id = false, $type = 'order')
    {
        if($order = \Order\Model_Order::find_one_by_id($order_id))
        {
            $notes = \User\Model_Notes::find_by('user_id',$order->user_id);

            \Package::load('Pdf');
            $pdf = \Pdf\Pdf::forge('tcpdf')->init('P', 'mm', 'A4', true, 'UTF-8', false);
            $settings = \Config::load('autoresponder.db');
            
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor($settings['company_name']);

            switch ($type) {
                case 'invoice':
                    $pdf->SetTitle('Tax Invoice '. $order_id);
                    $pdf->SetSubject('Tax Invoice '. $order_id);
                    break;
                case 'delivery-note':
                    $pdf->SetTitle('Delivery Note '. $order_id);
                    $pdf->SetSubject('Delivery Note '. $order_id);
                    break;
                case 'packing-slip':
                    $pdf->SetTitle('Packing Slip '. $order_id);
                    $pdf->SetSubject('Packing Slip '. $order_id);
                    break;
                default:
                    $pdf->SetTitle('Order '. $order_id);
                    $pdf->SetSubject('Order '. $order_id);
                    break;
            }
            //$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

            // remove default header/footer
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetMargins(5, 10, 10);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
          
            // set font
            //$pdf->SetFont('times', 'BI', 20);

            // add a page
            $pdf->AddPage();
            
            // ---------------------------------------------------------
            
            $content['content']['content'] = $order;
            $content['content']['notes'] = $notes;

            $email_subject = (\Input::post('email_subject')) ? ucwords(\Input::post('email_subject')).' - ' : '';
            $subject = $email_subject.'Order';
            
            switch ($type) {
                case 'invoice':
                    $subject = $email_subject.'Tax Invoice';
                    $html = \Theme::instance()->view($this->view_dir . 'invoice/invoice', $content, false) ;
                    break;
                case 'delivery-note':
                    $subject = $email_subject.'Delivery Note';
                    $html = \Theme::instance()->view($this->view_dir . 'invoice/delivery-note', $content, false) ;
                    break;
                case 'packing-slip':
                    $subject = $email_subject.'Packing Slip';
                    $html = \Theme::instance()->view($this->view_dir . 'invoice/packing-slip', $content, false) ;
                    break;
                default:
                    $subject = $email_subject.'Order';
                    $html = \Theme::instance()->view($this->view_dir . 'invoice/order', $content, false) ;
                    break;
            }
            
            
            // output the HTML content
            $pdf->writeHTML($html, true, false, true, false, '');

            // ---------------------------------------------------------

            ob_end_clean();

            //Close and output PDF document
            $save_to = DOCROOT.'media/pdf/'.$subject.' '. $order_id .'.pdf';
            $pdf->Output($save_to, 'F');

            $settings = \Config::load('autoresponder.db'); 

            $email = \Email::forge();

            if($email_from = \Input::post('email_from'))
            {
                if (strpos($email_from, '<') !== false)
                {
                    $emails = explode('<', $email_from);
                    $name = trim($emails[0]);
                    $email_hold = str_replace('>', '', (trim($emails[1])));
                    if($email_hold)
                        $email->from($email_hold, $name);
                    else
                        $email->from($settings['email_address'], $settings['company_name']);
                }
                else
                    $email->from($email_from);
            }
            else
                $email->from($settings['email_address'], $settings['company_name']);

            $email->to(\Input::post('email_to')?explode(',', str_replace(' ', '', \Input::post('email_to'))):$order->email);

            if(\Input::post('email_cc')){
                 $email->cc(explode(',', str_replace(' ', '', \Input::post('email_cc'))));
            }

            $email_data = array();

            $email_data['order'] = $order;
            $email_data['type'] = $subject;
            $email_data['message'] = \Input::post('message');

            $email->html_body(\Theme::instance()->view('views/_email/downloadable', $email_data, false));

            $email->subject($subject.' '.$order->id);
            
            $email->attach($save_to);

            $email->send();
            @unlink($save_to); 
        }
    }
    
    public function action_get_states($country = false)
    {
        $out = '<option value="">-</option>';
        $states = \App\States::forge();
        if($states_array = $states->getStateProvinceArray($country))
        {
            $out = '';
            foreach ($states_array as $key => $value)
            {
                $out .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        echo $out;
        exit;
    }
    
    public function action_download_artwork($file_id = null)
    {
        $ysi = \Yousendit\Base::forge();
        $ysi->login();
        
        $response = $ysi->file_info($file_id);
        
        if(isset($response['data']['download_url']))
        {
            $ysi->downloada_file($response['data']['download_url']);
        }
        else
        {
            echo '<h3>There was an error while downloading file.</h3>';
            echo 'Error code: '. $response['errorcode'] . '<br>';
            echo 'Error message: '. $response['errormessage'] . '<br>';
        }
        
        exit;
    }
    
    public function action_download_artwork_url($file_id = null)
    {
        $ysi = \Yousendit\Base::forge();
        $ysi->login();
        
        $response = $ysi->file_info($file_id);
        
        if(isset($response['data']['clickable_download_url']))
        {
            header("Location: " . $response['data']['clickable_download_url']);
            exit;
        }
        else
        {
            echo '<h3>There was an error while downloading file.</h3>';
            echo 'Error code: '. $response['errorcode'] . '<br>';
            echo 'Error message: '. $response['errormessage'] . '<br>';
        }
        
        exit;
    }
    
    public function action_delete_artwork($file_id = null)
    {
        $ysi = \Yousendit\Base::forge();
        $response = $ysi->delete_artwork($file_id, true);
        
        if(isset($response['errormessage']))
        {
             \Messages::error('<strong>' . 'There was an error while trying to delete artwork file.' . '</strong>');
        }
        else
        {
             \Messages::success('Artwork file successfully deleted.');
        }
        
        \Response::redirect(\Input::referrer(\Uri::create('admin/order/list')));
    }
    
    public function action_abandon_cart()
    {
        \View::set_global('menu', 'admin/order/abandon_cart');
        \View::set_global('title', 'List Abandoned Carts');
        
        $search = $this->get_search_items_abandon();
        
        $items      = $search['items'];
        $pagination = $search['pagination'];
        
        \Theme::instance()->set_partial('content', $this->view_dir . 'abandon_cart')
            ->set('items', $items)
            ->set('pagination', $pagination, false);
    }
    
    public function get_search_items_abandon($user_id = false)
    {
        // Override group_id if its a search
        $user_id = \Input::get('user_id', $user_id);
        
        if($user_id && \SentryAdmin::user_exists((int)$user_id))
        {
            $user = \SentryAdmin::user((int)$user_id);
        }
        
        $items = \Order\Model_Order::find(function($query){

            if(isset($user))
                $query->where('user_id', $user->id);

            // check if order has product in order_abandon_cart_products table
            $query->where(\DB::expr('(SELECT count(*) FROM order_abandon_cart_products WHERE orders.id = order_abandon_cart_products.order_id)'), '>', 0);

            $query->where('finished', '0');
            $query->order_by('id', 'desc');
            
        });

        foreach(\Input::get() as $key => $value)
        {
            if(!empty($value) || $value == '0')
            {
                switch($key)
                {
                    case 'title':
                        foreach($items as $number => $item)
                        {
                            $full_name = '';
                            $email = '';
                            if((int)$item->user_id)
                            {
                                $user = \SentryAdmin::user((int)$item->user_id);
                                $full_name = $user->get('metadata.first_name') . ' ' . $user->get('metadata.last_name');
                                $email = $user->email;
                            }
                            if(stripos($full_name, $value) === false && stripos($email, $value) === false && stripos($item->id, $value) === false) unset($items[$number]);
                            if(stripos($full_name, $value) === false && stripos($email, $value) === false && stripos($item->id, $value) === false) unset($items[$number]);
                        }
                    break;
                }
            }
        }
        
        // Reset to empty array if there are no result found by query
        if(is_null($items)) $items = array();
        
        // Initiate pagination
        $pagination = \Hybrid\Pagination::make(array(
            'total_items' => count($items),
            'per_page' => \Input::get('per_page', 10),
            'uri_segment' => null,
        ));

        // Remove unwanted items, and show only required ones
        $items = array_slice($items, $pagination->offset, $pagination->per_page);
        
        return array('items' => $items, 'pagination' => $pagination);
    }

    public function action_abandon_cart_view($id)
    {
        if(!$order = \Order\Model_Order::find_one_by_id($id)) \Response::redirect('admin/order/abandon_cart');
        \View::set_global('menu', 'admin/order/abandon_cart');
        \View::set_global('title', 'Edit Abandoned Cart');
        $user = false;
        if((int)$order->user_id)
            $user = \SentryAdmin::user((int)$order->user_id);

        $products = \Order\Model_Abandon_Cart_Products::find( array('where' => array( 'order_id' => $order->id ) ) );
        
        $this_class = $this;
        $get_user = function($user_id) use ($this_class)
        {
            return $this_class->get_user($user_id);
        };
        $get_attributes = function($attributes) use ($this_class)
        {
            return $this_class->attributes_to_string($attributes);
        };

        $order = \Order\Model_Order::forge();
        $shipping_price = $order->shipping_price(null, null, true);

        \Theme::instance()->set_partial('content', $this->view_dir . 'abandon_cart_view')
                ->set('order', $order, false)
                ->set('products', $products, false)
                ->set('user', $user, false)
                ->set('shipping_price', $shipping_price)
                ->set('get_attributes', $get_attributes, false);
    }

    public function action_duplicate($path = false,$order_id = false){
        
        $order_info = \Order\Model_Order::find_one_by_id($order_id);

        $order_data = array();
       
        foreach($order_info as $key => $value){
            if($key == 'id') continue;
            if(!is_object($value)){
                $order_data[$key] = $value;
            }
            
        }
        
        $order = \Order\Model_Order::forge($order_data);
        $order_products_dat = \Order\Model_Products::find_by('order_id',$order_info->id);

        if(empty($order_products_dat)){
            \Messages::error('Nothing to duplicate.');
                
            if($path == 'orderhistory'){
                \Response::redirect('admin/user/orderhistory/'.$order_info->user_id);
            }else{
                 \Response::redirect('admin/order/list');
            }
        }
        else{
            $op_data = array();
            $all_op_data = array();
            $count = 0;
            $order->save();
            foreach ($order_products_dat as $key => $value) {
                foreach ($value as $k => $v) {
                    $op_data['order_id'] = $order->id;
                    if($k == 'id') continue;
                    if(!is_object($v)){
                        $op_data[$k] = $v;
                    }
                }
                array_push($all_op_data, $op_data);
                
            }
          
            for($x=0;$x<sizeof($all_op_data);$x++){
                $order_products = \Order\Model_Products::forge($all_op_data[$x]);
                $order_products->save();
                $count++;
            }
            
            
            
            if($count == sizeof($all_op_data)){
                
                \Messages::success('Duplicate order success.');
                
                if($path == 'orderhistory'){
                    \Response::redirect('admin/user/orderhistory/'.$order_info->user_id);
                }else{
                    \Response::redirect('admin/order/list');
                }
                
            }
        }
    }

    public function action_update_status($order_id = false)
    {
        $order = \Order\Model_Order::find_one_by_id($order_id);

        if($order && \Input::get('status'))
        {
            $event = 'Status changed. ' . \Inflector::humanize($order->status) . ' => ' . \Inflector::humanize(\Input::get('status'));

            $data['status'] = \Input::get('status');
            $order->set($data);

            if($order->save())
            {
                $order_history = Model_History::forge(array(
                    'order_id' => $order->id,
                    'event' => $event,
                ));
                $order_history->save();
            }

            if(\Input::get('status') == 'shipped')
                $this->sendNoticeShipped($order);
            else if(\Input::get('status') == 'received')
                $this->sendNoticeReceived($order);
            else if(\Input::get('status') == 'invoiced')
                $this->sendNoticeInvoiced($order);
            \Messages::success('Successfully updated status.');

        }
        else
        {
            \Messages::error('<strong>There was an error while trying to update status</strong>');
        }
        \Response::redirect('admin/order/list');
    }

    public function action_get_attr($id = false){
        
        if($id)
        {
            $aproduct = \Product\Model_Product::find_by('id',$id);
            $aproduct = ($aproduct) ? $aproduct[0] : '';
            $aproduct->datas = $aproduct->data;

            $pack_options = false;
            if($aproduct->product_type == 'pack')
            {
                $pack_list = array();
                $product_id = $aproduct->id;
                $get_packs = \Product\Model_Packs::find(function($query) use ($product_id) {
                    $query->where('product_id', $product_id);
                    $query->order_by('id', 'asc');
                });

                if($get_packs)
                {
                    foreach ($get_packs as $pack)
                    {
                        array_push($pack_list, array(
                            'id' => $pack->id,
                            'name' => $pack->name,
                            'description' => $pack->description,
                            'type' => $pack->type,
                            'selection_limit' => $pack->selection_limit,
                            'products' => explode(',', $pack->products),
                        ));
                    }
                }

                if($packs = $pack_list)
                {
                    $pack_options = array();
                    foreach($packs as $key => $pack)
                    {
                        $products = array();
                        if(isset($pack['products']))
                        {
                            foreach ($pack['products'] as $key => $pack_product)
                            {
                                if($get_item = \Product\Model_Product::find_one_by_id($pack_product))
                                {
                                    $product_data = $get_item->data;
                                    $get_default_select = array();
                                    if(isset($get_item->data['sx']))
                                    {
                                        foreach($get_item->data['sx'] as $num => $sx)
                                        {
                                            if(isset($sx) && isset($get_item->data['select'][$sx]))
                                            {
                                                if(!empty($get_item->data['select'][$sx]))
                                                {
                                                    array_push($get_default_select, $sx.':'.$get_item->data['current_attributes'][$num]->option->id);
                                                }
                                            }
                                        }
                                    }
                                    array_push($products, array(
                                        'id' => $pack_product,
                                        'title' => $get_item->title,
                                        'minimum_order' => $get_item->minimum_order?:1,
                                        'price' => $product_data['price']?$product_data['price']:$product_data['retail_price'],
                                        'code' => $get_item->code,
                                        'select' => $get_default_select?implode(',',$get_default_select):'',
                                    ));
                                }
                            }
                        }
                        array_push($pack_options, array(
                            'id' => $pack['id'],
                            'name' => $pack['name'],
                            'type' => $pack['type'],
                            'selection_limit' => $pack['selection_limit'],
                            'products' => $products,
                        ));
                    }
                }
            }

            echo json_encode(array(
                    'attributes' => $aproduct,
                    'pack_options' => $pack_options,
                ));
        }
        exit;
    }

    public function action_paypal($id = false)
    {
        if(!$id)
            $id = \Session::get('order.id');

        if((int)$id)
        {
            $processPayment = new \PaymentProccess\PaymentProccessTypePaypal(array('payment_type' => 'paypal'), \Order\Model_Order::find_one_by_id($id));
            $failed = $processPayment->notify();

            if ($failed)
            {
                \Messages::error('There was an error in processing your payment.');
                \Response::redirect(\Uri::create('admin/order/list'));
            }
        }
        else
        {
            \Messages::error('Order not found');
            \Response::redirect(\Uri::create('admin/order/list'));
        }
    }
    
    public function action_finalise_order($id = false)
    {
        if(!$id)
            $id = \Session::get('order.id');

        if((int)$id)
        {
            $order = \Order\Model_Order::find_one_by_id($id);
            if ($order)
            {
                try
                {
                    $order->set(array('accepted' => 1));
                    $order->save();


                    if($get_last_payment = $order->last_payment)
                    {
                        if($get_last_payment->partial_payment)
                        {
                            //\Response::redirect(\Uri::create('admin/order/edit/'.$order->id.'?step=3'));
                            \Response::redirect(\Uri::create('admin/order/list'));

                        }
                        else
                        {
                            if($user = \Sentry::user((int)$order->user_id))
                                $this->autoresponder($user, $order);
                            //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                            \Response::redirect(\Uri::create('admin/order/list'));
                        }
                    }
                }
                catch (\Database_Exception $e)
                {
                    // show validation errors
                    \Messages::error('There was an error while trying to save order.');

                    // Uncomment lines below to show database errors
                    $errors = $e->getMessage();
                    \Messages::error($errors);

                    \Response::redirect(\Uri::create('admin/order/list'));
                }   
            }
            else
            {
                \Messages::error('Order not found');
                \Response::redirect(\Uri::create('admin/order/list'));
            }
        }
        else
        {
            \Messages::error('Order not found');
            \Response::redirect(\Uri::create('admin/order/list'));
        }
    }
    
    public function action_cancel_order($id = false)
    {
        if(!$id)
            $id = \Session::get('order.id');

        if((int)$id)
        {
            $order = \Order\Model_Order::find_one_by_id($id);
            if ($order)
            {
                \Messages::error('Payment cancelled.');
                \Response::redirect(\Uri::create('admin/order/update/'. $id));
            }
            else
            {
                \Messages::error('Order not found');
                \Response::redirect(\Uri::create('admin/order/list'));
            }
        }
        else
        {
            \Messages::error('Order not found');
            \Response::redirect(\Uri::create('admin/order/list'));
        }
    }

    public function action_return_securepay($id = false)
    {
        $settings = \Config::load('securePay.db');
        $hold_code = (isset($settings['code']) ? $settings['code'] : 'securepay');

        $this->payment_return($id, $hold_code);
    }

    public function payment_return($id, $code)
    {
        if(!$id)
            $id = \Session::get('order.id');

        if((int)$id)
        {
            $order = \Order\Model_Order::find_one_by_id($id);
            if ($order)
            {
                if(isset(\Input::get()['restext']))
                {
                    if(\Input::get()['restext'] == 'Approved')
                    {
                        try
                        {
                            $paymentSaver = new \PaymentProccess\PaymentSaver();
                            $paymentSaver->savePaymentDetails($code, $order, 'Completed', 'Transaction completed', \Input::get()['restext']);
                            
                            $order->set(array('accepted' => 1));
                            $order->save();

                            if($get_last_payment = $order->last_payment)
                            {
                                if($get_last_payment->partial_payment)
                                {
                                    //\Response::redirect(\Uri::create('admin/order/edit/'.$order->id.'?step=3'));
                                    \Response::redirect(\Uri::create('admin/order/list'));
                                }
                                else
                                {
                                    if($user = \Sentry::user((int)$order->user_id))
                                        $this->autoresponder($user, $order);
                                    //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                                    \Response::redirect(\Uri::create('admin/order/list'));
                                }
                            }
                        }
                        catch (\Database_Exception $e)
                        {
                            // show validation errors
                            \Messages::error('There was an error while trying to save order.');

                            // Uncomment lines below to show database errors
                            $errors = $e->getMessage();
                            \Messages::error($errors);

                            \Response::redirect(\Uri::create('admin/order/list'));
                        }
                    }
                    else
                    {
                        $paymentSaver = new \PaymentProccess\PaymentSaver();
                        $paymentSaver->savePaymentDetails($code, $order, 'Failed', 'Transaction failed', 'Transaction failed');
                        \Messages::error('Transaction failed');

                        if($get_last_payment = $order->last_payment)
                        {
                            if($get_last_payment->partial_payment)
                            {
                                //\Response::redirect(\Uri::create('admin/order/edit/'.$order->id.'?step=3'));
                                \Response::redirect(\Uri::create('admin/order/list'));
                            }
                            else
                            {
                                //\Response::redirect(\Uri::create('admin/order/update/'.$order->id));
                                \Response::redirect(\Uri::create('admin/order/list'));
                            }
                        }
                        else
                        {
                            //\Response::redirect(\Uri::create('admin/order/update/'.$id));
                            \Response::redirect(\Uri::create('admin/order/list'));
                        }
                    }
                }
            }
            else
            {
                \Messages::error('Order not found');
                \Response::redirect(\Uri::create('admin/order/list'));
            }
        }
        else
        {
            \Messages::error('Order not found');
            \Response::redirect(\Uri::create('admin/order/list'));
        }
    }

    public function action_delete_ppt($payment_id)
    {
        // delete only payment gateway from \Config::get('details.allow_delete_gateway_partial_payment_transaction', array())

        $payment = \Payment\Model_Payment::find_one_by_id($payment_id);
        if($payment)
        {
            if(in_array($payment->method, \Config::get('details.allow_delete_gateway_partial_payment_transaction', array())))
            {
                $payment->delete();
                \Messages::success("Successfully deleted transaction.");
            }
            else
            {
                \Messages::error("You cannot delete this transaction.");
            }
        }
        else
        {
            \Messages::error('Transaction not found');
        }

        \Response::redirect(\Input::referrer(\Uri::admin('current')));
    }

    public function action_import_to_myob($user_id = false)
    {
        // check if enabled myob
        $accounting_api_settings = \Config::load('accounting_api.db');
        $accounting_api = (isset($accounting_api_settings['accounting_api']) ? $accounting_api_settings['accounting_api'] : '');

        if($accounting_api != 'myob')
        {
            \Messages::error("MYOB is disabled.");
            \Response::redirect('admin/settings/accounting_api');
        }

        \View::set_global('menu', 'admin/order/list');
        \View::set_global('title', 'Import to MYOB');

        $settings = \Config::load('myob.db');

        if (\Input::post())
        {
            $input = \Input::post();
            $val = Model_Myob::validate('create');

            if($input['submit'] == 'auth')
            {
                $redirect_uri = \Uri::create('admin/order/process_import_to_myob');
                $api_key = (isset($settings['api_key']) ? $settings['api_key'] : '');
                $api_secret = (isset($settings['api_secret']) ? $settings['api_secret'] : '');
                $scope = \Config::get('myob.config.scope');
                $login_uri = \Config::get('myob.config.login_uri');
                header('Location: '.$login_uri.'?client_id='.$api_key.'&redirect_uri='.$redirect_uri.'&response_type=code&scope='.$scope);
                exit;
            }
            else if(!$val->run())
            {
                if($val->error() != array())
                {
                    // show validation errors
                    \Messages::error('<strong>There was an error while trying to update settings</strong>');
                    foreach($val->error() as $e)
                    {
                        \Messages::error($e->get_message());
                    }
                }

            }
            else
            {
                try
                {
                    $data =  array(
                        'name' => \Config::get('myob.name'),
                        'code' => \Config::get('myob.code'),
                        'api_key' => $input['api_key'],
                        'api_secret' => $input['api_secret']
                    );
                    \Config::save('myob.db', $data);
                }
                catch (\Database_Exception $e)
                {
                    // show validation errors
                    \Messages::error('<strong>There was an error while trying to update settings.</strong>');

                    // Uncomment lines below to show database errors
                    $errors = $e->getMessage();
                    \Messages::error($errors);
                }
            }
        }

        \Theme::instance()->set_partial('content', $this->view_dir . 'import_to_myob')
                ->set('settings', $settings);
    }

    public function action_process_import_to_myob()
    {
        // check if enabled myob
        $accounting_api_settings = \Config::load('accounting_api.db');
        $accounting_api = (isset($accounting_api_settings['accounting_api']) ? $accounting_api_settings['accounting_api'] : '');

        if($accounting_api != 'myob')
        {
            \Messages::error("MYOB is disabled.");
            \Response::redirect('admin/settings/accounting_api');
        }

        if( \Input::get('code') )
        {
            $settings = \Config::load('myob.db');

            // ideally you want to check more, eg: did the user really come from secure.myob.com?
            $api_access_code = \Input::get('code');
            $redirect_uri = \Uri::create('admin/order/process_import_to_myob');
            $api_key = (isset($settings['api_key']) ? $settings['api_key'] : '');
            $api_secret = (isset($settings['api_secret']) ? $settings['api_secret'] : '');
            $scope = \Config::get('myob.config.scope');
            $api_url = \Config::get('myob.config.api_url');

            // lets get the access token now
            // include our oauth class
            $oauth = new \App\MYOB_API_OAUTH();
            // getAccessToken would return false if there was an error
            $oauth_tokens = $oauth->getAccessToken($api_key, $api_secret, $redirect_uri, $api_access_code, $scope);
            //var_dump($oauth_tokens);
            if( isset($oauth_tokens->user) )
            {
                $api_url = $api_url;
                $access_token = $oauth_tokens->access_token;
                $expires_in = time() + $oauth_tokens->expires_in;
                $refresh_token = $oauth_tokens->refresh_token;

                $headers = array(
                    'Authorization: Bearer '.$access_token,
                    'x-myobapi-cftoken: '.base64_encode("Administrator:"),
                    'x-myobapi-key: '.$api_key,
                    'x-myobapi-version: v2'
                );

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $api_url);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_HEADER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                  
                // get the response & close the session
                $response = curl_exec($ch); 
                curl_close($ch); 
                // return what we got
                $company_files = json_decode($response, true);
                if($company_files)
                {
                    $search = $this->get_search_items(false, true);
                    $items      = $search['items'];

                    foreach ($company_files as $cf)
                    {
                        $cf_uri = $cf['Uri'];
                        $create_customer_uri = $cf_uri.'/Contact/Customer';
                        $check_order_uri = $cf_uri.'/Sale/Order';
                        $create_order_uri = $cf_uri.'/Sale/Order/Item';
                        $create_product_uri = $cf_uri.'/Inventory/Item';
                        $taxcode_uri = $cf_uri.'/GeneralLedger/TaxCode';
                        $account_uri = $cf_uri.'/GeneralLedger/Account';

                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL, $taxcode_uri);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                          
                        // get the response & close the session
                        $response = curl_exec($ch); 
                        curl_close($ch); 

                        $get_gst_uid = '';
                        if($taxcodes = json_decode($response, true))
                        {
                            foreach ($taxcodes["Items"] as $taxcode)
                            {
                                if($taxcode['Code'] == 'GST')
                                {
                                    $get_gst_uid = $taxcode['UID'];
                                    break;
                                }
                            }
                        }

                        $ch = curl_init();

                        curl_setopt($ch, CURLOPT_URL, $account_uri);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_HEADER, false);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                          
                        // get the response & close the session
                        $response = curl_exec($ch); 
                        curl_close($ch); 

                        $get_account_inventory = '';
                        if($accounts = json_decode($response, true))
                        {
                            foreach ($accounts["Items"] as $account)
                            {
                                if($account['Name'] == 'Inventory')
                                {
                                    $get_account_inventory = $account['UID'];
                                    break;
                                }
                            }
                        }


                        if($items)
                        {
                            foreach ($items as $order)
                            {
                                // check if order is existing
                                $ch = curl_init();

                                curl_setopt($ch, CURLOPT_URL, $check_order_uri);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_HEADER, false);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                  
                                // get the response & close the session
                                $response = curl_exec($ch); 
                                curl_close($ch);
                                $order_exsts = false;
                                if($get_orders = json_decode($response, true))
                                {
                                    foreach ($get_orders["Items"] as $ordr)
                                    {
                                        if($ordr['Number'] == $order->id)
                                        {
                                            $order_exsts = true;
                                            break;
                                        }
                                    }
                                }
                                if($order_exsts)
                                    continue;

                                $customer_display_id = strtoupper(substr($_SERVER['HTTP_HOST'], 0, 5)).$order->user_id;

                                // Create customer first
                                $postData = array(
                                    'LastName' => $order->first_name,
                                    'FirstName' => $order->last_name,
                                    'IsIndividual' => true,
                                    'DisplayID' => $customer_display_id,
                                    'IsActive' => true,
                                    'Addresses' => array(
                                        array(
                                            "Location" => 1,
                                            'Street' => $order->address,
                                            'City' => $order->suburb,
                                            'State' => $order->state,
                                            'PostCode' => $order->postcode,
                                            'Country' => ($order->country?:'AU'),
                                            'Phone1' => $order->phone,
                                            'Salutation' => $order->title,
                                            'Email' => $order->email
                                        )
                                    ),
                                    'SellingDetails' => array(
                                        "SaleLayout" => "NoDefault",
                                        'TaxCode' => array(
                                            "UID" => $get_gst_uid,
                                        ),
                                        'FreightTaxCode' => array(
                                            "UID" => $get_gst_uid,
                                        )
                                    )
                                );

                                $dataString = json_encode($postData);

                                $ch = curl_init();

                                curl_setopt($ch, CURLOPT_URL, $create_customer_uri);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                                  
                                // get the response & close the session
                                $response = curl_exec($ch); 
                                curl_close($ch);


                                $product_list = array();
                                $total = 0;
                                if($order->products)
                                {
                                    foreach ($order->products as $product)
                                    {
                                        $total += ($product->price * $product->quantity);

                                        $get_product = \Product\Model_Product::find_one_by_id($product->product_id);

                                        $productData = array(
                                            "Number" => $product->product_id,
                                            "Name" => $get_product->title,
                                            "Description" => $get_product->description_full,
                                            "IsSold" => true,
                                            "IsInventoried" => true,
                                            'CostOfSalesAccount' => array(
                                                'UID' => $get_account_inventory
                                            ),
                                            'IncomeAccount' => array(
                                                'UID' => $get_account_inventory
                                            ),
                                            'AssetAccount' => array(
                                                'UID' => $get_account_inventory
                                            ),
                                            'SellingDetails' => array(
                                                'BaseSellingPrice' => $product->price,
                                                'ItemsPerBuyingUnit' => $get_product->minimum_order?:1,
                                                'TaxCode' => array(
                                                    'UID' => $get_gst_uid,
                                                ),
                                            )
                                        );
                                        $dataString = json_encode($productData);

                                        $ch = curl_init();

                                        curl_setopt($ch, CURLOPT_URL, $create_product_uri);
                                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                                          
                                        // get the response & close the session
                                        $response = curl_exec($ch); 
                                        curl_close($ch);


                                        $ch = curl_init();

                                        curl_setopt($ch, CURLOPT_URL, $create_product_uri);
                                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                        curl_setopt($ch, CURLOPT_HEADER, false);
                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                          
                                        // get the response & close the session
                                        $response = curl_exec($ch); 
                                        curl_close($ch); 

                                        if($inventories = json_decode($response, true))
                                        {
                                            foreach ($inventories["Items"] as $inventory)
                                            {
                                                if($inventory['Number'] == $product->product_id)
                                                {
                                                    $hold_product = array(
                                                        "Type" => "Transaction",
                                                        "ShipQuantity" => $product->quantity,
                                                        "UnitPrice" => round($product->price, 2),
                                                        "Total" => round(($product->price * $product->quantity), 2),
                                                        "Item" => array(
                                                            'UID' => $inventory['UID']
                                                        ),
                                                        "TaxCode" => array(
                                                            'UID' => $get_gst_uid,
                                                        )
                                                    );
                                                    array_push($product_list, $hold_product);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                $orderData = array(
                                    'Number' => $order->id,
                                    "Date" => date('Y-m-d H:i:s', $order->created_at),
                                    "ShipToAddress" => $order->shipping_address.' '.$order->shipping_address2.' '.$order->shipping_suburb.' '.$order->shipping_state.' '.$order->shipping_postcode.' '.$order->shipping_country,
                                    "Customer" => array(
                                        "DisplayID" => $customer_display_id,
                                    ),
                                    'Terms' => array(
                                        'Discount' => round($order->discount_amount, 2)
                                    ),
                                    "Lines" => $product_list,
                                    "Subtotal" => round($total, 2),
                                    "TotalAmount" => round($order->total_price(), 2),
                                );
                                $dataString = json_encode($orderData);

                                $ch = curl_init();

                                curl_setopt($ch, CURLOPT_URL, $create_order_uri);
                                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
                                  
                                // get the response & close the session
                                $response = curl_exec($ch); 
                                curl_close($ch);
                            }
                        }
                    }

                    \Messages::success('Successfully imported orders to MYOB');
                    \Response::redirect('admin/order/import_to_myob');
                }
            }
            \Messages::error('Invalid import process in MYOB');
            \Response::redirect('admin/order/import_to_myob');
        }
        else
        {
            \Messages::error('Invalid import process in MYOB');
            \Response::redirect('admin/order/import_to_myob');
        }
    }


    public function action_import_to_xero($user_id = false)
    {
        // check if enabled xero
        $accounting_api_settings = \Config::load('accounting_api.db');
        $accounting_api = (isset($accounting_api_settings['accounting_api']) ? $accounting_api_settings['accounting_api'] : '');

        if($accounting_api != 'xero')
        {
            \Messages::error("Xero is disabled.");
            \Response::redirect('admin/settings/accounting_api');
        }

        \View::set_global('menu', 'admin/order/list');
        \View::set_global('title', 'Import to Xero');
        $search = $this->get_search_items($user_id, true);
        $settings = \Config::load('xero.db');
        $items      = $search['items'];

        if (\Input::post()) {
            $input = \Input::post();
            if(!\Input::is_ajax())
            {
                $val = Model_Xero::validate('create');
                if($input['submit'] == 'auth'){
                    $XRO_APP_TYPE = "Private";
                    $useragent = "Edge Commerce - Xero";
                    $OAUTH_CALLBACK = "oob";

                    $signatures = array (
                        'consumer_key' => $input['auth_token'],
                        'shared_secret' => $input['auth_secret'],
                        'core_version' => '2.0',
                        'payroll_version' => '1.0',
                        'file_version' => '1.0'
                    );

                    if (!file_exists(PKGPATH . 'xerooauth/certs/'.$input['pem_hidden'])) {
                        \Messages::error('<strong>Cannot locate pem file, please upload a new pem file and try again.</strong>');
                        \Response::redirect('admin/order/import_to_xero');

                    }
                    else if(!file_exists(PKGPATH . 'xerooauth/certs/'.$input['cer_hidden'])){
                        \Messages::error('<strong>Cannot locate cer file, please upload a new cer file and try again.</strong>');
                        \Response::redirect('admin/order/import_to_xero');
                    }
                    else{
                        $signatures ['rsa_private_key'] = PKGPATH . 'xerooauth/certs/' . $input['pem_hidden'];
                        $signatures ['rsa_public_key'] = PKGPATH . 'xerooauth/certs/' . $input['cer_hidden'];
                    }

                    \Package::load('XeroOAuth');
                    $XeroOAuth = new \XeroOAuth (array_merge ( array (
                        'application_type' => $XRO_APP_TYPE,
                        'oauth_callback' => $OAUTH_CALLBACK,
                        'user_agent' => $useragent
                    ), $signatures ));

                    $initialCheck = $XeroOAuth->diagnostics ();
                    $checkErrors = count ( $initialCheck );

                    if ($checkErrors > 0) {
                        // you could handle any config errors here, or keep on truckin if you like to live dangerously
                        foreach ( $initialCheck as $check ) {
                            echo 'Error: ' . $check . PHP_EOL;
                        }
                    } else {
                        $session = persistSession ( array (
                            'oauth_token' => $XeroOAuth->config ['consumer_key'],
                            'oauth_token_secret' => $XeroOAuth->config ['shared_secret'],
                            'oauth_session_handle' => ''
                        ) );
                        $oauthSession = retrieveSession ();

                        if (isset ( $oauthSession ['oauth_token'] )) {
                            $XeroOAuth->config ['access_token'] = $oauthSession ['oauth_token'];
                            $XeroOAuth->config ['access_token_secret'] = $oauthSession ['oauth_token_secret'];
                        }

                    }
                    $data_product = "<Items>";
                    $products = \DB::select()->from('product')->join('product_attributes','left')->on('product.id', '=', 'product_attributes.product_id')->group_by('product.code')->as_object('\Product\Model_Product')->execute();
                    foreach ($products as $prd){
                        $pr_price = ($prd->retail_price)?$prd->retail_price:0;
                        $data_product .= "<Item>
                                              <Code>".$prd->code."</Code>
                                              <Description>".$prd->title."</Description>
                                              <PurchaseDetails>
                                                <UnitPrice>".$pr_price."</UnitPrice>
                                              </PurchaseDetails>
                                            </Item>";
                    }
                    $data_product .= "</Items>";
                    $data_product = str_replace("&", '&#038;', $data_product);
                    $XeroOAuth->request('POST', $XeroOAuth->url('Items'), array(), $data_product);

                    $ordersss = $XeroOAuth->request('GET', $XeroOAuth->url('purchaseorders'), array(), $data_product);
                    $ord_xero = json_decode($ordersss['response']);
                    $ord_lst = (isset($ord_xero->PurchaseOrders))?$ord_xero->PurchaseOrders:array();
                    $deleted_list = array();
                    foreach($ord_lst as $ol){
                        if($ol->Status == "DELETED")$deleted_list[] = $ol->PurchaseOrderNumber;
                    }

                    $data_param = "<PurchaseOrders>";
                    foreach ($items as $itm) {
                        if (!in_array($itm->id, $deleted_list)){
                            $contact_param = "<Contact>
                                              <ContactNumber>" . $itm->user_id . "</ContactNumber>
                                              <Name>" . $itm->first_name . ' ' . $itm->last_name . "</Name>
                                              <FirstName>" . $itm->first_name . "</FirstName>
                                              <LastName>" . $itm->last_name . "</LastName>
                                              <Email>" . $itm->email . "</Email>
                                              <Addresses>
                                                <Address>
                                                  <AddressLine1>" . $itm->address . "</AddressLine1>
                                                  <AddressLine2>" . $itm->address2 . "</AddressLine2>
                                                </Address>
                                              </Addresses>
                                              <IsCustomer>true</IsCustomer>
                                            </Contact>";

                            $a = $XeroOAuth->request('POST', $XeroOAuth->url('contacts'), array(), $contact_param);
                            $res = json_decode($a['response']);
                            $contact_id = (isset($res->Id)) ? $res->Id : '';

                            if ($contact_id != '')
                            {
                                $data_param .= "<PurchaseOrder>
                                          <PurchaseOrderNumber>" . $itm->id . "</PurchaseOrderNumber>
                                          <Date>" . date('Y-m-d', $itm->created_at) . "</Date>
                                          <Contact>
                                            <ContactNumber>" . $itm->user_id . "</ContactNumber>
                                            <ContactStatus>ACTIVE</ContactStatus>
                                            <Name>" . $itm->first_name . ' ' . $itm->last_name . "</Name>
                                            <UpdatedDateUTC>" . date('Y-m-d', time()) . "</UpdatedDateUTC>
                                            <DefaultCurrency>AUD</DefaultCurrency>
                                          </Contact>
                                          <LineAmountTypes>Exclusive</LineAmountTypes>";
                                $data_param .= "<LineItems>";
                                $total = 0;
                                foreach ($itm->products as $prd) 
                                {
                                    $get_product = \Product\Model_Product::find_one_by_id($prd->product_id);

                                    $data_item = "<Item>";
                                    $data_item .= "<Code>".($get_product->code?:$get_product->id)."</Code>";
                                    $data_item .= "<Name>" . $get_product->title . "</Name>";
                                    $data_item .= "<Description>" . $get_product->description_full . "</Description>";
                                    $data_item .= "</Item>";
                                    $a = $XeroOAuth->request('POST', $XeroOAuth->url('Items'), array(), $data_item);
                                    $res = json_decode($a['response']);

                                    $data_param .= "<LineItem>
                                              <ItemCode>".($get_product->code?:$get_product->product_id)."</ItemCode>
                                              <Description>" . $get_product->title . "</Description>
                                              <UnitAmount>" . $prd->price . "</UnitAmount>
                                              <TaxAmount>0.00</TaxAmount>
                                              <Quantity>" . $prd->quantity . "</Quantity>
                                            </LineItem>";

                                    $total += ($prd->price * $prd->quantity);
                                }
                                $data_param .= "</LineItems>";
                                $data_param .= "<SubTotal>".$total."</SubTotal>";
                                $data_param .= "<Total>".$itm->total_price()."</Total>";
                                $data_param .= "</PurchaseOrder>";
                            }

                        }
                    }
                    $data_param .= "</PurchaseOrders>";
                    $data_param = str_replace("&", '&#038;', $data_param);
                    $a = $XeroOAuth->request('POST', $XeroOAuth->url('PurchaseOrders'), array(), $data_param);
                    $res = $a['response'];
                    if($a['code'] == 401){
                        \Messages::error("Invalid API credentials. Get one <a href='https://app.xero.com' target='_blank'>here.</a>");
                    }else {
                        \Messages::success("Successfully imported orders to Xero.");
                    }
                }
                else if(!$val->run())
                {
                    if($val->error() != array())
                    {
                        // show validation errors
                        \Messages::error('<strong>There was an error while trying to update settings</strong>');
                        foreach($val->error() as $e)
                        {
                            \Messages::error($e->get_message());
                        }
                    }

                }
                else {
                    try {

                        $data_submit =  array(
                            'name' => "Xero",
                            'code' => "xero",
                            'mode' => $input['mode'],
                            'auth_token' => $input['auth_token'],
                            'auth_secret' => $input['auth_secret']
                        );

                        $check_upload = $this->upload_xero_file();
                        foreach($check_upload as $file_err){
                            if($file_err['field'] == 'pem'){
                                $data_submit['pem'] = $settings['pem'];
                            }
                            if($file_err['field'] == 'cer'){
                                $data_submit['cer'] = $settings['cer'];
                            }
                        }
                        foreach(\Upload::get_files() as $files){
                            if(files['saved_as'] != "") {
                                if ($files['field'] == 'pem') {
                                    $data_submit['pem'] = $files['saved_as'];
                                    unlink(PKGPATH . 'xerooauth/certs/' . $settings['pem']);
                                } else if ($files['field'] == 'cer') {
                                    $data_submit['cer'] = $files['saved_as'];
                                    unlink(PKGPATH . 'xerooauth/certs/' . $settings['cer']);
                                }
                            }
                        }

                        \Config::save('xero.db', $data_submit);
                        \Messages::success('Credentials successfully updated.');
                        \Response::redirect('admin/order/import_to_xero');
                    }
                    catch (\Database_Exception $e)
                    {
                        // show validation errors
                        \Messages::error('<strong>There was an error while trying to update settings.</strong>');

                        // Uncomment lines below to show database errors
                        $errors = $e->getMessage();
                        \Messages::error($errors);
                    }
                }
            }
        }

        \Theme::instance()->set_partial('content', $this->view_dir . 'import_to_xero')
            ->set('items', $items)
            ->set('settings', $settings);
    }


    public function upload_xero_file($type='')
    {
        $config = array(
            'path' => PKGPATH . 'xerooauth/certs/',
            'randomize' => true,
            'ext_whitelist' => array('pem', 'cer'),
        );

        \Upload::process($config);
        if (\Upload::is_valid())
        {
            \Upload::save();
        }
        return \Upload::get_errors();
    }

     public function action_approved($order_id = false)
    {
        $settings = \Config::load('autoresponder.db'); 
        $order = \Order\Model_Order::find_one_by_id($order_id);
        
        if($order)
        {
           
            $order->set(array('status' => 'approved'));
            $order->save();
            
            $user = \Sentry::user((int)$order->user_id);
            
            $content['content']['content'] = $order;
           
            $email = \Email::forge();
            
            $email->to($user->email, $order->first_name.' '.$order->last_name);
            $email->subject('Order Shipped from '.$settings['company_name']);
            $email->from($settings['email_address'], $settings['company_name']);
        
            $email->html_body(\Theme::instance()->view('views/_email/approved_order', $content, false));
            
            try
            {
                $email->send();
                \Messages::success('Order was successfully approved');
                \Response::redirect(\Uri::create('admin/order/update/'.$order_id));
            }
            catch(\Exception $e)
            {
                if(\Fuel::$env == 'development')
                    throw new \Exception($e->getMessage());
                else 
                    return false;
            }
            return true;
        } 
    } 
   
}

                            