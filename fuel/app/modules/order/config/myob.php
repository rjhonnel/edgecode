<?php
return array(

    /**
     * Enable or disable this payment
     */
    'enabled' => true,  // Required true or false

    'code'	  => 'myob',  // Required

    'name'	  => 'MYOB',  // Required

    'config' => array(

        'login_uri' => 'https://secure.myob.com/oauth2/account/authorize',

        'scope' => 'CompanyFile',

        'api_url' => 'https://api.myob.com/accountright/'

    )

);