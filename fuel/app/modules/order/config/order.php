<?php
 return array(
 	
 	/**
 	 * Enable or disable this module
 	 */
 	'enabled' => true,
 		
 	'status_old' => array(
        'pending'             => 'Pending',
        'order_submited'      => 'Order submited',
 		'in_progress'         => 'In progress',
 		'closed'              => 'Closed',
        'shipped_to_customer' => 'Shipped to customer',
        'cancelled'           => 'Canceled',
 	),
 		
 	/*'status' => array(
        'pending'             => 'Pending',
 		'in_progress'         => 'In Progress',
        'shipped_to_customer' => 'Shipped',
        'order_submited'      => 'Complete',
        'cancelled'           => 'Canceled',
 	),*/
        
    'status' => array(
        'pending'               => 'Pending',
        'new'                   => 'New',
        /*'received'              => 'Received',
        'shipped'               => 'Shipped',*/
        'approved'              => 'Approved',
        'invoiced'              => 'Invoiced',
        'partially_paid'        => 'Partially Paid',
        'paid'                  => 'Paid',
        'cancelled'             => 'Canceled',
       
    ),
     
    'invoice' => array(
        'pending'       =>  'Pending',
 		'invoiced' 	    =>  'Invoiced',
        'paid' 	        =>  'Paid',
    ),
     
    'shipping_status' => array(
        'pending'     =>  'Pending',
        'delivered'   =>  'Delivered',
    ),
     
    'shipping_method_old' => array(
        'ups'            => 'ups',
        'dhl'            => 'DHL',
        'fedex'          => 'FedEx',
        'australia_post' => 'Australia Post',
    ),
     
    'payment_method' => function(){

        $payment_method_list = array( 'cash'  => 'Cash', 'invoice'  => 'Invoice',  );

        if(isset(\Config::load('paypal.db')['activate']))
        {
            if(\Config::load('paypal.db')['activate'] == 1)
                $payment_method_list += array( \Config::load('paypal.db')['code'] => \Config::load('paypal.db')['name'] );
        }
        if(isset(\Config::load('securePay.db')['activate']))
        {
            if(\Config::load('securePay.db')['activate'] == 1)
                $payment_method_list += array( \Config::load('securePay.db')['code'] => \Config::load('securePay.db')['name'] );
        }
        if(isset(\Config::load('eWay.db')['activate']))
        {
            if(\Config::load('eWay.db')['activate'] == 1)
                $payment_method_list += array( \Config::load('eWay.db')['code'] => \Config::load('eWay.db')['name'] );
        }
        return $payment_method_list;
    },

    'dynamic_payment_method' => function(){

        $payment_method_list = array();

        if(isset(\Config::load('paypal.db')['activate']))
        {
            if(\Config::load('paypal.db')['activate'] == 1)
                $payment_method_list += array( \Config::load('paypal.db')['code'] => \Config::load('paypal.db')['name'] );
        }
        if(isset(\Config::load('securePay.db')['activate']))
        {
            if(\Config::load('securePay.db')['activate'] == 1)
                $payment_method_list += array( \Config::load('securePay.db')['code'] => \Config::load('securePay.db')['name'] );
        }
        if(isset(\Config::load('eWay.db')['activate']))
        {
            if(\Config::load('eWay.db')['activate'] == 1)
                $payment_method_list += array( \Config::load('eWay.db')['code'] => \Config::load('eWay.db')['name'] );
        }
        return $payment_method_list;
    },

    'allow_delete_gateway_partial_payment_transaction' => array(
        'invoice',
        'cash',
    ),
     
    'payment_status' => array(
        'pending'       => 'Pending',
        'completed'     => 'Completed',
        'failed'        => 'Failed',
    ),
     
    'shipping_method' => array(
        'australia_post' => 'Australia Post',
        'courier'            => 'Courier',
        'fedex'          => 'FedEx',
        'dhl'            => 'DHL',
        'transport_co'            => 'Transport Co.',
        'delivery'        => 'Delivery',
        'pickup'        => 'Store Pick Up'
    ),

     
    'file'	=> array(
       'enabled'	=> true,
       'required'	=> false,
       'multiple'	=> false,
       /**
        * Set location based items
        */
       'location' => array(
           'folder'	=> 'documents',
           'root' 		=> function(){ 
               return DOCROOT . 'media' . DIRECTORY_SEPARATOR . 'documents'. DIRECTORY_SEPARATOR; 
           },
       ),
       // END OF LOCATION
 	), 
                    
    'bulk_actions' => array(
        '0' => 'Select Action',
        'export' => 'Export Orders',
        'delete' => 'Delete',
        'change_order_status' => 'Change Order Status',
        'change_payment_status' => 'Change Payment Status',
        'change_shipping_status' => 'Change Shipping Status',
        'email_customer_invoice' => 'Email Customer Invoice',
        'download_invoices' => 'Download Invoices',
     ),
                   
    // Artwork upload
    'artwork' => array(
        'max_types_required' => 5,
        'max_number_of_files' => 5,
    ),

    'time' => array(
        'hours' => array(
            '-' => 'HH',
            '07' => '07',
            '08' => '08',
            '09' => '09',
            10 => 10,
            11 => 11,
            12 => 12,
            13 => 13,
            14 => 14,
            15 => 15,
            16 => 16,
            17 => 17,
        ),
        'minutes' => array(
            '-' => 'MM',
            '00' => '00',
            '05' => '05',
            10 => 10,
            15 => 15,
            20 => 20,
            25 => 25,
            30 => 30,
            35 => 35,
            40 => 40,
            45 => 45,
            50 => 50,
            55 => 55,
        ),
        'periods' => array(
            '' => '-',
            'am' => 'AM',
            'pm' => 'PM',
        ),
        'hour_list' => array(
            '-' => 'HH:MM',
            '07:00' => '7:00 AM',
            '07:30' => '7:30 AM',
            '08:00' => '8:00 AM',
            '08:30' => '8:30 AM',
            '09:00' => '9:00 AM',
            '09:30' => '9:30 AM',
            '10:00' => '10:00 AM',
            '10:30' => '10:30 AM',
            '11:00' => '11:00 AM',
            '11:30' => '11:30 AM',
            '12:00' => '12:00 PM',
            '12:30' => '12:30 PM',
            '13:00' => '1:00 PM',
            '13:30' => '1:30 PM',
            '14:00' => '2:00 PM',
            '14:30' => '2:30 PM',
            '15:00' => '3:00 PM',
            '15:30' => '3:30 PM',
            '16:00' => '4:00 PM',
            '16:30' => '4:30 PM',
            '17:00' => '5:00 PM',
            '17:30' => '5:30 PM',
        )
    )
 
 );