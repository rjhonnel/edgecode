<?php
return array(

    /**
     * Enable or disable this payment
     */
    'enabled' => true,  // Required true or false

    'code'	  => 'xero',  // Required

    'name'	  => 'Xero',  // Required

    'config' => array(

        /**
         * New config
         */
        'active' => 'test',

        'default' => array(
            'auth_token' 		=> 'AFcWxV21C7fd0v3bYYYRCpSSRl31AiFAH',
            'auth_secret' 	=> 'AFcWxV21C7fd0v3bYYYRCpSSRl31AiFAH',
            'pem' 	=> 'privatekey.pem',
            'cer' 	=> 'publickey.cer',
        ),
        'test' => array(
            'auth_token' 		=> 'AFcWxV21C7fd0v3bYYYRCpSSRl31AiFAH',
            'auth_secret' 	=> 'AFcWxV21C7fd0v3bYYYRCpSSRl31AiFAH',
            'pem' 	=> 'privatekey.pem',
            'cer' 	=> 'publickey.cer',
        ),

    ),

);