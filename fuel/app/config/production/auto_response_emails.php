<?php 

	// Emails config
	return array(

		"default_emails" => array(

			"info@edge.lightmedia.com.au",

		),
        
		"order_emails" => array(

			"info@edge.lightmedia.com.au",

		),
                
		"register_emails" => array(

            "info@edge.lightmedia.com.au",

		),
        
        "discuss_brief" => array(

			"info@edge.lightmedia.com.au",

		),
        
		"forgot_password_emails" => array(

            "info@edge.lightmedia.com.au",

		),
        
        "no-reply" => "no-reply@edge.lightmedia.com.au",
        
        "autoresponder_from_email" => "info@edge.lightmedia.com.au",
        
        "bcc" => array(
		),

	);
