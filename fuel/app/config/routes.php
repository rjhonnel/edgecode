<?php

	return array(
		'_root_'  => 'page/index', // The default route
		'_404_'   => 'base/public/404', // The main 404 route
		'admin/newscategory/list'   => 'application/admin_application/list',
		'admin/newslist'   => 'application/admin_casestudy/list',
		'search'   => 'product/_search',
		
		'deals'   => 'product/deals',
		'deals/(:segment)/(:segment)'   => 'product/deals/$1/$2',

		'admin'   => 'admin/index',
		'api'   => 'api/index',

		'page/index/(:any)'   => 'base/public/404', // disable routes
		'product/category/index/(:segment)'   => 'base/public/404', // disable routes
		'product/index/(:segment)'   => 'base/public/404', // disable routes

		'(:segment)' => 'base/public/is_exist/$1', // Check if segment is existing page, product category or product
	);