<?php
// Set pagination template for frontend
$active = \Theme::instance()->active();
if($active['name'] == 'frontend') 
{
     return array(
         'template' => array(
            'wrapper_start'  => '<ul class="pagination">',
            'wrapper_end'    => '</ul>',
            'page_start'     => '<li class=":state"><a href=":url">',
            'page_end'       => '</a></li>',
            'previous_start' => '<li class=":state"><a class ="back" href=":url">',
            'previous_end'   => '</a></li>',
            'previous_mark'  => '',
            'next_start'     => '<li class=":state"><a class="next" href=":url">',
            'next_end'       => '</a></li>',
            'next_mark'      => '',
            'state'          => array(
                'previous_next' => array(
                        'active'   => '',
                        'disabled' => 'disabled" onclick="return false;',
                ),
                'current_page' => 'current',
            ),
        ),
    );
}
return array();