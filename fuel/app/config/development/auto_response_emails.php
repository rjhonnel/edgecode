<?php 
	
	// Emails config
	return array(

		"default_emails" => array(

			"rasplinjac@gmail.com",

		),
        
		"order_emails" => array(

			"rasplinjac@gmail.com",

		),
                
		"register_emails" => array(

            "rasplinjac@gmail.com",

		),
        
        "discuss_brief" => array(

			"rasplinjac@gmail.com",

		),
        
		"forgot_password_emails" => array(

            "rasplinjac@gmail.com",

		),
        
        "no-reply" => "no-reply@web-site-dev.com",
        
        "autoresponder_from_email" => "no-reply@web-site-dev.com",
        
        "bcc" => array(
            "rasplinjac@gmail.com",
            "testing@test123.com",
		),

	);
