<?php
return array(
    'default' => array(
        'thumb' => array(
           'bgcolor' => '#ffffff',
            'filetype' => 'jpg', 
            'quality' => 75,
            'actions' => array(
                array('resize', 200, 200, true, false),
                array('output', 'jpg')
            )
        )
        
    ),
	'blog' => array(
        'thumb' => array(
            'bgcolor' => '#ffffff',
            'filetype' => 'jpg', 
            'quality' => 75,
            'actions' => array(
                array('resize', 300, 200, true, true),
                array('output', 'jpg')
            )
        )
    ),
    'featured' => array(
        'thumb' => array(
            'bgcolor' => '#ffffff',
            'filetype' => 'jpg', 
            'quality' => 75,
            'actions' => array(
                array('resize', 300, 200, true, true),
                array('output', 'jpg')
            )
        )
    )
);