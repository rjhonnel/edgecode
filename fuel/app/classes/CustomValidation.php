<?php
class Validation extends \Fuel\Core\Validation {

	/**
	 * Check if a value is unique in column
	 *
	 * @param   string   $val
	 * @param   array    $options   keys are 0:table, 1:field, 2:id field value, 3:id field. Default id field is id
	 */
	public function _validation_unique($val, $options = array())
	{
		Validation::active()->set_message('unique', 'It appears you have already registered using this email address.');
		 
		$result = \DB::select(\DB::expr("LOWER (\"{$options['1']}\")"))->from($options['0'])
		->where($options['1'], '=', Str::lower($val));
		 
		if(!empty($options['2']) && is_numeric($options['2']))
		{
			$id = empty($options['3']) ? 'id' : $options['3'];
			$result->where($id, '!=', $options['2']);
		}

		if(count($result->execute()) > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}



}