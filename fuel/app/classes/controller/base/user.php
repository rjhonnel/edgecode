<?php
/**
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    1.0
 * @author     CMS Development Team
 */

class Controller_Base_User extends Controller_Base_Public
{
	/**
	 * @param   none
	 * @throws  none
	 * @returns	void
	 */
	public function before()
	{
		// users need to be logged in to access this controller
		//if ( ! \Sentry::check())
        if ($this->check_logged_type() != 'user')
		{
			\Messages::error('Access denied. Please login first');
			\Response::redirect('/user/login');
		}

		parent::before();
	}
    
    public function check_logged()
    {
        $logged = true;
        
        if (!(\Sentry::check() && !\Sentry::user()->is_admin()))
        {
            $logged = false;
        }
        
        return $logged;
    }
    
    public function check_guest()
    {
        $guest = false;
        if($this->check_logged())
        {
            $user = \Sentry::user();
            if(!empty($user['metadata']) && $user['metadata']['guest'] == 1)  $guest = true;
        }
        return $guest;
    }
    
    /**
	 * After controller method has run, render the theme template
	 *
	 * @return  mixed:  false, user, guest
	 */
    public function check_logged_type()
    {
        if(!$this->check_logged())
        {
           return false;
        }
        
        $user = \Sentry::user();
        if(!empty($user['metadata']) && $user['metadata']['guest'] == 1)  return 'guest';
            
        return 'user';
    }

}
