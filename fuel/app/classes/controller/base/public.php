<?php
/**
 * /fuel/app/classes/controller/base
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    2.0
 * @author     CMS Development Team
 */


class Controller_Base_Public extends Controller_Base_Template
{

    /**
	* @var string page template for admin controllers
	*/
	public $template = 'views/layout';
    public $hide_out_of_stock = 0;
    public $do_not_allow_buy_out_of_stock = 0;

    public function before()
	{
		parent::before();

        //Visits
        $visited = \Cookie::get('visited', false);
        if ( ! $visited ) {

            \Dashboard::log_visitor();

           \Cookie::set( "visited", true , 86400 );
        }


         // Cart
        \Config::load('cart', true);    

        $cartManager = new \Extendedcart\Manager(\Config::get('cart'));
        
        \CartManager::init($cartManager);
        
        \CartManager::context('Cart');

        // check if there is order session id and not found in db
        // resave if not found
        $order_id = \Session::get('order.id', false);
        if(is_numeric($order_id))
        {
            $order = \Order\Model_Order::find_one_by_id($order_id);
            $items = \Cart::items();
            if(!$order && $items)
            {
                if(\Sentry::check() && $this->check_logged() && $this->check_logged_type() != 'guest')
                {
                    $user = \Sentry::user();
                    $order = \Order\Model_Order::forge(array(
                        'user_id' => $user->get('id'),
                        'finished' => 0
                    ));
                }
                else
                {
                    $order = \Order\Model_Order::forge(array(
                        'finished' => 0
                    ));
                }
                $order->save();

                $order->set(array('id'=>$order_id));
                $order->save();

                foreach ($items as $item)
                {
                    $abandon_cart_products = \Order\Model_Abandon_Cart_Products::forge(array(
                        'order_id'          => $order_id,
                        'title'             => $item->get('title'),
                        'code'              => $item->get('product_code'),
                        'price'             => $item->get('price'),
                        'price_type'        => $item->get('price_type'),
                        'quantity'          => $item->get('quantity'),
                        'product_id'        => $item->get('id'),
                        'subtotal'          => ($item->get('price')*$item->get('quantity')),
                        'attributes'        => $item->get('attributes_json'),
                        'attributes_id'     => $item->get('attributes'),
                        'delivery_charge'   => $item->get('delivery_charge')?:0,
                        'product_category'  => $item->get('product_category')
                    ));

                    $abandon_cart_products->save();
                }
            }
        }
        
        // Set Visitors group default
        \Product\Model_Attribute::set_user_group(3);

		\Theme::instance()->active('frontend');
		\Theme::instance()->set_template($this->template);
        
        // Set a global variable so views can use it
        $seo = array(
            'meta_title' => '',
            'meta_description' => '',
            'meta_keywords' => '',
            'canonical_links' => '',
            'meta_robots_index' => 1,
            'meta_robots_follow' => 1,
        );
        \View::set_global('seo', $seo, false);
		\View::set_global('theme', \Theme::instance(), false);
        \View::set_global('logged', $this->check_logged(), false);
        \View::set_global('guest', $this->check_guest(), false);
        \View::set_global('logged_type', $this->check_logged_type(), false);

        // Set Stock Options Global
        $stock_options = \Config::load('stock-option.db');
        $hold_manage_stock = (isset($stock_options['manage_stock']) ? 1 : 0);
        $hold_hide_out_of_stock = 0;
        $hold_do_not_allow_buy_out_of_stock = 0;
        if($hold_manage_stock)
        {
            $hold_hide_out_of_stock = (isset($stock_options['hide_out_of_stock']) ? 1 : 0);
            $hold_do_not_allow_buy_out_of_stock = (isset($stock_options['do_not_allow_buy_out_of_stock']) ? 1 : 0);
        }
        
        $this->hide_out_of_stock = $hold_hide_out_of_stock;
        $this->do_not_allow_buy_out_of_stock = $hold_do_not_allow_buy_out_of_stock;

        \View::set_global('hide_out_of_stock', $hold_hide_out_of_stock, false);
        \View::set_global('do_not_allow_buy_out_of_stock', $hold_do_not_allow_buy_out_of_stock, false);
	}
    
    /**
	 * After controller method has run, render the theme template
	 *
	 * @param  Response  $response
	 */
	public function after($response)
	{
			
		if ( ! \Input::is_ajax())
		{
			// If nothing was returned set the theme instance as the response
			if (empty($response))
			{
				$response = \Response::forge(\Theme::instance());
			}
				
			if ( ! $response instanceof Response)
			{
				$response = \Response::forge($response);
			}
	
			return $response;
		}
	
		return parent::after($response);
	}
    
    public function action_get_states($country = false)
    {
        if(!\Input::is_ajax()) return;
        $out = '<option value="">-</option>';
        $states = \App\States::forge();
        if($states_array = $states->getStateProvinceArray($country))
        {
            $out = '';
            foreach ($states_array as $key => $value)
            {
                $out .= '<option value="' . $key . '">' . $value . '</option>';
            }
        }
        echo $out;
        exit;
    }
    
    public function check_logged()
    {
        $logged = true;
        
        if (!(\Sentry::check() && !\Sentry::user()->is_admin()))
        {
            $logged = false;
        }
        
        return $logged;
    }
    
    public function check_guest()
    {
        $guest = false;
        if($this->check_logged())
        {
            $user = \Sentry::user();
            if(!empty($user['metadata']) && $user['metadata']['guest'] == 1)  $guest = true;
        }
        return $guest;
    }
    
    /**
	 * After controller method has run, render the theme template
	 *
	 * @return  mixed:  false, user, guest
	 */
    public function check_logged_type()
    {
        if(!$this->check_logged())
        {
           return false;
        }
        
        $user = \Sentry::user();
        if(!empty($user['metadata']) && $user['metadata']['guest'] == 1)  return 'guest';
            
        return 'user';
    }

    /**
     * The 404 action for the application.
     * 
     * @access  public
     * @return  Response
     */
    public function action_404()
    {
        if(strpos($_SERVER['REQUEST_URI'], '/admin/') !== false)
        {
            \Theme::instance()->active('admin');
            \Theme::instance()->set_template('views/404');
        }
        else
            \Theme::instance()->set_template('views/404');
    }


    /**
     * Check if segment is existing page, product category or product
     * 
     * @access  public
     * @return  Response
     */
    public function action_is_exist($slug = false)
    {
        $item = false;

        if($slug !== false)
        {
            if($item = \Page\Model_Page::get_by_slug($slug)) // Check if slug is a page
            {
                $page = new \Page\Controller_Page($this->request);
                $page->show($slug);
            }
            else if($item = \Product\Model_Category::get_by_slug($slug))
            {
                $category = new \Product\Controller_Category($this->request);
                $category->show($slug);
            }
            else if($item = \Product\Model_Brand::get_by_slug($slug))
            {
                $brand = new \Product\Controller_Brand($this->request);
                $brand->show($slug);
            }
            else if($item = \Product\Model_Product::get_by_slug($slug))
            {
                $product = new \Product\Controller_Product($this->request);
                $product->show($slug);
            }
        }

        if(!$item)
        {
            // Send to 404 page
            throw new \HttpNotFoundException;
        }
    }
}
