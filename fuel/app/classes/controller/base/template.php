<?php
/**
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    2.0
 * @author     CMS Development Team
 */

class Controller_Base_Template extends Controller_Hybrid
{
	/**
	* @var string page template
	*/
	public $template = 'views/layout';

	/**
	 * Is this a setup controller ?
	 *
	 * @var bool
	 */
	var $is_setup_controller = false;

	/**
	* @var string global date format to use
	*/
	public $date_format = 'eu';

	/**
	 * @param   none
	 * @throws  none
	 * @returns	void
	 */
	public function before()
	{
		// Redirect to setup script is isAllow method is passed
		if ($this->is_setup_controller == false && (new \Setup\Setup_Installer())->isAllow()) {
			\Fuel\Core\Response::redirect('/setup/index');
			return ;
		}

		if (\Input::is_ajax())
		{
			return parent::before();
		}

        // define the theme template to use for this page, set a default if needed
		\Theme::instance()->set_template($this->template);
	}

	/**
	 * After controller method has run, render the theme template
	 *
	 * @param  Response  $response
	 */
	public function after($response)
	{
		if ( ! \Input::is_ajax())
		{
			// If nothing was returned set the theme instance as the response
			if (empty($response))
			{
				$response = \Response::forge(\Theme::instance());
			}
			
			if ( ! $response instanceof Response)
			{
				$response = \Response::forge($response);
			}

			return $response;
		}

		return parent::after($response);
	}
}
