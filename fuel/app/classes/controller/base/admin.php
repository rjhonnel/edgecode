<?php
/**
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    2.0
 * @author     CMS Development Team
 */

class Controller_Base_Admin extends Controller_Base_Template
{
	/**
	* @var string page template for admin controllers
	*/
	public $template = 'views/layout';
	
	/**
	 * @param   none
	 * @throws  none
	 * @returns	void
	 */
	public function before()
	{
        
		$result = array();

		// users need to be logged in to access this controller
		if (!\SentryAdmin::check())
		{
			$result = array(
				'message' => 'You need to be logged in to access that page.',
				'url' => '/admin/login',
			);
			
			// Don't show this message if url is just 'admin'
			if(\Uri::string() == 'admin/admin/index')
				unset($result['message']);
			
			\Session::set('redirect_to', \Uri::admin('current'));
		}
		else if (!\SentryAdmin::user()->is_admin())
		{
			$result = array(
				'message' => 'Access denied. You need to be a member of staff to access that page.',
				'url' => '/admin/login',
			);
			
			\Session::set('redirect_to', \Uri::admin('current'));
		}
		
		if ( ! empty($result))
		{
			if (\Input::is_ajax())
			{
				\Messages::error('You need to be logged in to complete this action.');
				echo \Messages::display('left', false); exit;
			}
			else
			{
				if(isset($result['message']))
					\Messages::warning($result['message']);
					
				\Response::redirect($result['url']);
			}
		}

		parent::before();
	}
}

if(!function_exists('nf'))
{
    function nf($number)
    {
        return number_format($number, 2);
    }
}

if (get_magic_quotes_gpc()) {
    function stripslashes_gpc(&$value)
    {
        $value = stripslashes($value);
    }
    array_walk_recursive($_GET, 'stripslashes_gpc');
    array_walk_recursive($_POST, 'stripslashes_gpc');
    array_walk_recursive($_COOKIE, 'stripslashes_gpc');
    array_walk_recursive($_REQUEST, 'stripslashes_gpc');
}

function scanDirectories($rootDir, $allData=array()) {
    // set filenames invisible if you want
    $invisibleFileNames = array(".", "..", ".htaccess", ".htpasswd");
    // run through content of root directory
    $dirContent = scandir($rootDir);
    foreach($dirContent as $key => $content) {
        // filter all files not accessible
        $path = $rootDir.'/'.$content;
        if(!in_array($content, $invisibleFileNames)) {
            // if content is file & readable, add to array
            if(is_file($path) && is_readable($path)) {
                // save file name with path
                $allData[] = '/'.$path;
            // if content is a directory and readable, add path and name
            }
            // elseif(is_dir($path) && is_readable($path)) {
            //     // recursive callback to open new directory
            //     $allData = scanDirectories($path, $allData);
            // }
        }
    }
    return $allData;
}