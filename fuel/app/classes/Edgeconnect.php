<?php

use Exception;

/**
 * Edgeconnect PHP Client for Api Version 2
 *
 * @version 0.1.0
 * @author Rex Taylor <rex@lightmedia.com.au>
 */
class Edgeconnect {

    const API_CLIENT_VERSION = "0.1.0";
    const API_VERSION = "2";

    const RESULT_SUCCESS = 'success';
    const RESULT_FAILURE = 'error';

    const HTTP_POST = 'POST';
    const HTTP_GET  = 'GET';
    const HTTP_PUT  = 'PUT';
    const HTTP_DELETE = 'DELETE';

    const API_HEADER_API_VERSION = 'X-EDM-API-VERSION';
    const API_HEADER_KEY = 'X-API-KEY';
    const API_HEADER_CLIENT = 'X-EDM-API-CLIENT';

    const ENCRYPTED_DATA_NAME = 'encrypted_data';

    /**
     * Edm account API Keys
     *
     * @var string
     */
    protected $apiKey;

    /**
     * Default settings
     *
     * @var array
     */
    protected $settings = array(
        'protocol' => 'https',
        'host' => 'edc.lightmedia.com.au',
        'timeout' => 30,
        'debug' => false,
        'force_encrypt' => false
    );

    /**
     * Create an instance of Edge mailer API
     *
     * @param string    $appKey
     * @param array     $options
     */
    public function __construct($appKey, array $options = array()) {
        $this->apiKey = $appKey;
        $this->settings = array_merge($this->settings, $options);
    }

    /**
     * Create Group
     *
     * @param string $group_name    The group name
     * @param array $data (optional) group data
     *
     * parameter $data value and key description:
     * (int)'parent'      - (optional) The group parent id, default is 0
     * (int)'order'       - (optional) The order position of the group, default is 0
     * (string)'comment'  - (optional) The group comment, default is null
     *
     * @return array API response object.
     */
    public function createGroup($group_name, $data = array())
    {
        $uri = '/groups';
        $post_data = array_merge(array(
            'name' => $group_name,
        ), $data);

        return $this->apiRequest($uri, self::HTTP_POST, array(), $post_data);
    }

    /**
     * Update Group
     *
     * @param string $group_id      the group id
     * @param array $data (required) group data
     *
     * parameter $data value and key description:
     * (string)'name'     - (required) The group name
     * (int)'parent'      - (optional) The group parent id, default is 0
     * (int)'order'       - (optional) The order position of the group, default is 0
     * (string)'comment'  - (optional) The group comment, default is null
     *
     * @return array API response object.
     */
    public function updateGroup($group_id, $data)
    {
        $uri = '/groups/'.urlencode($group_id);
        $post_data = $data;

        return $this->apiRequest($uri, self::HTTP_PUT, array(), $post_data);
    }

    /**
     * Get Group
     *
     * @param string $group_id      the group id
     * @return array API response object.
     */
    public function getGroup($group_id)
    {
        $uri = '/groups/'.urlencode($group_id);

        return $this->apiRequest($uri, self::HTTP_GET);
    }

    /**
     * list Group
     *
     * @param string    $q          (optional) search string
     * @param int       $page       (optional) result page
     * @param int       $per_page   (optional) results per page
     * @param array     $data       (optional) additional data
     *
     * parameter $data value and key description:
     * (string)'sort_by'     - (optional) column name to sort
     * (string)'sort_method' - (optional) (asc|desc) default to as
     *
     * @return array API response object.
     */
    public function listGroup($q = null, $page = 1, $per_page = 10, $data = array())
    {
        $uri = '/groups';
        $query = array_merge(array(
            'q' => $q,
            'page' => $page,
            'per_page' => $per_page,
        ), $data);

        return $this->apiRequest($uri, self::HTTP_GET, $query);
    }

    /**
     * Delete Group
     *
     * @param int $group_id (optional) search string
     * @return array API response object.
     */
    public function deleteGroup($group_id)
    {
        $uri = '/groups/'.urlencode($group_id);
        return $this->apiRequest($uri, self::HTTP_DELETE);
    }

    /**
     * Create User to Group
     *
     * @param int       $group_id   The Group ID
     * @param array     $data       (required) user data
     *
     * parameter $data value and key description:
     * (string)'parent'     - (required) The group id
     * (string)'email'      - (required) Email of the user belong to the group
     * (string)'first_name' - (optional) First Name of the user
     * (string)'last_name'  - (optional) Last Name of the user
     * (string)'title'      - (optional) Title Name
     * (string)'company'    - (optional) Company Name
     * (string)'active'     - (optional) (yes|no) Active
     * (string)'phone'      - (optional) Phone Number
     * (string)'country'    - (optional) Country
     * (string)'message'    - (optional) Message
     *
     * @return array API response object.
     */
    public function createGroupUser($group_id, $data = array())
    {
        $clean_group_id = urlencode($group_id);
        $uri = "/groups/{$clean_group_id}/users";
        $post_data = $data;

        return $this->apiRequest($uri, self::HTTP_POST, array(), $post_data);
    }

    /**
     * Update Group
     *
     * @param int $group_id      the group id
     * @param string $email         user email data
     * @param array $data (required) group data
     *
     * parameter $data value and key description:
     * (string)'parent'     - (required) The group id
     * (string)'email'      - (required) New Email of the user belong to the group
     * (string)'first_name' - (optional) First Name of the user
     * (string)'last_name'  - (optional) Last Name of the user
     * (string)'title'      - (optional) Title Name
     * (string)'company'    - (optional) Company Name
     * (string)'active'     - (optional) (yes|no) Active
     * (string)'phone'      - (optional) Phone Number
     * (string)'country'    - (optional) Country
     * (string)'message'    - (optional) Message
     *
     * @return array API response object.
     */
    public function updateGroupUser($group_id, $email, $data)
    {
        $clean_group_id = urlencode($group_id);
        $clean_email = urlencode($email);
        $uri = "/groups/{$clean_group_id}/users/{$clean_email}";
        $post_data = $data;

        return $this->apiRequest($uri, self::HTTP_PUT, array(), $post_data);
    }

    /**
     * List users from group
     *
     * @param int       $group_id   (required) group id
     * @param string    $q          (optional) search string
     * @param int       $page       (optional) result page
     * @param int       $per_page   (optional) results per page
     * @param array     $data       (optional) additional data
     *
     * parameter $data value and key description:
     * (string)'sort_by'     - (optional) column name to sort
     * (string)'sort_method' - (optional) (asc|desc) default to as
     *
     * @return array API response object.
     */
    public function listGroupUser($group_id, $q = null, $page = 1, $per_page = 10, $data = array())
    {
        $clean_group_id = urlencode($group_id);
        $uri = "/groups/{$clean_group_id}/users";
        $query = array_merge(array(
            'q' => $q,
            'page' => $page,
            'per_page' => $per_page,
        ), $data);

        return $this->apiRequest($uri, self::HTTP_GET, $query);
    }


    /**
     * Get Group
     *
     * @param string $group_id      the group id
     * @param string $email         user email data
     * @return array API response object.
     */
    public function getGroupUser($group_id, $email)
    {
        $clean_group_id = urlencode($group_id);
        $clean_email = urlencode($email);
        $uri = "/groups/{$clean_group_id}/users/{$clean_email}";

        return $this->apiRequest($uri, self::HTTP_GET);
    }

    /**
     * Delete Group
     *
     * @param int $group_id (optional) search string
     * @param string $email (required) user email data
     * @return array API response object.
     */
    public function deleteGroupUser($group_id, $email)
    {
        $clean_group_id = urlencode($group_id);
        $clean_email = urlencode($email);
        $uri = "/groups/{$clean_group_id}/users/{$clean_email}";

        return $this->apiRequest($uri, self::HTTP_DELETE);
    }


    /**
     * Create an API endpoint
     *
     * @param $uri
     * @param array $params
     * @return string
     * @internal param $endpoint
     */
    private function createPath($uri, $params = array())
    {
        // if uri is a direct url ?
        if (strpos($uri, 'http') === 0) {
            return $uri;
        }

        $protocol = $this->settings['protocol'];

        $path = sprintf("%s://%s/api/v%s%s", $protocol, $this->settings['host'], self::API_VERSION, $uri);

        // pass query string, including the api if force encrypted locally
        if ($this->isForceEncrypt()) {
            $params['api_key'] = $this->apiKey;
            $params = array(
                self::ENCRYPTED_DATA_NAME => $this->encryptParam($params)
            );
        }

        if (!empty($params)) {
            $url_sep = (strpos($path, '?') === false) ? '?' : '&';
            $path = $path . $url_sep . http_build_query($params);
        }

        return $path;
    }

    /**
     * Logger message
     *
     * @param $message
     */
    public function logger($message) {
        error_log($message);
    }


    /**
     * Locally encrypt data, and automatically include api key
     *
     * @param $param
     * @return string
     */
    function encryptParam($param) {
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->apiKey, json_encode($param), MCRYPT_MODE_ECB));
    }

    /**
     * Decrypt encrypted data from response
     *
     * @param $encrypted_param_str
     * @return mixed
     */
    function decryptData($encrypted_param_str) {
        return json_decode(trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->apiKey, base64_decode($encrypted_param_str), MCRYPT_MODE_ECB)), true);
    }

    /**
     * Send API Request
     *
     * @param string    $endpoint
     * @param string    $request
     * @param array     $query
     * @param array     $postData
     * @return array|mixed|object
     */
    public function apiRequest($endpoint, $request = self::HTTP_POST, array $query = array(), array $postData = array()) {
        $full_path = $this->createPath($endpoint, $query);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request);
        // set headers
        $http_headers = array();
        $http_headers[] = self::API_HEADER_API_VERSION . ": " . self::API_VERSION;
        $http_headers[] = 'X-Requested-With: XMLHttpRequest';

        // if post, put and delete request
        // then manage post data request
        if ($postData && ($request != self::HTTP_GET)) {
            if ($this->isForceEncrypt()) {
                // set payload encrypt locally
                $dataString = http_build_query(array(
                    'encrypted_query' => $this->encryptParam($postData)
                ));
            } else {
                // set payload
                $dataString = http_build_query($postData);
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
            $http_headers[] = 'Content-Length: ' . strlen($dataString);
        }

        // if not force encrypt, then pass the api header key
        if (!$this->isForceEncrypt()) {
            $http_headers[] = self::API_HEADER_KEY . ": " . $this->apiKey;
        }

        curl_setopt($ch, CURLOPT_URL, $full_path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $http_headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->settings['timeout']);

        if ($this->settings['debug']) {
            // enable curl verbose output to STDERR
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            $this->logger(sprintf("payload: %s\r\n", $dataString));
            $this->logger(sprintf("path: %s\r\n", $full_path));
        }

        try {
            $result = curl_exec($ch);
            $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $response = json_decode($result, true);

            if (!$response || $response === null || $status_code != 200) {
                throw new EdgeconnectException("Request was not successful", $status_code, $result, $response);
            }

            // if response encrypted ?
            if (array_key_exists(self::ENCRYPTED_DATA_NAME, $response)) {
                $response = $this->decryptData($response[self::ENCRYPTED_DATA_NAME]);
            }
        } catch (EdgeconnectException $e) {
            if ($this->settings['debug']) {
                $this->logger(sprintf("Caught exception: %s\r\n", $e->getMessage()));
                $this->logger(print_r($e, true));
            }
            $response = array(
                'status_code' => $e->getStatusCode(),
                'status' => Edgeconnect::RESULT_FAILURE,
                /*'exception' => $e,*/
                'message' => $e->getMessage(),
                'response_body' => $e->getBody()
            );
        }

        curl_close($ch);

        return $response;
    }

    /**
     * Check if locally force encryption
     *
     * @return bool
     */
    private function isForceEncrypt()
    {
        return $this->settings['protocol'] == 'http' && $this->settings['force_encrypt'];
    }

}


/**
 * Class EdgeconnectException
 *
 * @package Edm
 */
class EdgeconnectException extends Exception {

    /**
     * Create instance of EdgeconnectException
     *
     * @param string $message
     * @param null $status_code
     * @param null $body
     * @param null $json
     */
    public function __construct($message, $status_code = null, $body = null, $json = null)
    {
        parent::__construct($message);
        $this->statusCode = $status_code;
        $this->body = $body;
        $this->json = $json;
    }

    /**
     * Return the status
     *
     * @return null
     */
    public function getStatusCode() {
        return $this->statusCode;
    }

    /**
     * Get the body of the response
     *
     * @return null
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * Get the json of the response
     *
     * @return string
     */
    public function getJson() {
        return $this->json;
    }
}