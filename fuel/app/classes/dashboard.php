<?php
// /fuel/app/classes
class Dashboard {




    public static function get_visits($type = 'd') {
        $arr = array( 'd' => 'DAY', 'm' => 'MONTH', 'y' => 'YEAR', 'w' => 'WEEK'  );

        $sql = 'SELECT count( Distinct DAY(FROM_UNIXTIME(`times`)), ip ) as visits FROM `visits`';

        if ( $type = $arr[$type] ) {
            $sql .= ' WHERE '.$type.'(NOW()) = '.$type.'(FROM_UNIXTIME(`times`)) ';//' GROUP BY ip, DAY(FROM_UNIXTIME(`times`))';
        }
        
        
        return \DB::query($sql)->execute();
    }

    public static function get_order($type = 'd') {

        $arr = array( 'd' => 'DAY', 'm' => 'MONTH', 'y' => 'YEAR', 'w' => 'WEEK'  );
        $sql = 'SELECT count(*) as total_order, sum((total_price + total_delivery_charge + shipping_price) - discount_amount) as total_sales FROM `orders` WHERE finished = 1';

        if ( $type = $arr[$type] ) {
            $sql .= ' AND '.$type.'(NOW()) = '.$type.'(FROM_UNIXTIME(`created_at`))';
        }


        return \DB::query($sql)->execute();
    }


    public static function get_chart_order() {
        $results = \DB::query('SELECT m.MNTH, sum((orders.total_price + orders.total_delivery_charge + orders.shipping_price) - discount_amount) as total_price FROM (SELECT 1 AS MNTH UNION SELECT 2 AS MNTH UNION SELECT 3 AS MNTH UNION SELECT 4 AS MNTH UNION SELECT 5 AS MNTH UNION SELECT 6 AS MNTH UNION SELECT 7 AS MNTH UNION SELECT 8 AS MNTH UNION SELECT 9 AS MNTH UNION SELECT 10 AS MNTH
        UNION SELECT 11 AS MNTH UNION SELECT 12 AS MNTH) AS m
        LEFT JOIN orders ON m.MNTH = MONTH(FROM_UNIXTIME(orders.created_at)) WHERE finished = 1 AND YEAR(FROM_UNIXTIME(orders.created_at)) = YEAR(NOW()) GROUP BY m.MNTH')->execute();

        return $results;
    }

    public static function get_chart_visits() {
        $results = \DB::query('SELECT m.MNTH, count(*) as visits FROM (SELECT 1 AS MNTH UNION SELECT 2 AS MNTH UNION SELECT 3 AS MNTH UNION SELECT 4 AS MNTH UNION SELECT 5 AS MNTH UNION SELECT 6 AS MNTH UNION SELECT 7 AS MNTH UNION SELECT 8 AS MNTH UNION SELECT 9 AS MNTH UNION SELECT 10 AS MNTH
        UNION SELECT 11 AS MNTH UNION SELECT 12 AS MNTH) AS m
        LEFT JOIN visits ON m.MNTH = MONTH(FROM_UNIXTIME(visits.times)) WHERE YEAR(FROM_UNIXTIME(visits.times)) = YEAR(NOW()) GROUP BY m.MNTH')->execute();

        return $results;
    }

    public static function log_visitor() {
        $ip     = self::get_client_ip();
        $brow   = self::get_client_browser();

        $brow = $brow['name'];

        $props = array( 'ip' => $ip, 'brow' => $brow, 'times' => time() );
        
        \DB::insert('visits')->set($props)->execute();
    }
    /**
     *Get client ip address
     *
     *@param none
     *@return $ip(string) - client ip address
     */
    public static function get_client_ip() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    /**
     *Get client browser information
     *
     *@param none
     *@return array - browser information
     */
    public static function get_client_browser() 
    { 
        $u_agent = $_SERVER['HTTP_USER_AGENT']; 
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        }
        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        }
        elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
        
        // Next get the name of the useragent yes seperately and for good reason
        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
        { 
            $bname = 'Internet Explorer'; 
            $ub = "MSIE"; 
        } 
        elseif(preg_match('/Firefox/i',$u_agent)) 
        { 
            $bname = 'Mozilla Firefox'; 
            $ub = "Firefox"; 
        } 
        elseif(preg_match('/Chrome/i',$u_agent)) 
        { 
            $bname = 'Google Chrome'; 
            $ub = "Chrome"; 
        } 
        elseif(preg_match('/Safari/i',$u_agent)) 
        { 
            $bname = 'Apple Safari'; 
            $ub = "Safari"; 
        } 
        elseif(preg_match('/Opera/i',$u_agent)) 
        { 
            $bname = 'Opera'; 
            $ub = "Opera"; 
        } 
        elseif(preg_match('/Netscape/i',$u_agent)) 
        { 
            $bname = 'Netscape'; 
            $ub = "Netscape"; 
        } 
        
        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }
        
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
                $version= $matches['version'][0];
            }
            else {
                $version= $matches['version'][1];
            }
        }
        else {
            $version= $matches['version'][0];
        }
        
        // check if we have a number
        if ($version==null || $version=="") {$version="?";}
        
        return array(
            'userAgent' => $u_agent,
            'name'      => $bname,
            'version'   => $version,
            'platform'  => $platform,
            'pattern'    => $pattern
        );
    } 



}