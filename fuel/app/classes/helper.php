<?php

/**
 * Helper class
 *
 * @package    CMS
 * @version    2.0
 * @author     CMS Development Team
 */
class Helper {
    
    static public $sorting = array();

    public static function truncate($str = false, $length = false, $end_string = '...') 
    {
        $str = strip_tags($str);
        $str = str_replace(array("\r", "\r\n", "\n"), '', $str);
        if (strlen($str) < $length)
            return $str;
        $position = strrpos(substr($str, 0, $length), ' ');
        return substr($str, 0, $position) . $end_string;
    }

    public static function truncate_decode($str = false, $length = false, $end_string = '...') 
    {
        $str = trim(strip_tags(html_entity_decode($str, ENT_COMPAT, 'utf-8')));
        $str = str_replace(array("\r", "\r\n", "\n"), '', $str);
        if (strlen($str) < $length)
            return $str;
        $position = strrpos(substr($str, 0, $length), ' ');
        return substr($str, 0, $position) . $end_string;
    }

    public static function numbers_array($from = false, $to = false, $order = 'asc', $leading_zeros = 0, $multiplier = false, $suffix = '') 
    {
        $numbers = array();
        for ($i = $from; $i <= $to; $i++) {
            if (is_numeric($multiplier))
                $num = $i * $multiplier;
            else
                $num = $i;

            $value = $num;
            if ($leading_zeros > 0) {
                $num = sprintf("%0" . $leading_zeros . "d", $num);
                $value = $num;
            }

            if (!empty($suffix)) {
                $value .= ' ' . $suffix;
            }

            $numbers[$num] = $value;
        }

        if ($order == 'desc')
            arsort($numbers);
        return $numbers;
    }

    public static function sort_array_by_array($array, $order_array) 
    {
        $out = array();
        foreach ($order_array as $key) {
            if (isset($array[$key])) {
                $out[$key] = $array[$key];
                unset($array[$key]);
            }
        }
        return $out + $array;
    }

    public static function array_to_list($array, $parent = 0)
    {
        $r = '';
        foreach ($array as $item) {
            if ($item->parent_id == $parent) {
                $r = $r . "<li>" . $item->title . self::array_to_list($array, $item->id) . "</li>";
            }
        }
        return ($r == '' ? '' : "<ul>" . $r . "</ul>");
    }

    public static function array_to_string($array, $key_value_joiner = ': ', $item_joiner = ' &#124; ') 
    {
        $out = '';
        if (is_array($array)) {
            $items = array();
            foreach ($array as $key => $value) {
                $items[] = $key . $key_value_joiner . $value;
            }
            if ($items)
                $out = implode($item_joiner, $items);
        }
        return $out;
    }

    public static function urlencodeall($x) 
    {
        $out = '';
        for ($i = 0; isset($x[$i]); $i++) {
            $c = $x[$i];
            if (!ctype_alnum($c))
                $c = '%' . sprintf('%02X', ord($c));
            $out .= $c;
        }
        return $out;
    }
    
    public static function total_price($gst_prices = 0, $non_gst_prices = 0) 
    {
       $gst = 10;
       $gst_cal = $gst / 100;//+ 1;
       
       if(is_array($gst_prices)) $total_gst_prices = array_sum($gst_prices);
       else $total_gst_prices = $gst_prices;
       
       if(is_array($non_gst_prices)) $total_non_gst_prices = array_sum($non_gst_prices);
       else $total_non_gst_prices = $non_gst_prices;
       
       $total_price = $total_gst_prices + $total_non_gst_prices;
       
       $total_price = new stdClass;
       
       $total_price->gst_percent = $gst;
       $total_price->gst_percent_nf = number_format($gst, 2);
       $total_price->gst = ( $total_gst_prices + $total_non_gst_prices ) * $gst_cal; //$total_gst_prices - ($total_gst_prices / $gst_cal);
       $total_price->gst_nf = number_format($total_price->gst, 2);
       $total_price->total_price = $total_gst_prices;
       $total_price->total_price_nf = number_format($total_price->total_price, 2);
       $total_price->grand_total = $total_gst_prices + $total_non_gst_prices + $total_price->gst;
       $total_price->grand_total_nf = number_format($total_price->grand_total, 2);
       
       //NRB-Gem: gst_prices is already inclusive of GST
       $total_price->gst_inclusive = $total_gst_prices - ($total_gst_prices/1.1);
       
       return $total_price;
           
    }
    
    public static function sort_objects($a, $b) 
    {
        if(static::$sorting)
        {
            $sorting = static::$sorting;
            $property = $sorting[0];
            $num1 = 1;
            $num2 = -1;
            if($sorting[1] == 'asc')
            {
                $num1 = -1;
                $num2 = 1;
            }
            if($a->$property == $b->$property){ return 0 ; }
            return ($a->$property < $b->$property) ? $num1 : $num2;
        }
        else 
        {
            return 0;
        }
    }
    
    public static function sorting(&$items = array())
    {
        if(\Input::get('sort') == 'title_desc') $sorting = array('title', 'desc');
        if(\Input::get('sort') == 'title_asc') $sorting = array('title', 'asc');

        if(isset($sorting))
        {
           \Helper::$sorting = $sorting;
            usort($items, array('\Helper', 'sort_objects')); 
        }
    }

    public static function scanDirectories($rootDir, $allData=array()) {
        // set filenames invisible if you want
        $invisibleFileNames = array(".", "..", ".htaccess", ".htpasswd");
        // run through content of root directory
        $dirContent = scandir($rootDir);
        foreach($dirContent as $key => $content) {
            // filter all files not accessible
            $path = $rootDir.'/'.$content;
            if(!in_array($content, $invisibleFileNames)) {
                // if content is file & readable, add to array
                if(is_file($path) && is_readable($path)) {
                    // save file name with path
                    $allData[] = '/'.$path;
                }
            }
        }
        return $allData;
    }

    /**
     * Check if a given menu(s) matches the selected
     *
     * @param string|array
     * @return false|string
     */
    public static function menuIsActive($menu, $selected, $class='active')
    {
        $menu = is_array($menu) ? $menu : [$menu];

        // match advanced url that uses regular expression
        foreach ($menu as $v) {
            if ($v == $selected) {
                return $class;
            }
        }

        return false;
    }

    public static function parse_info_table($table)
    {
        $table = trim($table, "\n\r ");
        $items = array();
        
        if(!empty($table))
        {
            $items = preg_split ('/$\R?^/m', $table);
            
            $table_counter  = 0;
            $row_counter    = 0;
            $tmp_tables     = array();
            $maximum        = array();
            
            // Parse table line by line and fill tables and regular content
            foreach($items as $key => $item)
            {
                $item = trim($item);
                
                if(trim($item, '=') == 'table')
                {
                    if($table_counter%2 == 0)
                    {
                        // Table open
                        $items[$key] = 'table_'.$table_counter;
                        $table_counter++;
                    }
                    else
                    {
                        // Table close
                        $table_counter++;
                        unset($items[$key]);
                    }
                }
                else
                {
                    if($table_counter%2 == 1)
                    {
                        // Table fill
                        if(isset($item[0]) && $item[0] == '#') 
                        {
                            // Fill table headers
                            if(!isset($tmp_tables[$table_counter-1]['tbody']))
                                $tmp_tables[$table_counter-1]['thead'][$row_counter][] = substr($item, 1);
                            else    
                                $tmp_tables[$table_counter-1]['tbody'][$row_counter][] = substr($item, 1);
                        }
                        elseif($item != '')
                        {
                            // Fill table rows
                            $tmp_tables[$table_counter-1]['tbody'][$row_counter][] = $item;
                        }
                        else
                        {
                            // Empty row means we have new table row
                            $row_counter++;
                        }
                        
                        unset($items[$key]);
                        
                    }
                    else
                    {
                        // Content fill
                        // Content is already copied as it is, so we dont need to do anything here
                    }
                }
                
            }
            
            // If we have any tables in our content we need to generate their HTML
            if(!empty($tmp_tables))
            {
                // Get maximum number of rows in one column (as table is simetric)
                foreach($tmp_tables as $key => $tmp_table)
                {
                    $maximum[$key] = 0;
                    
                    foreach($tmp_table as $tmp_part)
                        foreach($tmp_part as $tmp_row)
                            if(count($tmp_row) > $maximum[$key]) $maximum[$key] = count($tmp_row);
                }
                
                // Set all other columns to have same number of rows as maximum
                foreach($tmp_tables as $key => $tmp_table)
                {
                    foreach($tmp_table as $key1 => $tmp_part)
                        foreach($tmp_part as $key2 => $tmp_row)
                            if(count($tmp_row) < $maximum[$key])
                            {
                                for($i = 0; $i < $maximum[$key] - count($tmp_row); $i++)
                                {
                                    $tmp_tables[$key][$key1][$key2][] = '';
                                }
                            }
                }
                
                // Generate table HTML
                foreach($tmp_tables as $key => $tmp_table)
                {
                    $tables[$key] = '<table class="table table-bordered table-striped table-condensed">';
                        
                        if(isset($tmp_table['thead']))
                        {
                            $tables[$key] .= '<thead>';
                            
                            foreach($tmp_table['thead'] as $tmp_thead)
                            {
                                $tables[$key] .= '<tr>';
                                foreach($tmp_thead as $tmp_column)
                                {
                                    $tables[$key] .= '<th>' . $tmp_column . '</th>';
                                }
                                $tables[$key] .= '</tr>';
                            }
                            
                            $tables[$key] .= '</thead>';
                        }
                        
                        if(isset($tmp_table['tbody']))
                        {
                            $tables[$key] .= '<tbody>';
                            
                            foreach($tmp_table['tbody'] as $tmp_tbody)
                            {
                                $tables[$key] .= '<tr>';
                                foreach($tmp_tbody as $tmp_column)
                                {
                                    $tables[$key] .= '<td>' . $tmp_column . '</td>';
                                }
                                $tables[$key] .= '</tr>';
                            }
                            
                            $tables[$key] .= '</tbody>';
                        }
                        
                    $tables[$key] .= '</table>';
                }
                
                // Return table HTMLs inside original array
                foreach($tables as $key => $table)
                {
                    foreach($items as $key1 => $item)
                    {
                        if($item == 'table_'.$key)
                            $items[$key1] = $table;
                    }
                }
            }
            
            // Recalculate array keys
            $items = array_values($items);
        }
        
        return !empty($items) ? implode('<br />', $items) : '';
    }

    public static function dmyToymd($dmy_date, $separator)
    {
        return strtotime(date('Y-m-d', strtotime(str_replace($separator, '-', $dmy_date))));
    }

    // returns gst value
    public static function calculateGST($subtotal, $gst_percentage)
    {
        $final_gst = 0;

        if($gst_percentage > 0)
        {
            $gst =  $subtotal * floatval($gst_percentage/100);
            if($gst > 0)
            {
                $gst_inclusive_value = $subtotal + $gst;
                $by = $gst_inclusive_value / $gst;
                $final_gst = $subtotal / $by;

                $final_gst = round($final_gst, 2);
            }
        }

        return $final_gst;
    }

    public static function uploadFileToAmazonS3($file_info = false, $settings = false)
    {
        if(!$settings) $settings = \Config::load('amazons3.db');

        $hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
        $hold_amazon_key = (isset($settings['amazon_key']) ? $settings['amazon_key'] : '');
        $hold_amazon_secret = (isset($settings['amazon_secret']) ? $settings['amazon_secret'] : '');
        $hold_amazon_site = (isset($settings['amazon_site']) ? $settings['amazon_site'] : '');
        $hold_amazon_bucket = (isset($settings['amazon_bucket']) ? $settings['amazon_bucket'] : '');
        $hold_amazon_region = (isset($settings['amazon_region']) ? $settings['amazon_region'] : '');

        if($hold_amazon_enable && $file_info)
        {
            try
            {
                $savePath = $file_info['savePath'];
                $filePath = $file_info['filePath'];
                $contentType = $file_info['contentType'];

                $s3 = new Aws\S3\S3Client([
                    'version' => 'latest',
                    'region'  => $hold_amazon_region,
                    'credentials' => array(
                        'key'    => $hold_amazon_key,
                        'secret' => $hold_amazon_secret,
                    )
                ]);

                $result = $s3->putObject([
                    'Bucket'       => $hold_amazon_bucket,
                    'Key'          => $savePath,
                    'SourceFile'   => $filePath,
                    'ContentType'  => $contentType,
                    'ACL'          => 'public-read',
                ]);
            }
            catch(Exception $e) {}
        }
    }

    public static function deleteFileFromAmazonS3($fileLink = false, $settings = false)
    {
        if(!$settings) $settings = \Config::load('amazons3.db');

        $hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
        $hold_amazon_key = (isset($settings['amazon_key']) ? $settings['amazon_key'] : '');
        $hold_amazon_secret = (isset($settings['amazon_secret']) ? $settings['amazon_secret'] : '');
        $hold_amazon_site = (isset($settings['amazon_site']) ? $settings['amazon_site'] : '');
        $hold_amazon_bucket = (isset($settings['amazon_bucket']) ? $settings['amazon_bucket'] : '');
        $hold_amazon_region = (isset($settings['amazon_region']) ? $settings['amazon_region'] : '');

        if($hold_amazon_enable && $fileLink)
        {
            try
            {
                // Connect to S3 Amazon Server
                $s3 = new Aws\S3\S3Client([
                    'version' => 'latest',
                    'region'  => $hold_amazon_region,
                    'credentials' => array(
                        'key'    => $hold_amazon_key,
                        'secret' => $hold_amazon_secret,
                    )
                ]);

                // Delete in aws s3
                $s3->deleteObject(array(
                    'Bucket'  => $hold_amazon_bucket,
                    'Key' => $fileLink
                ));
            }
            catch(Exception $e) {}
        }
    }

    public static function amazonFileURL($fileLink = false)
    {
        $settings = \Config::load('amazons3.db');

        $hold_amazon_enable = (isset($settings['amazon_enable']) ? 1 : 0);
        if($hold_amazon_enable && $fileLink)
        {
            $hold_amazon_site = (isset($settings['amazon_site']) ? $settings['amazon_site'] : '');
            return $hold_amazon_site.$fileLink;
        }
        else
        {
            return \Uri::create($fileLink);
        }
    }
}
