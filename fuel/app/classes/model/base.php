<?php
class Model_Base extends \Model_Crud
{
	/**
	 * Fetch Array of key=>value to use for select
	 * 
	 * @param $field_first		= Key column name
	 * @param $field_second		= Value column name
	 * @param $config			= Additional query conditions
	 * @param $default			= Default selected value (first value in array) EXAMPLE: array('0' => 'Select values')
     * @param $override         = Override search array with your own array of values
	 */
	public static function fetch_pair($field_first, $field_second, array $config=array(), $default = false, $override = false)
	{
		$fetchpair = $default ? array(0 => $default) : array();
		$fetchpair = is_array($default) ? $default : $fetchpair;
		
        if(!is_array($override))
        {
            if (!empty($config))
            {
                $rows = parent::find($config);
            }
            else 
            {
                $rows = parent::find();
            }		
        }
        else
        {
            $rows = $override;
        }

		if (!empty($rows))
		{
			foreach ($rows as $row)
			{
				if(is_array($row)) 
				{
					$fetchpair[$row[$field_first]] = $row[$field_second];
				}
				else
				{
					$fetchpair[$row->{$field_first}] = $row->{$field_second};	
				}
					
			}
		}
		return $fetchpair;
	}
    
    public static function check_logged()
    {
        $logged = true;
        
        if (!(\Sentry::check() && !\Sentry::user()->is_admin()))
        {
            $logged = false;
        }
        
        return $logged;
    }
	
	/**
	 * Get Model protected properties
	 * 
	 * @param $property	= Property name
	 */
	public static function get_protected($property)
	{
		return isset(static::$$property) ? static::$$property : false;
	}
	
	/**
	 * MAGIC METHODS USED FOR LAZY LOADING
	 */
	public function __isset($property)
    {
    	return array_key_exists('get_' . $property, $this);
    }
    
    public function __get($property)
    {
    	if (array_key_exists('get_' . $property, $this))
    	{
	    	if(is_callable(array($this, 'get_' . $property))) {
	            return call_user_func_array($this->{'get_' . $property}, array());
	        }
    	}

    	if (isset($this->{$property}))
		{
			return $this->{$property};
		}

		try
		{
			return parent::__get($property);
		}
		catch(Exception $e)
		{
			
		}
    }
	
    public function __set($property, $value)
    {
    	$this->$property = $value;
    	parent::__set($property, $value);
    }
	
	public function __call($method, $args)
    {
        if(is_callable(array($this, $method))) {
            return call_user_func_array($this->$method, $args);
        }
    }
    
    public function save($validate = true)
	{
		if (isset($this->{static::primary_key()}) || isset($this->_data[static::primary_key()]))
		{
			$this->is_new(false);
		}
		return parent::save($validate);
	}
  
}