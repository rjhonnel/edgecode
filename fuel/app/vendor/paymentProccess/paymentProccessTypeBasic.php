<?php

namespace PaymentProccess;

interface iPaymentProccessType
{
    /**
     *  Load environment from config which basically means what service to use: test or live, what user, pass etc  
     */
    public function setEnvironment();
    
    /**
     * Proccess Payment differs from payment type.
     * In Bank Transfer case for example we'll just send email to customer
     */
    public function proccessPayment();
    
    /**
     * Notify user about successfullness of payment
     */
    public function notify();
}


class PaymentProccessTypeBasic
{
    /**
     * Credit card details, and type of payment
     * @var array
     */
    protected $postPaymentData;
    
    /**
     *  Proccess payment for this order
     * 
     * @var object \Order\Model_Order
     */
    protected $order;
    
    /**
     * Payment data stored in config. ( Payment usernames, returnUrls ..)
     * 
     */
    protected $config;
    
    /**
     * Save initial payment with creation of this object
     * 
     * @param array $postData = Data from post ( payment data, cc number, payment method etc...)
     * @param \Order\Model_Order object $order
     */
    public function __construct($postPaymentData, $order) 
    {
        $this->order = $order;
        $this->postPaymentData = $postPaymentData;
        
        $type = $postPaymentData['payment_type'];
        
        // Create and save initial payment to database
        $paymentDbOject = new PaymentSaver();
        $paymentDbOject->saveInitialPayment($type, $order, $this->getOrderTotal());
    }
    
    public function getOrderTotal()
    {
        $total = 0;
        if(\SentryAdmin::user()->is_admin())
        {
            $payment_settings = \Config::load('payment_settings.db');
            $hold_enable_partial_payment = (isset($payment_settings['enable_partial_payment']) ? $payment_settings['enable_partial_payment'] : 0);
            if($hold_enable_partial_payment)
                $total = $this->order->last_payment->total_price;
            else
                $total = $this->order->total_price();
        }
        else
            $total = $this->order->total_price();

        return (float)$total;
    }
    
    /**
     * Call PaymentSaver class to save payment
     * 
     * @param string $status            
     * @param string $statusDetail
     * @param mixed  $transactionInfo
     */
    public function savePayment( $status, $statusDetail, $transactionInfo = array()) {
        $paymentSaver = new PaymentSaver();
        $paymentSaver->savePaymentDetails($this->config['code'], $this->order, $status, $statusDetail, $transactionInfo);
    }
    
}