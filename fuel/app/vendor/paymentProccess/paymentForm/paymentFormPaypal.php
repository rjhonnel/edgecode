<?php

namespace PaymentProccess\PaymentForm;

class PaymentFormPaypal extends PaymentForm
{
    public function __construct($paymentData, $order) {
        parent::__construct($paymentData, $order);
    }
    
    /**
     * Create simple HTML form using paypal documentation
     * @return string
     */
    public function createForm(){
	
		// Form to submit to PayPal 
		// https://www.x.com/developers/paypal/documentation-tools/paypal-payments-standard/integration-guide/Appx_websitestandard_htmlvariables
		$form = '<form action="' . $this->paymentConnfigData['url'] . '" method="post" id="payment_form" target="_parent">';
	
		// Technical HTML Variables
//		$form .= \Form::hidden('cmd', '_cart');
		$form .= \Form::hidden('cmd', '_xclick');
		$form .= \Form::hidden('notify_url', $this->paymentConnfigData['notify_url']);
	
			
		// HTML Variables for Payment Transactions
		$form .= \Form::hidden('invoice', $this->order->id);
		$form .= \Form::hidden('currency_code', 'AUD');
	
		// HTML Variables for Shopping Carts
		$form .= \Form::hidden('business', $this->paymentConnfigData['business']);
		//$form .= \Form::hidden('upload', '1');
	
//		foreach ($this->order->products as $key => $product)
//		{
//			$form .= \Form::hidden('item_name_' . ($key+1), $product->product_name);
//			//$form .= \Form::hidden('item_number_' . $key, $product->id);
//			$form .= \Form::hidden('amount_' . ($key+1),    $product->price);
//			$form .= \Form::hidden('quantity_' . ($key+1),  $product->qty);
//				
//        }
        $form .= \Form::hidden('amount', $this->order->total_price + $this->order->shipping_price);
        //$form .= \Form::hidden('handling_cart', );
        
		$form .= \Form::hidden('first_name', $this->order->billing_first_name);
		$form .= \Form::hidden('last_name', $this->order->billing_last_name);
	
		// HTML Variables for Displaying PayPal Checkout Pages
		$form .= \Form::hidden('lc', 'AU');
		$form .= \Form::hidden('return', $this->paymentConnfigData['return']);
		
		$form .= \Form::hidden('rm', 2);
	
		$form .= '</form>';
	
        return $form;
        
    }
    
}