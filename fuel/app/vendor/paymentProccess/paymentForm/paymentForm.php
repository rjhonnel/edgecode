<?php

namespace PaymentProccess\PaymentForm;

class PaymentForm
{
    /**
     *  Payment config data. ( Seller usernames, returnUrl addresses etc)
     * @var array 
     */
    protected $paymentConnfigData;
    
    /**
     *  Order object
     * @var \Order\Model_Order object
     */
    protected $order;
    
    public function __construct($paymentConnfigData,$order) {
        $this->order        = $order;
        $this->paymentConnfigData  = $paymentConnfigData;
    }
    
}