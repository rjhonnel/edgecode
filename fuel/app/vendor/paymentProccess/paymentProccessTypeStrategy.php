<?php

namespace PaymentProccess;

class PaymentProccessTypeStrategy
{
    protected static $paymentProccessType;
    
    public function __construct($postPaymentData, $order) {
        static::$paymentProccessType = PaymentProccessTypeFactory::forgePaymentType($postPaymentData, $order);
    }
    
    /**
     * Proccess payment by calling proccessPayment method of each separete paymentProccessType
     * thus simulating strategy patern 
     */
    public function proccessPayment()
    {
        return static::$paymentProccessType->proccessPayment();
    }
    
    /**
     * Fetch PaymentProccessType class name that we just used to proccess payment.
     * @return string = Name of PaymentProccessType class 
     */
    public function fetchTypeOfProccess()
    {
        return get_class(static::$paymentProccessType);
    }
    
}