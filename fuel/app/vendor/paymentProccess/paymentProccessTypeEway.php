<?php


namespace PaymentProccess;

class PaymentProccessTypeEway extends PaymentProccessTypeBasic implements iPaymentProccessType
{
    public function __construct($postData, $order) 
    {
        parent::__construct($postData, $order);
        $this->setEnvironment();
    }

    public function proccessPayment() {
        
        $gateway = \Omnipay\Omnipay::create('Eway_RapidDirect');
        $gateway->initialize(array(
            'apiKey' => $this->config['apiKey'],
            'password' => $this->config['apiPassword'],
            'testMode' => $this->config['mode'],
        ));
 
        $options = array(
            'amount' => $this->getOrderTotal(),
            'card' => new \Omnipay\Common\CreditCard(array(
               'firstName' => $this->order->first_name,
               'lastName' => $this->order->last_name,
               'number' => $this->postPaymentData['ccnumber'],
               'expiryMonth' => $this->postPaymentData['exmonth'],
               'expiryYear' => $this->postPaymentData['exyear'],
               'cvv' => $this->postPaymentData['cvv'],
        )),
            'transactionId' => $this->order->id,
            'currency' => 'AUD',
        );

        try {
            $response = $gateway->purchase($options)->send();
            if ($response->isSuccessful()) {
                $this->savePayment('completed', 'Transaction Completed', 'Transaction ID = '.$response->getTransactionReference());
            } else {
                $this->savePayment('failed', 'Transaction failed', $response->getMessage());

                \Messages::error($response->getMessage());
                \Response::redirect(\Uri::create($this->config['return_edit_url'].'/'.$this->order->id.'?step=3'));
            }
        } catch (\Exception $e) {    
            \Messages::error('Sorry, there was an error processing your payment. Please try again later.<br>'.$e->getMessage());
            \Response::redirect(\Uri::create($this->config['return_edit_url'].'/'.$this->order->id.'?step=3'));
        }
        
    }
    
    
    public function notify() {

    }
    
    public function setEnvironment() {

        $this->config = $this->fetchConfig();
    }
    
    /**
     * Just fetch Payment config file
     */
    private function fetchConfig(){
        
        $settings = \Config::load('eWay.db');
        \Config::load('payment::eway', 'details', true, true);
        
        // Active config
        $active = \Config::get('details.config.active', 'test');
        $config = \Config::get('details.config.' . $active, false);

        // override
        $customerId = $settings['customerId_test'];
        $apiKey = $settings['username_test'];
        $apiPassword = $settings['password_test'];
        if($settings['mode'] == 1)
        {
            $customerId = $settings['customerId_live'];
            $apiKey = $settings['username_live'];
            $apiPassword = $settings['password_live'];
        }
        $config['mode'] = ($settings['mode'] == 0 ? true:false);
        $config['customerId'] = $customerId;
        $config['apiKey'] = $apiKey;
        $config['apiPassword'] = $apiPassword;

        $config['code'] = (isset($settings['code']) ? $settings['code'] : 'eway');
        
        if($config === false)
        {
            throw new \Exception('Invalid payment config.');
        }
        
        return $config;
    }
}