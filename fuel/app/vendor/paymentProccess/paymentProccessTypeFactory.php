<?php

namespace PaymentProccess;

class PaymentProccessTypeFactory
{
    public static function forgePaymentType( $postPaymentData, $order ) 
    { 
        $type = $postPaymentData['payment_type'];
        
        $className = '\PaymentProccess\PaymentProccessType'.ucfirst($type);
        
        if( ! class_exists($className))
            throw new \Exception ('PaymentProccess class does not exists');
        
        return new $className( $postPaymentData, $order );
    }
    
}