<?php


namespace PaymentProccess;

class PaymentProccessTypeSecurePay extends PaymentProccessTypeBasic implements iPaymentProccessType
{
    public function __construct($postData, $order) 
    {
        parent::__construct($postData, $order);
        $this->setEnvironment();
    }

    public function proccessPayment() {

        $gateway = \Omnipay\Omnipay::create('SecurePay_DirectPost');
        $gateway->setMerchantId($this->config['merchatId']);
        $gateway->setTransactionPassword($this->config['password']);
        
        $gateway->setTestMode($this->config['mode']);

           $options = array(
            'amount' => $this->getOrderTotal(),
            'card' => new \Omnipay\Common\CreditCard(array(
               'firstName' => $this->order->first_name,
               'lastName' => $this->order->last_name,
               'number' => $this->postPaymentData['ccnumber'],
               'expiryMonth' => $this->postPaymentData['exmonth'],
               'expiryYear' => $this->postPaymentData['exyear'],
               'cvv' => $this->postPaymentData['cvv'],
           )),
            'returnUrl' => $this->config['return_url'].'/'.$this->order->id,
            'cancelUrl' => $this->config['cancel_url'].'/'.$this->order->id,
            'transactionId' => $this->order->id,
            'currency' => 'AUD',
        );

        try
        {
            $response = $gateway->purchase($options)->send();
            if ($response->isSuccessful()) {
                $this->savePayment('completed', 'Transaction Completed', $response->getMessage());
            } elseif ($response->isRedirect()) {
                $response->redirect();
            } else {
                $this->savePayment('failed', 'Transaction failed', $response->getMessage());

                \Messages::error($response->getMessage());
                \Response::redirect($this->config['return_edit_url'].'/'.$this->order->id.'?step=3');
            }
        } catch (\Exception $e) {
            \Messages::error('Sorry, there was an error processing your payment. Please try again later.<br>'.$e->getMessage());
            \Response::redirect($this->config['return_edit_url'].'/'.$this->order->id.'?step=3');
        }

    }
    
    
    public function notify() {

    }
    
    public function setEnvironment() {

        $this->config = $this->fetchConfig();
    }
    
    /**
     * Just fetch Payment config file
     */
    private function fetchConfig(){
        
        $settings = \Config::load('securePay.db');
        \Config::load('payment::securepay', 'details', true, true);
        
        // Active config
        $active = \Config::get('details.config.active', 'test');
        $config = \Config::get('details.config.' . $active, false);

        // override
        $merchatId = $settings['merchantId_test'];
        $password = $settings['password_test'];
        if($settings['mode'] == 1)
        {
            $merchatId = $settings['merchantId_live'];
            $password = $settings['password_live'];
        }
        $config['mode'] = ($settings['mode'] == 0 ? true:false);
        $config['merchatId'] = $merchatId;
        $config['password'] = $password; 

        $config['code'] = (isset($settings['code']) ? $settings['code'] : 'securepay');
        
        if($config === false)
        {
            throw new \Exception('Invalid payment config.');
        }
        
        return $config;
    }
}