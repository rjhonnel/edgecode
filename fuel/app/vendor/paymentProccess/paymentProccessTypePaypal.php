<?php


namespace PaymentProccess;

class PaymentProccessTypePaypal extends PaymentProccessTypeBasic implements iPaymentProccessType
{
    public function __construct($postData, $order) 
    {
        parent::__construct($postData, $order);
        $this->setEnvironment();
    }

    /**
     * Proccess payment : create paypal form and return it 
     * @return type
     */
    public function proccessPayment() {

        $config = array (
		 	'mode' => $this->config['mode'],
		 	'acct1.UserName' => $this->config['user_name'],
			'acct1.Password' => $this->config['password'], 
			'acct1.Signature' => $this->config['signature']
		);
		$paypalService = new \PayPal\Service\PayPalAPIInterfaceServiceService($config);
		$paymentDetails= new \PayPal\EBLBaseComponents\PaymentDetailsType();

		$itemDetails = new \PayPal\EBLBaseComponents\PaymentDetailsItemType();
		$itemDetails->Name = 'Order ID: '.$this->order->id;
		$itemAmount = $this->getOrderTotal();
		$itemDetails->Amount = $itemAmount;
		$itemQuantity = 1;
		$itemDetails->Quantity = $itemQuantity;

		$paymentDetails->PaymentDetailsItem[0] = $itemDetails;

		$orderTotal = new \PayPal\CoreComponentTypes\BasicAmountType();
		$orderTotal->currencyID = $this->config['currency'];
		$orderTotal->value = $itemAmount * $itemQuantity; 

		$paymentDetails->OrderTotal = $orderTotal;
		$paymentDetails->PaymentAction = 'Sale';

		$setECReqDetails = new \PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType();
		$setECReqDetails->PaymentDetails[0] = $paymentDetails;
		$setECReqDetails->CancelURL = $this->config['cancel_url'].'/'.$this->order->id;
		$setECReqDetails->ReturnURL = $this->config['return_url'].'/'.$this->order->id;

		$setECReqType = new \PayPal\PayPalAPI\SetExpressCheckoutRequestType();
		$setECReqType->Version = $this->config['version'];
		$setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;

		$setECReq = new \PayPal\PayPalAPI\SetExpressCheckoutReq();
		$setECReq->SetExpressCheckoutRequest = $setECReqType;

		$setECResponse = $paypalService->SetExpressCheckout($setECReq);
		
		\Session::set('paypal.token', $setECResponse->Token);
		if($this->config['mode'] == 'sandbox')
			\Response::redirect(\PayPal\Core\PPConstants::IPN_SANDBOX_ENDPOINT.'?'.$this->config['param'].$setECResponse->Token);
		else
			\Response::redirect(\PayPal\Core\PPConstants::IPN_LIVE_ENDPOINT2.'?'.$this->config['param'].$setECResponse->Token);
    }
    
    /**
     * Because Paypal Ipn is redirected to \Payment\PayPal\ipn
     * There is no need to notify customer here, we'll do that in Ipn method of Payment module
     */
    public function notify() {

        $config = array (
		 	'mode' => $this->config['mode'],
		 	'acct1.UserName' => $this->config['user_name'],
			'acct1.Password' => $this->config['password'], 
			'acct1.Signature' => $this->config['signature']
		);
		$paypalService = new \PayPal\Service\PayPalAPIInterfaceServiceService($config);
		$getExpressCheckoutDetailsRequest = new \PayPal\PayPalAPI\GetExpressCheckoutDetailsRequestType(\Session::get('paypal.token'));
		$getExpressCheckoutDetailsRequest->Version = $this->config['version'];
		$getExpressCheckoutReq = new \PayPal\PayPalAPI\GetExpressCheckoutDetailsReq();
		$getExpressCheckoutReq->GetExpressCheckoutDetailsRequest = $getExpressCheckoutDetailsRequest;

		$getECResponse = $paypalService->GetExpressCheckoutDetails($getExpressCheckoutReq);

		// COMMIT THE PAYMENT
		$paypalService = new \PayPal\Service\PayPalAPIInterfaceServiceService($config);

		$paymentDetails = new \PayPal\EBLBaseComponents\PaymentDetailsType();
		
		$orderTotal = new \PayPal\CoreComponentTypes\BasicAmountType($this->config['currency'], $this->getOrderTotal());
		$paymentDetails->OrderTotal = $orderTotal;
		
		$paymentDetails->PaymentAction = 'Sale';
		$paymentDetails->NotifyURL = $this->config['notify_url'].'/'.$this->order->id;

		$DoECRequestDetails = new \PayPal\EBLBaseComponents\DoExpressCheckoutPaymentRequestDetailsType();
		$DoECRequestDetails->PayerID = $getECResponse->GetExpressCheckoutDetailsResponseDetails->PayerInfo->PayerID;
		$DoECRequestDetails->Token = $getECResponse->GetExpressCheckoutDetailsResponseDetails->Token;
		$DoECRequestDetails->PaymentDetails[0] = $paymentDetails;

		$DoECRequest = new \PayPal\PayPalAPI\DoExpressCheckoutPaymentRequestType();
		$DoECRequest->DoExpressCheckoutPaymentRequestDetails = $DoECRequestDetails;
		$DoECRequest->Version = $this->config['version'];

		$DoECReq = new \PayPal\PayPalAPI\DoExpressCheckoutPaymentReq();
		$DoECReq->DoExpressCheckoutPaymentRequest = $DoECRequest;
		
		$DoECResponse = $paypalService->DoExpressCheckoutPayment($DoECReq);
		
		if ($DoECResponse->Ack == 'Success')
		{
			$this->savePayment('Completed', 'Completed', $DoECResponse->toXMLString());
			\Response::redirect($this->config['notify_url'].'/'.$this->order->id);	
		}
		$this->savePayment('Failed', 'Transaction failed', $DoECResponse->Errors[0]->LongMessage);
		return true; // failed
    }
    
    /**
     *  Load environment from config which basically means what service to use: test or live, what user, pass etc  
     */
    public function setEnvironment() {
        
        $this->config = $this->fetchConfig();
        
    }
    
    /**
     * Just fetch Payment config file
     */
    private function fetchConfig(){
        
		$settings = \Config::load('paypal.db');
        \Config::load('payment::paypal', 'details', true, true);
        
		// Active config
        $active = \Config::get('details.config.active', 'test');
		$config = \Config::get('details.config.' . $active, false);

		// override
        $username = $settings['username_test'];
        $password = $settings['password_test'];
        $signature = $settings['signature_test'];
        if($settings['mode'] == 1)
        {
	        $username = $settings['username_live'];
	        $password = $settings['password_live'];
	        $signature = $settings['signature_live'];
        }
	 	$config['mode'] = ($settings['mode'] == 0 ? 'sandbox':'live');
	 	$config['user_name'] = $username;
		$config['password'] = $password; 
		$config['signature'] = $signature;

		$config['code'] = (isset($settings['code']) ? $settings['code'] : 'paypal');
		
        if($config === false)
        {
            throw new \Exception('Invalid payment config.');
        }
        
        return $config;
    }
}