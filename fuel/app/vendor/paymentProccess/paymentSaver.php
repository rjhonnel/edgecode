<?php

namespace PaymentProccess;

class PaymentSaver
{
    /**
     *  Payment object 
     * @var object \Payment\Model_Payment 
     */
    protected $payment;
    
    public function __construct() {
        
        $this->payment = \Payment\Model_Payment::forge();
        
    }

    /**
     * Save initial Payment details
     * @param string $type type of Payment to be saved 
     */
    public function saveInitialPayment($type, $order, $total_price = false)
    {
        $last_payments = \Payment\Model_Payment::find(array(
            'where' => array(
                'order_id' => $order->id,
            ),
            'order_by' => array(
                'id' => 'desc',
            ),
        ));

        if(count($last_payments))
            $this->payment = $last_payments[0];
        
        if(!$total_price)
            $order->total_price();

    	$this->payment->set(array(
    		'order_id'      => $order->id,
			'total_price'   => $total_price,
			'method'        => $type,
            'status'        => 'payment_selected',
			'status_detail' => 'Payment selected'
    	));
        $this->savePayment();
    }
 
    private function savePayment()
    {
        $this->payment->save();
    }
    
    public function savePaymentDetails($type, $order, $status, $statusDetail, $response=array())
    {
        // BOL - update number of stock for successful payment
        if($status == 'Completed')
        {
            foreach ($order->products as $product)
            {
                if($product->attributes_id)
                {
                    $get_attributes = \Product\Model_Attribute::find_one_by(array(
                        'product_id' => $product->product_id, 
                        'attributes' => $product->attributes_id
                    ));
                    $get_attributes->set(array(
                        'stock_quantity' => $get_attributes->stock_quantity - $product->quantity
                    ));                                
                    $get_attributes->save();
                }
                else
                {
                    $get_attributes = \Product\Model_Attribute::find_one_by_product_id($product->product_id);
                    $get_attributes->set(array(
                        'stock_quantity' => $get_attributes->stock_quantity - $product->quantity
                    ));                                
                    $get_attributes->save();
                }
            }
        }
        // EOL - update number of stock for successful payment

        $last_payments = \Payment\Model_Payment::find(array(
            'where' => array(
                'order_id' => $order->id,
            ),
            'order_by' => array(
                'id' => 'desc',
            ),
        ));

        if(count($last_payments))
            $this->payment = $last_payments[0];
        
    	$this->payment->set(array(
    		'order_id'        => $this->payment->order_id,
			'total_price'     => $this->payment->total_price,
			'method'          => $type,
			'status'          => $status,
			'status_detail'   => $statusDetail,
			'response'        => json_encode($response)
    	));
        $this->savePayment();
    }
    
}