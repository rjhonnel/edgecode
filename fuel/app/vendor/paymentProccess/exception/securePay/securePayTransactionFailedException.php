<?php

namespace PaymentProccess\Exception\SecurePay;

class SecurePayTransactionFailedException extends \Exception {}