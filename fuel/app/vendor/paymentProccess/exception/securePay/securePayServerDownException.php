<?php

namespace PaymentProccess\Exception\SecurePay;

class SecurePayServerDownException extends \Exception {}