<?php


namespace PaymentProccess;

use PaymentProccess\Exception\SecurePay\SecurePayServerDownException; 
use PaymentProccess\Exception\SecurePay\SecurePayTransactionFailedException;
use PaymentProccess\Exception\SecurePay\SecurePayCustomerDataInvalidException;


class PaymentProccessTypeCreditCard extends PaymentProccessTypeBasic implements iPaymentProccessType
{
    public function __construct($postData, $order) 
    {
        parent::__construct($postData, $order);
        $this->setEnvironment();
    }
    
    /**
     * Proccess SecurePayment. Try to complete SecurePay transaction
     * 
     * @throws SecurePayServerDownException
     * @throws SecurePayTransactionFailedException
     * @throws SecurePayCustomerDataInvalidException
     */
    public function proccessPayment() {
        
        $sp = new \SecurePay($this->config['merchantId'], $this->config['password'] );
        
        $sp->TestMode(TRUE); // Remove this line to actually preform a transaction
        
        if( ! $sp->TestConnection() )
        {
            $this->savePayment( 'Failed','SecurePay server is down');  
            throw new SecurePayServerDownException ('SecurePay server is down');
        }
       
        $sp->PreAuth = 1;   // Mark proccess as Pre Auth proccess ( With this one we must comunicate with SecurePay 
                            // server two times )
        
        $sp->Cc = $this->postPaymentData['ccnumber'];
        $sp->ExpiryDate = $this->postPaymentData['expmonth'].'/'.$this->postPaymentData['expyear'];
        $sp->Cvv = $this->postPaymentData['cvv'];
         
        $total_amount = (($this->order->total_price + $this->order->shipping_price) - $this->order->discount_amount);
        // NRB-Gem: total and shipping price are already inclusive of GST
        // $gst_amount = $total_amount * .10;
        // $sp->ChargeAmount = $total_amount + $gst_amount;

        $sp->ChargeAmount = $total_amount;
        $sp->ChargeCurrency = 'AUD';
       
        $sp->OrderId = $this->order->id;
        
        if ($sp->Valid()) 
        { // Is the above data valid?
            $response = $sp->Process();
            
//            echo $sp->ResponseXml; // Uncomment to see response
            
            if ($response == SECUREPAY_STATUS_APPROVED) 
            {
                $preauthid = $sp->PreAuthId;
                
            }else 
            {
                $this->savePayment( 'Failed','Transaction failed');
                // throw new SecurePayTransactionFailedException("Transaction authorisation failed with the error code: $response");
                throw new SecurePayTransactionFailedException("It appears you have not entered your information correctly. Please try again.");
            }
        }else 
        {
            $this->savePayment( 'Failed','Data invalid');   
            throw new SecurePayCustomerDataInvalidException("Your data is invalid!");
            
        }
        
        $sp->PreAuth = 1;
        $sp->PreAuthID = $preauthid;
        
        // NRB-Gem: total and shipping price are already inclusive of GST
        // $sp->ChargeAmount = $total_amount + $gst_amount;
        $sp->ChargeAmount = $total_amount;
        $sp->ChargeCurrency = 'AUD';
        $sp->OrderId = $this->order->id;
        
        if ($sp->Valid()) 
        { // Is the above data valid?
            $response = $sp->Process();
            
            if ($response == SECUREPAY_STATUS_APPROVED) {
               
                $txnData = array();
                if( !empty($sp->ResponseTree->Payment->TxnList->Txn) )
                {
                    // Get transaction data from server response
                    $txnData = $sp->ResponseTree->Payment->TxnList->Txn;
                    unset($txnData->CreditCardInfo);
                }
                
                $this->savePayment( 'Completed','Completed', $txnData);
                //$this->notify();
                
                
            }else 
            {
                // Save payment for this order as completed
                $this->savePayment( 'Failed','Transaction failed');
                throw new SecurePayTransactionFailedException("Transaction failed with the error code: $response");
            }
            
        } else 
        {
            $this->savePayment( 'Failed','Data invalid');
            throw new SecurePayCustomerDataInvalidException("Your data is invalid!");
        }
        
    }
    
    /**
     * Call PaymentSaver class to save payment
     * 
     * @param string $status            
     * @param string $statusDetail
     * @param mixed  $transactionInfo
     */
    public function savePayment( $status, $statusDetail, $transactionInfo = array()) {
        
        $paymentSaver = new PaymentSaver();
        $paymentSaver->savePaymentDetails('creditCard', $this->order, $status, $statusDetail, $transactionInfo);
        
    }
    
    /**
     * Send email to customer
     */
    public function notify() {
        //$emailer = new \Autorespondertrigger\Trigger();
        //$emailer->sendOrderPaymentReceivedPaypalCredit($this->order);
        /*$emailer = new PaymentEmailer($this->order);
        $emailer->send();*/
    }
    
    /**
     *  Load environment from config which basically means what service to use: test or live, what user, pass etc  
     */
    public function setEnvironment() {
        
        $this->config = $this->fetchConfig();
        
    }
    
    /**
     * Just fetch Payment config file
     */
    private function fetchConfig(){
        
        \Config::load('payment::securepay', 'details', true, true);
        
		// Active config
        $active = \Config::get('details.config.active', 'default');
		$config = \Config::get('details.config.' . $active, false);

        if($config === false)
        {
            throw new \Exception('Invalid payment config.');
        }
        
        return $config;
    }
    
    /**
     * Test to see if the connection is woriking
     * 
     * @param object $securePayObj
     */
    private function testServerConnection($securePayObj){
        
        if ( ! $securePayObj->TestConnection()) {
            throw new \Exception('SecurePay server is down.');
        }
        
    }
}