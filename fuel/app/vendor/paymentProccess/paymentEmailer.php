<?php

namespace PaymentProccess;

class PaymentEmailer
{
    protected $emailData;
    
    public function __construct($order)
    {
        \Package::load('email');
        
		// Load email addresses from config (these will be bcc receivers)
		\Config::load('auto_response_emails', 'autoresponders');
			
		$bcc = \Config::get('autoresponders.order_emails', false);
		if(!$bcc) $bcc = \Config::get('autoresponders.default_emails', false);
        
        $this->emailData = $this->createEmailData($order,$bcc);
    }
    
    private function createEmailData($order,$bcc)
    {
        return  array(
            'order'         => $order,
            'products'      => $order->products,
            'site_title'    => \Config::get('site_title'),
            'bcc'           => $bcc
		);
    }
    
    public function send()
    {
		$email = \Email::forge();
        
        $order = $this->emailData['order'];
        
		$email->to($order->shipping_email, ucwords($order->shipping_first_name . ' ' . $order->shipping_last_name));
		if($this->emailData['bcc']) $email->bcc($this->emailData['bcc']);
		$email->subject($this->emailData['site_title']. ' - Your Order');
		
        $autoresponder_body = \Theme::instance()->view('views/_email/order_confirmation')->set('emailData', $this->emailData, false);
        
		$emailHtml = \Theme::instance()->view('views/_email/autoresponder')->set('autoresponder_body',$autoresponder_body);;

		$email->html_body($emailHtml);

		try
		{
			$email->send();
			//\Messages::success('A copy of your request has been sent to ' . $this->emailData['order']['billing_email'] . ' for your own reference.');
		}
		catch(\EmailValidationFailedException $e)
		{
			\Messages::error('Error while sending email.');
		}
		catch(\EmailSendingFailedException $e)
		{
			\Messages::error('Error while sending email.');
		} ;
    }
    
    
}
