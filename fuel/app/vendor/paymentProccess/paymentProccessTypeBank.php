<?php

namespace PaymentProccess;

class PaymentProccessTypeBank extends PaymentProccessTypeBasic implements iPaymentProccessType
{
    public function __construct($postData, $order) 
    {
        parent::__construct($postData, $order);
    }
    
    /**
     * Notify user about successfullness of payment
     */
    public function notify() 
    {
        $emailer = new \Autorespondertrigger\Trigger();
        $emailer->sendOrderSubmittedBank($this->order);
		/*$emailer = new PaymentEmailer($this->order);
        $emailer->send();*/
    }
    
    /**
     * Proccess Payment differs from payment type.
     * In Bank Transfer case for example we'll just send email to customer
     */
    public function proccessPayment() 
    {
        $this->notify();
        
        return null;
    }
    
    /**
     *  Load environment from config which basically means what service to use: test or live, what user, pass etc  
     */
    public function setEnvironment() 
    {
        ; // No need to set environment for this type of proccess
    }
    
}