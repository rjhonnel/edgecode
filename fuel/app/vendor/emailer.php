<?php

namespace App;

class Emailer
{
    /**
     * Send single email to user
     * 
     * @param type $to_mail
     * @param type $to_name
     * @param type $subject
     * @param type $view
     * @param type $email_data
     * @return boolean
     * @throws \Exception
     */
	public static function send_email_to_user($to_mail, $to_name, $subject, $view, $email_data, $attachment = false)
	{

        $settings = \Config::load('autoresponder.db');


        // \Config::load('auto_response_emails', true);
        
		$email = \Email::forge();
		$email->to($to_mail, $to_name);
		$email->subject($subject);
        $email->from($settings['sender_email_address'], $settings['company_name']);
       
        if($attachment)
            $email->attach($attachment);
        
		$emailView = \Theme::instance()->view('views/' . $view)->set('email_data', $email_data, false);
		$email->html_body($emailView);
		
        try
        {
            $email->send();
        }
        catch(\Exception $e)
        {
            if(\Fuel::$env == 'development')
                throw new \Exception($e->getMessage());
            else 
                return false;
        }
        return true;
	}
    
    /**
     * Send email to group of admin users
     * 
     * @param type $group_name
     * @param type $subject
     * @param type $view
     * @param type $email_data
     * @param type $attachment = Attache a file
     * @param type $theme = theme to load views from
     * @return boolean
     * @throws \Exception
     */
	public static function send_email_to_group($group_name, $subject, $view, $email_data, $attachment = false)
	{
        \Config::load('auto_response_emails', 'auto_response_emails', true);
        
        $emails = \Config::get('auto_response_emails.' . $group_name . '_emails', false);
        
        if($emails == false)
        {
            $emails = \Config::get('auto_response_emails.default_emails', false);
        }
        
        $bcc = \Config::get('auto_response_emails.bcc');
        
        
        
		$email = \Email::forge();
		$email->cc($emails);
		
        if($bcc) $email->bcc($bcc);

        $email->subject($subject);
        $email->from(\Config::get('auto_response_emails.autoresponder_from_email'), \Config::get('site_title'));
       
        if($attachment)
            $email->attach($attachment);
        
		$emailView = \Theme::instance()->view('views/' . $view)->set('email_data', $email_data, false);
		$email->html_body($emailView);
		
        try
        {
            $email->send();
        }
        catch(\Exception $e)
        {
            if(\Fuel::$env == 'development')
                throw new \Exception($e->getMessage());
            else
                return false;
        }

        return true;
	}
}