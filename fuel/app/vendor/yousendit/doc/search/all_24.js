var searchData=
[
  ['_24account',['$account',['../class_user.html#ab5387c0c9516844744268f5935c07deb',1,'User']]],
  ['_24authtoken',['$authToken',['../class_auth.html#a11fe43916f18841e52688984a0561773',1,'Auth']]],
  ['_24availablestorage',['$availableStorage',['../class_account.html#a96e6c6f928463e04e111ed38481d9adc',1,'Account']]],
  ['_24blacklisteddomains',['$blackListedDomains',['../class_policy.html#a0f6d02b7ea0646d172fcb031cd7bce76',1,'Policy']]],
  ['_24blacklisteddomainsshare',['$blackListedDomainsShare',['../class_policy.html#ac90e7ed5816f77a84a9dc01d91c49aad',1,'Policy']]],
  ['_24bytesreceived',['$bytesReceived',['../class_status.html#a7967b87c51e89e70d82c8764254af87e',1,'Status']]],
  ['_24caninvite',['$canInvite',['../class_member.html#a25906c9d80c0112ee6d367ceff5a8a20',1,'Member']]],
  ['_24canread',['$canRead',['../class_member.html#aa9e09fb3f87f25c69f16e76fc6b8100b',1,'Member']]],
  ['_24canwrite',['$canWrite',['../class_member.html#ae8418c03756d5ddf755f8fc33f02a963',1,'Member']]],
  ['_24clickabledownloadurl',['$clickableDownloadUrl',['../class_file.html#a5b594e3e810455fd46a564c1c19309ae',1,'File']]],
  ['_24code',['$code',['../class_error_status.html#a3a830f5871ec2944b55f7cf557e9ec4c',1,'ErrorStatus']]],
  ['_24controlexpirationdate',['$controlExpirationDate',['../class_account.html#a057623d343ab59476d1730734405789e',1,'Account']]],
  ['_24create',['$create',['../class_item.html#ab69858bd470a9634141d888316090281',1,'Item']]],
  ['_24createdon',['$createdOn',['../class_folder.html#af3a918f1401bc06b8930e0d4ffe4e8e0',1,'Folder\$createdOn()'],['../class_file.html#ae4738cf42ae84f1ec048f9ff01876831',1,'File\$createdOn()']]],
  ['_24current',['$current',['../class_revision.html#a39c541700afac647eb65ab0e51b370be',1,'Revision']]],
  ['_24currentusage',['$currentUsage',['../class_storage.html#aab6677618fddf801e269d1ac28643662',1,'Storage\$currentUsage()'],['../class_folder.html#a292dbbc89295504db671e41dd79f7d13',1,'Folder\$currentUsage()'],['../class_storage_changes.html#a73ac770bf3d9d939f8eb78c7d5eda979',1,'StorageChanges\$currentUsage()']]],
  ['_24deletedstatus',['$deletedStatus',['../class_folder.html#a623afb7ca21f6e71eacd3a6cb83aa526',1,'Folder']]],
  ['_24deletestatus',['$deleteStatus',['../class_file.html#a2f99e9bd05bf07f8ef3039f2b784917a',1,'File']]],
  ['_24downloads',['$downloads',['../class_file.html#a70a60967c2050313225d6a8ee37a9081',1,'File']]],
  ['_24downloadurl',['$downloadUrl',['../class_commit.html#a615cf0a940a79d76302c50446d17f4fa',1,'Commit\$downloadUrl()'],['../class_file.html#aea40a037ac4c9e7e07f2b804e68e91ce',1,'File\$downloadUrl()'],['../class_revision.html#ab95a352cc7a4307bc2845a241f86db4c',1,'Revision\$downloadUrl()']]],
  ['_24email',['$email',['../class_user.html#a7727b0d91087524996c808025281cc95',1,'User\$email()'],['../class_tracking.html#a88b09aa4a13d6f0a48933cb8321b479a',1,'Tracking\$email()'],['../class_member.html#aed3065e8c4088daec2cc7e471d1e4508',1,'Member\$email()']]],
  ['_24errorcode',['$errorCode',['../class_ysi_error.html#ab73550c245a18be098ec434e867aa210',1,'YsiError']]],
  ['_24errormessage',['$errorMessage',['../class_ysi_error.html#a9108486a9e6c7a2c49b644dbdc1f162c',1,'YsiError']]],
  ['_24errorstatus',['$errorStatus',['../class_base.html#a5c362f4346302eba98419fe2f2ba29c7',1,'Base']]],
  ['_24expiration',['$expiration',['../class_item.html#a977192806fef109b309f66836f359e30',1,'Item']]],
  ['_24file',['$file',['../class_item.html#a63148a62ffafc559331a158ac4517551',1,'Item']]],
  ['_24filecount',['$fileCount',['../class_folder.html#a3afc2023912fd1f3bfe1ad541a470cca',1,'Folder']]],
  ['_24fileexpiration',['$fileExpiration',['../class_policy.html#a90bfd83338ad888bc63b775d46cb9e19',1,'Policy']]],
  ['_24fileexpirationoverwrite',['$fileExpirationOverwrite',['../class_policy.html#a25a13d35614326e9d311fad082ce086b',1,'Policy']]],
  ['_24fileid',['$fileId',['../class_file.html#a18750087534c8cfb3796e4f3700fd4b2',1,'File\$fileId()'],['../class_upload.html#a9e8c314b0919d5456fdad6e601e034f8',1,'Upload\$fileId()']]],
  ['_24filename',['$fileName',['../class_file.html#a8684b3138b02374c1887b9a25b4827cd',1,'File']]],
  ['_24files',['$files',['../class_folder.html#abf33107fb1aa9d3f98b295893d31b172',1,'Folder\$files()'],['../class_item.html#a53c7b2169f256b69fb8241efe3d5cb1e',1,'Item\$files()'],['../class_storage_changes.html#a802afe3af54ac62eb869d4eced32daa1',1,'StorageChanges\$files()']]],
  ['_24firstname',['$firstName',['../class_user.html#a2fcd36fa7f9aa3616c9e01cf1b8af41f',1,'User']]],
  ['_24foldercount',['$folderCount',['../class_folder.html#a46574b07d1245f619d6fee029c206373',1,'Folder']]],
  ['_24folderid',['$folderId',['../class_folder.html#a48f72deb75d7627562dd152a0d8c1cf0',1,'Folder\$folderId()'],['../class_file.html#a6ca4b55586252c92a474e0694b81ee38',1,'File\$folderId()']]],
  ['_24folders',['$folders',['../class_folder.html#a778cef8cc70047d0ff66a84b9ff3a76f',1,'Folder\$folders()'],['../class_storage_changes.html#a0f1b58e4ce36b55576ce72fa2fc4c382',1,'StorageChanges\$folders()']]],
  ['_24foldertype',['$folderType',['../class_folder.html#ae8f70e955ac5645d02bbc31b6b0572b1',1,'Folder']]],
  ['_24id',['$id',['../class_storage.html#aa803bf6cd9c3f941d537ea4187825bde',1,'Storage\$id()'],['../class_folder.html#a93f6309c2d46e08e6efd82af808cf1c5',1,'Folder\$id()'],['../class_file.html#af6bab12290b8806d2d5907b72f04fd69',1,'File\$id()'],['../class_item.html#ab080e059a79ad7ad71069fc4c63f4afb',1,'Item\$id()'],['../class_revision.html#ac5be82800afaebfb3a7b68b6021bd42f',1,'Revision\$id()']]],
  ['_24invitationkey',['$invitationKey',['../class_member.html#a95d05cc864a15c7292fbc7a9a47a2837',1,'Member']]],
  ['_24itemcount',['$itemCount',['../class_count.html#a63dc73a4ef63a29422dff0a1bf40a617',1,'Count']]],
  ['_24itemid',['$itemId',['../class_send.html#a8dbd6de398e76563c8f58e360dffc37c',1,'Send']]],
  ['_24items',['$items',['../class_items.html#a82bd21566695d586f70b61a20ef0c84c',1,'Items']]],
  ['_24knowledgebase',['$knowledgeBase',['../class_account.html#aec150fc1df406a3cf3cb9163cf2868df',1,'Account']]],
  ['_24lastname',['$lastName',['../class_user.html#a7bbe34c52e9eae257c0a5aaf41431c2f',1,'User']]],
  ['_24lastupdatedon',['$lastUpdatedOn',['../class_folder.html#a12458db06f7a1fdce2e5e3bdc9e43040',1,'Folder']]],
  ['_24maxdownloadbwpermonth',['$maxDownloadBWpermonth',['../class_account.html#a844b541e654f0151a47d02b635e8f750',1,'Account']]],
  ['_24maxfiledownloads',['$maxFileDownloads',['../class_account.html#a4f5948e44fac996591b6685af90b2974',1,'Account']]],
  ['_24maxfilesize',['$maxFileSize',['../class_account.html#ae3b72925e7a24be1b06f68ddfde744f0',1,'Account']]],
  ['_24message',['$message',['../class_error_status.html#a258a0c7de2051e3a03f249f0b73237fa',1,'ErrorStatus\$message()'],['../class_item.html#a335163f9f330b3fbaf4522875adc2ba9',1,'Item\$message()']]],
  ['_24name',['$name',['../class_folder.html#a41c7cb93bd7f259b872117a720b679c7',1,'Folder\$name()'],['../class_file.html#a9a48a48321b5ff8558c1ef2a1f7af3af',1,'File\$name()']]],
  ['_24ownedbystorage',['$ownedByStorage',['../class_file.html#a6201b42a5b8df6f0f7097a2839928dcc',1,'File']]],
  ['_24ownedbyuser',['$ownedByUser',['../class_folder.html#a40edc9372802bdf42b30b4bf38b0808a',1,'Folder']]],
  ['_24parentid',['$parentId',['../class_folder.html#abd0c61c3f2bff34181de02fdefc2f271',1,'Folder\$parentId()'],['../class_file.html#a23fbcc3bc67f9dc8d83a5752be375204',1,'File\$parentId()']]],
  ['_24passwordprotect',['$passwordProtect',['../class_account.html#a52d997dd43401956d85bf1270af53944',1,'Account\$passwordProtect()'],['../class_policy.html#aea955bd5943eb370bf0a6bd57b7cd83d',1,'Policy\$passwordProtect()'],['../class_file.html#ac7d28fa3315ccf3f353ff6f74e272eca',1,'File\$passwordProtect()']]],
  ['_24passwordprotectoverwrite',['$passwordProtectOverwrite',['../class_policy.html#aff30cf9e7b4933cde9770f00ad67a51e',1,'Policy']]],
  ['_24permissions',['$permissions',['../class_folder.html#af583230925937e734a917b5882b81fd5',1,'Folder']]],
  ['_24readable',['$readable',['../class_folder.html#ae149e4c06cdbe86eec66065096ea784e',1,'Folder']]],
  ['_24recipients',['$recipients',['../class_item.html#a68ab6ec9ff3e9936d201a805c9abe3f5',1,'Item']]],
  ['_24returnreceipt',['$returnReceipt',['../class_account.html#af15580b3affb1275f9d5bb3a1dafe26d',1,'Account\$returnReceipt()'],['../class_policy.html#a8419ff78106c8341123f332e1247ce4d',1,'Policy\$returnReceipt()'],['../class_file.html#a12a1ca72bbc17922a2cd576754bba0a9',1,'File\$returnReceipt()']]],
  ['_24returnreceiptoverwrite',['$returnReceiptOverwrite',['../class_policy.html#a3fee7d0c56e831312a7f4f3874a950ad',1,'Policy']]],
  ['_24revision',['$revision',['../class_storage.html#ae22c4f603339073ada7105e741725a3d',1,'Storage\$revision()'],['../class_folder.html#ac53fa47a9694a034603b262e7e34688b',1,'Folder\$revision()'],['../class_file.html#abfe78b3d02929ef380ee838697fa4573',1,'File\$revision()'],['../class_storage_changes.html#a2d53af775737053b7bdff835533d1564',1,'StorageChanges\$revision()']]],
  ['_24revisioncount',['$revisionCount',['../class_file.html#aa6b38ee759a9f0ab92a059fc4fd27b74',1,'File\$revisionCount()'],['../class_storage_changes.html#a7d5205e6e3f215a97596cddcf2c8ec28',1,'StorageChanges\$revisionCount()']]],
  ['_24revisions',['$revisions',['../class_file.html#af6165389fbabb6c20ba726fae96b7672',1,'File']]],
  ['_24shareinvitationpending',['$shareInvitationPending',['../class_folder.html#afe73163d522695e56c91bf46e529241d',1,'Folder']]],
  ['_24size',['$size',['../class_folder.html#a064eed4c3e62a2725b72d84b542d5c4b',1,'Folder\$size()'],['../class_file.html#a0fe80dde97ed6b7c2b4e5b1d946ea8a5',1,'File\$size()'],['../class_revision.html#a5e21fbb20e8f4fb0ad9a0532617d2b68',1,'Revision\$size()']]],
  ['_24status',['$status',['../class_user.html#a72192f9e35cfe3504d9c29832e00a6ff',1,'User\$status()'],['../class_status.html#a369ede730229c651761ff570abb4178a',1,'Status\$status()'],['../class_delete.html#abf7186197e904c046308864b01884512',1,'Delete\$status()'],['../class_expiration.html#a0de9823c99d0c8bf82a3d3d75d2c9388',1,'Expiration\$status()'],['../class_file.html#ae2ae4d39188025152dd45850115e6c72',1,'File\$status()'],['../class_share.html#a7051a61b226be45678228fd7a85a1603',1,'Share\$status()'],['../class_member.html#a249451a8d796b18550000b17fefd5261',1,'Member\$status()'],['../class_rename.html#ab42560fb4b8e38d7968e0e03d007e455',1,'Rename\$status()'],['../class_move.html#a3a2c567accf7d8e65179deeee79dcdd2',1,'Move\$status()']]],
  ['_24statusdisplay',['$statusDisplay',['../class_storage_changes.html#ab33604972fe4645102f0dbe34651caa4',1,'StorageChanges']]],
  ['_24storage',['$storage',['../class_user.html#aa094bff3cc7ba5404728aea8009c6fa8',1,'User']]],
  ['_24storagequota',['$storageQuota',['../class_storage.html#a8daeaaace31e10db58e1daf6407e3cd2',1,'Storage\$storageQuota()'],['../class_folder.html#ae8a9e94b1d6e19c19bcb669f0ba59a32',1,'Folder\$storageQuota()'],['../class_storage_changes.html#a109f355cf7b9b4e0836fab19c871cf21',1,'StorageChanges\$storageQuota()']]],
  ['_24subject',['$subject',['../class_item.html#aac3af7abb862f7e458d5aabe5e69c5ff',1,'Item']]],
  ['_24tracking',['$tracking',['../class_file.html#ae968a06ad177b4efc6039338d6b01e1c',1,'File']]],
  ['_24type',['$type',['../class_user.html#a1a5e81b2d5c4570cf7b58c8be325392e',1,'User\$type()'],['../class_folder.html#a8fa8c3a44c2fee6ecc8a17a8e29b3cf1',1,'Folder\$type()']]],
  ['_24ufid',['$ufid',['../class_ysi_response.html#ab6e8a6829d9d139ca0934edc8bab8af4',1,'YsiResponse']]],
  ['_24updatedon',['$updatedOn',['../class_folder.html#af25fdf88608c450f5bd8bb3b3fdcb6e7',1,'Folder']]],
  ['_24uploadstatus',['$uploadStatus',['../class_ysi_response.html#af386b17b678a03da88a53ffdc17d24c2',1,'YsiResponse']]],
  ['_24uploadurl',['$uploadUrl',['../class_send.html#aa6553eecf55dfd28ac3c520fa32d3156',1,'Send\$uploadUrl()'],['../class_upload.html#a706e190c0d9c4a6f3f4e58e5ee3de2f2',1,'Upload\$uploadUrl()']]],
  ['_24verifyidentity',['$verifyIdentity',['../class_file.html#ab671750f8569b5c45cd2551d4b6919cb',1,'File']]],
  ['_24verifyrecipientidentity',['$verifyRecipientIdentity',['../class_account.html#a68d17c8711224db1ff492b65e4dcdf45',1,'Account']]],
  ['_24version',['$version',['../class_user.html#a1b97b8410ff662e7a7ac148c2205c507',1,'User']]],
  ['_24vri',['$vri',['../class_policy.html#a77b243d3543f4b9bf7edc20877512fd8',1,'Policy']]],
  ['_24vrioverwrite',['$vriOverwrite',['../class_policy.html#aec54daf495feffa1c99a007bcf756861',1,'Policy']]],
  ['_24vrioverwriteshare',['$vriOverwriteShare',['../class_policy.html#af8d4d12abc5cb35f2cb4d8b96c51846b',1,'Policy']]],
  ['_24vrishare',['$vriShare',['../class_policy.html#a5277398e9afc439e33ed9ecc5d29f381',1,'Policy']]],
  ['_24when',['$when',['../class_tracking.html#af1c340c900af7cad8f7098ceb02e27c1',1,'Tracking']]],
  ['_24whitelisteddomains',['$whiteListedDomains',['../class_policy.html#a98f709e21cf319e29cdc7a198aa4b735',1,'Policy']]],
  ['_24whitelisteddomainsshare',['$whiteListedDomainsShare',['../class_policy.html#af13b14d413d1bb9b0f642039bbfd2140',1,'Policy']]],
  ['_24workspaceid',['$workspaceId',['../class_storage_changes.html#acc8701b72eabc44297cc5b62ffc1c166',1,'StorageChanges']]],
  ['_24writeable',['$writeable',['../class_folder.html#a8729dbc1fda4d4466b80947a93e48c2a',1,'Folder']]],
  ['_24wspermissionname',['$wsPermissionName',['../class_permission.html#abba91dfbcc2df41faaca4fbeb2894ab6',1,'Permission']]]
];
