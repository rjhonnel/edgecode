<?php

namespace Extendedcart;

use Cart\Exception\InvalidCartInstanceException;
use Cart\Exception\DuplicateCartInstanceException;
use Cart\Exception\InvalidStorageImplementationException;
use Cart\Manager as Cart_Manager;

class Manager extends Cart_Manager
{

	/**
	 * Create a new cart instance
	 *
	 * @param  string                                         $cartID        The ID for the cart instance
	 * @param  bool|array                                     $config        The configuration options associated with this cart
	 * @param  bool                                           $overwrite     If the cart instance already exists should if be overwritten
	 * @param  bool                                           $switchContext Should the context be switched to this cart instance
	 * @return mixed                                          The newly created cart instance
	 * @throws \Cart\Exception\DuplicateCartInstanceException
	 */
	public function newCart($cartID, $config = false, $overwrite = true, $switchContext = true)
	{
		if (!$this->cartExists($cartID) or $overwrite) {

			$config or $config = $this->getCartConfig($cartID);
			$this->carts[$cartID] = new \Extendedcart\Cart($cartID, $config);

			/*
			 * is there storage options associated with this instance of the cart?
			 * if so we need to retrieve any saved data
			 */
			if ($config['storage']['driver']) {
				$this->restoreCartState($cartID);
			}
			if ($config['storage']['autosave']) {
				// register shutdown function for auto save
				register_shutdown_function(array($this, 'saveCartState'), $cartID);
			}

			if ($switchContext) {
				$this->context = $cartID;
			}

			return $this->carts[$cartID];
		} else {
			throw new DuplicateCartInstanceException(sprintf('There is already a cart instance with the id: %s', $cartID));
		}
	}

}