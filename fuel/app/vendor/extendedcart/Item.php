<?php

namespace Extendedcart;

use Cart\Item as Cart_Item;

class Item extends Cart_Item
{

	/**
	 * Get price for 1 of this item
	 *
	 * @param  bool  $excludingTax Should the single price be returned tax excluded
	 * @return float Price for 1 of item
	 */
	public function singleDiscountedPrice($excludingTax = false)
	{
		$price = $excludingTax ?
			$this->data['price'] :
			$this->data['price'] + $this->data['tax'];

		$price = $this->calculate_discount( $this->data, $price );

		// $price =  number_format(
		//     $price,
		//     $this->config['decimal_places'],
		//     $this->config['decimal_point'],
		//     $this->config['thousands_separator']
		// );


		return $price;
	}

	/**
	 * Get items total price
	 *
	 * @param  bool  $excludingTax Should the total price be returned tax excluded
	 * @return float Total aggregate price for this item
	 */
	public function totalPrice($excludingTax = false)
	{

		$price = $excludingTax ?
			$this->data['price'] :
			$this->data['price'] + $this->data['tax'];

		$total_price = $price * $this->data['quantity'];






		// $total_price = $this->calculate_discount( $this->data, $total_price );



		$total_price =  number_format(
			$total_price,
			$this->config['decimal_places'],
			$this->config['decimal_point'],
			$this->config['thousands_separator']
		);


		return $total_price;
	}


	/**
	 * Get items total price
	 *
	 * @param  bool  $excludingTax Should the total price be returned tax excluded
	 * @return float Total aggregate price for this item
	 */
	public function totalDiscountedPrice($excludingTax = false)
	{
		$price = $excludingTax ?
			$this->data['price'] :
			$this->data['price'] + $this->data['tax'];

		$total_price = $price * $this->data['quantity'];
		$total_price = $this->calculate_discount( $this->data, $total_price );

		// $total_price =  number_format(
		//     $total_price,
		//     $this->config['decimal_places'],
		//     $this->config['decimal_point'],
		//     $this->config['thousands_separator']
		// );


		return $total_price;
	}

	public function get_discount( $item_id ) {

		$_discount = 0;


		$user = false;

		if (\Sentry::check())
		{
			$user = \Sentry::user();
		}

		if( $user ) {

			$user_group_id = $user['groups'][0]['id'];

			$product_groups = \Product\Model_Product_To_Groups::find_by_product_id( $item_id );

			$group_id = isset($product_groups[0]->group_id)?$product_groups[0]->group_id:0;
			$retail__discounts = \Product\Model_Group_Discounts::find_by(array('user_group_id' => $user_group_id, 'product_group_id' => $group_id ), null, null, null);

			$sale__discount = \Product\Model_Group_Options::find_by(array('user_group_id' => $user_group_id, 'product_group_id' => $group_id ), null, null, null);


			if( $this->data['price_type'] == 'sale_price' ) {
				if(isset($sale__discount[0]))
					$_discount = $sale__discount[0]->sale_discount;
			} else {
				if(isset($retail__discounts[0]))
					$_discount = $retail__discounts[0]->discount;
			}


			$_discount += (int) $_discount;
		}

		return $_discount;

	}
	public function calculate_discount( $item, $total_price = 0 ) {



		$_discount = $this->get_discount( $item['id'] );

		$total_price = $total_price - ( ( $total_price ? ((int)$_discount / $total_price): 0 ) * 100 );


		return $total_price;

	}


}