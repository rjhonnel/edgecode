<?php

namespace Extendedcart;

use Cart\Exception\InvalidCartConfigException;
use Cart\Exception\InvalidCartItemException;
use Cart\Cart as Cart_Cart;

class Cart extends Cart_Cart
{

	/**
	 * Get the total of a certain value that appears on each item in the cart.
	 *
	 * @param  string $key Key of the value
	 * @return int    Total value for the passed key
	 */
	public function getTotal($key)
	{
		$counter = 0;
		$items = $this->items;

		if (count($items) > 0) {
			foreach ($items as $item) {
				if ($key !== 'quantity') {


					if (is_numeric($item->get($key))) {

						// $total = ($item->get($key) * $item->get('quantity'));
						$counter += ($item->get($key) * $item->get('quantity'));
						// $counter += $item->calculate_discount( $item->get('id'), $total );

					}
				} // if the total quantity is required we do not want to multiply by the quantity like above...
				else {
					$counter += $item->get('quantity');
				}
			}
		}

		return $counter;
	}

	/**
	 * Get the total of a certain value that appears on each item in the cart.
	 *
	 * @param  string $key Key of the value
	 * @return int    Total value for the passed key
	 */
	public function getDiscountedTotal($key)
	{
		$counter = 0;
		$items = $this->items;

		if (count($items) > 0) {
			foreach ($items as $item) {
				if ($key !== 'quantity') {


					if (is_numeric($item->get($key))) {

						$total = ($item->get($key) * $item->get('quantity'));
						$product = array(
							'id' => $item->get('id'),
							'price' => $item->get('price'),
							'price_type' => $item->get('price_type'),
						);
						$counter += $item->calculate_discount( $product, $total );

					}
				}
				// if the total quantity is required we do not want to multiply by the quantity like above...
				else {
					$counter += $item->get('quantity');
				}
			}
		}

		return $counter;
	}

	/**
	 * Add an item to the cart. If the item already exists in the cart, it is updated.
	 *
	 * @param  array       $itemData Data associated with the item
	 * @return string|bool If the item is added or fields other than the quantity are updated the UID is returned,
	 *                     if the item is updated and only the quantity is amended true is returned
	 */
	public function add($itemData)
	{
		$uid = $this->generateUID($itemData);

		// if item does not have a quantity, set to one
		if ( ! array_key_exists('quantity', $itemData)) {
			$itemData['quantity'] = 1;
		}

		// save timestamp of when this item was added
		if ( ! array_key_exists('added_at', $itemData)) {
			$itemData['added_at'] = time();
		}

		// set meta data
		if ( ! array_key_exists('meta', $itemData)) {
			$itemData['meta'] = array();
		}

		// if item already exists, simply update the quantity
		if ($this->exists($uid)) {
			$newQuantity = $this->items[$uid]->get('quantity') + $itemData['quantity'];

			return $this->update($uid, 'quantity', $newQuantity);
		}
		// otherwise add as a new item
		else {
			$config = array(
				'decimal_point'       => $this->config['decimal_point'],
				'decimal_places'      => $this->config['decimal_places'],
				'thousands_separator' => $this->config['thousands_separator'],
			);

			$this->items[$uid] = new \Extendedcart\Item($itemData, $uid, $config);

			return $uid;
		}
	}

	/**
	 * Generate a unique identifier based on the items data. The array is JSON encoded
	 * to get a string representation of the data then md5 hashed to generate a unique
	 * value
	 *
	 * @param  array  $itemData Items data that will be hashed to generate the UID
	 * @return string UID
	 */
	public function generateUID($itemData)
	{
		/*
		 * remove keys from the array that are not to be included in the uid hashing process
		 * these keys identify supplementary data to the core product data i.e quantity, added_at, meta etc
		 */
		$ignoreKeys = array('quantity', 'added_at', 'meta', 'unique_id');

		foreach ($ignoreKeys as $k) {
			if (array_key_exists($k,$itemData)) {
				unset($itemData[$k]);
			}
		}

		return md5(json_encode($itemData));
	}

}