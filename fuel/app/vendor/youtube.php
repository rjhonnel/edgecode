<?php

namespace App;

class Youtube
{
    public $url;
    public $video_id;
    
    public static function forge()
    {
        return new Youtube();
    }
    
    function __construct()
    {
        // Video ID will be inserted between theese two parts
        $this->url = array(
            'first_part' => 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=',
            'second_part' => '&key=AIzaSyDgtQQVqyNqvEHl4M5I5oz9u2bKzIblwpg',
        );
    }
    
    /**
     * Parse YOutube URL and get video ID
     * 
     * @param $url
     */
    public function parse($url)
    {
        // Prepare url
        $url = $this->prep_url($url);
        
        $data = parse_url($url);
        $query = array();
        if (isset($data['query'])) {
            parse_str($data['query'], $query);
        }
        
        $data['host'] = isset($data['host']) ? $data['host'] : '';
        $data['path'] = isset($data['path']) ? $data['path'] : '';
        
        if (false !== strpos($data['host'], 'youtube.')
            && in_array($data['path'], array('/watch', '/all_comments'))
            && isset($query['v'])
            && preg_match('#^[\w-]{11}$#', $query['v'])
        ) {
            $id = $query['v'];
        } elseif (false !== strpos($data['host'], 'youtu.be')
            && preg_match('#^/?[\w-]{11}/?$#', $data['path'])
        ) {
            $id = trim($data['path'], '/');
        } elseif (false !== strpos($data['host'], 'youtube.com')
            && preg_match('{^/embed/([\w-]{11})}', $data['path'], $matches)
        ) {
            $id = $matches[1];
        } elseif (false !== strpos($data['host'], 'youtube-nocookie.com')
            && preg_match('{^/embed/([\w-]{11})}', $data['path'], $matches)
        ) {
            $id = $matches[1];
        } else {
            return $this;
        }

        $this->video_id = $id;
        
        return $this;
    }
    
    public function iframe($url, $width = false, $height = false)
    {
        // Check width and height
        is_numeric($width) or $width = 570;
        is_numeric($height) or $height = 350;
        
        $iframe = '';
        $video = $this->parse($url);
        
        if($video->video_id)
        {
            $iframe = '<iframe width="' . $width . '" height="' . $height . '" src="http://www.youtube.com/embed/' . $video->video_id . '" frameborder="0" allowfullscreen></iframe>';
        }
        
        return $iframe;
    }
    
    public function embed($url)
    {
        $video = $this->parse($url);
        
        if($video->video_id)
        {
            return "http://www.youtube.com/embed/" . $video->video_id;
        }
        
        return '';
    }
    
    public function get()
    {
        if($this->video_id)
        {
            try
            {
                // If value exists in cache ve just return it
                $content = \Cache::get(\Inflector::friendly_title($this->video_id));
                return $content;
            }
            catch (\CacheNotFoundException $e)
            {
                // Value not in cache
                $request = \App\Curl::forge();
                $request->cUrl(false);
                $response = $request->get($this->url['first_part'] . $this->video_id . $this->url['second_part']);

                if($array = $request->is_json($response, true))
                {
                    $return = array(
                        'id'            => $this->video_id,
                        'link'          => \Input::post('url'),
                        'title'         => $array->items[0]->snippet->title,
                        'description'   => $array->items[0]->snippet->description,
                        'thumbnail'     => array(
                            'small'         => $array->items[0]->snippet->thumbnails->default->url,
                            'middle'        => $array->items[0]->snippet->thumbnails->medium->url,
                            'large'         => $array->items[0]->snippet->thumbnails->standard->url,
                        ),
                    );

                    // Save value in cache for future use
                    \Cache::set(\Inflector::friendly_title($this->video_id), $return, 3600 * 24);

                    return $return;
                }
            }
        }
        
        return false;
    }
    
    function prep_url($str = '')
    {
        if ($str == 'http://' OR $str == '')
        {
            return '';
        }

        $url = parse_url($str);

        if ( ! $url OR ! isset($url['scheme']))
        {
            $str = 'http://'.$str;
        }

        return $str;
    }
    
    public function get_video_id()
    {
        return $this->video_id;
    }
}