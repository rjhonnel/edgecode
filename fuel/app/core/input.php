<?php

/**
 * EXTENDED URI FUEL CORE CLASS
 *
 * Extended core fuel class
 *
 * @subpackage system
 * @version 1.0
 * @author Eximius Solutions Development Team
 * @copyright 2011 - 2012 Eximius Solutions
 *
 * @todo
 *
 */


use Application\Exception;
class Input extends \Fuel\Core\Input
{
	/**
	  * Fetch an item from the POST array
	  *
	  * @param   string  The index key
	  * @param   mixed   The default value
	  * @param  array Array of filters - if empty then all filter will be used
	  * 
	  * @return  string|array
	  */
	 public static function secured_post($index = null, $default = null, $filters = array('strip_tags', 'htmlentities', 'xss_clean'))
	 {
		return \Security::clean(\Input::post($index,$default), $filters);
	 }
	 
	 /**
	  * Fetch an item from the POST array
	  *
	  * @param   string  The index key
	  * @param   mixed   The default value
	  * @param  array Array of filters - if empty then all filter will be used
	  * 
	  * @return  string|array
	  */
	 public static function secured_get_post($index = null, $default = null, $filters = array('strip_tags', 'htmlentities', 'xss_clean'))
	 {
		return \Security::clean(\Input::get_post($index,$default), $filters);
	 }
}
