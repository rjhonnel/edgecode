<?php
/**
 * EXTENDED URI FUEL CORE CLASS
 *
 * Extended core fuel class
 *
 * @subpackage system
 * @version 1.0
 * @author Eximius Solutions Development Team
 * @copyright 2011 - 2012 Eximius Solutions
 *
 * @todo
 *
 */


use Application\Exception;
class Str extends \Fuel\Core\Str
{
	/**
     * Prep URL
     *
     * Simply adds the http:// part if no scheme is included
     *
     * @param string the URL
     * @return string
     */
    public static function prep_url($str = '')
    {
        if ($str === 'http://' OR $str === '')
        {
            return '';
        }

        $url = parse_url($str);

        if (!$url OR !isset($url['scheme']))
        {
            return 'http://' . $str;
        }

        return $str;
    }
}