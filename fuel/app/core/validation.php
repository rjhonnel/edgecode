<?php

/**
 * Part of Leading Edge Creative CMS.
 *
 * @package    LEC CMS
 * @version    1.0
 * @author     CMS Development Team
 * 
 * @extends \Fuel\Core\Validation
 */

class Validation extends \Fuel\Core\Validation
{
	/**
	 * Check if a value is unique in column
	 *
	 * @param   string   $val
	 * @param   array    $options   keys are 0:table, 1:field, 2:id field value, 3:id field. Default id field is id
	 */
	public function _validation_unique($val, $options = array())
	{
		Validation::active()->set_message('unique', 'The field :label must be unique, but :value has already been used.');

		// Validation::active()->set_message('unique', 'It appears the :label has already been taken.');
		
		 
		$result = \DB::select(\DB::expr("LOWER (\"{$options['1']}\")"))->from($options['0'])
		->where($options['1'], '=', Str::lower($val));
		 
		if(!empty($options['2']) && is_numeric($options['2']))
		{
			$id = empty($options['3']) ? 'id' : $options['3'];
			$result->where($id, '!=', $options['2']);
		}

		if(count($result->execute()) > 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function _validation_date($val, $format = 'mm/dd/yyyy')
	{
		$format = $format ? ' Example format: ' . $format : '';
		Validation::active()->set_message('date', 'The field :label must a valid date.' . $format);
		 
		return $this->_empty($val) || strtotime($val);
	}

	public function _validation_is_float($val)
	{
		Validation::active()->set_message('is_float', 'The field :label must be number.');

		return $this->_empty($val) || floatval($val);;
	}
    
	/**
	 * Required but not zero
	 *
	 * Value may not be empty
	 *
	 * @param   mixed
	 * @return  bool
	 */
	public function _validation_required_not_zero($val)
	{
		Validation::active()->set_message('required_not_zero', 'The field :label is required and must contain a value.');
		
		return !($this->_empty($val) || $val == '0');
	}
	
	/**
	 * Extended to work with array inputs like checkboxes
	 * Minimum string length
	 *
	 * @param   string
	 * @param   int
	 * @return  bool
	 */
	public function _validation_min_length($val, $length)
	{
		if(is_array($val))
		{
			return $this->_empty($val) || count($val) >= $length;
		}
		else
		{
			return $this->_empty($val) || (MBSTRING ? mb_strlen($val) : strlen($val)) >= $length;
		}
	}
    
    /**
	 * Validate URL using PHP's filter_var()
	 *
	 * @param   string
	 * @return  bool
	 */
	public function _validation_valid_url($val)
	{
        $patern = '~^
        (http|https):// # protocol
        (
        ([\pL\pN\pS-]+\.)+[\pL]+ # a domain name
        | # or
        \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3} # a IP address
        | # or
        \[
        (?:(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){6})(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:::(?:(?:(?:[0-9a-f]{1,4})):){5})(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:[0-9a-f]{1,4})))?::(?:(?:(?:[0-9a-f]{1,4})):){4})(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,1}(?:(?:[0-9a-f]{1,4})))?::(?:(?:(?:[0-9a-f]{1,4})):){3})(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,2}(?:(?:[0-9a-f]{1,4})))?::(?:(?:(?:[0-9a-f]{1,4})):){2})(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,3}(?:(?:[0-9a-f]{1,4})))?::(?:(?:[0-9a-f]{1,4})):)(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,4}(?:(?:[0-9a-f]{1,4})))?::)(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,5}(?:(?:[0-9a-f]{1,4})))?::)(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,6}(?:(?:[0-9a-f]{1,4})))?::))))
        \] # a IPv6 address
        )
        (:[0-9]+)? # a port (optional)
        (/?|/\S+) # a /, nothing or a / with something
        $~ixu';
        
		return $this->_empty($val) || preg_match($patern, $val);
	}
}