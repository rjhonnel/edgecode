<?php

/**
 * EXTENDED VALIDATION FUEL CORE CLASS
 *
 * Extended core fuel class
 *
 * @subpackage system
 * @version 1.0
 * @author Eximius Solutions Development Team
 * @copyright 2011 - 2012 Eximius Solutions
 *
 * @todo
 *
 */
class Arr extends \Fuel\Core\Arr
{
	/**
	 * EDIT: Return array elements that are sub arrays
	 * if we get this kind of array we want to use "featured" and "status" as values too
	 * 
	 * $_properties = array(
	 *	    'id',
	 *	    'featured' => array('default' => 0),
	 *	    'status' => array('default' => 1),
	 *	    'active_from',
	 *	);
	 */
	
	/**
	 * Filters an array by an array of keys
	 *
	 * @param   array   the array to filter.
	 * @param   array   the keys to filter
	 * @param   bool    if true, removes the matched elements.
	 * @return  array
	 */
	public static function filter_keys($array, $keys, $remove = false)
	{
		$return = array();
		foreach ($keys as $key => $value)
		{
			if(!is_int($key)) $value = $key;
				
			if (array_key_exists($value, $array))
			{
				$remove or $return[$value] = $array[$value];
				if($remove)
				{
					unset($array[$value]);
				}
			}
		}
		return $remove ? $array : $return;
	}

}
