<?php
/**
 * EXTENDED URI FUEL CORE CLASS
 *
 * Extended core fuel class
 *
 * @subpackage system
 * @version 1.0
 * @author Eximius Solutions Development Team
 * @copyright 2011 - 2012 Eximius Solutions
 *
 * @todo
 *
 */


use Application\Exception;
class Uri extends \Fuel\Core\Uri
{
	public $_old_uri = null;
	
	/**
	 * EXTENDED URI FUEL CORE CLASS
	 *
	 * Construct takes a URI or detects it if none is given and generates
	 * the segments.
	 *
	 *
	 * @param   string  The URI
	 * @return  void
	 */
	public function __construct($uri = null)
	{
		parent::__construct($uri);
		
		$this->_old_uri = $this->uri;
		
		$this->_set_correct_segments();
	}

	/**
	 * EXTENDED URI FUEL CORE CLASS
	 *
	 * Returns current module name
	 *
	 * @return string
	 */
	public static function get_module_name()
	{
		return self::segment(1);
	}


	/**
	 * EXTENDED URI FUEL CORE CLASS
	 *
	 * Returns controller name
	 *
	 * @return string
	 */
	public static  function get_controller_name()
	{
		return self::segment(2);
	}

	/**
	 * EXTENDED URI FUEL CORE CLASS
	 *
	 * Replaces segments to load proper module (example - admin/club loads club module admin controller
	 *
	 * @return void
	 */
	private function _set_correct_segments()
	{
		if (empty ($this->segments))
		{
			$default_route = \Config::get('routes._root_');
			if (isset($default_route))
			{
				$this->segments = explode('/',\Config::get('routes._root_'));
			}
			else
			{
				throw Exception('Set the root route in route config');
			}
		}
		
		$all_segments = $this->segments;
		
		$this->segments = array();

		// we have to replace segments to load proper module (example - admin/club loads club module admin controller
		if($all_segments[0] == 'admin' AND count($all_segments) > 1)
		{
			if ($module_path = \Module::exists($all_segments[1]))
			{
				$additional_segment = 0;
				$this->segments[0] = $all_segments[1];
				$this->segments[1] = $all_segments[0];
				
				if(empty($all_segments[2])) $all_segments[2] = $all_segments[1];
				
				if (! file_exists($module_path.'classes'.DS.'controller'.DS.'admin'.DS.$all_segments[2].'.php'))
				{
					$this->segments[2] = $all_segments[1];
					$additional_segment = 1;
				}
				
				for ($i = 2; $i < count($all_segments); $i++)
				{
					$this->segments[$i + $additional_segment] = $all_segments[$i];
				}
			}
		}
		// if we want to make roote score/some_page to reroute to score/default_module/default_controller/default_method/some_page
		elseif(! \Module::exists($all_segments[0]))
		{
//			$this->segments[0] = 'member';
//			$this->segments[1] = 'index';
//			$this->segments[2] = 'index';
//			foreach ($all_segments as $segment => $value)
//			{
//				$this->segments[] = $value;
//			}
		}
		elseif(1 == 2 && $module_path = (\Module::exists($all_segments[0])))
		{
			// if we have route like score/news we want to make it like score/news/news/index
			if (count($all_segments) == 1)
			{
				$this->segments[0] = $all_segments[0];
				$this->segments[1] = $all_segments[0];;
				$this->segments[2] = 'index';
			}
			elseif(count($all_segments) == 2)
			{
				if ((! file_exists($module_path.'classes'.DS.'controller'.DS.$all_segments[1])) && (! file_exists($module_path.'classes'.DS.'controller'.DS.$all_segments[1].'.php')))
				{
					$this->segments[0] = $all_segments[0];
					$this->segments[1] = $all_segments[0];
					$this->segments[2] = $all_segments[1];
				}
			}
		}
        // Frontend Routing logic
		elseif($all_segments[0] != 'admin')
		{
			switch (count($all_segments))
			{
				case 0:
					$this->segments[0] = 'page';
					$this->segments[1] = 'index';
					break;
					
				case 1:
					$this->segments[0] = $all_segments[0];
					$this->segments[1] = 'index';
					break;
					
				default:
					
					if($module_path = (\Module::exists($all_segments[0])))
					{
						\Module::load($all_segments[0]);
						// Check if submodule exists like Category controller inside Product module (product/category/)
						if (file_exists($module_path.'classes'.DS.'controller'.DS.$all_segments[1].'.php'))
						{
							if(isset($all_segments[2]))
							{
								if(!method_exists('\\' . ucfirst($all_segments[0]) . '\\Controller_' . ucfirst($all_segments[1]), strtolower('action_' . $all_segments[2])))
								{
									// Insert "default" method as second uri string
									array_splice($all_segments, 2, 0, array('index'));
								}
							}
							else
							{
								// Insert "default" method as next uri string
								$all_segments[2] = 'index';
							}
						}
						else
						{
							// Default behavior, no submodule calls
							if (file_exists($module_path.'classes'.DS.'controller'.DS.$all_segments[0].'.php'))
							{
								
								if(!method_exists('\\' . ucfirst($all_segments[0]) . '\\Controller_' . ucfirst($all_segments[0]), strtolower('action_' . $all_segments[1])))
								{
									
									// Insert "default" method as second uri string
									array_splice($all_segments, 1, 0, array('index'));
								}
							}
						}
					}
					
					$this->segments = $all_segments;
					break;
			}
            
			
			// If we ever want to have more than one method in languge controller then we have to uncoment this part of code and we need to change route config in language module
			/*
			// Rewrite for language
			if(strlen($all_segments[1]) == 2 || ((preg_match('#(\w{2}/)#', $all_segments[1])) && (count($all_segments) >= 1)))
			{
			$this->segments[0] = 'language';
			$this->segments[1] = 'language';
			$this->segments[2] = 'set';
			$this->segments[3] = $all_segments[1];
			}
			*/
		}
		else
		{
			// If we ever want to have more than one method in languge controller then we have to uncoment this part of code and we need to change route config in language module
			/*
			// Rewrite for language
			if(strlen($all_segments[1]) == 2 || ((preg_match('#(\w{2}/)#', $all_segments[1])) && (count($all_segments) >= 1)))
			{
			$this->segments[0] = 'language';
			$this->segments[1] = 'language';
			$this->segments[2] = 'set';
			$this->segments[3] = $all_segments[1];
			}
			*/
		}
		if (empty($this->segments))
		{
			$this->segments=$all_segments;
		}

		$this->uri='';

		$i = 1;
		$segments_count = count($this->segments);
		foreach ($this->segments as $segment)
		{
			$this->uri .= $segment;

			if ($i < $segments_count)
			{
				$this->uri .= '/';
			}

			++$i;
		}
		
//		echo $this->uri; exit;
	}
	
	/**
	 * EXTENDED URI FUEL CORE CLASS
	 *
	 * Returns if request is https or not
	 *
	 * @return boolean
	 */
	public static function is_https()
	{
		if( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' )
		{
			return true;
		}
		else
		{
			return false;
		}
		return false;
	}

	/**
	 * EXTENDED URI FUEL CORE CLASS
	 *
	 * Return https base url, we use this for SSL uri
	 *
	 * @return string
	 */
	public static function base_secure()
	{
		return \Config::get('base_secure_url');
	}
	
	/**
	 * Gets the current URL, including the BASE_URL with https
	 *
	 * @return  string
	 */
	public static function current_secure()
	{
		return static::create(null, array(), array(), true);
	}
	
	/**
	 * If you want to get correct URLs while in admin panel you have to use 
	 * this method instead Uri::current() and Uri::string()
	 *
	 * @return  string
	 */
	public static function admin($return = 'current')
	{
		switch($return)
		{
			default:
			case 'current':
				return \Uri::create(\Request::active()->uri->_old_uri ?: \Request::active()->uri->uri);
				break;
			case 'string':
				return \Request::active()->uri->_old_uri ?: \Request::active()->uri->uri;
				break;
		}
	}
    
    /**
	 * Returns all segments in an array
	 *
	 * @return  array
	 */
	public static function segments()
	{
		if ($request = \Request::active())
		{
            $uri = \Uri::admin('string');
            
            $uri = trim($uri ?: \Input::uri(), '/');

            if (empty($uri))
            {
                $segments = array();
            }
            else
            {
                $segments = explode('/', $uri);
            }
            
			return $segments;
		}

		return null;
	}
    
     /**
	 * If you want to get correct URLs on frontend you have to use
	 * this method instead Uri::current() and Uri::string()
	 *
	 * @param   string   $return
	 * @param   string   $query_string
	 * @return  string
	 */
	public static function front_create($url = false, $query_string = false)
	{
		if($query_string && !empty($_SERVER['QUERY_STRING'])) $query_string = '?' . $_SERVER['QUERY_STRING'];
		else $query_string = '';
	
		switch($url)
		{
			default:
				return (\Uri::create($url !== false ? '/' . $url : '/' )) . $query_string;
				break;
		}
	}
	
	/**
	 * If you want to get correct URLs on frontend you have to use
	 * this method instead Uri::current() and Uri::string()
	 *
	 * @param   string   $return
	 * @param   string   $query_string
	 * @param   bool     $main_request
	 * @return  string
	 */	
	public static function front($return = 'current', $query_string = false, $main_request = true)
	{
		if($query_string && !empty($_SERVER['QUERY_STRING'])) $query_string = '?' . $_SERVER['QUERY_STRING'];
		else $query_string = '';
	
		if($main_request) $request = 'main';
			else $request = 'active';
			
		switch($return)
		{
			default:
			case 'current':				
				return (\Uri::create(\Request::$request()->uri->_old_uri ?: \Request::$request()->uri->uri)) . $query_string;
				break;			
			case 'string':
				return (\Request::$request()->uri->_old_uri ?: \Request::$request()->uri->uri) . $query_string;
				break;
			case 'segments':
				$uri = (\Request::$request()->uri->_old_uri ?: \Request::$request()->uri->uri);
				return $uri === '' ? array() : explode('/', $uri);
				break;
		}
	}
    
}