<?php

/**
 * EXTENDED VALIDATION FUEL CORE CLASS
 *
 * Extended core fuel class
 *
 * @subpackage system
 * @version 1.0
 * @author Eximius Solutions Development Team
 * @copyright 2011 - 2012 Eximius Solutions
 *
 * @todo
 *
 */

abstract class Image_Driver extends \Fuel\Core\Image_Driver
{
	/**
	 * EDIT: It happened that saved image is saved with miltiple dots before extension
	 * when called this function from presets config.
	 * 
	 * $extension is passed like empty string and not null, and we need to double check
	 * that in that case.
	 */
	
	/**
	 * Saves the file in the original location, adding the append and prepend to the filename.
	 *
	 * @param   string   $append       The string to append to the filename
	 * @param   string   $prepend      The string to prepend to the filename
	 * @param   string   $extension    The extension to save the image as, null defaults to the loaded images extension.
	 * @param   integer  $permissions  The permissions to attempt to set on the file.
	 * @return  Image_Driver
	 */
	public function save_pa($append, $prepend = null, $extension = null, $permissions = null)
	{
		$filename = substr($this->image_filename, 0, -(strlen($this->image_extension) + 1));
		$fullpath = $this->image_directory.'/'.$append.$filename.$prepend.'.'.
			($extension !== null ? $extension : $this->image_extension);
		
		if($extension !== null && empty($extension))
		{
			$fullpath = trim($fullpath, '.');
		}
			
		$this->save($fullpath, $permissions);
		return $this;
	}
}

