<?php
/**
 * EXTENDED VALIDATION FUEL CORE CLASS
 *
 * Extended core fuel class
 *
 * @subpackage system
 * @version 1.0
 * @author Eximius Solutions Development Team
 * @copyright 2011 - 2012 Eximius Solutions
 *
 * @todo
 *
 */
class Database_Query  extends \Fuel\Core\Database_Query
{

	/**
	 * Enable caching queries in a slightly different way.
	 * Current behaviour was if you enter $this->_cache_key cache will always be saved by that name,
	 * meaning that if query changes slightly (like when searching) it will still pull cached query
	 * 
	 * Now if your cache_key ends with dot (.) system will just create folders from your cache key name, 
	 * and save cache old fasioned way (hashing it)
	 * 
	 * So you will still have ability to do things like \Cache::delete('page') and have all different queries hashed
	 */
	
	/**
	 * Execute the current query on the given database.
	 *
	 * @param   mixed    Database instance or name of instance
	 * @return  object   Database_Result for SELECT queries
	 * @return  mixed    the insert id for INSERT queries
	 * @return  integer  number of affected rows for all other queries
	 */
	public function execute($db = null)
	{
		if ( ! is_object($db))
		{
			// Get the database instance
			$db = \Database_Connection::instance($db);
		}

		// Compile the SQL query
		$sql = $this->compile($db);

		switch(strtoupper(substr(ltrim($sql,'('), 0, 6)))
		{
			case 'SELECT':
				$this->_type = \DB::SELECT;
				break;
			case 'INSERT':
			case 'CREATE':
				$this->_type = \DB::INSERT;
				break;
		}

		if ($db->caching() and ! empty($this->_lifetime) and $this->_type === DB::SELECT)
		{
			$cache_key = empty($this->_cache_key) ?
				'db.'.md5('Database_Connection::query("'.$db.'", "'.$sql.'")') : $this->_cache_key;
			
			if(is_string($this->_cache_key) && substr($this->_cache_key, -1) == '.')
				$cache_key = $this->_cache_key.md5('Database_Connection::query("'.$db.'", "'.$sql.'")');
			
			$cache = \Cache::forge($cache_key);
			try
			{
				$result = $cache->get();
				return new Database_Result_Cached($result, $sql, $this->_as_object);
			}
			catch (CacheNotFoundException $e) {}
		}

		// Execute the query
		\DB::$query_count++;
		$result = $db->query($this->_type, $sql, $this->_as_object);

		// Cache the result if needed
		if (isset($cache) and ($this->_cache_all or $result->count()))
		{
			$cache->set_expiration($this->_lifetime)->set_contents($result->as_array())->set();
		}

		return $result;
	}

}
