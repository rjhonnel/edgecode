<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>FuelPHP Framework</title>
	<?php echo Asset::css('grid.css'); ?>
	<?php echo Asset::js('jquery-1.7.1.min.js'); ?>
	<?php echo Asset::js('grid-1.0.min.js'); ?>
	
</head>
<body>
	<?php echo $grid; ?>
</body>
</html>
