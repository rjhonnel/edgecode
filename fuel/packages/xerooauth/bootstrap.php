<?php

//\Autoloader::add_namespace('XeroOAuth');

\Autoloader::add_core_namespace('XeroOAuth');

//\Autoloader::add_core_namespace('XeroOAuth', true);

\Autoloader::add_classes(array(

	'XeroOAuth\\XeroOAuth'  => __DIR__.'/classes/XeroOAuth.php',

	)
);
