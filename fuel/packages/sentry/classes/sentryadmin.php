<?php

use Sentry\Sentry;

class SentryAdmin extends Sentry
{

	/**
	 * Get's either the currently logged in user or the specified user by id or Login
	 * Column value.
	 *
	 * @param   int|string  User id or Login Column value to find.
	 * @throws  SentryAuthException
	 * @return  Sentry_User
	 */
	public static function user($id = null, $recache = false)
	{
		if ($id === null and $recache === false and static::$current_user !== null)
		{
			return static::$current_user;
		}
		elseif ($id !== null and $recache === false and isset(static::$user_cache[$id]))
		{
			return static::$user_cache[$id];
		}

		try
		{
			if ($id)
			{
				static::$user_cache[$id] = new Sentry_User($id);
				return static::$user_cache[$id];
			}
			// if session exists - default to user session
			else if(static::check())
			{
				$user_id = Session::get(Config::get('sentry.session.user_admin'));
				static::$current_user = new \Sentry_User($user_id);
				return static::$current_user;
			}
		}
		catch (SentryUserNotFoundException $e)
		{
			throw new SentryAuthException($e->getMessage());
		}

		// else return empty user
		return new \Sentry_User();
	}

	/**
	 * Attempt to log a user in.
	 *
	 * @param   string  Login column value
	 * @param   string  Password entered
	 * @param   bool    Whether to remember the user or not
	 * @return  bool
	 * @throws  SentryAuthException
	 */
	public static function login($login_column_value, $password, $remember = false, $hash_password = '')
	{
		// log the user out if they hit the login page
		static::logout(); 

		// get login attempts
		if (static::$suspend)
		{
			$attempts = static::attempts($login_column_value, \Input::real_ip());

			// if attempts > limit - suspend the login/ip combo
			if ($attempts->get() >= $attempts->get_limit())
			{
				try
				{
					$attempts->suspend();
				}
				catch(SentryUserSuspendedException $e)
				{
					throw new \SentryAuthException($e->getMessage());
				}
			}
		}

		// make sure vars have values
		if (empty($login_column_value) or empty($password))
		{
			return false;
		}

		// if user is validated
		if ($user = static::validate_user($login_column_value, $password, 'password',$hash_password))
		{
			if (static::$suspend)
			{
				// clear attempts for login since they got in
				$attempts->clear();
			}

			// set update array
			$update = array();

			// if they wish to be remembers, set the cookie and get the hash
			if ($remember)
			{
				$update['remember_me'] = static::remember($login_column_value);
			}

			// if there is a password reset hash and user logs in - remove the password reset
			if ($user->get('password_reset_hash'))
			{
				$update['password_reset_hash'] = '';
				$update['temp_password'] = '';
			}

			$update['last_login'] = time();
			$update['ip_address'] = \Input::real_ip();

			// update user
			if (count($update))
			{
				$user->update($update, false);
			}

			// set session vars
			Session::set(Config::get('sentry.session.user_admin'), (int) $user->get('id'));
			Session::set(Config::get('sentry.session.provider_admin'), 'Sentry');

			return true;
		}

		return false;
	}

	/**
	 * Force Login
	 *
	 * @param   int|string  user id or login value
	 * @param   provider    what system was used to force the login
	 * @return  bool
	 * @throws  SentryAuthException
	 */
	public static function force_login($id, $provider = 'Sentry-Forced')
	{
		// check to make sure user exists
		if ( ! static::user_exists($id))
		{
			throw new \SentryAuthException(__('sentry.user_not_found'));
		}

		Session::set(Config::get('sentry.session.user_admin'), $id);
		Session::set(Config::get('sentry.session.provider_admin'), $provider);
		return true;
	}

	/**
	 * Checks if the current user is logged in.
	 *
	 * @return  bool
	 */
	public static function check()
	{
		// get session
		$user_id = Session::get(Config::get('sentry.session.user_admin'));

		// invalid session values - kill the user session
		if ($user_id === null or ! is_numeric($user_id))
		{
			// if they are not logged in - check for cookie and log them in
			if (static::is_remembered())
			{
				return true;
			}
			//else log out
			static::logout();

			return false;
		}

		return true;
	}

	/**
	 * Logs the current user out.  Also invalidates the Remember Me setting.
	 *
	 * @return  void
	 */
	public static function logout()
	{
		Cookie::delete(Config::get('sentry.remember_me.cookie_name_admin'));
		Session::delete(Config::get('sentry.session.user_admin'));
		Session::delete(Config::get('sentry.session.provider_admin'));
	}

	/**
	 * Remember User Login
	 *
	 * @param int
	 */
	protected static function remember($login_column)
	{
		// generate random string for cookie password
		$cookie_pass = \Str::random('alnum', 24);

		// create and encode string
		$cookie_string = base64_encode($login_column.':'.$cookie_pass);

		// set cookie
		\Cookie::set(
			\Config::get('sentry.remember_me.cookie_name_admin'),
			$cookie_string,
			\Config::get('sentry.remember_me.expire')
		);

		return $cookie_pass;
	}

	/**
	 * Check if remember me is set and valid
	 */
	protected static function is_remembered()
	{
		$encoded_val = \Cookie::get(\Config::get('sentry.remember_me.cookie_name_admin'));

		if ($encoded_val)
		{
			$val = base64_decode($encoded_val);
			list($login_column, $hash) = explode(':', $val);

			// if user is validated
			//	if ($user = static::validate_user($login_column, $hash, 'remember_me')) // Original Sentry code
            if (static::user_exists($login_column) && $user = static::validate_user($login_column, $hash, 'remember_me'))
			{
				// update last login
				$user->update(array(
					'last_login' => time()
				));

				// set session vars
				Session::set(Config::get('sentry.session.user_admin'), (int) $user->get('id'));
				Session::set(Config::get('sentry.session.provider_admin'), 'Sentry');

				return true;
			}
			else
			{
				static::logout();

				return false;
			}
		}

		return false;
	}
}