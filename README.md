EDGECOMEMRCE
========================================

### Application Hierarchy

	/fuel, which contains:
		/fuel/app: Everything related to the application. This is where you will work most of the time. 
		/fuel/core: The core classes and configuration. You should not change anything inside it.
		/fuel/packages: Packages are core extensions, they are bundles containing reusable classes and configuration files. Using the FuelPHP default configuration, 
						this is the only directory where you can install packages. Default FuelPHP packages are auth, email, oil, orm & parser. Others are used exclusively to the current application.
		/vendor: This directory contains third-party packages and libraries that are generally not FuelPHP-specific.
	/themes: This directory is accessible by external visitors. You want to put here files publicly available, as CSS, JS & other files for instance.
	/assets: This direcory is for the global files used, e.g: IMAGES, CSS, JS, third-party packages or others. 
	/media: This directory is used to save by the third-party e.g: ckeditor or ckfinder or the images

### These are the constants that define the location of the most important directories:

	* APPPATH: Application directory (fuel/app)
	* COREPATH: Core directory (fuel/core)
	* PKGPATH: Packages directory (fuel/packages)
	* VENDORPATH: Vendor directory (fuel/vendor)

### Environment 
 - set in "fuel/app/bootstrap.php" line 73-74
 - active environment is "development"

### Page
 - For themes/frontend/views/page, file name with format like this "_products_118.php" means 118 is the ID of the page. Format must be followed to read the file.

### Note:
 - when creating new columns or updating columns, use fuelphp migrations https://fuelphp.com/docs/general/migrations.html
 - run "php oil refine migrate -all" in your terminal to update database starting branch v3.5, this command is included in setup but you will need to run this if there are newly created migration files that you pull.


# **HOW TO SETUP** #
* Clone the repo "git clone https://lm-lily@bitbucket.org/lightmediadev/edgecommerce.git"
* Run "composer update"in your terminal inside the project folder
* add **fuel/app/config/config.php** file and copy example content from **fuel/app/config/config.php.example**
* add **.htaccess** file and copy example content from **.htaccess.example**
* refresh home page then you will see Edge Commerce Setup and proceed by filling the form.
![edgecommerce setup.jpg](https://bitbucket.org/repo/6EgoXA/images/2201121802-edgecommerce%20setup.jpg)


### Sample sites:
* http://demo.edgecommerce.org/
* http://store.tsikot.com/store/
* http://eccocatering.lmweb.com.au/
* http://netpharm.lmweb.com.au/
* http://nicit.com.au/
* http://aluminium.lmweb.com.au/