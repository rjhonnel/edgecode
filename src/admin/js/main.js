$.sidebarMenu = function(menu) {
	var animationSpeed = 300,
		subMenuSelector = '.sidebar-submenu';

	$(menu).on('click', 'li a', function(e) {
		var $this = $(this);
		var checkElement = $this.next();

		if (checkElement.is(subMenuSelector) && checkElement.is(':visible')) {
			checkElement.slideUp(animationSpeed, function() {
				checkElement.removeClass('menu-open');
			});
			checkElement.parent("li").removeClass("active");
		}

		//If the menu is not visible
		else if ((checkElement.is(subMenuSelector)) && (!checkElement.is(':visible'))) {
			//Get the parent menu
			var parent = $this.parents('ul').first();
			//Close all open menus within the parent
			var ul = parent.find('ul:visible').slideUp(animationSpeed);
			//Remove the menu-open class from the parent
			ul.removeClass('menu-open');
			//Get the parent li
			var parent_li = $this.parent("li");

			//Open the target menu and add the menu-open class
			checkElement.slideDown(animationSpeed, function() {
				//Add the class active to the parent li
				checkElement.addClass('menu-open');
				parent.find('li.active').removeClass('active');
				parent_li.addClass('active');
			});
		}
		//if this isn't a link, prevent the page from being redirected
		if (checkElement.is(subMenuSelector)) {
			e.preventDefault();
		}
	});
}


function abortAjax(x) {
	return x && x.readyState != 4 && x.abort(); // abort ajax
}

function isScreenSmall() {
	return $(window).width() <= APP.SCREEN_MD_MAX_WIDTH;
}

function focusOnTop()
{
	$('body').scrollTop(0);
}

jQuery(function() {
	var $body = $('body');


	// $.sidebarMenu($('.sidebar-menu'));

	$('#left_menu').find('.dropdown').on({
		"shown.bs.dropdown": function() {
			var open;
			open = $(this).siblings().filter('.open');
			if (open.length) {
				open.removeClass('open');
			}

			this.closable = false;
		},
		"click":             function(e) {
			var $target = $(e.target);
			this.closable = $target.hasClass('dropdown-toggle');
		},
		"hide.bs.dropdown":  function(e) {
			var $target = $(e.target);

			var ret = this.closable;
			this.closable = false;
			console.log(ret);
			return ret;
		}
	});

	$('#left_menu').find('.dropdown.open').removeClass('open').find('.dropdown-toggle').dropdown('toggle');
});



$(function() {
	fix();
	$(window, ".main_wrapper").resize(function() {
		fix();
	})

	function fix() {
		$(".layout-boxed > .wrapper").css("overflow", "hidden");
		var a = $(".main-footer").outerHeight() || 0
			, e = $(".navbar-fixed-top").outerHeight()
			, b = $(".navbar-fixed-top").outerHeight() + a
			, c = $(window).height()
			, d = $(".navbar-sidebar").height() || 0;
		$(".main-content-body").css("min-height", c - a - e - 29);

	}

});



$(document).ready(function (){
	//Export
	$('.export_table').click(function(e){
		e.preventDefault();
		window.location = $(this).data('href');
	});


	// from customer notes when editting single note
	$('.edit-note').click(function(e){
		e.preventDefault();
		$(this).closest('.note').addClass('is-edit');
	});
	$('.cancel-edit-note').click(function(e){
		e.preventDefault();
		$(this).closest('.note').removeClass('is-edit');
	});
	$('.update-edit-note').click(function(e){
		e.preventDefault();

		var _this = $(this);
		var note_value = _this.closest('.note').find('.note-state-edit textarea').val();
		$.ajax({
			type: 'POST',
			url: _this.data('url'),
			data: {
				'note': note_value,
			},
			success: function(data) {
				_this.closest('.note').find('.note-item-content .note-content').text(note_value);
				_this.closest('.note').removeClass('is-edit');
			}
		});

	});

});